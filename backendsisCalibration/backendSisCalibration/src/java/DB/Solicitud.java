package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * author leudi
 */
@WebServlet(urlPatterns = {"/Solicitud"})
public class Solicitud extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * param request servlet request param response servlet response throws
     * ServletException if a servlet-specific error occurs throws IOException if
     * an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String url_archivo = Globales.url_archivo;

    public String DatosClientesCompletar(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        sql = "select nombres from clientes_contac where idcliente = " + cliente + " ORDER BY 1";
        String sql2 = "select cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||'), ' || pa.descripcion as direccion "
                + " from clientes_sede cs inner join ciudad ci on ci.id = cs.idciudad "
                + "                               inner join departamento d on d.id = ci.iddepartamento "
                + "                               inner join pais pa on pa.id = d.idpais  "
                + " where idcliente = " + cliente + " ORDER BY 1";
        String sql3 = "select email from clientes_contac where idcliente = " + cliente + " ORDER BY 1";
        String sql4 = "select coalesce(telefonos,'')  || '/' || coalesce(fax,'') as telefono from clientes_contac where idcliente = " + cliente + " ORDER BY 1";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DatosClientesCompletar") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->DatosClientesCompletar") + "|"
                + Globales.ObtenerDatosJSon(sql3, this.getClass() + "-->DatosClientesCompletar") + "|" + Globales.ObtenerDatosJSon(sql4, this.getClass() + "-->DatosClientesCompletar");
    }

    public String GenerarSolicitud(HttpServletRequest request) {

        int IdSolicitud = Globales.Validarintnonull(request.getParameter("IdSolicitud"));
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        int Sede = Globales.Validarintnonull(request.getParameter("Sede"));
        int Contacto = Globales.Validarintnonull(request.getParameter("Contacto"));

        String MRDirecion = request.getParameter("MRDirecion");
        String MRContacto = request.getParameter("MRContacto");
        String MRTelefono = request.getParameter("MRTelefono");
        String MRCorreo = request.getParameter("MRCorreo");
        String MEDirecion = request.getParameter("MEDirecion");
        String MEContacto = request.getParameter("MEContacto");
        String METelefono = request.getParameter("METelefono");
        String MECorreo = request.getParameter("MEDirecion");
        String Observacion = request.getParameter("Observacion");

        if (MRDirecion == null) {
            MRDirecion = "";
        }
        if (MRContacto == null) {
            MRContacto = "";
        }
        if (MRTelefono == null) {
            MRTelefono = "";
        }
        if (MRCorreo == null) {
            MRCorreo = "";
        }

        if (MEDirecion == null) {
            MEDirecion = "";
        }
        if (MEContacto == null) {
            MEContacto = "";
        }
        if (METelefono == null) {
            METelefono = "";
        }
        if (MECorreo == null) {
            MECorreo = "";
        }
        if (Observacion == null) {
            Observacion = "";
        }

        try {
            if (IdSolicitud == 0) {

                if (Globales.PermisosSistemas("SOLICITUD GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                sql = "select solicitud FROM contadores";
                int contador = Globales.Validarintnonull(Globales.ObtenerUnValor(sql)) + 1;
                sql = "select id from version_documento WHERE documento = 'SOLICITUD' ORDER BY revision desc limit 1";
                int version = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                sql = "INSERT INTO web.solicitud(solicitud, idcliente, idsede, idcontacto, idusuario, "
                        + "                       rdireccion, rcontacto, rtelefonos, rcorreo, "
                        + "                        edireccion, econtacto, etelefonos, ecorreo, observacion, idversion) "
                        + " VALUES(" + contador + "," + Cliente + "," + Sede + "," + Contacto + "," + idusuario
                        + ",'" + MRDirecion + "','" + MRContacto.toUpperCase() + "','" + MRTelefono + "','" + MRCorreo.toLowerCase() + "'"
                        + ",'" + MEDirecion + "','" + MEContacto.toUpperCase() + "','" + METelefono + "','" + MECorreo.toLowerCase() + "','" + Observacion + "'," + version + ");";
                sql += "UPDATE web.solicitud_detalle SET solicitud = " + contador + " WHERE solicitud = 0 and idcliente = " + Cliente + ";";
                sql += "UPDATE contadores SET solicitud = " + contador + ";";
                sql += "UPDATE remision_detalle rd set solicitud=" + contador
                        + " FROM web.solicitud_detalle sd where sd.ingreso > 0 and sd.ingreso = rd.ingreso and sd.solicitud = " + contador + ";";
                if (Globales.DatosAuditoria(sql, "SOLICTUD", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GenerarSolicitud")) {
                    sql = "SELECT id from web.solicitud where solicitud = " + contador;
                    IdSolicitud = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    return "0|Solicitud generada con el número |" + contador + "|" + IdSolicitud;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {

                if (Globales.PermisosSistemas("SOLICITUD EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                sql = "SELECT estado, idusuario, solicitud FROM web.solicitud WHERE id = " + IdSolicitud + " and idcliente =" + Cliente;
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GenerarSolicitud", 1);
                datos.next();
                String estado = datos.getString("estado");
                int usuario = Globales.Validarintnonull(datos.getString("idusuario"));
                int solictud = Globales.Validarintnonull(datos.getString("solicitud"));

                if (!estado.equals("Temporal") && !estado.equals("Pendiente") && !estado.equals("")) {
                    return "1|No se puede editar una solicitud en estado " + estado;
                }
                if (usuario == 0) {
                    return "1|No se puede editar una solicitud realizada por el cliente";
                }
                sql = "UPDATE web.solicitud "
                        + "      SET idsede=" + Sede + ", idcontacto=" + Contacto + ", idusuario=" + idusuario
                        + ",rdireccion='" + MRDirecion + "', rcontacto='" + MRContacto + "', rtelefonos='" + MRTelefono + "', rcorreo='" + MRCorreo
                        + "',edireccion='" + MEDirecion + "', econtacto='" + MEContacto + "', etelefonos='" + METelefono + "', ecorreo='" + MECorreo
                        + "',observacion='" + Observacion + "' WHERE id=" + IdSolicitud + ";";
                sql += "UPDATE remision_detalle rd set solicitud=" + solictud
                        + "FROM web.solicitud_detalle sd where sd.ingreso > 0 and sd.ingreso = rd.ingreso and sd.solicitud = " + solictud + ";";
                if (Globales.DatosAuditoria(sql, "SOLICITUD", "EDITAR", idusuario, iplocal, this.getClass() + "-->GenerarSolicitud")) {
                    return "0|Solicitud actualizada con el número";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String BuscarSolicitud(HttpServletRequest request) {
        int solicitud = Globales.Validarintnonull(request.getParameter("solicitud"));
        sql = "select s.id, s.solicitud, idcliente, idsede, idcontacto, s.estado, observacion, to_char(s.fecha, 'dd/MM/yyyy HH24:MI') as fecha, u.nombrecompleto as usuario, c.documento, "
                + "  rdireccion, rcontacto, rtelefonos, rcorreo, edireccion, econtacto, etelefonos, ecorreo,  "
                + "  ua.nombrecompleto as usuarioanula, to_char(fechaanulado, 'dd/MM/yyyy HH24:MI') as fechaanulado, observacionanula "
                + "  from web.solicitud s inner join seguridad.rbac_usuario u on s.idusuario = u.idusu "
                + "		                         inner join clientes c on c.id = s.idcliente "
                + "                       inner join seguridad.rbac_usuario ua on s.idusuarioanula = ua.idusu "
                + "  where solicitud = " + solicitud;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarSolicitud");
    }

    public String BuscarRemision(HttpServletRequest request) {
        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        try {
            String sql2 = "select r.id, r.remision, idcliente, idsede, idcontacto, c.documento, c.nombrecompleto "
                    + "  from remision r inner join clientes c on c.id = r.idcliente "
                    + "  where remision = " + remision;
            datos = Globales.Obtenerdatos(sql2, this.getClass() + "-->BuscarRemision", 1);
            if (datos.next()) {
                sql = "SELECT count(solicitud) from remision_detalle rd inner join remision r on r.id = rd.idremision where solicitud = 0 and remision = " + remision;
                int cantidad = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                if (cantidad == 0) {
                    return "1|No hay ingresos sin solicitud en esta remisión";
                }
                String cliente = datos.getString("nombrecompleto");

                sql = "INSERT INTO web.solicitud_detalle(idcliente, solicitud, ingreso, idmagnitud, idequipo, idmarca, idmodelo, idintervalo, "
                        + "      acreditado, sitio, express, subcontratado, "
                        + "      punto, metodo, direccion, declaracion, certificado, cantidad, "
                        + "      serie, observacion, nombreca) "
                        + "  SELECT " + datos.getString("idcliente") + ",0,ingreso,idmagnitud,idequipo, idmarca, idmodelo,idintervalo, "
                        + "      'NO','NO','NO','NO', "
                        + "      'N/A','LABORATORIO','SEDE','N/A','DIGITAL',1, "
                        + "      serie,'N/A','" + cliente + "' "
                        + "  from remision_detalle rd inner join remision r on r.id = rd.idremision "
                        + "                           inner join equipo e on rd.idequipo = e.id  "
                        + "		                  inner join magnitudes m on m.id = e.idmagnitud "
                        + "                           inner join modelos mo on mo.id = rd.idmodelo "
                        + "				  inner join marcas ma on ma.id = mo.idmarca "
                        + "  WHERE r.remision = " + remision + ";";
                sql += "UPDATE remision_detalle rd SET solicitud = -1 "
                        + "  FROM remision r "
                        + "  WHERE r.id = rd.idremision and solicitud = 0 and remision = " + remision;
                if (Globales.DatosAuditoria(sql, "REMISION", "AGREGAR", idusuario, iplocal, this.getClass() + "-->BuscarRemision")) {
                    return "0|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->BuscarRemision") + "|" + cantidad + " equipo(s) agregado a la solicitud";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                return "1|El número de remisión " + remision + " no exixte";
            }
        } catch (Exception e) {
            return "1|" + e.getMessage();
        }
    }

    public String AgregarIngreso(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int idcliente = Globales.Validarintnonull(request.getParameter("idcliente"));
        int Solicitud = Globales.Validarintnonull(request.getParameter("Solicitud"));

        sql = "SELECT estado FROM web.solicitud WHERE solicitud = " + Solicitud + " AND idcliente=" + idcliente;
        String estado = Globales.ObtenerUnValor(sql);
        if (!estado.equals("Temporal") && !estado.equals("Pendiente") && !estado.equals("")) {
            return "1|No se puede editar una solicitud en estado " + estado;
        }
        try {
            if (ingreso > 0) {
                sql = "SELECT solicitud from remision_detalle where ingreso = " + ingreso;
                int numsolicitud = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                if (numsolicitud > 0) {
                    return "1|A este ingreso ya se le generó la solicitud";
                }

                String cliente = Globales.ObtenerUnValor("SELECT nombrecompleto FROM clientes where id=" + idcliente);

                sql = "INSERT INTO web.solicitud_detalle(idcliente, solicitud, ingreso, idmagnitud, idequipo, idmarca, idmodelo, idintervalo, "
                        + "  acreditado, sitio, express, subcontratado,  "
                        + "  punto, metodo, direccion, declaracion, certificado, cantidad, "
                        + "  serie, observacion, nombreca) "
                        + " SELECT " + idcliente + "," + Solicitud + ",ingreso,idmagnitud,idequipo, idmarca, idmodelo,idintervalo, "
                        + "  'NO','NO','NO','NO', "
                        + "  'N/A','LABORATORIO','SEDE','N/A','DIGITAL',1, "
                        + "  serie,'N/A','" + cliente + "'  "
                        + " from remision_detalle r inner join equipo e on r.idequipo = e.id  "
                        + "			    inner join magnitudes m on m.id = e.idmagnitud "
                        + "                         inner join modelos mo on mo.id = r.idmodelo  "
                        + "                         inner join marcas ma on ma.id = mo.idmarca "
                        + " WHERE r.ingreso = " + ingreso + ";";
                sql += "UPDATE remision_detalle SET solicitud = -1 WHERE ingreso = " + ingreso;
                if (Globales.DatosAuditoria(sql, "SOLICITUD", "AGREGAR INGRESO", idusuario, iplocal, this.getClass() + "-->AgregarIngreso")) {
                    return "0|Ingreso agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                return "0|";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String TablaSolicitudDetalle(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int solicitud = Globales.Validarintnonull(request.getParameter("solicitud"));
        int json = Globales.Validarintnonull(request.getParameter("json"));

        if (id == 0) {
            if (json == 0) {
                sql = "SELECT cd.id, cd.idmagnitud, cd.idintervalo, cd.ingreso,               '<b>' \n"
                        + "|| m.descripcion || '</b><br>' || e.descripcion || '<br><b>' \n"
                        + "|| ma.descripcion || '</b><br>' || mo.descripcion || '<br><b>' || case when cd.idintervalo = 0 then 'VER ESPECIFICACIONES' "
                        + "ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '</b><br>' || serie as equipo,               "
                        + "'<b>Acreditado:</b> ' || acreditado || '<br><b>Sitio:</b> ' || sitio || '<br><b>Express:</b> ' || express || '<br><b>SubContratado:</b> ' || "
                        + "subcontratado as servicio,               "
                        + " '<b>'||punto || '</b></br>' || metodo as metodo, "
                        + "'<b>'||nombreca || '</b></br>' || direccion as direccion,              "
                        + " '<b>'||coalesce(proxima,'') || '</b></br>' || certificado as certificado,           "
                        + "   declaracion, cantidad, observacion,   "
                        + "'<button class=''btn btn-glow btn-danger'' title=''Eliminar Item'' type=''button'' onclick=' || chr(34) || "
                        + "'EliminarItem(' || cd.id || ',''' || e.descripcion || ' ' || ma.descripcion || ' ' || mo.descripcion || ''')' ||chr(34) || '>"
                        + "<span data-icon=''&#xe0d7;''></span></button>' as opcion, idarticulo, idotroservicio, coalesce(os.descripcion,'') as otroservicio "
                        + "FROM web.solicitud_detalle cd inner join magnitudes m on m.id = cd.idmagnitud "
                        + "     inner join magnitud_intervalos i on i.id = cd.idintervalo"
                        + "inner join equipo e on e.id = cd.idequipo"
                        + "inner join modelos mo on mo.id = cd.idmodelo"
                        + "inner join marcas ma on ma.id = cd.idmarca  \n"
                        + "left join otros_servicios os on cd.idotroservicio = os.id  "
                        + "WHERE idcliente =" + cliente + " and solicitud = " + solicitud + " ORDER BY cd.id";
            } else {

                sql = "SELECT cd.id, cd.idmagnitud, cd.idintervalo, cd.idmarca, cd.idequipo as idequipo, cd.idmodelo as idmodelo,  m.descripcion as magnitud, \n"
                        + " e.descripcion as equipo,  ma.descripcion as marca,  mo.descripcion as modelo,cd.ingreso\n"
                        + "|| case when cd.idintervalo = 0 then 'VER ESPECIFICACIONES'\n"
                        + "ELSE '(' || desde || ' a ' || hasta || ') ' || medida end  \n"
                        + "as intervalo, serie as serie, acreditado as acreditado, \n"
                        + "sitio as sitio, express as express,  \n"
                        + "subcontratado as subcontratado,                \n"
                        + "punto as punto, metodo as metodo, nombreca as nombreca\n"
                        + ",direccion as direccion, coalesce(proxima,'') as proxima,\n"
                        + "certificado as certificado,              \n"
                        + "declaracion as declaracion, cantidad as cantidad, observacion as observacion , idarticulo, idotroservicio, cd.ingreso as ingreso, \n"
                        + "os.descripcion as otroservicio, os.id as idoservicio\n"
                        + "FROM web.solicitud_detalle cd inner join magnitudes m on m.id = cd.idmagnitud                                 \n"
                        + "inner join magnitud_intervalos i on i.id = cd.idintervalo                             \n"
                        + "inner join equipo e on e.id = cd.idequipo inner join modelos mo on mo.id = cd.idmodelo \n"
                        + "inner join marcas ma on ma.id = cd.idmarca  \n"
                        + "left join otros_servicios os on cd.idotroservicio = os.id  "
                        + "WHERE idcliente =" + cliente + " and solicitud = " + solicitud + " ORDER BY cd.id";
            }

        } else {
            sql = "SELECT id, ingreso, idcliente, solicitud, idmagnitud, idequipo, idmarca, idmodelo, "
                    + "      idintervalo, acreditado, sitio, express, subcontratado, punto, "
                    + "      metodo, direccion, declaracion, certificado, coalesce(proxima,'') as proxima, cantidad, "
                    + "  serie, observacion, nombreca from web.solicitud_detalle WHERE id = " + id;
        }
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaSolicitudDetalle");
    }

    public String AgregarItemCotizacion(HttpServletRequest request) {

        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int Magnitud = Globales.Validarintnonull(request.getParameter("Magnitud"));
        int Intervalo = Globales.Validarintnonull(request.getParameter("Intervalo"));
        String Serie = request.getParameter("Serie");
        int Cantidad = Globales.Validarintnonull(request.getParameter("Cantidad"));
        String Acreditacion = request.getParameter("Acreditacion");
        String Sitio = request.getParameter("Sitio");
        String Express = request.getParameter("Express");
        int Articulo = Globales.Validarintnonull(request.getParameter("Articulo"));
        int Servicio = Globales.Validarintnonull(request.getParameter("Servicio"));
        String SubContratado = request.getParameter("SubContratado");
        String Punto = request.getParameter("Punto");
        String Metodo = request.getParameter("Metodo");
        String NombreCa = request.getParameter("NombreCa");
        String Direccion = request.getParameter("Direccion");
        String Declaracion = request.getParameter("Declaracion");
        String Observacion = request.getParameter("Observacion");
        String Proxima = request.getParameter("Proxima");
        String Certificado = request.getParameter("Certificado");
        int Equipo = Globales.Validarintnonull(request.getParameter("Equipo"));
        int Marca = Globales.Validarintnonull(request.getParameter("Marca"));
        int Modelo = Globales.Validarintnonull(request.getParameter("Modelo"));
        int Solicitud = Globales.Validarintnonull(request.getParameter("Solicitud"));

        try {

            if (Solicitud > 0) {
                sql = "SELECT estado, idusuario FROM web.solicitud WHERE solicitud = " + Solicitud + " AND idcliente=" + Cliente;;
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AgregarItemCotizacion", 1);
                datos.next();
                String estado = datos.getString("estado");
                int usuarios = Globales.Validarintnonull(datos.getString("idusuario"));

                if (!estado.equals("Temporal") && !estado.equals("Pendiente") && !estado.equals("")) {
                    return "1|No se puede editar una solicitud en estado " + estado;
                }
                if (usuarios == 0) {
                    return "1|No se puede editar una solicitud realizada por el cliente";
                }
            }

            if (Id == 0) {
                sql = "SELECT id FROM web.solicitud_detalle WHERE solicitud = " + Solicitud + " and idmagnitud = " + Magnitud + " and idequipo = " + Equipo + " and idmodelo = " + Modelo + " and idmarca = " + Marca
                        + " and idintervalo = " + Intervalo + " and coalesce(serie,'') = '" + Serie + "' and nombreca ='" + NombreCa + "' and direccion='" + Direccion + "' and idarticulo=" + Articulo + " and idotroservicio=" + Servicio;
                int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                if (encontrado > 0) {
                    return "1|Ya existe un instrumento agregado con estas especificaciones.<br> Si desea agregar otra cantidad edite el item de la cotización";
                }

                sql = "INSERT INTO web.solicitud_detalle(Solicitud, idcliente, idmagnitud, idintervalo, idequipo, idmarca, idmodelo, acreditado, "
                        + "      sitio, express, subcontratado, punto, metodo, direccion, declaracion, certificado, proxima, cantidad, serie, "
                        + "      observacion, nombreca, idarticulo, idotroservicio) "
                        + " VALUES (" + Solicitud + "," + Cliente + "," + Magnitud + "," + Intervalo + ",'" + Equipo + "','" + Marca + "','" + Modelo + "','" + Acreditacion
                        + "','" + Sitio + "','" + Express + "','" + SubContratado + "','" + Punto + "','" + Metodo + "','" + Direccion + "','" + Declaracion
                        + "','" + Certificado + "','" + Proxima + "'," + Cantidad + ",'" + Serie + "','" + Observacion + "','" + NombreCa.toUpperCase().trim() + "'," + Articulo + "," + Servicio + ")";
                if (Globales.DatosAuditoria(sql, "SOLICITUD", "AGREGAR ITEMS COTIZACION", idusuario, iplocal, this.getClass() + "-->AgregarItemCotizacion")) {
                    return "0|Instrumento agregado a la solicitud de cotización";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                sql = "SELECT id FROM web.solicitud_detalle WHERE solicitud = " + Solicitud + " and id <> " + Id + " and idmagnitud = " + Magnitud + " and idequipo = " + Equipo + " and idmodelo = " + Modelo + " and idmarca = " + Marca
                        + " and idintervalo = " + Intervalo + " and coalesce(serie,'') = '" + Serie + "' and nombreca ='" + NombreCa + "' and direccion='" + Direccion + "';";
                int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                if (encontrado > 0) {
                    return "1|Ya existe un instrumento agregado con estas especificaciones.<br> Si desea agregar otra cantidad edite el item de la cotización";
                }

                sql = "UPDATE web.solicitud_detalle "
                        + "      SET idmagnitud=" + Magnitud + ",idequipo='" + Equipo + "', idmarca='" + Marca + "',idmodelo='" + Modelo + "',"
                        + "idintervalo=" + Intervalo + ", acreditado='" + Acreditacion + "',sitio='" + Sitio + "',express='" + Express + "',"
                        + "subcontratado='" + SubContratado + "',punto='" + Punto + "',metodo='" + Metodo + "',direccion='" + Direccion + "',"
                        + "declaracion='" + Declaracion + "',certificado='" + Certificado + "',proxima='" + Proxima + "',cantidad=" + Cantidad + ","
                        + "serie='" + Serie + "',observacion='" + Observacion + "',nombreca='" + NombreCa.toUpperCase().trim() + "' WHERE id = " + Id;
                if (Globales.DatosAuditoria(sql, "SOLICITUD", "EDITAR ITEMS COTIZACION", idusuario, iplocal, this.getClass() + "-->AgregarItemCotizacion")) {
                    return "0|Instrumento actualizado a la solicitud cotización";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarItemSolicitud(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int Solicitud = Globales.Validarintnonull(request.getParameter("Solicitud"));

        if (Globales.PermisosSistemas("SOLICITUD ELIMINAR ITEMS SOLICITUD", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        if (Solicitud > 0) {
            sql = "SELECT estado FROM web.solicitud inner join web.solicitud_detalle using (solicitud) WHERE solicitud = " + Solicitud;
            String estado = Globales.ObtenerUnValor(sql);
            if (!estado.equals("Temporal") && !estado.equals("Pendiente") && !estado.equals("")) {
                return "1|No se puede editar una solicitud en estado " + estado;
            }
        }
        try {
            sql = "UPDATE remision_detalle rd set solicitud = 0 "
                    + "  FROM web.solicitud_detalle sd "
                    + "  WHERE sd.ingreso = rd.ingreso and sd.id = " + id + ";";
            sql += "DELETE FROM web.solicitud_detalle WHERE id = " + id;
            if (Globales.DatosAuditoria(sql, "SOLICITUD", "ELIMINAR ITEMS COTIZACION", idusuario, iplocal, this.getClass() + "-->EliminarItemSolicitud")) {
                return "0|Item eliminado";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarSolicitud(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int Solicitud = Globales.Validarintnonull(request.getParameter("Solicitud"));

        if (Globales.PermisosSistemas("SOLICITUD ELIMINAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        sql = "SELECT estado FROM web.solicitud WHERE solicitud = " + Solicitud + " and idcliente=" + cliente;
        String estado = Globales.ObtenerUnValor(sql);
        if (!estado.equals("Temporal") && !estado.equals("")) {
            return "1|No se puede editar una solicitud en estado " + estado;
        }

        try {
            sql = "UPDATE remision_detalle rd set solicitud = 0 "
                    + "  FROM web.solicitud_detalle sd "
                    + "  WHERE sd.ingreso = rd.ingreso and sd.idcliente = " + cliente + " and sd.solicitud = 0;";
            sql += "DELETE FROM web.solicitud_detalle WHERE solicitud = 0 and idcliente = " + cliente;
            if (Globales.DatosAuditoria(sql, "SOLICITUD", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarSolicitud")) {
                return "0|Todos los items de la solicitud de cotización fueron eliminados";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1" + ex.getMessage();
        }
    }

    public String AnularSolicitud(HttpServletRequest request) {
        int solicitud = Globales.Validarintnonull(request.getParameter("solicitud"));
        String observacion = request.getParameter("observacion");
        if (Globales.PermisosSistemas("SOLICITUD ANULAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        try {
            sql = "SELECT estado, idusuario FROM web.solicitud WHERE solicitud = " + solicitud;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AnularSolicitud", 1);
            datos.next();
            String estado = datos.getString("estado");
            int usuarios = Globales.Validarintnonull(datos.getString("idusuario"));
            if (!estado.equals("Pendiente")) {
                return "1|No se puede anular una solicitud en estado " + estado;
            }
            if (usuarios == 0) {
                return "1|No se puede editar una solicitud realizada por el cliente";
            }
            sql = "update remision_detalle set solicitud = 0 "
                    + "  FROM web.solicitud_detalle sd "
                    + "  WHERE remision_detalle.ingreso = sd.ingreso and sd.solicitud = " + solicitud + ";";
            sql += "UPDATE web.solicitud SET estado='Anulado', idusuarioanula = " + idusuario + ", fechaanulado = now(), observacionanula = '" + observacion + "' WHERE solicitud = " + solicitud;
            if (Globales.DatosAuditoria(sql, "SOLICITUD", "ANULAR", idusuario, iplocal, this.getClass() + "-->AnularSolicitud")) {
                return "0|Solicitud anulada";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String ConsultarSolicitud(HttpServletRequest request) {
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        int solicitud = Globales.Validarintnonull(request.getParameter("solicitud"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String estado = request.getParameter("estado");
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = request.getParameter("equipo");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        String intervalo = request.getParameter("intervalo");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        int json = Globales.Validarintnonull(request.getParameter("json"));

        String busqueda = "WHERE 1=1 ";
        if (solicitud > 0) {
            busqueda += " AND s.solicitud = " + solicitud;
        } else if (cotizacion > 0) {
            busqueda += " AND s.cotizacion = " + cotizacion;
        } else {

            busqueda += " AND to_char(s.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(s.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
            if (cliente > 0) {
                busqueda += " AND s.idcliente = " + cliente;
            }
            if (!estado.equals("0")) {
                busqueda += " AND s.estado = '" + estado + "'";
            }
            if (!equipo.equals("")) {
                busqueda += " AND e.descripcion = '" + equipo + "'";
            }
            if (marca > 0) {
                busqueda += " AND mo.idmarca = " + marca;
            }
            if (!modelo.equals("")) {
                busqueda += " AND mo.descripcion = '" + modelo + "'";
            }
            if (!intervalo.equals("")) {
                busqueda += " AND '(' || desde || ' a ' || hasta || ') ' || medida = '" + intervalo + "'";
            }
            if (usuarios > 0) {
                busqueda += " AND s.asesor = " + usuarios;
            }
        }

        sql = "SELECT solicitud, to_char(fecha,'yyyy/MM/dd HH24:MI') as fecha, s.direccion as direccioncli, contacto, telefonos, c.email, "
                + "     s.observacion, c.nombrecompleto || ' (' || c.documento || ')'  as cliente, sum(sd.cantidad) as cantidad, "
                + "     '<b>' || to_char(fechaanulado,'yyyy/MM/dd HH24:MI')  || '</b><br>' || observacionanula as anulado, '<b>' || cotizacion  || '</b><br>' || to_char(fechacotizado,'yyyy/MM/dd HH24:MI') as cotizado, "
                + "     u.nombrecompleto as asesor, s.estado, "
                + "      '<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Solicitud'' type=''button'' onclick=' || chr(34) || 'ImprimirSolicitud(' || s.solicitud || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' || "
                + "      case when s.estado = 'Pendiente' and s.idusuario > 0 THEN '&nbsp;<button class=''btn btn-glow btn-danger anular'' title=''Aprobar/Reporbar Cotización'' type=''button'' " + (json == 0 ? "on" : "ng-") + "click=' || chr(34) || 'AnularSolicitud(' || s.solicitud || ')' ||chr(34) || '><span data-icon=''&#xe2a1;''></span></button>' ELSE '' END as opcion "
                + " FROM web.solicitud s INNER JOIN web.solicitud_detalle sd using (solicitud) "
                + "                     inner join magnitud_intervalos i on i.id = sd.idintervalo "
                + "                      inner join modelos mo on mo.id = sd.idmodelo "
                + "                     INNER JOIN clientes c on c.id = s.idcliente "
                + "                     INNER JOIN seguridad.rbac_usuario u on u.idusu = s.asesor " + busqueda
                + " GROUP BY solicitud, fecha, s.direccion, contacto, telefonos, c.email, "
                + "     s.observacion, c.nombrecompleto, c.documento, s.idusuario, "
                + "     fechaanulado,observacionanula, s.estado,cotizacion, fechacotizado,u.nombrecompleto "
                + " order by solicitud DESC";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarSolicitud");
    }

    public String ActualizarIngresoSol(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int solicitud = Globales.Validarintnonull(request.getParameter("solicitud"));
        try {
            if (Globales.PermisosSistemas("SOLICITUD ACTUALIZAR INGRESO", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "SELECT estado from web.solicitud where solicitud = " + solicitud;
            String estado = Globales.ObtenerUnValor(sql);
            if (!estado.equals("Temporal") && !estado.equals("Pendiente")) {
                return "1|No se puede editar un ingreso con la solicitud en estado " + estado;
            }
            sql = "UPDATE web.solicitud_detalle SET idequipo = rd.idequipo, idmodelo=rd.idmodelo, idintervalo=rd.idintervalo, idmarca= m.idmarca, idmagnitud=e.idmagnitud "
                    + " FROM remision_detalle rd inner join modelos m on m.id = rd.idmodelo "
                    + "                       inner join equipo e on e.id = rd.idequipo "
                    + " WHERE rd.ingreso = web.solicitud_detalle.ingreso and rd.ingreso = " + ingreso + " and web.solicitud_detalle.solicitud=" + solicitud;
            if (Globales.DatosAuditoria(sql, "SOLICITUD", "ACTUALIZAR INGRESO", idusuario, iplocal, this.getClass() + "-->ActualizarIngreso")) {
                sql = "select idequipo, idintervalo, idmagnitud, idmarca, idmodelo, serie "
                        + " from remision_detalle rd inner join modelos m on m.id = rd.idmodelo "
                        + "                       inner join equipo e on e.id = rd.idequipo "
                        + " where ingreso = " + ingreso;
                return "0|Ingreso actualizado con éxito|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ActualizarIngreso");
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setContentType("text/html;charset=UTF-8");
            String ruta_origen = request.getHeader("Origin");

            HttpSession misession = request.getSession();
            idusuario = misession.getAttribute("idusuario").toString();
            iplocal = misession.getAttribute("IPLocal").toString();
            usuario = misession.getAttribute("codusu").toString();
            response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
            response.setHeader("Access-Control-Allow-Credentials", "true");

            PrintWriter out = response.getWriter();
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else if (misession.getAttribute("codusu") == null) {
                out.print("cerrada");
            } else if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                out.print("cambio-ip");
            } else {
                String opcion = request.getParameter("opcion");
                switch (opcion) {
                    case "DatosClientesCompletar":
                        out.print(DatosClientesCompletar(request));
                        break;
                    case "GenerarSolicitud":
                        out.print(GenerarSolicitud(request));
                        break;
                    case "BuscarSolicitud":
                        out.print(BuscarSolicitud(request));
                        break;
                    case "BuscarRemision":
                        out.print(BuscarRemision(request));
                        break;
                    case "AgregarIngreso":
                        out.print(AgregarIngreso(request));
                        break;
                    case "TablaSolicitudDetalle":
                        out.print(TablaSolicitudDetalle(request));
                        break;
                    case "AgregarItemCotizacion":
                        out.print(AgregarItemCotizacion(request));
                        break;
                    case "EliminarItemSolicitud":
                        out.print(EliminarItemSolicitud(request));
                        break;
                    case "EliminarSolicitud":
                        out.print(EliminarSolicitud(request));
                        break;
                    case "AnularSolicitud":
                        out.print(AnularSolicitud(request));
                        break;
                    case "ConsultarSolicitud":
                        out.print(ConsultarSolicitud(request));
                        break;
                    case "ActualizarIngresoSol":
                        out.print(ActualizarIngresoSol(request));
                        break;

                    default:
                        throw new AssertionError();
                }
            }
        } catch (Exception ex) {
            try {
                PrintWriter out = response.getWriter();
                out.print(ex.getMessage());

            } catch (IOException exl) {
                Globales.GuardarLogger(exl.getMessage(), Laboratorio.class
                        .getName());
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * param request servlet request param response servlet response throws
     * ServletException if a servlet-specific error occurs throws IOException if
     * an I/O error occurs
     *
     * @param request
     * @param response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * param request servlet request param response servlet response throws
     * ServletException if a servlet-specific error occurs throws IOException if
     * an I/O error occurs
     *
     * @param request
     * @param response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

}
