d = new Date();
var Hora = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
localStorage.setItem("HoraInicio", "");
var totalchat = 0;
var UsuarioAcceso = "";
var usuariochat = localStorage.getItem('codusu');
var idusuariochat = localStorage.getItem('idusuario');
localStorage.setItem("vermensaje", "SI");

var ArchivoChat = "";
var PantallaBloqueada = 0;

var userName = usuariochat;
var websocket = null;

var url_servidor = "";
var url_cliente = "";
var url_socker = "";
var url_archivo = "";

$.ajaxSetup({
    xhrFields: {
        withCredentials: true
    },
    async: false
});

if (localStorage.getItem("TiempoAviso") == null)
	localStorage.setItem("TiempoAviso",0);


$.getJSON("../rutas.json", function(data) {
	localStorage.setItem("url_servidor", data[0].url_servidor);
	localStorage.setItem("url_cliente", data[0].url_cliente);
	localStorage.setItem("url_socker", data[0].socker);
	localStorage.setItem("url_archivo", data[0].url_archivo);
});


url_servidor = localStorage.getItem("url_servidor");
url_cliente = localStorage.getItem("url_cliente");
url_archivo = localStorage.getItem("url_archivo");
url_socker = localStorage.getItem("url_socker");

function VariableConexion(){
	var datos = LlamarAjax("RutaArchivo", "").split("||");
	if (datos[0] != "0"){
		swal("Acción Cancelada",datos[1],"warning");
		ewwewew;
	}
}

VariableConexion();

var AparatoMovil = 0;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
    AparatoMovil = 1;

_CD = "";
_CM = "";
_SM = "";
_DE = 0;

function misDocumentos() {
    //Aqui quede LEUDIS

    $("#SCambioNit").html(DocumentoCli);
    $("#modalCambioNitCli").modal("show");
}

function TabDocumentoEmpleados() {
    //MOstrando y llenando el mkodal para los documentos del usuario
    var parametros = "Empleado=" + 48 + "&Documento=0";
    var datos = LlamarAjax("Recursohumano","TablaDocumentosEmpleado&" + parametros);
    var data = JSON.parse(datos);
    var resultado = "";
    for (var x = 0; x < data.length; x++) {
        let descripcion = (data[x].descripcion !== undefined ? (data[x].descripcion) : descripcion);
        resultado += "<tr " + (data[x].obligatorio * 1 == 1 ? "class='bg-rojoclaro' " : "") + ">" +
            "<td>" + (data[x].nrodocumento * 1 > 0 ? data[x].nrodocumento : '') + "</td>" +
            "<td>" + ((data[x].nrodocumento * 1 == 0) ? "<strong>" + descripcion + "</strong>" : "     - Documento-" + data[x].nrodocumento * 1) + "</td>" +
            "<td>" +
            ((data[x].nrodocumento * 1 == 0) ? "" : "<button type = 'button' class='btn btn-glow btn-info btn-icon2' style = 'max-width:50px; padding-bottom: 3px;' title = 'Descargar PDF' onclick = 'javascript:DescargarPDF(" + (data[x].nrodocumento * 1 > 0 ? descripcion : '') + ",\"" + (data[x].nrodocumento * 1 > 0 ? data[x].id + "-" + data[x].nrodocumento : '') + "\")'><span style='font-size:18px; ' data-icon='&#xe22d;'></span></button >") +
            "</td > " +
            "</tr>";
    }
    $("#TablaDocumentos").html(resultado);
}


function AprobarCotizacion() {

}

function utf8_to_b64(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
}

function b64_to_utf8(str) {
    return decodeURIComponent(escape(window.atob(str)));
}

function MaximizarFotoChat(foto) {
    window.open(foto);
}

function MenuLLamarMA(ayuda) {
    localStorage.setItem("CodigoMesaAyuda", ayuda);
    LlamarOpcionMenu('GENERAL/mesa_trabajo', 2, 'ul_general');
}

function MostrarMensaje(name, message, fecha, idusuario) {
    var vermensaje = localStorage.getItem("vermensaje");
    var respuesta = "";
    var resumido = "";
    var fotochat = url_cliente + "Adjunto/imagenes/300.png";
    var foto = $.trim(localStorage.getItem('foto'));
    if (foto != "") {
        fotochat = url_cliente + "Adjunto/imagenes/FotoEmpleados/" + foto;
    }

    if (name == usuariochat) {
        respuesta = '<div class="message reversed">' +
            '<a class="message-img" href=\'javascript:MaximizarFotoChat("' + fotochat + '")\'><img src="' + fotochat + '" alt=""></a>' +
            "<div class='message-body' " + (message.indexOf("http") > -1 ? "" : "onclick =\"EscucharMensaje('" + message + "')\"") + ">" + message +
            '<span class="attribution">' + fecha + '</span></div></div>';

        resumido = '<li title="' + (message.indexOf("http") > -1 ? "" : message) + '" class="unread">' +
            '<a href = "javascript:LlamarChat()">' +
            '<img src="' + url_cliente + 'Adjunto/imagenes/300.png" alt="" class="user-face">' +
            '<strong>' + name + '</strong>' +
            '<strong>' + fecha + '</strong>' +
            '<span>' + message + '...</span></a></li>';
        totalchat += 1;

    } else {
        if (($.trim(idusuario) == "0") || ($.trim(idusuario).indexOf("/" + idusuariochat + "/")*1 > -1)) {
            respuesta = '<div class="message">' +
                '<a class="message-img" href = "#" > <img src="' + url_cliente + 'Adjunto/imagenes/300.png" alt=""></a>' +
                "<div class='message-body' " + (message.indexOf("http") > -1 ? "" : "onclick=\"EscucharMensaje('" + message + "')\"") + (idusuario != "0" ? " style='background-color:cornflowerblue'" : "") + ">" + message +
                '<span class="attribution"><b>' + name + '</b> ' + fecha + '</span></div></div>';

            resumido = '<li title="' + (message.indexOf("http") > -1 ? "" : message) + '" class="unread">' +
                '<a href = "javascript:LlamarChat()">' +
                '<img src="' + url_cliente + 'Adjunto/imagenes/300.png" alt="" class="user-face">' +
                '<strong>' + name + '</strong>' +
                '<strong>' + fecha + '</strong>' +
                '<span>' + message + '...</span></a></li>';
            totalchat += 1;
        }
    }


    resumido = resumido + $("#chatresumido").html();

    $("#cantidadchat").html(totalchat);
    $("#chatresumido").html(resumido);

    respuesta = $("#respuestachat").html() + respuesta;
    $("#respuestachat").html(respuesta);
    $("#respuestachat").animate({ scrollTop: $('#respuestachat')[0].scrollHeight }, 1000);
        
    if (($.trim(idusuario) == "0") || ($.trim(idusuario).indexOf("/" + idusuariochat + "/") > -1)) {
        if (vermensaje == "SI") {
            if (name != usuariochat) {

                swal.queue([{
                    title: ValidarTraduccion('Tienes un Mensaje!!!'),
                    text: name,
                    type: 'info',
                    showLoaderOnConfirm: true,
                    showCancelButton: true,
                    confirmButtonText: ValidarTraduccion('Ver Mensaje'),
                    cancelButtonText: ValidarTraduccion('Cancelar'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    preConfirm: function () {
                        return new Promise(function (resolve, reject) {
                            LlamarChat();
                            resolve();
                        })
                    }
                }]);
                EscucharMensaje("Tienes un mensaje de " + name.toLowerCase());
            }
        } else {
			if (name != usuariochat) {
				$.jGrowl("Tienes un mensaje de " + name, { life: 2500, theme: 'growl-success', header: '' });
				EscucharMensaje("Tienes un mensaje de " + name.toLowerCase());
				MensajeLeido();
			}
        }
    }
}

function DesbloquearCliente(documento, mensaje) {
    documento = $.trim(documento);
    if (documento == "-1")
        documento = $.trim($("#DCliente").val());
    if (documento == "")
        return false;
    var datos = LlamarAjax("Cotizacion","opcion=DesbloquearCliente&documento=" + documento).split("|");
    if (mensaje == 1) {
        if (datos[0] == "0") {
            swal("", datos[1], "success");
        } else {
            $("#DCliente").select();
            $("#DCliente").focus();
            swal("Acción Cancelada", datos[1], "warning");
        }
    }
}

function SubirArchivoChat() {
    swal.queue([{
        html: '<h3>¿' + ValidarTraduccion("Seleccione el archivo que desea subir ") + '?</h3><input type="file" class="form-control" id="documentochat" onchange="GuardarDocumentoChat()" name="documentochat" required />',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Importar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve) {
                if ($("#documentochat").val() == "") {
                    swal.insertQueueStep("Debe seleccionar un archivo");
                    resolve();
                    return
                } else {
                    $('#message_chat').val('<a href="' + url_cliente + 'Adjunto/ArchivoChat/' + ArchivoChat + '" target="_blank" style="color:black">' + ArchivoChat + "</a>");
                    resolve();
                }
            })
        }
    }]);
}

function GuardarDocumentoChat() {
    var documento = document.getElementById("documentochat");
    if ($.trim(documento.value) != "") {

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url_servidor + "Cotizacion/AdjuntarChat";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
                } else {
                    ArchivoChat = datos[1];
                }
            }
        });
    }
}

function LlenarEstIngresos() {
    var datos = LlamarAjax("Cotizacion", "opcion=EstadosIngresos");
    var data = JSON.parse(datos);
    var respuesta = "";
    var totales = 0;
    var porcentaje = 0;

    for (var x = 0; x < data.length; x++) {
        totales += data[x].cantidad * 1;
    }

    for (var x = 0; x < data.length; x++) {
        porcentaje = data[x].cantidad * 100 / totales;
        respuesta += "<tr>" +
            "<td>" + data[x].descripcion + "</td>" +
            "<td class='text-center'>" + formato_numero(data[x].cantidad, 0,_CD,_CM,"") + "</td>" +
            "<td class='text-center'>" + formato_numero(porcentaje, _DE,_CD,_CM,"") + "</td></tr>";

    }

    $("#BodyEstIngreso").html(respuesta);

}

function LlenarVueltasPendientes() {
    var datos = LlamarAjax("Configuracion","opcion=TablasVueltaMenu");
    var data = JSON.parse(datos);
    var respuesta = "";
    var totales = 0;
    var porcentaje = 0;

    for (var x = 0; x < data.length; x++) {
        totales += data[x].cantidad * 1;
    }

    for (var x = 0; x < data.length; x++) {
        porcentaje = data[x].cantidad * 100 / totales;
        respuesta += "<tr>" +
            "<td><b>" + data[x].cliente + "</b><br>" + data[x].contacto + "</td>" +
            "<td><b>" + data[x].direccion + "</b><br>" + data[x].actividad + "</td>" +
            "<td>" + data[x].usuario + "</td>" +
            "<td>" + data[x].fechapro + "</td>" +
            "<td>" + data[x].tiempo + "</td></tr>";
    }

    $("#BodyVueltas1").html(respuesta);
    $("#BodyVueltas2").html(respuesta);

}


function LlenarChat() {
    var datos = LlamarAjax("Configuracion","opcion=ObtenerChat");
    var data = JSON.parse(datos);
    var respuesta = "";
    var resumido = "";
    totalchat = 0;

    var fotochat = url_cliente + "Adjunto/imagenes/300.png";
    var foto = $.trim(localStorage.getItem('foto'));
    if (foto != "") {
        fotochat = url_cliente + "Adjunto/imagenes/FotoEmpleados/" + foto;
    }

    for (var x = 0; x < data.length; x++) {
						
        if ($.trim(data[x].codusu) == $.trim(usuariochat)) {
            respuesta = '<div class="message reversed">' +
                '<a class="message-img" href=\'javascript:MaximizarFotoChat("' + fotochat + '")\'><img src="' + fotochat + '" alt=""></a>' +
                "<div class='message-body' " + (data[x].mensaje.indexOf("http") > -1 ? "" : "onclick =\"EscucharMensaje('" + data[x].mensaje + "')\"") + ">" + data[x].mensaje +
                '<span class="attribution">' + data[x].fecha + '</span></div></div>' + respuesta;

            resumido += '<li title="' + (data[x].mensaje.indexOf("http") > -1 ? "" : data[x].mensaje) + '">' +
                '<a href = "javascript:LlamarChat()">' +
                '<img src="' + url_cliente + 'Adjunto/imagenes/300.png" alt="" class="user-face">' +
                '<strong>' + data[x].codusu + '</strong>' +
                '<strong>' + data[x].fecha + '</strong>' +
                '<span>' + (data[x].mensaje.indexOf("http") > -1 ? data[x].mensaje.substr(0, 30) : data[x].mensaje) + '...</span></a></li>';
            totalchat += 1;


        } else {
            if ((data[x].idusuario == "0") || (data[x].idusuario.indexOf("/" + idusuariochat + "/")*1 > -1)) {

                respuesta = '<div class="message">' +
                    '<a class="message-img" href = "#" > <img src="' + url_cliente + 'Adjunto/imagenes/300.png" alt=""></a>' +
                    "<div class='message-body' " + (data[x].mensaje.indexOf("http") > -1 ? "" : "onclick=\"EscucharMensaje('" + data[x].mensaje + "')\"") + (data[x].idusuario != "0" ? " style='background-color:cornflowerblue'" : "") + ">" + data[x].mensaje +
                    '<span class="attribution"><b>' + data[x].codusu + '</b> ' + data[x].fecha + '</span></div></div>' + respuesta;


                resumido += '<li title="' + (data[x].mensaje.indexOf("http") > -1 ? "" : data[x].mensaje) + '">' +
                    '<a href = "javascript:LlamarChat()">' +
                    '<img src="' + url_cliente + 'Adjunto/imagenes/300.png" alt="" class="user-face">' +
                    '<strong>' + data[x].codusu + '</strong>' +
                    '<strong>' + data[x].fecha + '</strong>' +
                    '<span>' + (data[x].mensaje.indexOf("http") > -1 ? data[x].mensaje.substr(0, 30) : data[x].mensaje) + '...</span></a></li>';
                totalchat += 1;

            }
        }

    }
    $("#cantidadchat").html(totalchat);
    $("#respuestachat").html(respuesta);
    $("#chatresumido").html(resumido);
    $("#respuestachat").animate({ scrollTop: $('#respuestachat')[0].scrollHeight }, 1000);

}

function MensajeLeido(){
	
}


function LlamarChat() {
    $("#Menu_Contenedor").html("");
    $("#menuchat").removeClass("hidden");
    $("#MenBienvenido").addClass("hidden");
    localStorage.setItem("vermensaje", "NO");
    if ($("#Usuarios_Chat").select2())
		$("#Usuarios_Chat").select2('destroy');
    $("#respuestachat").animate({ scrollTop: $('#respuestachat')[0].scrollHeight }, 1000);
    MensajeLeido();
}

/*var notireportados = $.connection.notireporte;
notireportados.client.addNewReporte = function (ingreso) {
    NotificacionesReporte(ingreso);
    //ConsularReportesIngresos();
};*/

/*var noticotizacion = $.connection.cotizacion;
noticotizacion.client.addNewAprobado = function (ingreso, estado) {
    if ($.trim(ingreso) != "") {
        NotificacionesCotiApro();
        var rol = $.trim($("#menu_rol").val());
        if (rol == 'ADMINISTRADOR' || rol == 'ESPECIALISTA') {
            swal("", "Las cotizaciones de los ingresos número " + ingreso + " han sido " + estado + " por el cliente", "info");
            EscucharMensaje("Las cotizaciones de los ingresos número " + ingreso + " han sido " + estado + " por el cliente");
        }
    }
};*/


function TablaGestionCartera() {

    if (!document.getElementById("TBGestionCartera"))
        return false;

    var fecha = $("#Fecha").val();
    var cliente = $("#Cliente").val() * 1;
    var asesor = $("#Asesor").val() * 1;
    var tipo = $("#TipoCliente").val() * 1;
    var dias = $("#Dias").val() * 1;
    var estado = $("#Estado").val() * 1;
    var orden = $("#Orden").val() * 1;
    var tipoorden = $("#TipoOrden").val() * 1;
    var gestion = $("#Gestion").val()*1;

    var facturado =0;
    var abonado = 0;
    var saldo = 0;
    var vencido = 0;

    var resultado = "";

    if (fecha == "") {
        $("#Fecha").focus();
        swal("Acción Cancelada", "Debe seleccionar una fecha", "warning");
        return false;
    }
    var parametros = "Cliente=" + cliente + "&Tipo=" + tipo + "&Dias=" + dias + "&Estado=" + estado + "&Asesor=" + asesor + "&Fecha=" +
        fecha + "&Orden=" + orden + "&TipoOrden=" + tipoorden + "&Gestionado=" + gestion;
        ;
    var datos = LlamarAjax("Caja","opcion=TablaGestionCartera&" + parametros);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        for (var x = 0; x < data.length; x++) {
            resultado += "<tr id='fila-" + x + "' " + (data[x].seguimiento ? "class='bg-celeste' " : "") + " ondblclick=\"GestionarCartera(" + data[x].idcliente + ",'" + data[x].cliente + "'," + x + ")\">" +
                "<td>" + (x + 1) + "</td>" +
                "<td><b>" + data[x].tipocliente + "</b><br>" + data[x].cliente + "<br>(<b>" + data[x].asesor + "</b>)</td>" +
                "<td><div id='correocliente" + x + "'>" + data[x].telefono + "<br><a href=\"javascript:EditarCorreoGC('" + data[x].email + "'," + x + ",'" + data[x].cliente + "'," + data[x].idcliente + ")\">" + ($.trim(data[x].email) == "" ? "SIN CORREO" : data[x].email) + "</a><br>" + data[x].ciudad + "</div></td>" +
                "<td class='text-XX' align='center'><a href=\"javascript:ModalEquipos(" + data[x].idcliente + ",'" + data[x].cliente + "')\">" + data[x].equipos + "</a></td>" +
                "<td align='right'>" + formato_numero(data[x].subtotal, 0,_CD,_CM,"") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].descuento, 0,_CD,_CM,"") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].iva, 0,_CD,_CM,"") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].total, 0,_CD,_CM,"") + "</td>" +
                "<td align='right'><a href='javascript:DetalleAbono(" + data[x].idcliente + "," + data[x].abonado + ")'>" + formato_numero(data[x].abonado, 0,_CD,_CM,"") + "</a></td>" +
                "<td align='right'><a href=\"javascript:GesEstadoCuenta(" + data[x].idcliente + ",'" + data[x].cliente + "','" + data[x].tipocliente + "'," + data[x].plazopago + "," + x + ")\">" + formato_numero(data[x].saldo, 0,_CD,_CM,"") + "</a></td>" +
                "<td class='text-XX text-danger' align='right'>" + formato_numero(data[x].vencido, 0,_CD,_CM,"") +
                "<input type='hidden' value='" + data[x].email + "' id='CorrreoActual" + x + "'></td>" +
                "<td onclick=\"DetalleGestion(" + data[x].idcliente + ",'" + data[x].cliente + "')\">" + (data[x].seguimiento ? data[x].seguimiento : "") + "</td></tr>";

            facturado += data[x].total;
            abonado += data[x].abonado;
            saldo += data[x].saldo;
            vencido += data[x].vencido;


        }
    }
    $("#TBGestionCartera").html(resultado);
    $("#TFacturado").val(formato_numero(facturado, 0,_CD,_CM,""));
    $("#TAbonado").val(formato_numero(abonado, 0,_CD,_CM,""));
    $("#TSaldo").val(formato_numero(saldo, 0,_CD,_CM,""));
    $("#TVencido").val(formato_numero(vencido, 0,_CD,_CM,""));
}

function NotificacionesReporte(ingreso, mensaje) {
    var datos = LlamarAjax("Laboratorio", "opcion=NotifiReportes");
    var data = JSON.parse(datos);
    var resultado = "";
    var cantidad = 0;
    for (var x = 0; x < data.length; x++) {
        cantidad += 1;
        resultado += '<li>' +
            '<div>' +
            '<span>' + data[x].nroreporte + '</span>' +
            '<b>Plantilla: </b>' + data[x].plantilla +
            '<br><a href="#">' + data[x].asesor + ' ' + data[x].fecha + '</a><br><b>' + data[x].ingreso + '</b><br>' + data[x].equipo +
            '<br><b>' + data[x].concepto + '</b>' +
            (data[x].ajuste ? '<br><b>Ajuste: </b>' + data[x].ajuste : "") +
            (data[x].suministro ? '<br><b>Suministro: </b>' + data[x].suministro : "") +
            (data[x].observacion ? '<br><b>Observacion: </b>' + data[x].observacion : "") +
            '<span>' + data[x].tiempo + '</span></div></li>';
    }
    $("#DivNotifiReportes").html(resultado);
    $("#cantreporteingreso").html(cantidad);
    
    
    if (ingreso) {
        var alerta = localStorage.getItem('alerta_mensajes');
        if ((alerta.indexOf("/Comercial/"))*1 > -1) {
            swal("", mensaje, "info");
            EscucharMensaje(mensaje.replace(/<br>/g, '').replace(/<b>/g, '').replace(/<\/b>/g, ''));
        }
    }
}

function NotificacionesCotiApro(ingreso) {
    var datos = LlamarAjax("Laboratorio", "opcion=NotifiReportesApro");
    var data = JSON.parse(datos);
    var resultado = "";
    var cantidad = 0;
    for (var x = 0; x < data.length; x++) {
        cantidad += 1;
        resultado += '<li>' +
            '<div>' +
            '<span>' + data[x].nroreporte + '</span>' +
            '<b>Plantilla: </b>' + data[x].plantilla +
            '<br><a href="#">' + data[x].asesor + ' ' + data[x].fecha + '</a><br><b>' + data[x].ingreso + '</b><br>' + data[x].equipo +
            '<br><b>' + data[x].tipoaprobacion + '</b>' +
            (data[x].ajuste ? '<br><b>Ajuste: </b>' + data[x].ajuste : "") +
            (data[x].suministro ? '<br><b>suministro: </b>' + data[x].suministro : "") +
            (data[x].observacion ? '<br><b>Observacion: </b>' + data[x].observacion : "") +
            (data[x].observacion ? '<br><b>Obser.Aprob: </b>' + data[x].observacionapro : "") +
            '<span>' + data[x].tiempo + '</span></div></li>';
    }
    $("#DivNotifiCotApro").html(resultado);
    $("#cantreporteaproba").html(cantidad);

    if (ingreso) {
        var rol = $.trim($("#menu_rol").val());
        if (rol == 'ADMINISTRADOR' || rol == 'COMERCIAL') {
            swal("", "El ingreso número " + ingreso + " ha sido reportado en el laboratorio", "info");
            EscucharMensaje("El ingreso número " + ingreso + " ha sido reportado en el laboratorio");
        }
    }
}


/*var Notificacion = $.connection.notificacion;
Notificacion.client.addNewMessageToPage = function (ingreso) {
    TablaTemporalIngreso();
};*/ //sinalert

function TablaTemporalIngreso() {

    if (!document.getElementById("bodyIngreso"))
        return false;

    var datos = LlamarAjax("Cotizacion","opcion=TemporalIngreso&cliente=" + IdCliente + "&remision=" + IdRemision);
    var resultado = "";
    totales = 0;
    var estado = "";
    if (datos != "[]") {
        eval("data=" + datos);
        for (var x = 0; x < data.length; x++) {
            switch (data[x].estado) {
                case "Temporal":
                    estado = "<label class='bg-warning'>" + data[x].estado + "</label>";
                    break;
                case "Ingresado":
                    estado = "<label class='bg-success'>" + data[x].estado + "</label>";
                    break;
                case "Cerrado":
                    estado = "<label class='bg-primary'>" + data[x].estado + "</label>";
                    break;
                default:
                    estado = "<label class='bg-default'>" + data[x].estado + "</label>";
                    break;
            }
            resultado += "<tr ondblclick=\"SelecccionarFila(" + data[x].ingreso + ")\">" +
                "<td>" + (x + 1) + "</td>" +
                "<td><b>" + data[x].ingreso + "</b><br>" + data[x].fechaing + "<br>" + data[x].usuario + "</td>" +
                "<td><b>" + data[x].magnitud + "</b><br>" + data[x].equipo + "<br>" + data[x].intervalo + "</td>" +
                "<td><b>" + data[x].marca + "</b><br>" + data[x].modelo + "</td>" +
                "<td>" + estado + "</label></td> " +
                "<td align='center' class='text-XX'>" + data[x].cantidad + "</td>" +
                "<td>" + data[x].serie + "</td>" +
                "<td>" + (data[x].cotizacion ? data[x].cotizacion : "0") + "</td>" +
                "<td>" + data[x].garantia + "</td>" +
                "<td>" + data[x].sitio + "</td>" +
                "<td>" + data[x].convenio + "</td>" +
                "<td>" + (data[x].accesorio ? data[x].accesorio : "") + "</td>" +
                "<td>" + data[x].observacion + "<input type='hidden' name='IdRemision[]' value='" + data[x].idremision + "'></td>" +
                "<td align='center'><button class='eliminar btn btn-danger' title='Eliminar Ingreso' type='button' onclick=\"EliminarIngreso(" + data[x].id + ",'" + data[x].equipo + " " + data[x].marca + " " + data[x].modelo + "'," + data[x].ingreso + ",'" + data[x].estado + "')\"><span data-icon='&#xe0d8;'></span></button></td>" +
                "<td align='center'><button class='verfotos btn btn-success' title='Ver Fotos' type='button' onclick='InicializarCamara(" + data[x].ingreso + "," + data[x].fotos + ")'><span data-icon='&#xe2c7;'></span></button></td></tr>"
            totales += data[x].cantidad;

        }
    }

    $("#bodyIngreso").html(resultado);
    $("#Totales").val(totales);
}

$.fn.delayPasteKeyUp = function (fn, ms) {
    var timer = 0;
    $(this).on("keyup paste", function () {
        clearTimeout(timer);
        timer = setTimeout(fn, ms);
    });
};

$.fn.delayPasteKeyUp = function (fn, ms) {
    var timer = 0;
    $(this).on("keyup paste", function () {
        clearTimeout(timer);
        timer = setTimeout(fn, ms);
    });
};

function EscucharMensaje(mensaje) {
    if ('speechSynthesis' in window) {
        var speech = new SpeechSynthesisUtterance(mensaje);
        window.speechSynthesis.speak(speech);
    }
}

function MenuPrincipal() {
    LlamarAjax("InicioSesion/QuitarMenu", "");
    window.location = url_cliente;
}

function MenuCerrarSesion() {
    LlamarAjax("InicioSesion", "&opcion=CerrarSession");
    window.location.href = url_cliente;
    cleanUp();
}

function ActivarDataTable(id, permiso) {
    var idioma = $("#menu_idioma").val() * 1;
    var lenguaje = url_cliente;
    var table;
    switch (idioma) {
        case 1:
            lenguaje += "json/Spanish.json"
            break;
    }
    if (permiso == 1) {
        table = $('#' + id).DataTable({
            "language": {
                "url": lenguaje
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ],
            "lengthMenu": [[10, 50, 100, -1], [10, 50, 10, "Todos"]],
            "paging": true
        });
    } else {
        if (permiso == 5) {
            table = $('#' + id).DataTable({
                "language": {
                    "url": lenguaje
                },
                "paging": true,
                "searching": true,
                "bLengthChange": false
            });
        } else {
            if (permiso == 6) {
                table = $('#' + id).DataTable({
                    "language": {
                        "url": lenguaje
                    },
                    dom: 'Bfrtip',
                    buttons: [
                        'excel', 'csv', 'copy','colvis'
                    ],
                    "paging": false,
                    "scrollY": "300px",
                    "scrollCollapse": true
                });
            } else {
                table = $('#' + id).DataTable({
                    "language": {
                        "url": lenguaje
                    },
                    "paging": true,
                    "searching": false,
                    "bLengthChange": false
                });
            }
        }

    }

    return table;
}

function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel, nocolum) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line


    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            if (index != "Eli" && index != "Edi") {
                row += index + ',';
            }
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            //alert(index);
            if (index != "Eli" && index != "Edi") {
                if (arrData[i][index])
                    row += '"' + arrData[i][index] + '",';
                else
                    row += ',';
            }
        }

        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {
        alert("Invalid data");
        return;
    }

    //Generate a file name
    var fileName = "";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g, "_");

    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

function PermisosGenerales() {
    datos = $.trim(LlamarAjax("InicioSesion", "opcion=PermisosDelSistema"));
    
    if (datos == "Cerrado"){
		window.location.href =url_cliente;
		wewewe232323=wewewewewe;
	}else{
		$("#permisosgenelaes").html(datos);
	}
}

function PermisosMenu() {
    datos = LlamarAjax("Seguridad","opcion=PermisosMenu");
    var data = JSON.parse(datos);
    for (var i in data) {
        if (data[i].permiso == "0") {
            var arreglo = data[i].control.split(",");
            for (var x = 0; x < arreglo.length; x++) {
                $("#" + $.trim(arreglo[x])).remove();
            }
        }
    }
}

function CambiarClave() {
    var clave = $("#cambio_Clave").val();
    var claven = $("#cambio_ClaveN").val();
    var clavec = $("#cambio_ClaveC").val();

    var mensaje = "";
    if (clavec == "") {
        $("#cambio_ClaveC").focus();
        mensaje = "<br> Debe de confirmar la clave"
    }

    if (claven == "") {
        $("#cambio_ClaveN").focus();
        mensaje = "<br> Debe de ingresar la nueva clave " + mensaje;
    }

    if (clave == "") {
        $("#cambio_Clave").focus();
        mensaje = "<br> Debe de ingresar la clave actual " + mensaje;
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var parametros = "ClaveC=" + clavec + "&ClaveN=" + claven + "&Clave=" + clave;
    var datos = LlamarAjax("InicioSesion" , "opcion=CambiarClave&" + parametros).split("|");
    if (datos[0] == "0") {
        $("#modalCambioClave").modal("hide");
        swal("", datos[1], "success");

    } else
        swal("Acción Cancelada", datos[1], "warning");

}



function LlamarOpcionMenu(opcion,tipo, nombre, empleado) {
	
	var vista;
    $('body').removeClass('nav-sm');
    $('body').addClass('nav-md');
    $("#MenBienvenido").addClass("hidden");
    localStorage.setItem('ModuloCotizacion', 0);

    if (empleado) {
        if (empleado == 0)
            localStorage.setItem('EmpleadoSeleccionado', 0);
    } else
        localStorage.setItem('EmpleadoSeleccionado', 0);

    if (tipo != 3)
        localStorage.setItem("CerIngreso", 0);

    if (tipo == 3)
        tipo = 2;
    var sesion = $.trim(MantenSesion2());

    

    if (sesion == "cerrada") {
		$.jGrowl("Sesion Cerrada", { life: 2500, theme: 'growl-warning', header: '' });
        localStorage.setItem("bloqueopantalla", 0);
        BloquearPantalla();
        return false;
    }


    ActivarLoad();
    setTimeout(function () {


        vista = url_cliente + "vistas/" + opcion + ".html";
        //var datos = LlamarAjax(vista, "");
        //$("#Menu_Contenedor").html(datos);
        $("#Menu_Contenedor").html("");
        $("#menuchat").addClass("hidden");
        localStorage.setItem("vermensaje", "SI");
        var unico = Math.round(Math.random() * 10000);
        $("#Menu_Contenedor").load(vista + "?uniqueID=" + unico);
        if (!(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
            if (tipo == 2) {
                $("body").addClass("sidebar-narrow");
                $("#" + nombre).css("display", "");
            }
            else
                $("body").removeClass("sidebar-narrow");
        }

    }, 15);
}

function LlamarOpcionSubMenu(opcion) {

    var img1 = $("#imagen1");
    var img2 = $("#imagen2");
    var img3 = $("#imagen3");
    if (img1) {
        $.removeData(img1, 'elevateZoom');//remove zoom instance from image
        $.removeData(img2, 'elevateZoom');//remove zoom instance from image
        $.removeData(img3, 'elevateZoom');//remove zoom instance from image

        $('.zoomContainer').remove();// remove zoom container from DOM

        $(".ligthbox").remove();
    }
    
    window.location = url_cliente + "vistas/Menu/" + opcion + ".html";

}

function validarserial(serial) {
    var valor = '';
    var esp = 0
    var CharValidoSer = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (var i = 0; i < serial.length; i++) {

        if (CharValidoSer.indexOf(serial.charAt(i)) >= 0) {

            if (esp == 0)
                valor += serial.charAt(i);
            else
                valor += serial.charAt(i).toLowerCase();
        }
        esp = 0;
        if (CharValidoSer.indexOf(serial.charAt(i)) < 0) {
            esp = 1;
        }
    }

    if (valor.length < 12) {
        var cant = 12 - valor.length;
        var ceros = ""
        for (x = 1; x <= cant; x++) {
            valor = "0" + valor;
        }

    }
    return valor;

}

function validarserialsincero(serial) {
    var valor = '';
    var esp = 0
    var CharValidoSer = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (var i = 0; i < serial.length; i++) {

        if (CharValidoSer.indexOf(serial.charAt(i)) >= 0) {

            if (esp == 0)
                valor += serial.charAt(i);
            else
                valor += serial.charAt(i).toLowerCase();
        }
        esp = 0;
        if (CharValidoSer.indexOf(serial.charAt(i)) < 0) {
            esp = 1;
        }
    }


    return valor;

}

function FormatoSalida(texto, decimal, cero) {
    if (decimal == undefined)
        decimal = 0;
    if ($.trim(texto.value) != "") {
        texto.value = NumeroDecimal(texto.value);
    }
    if (cero) {
        if (cero == 2) {
            texto.value = Math.abs(texto.value) * -1;
        }
        texto.value = formato_numero(texto.value, decimal, ".", ",");

    } else {
        texto.value = $.trim(texto.value) == "" ? "0" : formato_numero(texto.value, decimal, ".", ",");
    }

}

function BuscarArreglo(arreglo, dato) {
    if (!(arreglo))
        return -1;
    var result = -1;
    for (i = 0; i < arreglo.length; i++) {
        if (dato == arreglo[i]) {
            return i;
        }
    }
    return result;
}

function ActivarFila(f, control, id) {

    if ($("#gfila" + f).css("display") == "none") {

        $("#" + id + " > tbody > #ntr" + f + " > td > input ").removeClass("hidden");
        $("#" + id + " > tbody > #ntr" + f + " > td > span ").addClass("hidden");

        $("#gfila" + f).css("display", "block");
        $("#afila" + f).css("display", "none");
    } else {

        var obj_inp = $("#" + id + " > tbody > #ntr" + f + " > td > input ");
        var obj_spa = $("#" + id + " > tbody > #ntr" + f + " > td > span ");
        var obj_lab = $("#" + id + " > tbody > #ntr" + f + " > td > label ");

        var parametros = "";
        var requerido = "";
        for (var x = 0; x < obj_inp.length; x++) {
            obj_spa[x].innerHTML = obj_inp[x].value;
            requerido = obj_lab[x].innerHTML;

            if ((requerido == "rec") && ($.trim(obj_inp[x].value) == "")) {
                obj_inp[x].focus();
                swal("Error", "Debe de ingresar el valor del campo " + obj_inp[x].name, "error");
                return false;
            }
            if (parametros == "")
                parametros = obj_inp[x].name + "=" + obj_inp[x].value;
            else
                parametros += "&" + obj_inp[x].name + "=" + obj_inp[x].value;
        }

        var datos = LlamarAjax(control, parametros);
        datos = datos.split("|");

        if (datos[0] != "0")
            swal(datos[1]);

        $("#" + id + " > tbody > #ntr" + f + " > td > input ").addClass("hidden");
        $("#" + id + " > tbody > #ntr" + f + " > td > span ").removeClass("hidden");

        $("#gfila" + f).css("display", "none");
        $("#afila" + f).css("display", "block");



    }

}

/*$.fn.dataTableExt.afnSortData['dom-text'] = function (oSettings, iColumn) {
        var aData = [];
        oSettings.oApi._fnGetTrNodes(oSettings).forEach(function (nRow) {
        var oElem = $('td:eq(' + iColumn + ') input', nRow);
        aData.push(oElem.val());
     });
    return aData;
};*/

function CargarTablaEditableReiniciable2(datos, Lectura, Requeridos, id, des, Control, Numero, Decimales, permiso, encabezado, ancho, eliminar, tipoorden, botoneliminar, editar, valoreditar) {

    var f = 0;
    var lec = "";
    var req = "";
    var columna = "";
    var valor = "";
    var medida = "";
    var btneliminar = "";
    var btneditar = "";
    var decimal = 0;
    var ordenar = "[";
    var resultado = '<table class="table table-bordered" id="' + id + '"><thead><tr>';
    medida = "";
    for (var x = 0; x < encabezado.length; x++) {
        medida = "";
        if ((ancho[x] * 1) > 0)
            medida = "width = '" + ancho[x] + "%'";
        resultado += "<th " + medida + ">" + encabezado[x] + "</th>";
    }
    resultado += "<th width='3%'>&nbsp;</th><th width='3%'>&nbsp;</th></thead><tbody>";
    var data = JSON.parse(datos);
    for (var i in data) {
        f++;
        resultado += "<tr id='ntr" + f + "' ondblclick ='ActivarFila(" + f + ")'>";
        btneliminar = "";
        btneditar = "";
        for (var j in data[i]) {

            lec = "";
            req = "";
            valor = "";
            columna = $.trim(j);
            decimal = 0;

            if (BuscarArreglo(Lectura, columna) >= 0)
                lec = "readonly";

            if (BuscarArreglo(Requeridos, columna) >= 0)
                rec = "rec";

            if (data[i][j]) {
                valor = $.trim(data[i][j]);
                if (BuscarArreglo(Decimales, columna) >= 0)
                    decimal = 2;
                if (BuscarArreglo(Numero, columna) >= 0) {
                    valor = formato_numero(valor, decimal, ".", ",");
                }


                for (x = 0; x < valoreditar.length; x++) {
                    if (j == valoreditar[x]) {
                        if (btneditar == "")
                            btneditar = "'" + valor + "'";
                        else
                            btneditar += ",'" + valor + "'";
                    }
                }
            }

            resultado += "<td><label class='hidden'>" + rec + "</label><input " + lec + " class='no-borderl hidden' value='" + valor + "' name='" + columna + "' id='" + columna + f + "'/><span>" + valor + "</span></td>";
        }
        if (permiso > 0) {
            resultado += "<td><button title='Editar " + des + "' type='button' id='afila" + f + "' class='btn-primary' onclick=\"" + (editar == "" ? "ActivarFila(" + f + ",'" + Control + "','" + id + "')\"" : editar + "(" + btneditar + ")\"") + "><i class='glyphicon glyphicon-edit'></i></button >";
            resultado += "<button type='button' style='display:none' id='gfila" + f + "' class='btn-primary' onclick=\"ActivarFila(" + f + ",'" + Control + "','" + id + "')\"><i class='glyphicon glyphicon-ok'></i></button></td>";
            if (botoneliminar == 1)
                resultado += "<td><button title='Eliminar " + des + "' type='button' id='afila" + f + "' class='btn-danger' onclick=\"EliminarFila(" + btneliminar + ")\"><i class='glyphicon glyphicon-remove'></i></button>";
            else
                resultado += "<td>&nbsp;</td>";
        } else {
            resultado += "<td>&nbsp;</td>";
            resultado += "<td>&nbsp;</td>";
        }
        resultado += "</tr>";

    }
    resultado += "</tbody></table>"


    $("#div" + id).html(resultado);


    $("#" + id).dataTable({
        "language": {
            "url": LenguajeDataTable
        },

        "paging": false,
        "ordering": false,
        scrollY: 500,
        scroller: true
    });

    return f;
}

function CargarTablaEditableReiniciable(datos, Lectura, Requeridos, id, des, Control, Numero, Decimales, permiso, encabezado, ancho, eliminar, tipoorden, botoneliminar) {

    var f = 0;
    var lec = "";
    var req = "";
    var columna = "";
    var valor = "";
    var medida = "";
    var btneliminar = "";
    var decimal = 0;
    var ordenar = "[";
    var resultado = '<table class="table table- bordered table-responsive" id="' + id + '"><thead><tr>';
    medida = "";
    for (var x = 0; x < encabezado.length; x++) {
        medida = "";
        if (ancho[x] * 1 > 0)
            medida = "width = '" + ancho[x] + "%'";
        resultado += "<th " + medida + ">" + encabezado[x] + "</th>";
    }
    resultado += "<th width='3%'>&nbsp;</th><th width='3%'>&nbsp;</th></thead><tbody>";
    var data = JSON.parse(datos);
    for (var i in data) {
        f++;
        resultado += "<tr id='ntr" + f + "' ondblclick ='ActivarFila(" + f + ")'>";
        btneliminar = "";
        for (var j in data[i]) {

            lec = "";
            req = "";
            valor = "";
            columna = $.trim(j);
            decimal = 0;

            if (BuscarArreglo(Lectura, columna) >= 0)
                lec = "readonly";

            if (BuscarArreglo(Requeridos, columna) >= 0)
                rec = "rec";

            if (data[i][j]) {
                valor = $.trim(data[i][j]);
                if (BuscarArreglo(Decimales, columna) >= 0)
                    decimal = 2;
                if (BuscarArreglo(Numero, columna) >= 0) {
                    valor = formato_numero(valor, decimal, ".", ",");
                }


                for (x = 0; x < eliminar.length; x++) {
                    if (j == eliminar[x]) {
                        if (btneliminar == "")
                            btneliminar = "'" + valor + "'";
                        else
                            btneliminar += ",'" + valor + "'";
                    }
                }
            }

            resultado += "<td><label class='hidden'>" + rec + "</label><input " + lec + " class='no-borderl hidden' value='" + valor + "' name='" + columna + "' id='" + columna + f + "'/><span>" + valor + "</span></td>";
        }
        if (permiso > 0) {
            resultado += "<td><button title='Editar " + des + "' type='button' id='afila" + f + "' class='btn-primary' onclick=\"ActivarFila(" + f + ",'" + Control + "','" + id + "')\"><i class='glyphicon glyphicon-edit'></i></button>";
            resultado += "<button type='button' style='display:none' id='gfila" + f + "' class='btn-primary' onclick=\"ActivarFila(" + f + ",'" + Control + "','" + id + "')\"><i class='glyphicon glyphicon-ok'></i></button></td>";
            if (botoneliminar == 1)
                resultado += "<td><button title='Eliminar " + des + "' type='button' id='afila" + f + "' class='btn-danger' onclick=\"EliminarFila(" + btneliminar + ")\"><i class='glyphicon glyphicon-remove'></i></button>";
            else
                resultado += "<td>&nbsp;</td>";
        } else {
            resultado += "<td>&nbsp;</td>";
            resultado += "<td>&nbsp;</td>";
        }
        resultado += "</tr>";

    }
    resultado += "</tbody></table>"


    $("#divtable").html(resultado);

    var lenguaje = "json/Spanish.json";
    if ($("#menu_idioma").val() == "3")
        lenguaje = "json/Protuguese-Brasil.json"

    $("#" + id).dataTable({
        "language": {
            "url": url_cliente + lenguaje
        },

        "paging": false,
        "ordering": false,
        scrollY: 500,
        scroller: true
    });

    return f;
}

function CargarTablaEditable(datos, Lectura, Requeridos, id, des, Control, Numero, Decimales, permiso) {

    eval("var data =" + datos);
    var f = 0;
    $('#' + id + ' tr').not(function () { return !!$(this).has('th').length; }).remove();

    var lec = "";
    var req = "";
    var columna = "";
    var valor = "";
    var decimal = 0;
    var resultado = "";
    for (var i in data) {
        f++;
        resultado = "<tr id='ntr" + f + "' ondblclick ='ActivarFila(" + f + ")'>";

        for (var j in data[i]) {

            lec = "";
            req = "";
            valor = "";
            columna = $.trim(j);
            decimal = 0;

            if (BuscarArreglo(Lectura, columna) >= 0)
                lec = "readonly";

            if (BuscarArreglo(Requeridos, columna) >= 0)
                rec = "rec";

            if (data[i][j]) {
                valor = $.trim(data[i][j]);
                if (BuscarArreglo(Decimales, columna) >= 0)
                    decimal = 2;
                if (BuscarArreglo(Numero, columna) >= 0)
                    valor = formato_numero(valor, decimal, ".", ",");
            }

            resultado += "<td><label class='hidden'>" + rec + "</label><input " + lec + " class='no-borderl hidden' value='" + valor + "' name='" + columna + "' id='" + columna + f + "'/><span>" + valor + "</span></td>";
        }
        if (permiso > 0) {
            resultado += "<td><button title='Editar " + des + "' type='button' id='afila" + f + "' class='btn btn-primary' onclick=\"ActivarFila(" + f + ",'" + Control + "','" + id + "')\"><i class='glyphicon glyphicon-edit'></i></button>";
            resultado += "<button type='button' style='display:none' id='gfila" + f + "' class='btn btn-primary' onclick=\"ActivarFila(" + f + ",'" + Control + "','" + id + "')\"><i class='glyphicon glyphicon-ok'></i></button></td>";
        } else
            resultado += "<td>&nbsp;</td>";
        resultado += "</tr>";

        $("#" + id + " > tbody").append(resultado);

    }


    table = $("#" + id).dataTable(
        {
            destroy: true,
            retrieve: true,
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ],
            "language": {
                "url": url_cliente + "json/Spanish.json"
            }
        });

    return f;

}

function CargarCombo(tipo, inicial, varurl, opcion, vjson) {
    if (!(opcion))
        opcion = 0
	if (!vjson){
		vjson = 0;
	}
    if (vjson == 0)
		return LlamarAjax("Configuracion","opcion=CargarCombos&tipo=" + tipo + "&inicial=" + inicial + "&opciones=" + opcion + "&vjson=" + vjson);
	else
		return JSON.parse(LlamarAjax("Configuracion","opcion=CargarCombos&tipo=" + tipo + "&inicial=" + inicial + "&opciones=" + opcion + "&vjson=" + vjson));
}

function CargarTablaJsonTitulo(datos, id, div) {
    var resultado = '<table class="table table-bordered" id="' + id + '"><thead><tr>';
    var Totales = 0;
    if (datos != "[]") {
        var data = JSON.parse(datos);
        var titulo = data;
        var valor = "";

        fila = 0;
        var encontrado;

        for (var i in titulo) {
            for (var j in data[i]) {
                resultado += '<th>' + j + '</th>';
            }
            break;
        }
        resultado += '</thead><tbody>';
        for (var i in data) {
            fila++;
            encontrado = 0;
            resultado += "<tr>";
            Totales += (data[i].Valor * 1);
            for (var j in data[i]) {

                if (data[i][j]) {
                    valor = $.trim(data[i][j]);
                    if (valor.indexOf("/Date") >= 0)
                        valor = FechaJSonCorta(valor);
                    resultado += "<td>" + valor + "</td>";
                    encontrado = 1;
                } else

                    resultado += "<td>&nbsp;</td>";
            }
            resultado += "</tr>";
        }

    }
    $("#chTotales").html(formato_numero(Totales, "0", ".", ",", ""));
    resultado += "</tbody></table><hr></font>";
    $("#" + div).html(resultado);
}

function CargarTablaJsonTituloRep(datos, id, div) {
    var resultado = '<table class="table table-bordered" id="' + id + '"><thead><tr>';
    var Totales = 0;
    if (datos != "[]") {
        var data = JSON.parse(datos);
        var titulo = data;
        var valor = "";

        fila = 0;
        var encontrado;

        for (var i in titulo) {
            for (var j in data[i]) {
                resultado += '<th>' + j + '</th>';
            }
            break;
        }
        resultado += '</thead><tbody>';
        for (var i in data) {
            fila++;
            encontrado = 0;
            resultado += "<tr>";
            Totales += (data[i].Valor * 1);
            for (var j in data[i]) {
                if (data[i][j])
                    resultado += "<td>" + data[i][j] + "</td>";
                else
                    resultado += "<td>&nbsp;</td>";
            }
            resultado += "</tr>";
        }

    }
    resultado += "</tbody></table><hr></font>";
    $("#" + div).html(resultado);
    ActivarDataTable(id, 1);
}

function CargarTablaJSon(datos, id) {

    var data = JSON.parse(datos);
    var fila = 0;
    var encontrado;
    for (var i in data) {
        fila++;
        encontrado = 0;
        resultado = "<tr><td>" + fila + "</td>";
        for (var j in data[i]) {
            if (data[i][j]) {
                resultado += "<td>" + data[i][j] + "</td>";
                encontrado = 1;
            } else
                resultado += "<td>&nbsp;</td>";

        }
        if (encontrado == 0)
            break;
        resultado += "</tr>";
        $("#" + id + " > tbody").append(resultado);
    }
}


function FechaJson(fecha) {
    var myDate = new Date(fecha.match(/\d+/)[0] * 1);
    return myDate;
}

function FechaJSonCorta(fecha) {

    if (fecha == null)
        return false;

    var dateString = fecha.substr(6);
    var currentTime = new Date(parseInt(dateString));
    var month = currentTime.getMonth() + 1;

    if ($.trim(month).length == 1)
        month = "0" + $.trim(month);
    var day = currentTime.getDate();
    if ($.trim(day).length == 1)
        day = "0" + $.trim(day);
    var year = currentTime.getFullYear();
    var date = day + "/" + month + "/" + year;
    return date;
}

function FormatoEntrada(texto, tipo) {
    if (tipo == "1")
        texto.value = CambiarCaracter(texto.value, ",", "");
    //alert(tipo);
    if (tipo != "3")
        texto.select();
}

function QuitarComa(texto) {

    var numero = '';
    for (i = 0; i < texto.length; i++) {
        if (texto.charAt(i) != ",")
            numero = numero + texto.charAt(i);
    }

    return numero;

}

function QuitarComaPunto(texto) {

    var numero = '';
    for (i = 0; i < texto.length; i++) {
        if ((texto.charAt(i) != ",") && (texto.charAt(i) != "."))
            numero = numero + texto.charAt(i);
    }

    return numero;

}

function NumCheck(tecla, field, decimal) {
    if ((decimal == 0) && (tecla.charCode == 46))
        return false;
    if ((tecla.charCode < 48 || tecla.charCode > 57) && (tecla.charCode != 46) && (tecla.charCode != 8)) {
        return false;
    } else {
        var len = field.value.length;
        var index = field.value.indexOf('.');

        if (index > 0 && tecla.charCode == 46) {
            return false;
        }

        if (index > 0) {
            var CharAfterdot = (len + 1) - index;
            if (CharAfterdot > (decimal + 1)) {
                return false;
            }
        }
    }

}


function ValidarTexto(texto, tipo) {


    if (tipo == "1" || tipo == "3") {
        var valorCaja = '';
        var caja = '';
        if (texto.value == "-")
            return false;
        for (var i = 0; i < texto.value.length; i++) {
            if (tipo == 1) {
                if ((texto.value.charAt(i) != ".") && (texto.value.charAt(i) != ",") && (texto.value.charAt(i) != "-"))
                    valorCaja = valorCaja + texto.value.charAt(i);
            } else {
                if ((texto.value.charAt(i) != ".") && (texto.value.charAt(i) != "-"))
                    valorCaja = valorCaja + texto.value.charAt(i);
            }
        }

        if (isNaN(valorCaja) && valorCaja != "-")
            texto.value = "";
        else {
            if (tipo == 1) {
                texto.value = valorCaja.replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
            }
        }
    } else {
        valorCaja = ""
        for (var i = 0; i < texto.value.length; i++) {
            if (((texto.value.charAt(i) >= "0") && (texto.value.charAt(i) <= "9"))) {
                texto.value = valorCaja
                i = texto.length;
            }
            else
                valorCaja = valorCaja + texto.value.charAt(i);
        }
    }
}

function CambiarComa(texto) {
    alert(texto);
    texto = CambiarCaracter($.trim(texto), ".", ",");
    alert(texto);
    return texto;
}


function NumeroDecimal(texto) {
    texto = $.trim(texto);
    if (texto != "") {
        texto = CambiarCaracter(texto, ",", "");

        var valorCaja = "";

        for (var i = 0; i < texto.length; i++) {

            if ((texto.charAt(i) == "0") || (texto.charAt(i) == "1") || (texto.charAt(i) == "2") || (texto.charAt(i) == "3") || (texto.charAt(i) == "4") || (texto.charAt(i) == "5") || (texto.charAt(i) == "6") || (texto.charAt(i) == "7") || (texto.charAt(i) == "8") || (texto.charAt(i) == "9") || (texto.charAt(i) == ".") || (texto.charAt(i) == "-") || (texto.charAt(i) == "e"))
                valorCaja = valorCaja + texto.charAt(i);
        }

        return (valorCaja*1);
    } else {
        return 0;
    }

}

function ObligarDecimal(codigo) {
    if ((codigo.keyCode) == 190)
        codigo.keyCode = 188;
    return;
}


function CambiarCaracter(texto, c, r) {

    var numero = '';
    if ($.trim(texto) != "") {
        for (var i = 0; i < texto.length; i++) {
            if (texto.charAt(i) != c)
                numero = numero + texto.charAt(i);
            else
                numero = numero + r;
        }
    }
    return numero;

}

function formato_fecha(fecha) {
    return fecha.getFullYear() + "-" + (fecha.getMonth() + 1) + "-" + fecha.getDate();
}


function formato_numero(numero, decimales, separador_decimal, separador_miles, SiglaMoneda, blanco) { // v2007-08-06

    numero = parseFloat(numero);
    if (!(SiglaMoneda))
        SiglaMoneda = "";

    if (isNaN(numero)) {
        return "";
    }

    if (decimales !== undefined) {
        // Redondeamos
        numero = numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero = numero.toString().replace(".", separador_decimal !== undefined ? separador_decimal : ",");
    var valores = numero.split(separador_decimal);
    var entera = valores[0];

    if (separador_miles) {
        // Añadimos los separadores de miles
        var miles = new RegExp("(-?[0-9]+)([0-9]{3})");
        while (miles.test(entera)) {
            entera = entera.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    if (SiglaMoneda == "%")
        return $.trim(entera + (valores[1] ? separador_decimal + valores[1] : "") + " " + SiglaMoneda);
    else {
        if (blanco) {
            if (blanco == 3) {
                if (valores[1] * 1 == 0 && entera * 1 == 0) {
                    if ($.trim(numero) != "")
                        return "0.000"
                }
            } else {
                if (blanco == 5) {
                    texto = "";
                    for (var i = valores[1].length; i > 1; i--) {

                        if (valores[1].charAt(i) != "0")
                            texto += valores[1].charAt(i);
                    }
                    return $.trim(SiglaMoneda + " " + entera + separador_decimal + texto);
                } else {
                    if (valores[1] * 1 == 0 && entera * 1 == 0)
                        return "";
                }

            }
        }
        return $.trim(SiglaMoneda + " " + entera + (valores[1] ? separador_decimal + valores[1] : ""));
    }

}

function calcularDigitoVerificacion(idNit, iddigito) {

    CliNit = $.trim($("#" + idNit).val());

    if (CliNit == "") {
        iddigito.value = "";
        $("#" + idNit).focus();
        $.jGrowl(ValidarTraduccion("Debe ingresar un NIT"), { life: 2500, theme: 'growl-warning', header: '' });
        return;
    }
    var _array,
        x,
        y,
        z,
        DigitoControl;
    CliNit = CliNit.replace(/\s/g, ""); // Espacios
    CliNit = CliNit.replace(/,/g, ""); // Comas
    CliNit = CliNit.replace(/\./g, ""); // Puntos
    CliNit = CliNit.replace(/-/g, ""); // Guiones
    _array = new Array(16);
    z = CliNit.length
    _array[1] = 3;
    _array[2] = 7;
    _array[3] = 13;
    _array[4] = 17;
    _array[5] = 19;
    _array[6] = 23;
    _array[7] = 29;
    _array[8] = 37;
    _array[9] = 41;
    _array[10] = 43;
    _array[11] = 47;
    _array[12] = 53;
    _array[13] = 59;
    _array[14] = 67;
    _array[15] = 71;
    x = 0;
    y = 0;
    for (var i = 0; i < z; i++) {
        y = (CliNit.substr(i, 1));
        x += (y * _array[z - i]);
    }
    y = x % 11;
    DigitoControl = (y > 1) ? 11 - y : y;
    if (iddigito.value != DigitoControl) {
        iddigito.value = "";
        $.jGrowl(ValidarTraduccion("Digito Control no Valido"), { life: 2500, theme: 'growl-warning', header: '' });
    }
    return;
}

function InvertirFecha(fecha,tipo) {

    fecha = fecha.split(tipo);
    fecha = fecha[2] + '-' + fecha[1] + '-' + fecha[0];
    return fecha;

}

function ValidarCorreo(correo) {

    correo = $.trim(correo).toLowerCase();

    if (/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/.test(correo))
        return 1;
    else
        return 2;
}

function ValidarFecha(fecha) {

    var fechaArr = fecha.split('-');

    var dia = fechaArr[0];
    var mes = fechaArr[1];
    var aho = fechaArr[2];

    var plantilla = new Date(aho, mes - 1, dia);//mes empieza de cero Enero = 0

    if (!plantilla || plantilla.getFullYear() == aho && plantilla.getMonth() == mes - 1 && plantilla.getDate() == dia) {
        return 1;
    } else {
        return 2;
    }
}

function Obtener_Fecha(fecha){
	var month = fecha.getMonth() + 1;
    var day = fecha.getDate();
	var resultado = fecha.getFullYear() + '-' +
            (month < 10 ? '0' : '') + month + '-' +
            (day < 10 ? '0' : '') + day;
	return resultado;

}



function LlamarAjaxJSonGet(url, parametro) {

    var resultado = "";

    $.ajax({
        url: url,
        type: "GET",
        async: false,
        data: parametro,
        dataType: "json",
        cache: false,
        success: function (datos) {
            //alert(datos);
            resultado = datos;
        }
    });

    return resultado;

}


function LlamarAjaxJSon(url, parametro) {

    resultado = "";

    $.ajax({
        url: url,
        type: "POST",
        async: false,
        data: parametro,
        dataType: "json",
        cache: false,
        success: function (datos) {
            //alert(datos);
            resultado = datos;
        }
    });

    return resultado;
}


function LlamarAjaxGET(url, parametro) {

    resultado = "";

    $.ajax({
        url: url,
        type: "GET",
        async: false,
        data: parametro,
        dataType: "html",
        cache: false,
        success: function (datos) {
            //alert(datos);
            resultado = $.trim(datos);
        }
    })
    return resultado;

}

function BloquearPantalla() {
    DesactivarLoad();
    if (localStorage.getItem("bloqueopantalla") * 1 == 1)
        return false;
    var codigo = localStorage.getItem("codusu");
    var nomusuario = localStorage.getItem("usuario");    
    localStorage.setItem("bloqueopantalla", 1);
    LlamarAjax("InicioSesion", "&opcion=CerrarSession");
    VariableConexion();
    IPLocal();
    localStorage.setItem("MinSesion", 0);
    localStorage.setItem("SegSesion", 0);
    PantallaBloqueada = 1;
    var mensaje = 'Por favor ingrese la clave del usuario <br><b>' + nomusuario + '</b>';
    swal({
        title: 'Bloqueado',
        text: mensaje,
        input: 'password',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        allowOutsideClick: false,
        allowEscapeKey: false,
        confirmButtonText: ValidarTraduccion('Acceder'),
        cancelButtonText: ValidarTraduccion('Cambiar Usuario'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    IPLocal();
                    var datos = LlamarAjax("InicioSesion" ,"opcion=BuscarUsuario&usuario=" + codigo + "&clave=" + value)
                    datos = datos.split("|");
                    var data = JSON.parse(datos);
					var _error = data[0].error;
					var _mensaje = data[0].mensaje;
					switch (_error) {
						case "0":
							localStorage.setItem("bloqueopantalla", 0);
							resultado = datos[1];
							localStorage.setItem("MinSesion", 20);
							localStorage.setItem("SegSesion", 0);
							PantallaBloqueada = 0;
							if (!websocket){
								init_socker(); 
							}
							ComboUsuarioChat();
							resolve();
							break;
						case "1":
						case "8":
						case "505":
							$("#usuario").val("");
							$("#clave").val("");
							$("#usuario").focus();
							reject(_mensaje);
							if (_error == "8")
								IPLocal()
							break;
						case "9":
							window.location.href =url_cliente + "InicioSesion/CambioClave.php";
							resolve()
							break;
					  
					}

                } else {
                    reject(ValidarTraduccion('Debe de ingresar la clave del usuario'));
                }
            })
        }
    }).then(function (result) {
        if (result) {
            swal({
                type: 'success',
                html: 'Usuario desbloqueado con éxito'
            })
            PermisosGenerales();
            PermisosMenu();
        }
    },

    function(dismiss) {
        if (dismiss == 'cancel') {
            MenuCerrarSesion()
            return false;
        }
    });

    $(".swal2-content").html(mensaje);
}

function sleep(ms) {
    var d = new Date();
    var d2 = null;
    do { d2 = new Date(); }
    while (d2 - d < ms);
}


function LlamarAjax(ruta, parametro, direccion) {
    rutainicial = ruta;
    if (direccion) {
        ruta = direccion + ruta;
    } else {
        ruta = url_servidor + ruta;
    }
            
    resultado = "";
    $.ajax({
        url: ruta,
        type: "POST",
        async: false,
        data: parametro,
        dataType: "html",
        cache: false,
        xhrFields: {
			withCredentials: true
		},
        success: function (datos) {
            resultado = $.trim(datos);
        },
    }).fail(function( jqXHR, textStatus, errorThrown ) {
		BloquearPantalla();
		resultado = "error-ajax";
		return false;
	});
	
	if (resultado == "error-ajax")
		return false;

    var session = $.trim(resultado.substr(0, 7));
    var session2 = $.trim(resultado.substr(0, 9));
    if (session == "cerrada" || session2 == "cambio-ip") {
        if (session2 == "cambio-ip")
            $.jGrowl("Sesión cerrada por cambio de ip en otra sesión", { life: 3500, theme: 'growl-success', header: '' });
		else
			LlamarAjax("InicioSesion","opcion=CerrarSession");
        BloquearPantalla(rutainicial, parametro, direccion);
        return false;
    } else {
        if (session == "") {
            swal({
                title: 'Alerta',
                text: 'Error al conectarse con el servidor' + ' ' + ruta,
                type: 'error',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Aceptar!',
            }).then(function () {
                return resultado;
            });
        } else
            return resultado;
    }

    return resultado;


}

function ConvertirParametros2(json){
	
	for (var clave in json){
		if (json.hasOwnProperty(clave)) {
			console.log(clave, json[clave]);
		}
	}
}

function ConvertirParametros(json){
	var nivel = 0;
	var parametro = "";
	for (var clave in json){
		nivel = 0;
		parametro += "&" + clave + "=";
		if (json.hasOwnProperty(clave)) {
			for (var j in json[clave]) {
				if (j == "id") {
					nivel = 1;
					parametro += $.trim(json[clave][j]);
					break;
				}
			}
			if (nivel == 0)
				parametro += $.trim(json[clave]);
		}
	}
	return parametro;
}


function LlamarAjaxAsy(url, parametro, mensaje, separador) {
	if (!separador)
		separador = "|";
    resultado = "";
    $.ajax({
        url: url_servidor +  url,
        type: "POST",
        async: true,
        data: parametro,
        dataType: "html",
        cache: false,
        success: function (datos) {
			datos = datos.split(separador);
            if (mensaje){
				if (datos[0] == "0")
					$.jGrowl($.trim(datos[mensaje]), { life: 1000, theme: 'growl-success', header: '' });
				else
					$.jGrowl($.trim(datos[mensaje]), { life: 1000, theme: 'growl-warning', header: '' });
			}
        }
    }).fail(function( jqXHR, textStatus, errorThrown ) {
		BloquearPantalla();
		resultado = "error-ajax";
		return false;
	});
}



function CargarModalEmpresa(numero) {
    var ingreso = numero;
    if (!numero)
        ingreso = localStorage.getItem("Ingreso") * 1;
    if (ingreso == 0)
        return false;
    var datos = LlamarAjax('Cotizacion','opcion=ModalEmpresa&ingreso=' + ingreso);
    var data = JSON.parse(datos);
    $("#ERemision").val(data[0].remision);
    $("#EIngreso").val(data[0].ingreso);
    $("#EFecha").val(data[0].fecha);
    $("#EUsuario").val(data[0].usuario);

    $("#EmDocumento").html(data[0].documento);
    $("#ECliente").html(data[0].cliente);
    $("#ESede").html(data[0].direccion);
    $("#EContacto").html(data[0].contacto);
    $("#ETelefonos").html(data[0].telefonos + " / " + (data[0].fax ? data[0].fax : ""));
    $("#EEmail").html(data[0].email);
    $("#EAseComercial").html(data[0].asesor);

    $("#EHabitual").html(data[0].habitual);
    $("#EAproAjuste").html(data[0].aprobar_ajuste);
    $("#EAproCotizacion").html(data[0].aprobar_cotizacion);

    $("#ENombre").html(data[0].nombreca);
    $("#EDireccion").html(data[0].direccionca);

    $("#modalEmpresa").modal("show");

}


function ModalProveedores() {
    $("#modalProveedores").modal("show");
    $("#CRazon").focus();
}

function CargarModalClientes(prefijo) {

    var Razon = $.trim($("#CRazon").val());
    var Cedula = $.trim($("#CCedula").val());

    var parametros = "Razon=" + Razon + "&Cedula=" + Cedula;
    var datos = LlamarAjax('Cotizacion', 'opcion=ModalClientes&' + parametros);
    var datajson = JSON.parse(datos);
    var tabla = $('#tablamodalcliente' + prefijo).DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        searching: false,
        columns: [
            { "data": "tipocliente" },
            { "data": "documento" },
            { "data": "nombrecompleto" },
            { "data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(_CM, _CD, 2, '') },
            { "data": "iva", "className": "text-right", render: $.fn.dataTable.render.number(_CM, _CD, 2, '') },
            { "data": "moneda" },
            { "data": "direccion" },
            { "data": "ciudad" },
            { "data": "telefono" },
            { "data": "email" }
        ],
        "lengthMenu": [[8], [8]],
        "language": {
            "url": LenguajeDataTable
        }

    });
}


function ModalClientes(prefijo) {
    $("#modalClientes" + prefijo).modal("show");
    setTimeout(function () {
        $("#CRazon").focus();
    }, 500);
}

function ModalEmpleados() {
    $("#modalEmpleados").modal("show");
}


function CargarModalEmpleados(prefijo) {

    var Nombre = $.trim($("#ENombres").val());
    var Documento = $("#EDocumento").val()*1;

    var parametros = "nombre=" + Nombre + "&documento=" + Documento + "&tipodocumento=T&estado=TODOS&cargo=0&area=&afp=&eps=&cesantia=";
    var datos = LlamarAjax('Recursohumano','opcion=ConsultarEmpleados&' + parametros);
    var datajson = JSON.parse(datos);
    var tabla = $('#tablamodalempleados_' + prefijo).DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        searching: false,
        columns: [
            { "data": "tipodocumento" },
            { "data": "documento" },
            { "data": "nombrecompleto" },
            { "data": "estado" },
            { "data": "cargo" },
            { "data": "sueldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "celular" },
            { "data": "email" },
            { "data": "id" }
        ],
        "lengthMenu": [[5], [5]],
        "language": {
            "url": LenguajeDataTable
        }

    });


}

function CargarModalProveedores() {

    var Razon = $.trim($("#PRazon").val());
    var Cedula = $.trim($("#PCedula").val());

    var parametros = "Razon=" + Razon + "&Cedula=" + Cedula;
    var datos = LlamarAjax("Cotizacion","opcion=ModalProveedores&" + parametros);
    var datajson = JSON.parse(datos);
    var tabla = $('#tablamodalproveedore').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        searching: false,
        columns: [
            { "data": "documento" },
            { "data": "nombrecompleto" },
            { "data": "direccion" },
            { "data": "pais" },
            { "data": "telefono" },
            { "data": "email" }
        ],
        "lengthMenu": [[5], [5]],
        "language": {
            "url": LenguajeDataTable
        }

    });


}

function CargarModalRemision() {


    var ingreso = $("#MIngreso").val()*1;
    var remision = $("#MRemision").val() *1;
    var cliente = $.trim($("#MCliente").val());

    if (ingreso == 0 && remision == 0 && cliente.length > 4)
        return false;

    var parametros = "cliente=" + cliente + "&remision=" + remision + "&ingreso=" + ingreso;
    var datos = LlamarAjax('Cotizacion/ModalRemision', parametros);
    var datajson = JSON.parse(datos);
    var tabla = $('#tablamodalremision').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        searching: false,
        columns: [
            { "data": "remision" },
            { "data": "cotizacion" },
            { "data": "ingresos" },
            { "data": "cliente" },
            { "data": "estado" },
            { "data": "fecha" },
            { "data": "totales" },
            { "data": "usuario" },
            { "data": "observacion" }
        ],

        "language": {
            "url": LenguajeDataTable
        },
        "lengthMenu": [[8], [8]]
    });


}

function CargarModalIngreso(tipo,prefijo) {

    var idcliente = localStorage.getItem("idcliente") * 1;

    var parametros = "idcliente=" + idcliente + (tipo ? "&tipo=" + tipo : "&tipo=0");
    var datos = LlamarAjax("Cotizacion","opcion=ModalIngreso&" + parametros);
    var datajson = JSON.parse(datos);
    var tabla = $('#tablamodalingreso' + prefijo).DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        columns: [
            { "data": "remision" },
            { "data": "ingreso", "className": "text-XX" },
            { "data": "fecha" },
            { "data": "equipo" },
            { "data": "serie" },
            { "data": "usuario" },
            { "data": "observacion" }
        ],

        "lengthMenu": [5],

        "language": {
            "url": LenguajeDataTable
        }

    });


}


function CargarModalInventarios(prefijo) {

    var codigo = $.trim($("#MCodigo").val());
    var descripcion = $.trim($("#MDescripcion").val());
    var grupo = $("#MGrupo").val() * 1;
    var precio = $("#MPrecio").val() * 1;

    var parametros = "codigo=" + codigo + "&descripcion=" + descripcion + "&grupo=" + grupo + "&precio=" + precio;
    var datos = LlamarAjax('Inventarios','opcion=ModalArticulos&' + parametros);
    var datajson = JSON.parse(datos);
    
    var tabla = $('#TablaInventario' + prefijo).DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        searching: false,
        columns: [
            { "data": "foto" },
            { "data": "id" },
            { "data": "codigointerno" },
            { "data": "codigobarras" },
            { "data": "grupo" },
            { "data": "tipo" },
            { "data": "equipo" },
            { "data": "marca" },
            { "data": "modelo" },
            { "data": "presentacion" },
            { "data": "caracteristica" },
            { "data": "existencia" },
            { "data": "separadas" },
            { "data": "cotizadas" },
            { "data": "disponibles" },
            { "data": "ultimocosto", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "precio", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "ultimocostousd", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "preciousd", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "referencia" },
            { "data": "uso_interno" },
            { "data": "iva" },
            { "data": "cuenta" },
            { "data": "proveedor" }
        ],
        "lengthMenu": [[5], [5]],
        "language": {
            "url": LenguajeDataTable
        }

    });
}



function ComboUbicacion() {
    var resultado = "<option value=''>--</option>";
    var letras = ["A", "B", "C", "D", "E", "F", "G"]
    for (var x = 2; x <= 5; x++) {
        for (var y = 2; y <= 6; y++) {
            resultado += "<option value='" + letras[y] + x + "'>" + letras[y] + x + "</option>";
        }
    }
    return resultado;
}



function AbrirFotoModal() {
    var imagen = $("#FotoIngreso").attr("src");
    window.open(imagen);
}

function CerrarModalFoto() {
    var image = $("#FotoIngreso");
    $.removeData(image, 'elevateZoom');
    $('.zoomContainer').remove();// remove zoom container from DOM
    $(".ligthbox").remove();
    $("#modalFotos").modal("hide");
}


function CargarModalFoto(Ingreso, fotos, tipo, cliente) {
    var titulo = "";
    var mensaje = "";
    var ruta = "";
    switch (tipo) {
        case 1:
			fotos = LlamarAjax("Cotizacion","opcion=CantidadFoto&ingreso=" + Ingreso) * 1;
            titulo = "Ingreso";
            mensaje = "Al ingreso número " + Ingreso + " no se le han registrado fotos";
            ruta = url_cliente + "Adjunto/imagenes/ingresos/" + Ingreso + "/";
            break;
        case 2:
			fotos = LlamarAjax("Cotizacion","opcion=CantidadFotoDev&devolucion=" + Ingreso) * 1;
            titulo = "Devolución";
            mensaje = "A la devolución número " + Ingreso + " no se le han registrado fotos";
            ruta = url_cliente + "Adjunto/imagenes/Devoluciones/" + Ingreso + "/";
            break;
        case 3:
            titulo = "Entrega";
            mensaje = "A la entrega de la devolución número " + Ingreso + " no se le han registrado fotos";
            ruta = url_cliente + "Adjunto/imagenes/FotoEntrega/" + Ingreso + "/";
            break;
        case 4:
            titulo = "Precio";
            mensaje = "Al precio número " + Ingreso + " no se le han registrado fotos";
            ruta = url_cliente + "Adjunto/imagenes/Precios/" + Ingreso + "/";
            break;
        case 5:
            titulo = "Registro de Vueltas";
            mensaje = "La vuelta número " + Ingreso + " no se le han registrado fotos";
            ruta = url_cliente + "Adjunto/imagenes/FotoRecogida/" + cliente + "/" + Ingreso + "/";
            break;
    }

    if (fotos == 0) {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var contenedor = "";
    contenedor = "<div class='row'><div class='col-sm-4'><label>" + titulo +"</label><input class='form-control' readonly value='" + Ingreso + "'></div>";
    contenedor += "<div class='col-sm-8 text-right'><br><button class='btn btn-glow btn-primary' type='button' onclick='AbrirFotoModal()'><span>Maximizar Foto</span></button>&nbsp;&nbsp;&nbsp;" +
        "<button class='btn btn-glow btn-warning' type='button' onclick='CerrarModalFoto()'><span>Cerrar</span></button></div></div>" +
        "<hr><div class='row'><div class='col-xs-12 col-sm-12'>" +
        "<img ondblclick='AbrirImagen()'  id='FotoIngreso' class='zoom-img' src='" + ruta + "1.jpg' height='500px' data-zoom-image='" + ruta + "1.jpg'></div></div><hr>";
    contenedor += "<div class='row'><div class='col-xs-12 col-sm-12 scrolling-wrapper' id='my_images'>";
    for (var x = 1; x <= fotos; x++) {
        contenedor += "<a href='#' title='Foto número " + x + "' data-image='" + ruta + x + ".jpg' data-zoom-image='" + ruta + x + ".jpg'><img id='FotoIngreso' src='" + ruta + x + ".jpg' height='80px'></a>&nbsp;";
    }
    contenedor += "</div></div>";

    $("#AlbunFotos").html(contenedor);

    setTimeout(function () {

        $('#FotoIngreso').elevateZoom({
            scrollZoom: true,
            cursor: "crosshair",
            zoomWindowFadeIn: 500,
            zoomWindowFadeOut: 750,
            borderSize: 1,
            zoomLevel: 0.8,
            gallery: "my_images"
        });

    }, 300);

    $("#modalFotos").modal({ backdrop: 'static', keyboard: false }, 'show');
}

function DatosTemperatura(magnitud, sensor) {
    var datos = LlamarAjax("Laboratorio","opcion=DatosTemperatura&sensor=" + sensor + "&magnitud=" + magnitud).split("|");
    var resultado = "";
    var resultado2 = "";
    var resultado3 = "";
    var pos = 0;
    var data = JSON.parse(datos[0]);
    for (var x = 0; x < data.length; x++) {
        if (data[x].descripcion == "TERMOHIGRÓMETRO DIGITAL") {
			
			if (data[x].medida == "Temperatura"){
				resultado += "<tr class='tabcertr'>";
				resultado += "<td class='text-center'>" + data[x].patron + "</td>";
				resultado += "<td class='text-center'>" + data[x].instrumento + "</td>";
				resultado += "<td class='text-center'>" + formato_numero(data[x].patron - data[x].instrumento, _DE,_CD,_CM,"") + "</td>";
				resultado += "<td class='text-center'>" + data[x].variable + "</td></tr>";
            }else{
				resultado3 += "<tr class='tabcertr'>";
				resultado3 += "<td class='text-center'>" + data[x].patron + "</td>";
				resultado3 += "<td class='text-center'>" + data[x].instrumento + "</td>";
				resultado3 += "<td class='text-center'>" + formato_numero(data[x].patron - data[x].instrumento, _DE,_CD,_CM,"") + "</td>";
				resultado3 += "<td class='text-center'>" + data[x].variable + "</td></tr>";
			}
        }else{
			if (data[x].descripcion == "BARÓMETRO") {
				resultado2 += "<tr>" +
					"<td class='text-center'>" + data[x].patron + "</td>" +
					"<td class='text-center'>" + data[x].instrumento + "</td>" +
					"<td class='text-center'>" + formato_numero(data[x].patron - data[x].instrumento, 1, ".", ",", "") + "</td>" +
					"<td class='text-center'>" + formato_numero((data[x].patron - data[x].instrumento) * -1, 1, ".", ",", "") + "</td>" +
					"<td class='text-center'>" + data[x].variable + "</td></tr>";
			}
		}

    }
    $("#TBTemperatura").html(resultado);
    $("#TBHumedad").html(resultado3);
    $("#TBAtmosferica").html(resultado2);

    var data = JSON.parse(datos[1]);
    for (var x = 0; x < data.length; x++) {
        if (data[x].tipo == "Minimo" && data[x].medida == "Temperatura") {
            $("#MinTemp1").html(data[x].valor1);
            $("#MinTemp2").html(data[x].valor2);
            $("#MinTemp3").html(data[x].valor3);
        }

        if (data[x].tipo == "Minimo" && data[x].medida == "Humedad") {
            $("#MinHume1").html(data[x].valor1);
            $("#MinHume2").html(data[x].valor2);
            $("#MinHume3").html(data[x].valor3);
        }

        if (data[x].tipo == "Minimo" && data[x].medida == "Presion") {
            $("#MinPres1").html(data[x].valor1);
            $("#MinPres2").html(data[x].valor2);
            $("#MinPres3").html(data[x].valor3);
            $("#MinPres4").html(data[x].valor4);
        }

        if (data[x].tipo == "Maximo" && data[x].medida == "Temperatura") {
            $("#MaxTemp1").html(data[x].valor1);
            $("#MaxTemp2").html(data[x].valor2);
            $("#MaxTemp3").html(data[x].valor3);
        }
        if (data[x].tipo == "Maximo" && data[x].medida == "Humedad") {
            $("#MaxHume1").html(data[x].valor1);
            $("#MaxHume2").html(data[x].valor2);
            $("#MaxHume3").html(data[x].valor3);
        }

        if (data[x].tipo == "Maximo" && data[x].medida == "Presion") {
            $("#MaxPres1").html(data[x].valor1);
            $("#MaxPres2").html(data[x].valor2);
            $("#MaxPres3").html(data[x].valor3);
            $("#MaxPres4").html(data[x].valor4);
        }
    }
    $("#TBTemperatura").html(resultado);
}

function AlertaVueltas() {

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Configuracion", "opcion=AlertaVueltas");
        DesactivarLoad();
        if (datos != "[]") {
            resultado = "";
            var data = JSON.parse(datos);
            for (var x = 0; x < data.length; x++) {
                resultado += "<tr>" +
                    "<td>" + data[x]._ingreso + "</td>" +
                    "<td>" + data[x]._cliente + "</td>" +
                    "<td>" + data[x]._equipo + "</td>" + 
                    "<td>" + data[x]._fecha + "</td>" +
                    "<td>" + data[x]._actividad + "</td>" +
                    "<td>" + data[x]._programador + "</td>" +
                    "<td>" + data[x]._fechapro + "</td></tr>";
            }
            $("#GenTablaVueltas").html(resultado);
            $("#ModalAltertaVueltas").modal({ backdrop: 'static' }, 'show');
            EscucharMensaje("Buen día. Ingresos que se deben bajar a logística, para entregar equipos.");
        }
    }, 15);
}

function AlertaCotizaApro() {

    var datos = LlamarAjax("Cotizacion" + "opcion=AlertaCotizaApro", "");
    if (datos != "[]") {
        resultado = "";
        var data = JSON.parse(datos);
        for (var x = 0; x < data.length; x++) {
            resultado += "<tr>" +
                "<td>" + data[x].cotizacion + "</td>" +
                "<td>" + data[x].fecha+ "</td>" +
                "<td>" + data[x].estado + "</td>" +
                "<td>" + data[x].cliente + "</td>" +
                "<td>" + data[x].contacto + "</td>" +
                "<td>" + data[x].direccion + "</td>" +
                "<td>" + data[x].telefonos + "</td>" +
                "<td>" + data[x].tiempo + "</td></tr>";
        }
        $("#GenTablaCotiAprobada").html(resultado);
        $("#ModalAltertaCotiAproRech").modal("show");

    }
}

function AlertaSolicitudes() {

    var datos = LlamarAjax("Cotizacion","opcion=AlertaSolicitud");
    if (datos != "[]") {
        resultado = "";
        var data = JSON.parse(datos);
        for (var x = 0; x < data.length; x++) {
            resultado += "<tr>" +
                "<td>" + data[x].solicitud + "</td>" +
                "<td>" + data[x].fecha + "</td>" +
                "<td>" + data[x].cliente + "</td>" +
                "<td>" + data[x].contacto + "</td>" +
                "<td>" + data[x].direccioncli + "</td>" +
                "<td>" + data[x].telefonos + "</td>" +
                "<td>" + data[x].email + "</td>" +
                "<td>" + data[x].cantidad + "</td>" +
                "<td>" + data[x].tiempo + "</td></tr>";
        }
        $("#GenTablaSolicitudes").html(resultado);
        $("#ModalAltertaSolicitudes").modal("show");
    }
}

function MantenSesion2() {
    return LlamarAjax("InicioSesion", "opcion=MantenerSesion2");
}

function AbrirMenuCoti(cotizacion) {
    $("#ModalAltertaComer").modal("hide");
    if (localStorage.getItem('ModuloCotizacion') == 0)
        LlamarOpcionMenu('Registro/Cotizacion', 2, 'ul_comercial');
    localStorage.setItem('ModuloCotizacion', 2);
    setTimeout(function () {
        $("#Cotizacion").val(cotizacion);
        BuscarCotizacion(cotizacion);
    }, 500);
}

function MantenSesion() {

    ActivarLoad();
    setTimeout(function () {

        var mostrar = 0;
        var contador = 0;
        $("#TablaG1").addClass("hidden");
        $("#TablaG2").addClass("hidden");
        $("#TablaG3").addClass("hidden");
        $("#TablaG4").addClass("hidden");
        $("#TablaG5").addClass("hidden");
        $("#TablaG6").addClass("hidden");
        $("#TablaG7").addClass("hidden");
        $("#TablaG8").addClass("hidden");
        $("#TablaG9").addClass("hidden");
        $("#TablaG10").addClass("hidden");
        $("#TablaG11").addClass("hidden");
        $("#TablaG12").addClass("hidden");
        $("#TablaG13").addClass("hidden");
        $("#TablaG14").addClass("hidden");

        var datos = LlamarAjax("Cotizacion","opcion=AlertaComercial").split("|");
        if (datos[0] != "[]") {
            $("#TablaG1").removeClass("hidden");
            resultado = "";
            mostrar = 1;
            var data = JSON.parse(datos[0]);
            for (var x = 0; x < data.length; x++) {
                resultado += "<tr>" +
                    "<td>" + data[x].remision + "</td>" +
                    "<td>" + data[x].fecha + "</td>" +
                    "<td>" + data[x].cliente + "</td>" +
                    "<td>" + data[x].contacto + "</td>" +
                    "<td>" + data[x].direccion + "</td>" +
                    "<td>" + data[x].telefonos + "</td>" +
                    "<td>" + data[x].tiempo + "</td></tr>";
            }
            $("#GenTablaRemision").html(resultado);
        }
        if (datos[1] != "[]") {
            resultado = "";
            $("#TablaG2").removeClass("hidden");
            mostrar = 1;
            var data = JSON.parse(datos[1]);
            for (var x = 0; x < data.length; x++) {
                resultado += "<tr>" +
                    "<td><a href='javascript:AbrirMenuCoti(" + data[x].cotizacion + ")'><b>" + data[x].cotizacion + "</b></a><br>" + data[x].estado + "</td>" +
                    "<td>" + data[x].fecha + "</td>" +
                    "<td>" + data[x].cliente + "</td>" +
                    "<td>" + data[x].contacto + "</td>" +
                    "<td>" + data[x].direccion + "</td>" +
                    "<td>" + data[x].telefonos + "</td>" +
                    "<td>" + data[x].tiempo + "</td></tr>";
            }
            $("#GenTablaCotizacion").html(resultado);

            if (datos[2] != "[]") {
                resultado = "<tr>";
                $("#TablaG8").removeClass("hidden");
                mostrar = 1;
                var data = JSON.parse(datos[2]);
                contador = 0;
                for (var x = 0; x < data.length; x++) {
                    if (contador % 5 == 0) {
                        if (x > 0)
                            resultado += "</tr>"
                        resultado += "<tr>"
                    }
                    resultado += "<td>" + data[x].ingreso + "</td>" +
                        "<td>" + data[x].tiempo + "</td>";
                    contador++;
                }
                resultado += "</tr><tr><th colspan='10' class='text-XX'>TOTALES: " + data.length + "</th></tr>";
                $("#GenTablaIngCotizar").html(resultado);
            }
            if (datos[3] != "[]") {
                resultado = "<tr>";
                $("#TablaG9").removeClass("hidden");
                mostrar = 1;
                var data = JSON.parse(datos[3]);
                contador = 0;
                for (var x = 0; x < data.length; x++) {
                    if (contador % 5 == 0) {
                        if (x > 0)
                            resultado += "</tr>"
                        resultado += "<tr>"
                    }
                    resultado += "<td>" + data[x].ingreso + "</td>" +
                        "<td>" + data[x].tiempo + "</td>";
                    contador++;
                }
                resultado += "</tr><tr><th colspan='10' class='text-XX'>TOTALES: " + data.length + "</th></tr>";
                $("#GenTablaIngOrdenCompra").html(resultado);
            }
            if (datos[4] != "[]") {
                resultado = "<tr>";
                $("#TablaG10").removeClass("hidden");
                mostrar = 1;
                var data = JSON.parse(datos[4]);
                contador = 0;
                for (var x = 0; x < data.length; x++) {
                    if (contador % 5 == 0) {
                        if (x > 0)
                            resultado += "</tr>"
                        resultado += "<tr>"
                    }
                    resultado += "<td>" + data[x].ingreso + "</td>" +
                        "<td>" + data[x].tiempo + "</td>";
                    contador++;
                }
                resultado += "</tr><tr><th colspan='10' class='text-XX'>TOTALES: " + data.length + "</th></tr>";
                $("#GenTablaIngTerSalida").html(resultado);
            }
            if (datos[5] != "[]") {
                resultado = "<tr>";
                $("#TablaG11").removeClass("hidden");
                mostrar = 1;
                var data = JSON.parse(datos[5]);
                contador = 0;
                for (var x = 0; x < data.length; x++) {
                    if (contador % 5 == 0) {
                        if (x > 0)
                            resultado += "</tr>"
                        resultado += "<tr>"
                    }
                    resultado += "<td>" + data[x].ingreso + "</td>" +
                        "<td>" + data[x].tiempo + "</td>";
                    contador++;
                }
                resultado += "</tr><tr><th colspan='10' class='text-XX'>TOTALES: " + data.length + "</th></tr>";
                $("#GenTablaIngSalSinReto").html(resultado);
            }
        }

        //Alerta de Laboratorio

        var datos = LlamarAjax("Cotizacion","opcion=AlertaFacturacion").split("|");
        if (datos[0] != "[]") {
            resultado = "<tr>";
            $("#TablaG12").removeClass("hidden");
            mostrar = 1;
            var data = JSON.parse(datos[0]);
            contador = 0;
            for (var x = 0; x < data.length; x++) {
                if (contador % 5 == 0) {
                    if (x > 0)
                        resultado += "</tr>"
                    resultado += "<tr>"
                }
                resultado += "<td>" + data[x].ingreso + "</td>" +
                    "<td>" + data[x].tiempo + "</td>";
                contador++;
            }
            resultado += "</tr><tr><th colspan='10' class='text-XX'>TOTALES: " + data.length + "</th></tr>";
            $("#GenTablaIngFacturado").html(resultado);
        }

        var datos = LlamarAjax("Cotizacion","opcion=AlertaLaboratorio");
        var data = JSON.parse(datos);
        if (data.Table.length > 0) {
            resultado = "<tr>";
            $("#TablaG3").removeClass("hidden");
            mostrar = 1;
            contador = 0;
            for (var x = 0; x < data.Table.length; x++) {
                if (contador % 5 == 0) {
                    if (x > 0)
                        resultado += "</tr>"
                    resultado += "<tr>"
                }
                resultado += "<td>" + data.Table[x].ingreso + "</td>" +
                    "<td>" + data.Table[x].tiempo + "</td>";
                contador++;
            }
            resultado += "</tr><tr><th colspan='10' class='text-XX'>TOTALES: " + data.Table.length + "</th></tr>";
            $("#GenTablaIngReportar").html(resultado);
        }
        if (data.Table7.length > 0) {
            resultado = "<tr>";
            $("#TablaG17").removeClass("hidden");
            mostrar = 1;
            contador = 0;
            for (var x = 0; x < data.Table7.length; x++) {
                if (contador % 5 == 0) {
                    if (x > 0)
                        resultado += "</tr>"
                    resultado += "<tr>"
                }
                resultado += "<td>" + data.Table7[x].ingreso + "</td>" +
                    "<td>" + data.Table7[x].tiempo + "</td>";
                contador++;
            }
            resultado += "</tr><tr><th colspan='10' class='text-XX'>TOTALES: " + data.Table7.length + "</th></tr>";
            $("#GenTablaIngRecoIngreso").html(resultado);
        }
        if (data.Table1.length > 0) {
            resultado = "<tr>";
            $("#TablaG4").removeClass("hidden");
            mostrar = 1;
            contador = 0;
            for (var x = 0; x < data.Table1.length; x++) {
                if (contador % 5 == 0) {
                    if (x > 0)
                        resultado += "</tr>"
                    resultado += "<tr>"
                }
                resultado += "<td>" + data.Table1[x].ingreso + "</td>" +
                    "<td>" + data.Table1[x].tiempo + "</td>";
                contador++;
            }
            resultado += "</tr><tr><th colspan='10' class='text-XX'>TOTALES: " + data.Table1.length + "</th></tr>";
            $("#GenTablaIngCertificado").html(resultado);
        }
        if (data.Table2.length > 0) {
            resultado = "<tr>";
            $("#TablaG5").removeClass("hidden");
            mostrar = 1;
            contador = 0;
            for (var x = 0; x < data.Table2.length; x++) {
                if (contador % 5 == 0) {
                    if (x > 0)
                        resultado += "</tr>"
                    resultado += "<tr>"
                }
                resultado += "<td>" + data.Table2[x].ingreso + "</td>" +
                    "<td>" + data.Table2[x].tiempo + "</td>";
                contador++;
            }
            resultado += "</tr><tr><th colspan='10' class='text-XX'>TOTALES: " + data.Table2.length + "</th></tr>";
            $("#GenTablaIngLogistica").html(resultado);
        }

        if (data.Table3.length > 0) {
            resultado = "<tr>";
            $("#TablaG13").removeClass("hidden");
            mostrar = 1;
            contador = 0;
            for (var x = 0; x < data.Table3.length; x++) {
                if (contador % 5 == 0) {
                    if (x > 0)
                        resultado += "</tr>"
                    resultado += "<tr>"
                }
                resultado += "<td>" + data.Table3[x].ingreso + "</td>" +
                    "<td>" + data.Table3[x].tiempo + "</td>";
                contador++;
            }
            resultado += "</tr><tr><th colspan='10' class='text-XX'>TOTALES: " + data.Table3.length + "</th></tr>";
            $("#GenTablaIngStiquer").html(resultado);
        }

        if (data.Table4.length > 0) {
            resultado = "<tr>";
            $("#TablaG14").removeClass("hidden");
            mostrar = 1;
            contador = 0;
            for (var x = 0; x < data.Table4.length; x++) {
                if (contador % 5 == 0) {
                    if (x > 0)
                        resultado += "</tr>"
                    resultado += "<tr>"
                }
                resultado += "<td>" + data.Table4[x].ingreso + "</td>" +
                    "<td>" + data.Table4[x].tiempo + "</td>";
                contador++;
            }
            resultado += "</tr><tr><th colspan='10' class='text-XX'>TOTALES: " + data.Table4.length + "</th></tr>";
            EscucharMensaje("Servicios express pendientes por certificado ó informe técnico, que debe bajar a logística");
            $("#GenTablaIngExpress").html(resultado);
        }

        if (data.Table5.length > 0) {
            resultado = "<tr>";
            $("#TablaG15").removeClass("hidden");
            mostrar = 1;
            contador = 0;
            for (var x = 0; x < data.Table5.length; x++) {
                if (contador % 5 == 0) {
                    if (x > 0)
                        resultado += "</tr>"
                    resultado += "<tr>"
                }
                resultado += "<td>" + data.Table5[x].ingreso + "</td>" +
                    "<td>" + data.Table5[x].tiempo + "</td>";
                contador++;
            }
            resultado += "</tr><tr><th colspan='10' class='text-XX'>TOTALES: " + data.Table5.length + "</th></tr>";
            $("#GenTablaCerNoImpreso").html(resultado);
        }

        if (data.Table6.length > 0) {
            resultado = "<tr>";
            $("#TablaG16").removeClass("hidden");
            mostrar = 1;
            contador = 0;
            for (var x = 0; x < data.Table6.length; x++) {
                if (contador % 5 == 0) {
                    if (x > 0)
                        resultado += "</tr>"
                    resultado += "<tr>"
                }
                resultado += "<td>" + data.Table6[x].ingreso + "</td>" +
                    "<td>" + data.Table6[x].tiempo + "</td>";
                contador++;
            }
            resultado += "</tr><tr><th colspan='10' class='text-XX'>TOTALES: " + data.Table6.length + "</th></tr>";
            $("#GenTablaCerNoAprobado").html(resultado);
        }

        var datos = LlamarAjax("Cotizacion" , "opcion=AlertaLogistica").split("|");
        if (datos[0] != "[]") {
            resultado = "<tr>";
            $("#TablaG6").removeClass("hidden");
            mostrar = 1;
            var data = JSON.parse(datos[0]);
            contador = 0;
            for (var x = 0; x < data.length; x++) {
                if (contador % 5 == 0) {
                    if (x > 0)
                        resultado += "</tr>"
                    resultado += "<tr>"
                }
                resultado += "<td>" + data[x].ingreso + "</td>" +
                    "<td>" + data[x].tiempo + "</td>";
                contador++;
            }
            resultado += "</tr><tr><th colspan='10' class='text-XX'>TOTALES: " + data.length + "</th></tr>";
            $("#GenTablaIngDevolucion").html(resultado);
        }
        if (datos[1] != "[]") {
            resultado = "<tr>";
            $("#TablaG7").removeClass("hidden");
            mostrar = 1;
            var data = JSON.parse(datos[1]);
            contador = 0;
            for (var x = 0; x < data.length; x++) {
                if (contador % 5 == 0) {
                    if (x > 0)
                        resultado += "</tr>"
                    resultado += "<tr>"
                }
                resultado += "<td>" + data[x].ingreso + "</td>" +
                    "<td>" + data[x].tiempo + "</td>";
                contador++;
            }
            resultado += "</tr><tr><th colspan='10' class='text-XX'>TOTALES: " + data.length + "</th></tr>";
            $("#GenTablaEntrega").html(resultado);
        }

        DesactivarLoad();
        if (mostrar == 1) {
            $("#ModalAltertaComer").modal("show");
            EscucharMensaje("Hola Amigo. Usted tiene tareas pendientes.");
        }
    }, 15);

}

function ModalDetalleEstado(ingreso , plantilla) {

    var datos = LlamarAjax("Laboratorio","opcion=BuscarEstadoIngreso&ingreso=" + ingreso + "&plantilla=" + plantilla).split("|");
    if (datos[0] != "[]") {
        var data = JSON.parse(datos[0]);
        IdRemision = data[0].id * 1;
        Ingreso = data[0].ingreso * 1;
        localStorage.setItem("Ingreso", Ingreso);
        Fotos = data[0].fotos * 1;
        $("#MEEquipo").val(data[0].equipo);
        $("#MEMarca").val(data[0].marca);
        $("#MEModelo").val(data[0].modelo);
        $("#MESerie").val(data[0].serie);
        $("#MEObservacion").val(data[0].observacion);
        $("#MEAccesorio").val(data[0].accesorio);
        $("#MERango").val(data[0].intervalo);
        $("#MERemision").val(data[0].remision);
        $("#MEMagnitud").val(data[0].magnitud);
        $("#MEEmpresa").val(data[0].cliente);
        $("#MEPlantilla").val(data[0].plantilla);

        $("#MEUsuario").val(data[0].usuarioing);
        $("#MEFechaIng").val(data[0].fechaing);

        CargarFotosME();
        var resultado = "";
        if (datos[1] != "[]") {
            var datarep = JSON.parse(datos[1]);

            for (var x = 0; x < datarep.length; x++) {
                resultado += "<tr>" +
                    "<td>" + (x + 1) + "</td>" +
                    "<td>" + datarep[x].descripcion + "</td>" +
                    "<td>" + datarep[x].usuario + "</td>" +
                    "<td>" + datarep[x].fecha + "</td>" +
                    "<td>" + datarep[x].tiempo + "</td></tr>";
            }
        }
        $("#MEBodyEstados").html(resultado);

        $("#MEDesIngreso").html(ingreso);
        $("#ModalAltertaComer").modal("hide")
        $("#MEModalDetalleEstado").modal("show");


    }
}

function CambiarFotoME(numero) {
    $("#MEFotoIngreso").attr("src", url_cliente + "Adjunto/imagenes/ingresos/" + Ingreso + "/" + numero + ".jpg");
}

function CargarFotosME() {
    var contenedor = "";
    contenedor = "<div class='row'><div class='col-xs-12 col-sm-12'>" +
        "<img id='MEFotoIngreso' " + (Fotos > 0 ? "src='" + url_cliente + "Adjunto/imagenes/ingresos/" + Ingreso + "/1.jpg'" : "") + " width='70%'></div><br><br><div class='col-xs-12 col-sm-12'>";
    for (var x = 1; x <= Fotos; x++) {
        contenedor += "<button class='btn btn-primary' type='button' onclick='CambiarFotoME(" + x + ")'>" + x + "</button>&nbsp;";
    }
    contenedor += "</div></div>";
    $("#MEAlbunFotos").html(contenedor);
}



function RegresaTareaPend() {
    $("#MEModalDetalleEstado").modal("hide");
    $("#ModalAltertaComer").modal("show")
}

function RestarHoras(inicio, fin) {


    inicioMinutos = parseInt(inicio.substr(3, 2));
    inicioHoras = parseInt(inicio.substr(0, 2));

    finMinutos = parseInt(fin.substr(3, 2));
    finHoras = parseInt(fin.substr(0, 2));

    transcurridoMinutos = finMinutos - inicioMinutos;
    transcurridoHoras = finHoras - inicioHoras;

    if (transcurridoMinutos < 0) {
        transcurridoHoras--;
        transcurridoMinutos = 60 + transcurridoMinutos;
    }

    horas = transcurridoHoras.toString();
    minutos = transcurridoMinutos.toString();

    if (horas.length < 2) {
        horas = "0" + horas;
    }

    if (minutos.length < 2) {
        minutos = "0" + minutos;
    }

    return horas + ":" + minutos;

}

function RestarMinutos(inicio, fin) {


    inicioSegundos = parseInt(inicio.substr(3, 2));
    inicioMinutos = parseInt(inicio.substr(0, 2));

    finSegundos = parseInt(fin.substr(3, 2));
    finMinutos = parseInt(fin.substr(0, 2));

    transcurridoMinutos = finMinutos - inicioMinutos;
    transcurridoSegundos = finSegundos - inicioSegundos;

    if (transcurridoSegundos < 0) {
        transcurridoHoras--;
        transcurridoMinutos = 60 + transcurridoMinutos;
    }

    horas = transcurridoHoras.toString();
    minutos = transcurridoMinutos.toString();

    if (horas.length < 2) {
        horas = "0" + horas;
    }

    if (minutos.length < 2) {
        minutos = "0" + minutos;
    }

    return horas + ":" + minutos;

}


function SaldoActual(cliente, mensaje) {
    var datos = LlamarAjax("Caja","opcion=SaldoTotal&cliente=" + cliente + "&tipo=1").split("|");
    var saldo = datos[0]; 
    
    if (saldo > 0 && mensaje == 1) {
        swal("Advertencia", "Cliente posee un saldo de <b>" + formato_numero(saldo, 0,_CD,_CM,"") + "</b> por concepto de facturas vencidas...<br>Tome las precausiones necesarias", "warning");
        EscucharMensaje("Cliente posee facturas vencidas");
        sendMessage(3, "Cliente posee un saldo de <b>" + formato_numero(saldo, 0,_CD,_CM,"") + "</b> por concepto de facturas vencidas...<br>Por favor gestionar el pago para entregar los equipos " + "<br><b>Cliente:</b> " + datos[1] + "<br><b>Asesor:</b> " + datos[2],datos[3]);
    }
    return saldo;
}

function SaldoTotal(cliente) {
    resultado = "";
    var saldo = "0";
    var vencido = "0";
    var anticipo = 0;

    var datos = LlamarAjax("Caja","opcion=SaldoTotal&cliente=" + cliente + "&tipo=2").split("|");
    if (datos[1] != "[]") {
        var data = JSON.parse(datos[1]);
        anticipo = data[0].saldo;
    }
    //Fin Codigo

    if (datos[0] != "[]") {
        var data = JSON.parse(datos[0]);
        saldo = data[0].saldo - anticipo;
        vencido = data[0].vencido;
        if (saldo < 0)
            vencido = 0;
        saldo = formato_numero(saldo, 0, ".", ",", "$");
        vencido = formato_numero(vencido, 0, ".", ",", "$");
    }


    resultado = "<span class='text-sucess text-XX'><a href='javascript:VerEstadoCuenta()'>Saldo Actual</a>: " + saldo + "</span>&nbsp;&nbsp;&nbsp; <span class='text-danger text-XX'>Saldo Vencido: " + vencido + "</span>&nbsp;&nbsp;&nbsp;<span id='MenSeguiCart'></span><input type='hidden' id='IdMenSeguiCart' value='0'/><hr>"

    return resultado;
}

function SeguimientoCartera(devolucion) {
    var datos = LlamarAjax("Caja","opcion=SeguimientoCartera&devolucion=" + devolucion);
    $("#MenSeguiCart").html("");
    $("#IdMenSeguiCart").val("0");
    if (datos != "[]") {
        var data = JSON.parse(datos);
        $("#MenSeguiCart").html(data[0].seguimiento);
        $("#IdMenSeguiCart").val(data[0].id);
    } else {
        $("#MenSeguiCart").html("<b>SIN SEGUIMIENTO DE CARTERA</b>");
    }
}

function EstadoCuenta(id, cliente, email, tipo, plazopago, principal) {
    $("#EcIdCliente").val(id);
    $("#EcCliente").val(cliente);
    $("#EcEmail").val(email);
    $("#EcTipo").val(tipo);
    $("#EcPlazo").val(plazopago);
    $("#EcPrincipal").val(principal)

    var saldo = 0;
    var vencido = 0;

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Caja","opcion=EstadoCuenta&cliente=" + id);
        var datajson = JSON.parse(datos);
        DesactivarLoad();
        var table = $('#TablaEstadoCuenta').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "factura" },
                { "data": "fecha" },
                { "data": "vencimiento" },
                { "data": "dias" },
                { "data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "iva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "abonado", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "saldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });

        table.rows().data().each(function (datos, index) {
            if (datos.dias * 1 > 0)
                vencido += datos.saldo;
            saldo += datos.saldo;
        });

        if (saldo < 0)
            vencido = 0;

        $("#EcSaldo").val(formato_numero(saldo, 0,_CD,_CM,""));
        $("#EcVencido").val(formato_numero(vencido, 0,_CD,_CM,""));

        $("#ModalEstadoCuenta").modal("show");
    }, 15);

}

function EnviarEstadoCuenta() {
    var cliente = $("#EcIdCliente").val();
    var correo = $("#EcEmail").val();
    var principal = $("#EcPrincipal").val();
    var saldo = NumeroDecimal($("#EcSaldo").val());

    if (saldo == 0) {
        swal("Acción Cancelada", "No hay facturas pendientes para enviar", "warning");
        return false;
    }

    a_correo = correo.split(";");
    var observacion = $("#ObserEnvio").val();

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#EcEmail").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo onválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "principal=" + principal + "&e_email=" + correo + " &idcliente=" + cliente;
        var datos = LlamarAjax("Caja","opcion=EnvioEstadoCuenta&" + parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + principal + " " + correo, "success");
        } else
            swal("Error", datos[1], "error");
    }, 15);

}

function RespaldarBase(mensaje) {
    var datos = LlamarAjax("Base/RespaldarBaseDatos", "").split("|");
    if (mensaje == 1) {
        if (datos[0] == "0") {
            swal("", datos[1], "success");
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    }
}

function PerGeneSistema(accion) {
    var datos = LlamarAjax("Seguridad","opcion=PermisosSistemas&Accion=" + accion);
    return datos*1;
}

$("#FormControlAcceso").submit(function (e) {

    e.preventDefault();
    tipo = $("#TipoAccesso").val();
    usuario = $("#AccUsuario").val();
    clave = $("#AccClave").val();
    valor = $("#ValorAccesso").val();

    if (usuario == "" || clave == "") {
        $("#AccUsuario").focus();
        swal("Acción Cancelada", "Debe de ingresar el usuario y la clave", "warning");
        return false;
    }
    var datos = LlamarAjax("Seguridad","opcion=ControlAcceso&usuario=" + usuario + "&clave=" + clave + "&tipo=" + tipo).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        $("#modalControlAcceso").modal("hide");
        UsuarioAcceso = datos[2];
        switch (tipo) {
            case "GENERAR CERTIFICADO CON FIRMA":
                $("#FirmarCertificadoPT").removeClass("hidden");
                localStorage.setItem("UsuarioCertificado", UsuarioAcceso);
                var img = new Image();
                img.src = url_cliente + "Adjunto/imagenes/FirmaUsuario/" + UsuarioAcceso + ".png";
                var ctx = canvafirma.getContext("2d");

                clearCanvas(canvafirma, ctx);

                ctx.drawImage(img, 0, 0);

                img.onload = function () {
                    ctx.drawImage(img, 0, 0);
                }

                localStorage.setItem("FirmaCertificado", "1");

                break;
            case "CERTIFICADO APROBAR":
                ModalAprobarCertificado(valor, UsuarioAcceso);
                break;
        }
    }
    else {
        $("#AccClave").focus();
        swal("Acción Cancelada", datos[1], "warning");
    }
})



function Calcular_TStudent(grado) {
    valor = $.trim(grado).split(".");
    grado = valor[0]*1;

    var factor = 0;
    if (grado <= 29)
        factor = datastuden[(grado - 1)].por5;
    else {
        if (grado >= 30 && grado < 35)
            factor = datastuden[29].por5;
        if (grado >= 35 && grado < 40)
            factor = datastuden[30].por5;
        if (grado >= 40 && grado < 45)
            factor = datastuden[31].por5;
        if (grado >= 45 && grado < 50)
            factor = datastuden[32].por5;
        if (grado >= 50 && grado < 60)
            factor = datastuden[33].por5;
        if (grado >= 60 && grado < 80)
            factor = datastuden[34].por5;
        if (grado >= 80 && grado < 100)
            factor = datastuden[35].por5;
        if (grado >= 100 && grado < 159)
            factor = datastuden[36].por5;
        if (grado >= 159 && grado < 473)
            factor = datastuden[37].por5;
        if (grado >= 473)
            factor = datastuden[38].por5;
    }
    return factor;
}

function CentralTelefonica() {
    var datos = LlamarAjax("Configuracion", "opcion=CentralTelefonica");
    if (datos != "[]") {
        resultado = "";
        var data = JSON.parse(datos);
        for (var x = 0; x < data.length; x++) {
            resultado += "<tr>" +
                "<td>" + data[x].numero + "</td>" +
                "<td>" + data[x].oficina + "</td>" +
                "<td>" + data[x].usuario + "</td></tr>";
        }
        $("#GenTablaCentral").html(resultado);
        $("#ModalCentral").modal("show");
    }
}

function AvisoCorreoCertificado(ingreso, numero) {
    
    var direccion = url_cliente + "Laboratorio/AvisoEnvioCertificado";
    $.ajax({
        type: 'POST',
        url: direccion,
        data: "certificado=" + Certificado + "&numero=" + numero,
        async: true,
        success: function (datos) {
            datos = datos.split("|");
            if (datos[0] == "0") {
                $.jGrowl(datos[1], { life: 1500, theme: 'growl-success', header: '' });
            } else {
                $.jGrowl(datos[1], { life: 1500, theme: 'growl-warning', header: '' });
            }
        }
    });
}

function MiHojaVida(){
    localStorage.setItem('EmpleadoSeleccionado', localStorage.getItem('idempleado'))
    LlamarOpcionMenu('RRHH/HojaVida', 2, 'ul_rrhh',1);
}

function MiHojaVida() {
    localStorage.setItem('EmpleadoSeleccionado', localStorage.getItem('idempleado'))
    LlamarOpcionMenu('RRHH/HojaVida', 2, 'ul_rrhh', 1);
}

function misDocumentos() {
    var empleado = localStorage.getItem('idempleado') * 1;
    var parametros = "Empleado=" + empleado + "&Documento=0";
    var datos = LlamarAjax("Recursohumano","opcion=TablaDocumentosEmpleado&" + parametros);
    var data = JSON.parse(datos);
    var resultado = "";
    for (var x = 0; x < data.length; x++) {
        let descripcion = (data[x].descripcion !== undefined ? (data[x].descripcion) : descripcion);
        resultado += "<tr " + (data[x].obligatorio * 1 == 1 ? "class='bg-rojoclaro' " : "") + ">" +
            "<td>" + (data[x].nrodocumento * 1 > 0 ? data[x].nrodocumento : '') + "</td>" +
            "<td>" + ((data[x].nrodocumento * 1 == 0) ? "<strong>" + descripcion + "</strong>" : "     - Documento-" + data[x].nrodocumento * 1) + "</td>" +
            "<td>" +
            ((data[x].nrodocumento * 1 == 0) ? "" : "<button type = 'button' class='btn btn-glow btn-info btn-icon2' style = 'max-width:50px; padding-bottom: 3px;' title = 'Descargar PDF' onclick = 'javascript:DescargarPDFMenu(" + (data[x].nrodocumento * 1 > 0 ? descripcion : '') + ",\"" + (data[x].nrodocumento * 1 > 0 ? data[x].id + "-" + data[x].nrodocumento : '') + "\")'><span style='font-size:18px; ' data-icon='&#xe22d;'></span></button >") +
            "</td > " +
            "</tr>";
    }
    $("#TablaDocumentosMenu").html(resultado);
    $("#ModalDocumentosCliente").modal("show");

}

function DescargarPDFMenu(empleado, id) {
    window.open(url_archivo + "Adjunto/DocumentosEmpleados/" + empleado + "/" + id + ".pdf");
}


function ManualAyuda(opcion) {
    switch (opcion) {
        case 1:
            window.open(url_archivo + "Manuales/Logistica/Ingreso.html");
            break;
        case 2:
            window.open(url_archivo + "Manuales/Comercial/Cotizaciones.html");
            break;
    }
}

function ActivarLoad() {
    return false;
}

function DesactivarLoad() {
    return false;
}

function DatosEmpresa() {
    var datos = LlamarAjax("Configuracion","opcion=DatosEmpresa");
    var data = JSON.parse(datos);
    _CD = data[0].caracter_decimal_moneda;
    _CM = data[0].caracter_miles_moneda;
    _SM = data[0].simbolo_moneda;
    _DE = data[0].decimales*1;
}

function EnviarFelicitacion(idcumple, x){
	var mensaje = $.trim($("#MensajeCumpleanos"+ x).val());
	var empleado = idcumple;
	var datos = LlamarAjax("Seguridad", "opcion=UsuarioEmpleado&empleado=" + empleado) * 1;
	if (datos == 0) {
		swal("Acción Cancelada", "Este empleado no posee un usuario de acceso en el sistema", "warning");
		return false;
	}
	var idusuario = "/" + datos + "/";

	if (mensaje == "") {
		$("#MensajeCumpleanos" + x).focus();
		swal("Acción Cancelada", "Debe de ingresar un mensaje de felicitaciones", "warning");
		return false;
	}
	sendMessage(3,mensaje,idusuario);
	swal("", "Mensaje enviado con éxito", "success");
}


/*function RecordatorioEvento() {
    var recordatorio = $("#ProximaRecordada").val() * 1;
    var datos = LlamarAjax("Configuracion/GuardarRecordatorio", "recordatorio=" + recordatorio).split("||");

}*/

function RecordatorioEvento() {
    var fecha = new Date();
    var month = fecha.getMonth() + 1;
    var day = fecha.getDate();
    var Anio = fecha.getFullYear();
    var mensaje = 0;
    var FechaActual = Anio + '-' +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day;
    var datos = LlamarAjax("Configuracion","opcion=EventosCalendarios&fechad=" + FechaActual + "&fechah=" + FechaActual + "&cliente=0&recordatoria=1").split("||");
    var resultado = "";

    data = JSON.parse(datos[0]);
    for (var x = 0; x < data.length; x++) {
        mensaje = 1;
        resultado += "<tr>" +
            "<td><label class='bg-success'>Cumpleaños</label</td>" +
            "<td>" + data[x].fechacumple + "/" + Anio + "</td>" +
            "<td>" + data[x].fechacumple + "/" + Anio + "</td>" +
            "<td>" + data[x].evento + "</td>" +
            "<td><label>Mensaje de Felicitaciones</label><br><textarea placeholder='Enviar un mensaje de felicitaciones a tu compañero' id='MensajeCumpleanos" + x + "' rows='3' class='form-control'></textarea><br><button class='btn btn-success' type='button' onclick='EnviarFelicitacion(" + data[x].id + "," + x + ")'>Enviar Felicitaciones</button></td>" +
            "<td>RR-HH</td></tr>";
    }

    data = JSON.parse(datos[2]);
    for (var x = 0; x < data.length; x++) {
        mensaje = 1;
        resultado += "<tr>" +
            "<td><label class='bg-danger'>Festivos</label</td>" +
            "<td>" + data[x].fecha + "</td>" +
            "<td>" + data[x].fecha + "</td>" +
            "<td>" + data[x].evento + "</td>" +
            "<td>&nbsp;</td>" +
            "<td>&nbsp;</td></tr>";
    }

    data = JSON.parse(datos[3]);
    for (var x = 0; x < data.length; x++) {
        mensaje = 1;
        resultado += "<tr>" +
            "<td><label class='" + (data[x].tipo == "Evento" ? "bg-info" : "bg-primary") + "'>" + data[x].tipo + "</label</td>" +
            "<td>" + data[x].fecharecordatorio + "</td>" +
            "<td>" + CambiarCaracter(data[x].fechainicio, "-", "/") + (data[x].horainicio != "00:00" ? " " + data[x].horainicio : "") + "<br>" + CambiarCaracter(data[x].fechafinal, "-", "/") + (data[x].horafinal != "" ? " " + data[x].horafinal : "") + "</td>" +
            "<td>" + data[x].evento + "</td>" +
            "<td><b>" + data[x].cliente  + "</b><br>" + data[x].descripcion + "</td>" +
            "<td>" + data[x].registro + "</td></tr>";
    }



    if (mensaje == 1) {
        EscucharMensaje("Tiene Recordatorios de eventos ó actividades");
        $("#TBRecordatorio").html(resultado);
        $("#ModalRecordatorio").modal("show");
    }
}

function IPLocal() {
    var myIP = "Bloqueado";
    window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;   //compatibility for firefox and chrome
    var pc = new RTCPeerConnection({ iceServers: [{ urls: 'stun:stun.l.google.com:19302' }] }), noop = function () { };
    pc.createDataChannel("");    //create a bogus data channel
    pc.createOffer(pc.setLocalDescription.bind(pc), noop);    // create offer and set local description
    pc.onicecandidate = function (ice) {  //listen for candidate events
        if (!ice || !ice.candidate || !ice.candidate.candidate) {
            $.jGrowl("Sin conexión a internet", { life: 3500, theme: 'growl-warning', header: '' });
            myIP = "127.0.0.1";
            LlamarAjax("InicioSesion", "opcion=GuardarIP&iplocal=" + myIP);
			localStorage.setItem("myIP", myIP);
            return false;
        }
        var arreglo = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(ice.candidate.candidate);
        if (arreglo) {
            if (arreglo.length > 1) {
                if (myIP == "Bloqueado") {
                    myIP = arreglo[1];
                    if ($.trim(myIP) != ""){
						LlamarAjax("InicioSesion", "opcion=GuardarIP&iplocal=" + myIP);
					}
                }
            }
        }
        localStorage.setItem("myIP", myIP);
        if (myIP == "Bloqueado") {
            swal("Acción Cancelada", "Active la IP local del navegador, en el navegador", "warning");
            return false;
        }
        pc.onicecandidate = noop;
    };
    
}

function ValidarSesion() {
    IPLocal();
    setTimeout(function () {
        LlamarAjax("Base/ValidadarInicioIP", "");
    }, 3000);
}

IPLocal();


function TiempoSesion() {
    if ((AparatoMovil == 0) && (localStorage.getItem("tiempo_sesion") * 1 == 1)) {
        var MinSesion = localStorage.getItem("MinSesion") * 1;
        var SegSesion = localStorage.getItem("SegSesion") * 1;


        if (SegSesion <= 0) {
            SegSesion = 55;
            MinSesion -= 1;
        } else {
            SegSesion -= 5;
            if (MinSesion == 0 && SegSesion == 0)
                BloquearPantalla();
        }

        localStorage.setItem("MinSesion", MinSesion);
        localStorage.setItem("SegSesion", SegSesion);

        $("#ContadorSesion").html("[" + (MinSesion < 10 ? "0" : "") + MinSesion + ":" + (SegSesion < 10 ? "0" : "") + SegSesion + "]&nbsp;&nbsp;");
    }
}

function myMoveFunction() {
    if (localStorage.getItem("tiempo_sesion") * 1 == 1) {
        localStorage.setItem("MinSesion", 20);
        localStorage.setItem("SegSesion", 0);

        MinSesion = 20;
        SegSesion = 0;

        $("#ContadorSesion").html("[" + (MinSesion < 10 ? "0" : "") + MinSesion + ":" + (SegSesion < 10 ? "0" : "") + SegSesion + "]&nbsp;&nbsp;");
    }
}

$(document).on('hidden.bs.modal', function (event) {
    if ($('.modal:visible').length) {
        $('body').addClass('modal-open');
    }
});

function abrirModalSolicitud() {
    try {

        $("#ModalMesaTrabajo").modal("show");
    } catch (error) {
        console.log("Error en la funcion [abrirModalSolicitud]: " + error);
    }
}

function abrirModalCerrarSolicitud() {
    try {
        var combo = CargarCombo(82, 0, "", "");
        if (combo == "<option value''>SIN RESULTADOS</option>") {
            swal("Acción Cancelada", "Usted no posee reportes de mesas de trabajos respondida sin cerrar", "warning");
            return false;
        }
        $("#actividadesCerrar").html(combo);
        $("#ModalCerrarMesaTrabajo").modal("show");
    } catch (error) {
        console.log("Error en la funcion [ModalCerrarMesaTrabajo]: " + error);
    }
}




function mostrarSeguimiento(identificacion) {
    try {
        var iden_mesa = identificacion;
        if (iden_mesa !== undefined && String(iden_mesa) !== 'NaN' && iden_mesa !== "SIN RESULTADOS") {
            var consultaSeguimiento = LlamarAjax("Quejas/SeguimientoMesa", "identificacion=" + iden_mesa).split("|");
            if (consultaSeguimiento[1] !== '[]') {
                var json_consultaSeguimiento = JSON.parse(consultaSeguimiento[1]);
                var tabla = '';
                for (var i = 0; i < json_consultaSeguimiento.length; i++) {
                    if (json_consultaSeguimiento[i].status_mesa == 0) {
                        tabla += '<tr class="">' +
                            '<td><span><strong>' + json_consultaSeguimiento[i].control_act_mesa + '/' + json_consultaSeguimiento[i].caso_act_mesa + '</strong></span></td>' +
                            '<td><span>' + json_consultaSeguimiento[i].tipo_mesa + ', ' + json_consultaSeguimiento[i].prio_mesa + '<br >' + json_consultaSeguimiento[i].fe_ing_mesa + ', ' + json_consultaSeguimiento[i].usr_reg_mesa + ', ' + json_consultaSeguimiento[i].departamento_dest + '<br >' + json_consultaSeguimiento[i].detalle_mesa + ' </span></td>' +
                            '<td><span> Aun no se ha recibido respuesta de este tema </span></td>' +
                            '<td><span> Aun no se ha cerrado este tema </span></td>' +
                            '</tr>';
                    }
                    if (json_consultaSeguimiento[i].status_mesa == 5) {
                        tabla += '<tr class="">' +
                            '<td><span><strong>' + json_consultaSeguimiento[i].control_act_mesa + '/' + json_consultaSeguimiento[i].caso_act_mesa + '</strong></span></td>' +
                            '<td><span>' + json_consultaSeguimiento[i].tipo_mesa + ', ' + json_consultaSeguimiento[i].prio_mesa + '<br >' + json_consultaSeguimiento[i].fe_ing_mesa + ', ' + json_consultaSeguimiento[i].usr_reg_mesa + ', ' + json_consultaSeguimiento[i].departamento_dest + '<br >' + json_consultaSeguimiento[i].detalle_mesa + ' </span></td>' +
                            '<td><span>' + (json_consultaSeguimiento[i].tipo_esp_mesa + ', ' + json_consultaSeguimiento[i].prio_esp_mesa + '<br >' + json_consultaSeguimiento[i].fec_res_mesa + ', ' + json_consultaSeguimiento[i].esp_res_mesa + ', ' + json_consultaSeguimiento[i].dpto_res + '<br >' + json_consultaSeguimiento[i].resp_mesa) + '</span></td>' +
                            '<td><span> Aun no se ha cerrado este tema </span></td>' +
                            '</tr>';
                    }
                    if (json_consultaSeguimiento[i].status_mesa == 10) {

                    }
                    if (json_consultaSeguimiento[i].status_mesa == 15) {
                        tabla += '<tr class="">' +
                            '<td><span><strong>' + json_consultaSeguimiento[i].control_act_mesa + '/' + json_consultaSeguimiento[i].caso_act_mesa + '</strong></span></td>' +
                            '<td><span>' + json_consultaSeguimiento[i].tipo_mesa + ', ' + json_consultaSeguimiento[i].prio_mesa + '<br >' + json_consultaSeguimiento[i].fe_ing_mesa + ', ' + json_consultaSeguimiento[i].usr_reg_mesa + ', ' + json_consultaSeguimiento[i].departamento_dest + '<br >' + json_consultaSeguimiento[i].detalle_mesa + ' </span></td>' +
                            '<td><span>' + (json_consultaSeguimiento[i].tipo_esp_mesa + ', ' + json_consultaSeguimiento[i].prio_esp_mesa + '<br >' + json_consultaSeguimiento[i].fec_res_mesa + ', ' + json_consultaSeguimiento[i].esp_res_mesa + ', ' + json_consultaSeguimiento[i].dpto_res + '<br >' + json_consultaSeguimiento[i].resp_mesa) + ' </span></td>' +
                            '<td><span>' + json_consultaSeguimiento[i].usr_cerro + '<br >' + json_consultaSeguimiento[i].eval_mesa + '<br >' + json_consultaSeguimiento[i].fe_cierre_mesa + '<br >' + json_consultaSeguimiento[i].det_cierre_mesa + ' </span></td>' +
                            '</tr>';
                    }
                }
                $("#tabla_seguimiento").html(tabla);
                $("#ModalSeguimiento").modal("show");
            }
        } else {
            swal("Acción Cancelada", "Debe seleccionar una mesa de trabajo para poder ver el registro de seguimiento", "warning");
        }
    } catch (error) {
        console.log("Ha ocurrido un error en [mostrarSeguimiento]: " + error);
    }
}
function PosicionUsuario() {
    if (AparatoMovil == 1) {
        navigator.geolocation.getCurrentPosition(mostrarPosicion, showError); //Obtiene la posición
        function mostrarPosicion(position) {
            lat = position.coords.latitude; //Obtener latitud
            lon = position.coords.longitude; //Obtener longitud
            LlamarAjax("Seguridad/GuardarPosicion", "usuario=" + idusuariochat + "&log=" + lon + "&lat=" + lat);
        }
        function showError(error) {

        }
    }
}

async function SubirFotoFILE(){
	var arrArchivos = document.getElementById("imageData");
	var datosB64 = new FormData();
	if (typeof arrArchivos === 'object' && arrArchivos.files.length > 0) {
		for (var i = 0; i < arrArchivos.files.length; i++) {
			datosB64.append("datosB64", await archivoToBase64(arrArchivos.files[i]));
			datosB64.append("datosTipo", '1');
		}
		$.ajax({
			url: 'http://localhost:8080/backendSisCalibration/image',
			type: 'POST',
			processData: false,
			contentType: false,
			enctype: 'multipart/form-data',
			data: datosB64,
			cache: false,
			async: false,
			beforeSend: function () {
				$('#image_frame').html('<h1>Subiendo archivos</h1>');
			},
			success: function (response) {
				$('#mensaje').text(response);
			}
		});
	}
}

const archivoToBase64 = file => new Promise((resolve, reject) => {
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => resolve(reader.result);
		reader.onerror = error => reject(error);
	});
        

function GuardarDocumentoAdjunto(id,tipo,contenedor, tipo_adjunto) {
	
		
	var sampleFile;
	var formdata = new FormData();
	var cantidad = 0;
	var resultado = "";
	if (!tipo_adjunto)
		tipo_adjunto = 0;
	
	for (var x = 0; x< document.getElementById(id).files.length; x++){
		formdata.append("id", id + x);
		formdata.append("tipo", tipo_adjunto);
		sampleFile = document.getElementById(id).files[x];
		formdata.append(id + x, sampleFile);
		cantidad++;
	}
	
	if (cantidad == 0)
		return false;
		
	$.ajax({
		type: "POST",
		enctype: 'multipart/form-data',
		url: url_servidor + "AdjuntarArchivo",
		data: formdata,
		processData: false,
		contentType: false,
		cache: false,
		async: false,
		success: function (data) {
			var datos = $.trim(data).split("||");
			if (datos[0] == "0"){
				switch (tipo){
					case 1: 
						swal("",datos[1],"success");
						$("#" + contenedor).attr("src",url_cliente + "uploads/" + datos[2]);
						break;
					case 4:
						$.jGrowl(datos[1], { life: 2500, theme: 'growl-success', header: '' });
						resultado = $.trim(datos[2]);
						break;
					case 3: 
						$.jGrowl(datos[1], { life: 2500, theme: 'growl-success', header: '' });
						break;
				}
			}else{
				$("#" + id.id).val("");
			}
		},
		error: function (e) {
			var datos = $.trim(e.responseText).split("||");
			swal("Acción Cancelada",datos[1],"warning");
		}
	});
	return resultado;
}



function ComboUsuarioChat(){
	$("#Usuarios_Chat").html(LlamarAjax("Seguridad","opcion=UsuariosChat&Usuario=" + idusuariochat));
}

function ActivarUsuariosChat(){
	LlamarAjax("Seguridad","opcion=ActivarUsuariosChat&Usuario=" + idusuariochat);
	ComboUsuarioChat();
}

function FechaServidor(tipo){
	return LlamarAjax("Configuracion","opcion=FechaServidor&tipo=" + tipo);
}

function VersionDocumento(documento){
	return LlamarAjax("Laboratorio","opcion=VersionDocumento&documento=" + documento);
}

function init_socker() {
	if ("WebSocket" in window) {
		if (userName == null)
			userName = usuariochat;
		websocket = new WebSocket(url_socker + userName);
		websocket.onopen = function (data) {
			console.log(data);
			$.jGrowl("Inicializado sesión en el CHAT interno", { life: 2500, theme: 'growl-success', header: '' });
		};

		websocket.onmessage = function (data) {
			setMessage(JSON.parse(data.data));
		};

		websocket.onerror = function (e) {
			console.log(e);
			$.jGrowl("Ha ocurrido un error, Cierre la aplicación", { life: 2500, theme: 'growl-warning', header: '' });
			
			cleanUp();
		};

		websocket.onclose = function (data) {
			cleanUp();

			var reason = (data.reason && data.reason !== null) ? data.reason : 'Goodbye';
			if (reason == "Goodbye"){
				$.jGrowl("Sesión Cerrada", { life: 4500, theme: 'growl-warning', header: '' });
			}
		};
	} else {
		$.jGrowl("Chat no soportado", { life: 2500, theme: 'growl-warning', header: '' });
	}
}



function cleanUp() {
	userName = null;
	websocket = null;
}

function sendMessage(tipo, mensaje, vista) {
		
	var messageContent = "";
	var fecha = '';
	
	if (!vista)
		vista = "0";
		
	switch (tipo){
		case 1:
			vista = "/";
			$("#Usuarios_Chat option:selected").each(function () {
				if ($(this).attr('value')*1 > 0)
					vista += $(this).attr('value') + "/";
			});

			if (vista == "/")
				vista = "0";
							
			messageContent = $.trim($('#message_chat').val());
			if (messageContent == "")
				return false;
				
			fecha = FechaServidor(4);
		break;
		case 3:
		case 4:
			fecha = FechaServidor(4);
			messageContent = mensaje;
			break;
		case 6:
			messageContent = mensaje;
			break;
			
	}
	
	var message = buildMessage(userName, messageContent, tipo, vista, fecha);

	websocket.send(JSON.stringify(message));
	
	if (tipo == 1){
		$('#message_chat').val("");
		$.jGrowl("Mensaje enviado", { life: 2500, theme: 'growl-success', header: '' });
	}
		
	setMessage(message);
	
}

function buildMessage(userName, message, tipo, vista, fecha) {
	return {
		username: userName,
		message: message,
		tipo: tipo,
		vista: vista,
		fecha: fecha
	};
}

function setMessage(msg) {
	var tipo = msg.tipo * 1;
	switch (tipo){
		case 1:
			MostrarMensaje(msg.username, msg.message, msg.fecha, msg.vista);
			break;
		case 2:
			TablaTemporalIngreso();
			break;
		case 3:
			MostrarMensaje(msg.username, msg.message, msg.fecha, msg.vista);
			break;
		case 4:
			NotificacionesReporte(localStorage.getItem("IngresoSocker"), msg.message);
			LlenarChat();
			break;
		case 5:
			$.jGrowl("Actualizando gestón de cartera", { life: 4500, theme: 'growl-success', header: '' });
			TablaGestionCartera();
		case 6:
			$("#FotoAlumno").attr("src",msg.message);
			break;
		case 100:
			ComboUsuarioChat();
			$.jGrowl(msg.message, { life: 2500, theme: 'growl-success', header: '' });
			break;
		case 101:
			ActivarUsuariosChat();
			$.jGrowl(msg.message, { life: 2500, theme: 'growl-warning', header: '' });
			break;
	}
}

function NumeroAleatorio() {
	var min = 1;
	var max = 1000000;
  return Math.random() * (max - min) + min;
}

function Detectar_Navegador2(){
	console.log(navigator);
	var es_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
	if(es_chrome){
		return "chorme";
	}
	var es_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
	if(es_firefox){
		return "firefox";
	}
	var es_opera = navigator.userAgent.toLowerCase().indexOf('opera');
	if(es_opera){
		return "opera";
	}
	return "no_detectado"
	
	
}

function Detectar_Navegador(){
      var agente = window.navigator.userAgent;
      var navegadores = ["Chrome", "Firefox", "Safari", "Opera", "Trident", "MSIE", "Edge"];
      for(var i in navegadores){
          if(agente.indexOf( navegadores[i]) != -1 ){
              return navegadores[i];
          }
      }
  } 

function Abrir_WebRTC(){
	var navegador = Detectar_Navegador();
	if (ruta == "no_detectado"){
		swal("Acción Cancelada","Navegador no detectado","warning");
		return;
	}
	var ruta = "";
	switch (navegador){
		case "Firefox": 
			ruta = "https://addons.mozilla.org/es/firefox/addon/happy-bonobo-disable-webrtc/";
			break;
		case "Opera": 
			ruta = "https://addons.opera.com/es-419/extensions/details/webrtc-control/";
			break;
		case "Chrome":
			ruta = "https://chrome.google.com/webstore/detail/webrtc-network-limiter/npeicpdbkakmehahjeeohfdhnlpdklia/related?hl=en-US";
			break;
	}
	window.open(ruta);
}

function _escape(texto){
	var caracteres = ['Á','á','É','é','Í','í','Ó','ó','Ú', 'ú', 'Ü','ü','Ṅ','ñ','&','<','>','“','‘','©','®','€','°','²','³','µ','¼','½','¾','π','%'];
	var cambios = ['u00C1','u00E1','u00C9','u00E9','u00CD','u00ED','u00D3','u00F3','u00DA', 'u00FA', 'u00DC','u00FC','u00D1','u00F1','u0022','u003C','u003E','u0022','u0027','u00A9','u00AE','u20AC','u00B0','u00B2','u00B3','u00B5','u00BC','u00BD','u00BE','u03C0','u0025'];
	var valorCaja = "";
	var encontrado = 0;
	for (var i = 0; i < texto.length; i++) {
		encontrado = 0;
		for (var x=0; x<caracteres.length; x++){
			
			if (texto.charAt(i) == caracteres[x]){
				console.log(texto.charAt(i) + " == " + caracteres[x]);
				valorCaja += cambios[x];
				console.log(valorCaja);
				encontrado = 1;
				break;
            }
        }
        if (encontrado == 0){
			valorCaja += texto.charAt(i);
		}
	}
	return valorCaja;
	
}

function InicializarFormulario(ul, menu){
	$('select').select2();
	DesactivarLoad();
	$(".navigation > li").removeClass("active")
	$("#" + menu).addClass("active")
	$("body").addClass("sidebar-narrow");
	$("#" + ul).css("display", "");
	TraducirIdioma(1);
}
	
