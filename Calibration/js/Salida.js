﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);


var datos = LlamarAjax("Configuracion","opcion=CargaComboInicial&"+ "tipo=4").split("||");

$("#CMagnitud").html(datos[0]);
$("#CEquipo").html(datos[1]);
$("#CMarca").html(datos[2]);
$("#CCliente").html(datos[3]);
$("#CProveedor").html(datos[4]);
$("#CModelo").html(datos[5]);
$("#CIntervalo").html(datos[6]);
$("#CUsuario").html(datos[7]);
$("#Motivo, #CMotivo").html(datos[8]);

var IdProveedor = 0;
var DocumentoPro = "";
var TotalSeleccion = 0;
var Salida = 0;
var totalitems = 0;
var Estado = "Temporal";
var Orden = 0;
var AutoDireccion = [];

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function CargarOIngreso() {
    var cliente = $("#Cliente").val() * 1;
    if (cliente == 0)
        return false;
    localStorage.setItem("idcliente", cliente);
    CargarModalIngreso(1, "orden");
    $("#modalIngresoOrden").modal("show");
}

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Cliente") {
            if ($.trim($(this).val()) != "") {

                BuscarTercero($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function SeleccionarTodo() {

    if ($("#seleccionatodo").prop("checked")) {
        for (var x = 0; x < totalitems; x++) {
            $("#fila-" + x).addClass("bg-gris");
            $("#seleccionado" + x).prop("checked", "checked");
        }
    } else {
        for (var x = 0; x < totalitems; x++) {
            $("#fila-" + x).removeClass("bg-gris");
            $("#seleccionado" + x).prop("checked", "");
        }
    }
    
}

function SeleccionarFila(x) {

    if ($("#seleccionado" + x).prop("checked")) {
        $("#fila-" + x).addClass("bg-gris");
    } else {
        $("#fila-" + x).removeClass("bg-gris");
    }

}


function ValidarProveedor(documento) {
    if ($.trim(DocumentoPro) != "") {
        if (DocumentoPro != documento) {
            DocumentoPro = "";
            LimpiarDatos();
        }
    }
}

function BuscarProveedor(documento) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoPro == documento)
        return false;

    DocumentoPro = documento;

    var parametros = "documento=" + $.trim(documento);
    var datos = LlamarAjax("Facturacion","opcion=BuscarProveedor&"+ parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        var data = JSON.parse(datos[1]);
        DesactivarLoad();
        IdProveedor = data[0].id;
        $("#NombreProveedor").val(data[0].nombrecompleto);
        $("#Ciudad").val(data[0].ciudad + "/" + data[0].departamento);
        $("#Direccion").val(data[0].direccion);
        $("#Email").val(data[0].email);
        $("#Telefonos").val(data[0].telefono + " / " + data[0].celular);
        $("#Cliente").html(datos[2]);
    } else {
        DocumentoPro = "";
        $("#Proveedor").val("");
        $("#Proveedor").focus();
        swal("Acción Cancelada", "El documento " + documento + ", no se encuentra registrado como proveedor", "warning");
    }

}

$("#tablamodalproveedore > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    $("#modalProveedores").modal("hide");
    $("#Proveedor").val(numero);
    BuscarProveedor(numero);
});

function LimpiarTodo() {
    LimpiarDatos();
    $("#Proveedor").val("");
    $("#Proveedor").focus();
}


function LimpiarDatos() {

    IdProveedor = 0;
    DocumentoPro = "";
    Orden = 0;
    Salida = 0;
    Orden = 0;
    totalitems = 0;
    Estado = "Temporal";

    $("#Estado").val(Estado);
    $("#NombreProveedor").val("");
    $("#Orden").val("");
    $("#Salida").val("");
    $("#Direccion").val("");
        
    $("#Salida").attr("disabled", false);

    $("#Ciudad").val("");
    
    $("#Email").val("");
    $("#Telefonos").val("");
    
    $("#bodySalida").html("");
    totales = 0;
    subtotal = 0;
    descuento = 0;
    iva = 0;
    gravable = 0;

    $("#Observacion").val("");
    $("#Motivo").val("").trigger("change");
    $("#Cliente").val("").trigger("change");
    
    $("#FechaSalida").val("");
    $("#UsuarioSalida").val("");
    $("#FechaRetorno").val("");
    $("#UsuarioRetorno").val("");
    $("#EquipoRetorno").val("0");

    $("#NombreCer").val("");
    $("#DireccionCer").val("");

    $("#SaldoClienteDev").html("");
    $("#divAnulaciones").html("");

}

function TablaSalida(cliente) {
    cliente = cliente * 1;
    if (IdProveedor == 0 && Orden == 0) 
        return false;
    if (Salida > 0)
        return false;

    var proveedor = 0;
    var resultado = "";
    totalitems = 0;

    if (cliente == 0 && Orden == 0) {
        $("#bodySalida").html("");
        return false;
    }

    $("#NombreCer").val($("#Cliente option:selected").text());

    $("#bodySalida").html("");
    var datos = LlamarAjax("Salida","opcion=TablaSalidaDetalle&Cliente=" + cliente + "&Orden=" + Orden + "&Proveedor=" + IdProveedor);

    if (datos != "[]") {
        eval("data=" + datos);
        for (var x = 0; x < data.length; x++) {
            totalitems += 1;
            resultado += "<tr id='fila-" + x + "'>" +
                "<td class='text-center'><b>" + (x + 1) + "</b><br><input class='form-control' type='checkbox' value='" + data[x].ingreso + "' name='Ingresos[]' id='seleccionado" + x + "' onchange='SeleccionarFila(" + x + ")'></td>" +
                "<td><b>" + data[x].ingreso + "</b><br>" + data[x].fechaing + "<br>" + data[x].usuarioing + "</td>" +
                "<td>" + data[x].equipo + "</td>" +
                "<td>" + data[x].marca + "</td>" +
                "<td>" + data[x].modelo + "</td>" +
                "<td>" + data[x].intervalo + "</td>" +
                "<td>" + data[x].serie + "</td>" +
                "<td><div name='Ordenes[]'>" + data[x].orden + "</div></td>" +
                "<td><textarea style='width:100%' rows='6' name='Accesorios[]'>" + (data[x].accesorio ? data[x].accesorio : "") + "</textarea></td>" +
                "<td>&nbsp;</td>" +
                "<td>&nbsp;</td>" +
                "<td align='center'><button class='verfotos btn btn-success' title='Ver Fotos' type='button' onclick='CargarModalFoto(" + data[x].ingreso + "," + data[x].fotos + ",1)'><span data-icon='&#xe2c7;'></span></button></td></tr>";
        }

    } else {
        if (Orden > 0) {
            swal("Acción Cancelada", "La órden de tercerizado número " + Orden + ", no posee equipos pendientes por tercerizar", "warning");
            Orden = 0;
            return false;
        }
    }
    cliente = cliente * 1;
    AutoCompletar(cliente);
    SaldoActual(cliente, 1);
    $("#SaldoClienteDev").html(SaldoTotal(cliente));
    Orden = 0;
    $("#bodySalida").html(resultado);
}

function GuardarSalida() {

    if (IdProveedor == 0)
        return false

    if (Salida > 0) {
        swal("Acción Cancelada", "Para editar esta salida debe anularla", "warning");
        return false;
    }

    var cliente = $("#Cliente").val() * 1;
    var motivo = $("#Motivo").val() * 1;
    var observacion = $.trim($("#Observacion").val());
    var nombrecer = $.trim($("#NombreCer").val());
    var direccioncer = $.trim($("#DireccionCer").val());
    var fechasal = $("#FechaSalida").val();

    if (fechasal < output2) {
        swal("Acción Cancelada", "La fecha debe ser mayor o igual a " + output2, "warning");
        return false;
    }
        
    if (cliente == 0) {
        $("#Cliente").focus();
        swal("Acción Cancelada", "Debe seleccionar un cliente", "warning");
        return false;
    }
    if (motivo == 0) {
        $("#Motivo").focus();
        swal("Acción Cancelada", "Debe seleccionar el motivo de la salida", "warning");
        return false;
    }

    if (nombrecer == "") {
        $("#NombreCer").focus();
        swal("Acción Cancelada", "Debe ingresar a nombre de quién va el certificado", "warning");
        return false;
    }

    if (direccioncer == "") {
        $("#DireccionCer").focus();
        swal("Acción Cancelada", "Debe ingresar la dirección del certificado", "warning");
        return false;
    }

    if (fechasal == "") {
        $("#FechaSalida").focus();
        swal("Acción Cancelada", "Debe ingresar la fecha de salida", "warning");
        return false;
    }
        
    var seleccionado = 0;
    var ingresos = document.getElementsByName("Ingresos[]");
    var accesorio = document.getElementsByName("Accesorios[]");
    var ordenes = document.getElementsByName("Ordenes[]")
    
    var a_ingreso = "array[";
    var a_accesorio = "array[";
    var a_orden = "array[";
    
    for (var x = 0; x < ingresos.length; x++) {
        if (ingresos[x].checked) {
            if (a_ingreso == "array[") {
                a_ingreso += ingresos[x].value;
                a_accesorio += "'" + accesorio[x].value + "'";
                a_orden += "'" + $.trim(ordenes[x].innerHTML) + "'";
            } else {
                a_ingreso += "," + ingresos[x].value;
                a_accesorio += ",'" + accesorio[x].value + "'";
                a_orden += ",'" + $.trim(ordenes[x].innerHTML) + "'";
            }
            seleccionado += 1;
        }
    }
    a_ingreso += "]";
    a_accesorio += "]";
    a_orden += "]";
    
    if (seleccionado == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un ingreso", "warning");
        return false;
    }

    var parametros = "proveedor=" + IdProveedor + "&cliente=" + cliente + "&motivo=" + motivo + "&observacion=" + observacion + "&a_ingreso=" + a_ingreso + "&a_accesorio=" + a_accesorio + "&a_orden=" + a_orden + "&fechasal=" +
        fechasal + "&direccioncer=" + direccioncer + "&nombrecer=" + nombrecer;
    var datos = LlamarAjax("Salida","opcion=GuardarSalida&"+ parametros).split("|");
    if (datos[0] == "0") {
        swal("", datos[1] + " " + datos[2], "success");
        BuscarSalida(datos[2]);
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

}

function BuscarOrdenSal(orden) {
    if (orden == Orden)
        return false;
    LimpiarDatos();
    Orden = orden;
    var datos = LlamarAjax("Salida","opcion=BuscarOrdenSalida&"+ "Orden=" + Orden);
    if (datos == "[]") {
        $("#Orden").val("");
        Orden = 0;
        $("#Orden").focus();
        swal("Acción Cancelada", "Esta orden no se encuentra registrada en el sistema", "warning");
    } else {
        DocumentoPro = "";
        var data = JSON.parse(datos);
        $("#Proveedor").val(data[0].documento);
        $("#Orden").val(Orden);
        BuscarProveedor(data[0].documento);
        $("#Cliente").val(data[0].idcliente).trigger("change");
    }
}

function BuscarSalida(salida) {
    if (Salida == salida)
        return false;
    LimpiarDatos();
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Salida","opcion=BuscarSalida&"+ "Salida=" + salida);
        DesactivarLoad();
        if (datos != "[]") {
            Salida = salida;
            $("#Salida").val(Salida);
            $("#Salida").attr("disabled", true);
            var data = JSON.parse(datos);
            var proveedor = data[0].proveedor;
            var cliente = data[0].idcliente;
            $("#Proveedor").val(proveedor);
            DocumentoPro = "";
            BuscarProveedor(proveedor);
            $("#Cliente").val(cliente).trigger("change");
            var resultado = "";
            totalitems = 0;
            for (var x = 0; x < data.length; x++) {
                resultado += "<tr id='fila-" + x + "'>" +
                    "<td class='text-center'><b>" + (x + 1) + "</b></td>" +
                    "<td><b>" + data[x].ingreso + "</b><br>" + data[x].fechaing + "<br>" + data[x].usuarioing + "</td>" +
                    "<td>" + data[x].equipo + "</td>" +
                    "<td>" + data[x].marca + "</td>" +
                    "<td>" + data[x].modelo + "</td>" +
                    "<td>" + data[x].intervalo + "</td>" +
                    "<td>" + data[x].serie + "</td>" +
                    "<td>" + data[x].orden + "</td>" +
                    "<td>" + data[x].accesorio + "</td>" +
                    "<td>" + (data[x].retorno ? data[x].retorno : "") + "</td>" +
                    "<td>" + (data[x].observacionretorno ? data[x].observacionretorno : "") + "</td>" +
                    "<td align='center'><button class='verfotos btn btn-success' title='Ver Fotos' type='button' onclick='CargarModalFoto(" + data[x].ingreso + "," + data[x].fotos + ",1)'><span data-icon='&#xe2c7;'></span></button></td></tr>";
            }

            SaldoActual(cliente, 1);
            $("#SaldoClienteDev").html(SaldoTotal(cliente));
            $("#bodySalida").html(resultado);

            $("#Observacion").val(data[0].observacion);
            $("#Motivo").val(data[0].idmotivo).trigger("change");

            $("#FechaSalida").val(data[0].fechasal);
            $("#UsuarioSalida").val(data[0].usuariosal);
            $("#FechaRetorno").val(data[0].fecharet);
            $("#UsuarioRetorno").val(data[0].usuarioret);
            $("#EquipoRetorno").val(data[0].equiporet);

            $("#NombreCer").val(data[0].nombrecer);
            $("#DireccionCer").val(data[0].direccioncer);

            Estado = data[0].estado;
            $("#Estado").val(Estado);

            if (data[0].fechaanula) {
                Anulacion = "<span class='text-XX text-danger'>Devolución Anulada el día: " + data[0].fechaanula + ", por el <b>Asesor</b>: " + data[0].usuarioanula + ", <b>Observación:</b> " + data[0].observacionanula + "</span>";
                $("#divAnulaciones").html(Anulacion);
            }

        } else {
            $("#Salida").val("");
            $("#Salida").focus();
            Salida = 0;
            swal("Acción Cancelada", "La salida número " + salida + ", no se encuentra registrada en el sistema", "warning");
        }
    }, 15);
}

function AnularSalida() {

    if (Salida == 0)
        return false;

    if (Estado != "Salida") {
        swal("Acción Cancelada", "No se puede anular una salida con estado " + Estado, "warning");
        return false;
    }

    var resultado = "";
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Desea Anular la Salida') + ' ' + Salida + ' ' + ValidarTraduccion('del cliente') + ' ' + $("#Cliente option:selected").text(),
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Anular'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Salida","opcion=AnularSalida&"+ "salida=" + Salida + "&observaciones=" + value)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resultado = Salida;
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar una observación'));
                }
            })
        }
    }).then(function (result) {
        LimpiarTodo();
        swal({
            type: 'success',
            html: 'Salida Nro ' + resultado + ' anulada con éxito'
        })
    })

}

function ImprimirSalida(numero) {
    if (numero * 1 == 0)
        numero = Salida;
    if (numero * 1 != 0) {
        ActivarLoad();
        setTimeout(function () {
            var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=Salida&documento=" + numero).split("||");
            DesactivarLoad();
            if (datos[0] == "0") {
                window.open(url_archivo + "DocumPDF/" + datos[1]);
            } else {
                swal("", ValidarTraduccion(datos[1]), "warning");
            }
        }, 15);
    }
}

function SeleccionarRetorno(x) {

    if ($("#seleccionadoret" + x).prop("checked")) {
        $("#filaRet-" + x).addClass("bg-gris");
    } else {
        $("#filaRet-" + x).removeClass("bg-gris");
    }

}

function RecibirEquipo() {
    if (Salida == 0)
        return false;

    if (Estado != "Salida") {
        swal("Acción Cancelada", "No se puede recibir equipos con la salida en estado " + Estado, "warning");
        return false;
    }
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Salida","opcion=TablaRetornar&"+ "Salida=" + Salida);
        DesactivarLoad();
        if (datos != "[]") {
            var data = JSON.parse(datos);
            var resultado = "";
            for (var x = 0; x < data.length; x++) {
                resultado += "<tr id='filaRet-" + x + "'>" +
                    "<td class='text-center'><b>" + (x + 1) + "</b><br><input class='form-control' style='width:60px;' type='checkbox' value='" + data[x].ingreso + "' name='IngresosRet[]' id='seleccionadoret" + x + "' onchange='SeleccionarRetorno(" + x + ")'></td>" +
                    "<td>" + data[x].ingreso + "</td>" +
                    "<td>" + data[x].equipo + "</td>" +
                    "<td>" + data[x].marca + "<br>" + data[x].modelo + "</td>" +
                    "<td>" + data[x].intervalo + "<br>" + data[x].serie + "</td>" +
                    "<td>" + data[x].accesorio + "</td>" +
                    "<td><textarea class='form-control' name='ObservaRetor[]'></textarea></td>" +
                    "<td align='center'><button class='verfotos btn btn-success' title='Ver Fotos' type='button' onclick='CargarModalFoto(" + data[x].ingreso + "," + data[x].fotos + ",1)'><span data-icon='&#xe2c7;'></span></button></td></tr>";
            }

            $("#TBRetorno").html(resultado);

            $("#DetSalida").html("Retornar equipos de la salida número " + Salida + ", " + $("#Cliente option:selected").text());
            $("#modalRetornarEquipo").modal("show");
        } else {
            swal("Acción Cancelada", "Esta salida no posee equipos pendientes por entregar", "warning");
        }
    }, 15);
}

function RetornarEquipo() {

    var seleccionado = 0;
    var ingresos = document.getElementsByName("IngresosRet[]");
    var observacion = document.getElementsByName("ObservaRetor[]");
    
    var a_ingreso = "";
    var a_observacion = "";
    
    for (var x = 0; x < ingresos.length; x++) {
        if (ingresos[x].checked) {
            if (a_ingreso == "") {
                a_ingreso += ingresos[x].value;
                a_observacion += "'" + observacion[x].value + "'";
            } else {
                a_ingreso += "|" + ingresos[x].value;
                a_observacion += "|'" + observacion[x].value + "'";
            }
            seleccionado += 1;
        }
    }

    if (seleccionado == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un ingreso", "warning");
        return false;
    }

    var parametros = "salida=" + Salida + "&a_ingreso=" + a_ingreso + "&a_observacion=" + a_observacion;
    var datos = LlamarAjax("Salida","opcion=RetornarEquipo&"+ parametros).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        $("#modalRetornarEquipo").modal("hide");
        var salida = Salida;
        LimpiarTodo();
        BuscarSalida(salida);
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function AutoCompletar(cliente) {
    var datos = LlamarAjax("Solicitud","opcion=DatosClientesCompletar&cliente=" + cliente).split("|");

    AutoDireccion.splice(0, AutoDireccion.length);
    var data1 = JSON.parse(datos[1]);
    for (var x = 0; x < data1.length; x++) {
        AutoDireccion.push(data1[x].direccion);
    }
}

$("#DireccionCer").autocomplete({ source: AutoDireccion, minLength: 0 }).focus(function () {
    $(this).data("uiAutocomplete").search($(this).val());
});

function ConsularSalida() {


    var magnitud = $("#CMagnitud").val() * 1;
    var equipo = $("#CEquipo").val();
    var modelo = $("#CModelo").val();
    var marca = $("#CMarca").val() * 1;
    var intervalo = $("#CIntervalo").val();
    var ingreso = $("#CIngreso").val() * 1;
    var serie = $.trim($("#CSerie").val());
        
    var salida = $("#CSalida").val() * 1;
    var orden = $("#COrden").val() * 1;
    var cliente = $("#CCliente").val() * 1;
    var proveedor = $("#CProveedor").val() * 1;
    var usuario = $("#CUsuario").val() * 1;

    var estado = $("#CEstado").val();
    var fechad = $("#CFechaDesde").val();
    var fechah = $("#CFechaHasta").val();
    var retornado = $("#CRetorno").val();

    if (fechad == "" && fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar una fecha o números de días"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "salida=" + salida + "&orden=" + orden + "&cliente=" + cliente + "&proveedor=" + proveedor + "&ingreso=" + ingreso + "&estado=" + estado +
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&usuario=" + usuario + "&retornado=" + retornado;
        var datos = LlamarAjax("Salida","opcion=ConsultarSalida&"+ parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaSalida').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "imprimir" },
                { "data": "salida", "className": "text-XX" },
                { "data": "proveedor" },
                { "data": "cliente" },
                { "data": "motivo" },
                { "data": "estado" },
                { "data": "equipossal" },
                { "data": "fecha" },
                { "data": "equiporet" },
                { "data": "fecharet" },
                { "data": "anulado" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

$("#TablaSalida > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var salida = row[1].innerText;
    $("#Salida").val(salida);
    $('#tabsalida a[href="#tabcrear"]').tab('show')
    BuscarSalida(salida);
});

function CargarOIngreso() {
    var cliente = $("#Cliente").val() * 1;
    if (cliente == 0)
        return false;
    localStorage.setItem("idcliente", cliente);
    CargarModalIngreso(7, "salida");
    $("#modalIngresoSalida").modal("show");
}

$("#tablamodalingresosalida > tbody").on("dblclick", "tr", function (e) {

    if (Salida > 0 || IdProveedor == 0)
        return false;
    
    var cliente = $("#Cliente").val() * 1;
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    var serie = row[4].innerText;
    var a_descripcion = $.trim(row[3].innerText).split("\n");
    var descripcion = "CALIBRACION DE " + a_descripcion[1] + ", MARCA:  " + a_descripcion[2] + ", MODELO: " + a_descripcion[3] +
        ", INTERVALO: " + a_descripcion[4] + ", SERIE: " + serie + ", INGRESO: " + numero;
    var parametro = "ingreso=" + numero + "&cliente=" + cliente + " &proveedor=" + IdProveedor + " &id=0&descripcion=" + descripcion;
    var datos = LlamarAjax("Salida","opcion=GuardarOtroIngreso&"+ parametro);
    datos = datos.split("|");
    if (datos[0] == "0") {
        $("#modalIngresoSalida").modal("hide");
        Estado = "Temporal";
        $("#Estado").val(Estado);
        swal("", datos[1], "success");
        TablaSalida(cliente)
    } else
        swal("Acción Cancelada", datos[1], "warning");
});


$('select').select2();
DesactivarLoad();
