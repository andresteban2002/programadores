package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.text.DateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Configuration;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/OpcioneAPP"})
public class OpcioneAPP extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";

    //WILMER
    public String GuardarPosicionAPP(HttpServletRequest request) {
        String usuarios = request.getParameter("usuario");
        String latitud = request.getParameter("latitud");
        String longitud = request.getParameter("longitud");

        sql = "INSERT INTO seguridad.posicion(idusuario, log, lat)"
                + "  VALUES((SELECT idusu FROM seguridad.rbac_usuario WHERE nomusu ='" + usuarios + "'),'" + longitud + "','" + latitud + "')";
        if (Globales.DatosAuditoria(sql, "POSICION", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarPosicionAPP")) {

            return "{\"estado\":\"0\",\"mensaje\":\"Ubicación almacenada\"}";
        } else {
            return "{\"estado\":\"1\",\"mensaje\":\"Error inesperado... Revise el log de errores\"}";
        }
    }

    public String BuscarIngreso(HttpServletRequest request) {
        Cotizacion coti = new Cotizacion();
        return coti.BuscarIngreso(request);
    }

    public String SubirFoto(HttpServletRequest request) {
        Configuracion confi = new Configuracion();
        return "{ \"respuesta\": \"" + confi.SubirFoto(request) + "\"}";
    }

    public String BuscarDevolucion(HttpServletRequest request) {
        Cotizacion coti = new Cotizacion();
        return coti.BuscarDevolucion(request);
    }

    public String CargarEmpresas() {

        sql = "SELECT id, empresa from empresa_envio WHERE estado = 1  ORDER BY 2 ";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->CargarEmpresas");
    }

    public String BuscarVueltasPendientes(HttpServletRequest request) {
        String usuarios = request.getParameter("usuario");
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int pendiente = Globales.Validarintnonull(request.getParameter("pendiente"));
        int entregar = Globales.Validarintnonull(request.getParameter("entregar"));

        String busqueda = " where idusuarioasi = (SELECT idusu FROM seguridad.rbac_usuario WHERE nomusu = '" + usuarios + "') and " + (tipo == 1 ? "idcliente = " + cliente : "idproveedor = " + cliente) + (entregar == 0 ? " and tipovuelta<> 'ENTREGAR EQUIPO'" : "");
        if (pendiente == 1) {
            busqueda += " and v.estado = 'ASIGNADA'";
        } else {
            busqueda += " and v.estado = 'EJECUTADA'";
        }

        String sql = "SELECT v.id, v.actividad || ' ' || v.contacto || ' ' || v.direccion || ' ' || v.observacion || ' <b> (' || nomusu || ')</b>' AS actividad,"
                + "   to_char(fechaapro,'yyyy/MM/dd HH24:MI') as fechaapro, fotos, v.estado"
                + "   FROM registrovueltas v inner join seguridad.rbac_usuario u on u.idusu = v.idusuario " + busqueda + " order by fechaapro, v.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarVueltasPendientes");
    }

    public String EntregarDevolucionFoto(HttpServletRequest request) {

        String recibio = request.getParameter("recibio");
        String identificacion = request.getParameter("identificacion");
        String cargo = request.getParameter("cargo");
        String telefonos = request.getParameter("telefonos");
        String observacion = request.getParameter("observacion");
        int devolucion = Globales.Validarintnonull(request.getParameter("devolucion"));
        int empresaenvio = Globales.Validarintnonull(request.getParameter("empresaenvio"));
        String guia = request.getParameter("guia");
        String firma = request.getParameter("firma");
        try {
            sql = "SELECT entregado, fotoentrega FROM devolucion where devolucion = " + devolucion;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->EntregarDevolucionFoto", 1);

            int entregado = Globales.Validarintnonull(datos.getString(1));
            int fotos = Globales.Validarintnonull(datos.getString(2));
            if (entregado > 0) {
                return "{ \"respuesta\": \"1|Esta devolución ya fue entregada\"}";
            }

            sql = "UPDATE devolucion"
                    + "      SET fechaentrega=now(), fotodev = 2, entregado = 1, nombreentrega='MOVIL-CLIENTE', observacionentrega='ENTREGA DESDE LA MOVIL',"
                    + "          empresa_envia = " + empresaenvio + ", guia='" + guia + "', idusuarioentrega=" + idusuario + ", recibio='" + recibio + "', identificacionr='" + identificacion + "', telefonor='" + telefonos + "',cargor='" + cargo + "'"
                    + "       WHERE devolucion = " + devolucion + ";";
            sql += "UPDATE remision_detalle rd set entregado = 1 "
                    + "  FROM devolucion_detalle dd INNER JOIN devolucion d on d.id = dd.iddevolucion"
                    + "  WHERE dd.ingreso = rd.ingreso and d.devolucion = " + devolucion + ";";
            if (Globales.DatosAuditoria(sql, "DEVOLUCION", "ENTREGAR", idusuario, iplocal, this.getClass() + "-->EntregarDevolucionFoto")) {
                return "{ \"respuesta\": \"1|Devolución entregada con éxito\"}";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "{ \"respuesta\": \"1|" + ex.getMessage() + "\"}";
        }
    }

    public String SubirFotoEntrega(HttpServletRequest request) {
        Configuracion confi = new Configuracion();
        return "{ \"respuesta\": \"" + confi.SubirFoto(request) + "\"}";
    }

    public String SubirFirma(HttpServletRequest request) {
        int codigo = Globales.Validarintnonull(request.getParameter("codigo"));
        String usuarios = request.getParameter("usuario");
        String recibio = request.getParameter("recibio");
        String cargo = request.getParameter("cargo");
        String empr_env = request.getParameter("empr_env");
        String telefono = request.getParameter("telefono");
        String identificacion = request.getParameter("identificacion");
        String nro_guia = request.getParameter("nro_guia");
        String firma = request.getParameter("firma");
        int vueltas = Globales.Validarintnonull(request.getParameter("vueltas"));
        String observacioncli = request.getParameter("observacioncli");
        try {
            int destWidth = 0;
            int destHeight = 0;
            int sourceWidth = 0;
            int sourceHeight = 0;

            String base64 = firma;

            sql = "SELECT entregado FROM devolucion where devolucion = " + codigo;
            int entregado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

            if (entregado > 0) {
                return "{ \"respuesta\": \"1|Esta devolución ya fue entregada\"}";
            }

            sql = "SELECT idusuaeje FROM registrovueltas where id = " + vueltas;
            int ejecutada = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

            if (ejecutada > 0) {
                return "{ \"respuesta\": \"1|Esta vuelta ya fue ejecutada\"}";
            }

            sql = "UPDATE devolucion"
                    + "      SET fechaentrega=now(), fotodev = 2, entregado = 1, nombreentrega='MOVIL-CLIENTE', observacionentrega='ENTREGA DESDE LA MOVIL',"
                    + "          empresa_envia = " + empr_env + ", guia='" + nro_guia + "', observacioncliente = '" + observacioncli + "', idusuarioentrega =(select idusu from seguridad.rbac_usuario WHERE nomusu = '" + usuario + "'), recibio='" + recibio + "', identificacionr='" + identificacion + "', telefonor='" + telefono + "',cargor='" + cargo + "' WHERE devolucion = " + codigo + ";";
            sql += "UPDATE remision_detalle rd set entregado = 1 "
                    + "  FROM devolucion_detalle dd INNER JOIN devolucion d on d.id = dd.iddevolucion "
                    + "  WHERE dd.ingreso = rd.ingreso and d.devolucion = " + codigo + ";";
            if (vueltas > 0) {
                sql = "UPDATE registrovueltas SET estado ='EJECUTADA',  fechaeje = now(), idusuaeje=(select idusu from seguridad.rbac_usuario WHERE nomusu = '" + usuarios + "'), ejecutado='SI', observacion='Ejecutada por devolución de equipo' WHERE id = " + vueltas;
            }
            Globales.DatosAuditoria(sql, "DEVOLUCION", "ENTREGAR", "(select idusu from seguridad.rbac_usuario WHERE nomusu = '" + usuarios + "')", "APP-MOVIL");

            if (firma.length > 0 && firma.StartsWith("data:image/png;base64")) {
                base64 = firma.Replace("data:image/png;base64,", "").Replace(" ", "+");
            }

            byte[] bytes = Convert.FromBase64String(base64);
            Image fotonew;

            using(var ms = new MemoryStream(bytes, 0, bytes.Length)
            
                )
                {
                    Image Imagen = Image.FromStream(ms, true);
                fotonew = CambiarTamanoImagen(Imagen, 600);

                sourceWidth = Imagen.Width;
                sourceHeight = Imagen.Height;

                if (sourceHeight >= sourceWidth) {
                    destHeight = 600;
                    destWidth = sourceWidth * 600 / sourceHeight;
                } else {
                    destWidth = 600;
                    destHeight = sourceHeight * 600 / sourceWidth;

                }
            }
            if (fotonew == null) {
                return JsonConvert.SerializeObject("{ \"respuesta\": \"1|Error al guardar la foto\"}");
            }

            String path = HostingEnvironment.MapPath("~/imagenes/DevoFirma/" + codigo);
            if (!Directory.Exists(path)) {
                DirectoryInfo di = Directory.CreateDirectory(path);
            }
            ImageCodecInfo myImageCodecInfo;
            Encoder myEncoder;
            EncoderParameter myEncoderParameter;
            EncoderParameters myEncoderParameters;

            myImageCodecInfo = GetEncoderInfo("image/png");

            myEncoder = Encoder.Quality;

            String ruta = HostingEnvironment.MapPath("~/imagenes/DevoFirma/" + codigo + "/firma-" + codigo + ".png");

            myEncoderParameters = new EncoderParameters(1);

            myEncoderParameter = new EncoderParameter(myEncoder, 100L);
            myEncoderParameters.Param[0] = myEncoderParameter;
            fotonew.Save(ruta, myImageCodecInfo, myEncoderParameters);

            String nombreArchivo = RpDevolucion(codigo);

            return JsonConvert.SerializeObject("{ \"respuesta\": \"0|Devolución entregada con éxito|" + nombreArchivo + "\"}");

        } catch (Exception ex) {
            return JsonConvert.SerializeObject("{ \"respuesta\": \"1|" + ex.getMessage() + "\"}");
        }
    }

    public String BuscarClieProv(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int ejecutado = Globales.Validarintnonull(request.getParameter("ejecutado"));
        String usuarios = request.getParameter("usuario");
        try {
            String sql = "";
            if (cliente > 0) {
                sql += "SELECT id, nombrecompleto, estado FROM public.clientes WHERE estado = 1 AND id > 0 ORDER BY 2 ASC;";
            }

            if (proveedor > 0) {
                sql += "SELECT id, nombrecompleto, estado FROM public.proveedores WHERE estado = 1 AND id > 0 ORDER BY 2 ASC;";
            }

            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarClieProv");
        } catch (Exception ex) {
            return "{ \"respuesta\": \"1|" + ex.getMessage() + "\"}";
        }
    }

    public String BuscarVueltas(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int ejecutado = Globales.Validarintnonull(request.getParameter("ejecutado"));
        int cli_pro = Globales.Validarintnonull(request.getParameter("cli_pro"));
        String usuarios = request.getParameter("usuario");
        try {
            String where = " WHERE idusuarioasi = (SELECT idusu FROM seguridad.rbac_usuario WHERE nomusu = '" + usuarios + "')";
            if (cliente > 0) {
                where += " AND v.idcliente = " + cli_pro + " AND v.tipo = 'CLIENTE'";
            }

            if (proveedor > 0) {
                where += " AND v.idcliente = " + cli_pro + " AND v.tipo = 'PROVEEDOR'";
            }

            if (ejecutado > 0) {
                where += " AND v.estado = 'ASIGNADA'";
            }

            String sql = "SELECT DISTINCT v.id, v.actividad || ' ' || v.contacto || ' ' || v.direccion || ' ' || v.observacion || ' (' || nomusu || ')' AS actividad,"
                    + "   to_char(fechaapro,'yyyy/MM/dd HH24:MI') as fechaapro, fotos, v.estado"
                    + "   FROM registrovueltas v "
                    + "   inner join seguridad.rbac_usuario u on u.idusu = v.idusuario" + (cliente > 0 ? " INNER JOIN public.clientes cli ON cli.id = v.idcliente " : " INNER JOIN public.proveedores cli ON cli.id = v.idcliente ") + where + " order by fechaapro;";

            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarVueltas");
        } catch (Exception ex) {
            return "{ \"respuesta\": \"1|" + ex.getMessage() + "\"}";
        }
    }

    public String CerrarVueltas(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String observacion = request.getParameter("observacion");
        int ejecutado = Globales.Validarintnonull(request.getParameter("ejecutado"));
        String usuarios = request.getParameter("usuario");

        try {
            String resultado;
            sql = "SELECT idusuarioasi FROM registrovueltas where id = " + id;
            String usuarioasig = Globales.ObtenerUnValor(sql).trim();
            String idusuario = Globales.ObtenerUnValor("SELECT idusu FROM seguridad.rbac_usuario WHERE nomusu = '" + usuarios + "';").trim();
            /*if (PermisosSistemas("VUELTAS EJECUTAR OTRO USUARIO") == 0)
                {
                    
                }
                if (PermisosSistemas("VUELTAS EJECUTAR") == 0)
                    return JsonConvert.SerializeObject("{ \"respuesta\": \"1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema\" }");
             */

            if (usuarioasig != idusuario) {
                return JsonConvert.SerializeObject("{ \"respuesta\": \"1|El usuario que ejecuta las vueltas debe ser el usuario que se le asignó las vueltas\" }");
            }

            if (ejecutado == 1) {
                sql = "UPDATE registrovueltas SET estado ='EJECUTADA',  fechaeje = now(), idusuaeje= (SELECT idusu FROM seguridad.rbac_usuario WHERE nomusu = '" + usuarios + "'), ejecutado='SI', observacion='" + observacion + "' WHERE id = " + id;
                resultado = "0|Vuelta Ejecutada";
                Globales.DatosAuditoria(sql, "EJECUTAR VUELTA", "VUELTA", "(select idusu from seguridad.rbac_usuario WHERE nomusu = '" + usuarios + "')", "APP-MOVIL");
            } else {
                if (ejecutado == 2) {
                    sql = "UPDATE registrovueltas SET estado ='NO EJECUTADA - RECHAZADA', fechaeje = now(), idusuaeje= (SELECT idusu FROM seguridad.rbac_usuario WHERE nomusu = '" + usuarios + "'), ejecutado='RECHAZADA', observacion='" + observacion + "' WHERE id = " + id;
                    resultado = "0|Vuelta RECHAZADA";
                    Globales.DatosAuditoria(sql, "EJECUTAR VUELTA", "VUELTA", "(select idusu from seguridad.rbac_usuario WHERE nomusu = '" + usuarios + "')", "APP-MOVIL");
                } else {
                    sql = "UPDATE registrovueltas SET estado ='NO EJECUTADA', fechaeje = now(), idusuaeje= (SELECT idusu FROM seguridad.rbac_usuario WHERE nomusu = '" + usuarios + "'), ejecutado='NO', observacion='" + observacion + "' WHERE id = " + id;
                    resultado = "0|Vuelta NO Ejecutada";
                    Globales.DatosAuditoria(sql, "EJECUTAR VUELTA", "VUELTA", "(select idusu from seguridad.rbac_usuario WHERE nomusu = '" + usuarios + "')", "APP-MOVIL");
                }
            }

            return JsonConvert.SerializeObject("{ \"respuesta\": \"" + resultado + "\" }");
        } catch (Exception ex) {
            return JsonConvert.SerializeObject("{ \"respuesta\": \"1|" + ex.getMessage() + "\"}");
        }
    }

    public String SubirFotoVuelta(HttpServletRequest request) {
        Configuracion confi = new Configuracion();
        return "{ \"respuesta\": \"" + confi.SubirFoto(request) + "\"}";
    }

    public String EnvioDevolucion(HttpServletRequest request) {
        return "1|Error inesperado... Revise el log de errores";
    }

    private String RpDevolucion(HttpServletRequest request) {

        return "";

    }

    //FIN DEL CODIGO
    protected void Servidor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String ruta_origen = request.getHeader("Origin");

        HttpSession misession = request.getSession();
        idusuario = misession.getAttribute("idusuario").toString();
        iplocal = misession.getAttribute("IPLocal").toString();
        usuario = misession.getAttribute("codusu").toString();
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");

        try (PrintWriter out = response.getWriter()) {
            String opcion = request.getParameter("opcion");
            switch (opcion) {

                //WILMER
                case "ValidarUsuarioAPP":
                    out.println(ValidarUsuarioAPP(request));
                    break;
                case "GuardarPosicionAPP":
                    out.println(GuardarPosicionAPP(request));
                    break;
                case "BuscarIngreso":
                    out.println(BuscarIngreso(request));
                    break;
                case "SubirFoto":
                    out.println(SubirFoto(request));
                    break;
                case "BuscarDevolucion":
                    out.println(BuscarDevolucion(request));
                    break;
                case "SubirFotoDevolucion":
                    out.println(SubirFotoDevolucion(request));
                    break;
                case "BuscarVueltasPendientes":
                    out.println(BuscarVueltasPendientes(request));
                    break;
                case "EntregarDevolucionFoto":
                    out.println(EntregarDevolucionFoto(request));
                    break;
                case "SubirFotoEntrega":
                    out.println(SubirFotoEntrega(request));
                    break;
                case "SubirFirma":
                    out.println(SubirFirma(request));
                    break;
                case "BuscarClieProv":
                    out.println(BuscarClieProv(request));
                    break;
                case "BuscarVueltas":
                    out.println(BuscarVueltas(request));
                    break;
                case "CerrarVueltas":
                    out.println(CerrarVueltas(request));
                    break;
                case "SubirFotoVuelta":
                    out.println(SubirFotoVuelta(request));
                    break;
                case "EnvioDevolucion":
                    out.println(EnvioDevolucion(request));
                    break;
                case "RpDevolucion":
                    out.println(RpDevolucion(request));
                    break;
                //FIN
                default:
                    throw new AssertionError();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

}
