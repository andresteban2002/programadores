var TablaVC = null;

$("#CMagnitudS").html(CargarCombo(1,1));
$("#DescripcionPAux, #CDescripcionPAux").html(CargarCombo(93, 1));
$("#IdPatronAuxS").html(CargarCombo(94,1));
$("#MarcaPAux").focus();


tb = $('select');
$(tb).keypress(enter2tab);
d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))
var AprobarCotiza = "";

output = d.getFullYear() + 1 + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Documento") {
            if ($.trim($(this).val()) != "") {

                BuscarCliente($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function LimpiarTablas(){
	$('#TablaSensores tbody').empty();
	$('#TablaVM tbody').empty();
	$('#TablaVC tbody').empty();
	
}

function LimpiarSensor(){
	$("#IdSensor").val("0");
	$("#SerieS").val("");
	$("#CMagnitudS").val("").trigger("change");
	$("#UrlS").val("");
	$("#LecturaS").val("").trigger("change");
	$('#CertificadoS').val("");
	$('#FechaCalibracionS').val("");
	$('#ProximaS').val("");
	LimpiarVMedida();
	LimpiarVCertificado();
	if (TablaVC){
		TablaVC.clear().draw();
		TablaVC = null;
	}
}

function LimpiarVMedida(){
	
	$("#MinTem1").val("");
	$("#MinTem2").val("");
	$("#MinTem3").val("");
	$("#MinTem4").val("0");
	
	$("#MinHume1").val("");
	$("#MinHume2").val("");
	$('#MinHume3').val("");
	$('#MinHume4').val("0");
	
	$("#MaxTem1").val("");
	$("#MaxTem2").val("");
	$("#MaxTem3").val("");
	$("#MaxTem4").val("0");
	
	$("#MaxHume1").val("");
	$("#MaxHume2").val("");
	$('#MaxHume3').val("");
	$('#MaxHume4').val("0");
	
}

function LimpiarVCertificado(){
	$("#idVC").val("");
	$("#PatronVC").val("");
	$("#MedidaVC").val("").trigger("change");              
	$("#CorreccionVC").val("");
	$("#InstrumentoVC").val("");
	$("#VariableVC").val("");
	$("#OrdenVC").val("");
}

function LimpiarDatosPatronAux(){
	$("#IdPatronAux").val("");
	$("#MarcaPAux").val("");
	$("#ModeloPAux").val("");
	$("#SeriePAux").val("");
	$("#CertificadoPAux").val("");
	$("#CapacidadPAux").val("");
	$("#DireccionPAux").val("");
	$("#DescripcionPAux").val("").trigger("change");
	$("#PisoPAux").val("").trigger("change");
}

function ConsultarVM(){
	var idsensor = $("#IdSensor").val();
    if (idsensor == 0) {
        $("#IdSensor").focus();
        swal("Acción Cancelada", "Debe de seleccionar una base", "warning");
        return false;
    }
        
    ActivarLoad();
    setTimeout(function () {
        var parametros = "IdSensor=" + idsensor;
        var datos = LlamarAjax("Patron","opcion=ConsultarVM&"+parametros);
        DesactivarLoad();
        //Fin Codigo
        var data = JSON.parse(datos);
        
		for (var x = 0; x < data.length; x++) {
			if (data[x].tipo == "Minimo" && data[x].medida == "Temperatura") {
				$("#MinTem1").val(data[x].valor1);
				$("#MinTem2").val(data[x].valor2);
				$("#MinTem3").val(data[x].valor3);
				$("#MinTem4").val(data[x].valor4);
			}

			if (data[x].tipo == "Minimo" && data[x].medida == "Humedad") {
				$("#MinHume1").val(data[x].valor1);
				$("#MinHume2").val(data[x].valor2);
				$("#MinHume3").val(data[x].valor3);
				$("#MinHume4").val(data[x].valor4);
			}

			if (data[x].tipo == "Minimo" && data[x].medida == "Presion") {
				$("#MinPres1").val(data[x].valor1);
				$("#MinPres2").val(data[x].valor2);
				$("#MinPres3").val(data[x].valor3);
				$("#MinPres4").val(data[x].valor4);
			}

			if (data[x].tipo == "Maximo" && data[x].medida == "Temperatura") {
				$("#MaxTem1").val(data[x].valor1);
				$("#MaxTem2").val(data[x].valor2);
				$("#MaxTem3").val(data[x].valor3);
				$("#MaxTem4").val(data[x].valor4);
			}
			if (data[x].tipo == "Maximo" && data[x].medida == "Humedad") {
				$("#MaxHume1").val(data[x].valor1);
				$("#MaxHume2").val(data[x].valor2);
				$("#MaxHume3").val(data[x].valor3);
				$("#MaxHume4").val(data[x].valor4);
			}

			if (data[x].tipo == "Maximo" && data[x].medida == "Presion") {
				$("#MaxPres1").val(data[x].valor1);
				$("#MaxPres2").val(data[x].valor2);
				$("#MaxPres3").val(data[x].valor3);
				$("#MaxPres4").val(data[x].valor4);
			}
		}
                                        
	}, 15);		        
	
}

function ConsultarVC(){
	 var idsensor = $("#IdSensor").val();
    if (idsensor == 0) {
        $("#IdSensor").focus();
        swal("Acción Cancelada", "Debe de seleccionar una base", "warning");
        return false;
    }

  
        
    ActivarLoad();
    setTimeout(function () {
        var parametros = "IdSensor=" + idsensor;
        var datos = LlamarAjax("Patron","opcion=ConsultarVC&"+parametros).split("||");
        DesactivarLoad();
        //Fin Codigo
        var datajson = JSON.parse(datos[0]);
        
		
		TablaVC = $('#TablaVC').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [                
                { "data": "id"},                
                { "data": "medida"},                
                { "data": "patron", "className": "text-right" },
                { "data": "instrumento", "className": "text-right" },
                { "data": "correccion", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 3, '') },
                { "data": "variable", "className": "text-right" },
                { "data": "orden", "className": "text-right" },
                { "data": "eliminar" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
            
        });  
        
                                    
	}, 15);		        
	
}

function ConsultarPatronAux() {

        
//Andres
    ActivarLoad();
    setTimeout(function () {
        var parametros = "";
        var datos = LlamarAjax("Patron","opcion=ConsultarPatronAux&"+parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaPatronesAux').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [                
                { "data": "id"},
                { "data": "descripcion" },
                { "data": "serie" }, 
                { "data": "marca"},
                { "data": "modelo" },                              
                { "data": "capacidad" },                
                { "data": "piso" },
                { "data": "sensor" },
                { "data": "direccion" }
                
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });       
	},15);		
}

$("#TablaPatronesAux > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var id = row[0].innerText;
    BuscarPatronAux(id);
});


function BuscarPatronAux(id) {
    
    ActivarLoad();
    setTimeout(function () {

        LimpiarDatosPatronAux();              
        if ((id * 1) == 0)
            return false;
        
        var parametros = "patron=" + id;
        var datos = LlamarAjax('Patron','opcion=BuscarPatronAux&'+parametros);
        DesactivarLoad();
        
        if (datos != "[]") {
            var datacot = JSON.parse(datos);           
            $("#IdPatronAux").val(datacot[0].id);                                                      
            $("#MarcaPAux").val(datacot[0].marca);            
            $("#ModeloPAux").val(datacot[0].modelo);                       
            $("#SeriePAux").val(datacot[0].serie);   
            $("#CapacidadPAux").val(datacot[0].capacidad);         
            $("#DescripcionPAux").val(datacot[0].descripcion).trigger("change");
            $("#IdPatronAuxS").val(datacot[0].id).trigger("change");
            $("#PisoPAux").val(datacot[0].piso).trigger("change");
			$("#DireccionPAux").val(datacot[0].direccion);                       

        } else {            
            swal("Acción Cancelada", "Patron número " + id + " no registrado", "warning");
        }
    },15);
}

$("#formPatronesAux").submit(function (e) {
    e.preventDefault();    
    var parametros = $("#formPatronesAux").serialize() + "&IdPatronAux=" + IdPatronAux;
    var datos = LlamarAjax("Patron","opcion=GuardarPatronAux&" + parametros).split("|");
    if (datos[0] == "0") {
        ConsultarPatronAux();
        
        swal("", datos[1], "success");
        $("#IdPatronAux").val(datos[2]); 
       
      
        
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

    return false;

});

$("#formSensor").submit(function (e) {
    e.preventDefault();    
    IdPatronAux = $("#IdPatronAuxS").val();
    var fecha = $("#FechaCalibracionS").val();
    var proxima = $("#ProximaS").val();
    if (fecha >= proxima){
		 $("#FechaCalibracionS").focus();
		 swal("Acción Cancelada","La fecha de calibración debe ser menor a la próxima fecha de calibración","warning");
		 return false;
	}
	if (fecha > output2){
		 $("#FechaCalibracionS").focus();
		 swal("Acción Cancelada","La fecha de calibración no puede ser mayor a " + output2,"warning");
		 return false;
	}
    var parametros = $("#formSensor").serialize() + "&IdPatron=" + IdPatronAux;
    var datos = LlamarAjax("Patron","opcion=GuardarSensor&" + parametros).split("|");
    if (datos[0] == "0") {        
		$("#IdSensor").val(datos[2]);
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
    return false;
});

$("#formVM").submit(function (e) {
    e.preventDefault();    
    IdSensor = $("#IdSensor").val();
    if (IdSensor == 0) {
        $("#IdPatronAuxS").focus();
        swal("Acción Cancelada", "Debe de seleccionar un sensor", "warning");
        return false;
    }
    var parametros = $("#formVM").serialize() + "&IdSensor=" + IdSensor;
    var datos = LlamarAjax("Patron","opcion=GuardarValoresMedida&" + parametros).split("|");
    if (datos[0] == "0") {  
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

    return false;

});


$("#formVC").submit(function (e) {
    e.preventDefault();    
    IdSensor = $("#IdSensor").val();
     if (IdSensor == 0) {
        $("#IdPatronAuxS").focus();
        swal("Acción Cancelada", "Debe de seleccionar un sensor", "warning");
        return false;
    }
    var parametros = $("#formVC").serialize() + "&IdSensor=" + IdSensor;
    var datos = LlamarAjax("Patron","opcion=GuardarValoresCertificado&" + parametros).split("|");
    if (datos[0] == "0") {  
		ConsultarVC();    
		LimpiarVCertificado();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
    return false;
});


function ModalSensor() {
    var idpatron = $("#IdPatronAuxS").val() * 1;
    if (idpatron == 0) {
        $("#IdPatronAuxS").focus();
        swal("Acción Cancelada", "Debe de seleccionar una base", "warning");
        return false;
    }

    $("#modnomsensor").html($("#IdPatronAuxS option:selected").text());
        
    ActivarLoad();
    setTimeout(function () {
        var parametros = "IdPatron=" + idpatron;
        var datos = LlamarAjax("Patron","opcion=ConsultarSensor&"+parametros).split("||");
        DesactivarLoad();
        //Fin Codigo
        var datajson = JSON.parse(datos[0]);
        
		
		$('#TablaSensores').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [                
                { "data": "id"},                
                { "data": "serie" },
                { "data": "magnitud" },
                { "data": "lectura" },                
                { "data": "certificado" },                
                { "data": "fecha_calibracion" },                
                { "data": "proxima_fecha" },                
                { "data": "url" }
                
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip'
            
        });  
         $("#modalSensores").modal("show"); 
                                    
        }, 15);		
        
}

$("#TablaSensores > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var sensor = row[0].innerText;
    $("#IdSensor").val(sensor);
    BuscarSensor(sensor); 
    LimpiarVMedida()
    $("#modalSensores").modal("hide");
});

function BuscarSensor(id) {
    
    ActivarLoad();
    setTimeout(function () {

        LimpiarSensor();
        if ((id * 1) == 0)
            return false;
            
		$("#IdSensor").val(id);
        
        var parametros = "sensor=" + id;
        var datos = LlamarAjax('Patron','opcion=BuscarSensor&'+parametros);
        DesactivarLoad();
        
        if (datos != "[]") {
            var datacot = JSON.parse(datos);
            $("#SerieS").val(datacot[0].serie);            
            $("#CMagnitudS").val(datacot[0].idmagnitud).trigger("change");  
            $("#UrlS").val(datacot[0].url);   
            $("#LecturaS").val(datacot[0].lectura).trigger("change");                    
            $('#CertificadoS').val(datacot[0].certificado);
			$('#FechaCalibracionS').val(datacot[0].fecha_calibracion);
			$('#ProximaS').val(datacot[0].proxima_fecha);
            ConsultarVM();
            ConsultarVC();
                                                                                          } else {            
            swal("Acción Cancelada", "Sensor número " + id + " no registrado", "warning");
        }
    },15);
}


$("#TablaVC > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var VC = row[0].innerText;
    LimpiarVCertificado();
    $("#idVC").val(VC);
    BuscarVC(VC);
    
    
});

function BuscarVC(VC) {
        
    

    ActivarLoad();
    setTimeout(function () {

        LimpiarVCertificado();              
        if ((VC * 1) == 0)
            return false;
        var parametros = "vc=" + VC;
        var datos = LlamarAjax('Patron','opcion=BuscarVC&'+parametros);
        DesactivarLoad();
        
        if (datos != "[]") {
            var datacot = JSON.parse(datos);
			$("#idVC").val(VC);            
            $("#PatronVC").val(datacot[0].patron);            
            $("#MedidaVC").val(datacot[0].medida).trigger("change");              
            $("#InstrumentoVC").val(datacot[0].instrumento);          
            $("#CorreccionVC").val(formato_numero(datacot[0].patron - datacot[0].instrumento, 3, ",", ".", ""));          
            $("#VariableVC").val(datacot[0].variable);  
            $("#OrdenVC").val(datacot[0].orden);   
        } else {            
            swal("Acción Cancelada", "Valor número " + VC + " no registrado", "warning");
        }
    },15);
}

function CalcularCorreccion(objeto){
	ValidarTexto(objeto,3);
	var patron = NumeroDecimal($("#PatronVC").val());
	var instrumento = NumeroDecimal($("#InstrumentoVC").val());
	var coreccion = patron - instrumento;
	$("#CorreccionVC").val(coreccion);
}

function EliminarVM(id) {
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar el item número ' + id + ', del valor de medida del sensor?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor+ "Patron", "opcion=EliminarValoresMedida&id=" + id)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            ConsultarVM();
                            resolve();
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};

function EliminarVC(id) {
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar el item número ' + id + ', del valor del certificado del sensor?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor+ "Patron", "opcion=EliminarValoresCertificado&id=" + id)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            ConsultarVC();
                            resolve();
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

ConsultarPatronAux();

$('select').select2();
DesactivarLoad();
