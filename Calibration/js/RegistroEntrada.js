﻿var DocumentoPer = "";
var AutoEmpresa = [];
var AutoCargo = [];
var FotoDocumento = "";

$("#Pais").html(CargarCombo(18, 1));
$("#EPS").html(CargarCombo(43, 1));
$("#ARL").html(CargarCombo(44, 1));
$("#Responsable").html(CargarCombo(13, 1));
$("#Pais").val(1).trigger("change");

$("#formPersonal").submit(function (e) {
    e.preventDefault();
    var parametros = $("#formPersonal").serialize();
    var datos = LlamarAjax("RegistroEntrada","opcion=GuardarPersonal&"+ parametros).split("|");
    if (datos[0] == "0") {
        $("#IdPersonal").val(datos[2]);
        $("#IdVisita").val(datos[3]);
        $("#FechaEntrada").val(datos[4]);
        $("#FechaSalida").val(datos[5]);
        TablaVisitas(datos[2]);
        AutoCompletar();
        if (FotoDocumento != "") {
            GuardarFoto(DocumentoPer, FotoDocumento, 2, datos[3])
        }
        
        swal("",datos[1],"success");
        
        var responsable = $("#Responsable option:selected").text();
        var nombres = $("#Nombres").val();
        var cargo = $("#Cargo").val();
        var area = $("#Area").val();
        var salida = $.trim(datos[5]);
        var empresa = $("#Empresa").val();
        var mensaje = "La visita <b>" + nombres + "</b> (" + cargo + ")<br>De la empresa <b>" + empresa + 
					  "</b><br>Responsable Interno <b>" + responsable + "</b><br>Área a visitar <b>" + area + 
					  "</b><br>" + (salida == "null" ? "ha sido registrado en nuestras instalaciones" : "ha registrado su salidad de nuestras instalaciones");
		sendMessage(3,mensaje);
        
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

    return false;

});

d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CDesde").val(output);
$("#CHasta").val(output2);



function CambioDepartamento(pais) {
    $("#Departamento").html(CargarCombo(11, 1, "", pais));
}

function CambioCiudad(departamento) {
    $("#Ciudad").html(CargarCombo(12, 1, "", departamento));
}

function LimpiarTodo() {
    $("#Documento").val("");
    DocumentoPer = "";
    LimpiarDatos();
    $("#Documento").focus();
    TablaVisitas(0);
}

function LimpiarDatos() {
    $("#IdPersonal").val("0");
    $("#IdVisita").val("0");
    $("#Nombres").val("");
    $("#Usuario").val("");
    $("#Cargo").val("");
    $("#Telefonos").val("");
    $("#Celular").val("");
    $("#Motivo").val("");
    $("#Empresa").val("");
    $("#Area").val("");
    $("#Contacto").val("");
    $("#TelefonosEmer").val("");
    $("#CorreoPersonal").val("");
    $("#Departamento").val("").trigger("change");
    $("#DocumentoEntr").val("Ninguno").trigger("change");
    $("#FechaEntrada").val("");
    $("#EPS").val("").trigger("change");
    $("#ARL").val("").trigger("change");
    $("#Responsable").val("").trigger("change");
    $("#EPP").prop("checked", "");
    $("#FechaSalida").val("");
    FotoDocumento = "";
    $("#ImagenPersona").attr("src", url_cliente + "Adjunto/imagenes/img_avatar.png");
}

$('#ImagenPersona').load(function (e) {

}).error(function (e) {
    $("#" + this.id).attr("src", url_cliente + "Adjunto/imagenes/img_avatar.png")
});

function LlamarFoto(documento) {
    $("#FotoVisita").attr("src", url_cliente + "Adjunto/imagenes/FotoVisita/" + documento + ".jpg");
    $("#modalFotoPersona").modal("show");
}

function VerDocumento() {
    var visita = $("#IdVisita").val()*1;
    $("#FotoDocumento").attr("src", url_cliente + "Adjunto/imagenes/FotoDocumento/" + visita + ".jpg");
    $("#modalFotoDocumento").modal("show");
}

$('#FotoDocumento').load(function (e) {

}).error(function (e) {
    swal("Acción Cancelada", "Esta visita no posee documento registrado con foto", "warning");
    $("#modalFotoDocumento").modal("hide");
});

function BuscarPersonal(documento) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoPer == documento)
        return false;
    LimpiarDatos();
    DocumentoPer = documento;
    var parametros = "documento=" + $.trim(documento);
    var id = 0;
    var datos = LlamarAjax('RegistroEntrada','opcion=BuscarPersonal&'+ parametros);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        id = data[0].id;
        $("#IdPersonal").val(data[0].id);
        $("#Nombres").val(data[0].nombrecompleto);
        $("#Cargo").val(data[0].cargo);
        $("#Telefonos").val(data[0].telefono);
        $("#Celular").val(data[0].celular);
        $("#CorreoPersonal").val(data[0].email);
        $("#Empresa").val(data[0].empresa);
        $("#Area").val(data[0].area);
        $("#Contacto").val(data[0].contacto);
        $("#TelefonosEmer").val(data[0].telefonoemer);
        $("#Motivo").val(data[0].motivo);
        $("#DocumentoEntr").val(data[0].documentoentre).trigger("change");
        $("#EPS").val(data[0].eps).trigger("change");
        $("#ARL").val(data[0].arl).trigger("change");
        if (data[0].responsable * 1 > 0) 
            $("#Responsable").val(data[0].responsable).trigger("change");
        if (data[0].epp * 1 > 0) {
            $("#EPP").prop("checked", "checked");
        }

        $("#ImagenPersona").attr("src", url_cliente + "Adjunto/imagenes/FotoVisita/" + documento + ".jpg?randor=" + Math.random());

        if (data[0].ciudad * 1 > 0) {
            $("#Pais").val(data[0].pais).trigger("change");
            $("#Departamento").val(data[0].departamento).trigger("change");
            $("#Ciudad").val(data[0].ciudad).trigger("change");
        }
        if (data[0].fechasalida == null && data[0].fechaentrada != null) {
            $("#IdVisita").val(data[0].idvisita);
            $("#FechaEntrada").val(data[0].fechaentrada);
        }
    } 
    TablaVisitas(id);
}

function ValidarPersonal(documento) {
    if ($.trim(DocumentoPer) != "") {
        if (DocumentoPer != documento) {
            DocumentoPer = "";
            LimpiarDatos();
        }
    }
}

function TablaVisitas(persona) {
        
    ActivarLoad();
    setTimeout(function () {
        var parametros = "personal=" + persona;
        var datos = LlamarAjax("RegistroEntrada","opcion=TablaVisitas&"+ parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaVisitasPers').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "motivo" },
                { "data": "empresa" },
                { "data": "fechaentrada" },
                { "data": "fechasalida" }
            ],
            "language": {
                "url": LenguajeDataTable
            }
        });
    }, 15);
}

$("#TablaPersonaVisita > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    
    var datos = LlamarAjax('RegistroEntrada','opcion=DetalleVisita&visita=' + numero);
    var data = JSON.parse(datos);
    $("#MoDocumento").val(data[0].documento);
    $("#MoNombres").val(data[0].nombrecompleto);
    $("#MoCargo").val(data[0].cargo);
    $("#MoTelefonos").val(data[0].telefono);
    $("#MoCelular").val(data[0].celular);
    $("#MoCorreoPersonal").val(data[0].email);
    $("#MoEmpresa").val(data[0].empresa);
    $("#MoArea").val(data[0].area);
    $("#MoContacto").val(data[0].contacto);
    $("#MoTelefonosEmer").val(data[0].telefonoemer);
    $("#MoMotivo").val(data[0].motivo);
    $("#MoDocumentoEntr").val(data[0].documentoentre);
    $("#MoEPS").val(data[0].eps);
    $("#MoARL").val(data[0].arl);
    $("#MoResponsable").val(data[0].responsable);
    if (data[0].epp * 1 > 0)
        $("#MoEPP").prop("checked", "checked");
    else
        $("#MoEPP").prop("checked", "");
                
    $("#MoPais").val(data[0].pais);
    $("#MoDepartamento").val(data[0].departamento);
    $("#MoCiudad").val(data[0].ciudad);
    $("#MoFechaEntrada").val(data[0].fechaentrada);
    $("#MoFechaSalida").val(data[0].fechasalida);
    
    $("#modalVisita").modal("show");
});

function ConsultarVisitas() {

    var empresa = $.trim($("#CEmpresa").val());
    var visitante = $.trim($("#CVisitante").val());
    var motivo = $.trim($("#CMotivo").val());
    var desde = $("#CDesde").val();
    var hasta = $("#CHasta").val();

    ActivarLoad();
    setTimeout(function () {
        var parametros = "empresa=" + empresa + "&visitante=" + visitante + "&motivo=" + motivo + "&desde=" + desde + "&hasta=" + hasta;
        var datos = LlamarAjax("RegistroEntrada","opcion=ConsultaVisitas&"+ parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaPersonaVisita').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "foto" },
                { "data": "visita" },
                { "data": "empresa" },
                { "data": "visitante" },
                { "data": "telefono" },
                { "data": "cargo" },
                { "data": "email" },
                { "data": "motivo" },
                { "data": "fechaentrada" },
                { "data": "fechasalida" },
                { "data": "pais" },
                { "data": "departamento" },
                { "data": "ciudad" },
                { "data": "usuario" }

            ],
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ],
            "language": {
                "url": LenguajeDataTable
            }
        });
    }, 15);
}


function TomarFoto() {
    var tipo = $("#TipoFoto").val() * 1;
    Webcam.snap(function (data_uri) {
        if (tipo == 1)
            GuardarFoto(DocumentoPer, data_uri, tipo, 0);
        else {
            FotoDocumento = data_uri;
            $("#modalFotoPer").modal("hide");
        }
            
    });
}

function GuardarFoto(documento, archivo, tipo, visita) {
	var parametros = "opcion=SubirFoto&archivo=" + archivo + "&documento=" + documento + "&visita=" + visita + "&opciones=" + tipo +
			"&tipo_imagen=1"; 
	var data = LlamarAjax("Configuracion",parametros).split("|");
	if (data[0] == "0") {
		if (tipo == 1){
			$("#ImagenPersona").attr("src", url_cliente + "Adjunto/imagenes/FotoVisita/" + documento + ".jpg?lastmod=" + data[2]);
			$("#modalFotoPer").modal("hide");
		}
	} else
		swal("Acción Cancelada", data[1], "warning");
}

function SubirFoto(tipo) {
    var fechaentrada = $.trim($("#FechaEntrada").val());
    var tipodocumento = $("#DocumentoEntr").val();
    if (DocumentoPer == "")
        return false;
    if (tipo == 1)
        $("#titulotipofoto").html("Foto de la Visita");
    else {
        if (fechaentrada != "" || tipodocumento == "Ninguno")
            return false
        $("#titulotipofoto").html("Foto del documento");
    }
        
    $("#TipoFoto").val(tipo);
    $("#modalFotoPer").modal("show");
}


Webcam.attach('#my_cameraperso');

function AutoCompletar() {
    var datos = LlamarAjax("RegistroEntrada","opcion=Autocompletar").split("|");

    AutoEmpresa.splice(0, AutoEmpresa.length);
    AutoCargo.splice(0, AutoCargo.length);
    var data = JSON.parse(datos[0]);
    for (var x = 0; x < data.length; x++) {
        AutoEmpresa.push(data[x].empresa);
    }
    var data1 = JSON.parse(datos[1]);
    for (var x = 0; x < data1.length; x++) {
        AutoCargo.push(data1[x].cargo);
    }
}

AutoCompletar();

$("#Cargo").autocomplete({ source: AutoCargo, minLength: 0 }).focus(function () {
    $(this).data("uiAutocomplete").search($(this).val());
});

$("#Empresa").autocomplete({ source: AutoEmpresa, minLength: 0 }).focus(function () {
    $(this).data("uiAutocomplete").search($(this).val());
});


$('select').select2();
DesactivarLoad();
