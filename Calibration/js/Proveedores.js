﻿var IdProveedor = 0;
var DocumentoPro = "";

$("#Pais").html(CargarCombo(18, 1));
$("#Pais").val(1).trigger("change");
$("#CuentaContable").html(CargarCombo(83, 1));

$("#ReteFuente").html(CargarCombo(89, 7, "", "ReteFuente"));
$("#ReteIva").html(CargarCombo(89, 7, "", "ReteIVA"));
$("#ReteIca").html(CargarCombo(89, 7, "", "ReteICA"));

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Documento") {
            if ($.trim($(this).val()) != "") {

                BuscarProveedor($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function ValidarProveedor(Caja) {
    ValidarTexto(Caja, 3);
    documento = Caja.value;
    if ($.trim(DocumentoPro) != "") {
        if (DocumentoPro != documento) {
            DocumentoPro = "";
            LimpiarDatos();
        }
    }
}

function CambioDepartamento(pais) {
    $("#Departamento").html(CargarCombo(11, 1, "", pais));
}

function CambioCiudad(departamento) {
    $("#Ciudad").html(CargarCombo(12, 1, "", departamento));
}


function LimpiarTodo() {
    $("#Documento").val("");
    DocumentoPro = "";
    LimpiarDatos();
    $("#Documento").focus();
}

function LimpiarDatos() {
    IdProveedor = 0;
    $("#Nombres").val("");
    $("#Comercial").val("");
    $("#Telefonos").val("");
    $("#Celular").val("");
    $("#Email").val("");
    $("#IdProveedor").val("0");
    $("#Direccion").val("");
    $("#Pais").val(1).trigger("change");
    $("#CuentaContable").val("").trigger("change");
    $("#Departamento").val(11).trigger("change");
    $("#Ciudad").val(150).trigger("change");
    $("#Estado").val(1).trigger("change");
    $("#ReteIva").val("0").trigger("change");
    $("#ReteIca").val("0").trigger("change");
    $("#ReteFuente").val("0").trigger("change");
    $("#PlazoPago").val("");


    $("#Contacto").val("");
}

function BuscarProveedor(documento) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoPro == documento)
        return false;

    DocumentoPro = documento;
    LimpiarDatos();
    var parametros = "documento=" + $.trim(documento);
    var datos = LlamarAjax('Configuracion','opcion=BuscarProveedor&' + parametros);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        IdProveedor = data[0].id;
        $("#Pais").val(data[0].idpais).trigger("change");
        $("#Departamento").val(data[0].iddepartamento).trigger("change");
        $("#Ciudad").val(data[0].idciudad).trigger("change");
        $("#Nombres").val(data[0].nombrecompleto);
        $("#Comercial").val((data[0].nombrecom ? data[0].nombrecom : ""));
        $("#Telefonos").val(data[0].telefono);
        $("#Celular").val(data[0].celular);
        $("#Email").val(data[0].email);
        $("#IdProveedor").val(data[0].id);
        $("#Direccion").val(data[0].direccion);
        $("#ReteIva").val(data[0].reteiva).trigger("change");
        $("#ReteIca").val(data[0].reteica).trigger("change");
        $("#PlazoPago").val(data[0].plazopago);
        $("#ReteFuente").val(data[0].retefuente).trigger("change");
        $("#CuentaContable").val(data[0].idcuecontable).trigger("change");

        $("#Contacto").val(data[0].contacto);
        $("#Estado").val(data[0].estado).trigger("change");
    }
}

function CambioNIT() {
    if (IdProveedor == 0) {
        swal("Acción Cancelada", "Debe seleccionar el proveedor", "warning");
        return false;
    }
    $("#SCambioNit").html(DocumentoPro);
    $("#modalCambioNitPro").modal("show");

}

function CambiarNIT() {
    var documento = $.trim($("#DDocumento").val());
    if (documento == "") {
        $("#DDocumento").focus();
        swal("Acción Cancelada", "Debe de ingresar el nuevo NIT/Cédula", "warning");
        return false;
    }

    var datos = LlamarAjax("Configuracion","opcion=CambioNit&documento=" + documento + "&id=" + IdProveedor + "&tipo=2").split("|");
    if (datos[0] == "0") {
        $("#modalCambioNitPro").modal("hide");
        $("#Documento").val(documento);
        BuscarProveedor(documento);
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

$("#formProveedores").submit(function (e) {
    e.preventDefault();
    var parametros = $("#formProveedores").serialize().replace(/\%2C/g, "");
    var datos = LlamarAjax("Configuracion","opcion=GuardarProveedor&" + parametros).split("|");
    if (datos["0"] == "0") {
        if (IdProveedor == 0) {
            IdProveedor = datos[1];
            $("#IdProveedor").val(IdProveedor);
            swal("", "Proveedor guardado con éxito", "success");
        } else
            swal("", "Proveedor actualizado con éxito", "success");
        return false;
    } else {
        swal("Acción Cancelada", datos[1], "warning");
        return false;
    }
});


$("#TablaProveedores > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    $('#tab-proveedores a[href="#tabcrear"]').tab('show');
    $("#Documento").val(numero);
    BuscarProveedor(numero);
});


function ConsultarProveedores() {
    
    var documento =$.trim($("#MDocumento").val());
    var cliente= $.trim($("#MCliente").val());
    var comercial = $.trim($("#MComercial").val());
    var estado = $("#MEstado").val()*1;

    var pais = $("#MPais").val() * 1;
        
    ActivarLoad();
    setTimeout(function () {
        var parametros = "documento=" + documento + "&nombrecomp=" + cliente + "&nombrecomer=" + comercial + "&estado=" + estado + "&pais=" + pais;
        var datos = LlamarAjax("Configuracion","opcion=ConsultarProveedores&" + parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaProveedores').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "documento" },
                { "data": "nombrecompleto" },
                { "data": "nombrecom" },
                { "data": "contacto" },
                { "data": "estado" },
                { "data": "direccion" },
                { "data": "telefono" },
                { "data": "celular" },
                { "data": "email" },
                { "data": "ciudad" },
                { "data": "departamento" },
                { "data": "pais" },
                { "data": "cuenta" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

$('select').select2();
DesactivarLoad();


