﻿var movil = 0;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    movil = 1;
}

var datastuden = JSON.parse(LlamarAjax("Laboratorio/TStuden", ""));


var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(2, 254, 21)', 'rgb(6, 252, 245)', 'rgb(240, 20, 10)', 'rgb(254, 246, 2)'];

var graficasb = document.getElementById('myCharBarraSB').getContext('2d');
var mygraficasb = null;



function ConfiguracionTablas(cantidad) {
    var resultado = "";
    var combo1 = "<option value=''>--</option>" +
        "<option value='°C' selected>°C</option>" +
        "<option value='°F'>°F</option>";

    var combo2 = "<option value=''>--</option>" +
        "<option value='A' selected>A</option>" +
        "<option value='B'>B</option>";

    var combo3 = "<option value=''>--</option>" +
        "<option value='A'>A</option>" +
        "<option value='B' selected>B</option>";

    var combo4 = "<option value=''>--</option>" +
        "<option value='N'>N</option>" +
        "<option value='R' selected>R</option>";

    var combo5 = "<option value=''>--</option>" +
        "<option value='N' selected>N</option>" +
        "<option value='R'>R</option>";

    var combo6 = "<option value='' selected>--</option>" +
        "<option value='°C'>°C</option>" +
        "<option value='°F'>°F</option>";

    for (var x = 1; x <= cantidad; x++) {
        resultado += '<table width="100%" align="center" border="1" class="tabcertificado">' +
            '<tr class="tabcertr">' +
            '<th class="bg-celeste" style="font-size:20px">' + x + '</th>' +
            '</tr>' +
            '</table><br/>';
        resultado += '<table width="60%" align="center" border="1" class="tabcertificado">' +
            '<tr class="tabcertr">' +
            '<th width="25%" class="bg-gris">&nbsp;</th>' +
            '<th width="25%" class="bg-gris">T (K)</th>' +
            '<th width="25%" class="bg-gris">S(T)</th>' +
            '<th width="25%" class="bg-gris">dS/DT</th>' +
            '</tr>' +
            '<tr class="tabcertr">' +
            '<th class="bg-gris">T Fuente</th>' +
            '<td align="right" id="TFuente' + x + '1"></td>' +
            '<td align="right" id="TFuente' + x + '2"></td>' +
            '<td align="right" id="TFuente' + x + '3"></td>' +
            '</tr>' +
            '<tr class="tabcertr">' +
            '<th class="bg-gris">T reflejada</th>' +
            '<td align="right" id="TReflejada' + x + '1"></td>' +
            '<td align="right" id="TReflejada' + x + '2"></td>' +
            '<td align="right" id="TReflejada' + x + '3"></td>' +
            '</tr>' +
            '<tr class="tabcertr">' +
            '<th class="bg-gris">T nominal</th>' +
            '<td align="right" id="TNominal' + x + '1"></td>' +
            '<td align="right" id="TNominal' + x + '2"></td>' +
            '<td align="right" id="TNominal' + x + '3"></td>' +
            '</tr>' +
            '<tr class="tabcertr">' +
            '<th class="bg-gris">T IBC</th>' +
            '<td align="right" id="TIBC' + x + '1"></td>' +
            '<td align="right" id="TIBC' + x + '2"></td>' +
            '<td align="right" id="TIBC' + x + '3"></td>' +
            '</tr></table><br>';
    }

    $("#TabSakHatt").html(resultado);

    resultado = "";
    var temperatura = [0, 100, 200, 300];
    for (var x = 1; x <= cantidad; x++) {
        resultado += '<table width="100%" align="center" border="1" class="tabcertificado">' +
            '<tr class="tabcertr">' +
            '<th class="bg-celeste" style="font-size:28px">' + x + '</th>' +
            '</tr>' +
            '</table><br/>' +
            '<div class="form-group row">' +
            '<div class="col-md-8">' +
            '    <table width="100%" border="1" class="tabcertificado">' +
            '        <tr class="tabcertr">' +
            '            <th class="bg-gris"><span idioma="Temperatura"></span> <span class="medida">°C</span></th>' +
            '            <th class="text-XX14"><input type="text" name="Temperatura" id="Temperatura' + x + '" value="' + temperatura[x] + '" readonly class="form-control sinbordecon text-XX14 text-center" /></th>' +
            '        </tr>' +
            '        <tr class="tabcertr">' +
            '            <th width="16%" class="bg-gris"><span idioma="Lecturas"></span></th>' +
            '            <th width="18%" class="bg-gris"><span idioma="Temperatura"></span> <span class="medida">°C</span><br><span idioma="Patrón"></span></th>' +
            '            <th width="18%" class="bg-gris"><span idioma="Temperatura"></span> <span class="medida">°C</span><br><span idioma="IBC"></span></th>' +
            '            <th width="15%" class="bg-gris" idioma="Fecha"></th>' +
            '            <th width="15%" class="bg-gris" idioma="Hora"></th>' +
            '        </tr>';
        for (var y = 1; y <= 12; y++) {
            if (y <= 10) {
                resultado += '<tr>' +
                    '<td><input type="text" name="Lectura' + x + '" id="Lectura' + x + y + '" value="' + y + '" readonly class="form-control sinbordecon  text-right text-XX" /></td>' +
                    '<td><input type="text" name="TemPat' + x + '" id="TemPat' + x + y + '" required onfocus="FormatoEntrada(this, 1)" onchange="GuardarTemporal()" onkeyup="CalcularSerie(this,' + x + ')" class="form-control sinbordecon text-right text-XX14 bg-amarillo" /></td>' +
                    '<td><input type="text" name="TemIBC' + x + '" id="TemIBC' + x + y + '" required onfocus="FormatoEntrada(this, 1)" onchange="GuardarTemporal()" onkeyup="CalcularSerie(this,' + x + ')" class="form-control sinbordecon text-right text-XX14 bg-amarillo" /></td>';
                if (y == 2) {
                    resultado += '<th rowspan="2" class="bg-gris text-center"><span idioma="T Ambiente"></span><br><span idioma="°C"></span></th>' +
                        '<th rowspan="2" class="bg-gris text-center"><span idioma="H Ambiente"></span><br><span idioma="% HR"></span></th>';
                }
                if (y == 4) {
                    resultado += '<td><input type="text" name="TAmbiente" id="TAmbiente' + x + '" class="form-control sinbordecon bg-celeste  text-center text-XX14 " onkeyup="CalcularSerie(this,' + x + ')" /></td>' +
                        '<td><input type="text" name="HAmbiente" id="HAmbiente' + x + '"  class="form-control sinbordecon  bg-celeste text-center text-XX14 " onkeyup="CalcularSerie(this,' + x + ')" /></td>';
                }
                if (y == 5) {
                    resultado += '<th class="bg-gris text-XX">Distancia</th>' +
                        '<td><input type="text" name="Distancia" id="Distancia' + x + '" required onfocus="FormatoEntrada(this, 1)" onchange="GuardarTemporal()" onkeyup="ValidarTexto(this,3)" class="form-control sinbordecon text-right text-XX14 bg-amarillo" /></td>';
                }
            } else {
                
                if (y == 11) {
                    resultado += '<tr>' +
                        '<th class="bg-gris"><span idioma="Promedio"></span></th>' +
                        '<td><input type="text" name="PromedioLecTP" id="PromedioLecTP' + x + '" readonly class="form-control sinbordecon  text-right text-XX14 " /></td>' +
                        '<td><input type="text" name="PromedioLecTIBC" id="PromedioLecTIBC' + x + '" readonly class="form-control sinbordecon  text-right text-XX14 " /></td>';
                } else {
                    resultado += '<tr>' +
                        '<th class="bg-gris"><span idioma="DS"></span></th>' +
                        '<td><input type="text" name="DSLecTP" id="DSLecTP' + x + '" readonly class="form-control sinbordecon  text-right text-XX14 " /></td>' +
                        '<td><input type="text" name="DSLecTIBC" id="DSLecTIBC' + x + '" readonly class="form-control sinbordecon  text-right text-XX14 " /></td>';
                }
                
            }



            if (y == 1) {
                resultado += '<td><input type="text" name="FechaLec" id="FechaLec' + x + '" readonly class="form-control sinbordecon  text-right text-XX14 " /></td>' +
                    '<td><input type="text" name="HoraLec" id="HoraLec' + x + '" readonly class="form-control sinbordecon  text-right text-XX14 " /></td>';
            }
            resultado += '</tr>';

        }

        resultado += '</table>' +
            '</div>' +
            '<div class="col-md-4">' +
            '    <label idioma="Observaciones"></label>' +
            '    <textarea rows="4" class="form-control bg-amarillo" id="Observacion' + x + '" onchange="GuardarTemporal()" name="Observacion"></textarea>' +
            '</div>' +
            '</div>';

        resultado += '<center><h3 style="color:blue; font-size:22px;"><span idioma="CALCULOS DE LA OPERACION"></span></h3></center>' +
            '<table width="100%" border="1" class="tabcertificado">' +
                                '<tr class="tabcertr">' +
                                '    <th colspan="3" class="bg-gris text-center"><span idioma="Fuente de radiación"></span></th>' +
                                '    <th colspan="2" class="text-center">&nbsp;</th>' +
                                '    <th colspan="2" class="text-center"><span idioma="Incertidumbre"></span></th>' +
                                '    <th class="text-center">&nbsp;</th>' +
                                '    <th colspan="2" class="text-center"><span idioma="Incertidumbre Standard"></span></th>' +
                                '    <th class="text-center"><span idioma="Grado de Libertad"></span></th>' +
                                '    <th colspan="2" class="text-center"><span idioma="Coeficiente de Sensibilidad"></span></th>' +
                                '    <th colspan="2" class="text-center">&nbsp;</th>' +
                                '    <th class="text-center"><span idioma="Porcentaje"></span></th>' +
                                '</tr>' +
                                '<tr class="tabcertr">' +
                                '    <th class="bg-gris text-center"><span idioma="Fuente de Incertidumbre"></span></th>' +
                                '    <th colspan="2" class="text-center bg-gris"><span idioma="Valor Estimado"></span></th>' +
                                '    <th class="text-center bg-gris"><span idioma="Tipo"></span></th>' +
                                '    <th class="text-center bg-gris"><span idioma="Distri-bución"></span></th>' +
                                '    <th colspan="2" class="text-center bg-gris"><span idioma="u"></span></th>' +
                                '    <th class="text-center bg-gris"><span idioma="Factor"></span></th>' +
                                '    <th colspan="2" class="text-center bg-gris"><span idioma="u tipica"></span></th>' +
                                '    <th class="text-center bg-gris"><span idioma="G.L."></span></th>' +
                                '    <th colspan="2" class="text-center bg-gris"><span idioma="C"></span><sub>i</sub></th>' +
                                '    <th colspan="2" class="text-center bg-gris"><span idioma="Contribución a u"></span></th>' +
                                '    <th colspan="2" class="text-center text-XX14 bg-gris"><span idioma="%"></span></th>' +
                                '</tr>' +
                                '<tr class="tabcertr">' +
                                '    <th width="15%"><span idioma="U12: Repetibilidad"></span></th>' +
                                '    <td width="8%"><input type="text" name="FRRepValEst" id="FRRepValEst' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td width="4%"><select name="FRRepMedEst" id="FRRepMedEst' + x + '" class="form-control sinbordecon medida" style="width:100%">' + combo1 + '</select></td>' +
                                '    <td width="4%"><select name="FRRepTip" id="FRRepTip' + x + '" class="form-control sinbordecon" style="width:100%">' + combo2 + '</select></td>' +
                                '    <td width="4%"><select name="FRRepDis" id="FRRepDis' + x + '" class="form-control sinbordecon" style="width:100%">' + combo5 + '</select></td>' +
                                '    <td width="8%"><input type="text" name="FRRepValIns" id="FRRepValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td width="4%"><select name="FRRepMedIns" id="FRRepMedIns' + x + '" class="form-control sinbordecon medida" style="width:100%">' + combo1 + '</select></td>' +
                                '    <td width="8%" class="text-center"><input type="text" name="FRRepFac" id="FRRepFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td width="8%"><input type="text" name="FRRepValInsSta" id="FRRepValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td width="3%"><input type="text" name="FRRepMedInsSta" id="FRRepMedInsSta' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td width="8%"><input type="text" name="FRRepGraLib" id="FRRepGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td width="8%"><input type="text" name="FRRepValCoeLib" id="FRRepValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td width="3%"><input type="text" name="FRRepMedCoeLib" id="FRRepMedCoeLib' + x + '" value="" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td width="8%"><input type="text" name="FRRepValCon" id="FRRepValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td width="3%"><input type="text" name="FRRepMedCon" id="FRRepMedCon' + x + '" value="" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td width="4%"><input type="text" name="FRRepPor" id="FRRepPor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '</tr>' +
                                '<tr class="tabcertr">' +
                                '    <th><span idioma="U2: Emisividad/emisivity"></span></th>' +
                                '    <td><input type="text" name="FREmiValEst" id="FREmiValEst' + x + '" value="0.0000" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FREmiMedEst" id="FREmiMedEst' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo1 + '</select></td>' +
                                '    <td><select name="FREmiTip" id="FREmiTip' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo3 + '</select></td>' +
                                '    <td><select name="FREmiDis" id="FREmiDis' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo4 + '</select></td>' +
                                '    <td><input type="text" name="FREmiValIns" id="FREmiValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FREmiMedIns" id="FREmiMedIns' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo6 + '</select></td>' +
                                '    <td class="text-center"><input type="text" name="FREmiFac" id="FREmiFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><input type="text" name="FREmiValInsSta" id="FREmiValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FREmiMedInsSta" id="FREmiMedInsSta' + x + '" value="--" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FREmiGraLib" id="FREmiGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FREmiValCoeLib" id="FREmiValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FREmiMedCoeLib" id="FREmiMedCoeLib' + x + '" value="K" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FREmiValCon" id="FREmiValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FREmiMedCon" id="FREmiMedCon' + x + '" value="K" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FREmiPor" id="FREmiPor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '</tr>' +
                                '<tr class="tabcertr">' +
                                '    <th><span idioma="U6: Uniformidad  / Uniformity "></span></th>' +
                                '    <td><input type="text" name="FRUniValEst" id="FRUniValEst' + x + '" value="0" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FRUniMedEst" id="FRUniMedEst' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo1 + '</select></td>' +
                                '    <td><select name="FRUniTip" id="FRUniTip' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo3 + '</select></td>' +
                                '    <td><select name="FRUniDis" id="FRUniDis' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo4 + '</select></td>' +
                                '    <td><input type="text" name="FRUniValIns" id="FRUniValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FRUniMedIns" id="FRUniMedIns' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo1 + '</select></td>' +
                                '    <td class="text-center"><input type="text" name="FRUniFac" id="FRUniFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><input type="text" name="FRUniValInsSta" id="FRUniValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRUniMedInsSta" id="FRUniMedInsSta' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRUniGraLib" id="FRUniGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRUniValCoeLib" id="FRUniValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRUniMedCoeLib" id="FRUniMedCoeLib' + x + '" value="--" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRUniValCon" id="FRUniValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRUniMedCon" id="FRUniMedCon" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRUniPor" id="FRUniPor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '</tr>' +
                                '<tr class="tabcertr">' +
                                '    <th><span idioma="Estabilidad/Sta"></span></th>' +
                                '    <td><input type="text" name="FREstValEst" id="FREstValEst' + x + '" value="0" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FREstMedEst" id="FREstMedEst' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo1 + '</select></td>' +
                                '    <td><select name="FREstTip" id="FREstTip' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo3 + '</select></td>' +
                                '    <td><select name="FREstDis" id="FREstDis' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo4 + '</select></td>' +
                                '    <td><input type="text" name="FREstValIns" id="FREstValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FREstMedIns" id="FREstMedIns' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo1 + '</select></td>' +
                                '    <td class="text-center"><input type="text" name="FREstFac" id="FREstFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><input type="text" name="FREstValInsSta" id="FREstValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FREstMedInsSta" id="FREstMedInsSta' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FREstGraLib" id="FREstGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FREstValCoeLib" id="FREstValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FREstMedCoeLib" id="FREstMedCoeLib' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FREstValCon" id="FREstValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FREstMedCon" id="FREstMedCon' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FREstPor" id="FREstPor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '</tr>' +
                                '<tr class="tabcertr">' +
                                '    <th><span idioma="U1: Incertidumbre por calibración / Calibrarion uncertity."></span></th>' +
                                '    <td><input type="text" name="FRInsValEst" id="FRInsValEst' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FRInsMedEst" id="FRInsMedEst' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo1 + '</select></td>' +
                                '    <td><select name="FRInsTip" id="FRInsTip' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo3 + '</select></td>' +
                                '    <td><select name="FRInsDis" id="FRInsDis' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo5 + '</select></td>' +
                                '    <td><input type="text" name="FRInsValIns" id="FRInsValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FRInsMedIns" id="FRInsMedIns' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo1 + '</select></td>' +
                                '    <td class="text-center"><input type="text" name="FRInsFac" id="FRInsFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><input type="text" name="FRInsValInsSta" id="FRInsValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRInsMedInsSta" id="FRInsMedInsSta' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRInsGraLib" id="FRInsGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRInsValCoeLib" id="FRInsValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRInsMedCoeLib" id="FRInsMedCoeLib' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRInsValCon" id="FRInsValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRInsMedCon" id="FRInsMedCon' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRInsPor" id="FRInsPor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '</tr>' +
                                '<tr class="tabcertr">' +
                                '    <th><span idioma="Derivada"></span></th>' +
                                '    <td><input type="text" name="FRDerValEst" id="FRDerValEst' + x + '" readonly value="0"  class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FRDerMedEst" id="FRDerMedEst' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo1 + '</select></td>' +
                                '    <td><select name="FRDerTip" id="FRDerTip' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo3 + '</select></td>' +
                                '    <td><select name="FRDerDis" id="FRDerDis' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo4 + '</select></td>' +
                                '    <td><input type="text" name="FRDerValIns" id="FRDerValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FRDerMedIns" id="FRDerMedIns' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo1 + '</select></td>' +
                                '    <td class="text-center"><input type="text" name="FRDerFac" id="FRDerFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><input type="text" name="FRDerValInsSta" id="FRDerValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRDerMedInsSta" id="FRDerMedInsSta' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRDerGraLib" id="FRDerGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRDerValCoeLib" id="FRDerValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRDerMedCoeLib" id="FRDerMedCoeLib' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRDerValCon" id="FRDerValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRDerMedCon" id="FRDerMedCon' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRDerPor" id="FRDerPor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '</tr>' +
                                 '<tr class="tabcertr">' +
                                '    <th><span idioma="U3: Radiación ambiental reflejada"></span></th>' +
                                '    <td><input type="text" name="FRRadValEst" id="FRRadValEst' + x + '" value="0.0000" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FRRadMedEst" id="FRRadMedEst' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo1 + '</select></td>' +
                                '    <td><select name="FRRadTip" id="FRRadTip' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo3 + '</select></td>' +
                                '    <td><select name="FRRadDis" id="FRRadDis' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo4 + '</select></td>' +
                                '    <td><input type="text" name="FRRadValIns" id="FRRadValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FRRadMedIns" id="FRRadMedIns' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo1 + '</select></td>' +
                                '    <td class="text-center"><input type="text" name="FRRadFac" id="FRRadFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><input type="text" name="FRRadValInsSta" id="FRRadValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRRadMedInsSta" id="FRRadMedInsSta' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRRadGraLib" id="FRRadGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRRadValCoeLib" id="FRRadValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRRadMedCoeLib" id="FRRadMedCoeLib' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRRadValCon" id="FRRadValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRRadMedCon" id="FRRadMedCon' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRRadPor" id="FRRadPor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '</tr>' +
                                 '<tr class="tabcertr">' +
                                '    <th><span idioma="U4: Intercambio de calor de la fuente"></span></th>' +
                                '    <td><input type="text" name="FRIntValEst" id="FRIntValEst' + x + '" value="0.0000" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FRIntMedEst" id="FRIntMedEst' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo1 + '</select></td>' +
                                '    <td><select name="FRIntTip" id="FRIntTip' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo3 + '</select></td>' +
                                '    <td><select name="FRIntDis" id="FRIntDis' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo4 + '</select></td>' +
                                '    <td><input type="text" name="FRIntValIns" id="FRIntValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FRIntMedIns" id="FRIntMedIns' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo1 + '</select></td>' +
                                '    <td class="text-center"><input type="text" name="FRIntFac" id="FRIntFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><input type="text" name="FRIntValInsSta" id="FRIntValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRIntMedInsSta" id="FRIntMedInsSta' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRIntGraLib" id="FRIntGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRIntValCoeLib" id="FRIntValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRIntMedCoeLib" id="FRIntMedCoeLib' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRIntValCon" id="FRIntValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRIntMedCon" id="FRIntMedCon' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRIntPor" id="FRIntPor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '</tr>' +
                                 '<tr class="tabcertr">' +
                                '    <th><span idioma="U5: Condiciones ambientales Backround"></span></th>' +
                                '    <td><input type="text" name="FRConValEst" id="FRConValEst' + x + '" value="0.0000" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FRConMedEst" id="FRConMedEst' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo1 + '</select></td>' +
                                '    <td><select name="FRConTip" id="FRConTip' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo3 + '</select></td>' +
                                '    <td><select name="FRConDis" id="FRConDis' + x + '" class="form-control sinbordecon" style="width:100%" >' + combo4 + '</select></td>' +
                                '    <td><input type="text" name="FRConValIns" id="FRConValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><select name="FRConMedIns" id="FRConMedIns' + x + '" class="form-control sinbordecon medida" style="width:100%" >' + combo1 + '</select></td>' +
                                '    <td class="text-center"><input type="text" name="FRConFac" id="FRConFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '    <td><input type="text" name="FRConValInsSta" id="FRConValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRConMedInsSta" id="FRConMedInsSta' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRConGraLib" id="FRConGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRConValCoeLib" id="FRConValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRConMedCoeLib" id="FRConMedCoeLib' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRConValCon" id="FRConValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRConMedCon" id="FRConMedCon' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '    <td><input type="text" name="FRConPor" id="FRConPor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                                '</tr>' +
                                '<tr class="tabcertr">' +
                                '    <th><span idioma="Temperatura de la fuente"></span></th>' +
                                '    <td><input type="text" name="FRTemFue" id="FRTemFue' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                                '</tr>';

        resultado += '<tr>' +
                        '<td colspan="16">&nbsp;</td>' +
                     '</tr>' +
                           '     <tr class="tabcertr">' +
                           '         <th colspan="3" class="bg-gris text-center"><span idioma="Termómetro IBC"></span></th>' +
                           '     </tr>' +
                           '     <tr class="tabcertr">' +
                           '         <th class="bg-gris text-center"><span idioma="Fuente de Incertidumbre"></span></th>' +
                           '         <th colspan="2" class="text-center bg-gris"><span idioma="Valor Estimado"></span></th>' +
                           '         <th class="text-center bg-gris"><span idioma="Tipo"></span></th>' +
                           '         <th class="text-center bg-gris"><span idioma="Distri-bución"></span></th>' +
                           '         <th colspan="2" class="text-center bg-gris"><span idioma="u"></span></th>' +
                           '         <th class="text-center bg-gris"><span idioma="Factor"></span></th>' +
                           '         <th colspan="2" class="text-center bg-gris"><span idioma="u tipica"></span></th>' +
                           '         <th class="text-center bg-gris"><span idioma="G.L."></span></th>' +
                           '         <th colspan="2" class="text-center bg-gris"><span idioma="C"></span><sub>i</sub></th>' +
                           '         <th colspan="2" class="text-center bg-gris"><span idioma="Contribución a u"></span></th>' +
                           '         <th colspan="2" class="text-center text-XX14 bg-gris"><span idioma="%"></span></th>' +
                           '     </tr>' +
                           '     <tr class="tabcertr">' +
                           '         <th><span idioma="U7: Tamaño de la fuente / SSE"></span></th>' +
                           '         <td><input type="text" name="FTTamFueValEst" id="FTTamFueValEst' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input name="FTTamFueMedEst" id="FTTamFueMedEst' + x + '" value="°C" readonly class="form-control sinbordecon medida" /></td>' +
                           '         <td><input name="FTTamFueTip" id="FTTamFueTip' + x + '" value="B" readonly class="form-control sinbordecon" /></td>' +
                           '         <td><input name="FTTamFueDis" id="FTTamFueDis' + x + '" value="R" readonly class="form-control sinbordecon" /></td>' +
                           '         <td><input type="text" name="FTTamFueValIns" id="FTTamFueValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input name="FTTamFueMedIns" id="FTTamFueMedIns' + x + '" value="--" readonly class="form-control sinbordecon medida" /></td>' +
                           '         <td class="text-center"><input type="text" name="FTTamFueFac" id="FTTamFueFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input type="text" name="FTTamFueValInsSta" id="FTTamFueValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTTamFueMedInsSta" id="FTTamFueMedInsSta' + x + '" value="--" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTTamFueGraLib" id="FTTamFueGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTTamFueValCoeLib" id="FTTamFueValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTTamFueMedCoeLib" id="FTTamFueMedCoeLib' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTTamFueValCon" id="FTTamFueValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTTamFueMedCon" id="FTTamFueMedCon' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTTamFuePor" id="FTTamFuePor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '     </tr>' +
                           '     <tr class="tabcertr">' +
                           '         <th><span idioma="U8: Temperatura ambiental"></span></th>' +
                           '         <td><input type="text" name="FTTemAmbValEst" id="FTTemAmbValEst' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input name="FTTemAmbMedEst" id="FTTemAmbMedEst' + x + '" value="°C" readonly class="form-control sinbordecon medida" /></td>' +
                           '         <td><input name="FTTemAmbTip" id="FTTemAmbTip' + x + '" value="B" readonly class="form-control sinbordecon" /></td>' +
                           '         <td><input name="FTTemAmbDis" id="FTTemAmbDis' + x + '" value="R" readonly class="form-control sinbordecon" /></td>' +
                           '         <td><input type="text" name="FTTemAmbValIns" id="FTTemAmbValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input name="FTTemAmbMedIns" id="FTTemAmbMedIns' + x + '" value="°C" readonly class="form-control sinbordecon medida" /></td>' +
                           '         <td class="text-center"><input type="text" name="FTTemAmbFac" id="FTTemAmbFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input type="text" name="FTTemAmbValInsSta" id="FTTemAmbValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTTemAmbMedInsSta" id="FTTemAmbMedInsSta' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTTemAmbGraLib" id="FTTemAmbGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTTemAmbValCoeLib" id="FTTemAmbValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTTemAmbMedCoeLib" id="FTTemAmbMedCoeLib' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTTemAmbValCon" id="FTTemAmbValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTTemAmbMedCon" id="FTTemAmbMedCon' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTTemAmbPor" id="FTTemAmbPor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '     </tr>' +
                           '     <tr class="tabcertr">' +
                           '         <th><span idioma="U10: Ruido Ambiental"></span></th>' +
                           '         <td><input type="text" name="FTRuiAmbValEst" id="FTRuiAmbValEst' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input name="FTRuiAmbMedEst" id="FTRuiAmbMedEst' + x + '" value="°C" readonly class="form-control sinbordecon medida" /></td>' +
                           '         <td><input name="FTRuiAmbTip" id="FTRuiAmbTip' + x + '" value="B" readonly class="form-control sinbordecon" /></td>' +
                           '         <td><input name="FTRuiAmbDis" id="FTRuiAmbDis' + x + '" value="R" readonly class="form-control sinbordecon" /></td>' +
                           '         <td><input type="text" name="FTRuiAmbValIns" id="FTRuiAmbValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input name="FTRuiAmbMedIns" id="FTRuiAmbMedIns' + x + '" value="°C" readonly class="form-control sinbordecon medida" /></td>' +
                           '         <td class="text-center"><input type="text" name="FTRuiAmbFac" id="FTRuiAmbFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input type="text" name="FTRuiAmbValInsSta" id="FTRuiAmbValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTRuiAmbMedInsSta" id="FTRuiAmbMedInsSta' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTRuiAmbGraLib" id="FTRuiAmbGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTRuiAmbValCoeLib" id="FTRuiAmbValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTRuiAmbMedCoeLib" id="FTRuiAmbMedCoeLib' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTRuiAmbValCon" id="FTRuiAmbValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTRuiAmbMedCon" id="FTRuiAmbMedCon' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTRuiAmbPor" id="FTRuiAmbPor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '     </tr>' +
                           '     <tr class="tabcertr">' +
                           '         <th><span idioma="U12: Repetibilidad del Termómetro /Repeteability of thermometer"></span></th>' +
                           '         <td><input type="text" name="FTRepValEst" id="FTRepValEst' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input name="FTRepMedEst" id="FTRepMedEst' + x + '" value="°C" readonly class="form-control sinbordecon medida" /></td>' +
                           '         <td><input name="FTRepTip" id="FTRepTip' + x + '" value="A" readonly class="form-control sinbordecon" /></td>' +
                           '         <td><input name="FTRepDis" id="FTRepDis' + x + '" value="N" readonly class="form-control sinbordecon" /></td>' +
                           '         <td><input type="text" name="FTRepValIns" id="FTRepValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input name="FTRepMedIns" id="FTRepMedIns' + x + '" value="°C" readonly class="form-control sinbordecon medida" /></td>' +
                           '         <td class="text-center"><input type="text" name="FTRepFac" id="FTRepFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input type="text" name="FTRepValInsSta" id="FTRepValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTRepMedInsSta" id="FTRepMedInsSta' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTRepGraLib" id="FTRepGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTRepValCoeLib" id="FTRepValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTRepMedCoeLib" id="FTRepMedCoeLib' + x + '" value="--" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTRepValCon" id="FTRepValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTRepMedCon" id="FTRepMedCon' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTRepPor" id="FTRepPor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '     </tr>' +
                           '     <tr class="tabcertr">' +
                           '         <th><span idioma="U11: Resolución"></span></th>' +
                           '         <td><input type="text" name="FTResValEst" id="FTResValEst' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input name="FTResMedEst" id="FTResMedEst' + x + '" value="°C" readonly class="form-control sinbordecon medida" /></td>' +
                           '         <td><input name="FTResTip" id="FTResTip' + x + '" value="B" readonly class="form-control sinbordecon" /></td>' +
                           '         <td><input name="FTResDis" id="FTResDis' + x + '" value="R" readonly class="form-control sinbordecon" /></td>' +
                           '         <td><input type="text" name="FTResValIns" id="FTResValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input name="FTResMedIns" id="FTResMedIns' + x + '" value="°C" readonly class="form-control sinbordecon medida" /></td>' +
                           '         <td class="text-center"><input type="text" name="FTResFac" id="FTResFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input type="text" name="FTResValInsSta" id="FTResValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTResMedInsSta" id="FTResMedInsSta' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTResGraLib" id="FTResGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTResValCoeLib" id="FTResValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTResMedCoeLib" id="FTResMedCoeLib' + x + '" value="--" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTResValCon" id="FTResValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTResMedCon" id="FTResMedCon' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTResPor" id="FTResPor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '     </tr>' +
                           '     <tr class="tabcertr">' +
                           '         <th><span idioma="U9: Absorción Atmosférica/ Atmospheric Absorption"></span></th>' +
                           '         <td><input type="text" name="FTAbsValEst" id="FTAbsValEst' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input name="FTAbsMedEst" id="FTAbsMedEst' + x + '" value="°C" readonly class="form-control sinbordecon medida" /></td>' +
                           '         <td><input name="FTAbsTip" id="FTAbsTip' + x + '" value="B" readonly class="form-control sinbordecon" /></td>' +
                           '         <td><input name="FTAbsDis" id="FTAbsDis' + x + '" value="R" readonly class="form-control sinbordecon" /></td>' +
                           '         <td><input type="text" name="FTAbsValIns" id="FTAbsValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input name="FTAbsMedIns" id="FTAbsMedIns' + x + '" value="°C" readonly class="form-control sinbordecon medida" /></td>' +
                           '         <td class="text-center"><input type="text" name="FTAbsFac" id="FTAbsFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input type="text" name="FTAbsValInsSta" id="FTAbsValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTAbsMedInsSta" id="FTAbsMedInsSta' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTAbsGraLib" id="FTAbsGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTAbsValCoeLib" id="FTAbsValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTAbsMedCoeLib" id="FTAbsMedCoeLib' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTAbsValCon" id="FTAbsValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTAbsMedCon" id="FTAbsMedCon' + x + '" value="°C" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTAbsPor" id="FTAbsPor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '     </tr>' +
                           '     <tr>' +
                           '         <td colspan="16">&nbsp;</td>' +
                           '     </tr>' +
                           '     <tr class="tabcertr">' +
                           '         <th><span idioma="Corrección / Correction"></span></th>' +
                           '         <td><input type="text" name="FTCorValEst" id="FTCorValEst' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td class="text-XX8">°C</td>' +
                           '         <td><input name="FTCorTip" id="FTCorTip' + x + '" value="A" readonly class="form-control sinbordecon" /></td>' +
                           '         <td><input name="FTCorDis" id="FTCorDis' + x + '" value="N" readonly class="form-control sinbordecon" /></td>' +
                           '         <td><input type="text" name="FTCorValIns" id="FTCorValIns' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td class="text-XX8" >K total</td>' +
                           '         <td class="text-center"><input type="text" name="FTCorFac" id="FTCorFac' + x + '" readonly   class="form-control sinbordecon text-center" /></td>' +
                           '         <td><input type="text" name="FTCorValInsSta" id="FTCorValInsSta' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td class="text-XX8">Grado</td>' +
                           '         <td><input type="text" name="FTCorGraLib" id="FTCorGraLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTCorValCoeLib" id="FTCorValCoeLib' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td class="text-XX8">U combinada</td>' +
                           '         <td><input type="text" name="FTCorValCon" id="FTCorValCon' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td class="text-XX8">°C</td>' +
                           '         <td><input type="text" name="FTCorPor" id="FTCorPor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '     </tr>' +
                           '     <tr>' +
                           '         <td colspan="16">&nbsp;</td>' +
                           '     </tr>' +
                           '     <tr class="tabcertr">' +
                           '         <th class="text-center bg-gris"><span idioma="t fuente (°C)"></span></th>' +
                           '         <th class="text-center" bg-gris><span idioma="Corrección <br> C (°C)"></span></th>' +
                           '         <th colspan="2" class="text-center bg-gris"><span idioma="K"></span></th>' +
                           '         <th class="text-center bg-gris" colspan="4"><span idioma="µ (°C)"></span></th>' +
                           '     </tr>' +
                           '      <tr>' +
                           '         <td><input type="text" name="FTResFue" id="FTResFue' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td><input type="text" name="FTResCor" id="FTResCor' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td colspan="2"><input type="text" name="FTResK" id="FTResK' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td colspan="2"><input type="text" name="FTResIncSim" id="FTResIncSim' + x + '" value="±" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '         <td colspan="2"><input type="text" name="FTResIncVal" id="FTResIncVal' + x + '" value="0" readonly class="form-control sinbordecon  text-center" /></td>' +
                           '     </tr>' +
                           ' </table>' +
                           '<hr>'; 
    }
    $("#DatosCalibracion").html(resultado);
    TraducirIdioma($("#menu_idioma").val() * 1);
}

ConfiguracionTablas(3);

function CalularPruebla(Caja) {

    //$.jGrowl(C + "/  Math.pow((" + 2.71828183 + "," + c2 + " / (" + A + " * " + lectura1 + "+ " + B + ") - 1))", { life: 1500, theme: 'growl-success', header: '' });

    ValidarTexto(Caja, 3);
    A = NumeroDecimal($("#SBDato3").val());
    B = NumeroDecimal($("#SBDato4").val());
    C = NumeroDecimal($("#SBDato5").val());
    c2 = NumeroDecimal($("#SBDato8").val());

    valor1 = NumeroDecimal($("#SBAdiDato1").val());
    valor2 = NumeroDecimal($("#SBAdiDato2").val());
    valor3 = NumeroDecimal($("#SBAdiDato3").val());
    valor4 = NumeroDecimal($("#SBAdiDato4").val());
    valor5 = NumeroDecimal($("#SBAdiDato5").val());
    valor6 = NumeroDecimal($("#SBAdiDato6").val());




    var lecturas = [0];
    var radios = [0];
    var calculos = [0];
    var sigmas = [0];
    var prueba1 = [0];
    var prueba2 = [0];
    var sumaprueba = 0;
    for (x = 1; x <= 10; x++) {
        lecturas.push(NumeroDecimal($("#SBLectura" + x).val()));
        radios.push(NumeroDecimal($("#SBRadio" + x).val()));
        calculos.push(C / Math.pow(2.71828183, (c2 / (A * lecturas[x] + B) - 1)));
        
        $("#SBSr" + x).val(calculos[x]);
        
    }
    for (x = 1; x <= 10; x++) {
        sigmas.push((calculos[x] - calculos[6]) / calculos[1] * 100);
        $("#SBSigma" + x).val(sigmas[x]);
    }

    for (x = 1; x <= 10; x++) {
        prueba1.push(valor3 * (1 - Math.pow(2.71828183, (valor4 * (radios[6] - radios[x])))));
        prueba2.push(Math.pow((prueba1[x] - sigmas[x]), 2));
        sumaprueba += prueba2[x];
        $("#SBFPrueba1" + x).val(prueba1[x]);
        $("#SBFPrueba2" + x).val(prueba2[x]);
    }

    $("#SBAdiDato5").val(sumaprueba);

    var etiqueta = ['Sigma %', 'f prueba'];

    var config = {
        type:'line',
        data: {
            datasets: [],
            labels: []
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            responsive: true,
            tooltips: {
                enabled: true
            },
            legend: {
                position: 'top',
                display: (movil == 1 ? false : true)
            }
        },
        lineAtIndex: 2
    }

    var newDataset1 = {
        label: etiqueta[0],
        backgroundColor: colores[0],
        showLine: false,
        data: [],
    };

    var newDataset2 = {
        label: etiqueta[1],
        backgroundColor: colores[1],
        showLine: false,
        data: [],
    };

    for (x = 10; x >= 1; x--) {
        
        config.data.labels.push(radios[x]);
        if (radios[x] * 1 > 0) {
            newDataset1.data.push(sigmas[x]);
            newDataset2.data.push(prueba1[x]);
        } else {
            newDataset1.data.push(null);
            newDataset2.data.push(null);
        }

    }

    config.data.datasets.push(newDataset1);
    config.data.datasets.push(newDataset2);

    if (mygraficasb != null) {
        mygraficasb.destroy();
    }

    mygraficasb = new Chart(graficasb, config);
    mygraficasb.update();
}

function CalcularSakuma(Caja) {

}

function CalcularSerie(Caja, x) {
    ValidarTexto(Caja, 3);
    var promediotp = 0;
    var promediotibc = 0;
    var temperatura = NumeroDecimal($("#Temperatura" + x).val());
    var ambiente = NumeroDecimal($("TAmbiente" + x).val());


    var desviaciontp = 0;
    var desviaciontibc = 0;
    var desviaciontf = 0;

    for (y = 1; y <= 10; y++) {
        promediotp += NumeroDecimal($("#TemPat" + x + y).val());
        promediotibc += NumeroDecimal($("#TemIBC" + x + y).val());
    }
    promediotp = promediotp / 10;
    promediotibc = promediotibc / 10;

    for (y = 1; y <= 10; y++) {
        valor = NumeroDecimal($("#TemPat" + x + y).val()) - promediotp;
        desviaciontp += valor * valor;
        valor = NumeroDecimal($("#TemIBC" + x + y).val()) - promediotp;
        desviaciontibc += valor * valor;
    }

    desviaciontp = Math.sqrt((desviaciontp / 9));
    desviaciontibc = Math.sqrt((desviaciontibc / 9));

    $("#PromedioLecTP" + x).val(promediotp);
    $("#PromedioLecTIBC" + x).val(promediotibc);

    $("#DSLecTP" + x).val(desviaciontp);
    $("#DSLecTIBC" + x).val(desviaciontibc);

    if (Caja.name != "FRRepValEst")
        $("#FRRepValEst" + x).val(promediotp);
    if (Caja.name != "FRRepFac")
        $("#FRRepFac" + x).val(Math.sqrt(10));
    if (Caja.name != "FRRepValIns") {
        $("#FRRepValIns" + x).val(desviaciontp);
    }

    medida = $("#FRRepMedIns" + x).val()
    $("#FRRepMedInsSta" + x).val(medida);

    tipodis = $("#FRRepDis" + x).val();
    FRRepGraLib = tipodis == "N" ? 9 : 1000000;
    $("#FRRepGraLib" + x).val(FRRepGraLib);
    $("#FRRepValCoeLib" + x).val("1");



    FRRepValEst = NumeroDecimal($("#FRRepValEst" + x).val());

    $("#FRRepValInsSta" + x).val(NumeroDecimal($("#FRRepValIns" + x).val()) / NumeroDecimal($("#FRRepFac" + x).val()));
    FRRepValCon = NumeroDecimal($("#FRRepValInsSta" + x).val());
    $("#FRRepValCon" + x).val(FRRepValCon);

    CoreccionMP = $("#CoreccionMP").html() * 1;
    CoreccionBP = $("#CoreccionBP").html() * 1;

    InsertidumbreMP = $("#InsertidumbreMP").html() * 1;
    InsertidumbreBP = $("#InsertidumbreBP").html() * 1;

    FRInsValEst = promediotp * CoreccionMP + CoreccionBP;
    $("#FRInsValEst" + x).val(FRInsValEst);

    FRInsValIns = promediotp * InsertidumbreMP + InsertidumbreBP;
    $("#FRInsValIns" + x).val(FRInsValEst);

    UEmisividadP = NumeroDecimal($("#UEmisividadP").val());
    UniformidadP = NumeroDecimal($("#UniformidadP").val());
    EstabilidadP = NumeroDecimal($("#EstabilidadP").val());

    FREmiValIns = UEmisividadP / 2;

    $("#FREmiValIns" + x).val(FREmiValIns);
    $("#FRUniValIns" + x).val(UniformidadP);
    $("#FREstValIns" + x).val(EstabilidadP);

    DeriVDato1 = NumeroDecimal($("#DeriVDato1").val());
    DeriVDato2 = NumeroDecimal($("#DeriVDato2").val());
    DeriVDato3 = NumeroDecimal($("#DeriVDato3").val());
    DeriVDato4 = NumeroDecimal($("#DeriVDato4").val());
    DeriVDato5 = NumeroDecimal($("#DeriVDato5").val());



    $("#FRDerValIns" + x).val(DeriVDato2);
    $("#FRRadValIns" + x).val(DeriVDato3);
    $("#FRIntValIns" + x).val(DeriVDato4);
    $("#FRConValIns" + x).val(DeriVDato5);


    FREmiFac = 2;
    FRUniFac = Math.sqrt(3);
    FREstFac = Math.sqrt(3);
    FRInsFac = Math.sqrt(5);
    FRDerFac = Math.sqrt(12);
    FRRadFac = Math.sqrt(12);
    FRIntFac = Math.sqrt(12);
    FRConFac = Math.sqrt(12);

    $("#FREmiFac" + x).val(FREmiFac);
    $("#FRUniFac" + x).val(FRUniFac);
    $("#FREstFac" + x).val(FREstFac);
    $("#FRInsFac" + x).val(FRInsFac);
    $("#FRDerFac" + x).val(FRDerFac);
    $("#FRRadFac" + x).val(FRRadFac);
    $("#FRIntFac" + x).val(FRIntFac);
    $("#FRConFac" + x).val(FRConFac);

    FREmiGraLib = 1000000;
    FRUniGraLib = 1000000;
    FREstValCon = 1000000;
    FREstGraLib = 1000000;
    FRInsGraLib = 0.5 * Math.pow(1 - 0.9545, -2);
    FRDerGraLib = 1000000;
    FRRadGraLib = 1000000;
    FRIntGraLib = 1000000;
    FRConGraLib = 1000000;

    $("#FREmiGraLib" + x).val(FREmiGraLib);
    $("#FRUniGraLib" + x).val(FRUniGraLib);
    $("#FREstValCon" + x).val(FREstValCon);
    $("#FREstGraLib" + x).val(FREstGraLib);
    $("#FRInsGraLib" + x).val(FRInsGraLib);
    $("#FRDerGraLib" + x).val(FRDerGraLib);
    $("#FRRadGraLib" + x).val(FRRadGraLib);
    $("#FRIntGraLib" + x).val(FRIntGraLib);
    $("#FRConGraLib" + x).val(FRConGraLib);

    EmisividadIBC = NumeroDecimal($("#EmisividadIBC").val());

    Li = NumeroDecimal($("#SHDato1").val());
    L0 = NumeroDecimal($("#SHDato2").val());
    DL = NumeroDecimal($("#SHDato3").val());
    c2 = NumeroDecimal($("#SHDato4").val());
    A = L0 * (1 - (DL * DL) / (2 * L0 * L0));
    B = c2 * (DL * DL) / (24 * (L0 * L0));
    C = NumeroDecimal($("#SHDato7").val());

    Ls = NumeroDecimal($("#SHAdiDato1").val());

    $("#SHDato5").val(A);
    $("#SHDato6").val(B);

    TFuente1 = promediotp + 273.15;
    TFuente2 = C / (Math.pow(2.71828183, c2 / (A * TFuente1 + B)) - 1);
    TFuente3 = TFuente2 * TFuente2 * (c2 * A / (C * Math.pow(A * TFuente1 + B, 2))) * Math.pow(2.71828183, (c2 / (A * TFuente1 + B)));

    TReflejada1 = ambiente + 273.15;
    TReflejada2 = C / (Math.pow(2.71828183, c2 / (A * TReflejada1 + B)) - 1);
    TReflejada3 = TReflejada2 * TReflejada2 * (c2 * A / (C * Math.pow(A * TReflejada1 + B, 2))) * Math.pow(2.71828183, (c2 / (A * TReflejada1 + B)));

    TNominal1 = temperatura + 273.15;
    TNominal2 = C / (Math.pow(2.71828183, c2 / (A * TNominal1 + B)) - 1);
    TNominal3 = TNominal2 * TNominal2 * (c2 * A / (C * Math.pow(A * TNominal1 + B, 2))) * Math.pow(2.71828183, (c2 / (A * TNominal1 + B)));

    $.jGrowl(C + "/" + (Math.pow(2.71828183, c2 / (A * TNominal1 + B)) - 1), { life: 1500, theme: 'growl-success', header: '' });

    TIBC1 = promediotibc + 273.15;
    TIBC2 = C / (Math.pow(2.71828183, c2 / (A * TIBC1 + B)) - 1);
    TIBC3 = TIBC2 * TIBC2 * (c2 * A / (C * Math.pow(A * TIBC1 + B, 2))) * Math.pow(2.71828183, (c2 / (A * TIBC1 + B)));





    $("#TFuente" + x + "1").html(TFuente1);
    $("#TFuente" + x + "2").html(TFuente2);
    $("#TFuente" + x + "3").html(TFuente3);

    $("#TReflejada" + x + "1").html(TReflejada1);
    $("#TReflejada" + x + "2").html(TReflejada2);
    $("#TReflejada" + x + "3").html(TReflejada3);

    $("#TNominal" + x + "1").html(TNominal1);
    $("#TNominal" + x + "2").html(TNominal2);
    $("#TNominal" + x + "3").html(TNominal3);

    $("#TIBC" + x + "1").html(TIBC1);
    $("#TIBC" + x + "2").html(TIBC2);
    $("#TIBC" + x + "3").html(TIBC3);

    FREmiValCoeLib = 1 / EmisividadIBC * (TFuente2 - TReflejada2) / TFuente3;
    FRUniValCoeLib = 1;
    FREstValCoeLib = 1;
    FRInsValCoeLib = 1;
    FRDerValCoeLib = 1;
    FRRadValCoeLib = 1;
    FRIntValCoeLib = 1;
    FRConValCoeLib = 1;

    $("#FREmiValCoeLib" + x).val(FREmiValCoeLib);
    $("#FRUniValCoeLib" + x).val(FRUniValCoeLib);
    $("#FREstValCoeLib" + x).val(FREstValCoeLib);
    $("#FRInsValCoeLib" + x).val(FRInsValCoeLib);
    $("#FRDerValCoeLib" + x).val(FRDerValCoeLib);
    $("#FRRadValCoeLib" + x).val(FRRadValCoeLib);

    $("#FRIntValCoeLib" + x).val(FRIntValCoeLib);
    $("#FRConValCoeLib" + x).val(FRConValCoeLib);

    FREmiValInsSta = FREmiValIns / FREmiFac;
    FRUniValInsSta = UniformidadP / FRUniFac;
    FREstValInsSta = EstabilidadP / FREstFac;
    FRInsValInsSta = FRInsValIns / FRInsFac;
    FRDerValInsSta = DeriVDato2 / FRDerFac;
    FRRadValInsSta = DeriVDato3 / FRRadFac;
    FRIntValInsSta = DeriVDato4 / FRIntFac;
    FRConValInsSta = DeriVDato5 / FRConFac;

    $("#FREmiValInsSta" + x).val(FREmiValInsSta);
    $("#FRUniValInsSta" + x).val(FRUniValInsSta);
    $("#FREstValInsSta" + x).val(FREstValInsSta);
    $("#FRInsValInsSta" + x).val(FRInsValInsSta);
    $("#FRDerValInsSta" + x).val(FRDerValInsSta);
    $("#FRRadValInsSta" + x).val(FRRadValInsSta);
    $("#FRIntValInsSta" + x).val(FRIntValInsSta);
    $("#FRConValInsSta" + x).val(FRConValInsSta);


    //mALO

    FREmiValCon = Math.abs(FREmiValCoeLib * FREmiValInsSta);
    FRUniValCon = Math.abs(FRUniValCoeLib * FRUniValInsSta);
    FREstValCon = Math.abs(FREstValCoeLib * FREstValInsSta);
    FRInsValCon = Math.abs(FRInsValCoeLib * FRInsValInsSta);
    FRDerValCon = Math.abs(FRDerValCoeLib * FRDerValInsSta);
    FRRadValCon = Math.abs(FRRadValCoeLib * FRRadValInsSta);
    FRIntValCon = Math.abs(FRIntValInsSta * FRIntValInsSta);
    FRConValCon = Math.abs(FRConValCoeLib * FRConValInsSta);

    $("#FREmiValCon" + x).val(FREmiValCon);
    $("#FRUniValCon" + x).val(FRUniValCon);
    $("#FREstValCon" + x).val(FREstValCon);
    $("#FRInsValCon" + x).val(FRInsValCon);
    $("#FRDerValCon" + x).val(FRDerValCon);
    $("#FRRadValCon" + x).val(FRRadValCon);
    $("#FRIntValCon" + x).val(FRIntValCon);
    $("#FRConValCon" + x).val(FRConValCon);



    

    FRTemFue = FRInsValEst + promediotp;
    $("#FRTemFue" + x).val(FRTemFue);

    FTTamFueValEst = 0;
    FTTemAmbValEst = 0;
    FTRuiAmbValEst = 0;
    FTRepValEst = promediotibc;
    FTResValEst = 0;
    FTAbsValEst = 0;

    $("#FTTamFueValEst" + x).val(FTTamFueValEst);
    $("#FTTemAmbValEst" + x).val(FTTemAmbValEst);
    $("#FTRuiAmbValEst" + x).val(FTRuiAmbValEst);
    $("#FTRepValEst" + x).val(FTRepValEst);
    $("#FTResValEst" + x).val(FTResValEst);
    $("#FTAbsValEst" + x).val(FTAbsValEst);

    SBAdiDato3 = NumeroDecimal($("#SBAdiDato3").val());
    SBAdiDato4 = NumeroDecimal($("#SBAdiDato4").val());

    FTTamFueValIns = SBAdiDato3 * (1 - Math.pow(2.71828183, SBAdiDato4 * 0.001));
    FTTemAmbValIns = 0;
    FTRuiAmbValIns = 0;
    FTRepValIns = desviaciontibc;
    FTResValIns = 0.1 / 2;
    FTAbsValIns = 0.0006;

    $("#FTTamFueValIns" + x).val(FTTamFueValIns);
    $("#FTTemAmbValIns" + x).val(FTTemAmbValIns);
    $("#FTRuiAmbValIns" + x).val(FTRuiAmbValIns);
    $("#FTRepValIns" + x).val(FTRepValIns);
    $("#FTResValIns" + x).val(FTResValIns);
    $("#FTAbsValIns" + x).val(FTAbsValIns);

    FTTamFueFac = Math.sqrt(12);
    FTTemAmbFac = Math.sqrt(12);
    FTRuiAmbFac = Math.sqrt(12);
    FTRepFac = Math.sqrt(5);
    FTResFac = Math.sqrt(3);
    FTAbsFac = 2;

    $("#FTTamFueFac" + x).val(FTTamFueFac);
    $("#FTTemAmbFac" + x).val(FTTemAmbFac);
    $("#FTRuiAmbFac" + x).val(FTRuiAmbFac);
    $("#FTRepFac" + x).val(FTRepFac);
    $("#FTResFac" + x).val(FTResFac);
    $("#FTAbsFac" + x).val(FTAbsFac);

    FTTamFueValInsSta = FTTamFueValIns / FTTamFueFac;
    FTTemAmbValInsSta = FTTemAmbValIns / FTTemAmbFac;
    FTRuiAmbValInsSta = FTRuiAmbValIns / FTRuiAmbFac;
    FTRepValInsSta = FTRepValIns / FTRepFac;
    FTResValInsSta = FTResValIns / FTResFac;
    FTAbsValInsSta = FTAbsValIns / FTAbsFac;

    $("#FTTamFueValInsSta" + x).val(FTTamFueValInsSta);
    $("#FTTemAmbValInsSta" + x).val(FTTemAmbValInsSta);
    $("#FTRuiAmbValInsSta" + x).val(FTRuiAmbValInsSta);
    $("#FTRepValInsSta" + x).val(FTRepValInsSta);
    $("#FTResValInsSta" + x).val(FTResValInsSta);
    $("#FTAbsValInsSta" + x).val(FTAbsValInsSta);

    FTTamFueGraLib = 1000000;
    FTTemAmbGraLib = 1000000;
    FTRuiAmbGraLib = 1000000;
    FTRepGraLib = 9;
    FTResGraLib = 1000000;
    FTAbsGraLib = 1000000;

    $("#FTTamFueGraLib" + x).val(FTTamFueGraLib);
    $("#FTTemAmbGraLib" + x).val(FTTemAmbGraLib);
    $("#FTRuiAmbGraLib" + x).val(FTRuiAmbGraLib);
    $("#FTRepGraLib" + x).val(FTRepGraLib);
    $("#FTResGraLib" + x).val(FTResGraLib);
    $("#FTAbsGraLib" + x).val(FTAbsGraLib);

    angulo = NumeroDecimal($("#SBDato2").val());
    c2 = NumeroDecimal($("#SBDato8").val());

    FTTamFueValCoeLib = angulo * Math.pow(promediotibc,2) / c2;
    FTTemAmbValCoeLib = 1;
    FTRuiAmbValCoeLib = 1;
    FTRepValCoeLib = 1; 
    FTResValCoeLib = -1;
    FTAbsValCoeLib = TNominal2 / TNominal3;

    $("#FTTamFueValCoeLib" + x).val(FTTamFueValCoeLib);
    $("#FTTemAmbValCoeLib" + x).val(FTTemAmbValCoeLib);
    $("#FTRuiAmbValCoeLib" + x).val(FTRuiAmbValCoeLib);
    $("#FTRepValCoeLib" + x).val(FTRepValCoeLib);
    $("#FTResValCoeLib" + x).val(FTResValCoeLib);
    $("#FTAbsValCoeLib" + x).val(FTAbsValCoeLib);

    FTTamFueValCon = Math.abs(FTTamFueValCoeLib * FTTamFueValInsSta);
    FTTemAmbValCon = Math.abs(FTTemAmbValCoeLib * FTTemAmbValInsSta);
    FTRuiAmbValCon = Math.abs(FTRuiAmbValCoeLib * FTRuiAmbValInsSta);
    FTRepValCon = FTRepValCoeLib * FTRepValInsSta;
    FTResValCon = FTResValCoeLib * FTResValInsSta;
    FTAbsValCon = FTAbsValCoeLib * FTAbsValInsSta;

    $("#FTTamFueValCon" + x).val(FTTamFueValCon);
    $("#FTTemAmbValCon" + x).val(FTTemAmbValCon);
    $("#FTRuiAmbValCon" + x).val(FTRuiAmbValCon);
    $("#FTRepValCon" + x).val(FTRepValCon);
    $("#FTResValCon" + x).val(FTResValCon);
    $("#FTAbsValCon" + x).val(FTAbsValCon);

    //CORRECCION

    FREmiValCon = Math.abs(FREmiGraLib * FREmiValInsSta);
    FRUniValCon = Math.abs(FRUniGraLib * FRUniValInsSta);
    FREstValCon = Math.abs(FREstGraLib * FREstValInsSta);
    FRInsValCon = Math.abs(FRInsGraLib * FRInsValInsSta);
    FRDerValCon = Math.abs(FRDerGraLib * FRDerValInsSta);
    FRRadValCon = Math.abs(FRRadGraLib * FRRadValInsSta);
    FRIntValCon = Math.abs(FRIntGraLib * FRIntValInsSta);
    FRConValCon = Math.abs(FRConGraLib * FRConValInsSta);

    
    suma = (Math.pow(FRRepValCon, 4) / FRRepGraLib) + (Math.pow(FREmiValCon, 4) / FREmiGraLib) + (Math.pow(FRUniValCon, 4) / FRUniGraLib) + (Math.pow(FREstValCon, 4) / FREstGraLib) +
        (Math.pow(FRInsValCon, 4) / FRInsGraLib) + (Math.pow(FRDerValCon, 4) / FRDerGraLib) + (Math.pow(FRRadValCon, 4) / FRRadGraLib) +
        (Math.pow(FRIntValCon, 4) / FRIntGraLib) + (Math.pow(FRConValCon, 4) / FRConGraLib);

    suma += (Math.pow(FTTamFueValCon, 4) / FTTamFueGraLib) + (Math.pow(FTTemAmbValCon, 4) / FTTemAmbGraLib) + (Math.pow(FTRuiAmbValCon, 4) / FTRuiAmbGraLib) + (Math.pow(FTRepValCon, 4) / FTRepGraLib) +
        (Math.pow(FTResValCon, 4) / FTResGraLib) + (Math.pow(FTAbsValCon, 4) / FTAbsGraLib);

    FTCorValCon = Math.pow(FTTamFueValCon, 2) + Math.pow(FTTemAmbValCon, 2) + Math.pow(FTRuiAmbValCon, 2) + Math.pow(FTRepValCon, 2) +
        Math.pow(FTResValCon, 2) + Math.pow(FTAbsValCon, 2);

    FTCorGraLib = Math.pow(FTCorValCon, 4) / suma;
    FTCorFac = Calcular_TStudent(FTCorGraLib);
    FTCorValIns = FTCorFac * FTCorValCon;
    FTCorValEst = promediotibc - FRTemFue;


    $("#FTCorValCon" + x).val(FTCorValCon);
    $("#FTCorGraLib" + x).val(FTCorGraLib);
    $("#FTCorFac" + x).val(FTCorFac);
    $("#FTCorValIns" + x).val(FTCorValIns);
    $("#FTCorValEst" + x).val(FTCorValEst);

    FTResFue = FRTemFue;
    FTResCor = FTCorValEst;
    FTResK = FTCorFac;
    FTResIncVal = FTCorValIns;

    $("#FTResFue" + x).val(formato_numero(FTResFue,4,".",",",""));
    $("#FTResCor" + x).val(formato_numero(FTResCor, _DE,_CD,_CM,""));
    $("#FTResK" + x).val(formato_numero(FTResK, _DE,_CD,_CM,""));
    $("#FTResIncVal" + x).val(formato_numero(FTResIncVal, 1, ".", ",", ""));
       
    

}

$('select').select2();
DesactivarLoad();
