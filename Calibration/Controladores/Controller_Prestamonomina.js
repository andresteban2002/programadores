angular.module('myApp', ['ui', 'ngSanitize', 'ngTable']).controller('ControlPrestamonomina', function ($scope, $ocLazyLoad, $rootScope, General, NgTableParams) {

    function initTabs() {
        tabClasses = ["", ""];
    }

    $scope.getTabClass = function (tabNum) {
        return tabClasses[tabNum];
    };

    $scope.getTabPaneClass = function (tabNum) {
        return "tab-pane " + tabClasses[tabNum];
    }

    $scope.setActiveTab = function (tabNum) {
        initTabs();
        tabClasses[tabNum] = "active";
    };

$scope.IdEmpleado = 0;
$scope.IdPrestamo = 0;
$scope.DocumentoEmp = "";
$scope.CanCuotas = 0;






$scope.Combos={
    empleado:"",
    cargo:""
}

$scope.CargarCombos = function () {
        $rootScope.global.CargandoPrinicipal = true;
        General.CargarCombo(26, null).then(function (response) {$scope.Combos.cargo = response.data;});   
        General.CargarCombo(29, null).then(function (response) {$scope.Combos.empleado = response.data;});
        $rootScope.global.CargandoPrinicipal = false;

}



 $scope.Empleado = {
        id: 0,
        documento: "",
        
    }

$scope.InicializarRegistro = function (){
    $scope.Registro={
        idempleado:0,
        TipoDocumento:"",
        Documento:"",
        Empleado:"",
        Nombres:"",
        Cargo:"",
        Sueldo:"",
        Desembolso:"EFECTIVO",
        Prima:"",
        FechaIniCuota:"",
        FormaPago:"QUINCENAL",
        Cuota:"",
        ValorCuota:"",
        Monto:"",
        Descripcion:"",
        TipoPrestamo:"CONVENIO",
        Banco:"",
        TipoCuenta:"",
        Cuenta:"",
        Telefonos:"",
        Celular:"",
        Email:"",
        Prestamo:"",
        FechaInicio: {
            value: new Date(output2 + " 00:00"),
            id: output2
        },
        FechaFinal: {
            value: new Date(output2 + " 00:00"),
            id: output2
        }
       
    }
}




 $scope.Consulta = {
     empleado:"",
     cargo:"",
     tipo:"",
     estado:"TODOS"
     
 }





 $scope.ConsultarPrestamos = function () {
        
         var parametros ="opcion=ConsultarPrestamos&" + General.ConvertirParametros($scope.Consulta);
        General.LlamarAjax("Recursohumano", parametros, true, "", 1).then(function (response) {
            $rootScope.TablaPrestamos = new NgTableParams({count: 10}, {dataset: response});

        });
    }
    
    
    
     $scope.ObtenerConsulta = function (registro) {
        console.log(registro);
        $scope.LimpiarDatos();
        $scope.IdPrestamo = registro.id;
        $scope.BuscarEmpleado();
        $scope.setActiveTab(1);

    }



$scope.CambioFecha = function (registro, tipo) {
        switch (registro) {
            case 1:
                switch (tipo) {
                    case 1:
                        $scope.Registro.FechaInicio.id = General.Obtener_Fecha($scope.Registro.FechaInicio.value);
                        break;
                    case 2:
                        $scope.Registro.FechaFinal.id = General.Obtener_Fecha($scope.Registro.FechaFinal.value);
                        break;    
                }
        }
   }



$scope.LimpiarTodo= function() {
    $scope.Documento="";
    $scope.DocumentoEmp = "";
    $scope.LimpiarDatos();
}

$scope.LimpiarDatos = function () {
        $scope.Empleado = {Documento: ""};
        $scope.InicializarRegistro();
       
    }

$scope.ValidarEmpleado= function(documento) {
    if ($.trim($scope.DocumentoEmp) != "") {
        if ($scope.DocumentoEmp != documento) {
            $scope.DocumentoEmp = "";
            $scope.Registro.TipoDocumento="";
            $scope.Registro.Nombres="";
            
            $scope.LimpiarDatos();
        }
    }
} 


$scope.BuscarEmpleado= function() {
    var documento = $.trim($scope.Registro.Documento);
    if ($.trim(documento) == ""||$scope.DocumentoEmp == documento)
        return false;    

    //$scope.LimpiarDatos();
    $scope.DocumentoEmp = documento;
    var parametros = "opcion=BuscarEmpleados&" + "documento=" + documento;
    General.LlamarAjax("Recursohumano", parametros,false,"",1).then(function (data) {
     
    if (data.length > 0) {
        $scope.IdEmpleado = $.trim(data[0].id);
        $scope.Registro.TipoDocumento=data[0].tipodocumento;
        $scope.Registro.Cargo=data[0].cargo;
        $scope.Registro.Banco=data[0].banco;
        $scope.Registro.Sueldo=data[0].sueldo;
        $scope.Registro.Cuenta=data[0].cuenta;
        $scope.Registro.TipoCuenta=data[0].tipocuenta;
        $scope.Registro.Nombres=data[0].nombrecompleto;
        $scope.Registro.FechaInicio.id = General.Obtener_Fecha($scope.Registro.FechaInicio.value);
        $scope.Registro.FechaFinal.id = General.Obtener_Fecha($scope.Registro.FechaFinal.value);
        $scope.Registro.Telefonos=data[0].telefono;
        $scope.Registro.Celular=data[0].celular;
        $scope.Registro.Email=data[0].email;
        
    }
    });
}   
   
  
$scope.CambioFormaPago= function(forma) {
    var cuota = $scope.CanCuotas;
    var opcion = "";
    if (forma == "MENSUAL")
        cuota = $scope.CanCuotas / 2;
    for (var x = 1; x <= cuota; x++) {
        opcion += "<option value='" + x + "'>" + x + "</option>";
    }
    $("#NCuota").html(opcion);
    $("#NCuota").val(1).trigger("change");
    
}  


$scope.CalcularCuota= function(Caja) {
    if (Caja != "")
        //ValidarTexto(Caja, 1);
    var prestamo = $rootScope.NumeroDecimal($scope.Registro.monto == "");
    var cuota = $scope.CanCuotas;
    var prima = $rootScope.NumeroDecimal($scope.Registro.prima);
    var formapago = $scope.Registro.formapago;

    if (prima > prestamo) {
        $scope.Registro.Prima="0";
        prima = 0;
    }
    
    var monto = prestamo - prima;
    cuota = monto / cuota;
    
    $scope.Registro.ValorCuota = General.formato_numero(cuota, 0,_CD,_CM,"");  
}


$scope.GuardarPrestamo= function() {
   


    if ($scope.IdEmpleado == 0) {
        $("#Documento").focus();
        swal("Acci�n Cancelada", "Debe de seleccionar un empleado", "success");
        return false;
    }

    if ($scope.Registro.Prestamo == 0) {
        $("#Monto").focus();
        swal("Acci�n Cancelada", "Debe de ingresar el monto del pr�stamo", "success");
        return false;
    }

    if ($scope.Registro.FormaPago == "QUINCENAL")
        sueldo = sueldo / 2;

    if ($scope.Registro.Prestamo == 0) {
        $("#Monto").focus();
        swal("Acci�n Cancelada", "Debe de ingresar el monto del pr�stamo", "success");
        return false;
    }



    var parametros ="opcion=GuardarPrestamo&" + General.ConvertirParametros($scope.Registro);
    General.LlamarAjax("Recursohumano",parametros,true,"",1).then(function (response) {
    var datos = $.trim(response).split("|");
    if (datos["0"] == "0") {
        $scope.IdPrestamo = datos[2] * 1;
        $scope.IdPrestamo=IdPrestamo;
        swal("", datos[1], "success");
   
    }else {
        swal("Acci�n Cancelada", datos[1], "warning");
        }
    });
}




$scope.CalTipoPrestamo= function(prestamo) {
    var formapago = $scope.Registro.FormaPago;
    var opcion = "";
    switch (prestamo) {
        case "VALE":
            $scope.CanCuotas = 1;
            $("#FormaPago").html("<option value='QUINCENAL'>QUINCENAL</option>");
            $("#Prima").val("0");
            $("#Prima").prop("readonly", true);
            break;
        case "PRESTAMO":
            $scope.CanCuotas = 6;
            $("#FormaPago").html("<option value='QUINCENAL'>QUINCENAL</option><option value='MENSUAL'>MENSUAL</option>");
            $("#Prima").val("0");
            $("#Prima").prop("readonly", false);
            break;
        case "CONVENIO":
            $scope.CanCuotas = 50;
            $scope.Registro.FormaPago("<option value='QUINCENAL'>QUINCENAL</option><option value='MENSUAL'>MENSUAL</option>");
           ($scope.Registro.Prima == "0");
            $scope.Registro.Prima.prop("readonly", false);
            break;
    }
    for (var x = 1; x <= $scope.CanCuotas; x++) {
        opcion += "<option value='" + x + "'>" + x + "</option>";  
    }
    $("#NCuota").html(opcion);
}



   
 $scope.InicializarRegistro(); 
 $scope.CargarCombos();
 $rootScope.InicializarFormulario(16);
 $scope.setActiveTab(1);


});
