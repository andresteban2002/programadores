﻿
$("#Equ_Magnitud, #Int_Magnitud").html(CargarCombo(1, 1));
$("#Mag_Precio").html(CargarCombo(36, 1));
$("#Mod_Marca").html(CargarCombo(3, 1));
$("#Int_Medida").html(CargarCombo(41, 1));
$("#Mag_Estado, #Equ_Estado, #Int_Estado, #Mar_Estado, #Mod_Estado").html(CargarCombo(37, 0));

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab); 

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Documento") {
            if ($.trim($(this).val()) != "") {

                BuscarCliente($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function TablaMagnitudes() {
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=1&id=0');
    var datasedjson = JSON.parse(datos);
    $('#TablaMagnitudes').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "abreviatura" },
            { "data": "descripcion" },
            { "data": "codigoini" },
            { "data": "desprecio" },
            { "data": "desestado" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function LimpiarMagnitudes() {
    $("#Mag_Precio").val("").trigger("change");
    $("#Mag_Estado").val("1").trigger("change");
    $("#Mag_Descripcion").val("")
    $("#Mag_CodInicial").val("")
    $("#Mag_Id").val("0")
    $("#Mag_Inicial").val("");
}

$("#TablaMagnitudes > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Configuracion','TablaConfiguracion&tipo=1&id=' + numero);
    var data = JSON.parse(datos);
    $("#Mag_Precio").val(data[0].tipoprecio).trigger("change");
    $("#Mag_Estado").val(data[0].estado).trigger("change");
    $("#Mag_Descripcion").val(data[0].descripcion);
    $("#Mag_CodInicial").val(data[0].codigoini);
    $("#Mag_Id").val(data[0].id);
    $("#Mag_Inicial").val(data[0].abreviatura);
});


$("#formMagnitud").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Configuracion","opcion=GuardarMagnitudes&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaMagnitudes();
        LimpiarMagnitudes();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

});


//************************EQUIPOS********************************

function LimpiarEquipos() {
    $("#Equ_Estado").val("1").trigger("change");
    $("#Equ_Descripcion").val("")
    $("#Equ_Id").val("0")
    $("#Equ_Codigo").val("0");
}

function CambioMagEquipo(magnitud) {
    id = $("#Equ_Id").val() * 1
    TablaEquipos();
    if (magnitud * 1 == 0) {
        $("#Equ_Codigo").val("0");
        return false;
    }
    if (id == 0) {
        $("#Equ_Codigo").val(LlamarAjax("Configuracion","opcion=MaximoCodigo&magnitud=" + magnitud));
    }
}

$("#TablaEquipos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=2&id=' + numero);
    var data = JSON.parse(datos);
    $("#Equ_Magnitud").val(data[0].idmagnitud).trigger("change");
    $("#Equ_Estado").val(data[0].estado).trigger("change");
    $("#Equ_Descripcion").val(data[0].descripcion);
    $("#Equ_Codigo").val(data[0].codigo);
    $("#Equ_Id").val(data[0].id);
});


$("#formEquipos").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Configuracion","opcion=GuardarEquipos&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaEquipos();
        LimpiarEquipos();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

});

   

function TablaEquipos() {
    var magnitud = $("#Equ_Magnitud").val() * 1;
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=2&id=0&magnitud=' + magnitud);
    var datasedjson = JSON.parse(datos);
    $('#TablaEquipos').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "magnitud" },
            { "data": "codigo" },
            { "data": "descripcion" },
            { "data": "desestado" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function EliminarEquipo(id, equipo) {
    var accion = 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar el equipo ' + equipo + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,
        
        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Configuracion","opcion=EliminarEquipos&Id=" + id + "&IdReemplazo=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else {
                            accion = 0;
                            resolve();
                        }
                            
                    })
            })
        }
    }]).then(function (data) {
        if (accion == 1) {
            TablaEquipos();
            LimpiarEquipos();
        } else {
            $("#TituloReemplazar").html("Reemplazar Equipo");
            $("#TextoReemplazo").html("Reemplazar equipo " + equipo);
            $("#OpcionReemplazo").html(CargarCombo(58, 1));
            $("#NumeroReemplazo").val(1);
            $("#IdReemplazo").val(id);
            $("#modalReemplazo").modal("show");
        }
    });
}

function ReemplazarEquipos() {
    var numero = $("#NumeroReemplazo").val() * 1;
    var idreemplazo = $("#OpcionReemplazo").val() * 1;
    var id = $("#IdReemplazo").val() * 1;
    switch (numero) {
        case 1:
            datos = LlamarAjax("Configuracion","opcion=EliminarEquipos&Id=" + id + "&IdReemplazo=" + idreemplazo).split("|");
            if (datos[0] == "0") {
                TablaEquipos();
                LimpiarEquipos();
                swal("", datos[1], "success");
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
            break;
        case 2:
            datos = LlamarAjax("Configuracion","opcion=EliminarIntervalo&Id=" + id + "&IdReemplazo=" + idreemplazo).split("|");
            if (datos[0] == "0") {
                TablaIntervalos();
                LimpiarIntervalos();
                swal("", datos[1], "success");
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
            break;
        case 3:
            datos = LlamarAjax("Configuracion","opcion=EliminarMarcas&Id=" + id + "&IdReemplazo=" + idreemplazo).split("|");
            if (datos[0] == "0") {
                TablaMarcas();
                LimpiarMarcas();
                $("#Mod_Marca").html(CargarCombo(3, 1));
                swal("", datos[1], "success");
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
            break;
        case 4:
            datos = LlamarAjax("Configuracion","opcion=EliminarModelos&Id=" + id + "&IdReemplazo=" + idreemplazo).split("|");
            if (datos[0] == "0") {
                TablaModelos();
                LimpiarModelos();
                swal("", datos[1], "success");
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
            break;
    }
    $("#modalReemplazo").modal("hide");
    
}

//*********************MEDIDAS*****************************************

function TablaMedidas() {
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=3&id=0');
    var datasedjson = JSON.parse(datos);
    $('#TablaMedidas').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "descripcion" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function LimpiarMedidas() {
    $("#Med_Descripcion").val("")
    $("#Med_Id").val("0")
}


$("#TablaMedidas> tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=3&id=' + numero);
    var data = JSON.parse(datos);
    $("#Med_Descripcion").val(data[0].descripcion);
    $("#Med_Id").val(data[0].id);
});


$("#formMedidas").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Configuracion","opcion=GuardarMedidas&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaMedidas();
        LimpiarMedidas();
        $("#Int_Medida").html(CargarCombo(41, 1));
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

});

function EliminarMedida(id, Medida) {
    var accion = 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar la medida ' + Medida + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Configuracion","opcion=EliminarMedida&Id=" + id)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else
                            reject(datos[1]); 

                    })
            })
        }
    }]).then(function (data) {
        TablaMedidas();
        LimpiarMedidas();
    });
}

//**************************** INTERVALO *************************************

function TablaIntervalos() {
    var magnitud = $("#Int_Magnitud").val() * 1;
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=4&id=0&magnitud=' + magnitud);
    var datasedjson = JSON.parse(datos);
    $('#TablaIntervalos').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "magnitud" },
            { "data": "medida" },
            { "data": "desde" },
            { "data": "hasta" },
            { "data": "desestado" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function LimpiarIntervalos() {
    $("#Int_Id").val("0")
    $("#Int_Medida").val("").trigger("change");
    $("#Int_Desde").val("0");
    $("#Int_Hasta").val("0")
    $("#Int_Estado").val("1").trigger("change");
}


$("#TablaIntervalos> tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=4&id=' + numero);
    var data = JSON.parse(datos);
    $("#Int_Magnitud").val(data[0].idmagnitud).trigger("change");
    $("#Int_Medida").val(data[0].medida).trigger("change");
    $("#Int_Estado").val(data[0].estado).trigger("change");
    $("#Int_Desde").val(data[0].desde);
    $("#Int_Hasta").val(data[0].hasta);
    $("#Int_Id").val(data[0].id);
});


$("#formIntervalos").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Configuracion","opcion=GuardarIntervalos&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaIntervalos();
        LimpiarIntervalos();
        swal("", datos[1], "success");
    }else{
        swal("Acción Cancelada", datos[1], "warning");
    }

});

function EliminarIntervalos(id, intervalo) {
    var accion = 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar el intervalo ' + intervalo + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Configuracion","opcion=EliminarIntervalo&Id=" + id + "&IdReemplazo=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else {
                            accion = 0;
                            resolve();
                        }

                    })
            })
        }
    }]).then(function (data) {
        if (accion == 1) {
            TablaIntervalos();
            LimpiarIntervalos();
        } else {
            $("#TituloReemplazar").html("Reemplazar Intervalo");
            $("#TextoReemplazo").html("Reemplazar Intervalor " + intervalo);
            $("#OpcionReemplazo").html(CargarCombo(57, 1));
            $("#NumeroReemplazo").val(2);
            $("#IdReemplazo").val(id);
            $("#modalReemplazo").modal("show");
        }
    });
}


//************************* MARCAS ******************************

function TablaMarcas() {
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=5&id=0');
    var datasedjson = JSON.parse(datos);
    $('#TablaMarcas').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "descripcion" },
            { "data": "desestado" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function LimpiarMarcas() {
    $("#Mar_Id").val("0")
    $("#Mar_Descripcion").val("");
    $("#Mar_Estado").val("1").trigger("change");
}


$("#TablaMarcas> tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=5&id=' + numero);
    var data = JSON.parse(datos);
    $("#Mar_Descripcion").val(data[0].descripcion);
    $("#Mar_Estado").val(data[0].estado).trigger("change");
    $("#Mar_Id").val(data[0].id);
});


$("#formMarca").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Configuracion","opcion=GuardarMarcas&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaMarcas();
        LimpiarMarcas();
        $("#Mod_Marca").html(CargarCombo(3, 1));
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

});

function EliminarMarca(id, marca) {
    var accion = 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar la marca ' + marca + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Configuracion","opcion=EliminarMarcas&Id=" + id + "&IdReemplazo=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else {
                            accion = 0;
                            resolve();
                        }

                    })
            })
        }
    }]).then(function (data) {
        if (accion == 1) {
            TablaMarcas();
            LimpiarMarcas();
            $("#Mod_Marca").html(CargarCombo(3, 1));
        } else {
            $("#TituloReemplazar").html("Reemplazar Marcas");
            $("#TextoReemplazo").html("Reemplazar Marcas " + marca);
            $("#OpcionReemplazo").html(CargarCombo(3, 1));
            $("#NumeroReemplazo").val(3);
            $("#IdReemplazo").val(id);
            $("#modalReemplazo").modal("show");
        }
    });
}


//******************************* MODELOS *************************************

function TablaModelos() {
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=6&id=0');
    var datasedjson = JSON.parse(datos);
    $('#TablaModelos').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "marca" },
            { "data": "descripcion" },
            { "data": "desestado" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function LimpiarModelos() {
    $("#Mod_Id").val("0")
    $("#Mod_Descripcion").val("");
    $("#Mod_Marca").val("");
    $("#Mod_Estado").val("1").trigger("change");
}


$("#TablaModelos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=6&id=' + numero);
    var data = JSON.parse(datos);
    $("#Mod_Descripcion").val(data[0].descripcion);
    $("#Mod_Marca").val(data[0].idmarca).trigger("change");
    $("#Mod_Estado").val(data[0].estado).trigger("change");
    $("#Mod_Id").val(data[0].id);
});


$("#formModelos").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Configuracion","opcion=GuardarModelos&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaModelos();
        LimpiarModelos();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

});

function EliminarModelo(id, modelo, idmarca) {
    var accion = 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar el modelo ' + modelo + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Configuracion","opcion=EliminarModelo&Id=" + id + "&IdReemplazo=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else {
                            accion = 0;
                            resolve();
                        }

                    })
            })
        }
    }]).then(function (data) {
        if (accion == 1) {
            TablaModelos();
            LimpiarModelos();
        } else {
            $("#TituloReemplazar").html("Reemplazar Marcas");
            $("#TextoReemplazo").html("Reemplazar Marcas " + marca);
            $("#OpcionReemplazo").html(CargarCombo(5, 1, "", idmarca));
            $("#NumeroReemplazo").val(4);
            $("#IdReemplazo").val(id);
            $("#modalReemplazo").modal("show");
        }
    });
}


TablaMagnitudes();
TablaEquipos();
TablaMedidas();
TablaIntervalos();
TablaMarcas();
TablaModelos();

$('select').select2();
DesactivarLoad();
