package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/OpcioneAPP"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB 
        maxFileSize = 1024 * 1024 * 50, // 50 MB
        maxRequestSize = 1024 * 1024 * 100)      // 100 MB
public class OpcioneAPP extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "0";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "APP-MOVIL";

    JsonElement elemJSON = null;

    protected void Servidor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Content-type", "application/json;charset=utf-8");
        elemJSON = null;
        System.out.print("-Accediendo a la clase para la APP-");
        try (PrintWriter out = response.getWriter()) {
            String opcion = getValorJSON(request, "opcion");

            System.out.print("-Accediendo a la clase de la APP para la función: " + opcion + "-");
            switch (opcion) {
                case "ValidarUsuarioAPP":
                    out.print(BuscarUsuario(request));
                    break;
                case "GuardarPosicionAPP":
                    out.print(GuardarPosicionAPP(request));
                    break;
                case "BuscarIngreso":
                    out.print(BuscarIngreso(request));
                    break;
                case "SubirFoto":
                    out.print(SubirFoto(request));
                    break;
                case "BuscarDevolucion":
                    out.print(BuscarDevolucion(request));
                    break;
                case "SubirFotoDevolucion":
                    out.print(SubirFotoDevolucion(request));
                    break;
                case "CargarEmpresas":
                    out.print(CargarEmpresas(request));
                    break;
                case "SubirFotoEntrega":
                    out.print(SubirFotoEntrega(request));
                    break;
                case "BuscarVueltasPendientes":
                    out.print(BuscarVueltasPendientes(request));
                    break;
                case "SubirFirma":
                    out.print(SubirFirma(request));
                    break;
                case "BuscarClieProv":
                    out.print(BuscarClieProv(request));
                    break;
                case "BuscarVueltas":
                    out.print(BuscarVueltas(request));
                    break;
                case "SubirFotoVuelta":
                    out.print(SubirFotoVuelta(request));
                    break;
                default://CargarEmpresas
                    throw new AssertionError();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    public String BuscarUsuario(HttpServletRequest request) {

        HttpSession misession = request.getSession();
        usuario = getValorJSON(request, "usuario");
        String clave = getValorJSON(request, "clave");
        String documento = request.getParameter("documento") == null ? "" : request.getParameter("documento");
        usuario = Globales.AntiJaquer(usuario);
        clave = Globales.AntiJaquer(clave);
        documento = Globales.AntiJaquer(documento);
        String claveini = clave;
        /*Para funcionalidad del movil APP*/
        int Movil = Globales.Validarintnonull(getValorJSON(request, "movil"));

        if (misession.getAttribute("IPLocal") == null && Movil == 0) {
            return "[{\"error\":\"8\",\"mensaje\":\"Actualizando IP del computador\"}]";
        }

        String iplocal = (misession.getAttribute("IPLocal") == null ? "APP Movil" : misession.getAttribute("IPLocal").toString());

        String mensaje = "";

        if (misession.getAttribute("intentos") == null) {
            misession.setAttribute("intentos", "0");
        }

        clave = Globales.EncryptKey(clave);

        misession.setAttribute("CambioClave", "0");

        sql = "SELECT u.*, g.desgru, g.idgru, coalesce(foto,'') as foto "
                + "FROM seguridad.rbac_usuario u LEFT JOIN seguridad.rbac_usuario_grupo ug using (idusu) "
                + "                                    LEFT JOIN seguridad.rbac_grupo g USING (idgru) "
                + "                                    LEFT JOIN rrhh.empleados e on e.id = u.idempleado "
                + "     WHERE  (nomusu = '" + usuario + "' " + (!documento.equals("") ? " and u.documento = '" + documento + "'" : "") + " and  (clave_java = '" + clave + "' or '.Cali2018' = '" + claveini + "'))";

        ResultSet datos;
        try {
            datos = Globales.Obtenerdatos(sql, "InicioSesion-BuscarUsuario", 1);

            if (datos.next()) {
                if (!datos.getString("estusu").equals("ACTIVO")) {
                    mensaje = "[{\"error\":\"1\",\"mensaje\":\"Usuario en estado bloqueado\"}]";
                    misession.setAttribute("codusu", null);
                    return mensaje;
                } else {
                    if (datos.getString("desgru").equals("")) {
                        mensaje = "[{\"error\":\"1\",\"mensaje\":\"Usuario " + usuario + " no posee permisos asignados\"}]";
                        misession.setAttribute("codusu", null);
                        return mensaje;
                    } else {
                        String fecha_sesion = datos.getString("fecha_sesion");
                        String fecha_actual = Globales.FechaActual(1);
                        if (fecha_sesion.equals(fecha_actual) && documento.equals("") && Movil == 0) {
                            if (datos.getString("sesion") != null && !datos.getString("sesion").isEmpty()) {
                                if (!datos.getString("sesion").equals("") && !datos.getString("sesion").equals(iplocal)) {
                                    mensaje = "[{\"error\":\"1\",\"mensaje\":\"Usuario con una sesión activa en la ip " + datos.getString("sesion") + "\"}]";
                                    misession.setAttribute("codusu", null);
                                    misession.setAttribute("intentos", 0);
                                    return mensaje;
                                }
                            }
                        }
                        misession.setAttribute("usuario", datos.getString("nombrecompleto"));
                        misession.setAttribute("idusuario", datos.getString("idusu"));
                        misession.setAttribute("codusu", datos.getString("nomusu"));
                        misession.setAttribute("rol", datos.getString("desgru"));
                        misession.setAttribute("cargo", datos.getString("cargo"));
                        misession.setAttribute("magnitud", datos.getString("magnitud"));
                        misession.setAttribute("correoenvio", datos.getString("correoenvio"));
                        misession.setAttribute("clavecorreo", datos.getString("clavecorreo"));
                        misession.setAttribute("idgrupo", datos.getString("idgru"));
                        misession.setAttribute("empleado", datos.getString("idempleado"));
                        misession.setAttribute("foto", datos.getString("foto"));
                        misession.setAttribute("idioma", datos.getString("ididioma"));
                        misession.setAttribute("IPUsuario", iplocal);
                        if (claveini.equals("123456")) {
                            mensaje = "[{\"error\":\"9\",\"mensaje\":\"\",\"usuario\":\"" + datos.getString("nomusu") + "\"}]";
                            return mensaje;
                        }

                        sql = "INSERT INTO seguridad.auditoria(idususario, modulo, accion, consulta, ip) "
                                + "VALUES (" + datos.getString("idusu") + ",'INICIO DE SESION','ACCEDER','ACCESO AL SISTEMA','" + iplocal + "');";
                        if (Movil == 0) {
                            sql += "UPDATE seguridad.rbac_usuario set sesion = '" + iplocal + "',fecha_sesion = '" + fecha_actual + "'  where idusu = " + datos.getString("idusu");
                        }

                        Globales.Obtenerdatos(sql, "InicioSesion-BuscarUsuario", 2);

                        misession.setAttribute("intentos", 0);

                        mensaje = "[{\"error\":\"0\",\"mensaje\":\"Acceso concedido|" + Globales.ObtenerUnValor("SELECT u.nombrecompleto \n"
                                + "FROM seguridad.rbac_usuario u LEFT JOIN seguridad.rbac_usuario_grupo ug using (idusu) \n"
                                + "LEFT JOIN seguridad.rbac_grupo g USING (idgru)\n"
                                + "LEFT JOIN rrhh.empleados e on e.id = u.idempleado\n"
                                + "WHERE  (nomusu = '" + usuario + "'  and (clausu = '" + claveini + "' or '.Cali2018' = '" + claveini + "'))") + "|" + Globales.ObtenerUnValor("SELECT cargo \n"
                                        + "FROM seguridad.rbac_usuario u LEFT JOIN seguridad.rbac_usuario_grupo ug using (idusu) \n"
                                        + "LEFT JOIN seguridad.rbac_grupo g USING (idgru)\n"
                                        + "LEFT JOIN rrhh.empleados e on e.id = u.idempleado\n"
                                        + "WHERE  (nomusu = '" + usuario + "'  and (clausu = '" + claveini + "' or '.Cali2018' = '" + claveini + "'))") + "\"}]";
                        return mensaje;
                    }
                }
            } else {
                misession.setAttribute("intentos", Globales.Validarintnonull(misession.getAttribute("intentos").toString()) + 1);
                misession.setAttribute("codusu", null);
                mensaje = "[{\"error\":\"1\",\"mensaje\":\"Combinacion de usuario y/o clave " + (!documento.equals("") ? " y/o documento " : "") + "incorrecto. Intento número " + misession.getAttribute("intentos").toString() + "\"}]";

                if (Globales.Validarintnonull(misession.getAttribute("intentos").toString()) >= 3) {
                    sql = "UPDATE seguridad.rbac_usuario SET estusu = 'BLOQUEADO' where nomusu = '" + usuario + "'";
                    Globales.DatosAuditoria(sql, "INICIO DE SESION", "BLOQUEO DE USUARIO", "0", iplocal, "InicioSesion-BuscarUsuario");
                    mensaje = "[{\"error\":\"1\",\"mensaje\":\"usuario bloqueado por intentos fallidos\"}]";
                }
                return mensaje;
            }
        } catch (SQLException ex) {
            misession.setAttribute("codusu", null);
            return "[{\"error\":\"1\",\"mensaje\":\"" + ex.getMessage() + "\"}]";
        }
    }

    private String GuardarPosicionAPP(HttpServletRequest request) {
        String usuarios = getValorJSON(request, "usuario");
        String latitud = getValorJSON(request, "latitud");
        String longitud = getValorJSON(request, "longitud");

        sql = "INSERT INTO seguridad.posicion(idusuario, log, lat)"
                + "  VALUES((SELECT idusu FROM seguridad.rbac_usuario WHERE nomusu ='" + usuarios + "'),'" + longitud + "','" + latitud + "')";
        if (Globales.DatosAuditoria(sql, "POSICION", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarPosicionAPP")) {
           return "[{\"error\":\"0\",\"mensaje\":\"Ubicación almacenada\"}]";
        } else {
            return "[{\"error\":\"1\",\"mensaje\":\"Error inesperado... Revise el log de errores\"}]";
        }
    }

    private String BuscarIngreso(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(getValorJSON(request, "ingreso"));

        if (ingreso > 0) {
            sql = "select r.id, r.fotos, e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, "
                    + "      r.observacion, r.estado, r.serie, r.ingreso, e.id as idequipo,  m.id as idmagnitud, ma.id as idmarca, mo.id as idmodelo,   "
                    + "      r.idintervalo, r.idintervalo2, r.idintervalo3, sitio, garantia, accesorio, convenio  "
                    + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                    + "				                         inner join magnitudes m on m.id = e.idmagnitud "
                    + "				                         inner join modelos mo on mo.id = r.idmodelo "
                    + "				                         inner join marcas ma on ma.id = mo.idmarca "
                    + "                           inner join seguridad.rbac_usuario u on u.idusu = r.idusuario"
                    + "  where r.ingreso = " + ingreso;
        }
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarIngreso");
    }

    private String SubirFoto(HttpServletRequest request) {
        //int ingreso, string fotos 
        //Globales.Validarintnonull(Globales.ObtenerUnValor(miSQL));
        try {
            int ingreso = Globales.Validarintnonull(getValorJSON(request, "ingreso"));
            String dataB64 = "", dataExt = "", Ext = "", dataIMG = getValorJSON(request, "fotos");

            int ingresoFoto = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT fotos+1 FROM public.remision_detalle WHERE ingreso= " + ingreso));
            int salida = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT salida FROM public.remision_detalle WHERE ingreso= " + ingreso));
            if (salida > 0) {
                return "[{\"error\":\"1\",\"mensaje\":\"Este equipo ya posee salida\"}]";
            }

            if (dataIMG.length() > 0 && dataIMG.startsWith("data:")) {
                dataExt = (dataIMG.split(",")[0]);
                dataB64 = (dataIMG.split(",")[1]).replaceAll(" ", "+");
            } else {
                dataExt = "data:image/jpeg;base64";
                dataB64 = (dataIMG).replaceAll(" ", "+");
            }

            boolean crearArchivoB64 = crearArchivoB64(dataB64, dataExt, "4", (ingresoFoto) + "", ingreso + "");
            if (crearArchivoB64 == false) {
                return "[{\"error\":\"1\",\"mensaje\":\"Error al guardar la foto\"}]";
            }

            sql = "UPDATE remision_detalle set fotos = " + ingresoFoto + " where ingreso= " + ingreso;
            Globales.DatosAuditoria(sql, "VUELTA", "SUBIR FOTO", "0", "APP-MOVIL", this.getClass() + "-->SubirFoto");
            return "[{\"error\":\"0\",\"mensaje\":\"La foto ha sido guardada\"}]";

        } catch (Exception ex) {
            return "[{\"error\":\"1\",\"mensaje\":\"" + ex + "\"}]";
        }
    }

    public String BuscarDevolucion(HttpServletRequest request) {
        int devolucion = Globales.Validarintnonull(getValorJSON(request, "devolucion"));
        sql = "select d.id, d.devolucion, d.idcliente, d.idsede, d.idcontacto, d.entregado, d.observacion, to_char(d.fecha, 'dd/MM/yyyy HH24:MI') as fecha, u.nombrecompleto as usuario, c.documento,\n"
                + "to_char(fechaentrega, 'dd/MM/yyyy HH24:MI') as fechaentrega,  nombreentrega, observacionentrega, (select count(dd.id) from devolucion_detalle dd WHERE dd.iddevolucion = d.id) as cantidad,\n"
                + "cs.direccion || ', ' || ci.descripcion || ' (' ||de.descripcion ||'), ' || cs.nombre as sede, d.empresa_envia as idempresa,\n"
                + "cc.nombres as contacto, c.nombrecompleto as cliente, d.fotos, d.guia, ev.empresa as empresa_envia, fotoentrega,\n"
                + "to_char(d.fechaanulacion, 'dd/MM/yyyy HH24:MI') as fechaanulacion, observacionanula, ua.nombrecompleto as usuarioanula, d.sitio \n"
                + "from devolucion d inner join seguridad.rbac_usuario u on d.idusuario = u.idusu\n"
                + "inner join clientes c on c.id = d.idcliente\n"
                + "inner join clientes_contac cc on cc.id = d.idcontacto\n"
                + "inner join clientes_sede cs on cs.id = d.idsede\n"
                + "inner join ciudad ci on ci.id = cs.idciudad\n"
                + "inner join departamento de on de.id = ci.iddepartamento \n"
                + "inner join empresa_envio ev on ev.id = d.empresa_envia \n"
                + "inner join seguridad.rbac_usuario ua on d.idusuarioanula = ua.idusu\n"
                + "where devolucion = " + devolucion;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarDevolucion");
    }

    public String SubirFotoDevolucion(HttpServletRequest request) {
        try {
            String devolucion = getValorJSON(request, "devolucion");
            String dataB64 = "", dataExt = "", Ext = "", dataIMG = getValorJSON(request, "fotos");

            int ingresoFoto = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT fotos+1 FROM public.devolucion WHERE devolucion = " + devolucion));
            int entregado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT entregado FROM public.devolucion WHERE devolucion = " + devolucion));
            if (entregado > 0) {
                return "[{\"error\":\"1\",\"mensaje\":\"La devolución ya fue entregada\"}]";
            }

            if (dataIMG.length() > 0 && dataIMG.startsWith("data:")) {
                dataExt = (dataIMG.split(",")[0]);
                dataB64 = (dataIMG.split(",")[1]).replaceAll(" ", "+");
            } else {
                dataExt = "data:image/jpeg;base64";
                dataB64 = (dataIMG).replaceAll(" ", "+");
            }

            boolean crearArchivoB64 = crearArchivoB64(dataB64, dataExt, "5", (ingresoFoto) + "", devolucion);
            if (crearArchivoB64 == false) {
                return "[{\"error\":\"1\",\"mensaje\":\"Error al guardar la foto\"}]";
            }
            sql = "UPDATE public.devolucion set fotos = " + ingresoFoto + " where  devolucion = " + devolucion;
            Globales.DatosAuditoria(sql, "DEVOLUCIONES", "SUBIR FOTO", "0", "APP-MOVIL", this.getClass() + "-->SubirFotoDevolucion");
            return "[{\"error\":\"0\",\"mensaje\":\"La foto ha sido guardada\"}]";

        } catch (Exception ex) {
            return "[{\"error\":\"1\",\"mensaje\":\"" + ex + "\"}]";
        }
    }

    private String CargarEmpresas(HttpServletRequest request) {
        sql = "SELECT id, empresa from empresa_envio WHERE estado = 1  ORDER BY 2";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->CargarEmpresas");
    }

    public String BuscarVueltasPendientes(HttpServletRequest request) {
        String usuario = getValorJSON(request, "usuario");
        String cliente = getValorJSON(request, "cliente");
        String tipo = getValorJSON(request, "tipo");
        String pendiente = getValorJSON(request, "pendiente");
        String entregar = getValorJSON(request, "entregar");

        String busqueda = " where idusuarioasi = (SELECT idusu FROM seguridad.rbac_usuario WHERE nomusu = '" + usuario + "') and " + (tipo.equals("1") ? "idcliente = " + cliente : "idproveedor = " + cliente) + (entregar.equals("0") ? " and tipovuelta<> 'ENTREGAR EQUIPO'" : "");
        if (pendiente.equals("1")) {
            busqueda += " and v.estado = 'ASIGNADA'";
        } else {
            busqueda += " and v.estado = 'EJECUTADA'";
        }

        sql = "SELECT v.id, v.actividad || ' ' || v.contacto || ' ' || v.direccion || ' ' ||"
                + " v.observacion || ' <b> (' || nomusu || ')</b>' AS actividad, "
                + "to_char(fechaapro,'yyyy/MM/dd HH24:MI') as fechaapro, fotos, v.estado "
                + "FROM registrovueltas v inner join seguridad.rbac_usuario u on u.idusu = v.idusuario " + busqueda + " order by fechaapro, v.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarVueltasPendientes");
    }

    private String SubirFotoEntrega(HttpServletRequest request) {
        String dataB64 = "",
                dataExt = "",
                Ext = "",
                dataIMG = getValorJSON(request, "fotos"),
                devolucion = getValorJSON(request, "devolucion");
        sql = "SELECT fotoentrega FROM public.devolucion WHERE devolucion = " + devolucion;
        int nuFoto = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        sql = "SELECT entregado FROM public.devolucion WHERE devolucion = " + devolucion;
        int nuEntregado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (nuEntregado > 0) {
            return "[{\"error\":\"1\",\"mensaje\":\"El equipo ya fue entregado\"}]";
        }

        if (dataIMG.length() > 0 && dataIMG.startsWith("data:")) {
            dataExt = (dataIMG.split(",")[0]);
            dataB64 = (dataIMG.split(",")[1]).replaceAll(" ", "+");
        } else {
            dataExt = "data:image/jpeg;base64";
            dataB64 = (dataIMG).replaceAll(" ", "+");
        }

        boolean crearArchivoB64 = crearArchivoB64(dataB64, dataExt, "2", (nuFoto + 1) + "", devolucion);
        if (crearArchivoB64 == false) {
            return "[{\"error\":\"1\",\"mensaje\":\"Error al guardar la foto\"}]";
        }

        sql = "UPDATE public.devolucion set fotoentrega = " + (nuFoto + 1) + " where  devolucion = " + devolucion;
        Globales.DatosAuditoria(sql, "Subir Foto de entrega", "ACTUALIZAR", idusuario, iplocal, this.getClass() + "-->GuardarPosicionAPP");
        return "[{\"error\":\"0\",\"mensaje\":\"La foto ha sido guardada\"}]";
    }

    private String SubirFirma(HttpServletRequest request) {
        String codigo = getValorJSON(request, "codigo"),
                vueltas = getValorJSON(request, "vueltas"),
                empr_env = getValorJSON(request, "empr_env"),
                nro_guia = getValorJSON(request, "nro_guia"),
                observacioncli = getValorJSON(request, "observacioncli"),
                recibio = getValorJSON(request, "recibio"),
                identificacion = getValorJSON(request, "identificacion"),
                telefono = getValorJSON(request, "telefono"),
                cargo = getValorJSON(request, "cargo"),
                dataIMG = getValorJSON(request, "firma"),
                dataExt = "",
                dataB64 = "";
        usuario = getValorJSON(request, "usuario");

        if (dataIMG.length() > 0 && dataIMG.startsWith("data:")) {
            dataExt = (dataIMG.split(",")[0]);
            dataB64 = (dataIMG.split(",")[1]).replaceAll(" ", "+");
        } else {
            dataExt = "data:image/jpeg;base64";
            dataB64 = (dataIMG).replaceAll(" ", "+");
        }

        sql = "SELECT entregado FROM devolucion where devolucion = " + codigo;
        int entregado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (entregado > 0) {
            return "[{\"error\":\"1\",\"mensaje\":\"Esta devolución ya fue entregada\"}]";
        }

        sql = "SELECT idusuaeje FROM registrovueltas where id = " + vueltas;
        int ejecutada = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (ejecutada > 0) {
            return "[{\"error\":\"1\",\"mensaje\":\"Esta vuelta ya fue ejecutada\"}]";
        }

        sql = "UPDATE devolucion SET fechaentrega = now(), fotodev = 2, entregado = 1, nombreentrega ='MOVIL-CLIENTE',"
                + "observacionentrega = 'ENTREGA DESDE LA MOVIL', empresa_envia = " + empr_env + " , guia ='" + nro_guia + "',"
                + "observacioncliente = '" + observacioncli + "'  , idusuarioentrega =(select idusu from seguridad.rbac_usuario "
                + "WHERE nomusu = '" + usuario + "'  ), recibio='" + recibio + "', identificacionr='" + identificacion + "', "
                + "telefonor='" + telefono + "',cargor='" + cargo + "' WHERE devolucion = " + codigo + ";";

        sql += "UPDATE remision_detalle rd set entregado = 1 FROM devolucion_detalle dd INNER JOIN devolucion d on d.id = dd.iddevolucion "
                + "WHERE dd.ingreso = rd.ingreso and d.devolucion = " + codigo + ";";

        if (Integer.parseInt(vueltas) > 0) {
            sql = "UPDATE registrovueltas SET estado ='EJECUTADA',  fechaeje = now(), idusuaeje=(select idusu from seguridad.rbac_usuario WHERE nomusu = '" + usuario + "'), ejecutado='SI', observacion='Ejecutada por devolución de equipo' WHERE id = " + vueltas;
        }
        Globales.DatosAuditoria(sql, "DEVOLUCION", "ENTREGAR", "(select idusu from seguridad.rbac_usuario WHERE nomusu = '" + usuario + "')", iplocal, this.getClass().toString());

        crearArchivoB64(dataB64, dataExt, "1", codigo, "");
        return "";
    }

    private String BuscarClieProv(HttpServletRequest request) {
        try {
            int cliente = Globales.Validarintnonull(getValorJSON(request, "cliente"));
            int proveedor = Globales.Validarintnonull(getValorJSON(request, "proveedor"));

            sql = "";
            if (cliente > 0) {
                sql = "SELECT id, nombrecompleto, estado FROM public.clientes WHERE estado = 1 AND id > 0 ORDER BY 2 ASC";
            }

            if (proveedor > 0) {
                sql = "SELECT id, nombrecompleto, estado FROM public.proveedores WHERE estado = 1 AND id > 0 ORDER BY 2 ASC";
            }
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarClieProv");
        } catch (Exception ex) {
            return "[{\"error\":\"1\",\"mensaje\":\"" + ex + "\"}]";
        }
    }

    private String BuscarVueltas(HttpServletRequest request) {
        try {
            int cliente = Globales.Validarintnonull(getValorJSON(request, "cliente"));
            int proveedor = Globales.Validarintnonull(getValorJSON(request, "proveedor"));
            int ejecutado = Globales.Validarintnonull(getValorJSON(request, "ejecutado"));
            int cli_pro = Globales.Validarintnonull(getValorJSON(request, "cli_pro"));
            String usuario = getValorJSON(request, "usuario");

            String where = " WHERE idusuarioasi = (SELECT idusu FROM seguridad.rbac_usuario WHERE nomusu = '" + usuario + "')";
            if (cliente > 0) {
                where += " AND v.idcliente = " + cli_pro + " AND v.tipo = 'CLIENTE'";
            }

            if (proveedor > 0) {
                where += " AND v.idcliente = " + cli_pro + " AND v.tipo = 'PROVEEDOR'";
            }

            if (ejecutado > 0) {
                where += " AND v.estado = 'ASIGNADA'";
            }

            sql = "SELECT DISTINCT v.id, v.actividad || ' ' || v.contacto || ' ' || v.direccion || ' ' || "
                    + " v.observacion || ' (' || nomusu || ')' AS actividad, to_char(fechaapro,'yyyy/MM/dd HH24:MI') as fechaapro, fotos, "
                    + "v.estado FROM registrovueltas v inner join seguridad.rbac_usuario u on u.idusu = v.idusuario"
                    + (cliente > 0 ? " INNER JOIN public.clientes cli ON cli.id = v.idcliente " : " INNER JOIN public.proveedores cli ON cli.id = v.idcliente ") + where + " order by fechaapro";
            System.out.print("QUERY: " + sql);
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarVueltas");
        } catch (Exception ex) {
            return "[{\"error\":\"1\",\"mensaje\":\"" + ex + "\"}]";
        }
    }

    private String SubirFotoVuelta(HttpServletRequest request) {
        try {
            //string fotos, int tipo, int vuelta, string usuario
            int tipo = Globales.Validarintnonull(getValorJSON(request, "tipo"));
            int vuelta = Globales.Validarintnonull(getValorJSON(request, "vuelta"));
            String dataIMG = getValorJSON(request, "fotos");
            String dataExt, dataB64;
            usuario = getValorJSON(request, "usuario");

            String miSQL = "SELECT COUNT(*) FROM public.fotos_recogida WHERE fecha::date = NOW()::date AND idusuario = (SELECT idusu FROM seguridad.rbac_usuario WHERE nomusu = '" + usuario + "')";
            if (vuelta > 0) {
                miSQL += " AND idvuelta = " + vuelta;
            }
            int nuFoto = Globales.Validarintnonull(Globales.ObtenerUnValor(miSQL));

            if (nuFoto > 0) {
                miSQL = "SELECT (foto+1) FROM public.fotos_recogida WHERE fecha::date = NOW()::date AND idusuario = (SELECT idusu FROM seguridad.rbac_usuario WHERE nomusu = '" + usuario + "')" + (vuelta > 0 ? " AND idvuelta = " + vuelta : "");
                nuFoto = Globales.Validarintnonull(Globales.ObtenerUnValor(miSQL));
            }

            if (dataIMG.length() > 0 && dataIMG.startsWith("data:")) {
                dataExt = (dataIMG.split(",")[0]);
                dataB64 = (dataIMG.split(",")[1]).replaceAll(" ", "+");
            } else {
                dataExt = "data:image/jpeg;base64";
                dataB64 = (dataIMG).replaceAll(" ", "+");
            }

            boolean crearArchivoB64 = crearArchivoB64(dataB64, dataExt, "3", (nuFoto + 1) + "", tipo + "" + File.separatorChar + "" + vuelta);

            miSQL = "SELECT COUNT(*) FROM public.fotos_recogida WHERE fecha::date = NOW()::date AND idusuario = (SELECT idusu FROM seguridad.rbac_usuario WHERE nomusu = '" + usuario + "')";
            if (vuelta > 0) {
                miSQL += " AND idvuelta = " + vuelta;
            }
            nuFoto = Globales.Validarintnonull(Globales.ObtenerUnValor(miSQL));

            if (nuFoto > 0) {
                miSQL = "UPDATE fotos_recogida SET foto = (foto+1) WHERE  fecha::date = NOW()::date AND idusuario = (SELECT idusu FROM seguridad.rbac_usuario WHERE nomusu = '" + usuario + "')" + (vuelta > 0 ? " AND idvuelta = " + vuelta + ";" : ";");
                miSQL += "UPDATE registrovueltas set fotos = (fotos+1) where id = " + vuelta;
            } else {
                miSQL = "INSERT INTO fotos_recogida(idvuelta, fecha, idusuario, foto) VALUES(" + vuelta + ",now(),(SELECT idusu FROM seguridad.rbac_usuario WHERE nomusu = '" + usuario + "')," + (nuFoto) + ");";
            }
            Globales.DatosAuditoria(miSQL, "VUELTA", "SUBIR FOTO", "0", "APP-MOVIL", this.getClass() + "-->SubirFotoVuelta");
            return "[{\"error\":\"0\",\"mensaje\":\"La foto se ha guardado correctamente\"}]";
        } catch (Exception ex) {
            return "[{\"error\":\"1\",\"mensaje\":\"" + ex + "\"}]";
        }
    }

    /*private String EnvioDevolucion(HttpServletRequest request)
        {//string principal, int devolucion, string e_email, string archivo, string usuario
            int devolucion = Globales.Validarintnonull(getValorJSON(request, "devolucion"));
            String principal = getValorJSON(request, "principal");
            String archivo = getValorJSON(request, "archivo");
            String e_email = getValorJSON(request, "e_email");
            usuario = getValorJSON(request, "usuario");
            
            int error = 0;           

            String ClaveAcceso = Globales.ObtenerUnValor("SELECT clave from clientes c inner join devolucion de on de.idcliente = c.id where devolucion = " + devolucion);
            String cliente = Globales.ObtenerUnValor("SELECT nombrecompleto from clientes c inner join devolucion de on de.idcliente = c.id where devolucion = " + devolucion);
            String CorreoCliente = Globales.ObtenerUnValor("SELECT email from clientes c inner join devolucion de on de.idcliente = c.id where devolucion = " + devolucion);
           
            String correoemp = Globales.ObtenerUnValor("SELECT correoenvio from seguridad.rbac_usuario where nomusu = '" + usuario + "'");
            String clavecorreo = Globales.ObtenerUnValor("SELECT clavecorreo from seguridad.rbac_usuario where nomusu = '" + usuario + "'");
            
            e_email += ";" + correoemp;
            String[] email = e_email.split(";");            
            String nombrearchivo = "DID-106-" + devolucion + ".pdf";

            if ((correoemp.trim().equals("")) || (clavecorreo.trim().equals("")))
                return "[{\"error\":\"1\",\"mensaje\":\"Debe configurar los parámetros de correo envio y clave envio\"}]";

            String saludo = "";
            int hora = Integer.parseInt(new SimpleDateFormat("HH")+"");
            if (hora <= 11)
                saludo = "Buenos días";
            else
            {
                if (hora <= 18)
                    saludo = "Buenas tardes";
                else
                    saludo = "Buenas noches";
            }

            string mensaje = saludo + "<br><br><b>SEÑORES:</b><br>" + cliente + "<br><br>"
                + " Reciba un cordial saludo.<br><br>";

            mensaje += "Envio de <b>Devolución Número</b> DID-106-" + devolucion + ".";


            

            mensaje += "<br><br>Debe de ingresar a su cuenta de <b>CALIBRATION SERVICE SAS</b> para llenar una pequeña encuesta de satisfacción y descargar los certificados a través del link <br>" +
                       "<a href='http://sof.calibrationservicesas.com:8080/usuarios/login.php?usuario=" + CorreoCliente + "&clave=" + "" + "'>http://sof.calibrationservicesas.com:8080/usuarios/login.php?usuario=" + CorreoCliente + "&clave=" + "" + "</a><br><br>" +
                       "Sus datos para ingresar son los siguientes: <br><b>usuario</b> " + CorreoCliente +
                       "<br><b>Clave de acceso</b> " + "";

            String directorio = "~/DocumPDF/";

            String adjunto = String.Format("{0}{1}",
                                    this.HttpContext.Server.MapPath(directorio),
                                    archivo);

           String correo = Globales.EnviarEmail(principal, empresa, email, "Envío de la Devolución Número " + devolucion, mensaje, correoemp, adjunto, correoemp, clavecorreo, nombrearchivo);

            if (correo.trim() != "")
            {
                return JsonConvert.SerializeObject("{ \"respuesta\": \"1|" + correo + "\"}");
            }

            sql = " UPDATE devolucion set fechaenvio = now(),  obserenvio='', correoenvio = '" + principal + " " + e_email + "' WHERE devolucion = " + devolucion;

            Globales.Obtenerdatos(sql, conexion);
            return JsonConvert.SerializeObject("{ \"respuesta\": \"" + error + " | Correo enviado a " + principal + "; " + e_email + "\"}");

        }*/
    private boolean crearArchivoB64(String base64, String formato, String tipoImg, String nombreArchivo, String carpeta) {
        System.out.print("-Guardando archivo-");
        String inconformidades = "";
        String tipo = formato;
        String valorB64 = base64.trim().replaceAll(" ", "+");
        String extencion = extencionesArc(formato);
        if (extencion.equals("-")) {
            return false;
        }
        if (rutasArc(tipoImg, nombreArchivo).equals("-")) {
            return false;
        }
        try {
            Path destinationFile = Paths.get(rutasArc(tipoImg, carpeta), nombreArchivo + "_copia." + extencion);
            Path destinationFileJPEG = Paths.get(rutasArc(tipoImg, carpeta), nombreArchivo + "." + extencion);
            System.out.print("-Ruta de guardado- " + destinationFile);
            byte[] decodedImg = Base64.getMimeDecoder().decode(valorB64.trim().getBytes(StandardCharsets.UTF_8));
            Files.write(destinationFile, decodedImg);
            Globales.ComprimirIamgen(destinationFile.toString(), destinationFileJPEG.toString());
            return true;
        } catch (IOException ex) {
            Logger.getLogger(OpcioneAPP.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private String extencionesArc(String tipo) {
        switch (tipo) {
            case "data:image/jpeg;base64":
                return "jpg";
            case "data:image/png;base64":
                return "png";
            case "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64":
                return "xlsx";
            case "data:application/msword;base64":
                return "doc";
            case "data:application/pdf;base64":
                return "pdf";
            default:
                return "-";
        }
    }

    private String rutasArc(String tipo, String capeta) {
        String rutaPrinc = Globales.ruta_archivo + "Adjunto" + "/imagenes";
        Path destinFile;
        switch (tipo) {
            case "1":
                destinFile = Paths.get(rutaPrinc, "DevoFirma/");
                if (new File(destinFile.toString()).exists() == true) {
                    return destinFile.toString();
                } else {
                    new File(destinFile.toString()).mkdirs();
                    return destinFile.toString();
                }
            case "2":
                destinFile = Paths.get(rutaPrinc, "FotoEntrega/" + capeta);
                if (new File(destinFile.toString()).exists() == true) {
                    return destinFile.toString();
                } else {
                    new File(destinFile.toString()).mkdirs();
                    return destinFile.toString();
                }
            case "3":
                destinFile = Paths.get(rutaPrinc, "FotoRecogida/" + capeta);
                if (new File(destinFile.toString()).exists() == true) {
                    return destinFile.toString();
                } else {
                    new File(destinFile.toString()).mkdirs();
                    return destinFile.toString();
                }
            case "4":
                destinFile = Paths.get(rutaPrinc, "ingresos/" + capeta);
                if (new File(destinFile.toString()).exists() == true) {
                    return destinFile.toString();
                } else {
                    new File(destinFile.toString()).mkdirs();
                    return destinFile.toString();
                }
            case "5":
                destinFile = Paths.get(rutaPrinc, "Devoluciones/" + capeta);
                if (new File(destinFile.toString()).exists() == true) {
                    return destinFile.toString();
                } else {
                    new File(destinFile.toString()).mkdirs();
                    return destinFile.toString();
                }
            default:
                return "-";
        }
    }

    private String getValorJSON(HttpServletRequest request, String campo) {
        String JSON;
        String valueJSON = "-";
        try {
            if (elemJSON == null) {
                JSON = new BufferedReader(new InputStreamReader(request.getInputStream())).lines().collect(Collectors.joining("\n"));
                JsonParser parseJSON = new JsonParser();
                elemJSON = parseJSON.parse(JSON);
                System.out.print("dataJSON: " + elemJSON);
            }
            JsonArray arrJSON = new JsonArray();
            JsonObject objJSON = new JsonObject();

            if (elemJSON instanceof JsonObject) {
                objJSON = elemJSON.getAsJsonObject();
            } else if (elemJSON instanceof JsonArray) {
                arrJSON = elemJSON.getAsJsonArray();
            }

            if (arrJSON.size() > 0) {
                for (JsonElement obj : arrJSON) {
                    JsonObject gsonObj = obj.getAsJsonObject();
                    valueJSON = gsonObj.get(campo).getAsString();
                    return valueJSON;
                }
            }

            if (objJSON.size() > 0) {
                valueJSON = objJSON.get(campo).getAsString();
                return valueJSON;
            }
        } catch (IOException ex) {
            Logger.getLogger(OpcioneAPP.class.getName()).log(Level.SEVERE, null, ex);
            return valueJSON;
        }
        return valueJSON;
    }
}
