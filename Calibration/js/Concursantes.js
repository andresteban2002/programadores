﻿var DocumentoPer = "";
var AutoEmpresa = [];
var AutoCargo = [];
var FotoDocumento = "";
var IdParticipante = 0;
var ErrorTecla = 0;

$("#Curso, #ACurso").html(CargarCombo(60, 1));
$("#Pais").html(CargarCombo(18, 1));
$("#Pais").val(1).trigger("change");

$("#formParticipante").submit(function (e) {
    e.preventDefault();
    var curso = $("#Curso").val() * 1;
    var parametros = $("#formParticipante").serialize() + "&Foto=" + FotoDocumento;
    var datos = LlamarAjax("RegistroEntrada/GuardarParticipante", parametros).split("|");
    if (datos[0] == "0") {
        IdParticipante = datos[2] * 1;
        ImprimirEscarapela();
        swal("", datos[1], "success");
        $("#IdParticipante").val(datos[2]);
        CargarTabla(curso);
        AutoCompletar();

    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
    return false;

});

d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;





function CambioDepartamento(pais) {
    $("#Departamento").html(CargarCombo(11, 1, "", pais));
}

function CambioCiudad(departamento) {
    $("#Ciudad").html(CargarCombo(12, 1, "", departamento));
}

function LimpiarTodo() {
    $("#Documento").val("");
    DocumentoPer  = "";
    LimpiarDatos();
    $("#Documento").focus();
}

function LimpiarDatos() {
    IdParticipante = 0;
    $("#IdParticipante").val("0");
    FotoDocumento = "";
    $("#Nombres").val("");
    $("#Empresa").val("");
    $("#Cargo").val("");
    $("#Telefono").val("");
    $("#Email").val("");
    $("#Pais").val("1").trigger("change");
    $("#Departamento").val("11").trigger("change");
    $("#Ciudad").val("150").trigger("change");
    
    $("#ImagenPersona").attr("src", url + "imagenes/img_avatar.png");
}

$('#ImagenPersona').load(function (e) {

}).error(function (e) {
    $("#" + this.id).attr("src", url + "imagenes/img_avatar.png")
});

function LlamarFoto(documento) {
    $("#FotoVisita").attr("src", url + "imagenes/FotoParticipante/" + documento + ".jpg");
    $("#modalFotoPersona").modal("show");
}

function VerDocumento() {
    var visita = $("#IdVisita").val()*1;
    $("#FotoDocumento").attr("src", url + "imagenes/FotoDocumento/" + visita + ".jpg");
    $("#modalFotoDocumento").modal("show");
}

$('#FotoDocumento').load(function (e) {

}).error(function (e) {
    swal("Acción Cancelada", "Esta visita no posee documento registrado con foto", "warning");
    $("#modalFotoDocumento").modal("hide");
});

function BuscarPersonal(documento,tipo) {
    var curso = $("#Curso").val() * 1;

    if ($.trim(documento) == "" || curso == 0)
        return false;
    if (DocumentoPer == documento)
        return false;
    LimpiarDatos();
    
    DocumentoPer = documento;
    var parametros = "documento=" + $.trim(documento) + "&curso=" + curso;
    var datos = LlamarAjax('RegistroEntrada/BuscarParticipante', parametros);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        IdParticipante = data[0].id;
        $("#IdParticipante").val(data[0].id);
        $("#Nombres").val(data[0].nombrecompleto);
        $("#Cargo").val(data[0].cargo);
        $("#Telefono").val(data[0].telefono);
        $("#Email").val(data[0].email);
        $("#Empresa").val(data[0].empresa);
        FotoDocumento = data[0].foto;
        $("#ImagenPersona").attr("src", url + "imagenes/FotoParticipante/" + data[0].foto);
        
        if (data[0].idciudad * 1 > 0) {
            $("#Pais").val(data[0].pais).trigger("change");
            $("#Departamento").val(data[0].departamento).trigger("change");
            $("#Ciudad").val(data[0].ciudad).trigger("change");
        }

        if (curso == data[0].curso * 1 && tipo == 1) {
            swal("Accion Cancelada", "El participante " + data[0].nombrecompleto  + " ya se encuentra registrado en este curso", "warning");
        }

    } 

}

function ValidarPersonal(documento) {
    if ($.trim(DocumentoPer) != "") {
        if (DocumentoPer != documento) {
            DocumentoPer = "";
            LimpiarDatos();
        }
    }
}

$("#TablaParticipantes > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var documento = row[4].innerText;
    $("#Documento").val(documento);
    BuscarPersonal(documento,0);
});


function CargarTabla(curso) {
    curso = curso * 1;

    LimpiarTodo();

    if (curso > 0) {

        $("#Documento").prop("disabled", false);
        $("#Nombres").prop("disabled", false);
        $("#Empresa").prop("disabled", false);
        $("#Cargo").prop("disabled", false);
        $("#Telefono").prop("disabled", false);
        $("#Email").prop("disabled", false);
        $("#Pais").prop("disabled", false);
        $("#Departamento").prop("disabled", false);
        $("#Ciudad").prop("disabled", false);

    } else {
        $("#Documento").prop("disabled", true);
        $("#Nombres").prop("disabled", true);
        $("#Empresa").prop("disabled", true);
        $("#Cargo").prop("disabled", true);
        $("#Telefono").prop("disabled", true);
        $("#Email").prop("disabled", true);
        $("#Pais").prop("disabled", true);
        $("#Departamento").prop("disabled", true);
        $("#Ciudad").prop("disabled", true);
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "curso=" + curso;
        var datos = LlamarAjax("RegistroEntrada/ConsultaParticipante", parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaParticipantes').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "foto" },
                { "data": "curso" },
                { "data": "empresa" },
                { "data": "participante" },
                { "data": "documento" },
                { "data": "cargo" },
                { "data": "email" },
                { "data": "telefono" },
                { "data": "pais" },
                { "data": "departamento" },
                { "data": "ciudad" },
                { "data": "registro" }
            ],
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy'
            ],
            "language": {
                "url": LenguajeDataTable
            }
        });
    }, 15);
}

function CargarParticipantes(curso) {
    curso = curso * 1;
    var datos = LlamarAjax("RegistroEntrada/Participante_Curso", "curso=" + curso);
    $("#AParticipante").html(datos);
    CargarTablaAsistencia();

}

function CargarTablaAsistencia() {
    var participante = $("#AParticipante").val()*1;
    var curso = $("#ACurso").val() * 1;
    var fechad = $("#AFechaDesde").val();
    var fechah = $("#AFechaHasta").val();
    
    ActivarLoad();
    setTimeout(function () {
        var parametros = "curso=" + curso + "&participante=" + participante + "&fechad=" + fechad + "&fechah=" + fechah;
        var datos = LlamarAjax("RegistroEntrada/ConsultaAsistencia", parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaAsistencia').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "foto" },
                { "data": "curso" },
                { "data": "empresa" },
                { "data": "participante" },
                { "data": "cargo" },
                { "data": "email" },
                { "data": "telefono" },
                { "data": "fecha" },
                { "data": "entrada" },
                { "data": "salida" }
            ],
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy'
            ],
            "language": {
                "url": LenguajeDataTable
            }
        });
    }, 15);
}

function TomarFoto() {
    Webcam.snap(function (data_uri) {
        GuardarFoto(DocumentoPer, data_uri);
        $("#modalFotoPer").modal("hide");
    });
}

function GuardarFoto(documento, archivo) {
    $.ajax({
        type: 'POST',
        url: url + "RegistroEntrada/GuardarFotoPar",
        data: "archivo=" + archivo + "&documento=" + documento,
        success: function (data) {
            data = data.split("|");
            if (data[0] == "0") {
                FotoDocumento = data[1];
                $("#ImagenPersona").attr("src", url + "imagenes/FotoParticipante/" + FotoDocumento);
                $("#modalFotoPer").modal("hide");
            } else
                swal("Acción Cancelada", data[1], "warning");
        }
    });
}

function CambiarDocumento() {
    var curso = $("#Curso").val() * 1;
    var documento = $.trim($("#DDocumento").val());
    if (documento == "") {
        $("#DDocumento").focus();
        swal("Acción Cancelada", "Debe de ingresar el nuevo NIT/Cédula", "warning");
        return false;
    }

    var datos = LlamarAjax("RegistroEntrada/CambioDocumento", "documento=" + documento + "&id=" + IdParticipante).split("|");
    if (datos[0] == "0") {
        $("#modalCambioDocumento").modal("hide");
        $("#Documento").val(documento);
        DocumentoPer = documento;
        CargarTabla(curso)
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function CambioDocumento() {
    if (IdParticipante == 0) {
        swal("Acción Cancelada", "Debe seleccionar un participante", "warning");
        return false;
    }
    $("#DDocumento").val(DocumentoPer);
    $("#SCambioNit").html(DocumentoPer);
    $("#modalCambioDocumento").modal("show");
}

function SubirFoto(tipo) {
    if (DocumentoPer == "")
        return false;
    $("#titulotipofoto").html("Foto del participante con el documento número " + DocumentoPer);
    $("#modalFotoPer").modal("show");
}

Webcam.set({
    width: 650,
    height: 510,
    dest_width: 650,
    dest_height: 510,
    image_format: 'png',
    jpeg_quality: 100
});
Webcam.attach('#my_cameraperso');

function AutoCompletar() {
    var datos = LlamarAjax("RegistroEntrada/AutocompletarCurso", "").split("|");
    
    AutoEmpresa.splice(0, AutoEmpresa.length);
    AutoCargo.splice(0, AutoCargo.length);

    
    if (datos[0] != "[]") {
        var data = JSON.parse(datos[0]);
        for (var x = 0; x < data.length; x++) {
            AutoEmpresa.push(data[x].empresa);
        }
    }
    if (datos[1] != "[]") {
        var data1 = JSON.parse(datos[1]);
        for (var x = 0; x < data1.length; x++) {
            AutoCargo.push(data1[x].cargo);
        }
    }
}

AutoCompletar();

$("#Cargo").autocomplete({ source: AutoCargo, minLength: 0 }).focus(function () {
    $(this).data("uiAutocomplete").search($(this).val());
});

$("#Empresa").autocomplete({ source: AutoEmpresa, minLength: 0 }).focus(function () {
    $(this).data("uiAutocomplete").search($(this).val());
});

function ImprimirEscarapela() {

    if (IdParticipante == 0)
        return false;

    var curso = $("#Curso").val() * 1;
    var parametros = "id=" + IdParticipante + "&curso=" + curso;
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("RegistroEntrada/RpEscarapela", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function ExaminarFoto() {
    if (DocumentoPer == "")
        return false;
    swal("Subir Foto", '<h3>' + ValidarTraduccion("Seleccione la foto que desea subir del documento número " + DocumentoPer) + '</h3><input type="file" class="form-control" id="documento" onchange="GuardarDocumento()" name="documento" accept="image/*" />');
}

function GuardarDocumento() {
    var documento = document.getElementById("documento");
    if ($.trim(documento.value) != "") {
        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var direccion = url + "Cotizacion/AdjuntarFoto";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] == "0") {
                    var datos = LlamarAjax("RegistroEntrada/SubirFoto?documento=" + DocumentoPer).split("|");
                    if (datos[0] == "0") {
                        FotoDocumento = datos[1];
                        $("#ImagenPersona").attr("src", url + "imagenes/FotoParticipante/" + FotoDocumento);
                    } else {
                        swal(data);
                    }
                } else
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
            }
        });
    }
}

function RegistrarAsistencia(documento, code) {
    documento = $.trim(documento);
    var curso = $("#ACurso").val() * 1;

    
    if (documento == "" ) {
        return false
    }



    if (code.keyCode == 13 && ErrorTecla == 0) {

        if (curso == 0) {
            ErrorTecla = 1;
            swal("Acción Cancelada", "Debe de seleccionar un curso", "warning");
            return false;
        }

        var parametros = "documento=" + documento + "&curso=" + curso;
        var datos = LlamarAjax("RegistroEntrada/GuardarAsistencia", parametros).split("|");
        if (datos[0] == "0") {
            $("#ADocumento").val("");
            $.jGrowl(datos[1], { life: 2500, theme: 'growl-success', header: '' });
            CargarTablaAsistencia();
        } else {
            ErrorTecla = 1;
            swal("Acción Cancelada", datos[1], "warning");
        }
    } else
        ErrorTecla = 0;

}


$('select').select2();
DesactivarLoad();
