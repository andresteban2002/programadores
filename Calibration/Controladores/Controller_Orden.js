angular.module('myApp', ['ui', 'ngSanitize', 'ngTable']).controller('ControlOrden', function ($scope, $ocLazyLoad, $rootScope, General, NgTableParams) {

    function initTabs() {
        tabClasses = ["", "", "", "", "", "", ""];
    }

    $scope.getTabClass = function (tabNum) {
        return tabClasses[tabNum];
    };

    $scope.getTabPaneClass = function (tabNum) {
        return "tab-pane " + tabClasses[tabNum];
    }

    $scope.setActiveTab = function (tabNum) {
        initTabs();
        tabClasses[tabNum] = "active";
    };
    
	
    
    $rootScope.InicializarFormulario(2);
    $scope.setActiveTab(1);
    
});

