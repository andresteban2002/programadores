
angular.module('myApp').controller('ControlPedido', function ($scope, $ocLazyLoad, $rootScope, General) {

    $scope.Pedido = "";

    $scope.ActualizarCombos = function (tipo) {
        setTimeout(function () {
            var clase = "";
            switch (tipo) {
                case 1:
                    clase = "ComboRegistro";
                    break;
                case 2:
                    clase = "ComboInventario";
                    break;
            }
            $("." + clase).trigger("change");
        }, 200);
    }

    $scope.CambioFecha = function (fecha, tipo) {
        switch (tipo) {
            case 1:
                $scope.Registro.fechamax.id = General.Obtener_Fecha(fecha);
                break;
        }
    }

    $scope.InicializarRegistro = function () {
        $scope.Registro = {
            id: 0,
            tipo: "",
            desposito: "",
            dis_deposito: false,
            fecha: "",
            usuario: "",
            pedido: "",
            fechamax: {
                value: null,
                id: ""
            },
            descripcion: "",
            estado: "Temporal",
            tabla: {},
            tcantidad: 0,
            allselect: false,
            disabled: false,
            temporal: 0,
            tseleccionado: 0
        }
    }

    $scope.LimpiarTodo = function () {
        $scope.InicializarRegistro();
        $rootScope.TablaPedido();
        $scope.ActualizarCombos(1);
    }

    $scope.Combos = {
        deposito: ""
    }

    $scope.setCambioSelect = function (index) {
        if ($scope.Registro.tabla[index].seleccionado) {
            $scope.Registro.tabla[index].class_select = "bg-gris";
        } else {
            $scope.Registro.tabla[index].class_select = "";
        }
        $scope.Totales();
    }

    $scope.SeleccionarTodo = function () {
        for (var i = 0; i < $scope.Registro.tabla.length; i++) {
            if ($scope.Registro.allselect) {
                $scope.Registro.tabla[i].seleccionado = true;
                $scope.Registro.tabla[i].class_select = "bg-gris";
            } else {
                $scope.Registro.tabla[i].seleccionado = false;
                $scope.Registro.tabla[i].class_select = "";
            }
        }
        $scope.Totales();
    }

    General.CargarCombo(68, $rootScope.InicioSesion.idusuario).then(function (response) {
        $scope.Combos.deposito = response.data;
    });

    $scope.ActivarDeposito = function (tipo) {
        if (tipo == "INTERNO") {
            $scope.Registro.dis_deposito = false;
        } else {
            $scope.Registro.dis_deposito = true;
            $scope.Registro.deposito = "";
            $scope.ActualizarCombos(1);
        }
    }

    $scope.ModalArticulos = function () {
        $rootScope.Modal.grupo = "";
        $rootScope.Modal.codigobarras = "";
        $rootScope.Modal.descripcion = "";
        $rootScope.Modal.tipo = 1;
        $rootScope.Modal.dis_moneda = false;
        $rootScope.Modal.dis_precio = false;
        $rootScope.Modal.dis_ubicacion = false;
        $rootScope.Modal.Open("Inventarios");
    }

    $rootScope.ObtenerDatatable = function (data) {

        var descripcion = "CODIGO: " + data.codigointerno + ", TIPO: " + data.tipo + ", GRUPO: " + data.grupo +
                ", ARTICULO: " + data.equipo + ", MARCA: " + data.marca + ", MODELO: " + data.modelo + ", PRESENTACION: " + data.presentacion + ", CARACTERISTICA: " + data.caracteristica;

        var parametros = "opcion=GuardarDetPedido&id=0&idpedido=" + $scope.Registro.id + "&articulo=" + General.escape(descripcion) + "&cantidad=1&idarticulo=" + data.idarticulo + "&cuentacontable=" + data.cuenta;
        General.LlamarAjax("Pedido", parametros, false, "", 0).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                $.jGrowl(datos[1], {life: 2500, theme: 'growl-success', header: ''});
                $scope.TablaPedido();
            } else
                swal("Acción Cancelada", datos[1], "warning");
        });
    }

    $scope.CambioTemporal = function () {
        $scope.Registro.temporal = 1;
    }

    $scope.GuardarTempPedido = function (registro, texto, decimal) {
        console.log($scope.Registro.temporal, registro.idpedido);
        if ($scope.Registro.temporal == 0 || registro.idpedido * 1 > 0) {
            return false;
        }

        $rootScope.FormatoSalida(texto, decimal);

        var parametros = "opcion=GuardarDetPedido&id=" + registro.id + "&idpedido=" + $scope.Registro.id + "&articulo=" + General.escape(registro.articulo) + "&cantidad=" + $rootScope.NumeroDecimal(registro.cantidad) + "&idarticulo=" + registro.idarticulo;
        General.LlamarAjax("Pedido", parametros, false, "", 0).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                $.jGrowl(datos[1], {life: 2500, theme: 'growl-success', header: ''});
                $scope.Totales();
            } else
                swal("Acción Cancelada", datos[1], "warning");
        });

    }

    $rootScope.TablaPedido = function () {
        var parametros = "opcion=TablaPedido&id=" + $scope.Registro.id;
        General.LlamarAjax("Pedido", parametros, true, "", 1).then(function (response) {
            $scope.Registro.tabla = response;
            $scope.Totales();
        });
    }

    $scope.Totales = function () {
        $scope.Registro.tcantidad = 0;
        $scope.Registro.tseleccionado = 0;
        $scope.Registro.tabla.forEach(function (item) {
            if (item.seleccionado) {
                $scope.Registro.tcantidad += $rootScope.NumeroDecimal(item.cantidad);
                $scope.Registro.tseleccionado++;
            }
        });
    }


    $scope.GuardarPedido = function () {
        if ($scope.Registro.tseleccionado == 0) {
            swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un artiículo/servicio", "warning");
            return false;
        }
        var a_id = "";
        var a_cantidad = "";
        var a_articulo = "";
        for (var i = 0; i < $scope.Registro.tabla.length; i++) {
            if ($scope.Registro.tabla[i].seleccionado) {
                if (a_id != "") {
                    a_id += ",";
                    a_cantidad += ",";
                    a_articulo += ","
                }
                a_id += $scope.Registro.tabla[i].id;
                a_cantidad += $scope.Registro.tabla[i].cantidad;
                a_articulo += $scope.Registro.tabla[i].idarticulo;
                if ($rootScope.NumeroDecimal($scope.Registro.tabla[i].cantidad) == 0) {
                    swal("Acción Cancelada", "La cantidad del artículo " + $scope.Registro.tabla[i].articulo + " en la fila " + (i + 1) + " esta en " + $scope.Registro.tabla[i].cantidad, "warning");
                    return false;
                }
            }
        }

        var parametros = General.ConvertirParametros($scope.Registro) + "&a_id=" + a_id + "&a_cantidad=" + a_cantidad + "&a_articulo=" + a_articulo;
        console.log(parametros);
        
    }

    $scope.InicializarRegistro();
    $rootScope.TablaPedido();
    $rootScope.InicializarFormulario(7);

});

