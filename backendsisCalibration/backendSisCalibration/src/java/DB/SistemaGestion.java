package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/SistemaGestion"})
public class SistemaGestion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    int PermisoVerArchvio = 0;

    private int PermisoArchivo(String ruta, int tipo) {
        if (PermisoVerArchvio > 0) {
            return 1;
        }
        String accion = (tipo == 1 ? "lec" : "dil");
        sql = "select id from sg.permisos_carpeta where ruta = '" + ruta + "'";
        int idpermiso = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (idpermiso == 0) {
            return 1;
        }
        sql = "SELECT u.idusu\n"
                + "  FROM seguridad.rbac_usuario u inner join rrhh.empleados e on e.id = u.idempleado\n"
                + "			        inner join rrhh.cargo c on e.idcargo = c.id\n"
                + "where idusu = " + idusuario + "  and \n"
                + "	(trim(u.idusu::text) in (select permiso_usu_" + accion + " from sg.permisos_carpeta where ruta = '" + ruta + "')\n"
                + "	or trim(c.id::text) in (select permiso_car_" + accion + " from sg.permisos_carpeta where ruta = '" + ruta + "')\n"
                + "	or sg.permiso_magnitud(u.magnitud, (select permiso_mag_" + accion + " from sg.permisos_carpeta where ruta = '" + ruta + "')) = true\n"
                + "	or (select permiso_todo_" + accion + " from sg.permisos_carpeta where ruta = '" + ruta + "') = 1)";
        return Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
    }

    public String ArchivosSC(HttpServletRequest request) {
        String carpeta = request.getParameter("carpeta");

        int posicion;
        PermisoVerArchvio = Globales.PermisosSistemas("SG VER CUALQUIER ARCHIVO", idusuario);

        try {
            String ruta2 = Globales.ruta_gestion;
            String ruta = ruta2;
            String[] nombres;

            if (!carpeta.equals("XXX")) {
                ruta = carpeta;
                if (ruta.indexOf(ruta2) < 0) {
                    return "1|-|No tiene acceso permitido para esta ruta";
                }
            }

            posicion = ruta.trim().length();

            if (!ruta.trim().substring((posicion - 1), posicion).equals("/")) {
                ruta = ruta.trim() + "/";
            }

            File archivos = new File(ruta);
            String[] listado = archivos.list();
            File directorio;

            String a_extension = "";
            String a_nombre = "";
            String a_fecha = "";
            long a_tamanio = 0;
            String a_carpeta = "";
            String a_ruta = "";

            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            sql = "DELETE FROM sg.ficheros WHERE idusuario=" + idusuario;
            Globales.Obtenerdatos(sql, this.getClass() + "->ArchivosSG", 2);

            String Textosql = "";

            if (listado != null && listado.length > 0) {
                for (int i = 0; i < listado.length; i++) {

                    directorio = new File(ruta + listado[i]);
                    if (directorio.isDirectory()) {
                        a_nombre = listado[i];
                        a_ruta = ruta + listado[i] + "/";
                        if (PermisoArchivo(a_ruta, 1) > 0) {
                            a_carpeta = ruta;
                            a_extension = "carpeta";
                            a_tamanio = directorio.length();

                            Textosql += "INSERT INTO sg.ficheros(ruta, nombre, idusuario, fecha, tamanio, clasificacion, carpeta, tipo) "
                                    + " VALUES ('" + a_ruta + "','" + a_nombre + "'," + idusuario + ",'" + a_fecha + "'," + a_tamanio + ",'" + a_extension + "','" + a_carpeta + "',1);";
                        }
                    } else {
                        a_ruta = ruta;
                        if (PermisoArchivo(ruta + listado[i], 1) > 0) {
                            a_nombre = listado[i];
                            a_extension = Globales.ExtensionArchivo(listado[i]);
                            a_fecha = formato.format(directorio.lastModified());
                            a_tamanio = directorio.length();
                            Textosql += "INSERT INTO sg.ficheros(ruta, nombre, idusuario, fecha, tamanio, clasificacion, carpeta, tipo)  "
                                    + " VALUES ('" + a_ruta + "','" + a_nombre + "'," + idusuario + ",'" + a_fecha + "'," + a_tamanio + ",'" + a_extension + "','" + a_carpeta + "',2);";
                        }
                    }
                }
            }

            if (!Textosql.equals("")) {
                Globales.Obtenerdatos(Textosql, this.getClass() + "->ArchivosSG", 2);
            }

            sql = "SELECT coalesce(p.id,0) as id, f.ruta, case when p.fecha is null then f.fecha else to_char(p.fecha,'yyyy-MM-dd HH24:MI') end as fecha, f.tamanio, f.carpeta, f.nombre, f.clasificacion, "
                    + "      coalesce(v.version,'') as version, coalesce(v.codigo,'') as codigo, coalesce(to_char(v.revision,'yyyy-MM-dd'),'') as fecharevision, coalesce(to_char(v.vencimiento,'yyyy-MM-dd'),'') as vencimiento, "
                    + "      case when coalesce(p.id,0) = 0 then 'NO REVISTADO' ELSE case when p.estado = 'VIGENTE' and to_char(v.fecha,'yyyy-MM-dd HH24:mm') <= to_char(coalesce(v.vencimiento,'2800-01-01'::date),'yyyy-MM-dd HH24:mm')  then 'VIGENTE' ELSE CASE WHEN p.estado <> 'DILIGENCIADO' THEN 'OBSOLETO' ELSE p.estado end END END AS estado, "
                    + "      coalesce(u.nombrecompleto,'') as responsable, coalesce(uc.nombrecompleto,'') as creado, "
                    + "      f.tipo, coalesce(i.icono,'blanco') as icono, coalesce(gestion,'') as gestion "
                    + " FROM sg.ficheros f left join sg.propiedades_archivo p on upper(p.nombre) = upper(f.nombre) and upper(p.ruta) = upper(f.ruta) and p.estado <> 'ELIMINADO' "
                    + "           left join sg.iconos i on upper(i.clasificacion) = upper(f.clasificacion) "
                    + "          left join version_documento v on v.id = p.idversion "
                    + "          left join seguridad.rbac_usuario u on u.idusu = v.idresponsable "
                    + "  left join seguridad.rbac_usuario uc on uc.idusu = p.idusuario "
                    + " where f.nombre not like '%~$%' and f.nombre not like '%.db%' and uc.idusu = " + idusuario 
                    + " ORDER BY f.tipo, f.nombre";

            return "0|-|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ArchivosSG") + "|-|" + ruta;
        } catch (Exception ex) {
            return "1|-|" + ex.getClass();
        }

    }

    public String RutaGestion() {
        return Globales.ruta_gestion;
    }

    public String AbrirArchivosSG(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String ruta = request.getParameter("ruta");
        String nombre = request.getParameter("nombre");
        try {

            if (Globales.PermisosSistemas("SG ABRIR ARCHIVOS NO REVISADOS", idusuario) == 0) {
                if (id == 0) {
                    return "1|No se puede descargar un archivo no revisado por la gestión de calidad";
                }
            }

            String nombre2 = nombre; //.replace(" ", "_");

            String archivo = ruta + nombre;

            File origen = new File(archivo);
            File destino = new File(Globales.ruta_archivo + "ArchivoSG/" + nombre2);

            if (Globales.PermisosSistemas("SG ABRIR ARCHIVOS OBSOLETOS", idusuario) == 0) {
                sql = "SELECT ruta, case when p.estado = 1 and to_char(v.fecha,'yyyy-MM-dd HH24:mm') <= to_char(coalesce(v.vencimiento,'2800-01-01'::date),'yyyy-MM-dd HH24:mm')  then 'VIGENTE' ELSE 'OBSOLETO' END AS estado "
                        + " FROM sg.propiedades_archivo p left join version_documento v on v.id = p.idversion "
                        + " WHERE p.id = " + id;

                datos = Globales.Obtenerdatos(sql, this.getClass() + "->AbrirArchivosSG", 1);
                datos.next();

                String estado = datos.getString("estado");

                if (estado.equals("OBSOLETO")) {
                    return "1|No se puede descargar un archivo obsoleto";
                }
            }
            Files.copy(origen.toPath(), destino.toPath(), StandardCopyOption.REPLACE_EXISTING);
            return "0|" + nombre2;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String VerPermisosCarpeta(HttpServletRequest request) {
        String carpeta = request.getParameter("carpeta");
        sql = "SELECT permiso_usu_lec, permiso_usu_dil, permiso_mag_lec, "
                + " permiso_mag_dil, permiso_car_lec, permiso_car_dil, permiso_todo_lec, permiso_todo_dil "
                + " FROM sg.permisos_carpeta "
                + " where ruta = '" + carpeta + "'";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->VerPermisosCarpeta");

    }

    public String PermisosCarpeta(HttpServletRequest request) {
        String CarpetaPermiso = request.getParameter("CarpetaPermiso");
        String[] UsuariosLec = request.getParameterValues("UsuariosLec");
        String[] UsuariosDil = request.getParameterValues("UsuariosDil");
        String[] CargosLec = request.getParameterValues("CargosLec");
        String[] CargosDil = request.getParameterValues("CargosDil");
        String[] MagnitudesLec = request.getParameterValues("MagnitudesLec");
        String[] MagnitudesDil = request.getParameterValues("MagnitudesDil");
        String TodosLec = request.getParameter("TodosLec");
        String TodosDil = request.getParameter("TodosDil");

        if (TodosDil == null) {
            TodosDil = "0";
        }
        if (TodosLec == null) {
            TodosLec = "0";
        }

        String _usuarioslec = "";
        String _usuariosdil = "";

        String _magnitudeslec = "";
        String _magnitudesdil = "";

        String _cargoslec = "";
        String _cargosdil = "";

        if (Globales.PermisosSistemas("SG ASIGNAR PERMISOS CARPETA", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        if (UsuariosLec != null) {
            for (int x = 0; x < UsuariosLec.length; x++) {
                _usuarioslec += (!_usuarioslec.equals("") ? "," : "") + UsuariosLec[x];
            }
        }
        if (UsuariosDil != null) {
            for (int x = 0; x < UsuariosDil.length; x++) {
                _usuariosdil += (!_usuariosdil.equals("") ? "," : "") + UsuariosDil[x];
            }
        }
        if (MagnitudesLec != null) {
            for (int x = 0; x < MagnitudesLec.length; x++) {
                _magnitudeslec += (!_magnitudeslec.equals("") ? "," : "") + MagnitudesLec[x];
            }
        }
        if (MagnitudesDil != null) {
            for (int x = 0; x < MagnitudesDil.length; x++) {
                _magnitudesdil += (!_magnitudesdil.equals("") ? "," : "") + MagnitudesDil[x];
            }
        }
        if (CargosLec != null) {
            for (int x = 0; x < CargosLec.length; x++) {
                _cargoslec += (!_cargoslec.equals("") ? "," : "") + CargosLec[x];
            }
        }
        if (CargosDil != null) {
            for (int x = 0; x < CargosDil.length; x++) {
                _cargosdil += (!_cargosdil.equals("") ? "," : "") + CargosDil[x];
            }
        }

        sql = "SELECT id FROM sg.permisos_carpeta WHERE ruta = '" + CarpetaPermiso + "'";
        int idpermiso = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (idpermiso == 0) {
            sql = "INSERT INTO sg.permisos_carpeta(ruta, permiso_usu_lec, permiso_usu_dil, permiso_mag_lec, permiso_mag_dil, permiso_car_lec, "
                    + "permiso_car_dil, permiso_todo_lec, permiso_todo_dil,  idusuario) "
                    + "VALUES ('" + CarpetaPermiso + "','" + _usuarioslec + "','" + _usuariosdil + "','" + _magnitudeslec + "','" + _magnitudesdil
                    + "','" + _cargoslec + "','" + _cargosdil + "'," + TodosLec + "," + TodosDil + "," + idusuario + ")";
        } else {
            sql = "UPDATE sg.permisos_carpeta "
                    + "  SET permiso_usu_lec ='" + _usuarioslec + "', permiso_usu_dil='" + _usuariosdil + "', permiso_mag_lec='" + _magnitudeslec + "'"
                    + ",permiso_mag_dil='" + _magnitudesdil + "',permiso_car_lec='" + _cargoslec + "',permiso_car_dil='" + _cargosdil + "'"
                    + ",permiso_todo_lec=" + TodosLec + ", permiso_todo_dil=" + TodosDil  
                    + ",idusuario=" + idusuario + ",fecha = now() WHERE ruta='" + CarpetaPermiso + "'";
        }
        if (Globales.DatosAuditoria(sql, "SG", "SG ASIGNAR PERMISOS CARPETA", idusuario, iplocal, this.getClass() + "->PermisosCarpeta")) {
            return "0|Permisos asignados con exito";
        } else {
            return "1|Error inesperado.... Revise los archivos de log";
        }
    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String ruta_origen = request.getHeader("Origin");

        HttpSession misession = request.getSession();
        idusuario = misession.getAttribute("idusuario").toString();
        iplocal = misession.getAttribute("IPLocal").toString();
        usuario = misession.getAttribute("codusu").toString();
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");

        try (PrintWriter out = response.getWriter()) {
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else {
                if (misession.getAttribute("codusu") == null) {
                    out.print("cerrada");
                } else {
                    if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                        out.print("cambio-ip");
                    } else {
                        String opcion = request.getParameter("opcion");
                        switch (opcion) {

                            //WILMER
                            case "ArchivosSC":
                                out.print(ArchivosSC(request));
                                break;
                            case "AbrirArchivosSG":
                                out.print(AbrirArchivosSG(request));
                                break;
                            case "RutaGestion":
                                out.print(RutaGestion());
                                break;
                            case "PermisosCarpeta":
                                out.print(PermisosCarpeta(request));
                                break;
                            case "VerPermisosCarpeta":
                                out.print(VerPermisosCarpeta(request));
                                break;
                            default:
                                throw new AssertionError();
                        }
                    }
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);

    }

}
