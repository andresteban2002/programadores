﻿CargarTabla();
$("#UnidadDisco").html(LlamarAjax("Configuracion","opcion=UnidadesDisco"));
	
function RespaldarArchivos() {
    var selec = document.getElementsByName("seleccionar[]");
    var unidad = $("#UnidadDisco").val();
    if (unidad == ""){
		swal("Acción Cancelada","Debe de seleccionar una unidad de disco","warning");
		return false;
	}
    var archivos = "";
    ActivarLoad();
	$.jGrowl("Realizando respaldo en segundo plano. No cierre el módulo hasta que el sistema enviará un mensaje cuando el respaldo esté listo", { life: 8500, theme: 'growl-success', header: '' });
	for (x = 0; x < selec.length; x++) {
		if (selec[x].checked) {
			if (archivos != "")
				archivos += ",";
			archivos += selec[x].value;
		}
	}
	var parametro = "opcion=RespaldarArchivos&archivos=" + archivos + "&unidad=" + unidad
	$.ajax({
		url: url_servidor + "Configuracion",
		type: "POST",
		async: true,
		data: parametro,
		dataType: "html",
		cache: false,
		xhrFields: {
			withCredentials: true
		},
		success: function (datos) {
			DesactivarLoad();
			datos = $.trim(datos).split("||");
			if (datos[0] == "0") {
				swal("", datos[1], "success");
			}else{
				swal("Acción Cancelada", datos[1], "warning");
			}
		}
	});
}

function DescargarArchivo(archivo) {
    window.open(archivo);
}

function CargarTabla() {
    var resultado = "";
    var datos = LlamarAjax("Configuracion","opcion=TablaRespaldo");
    eval("data=" + datos);
    for (var x = 0; x < data.length; x++) {
        resultado += "<tr " + (data[x].estado *1 == 0 ? "class='bg-rojoclaro'" : (x % 2 == 1 ? " class='bg-gris' " : "")) + "><td>" + (data[x].estado*1 == 1 ? "<input type='checkbox' name='seleccionar[]' value='" + data[x].id + "' class='form-control' checked />" : "&nbsp;") + "</td>" +
			"<td>" + $.trim(data[x].dias).replace(/,/g, '<br>') + "</td>" +
			"<td>" + $.trim(data[x].horario).replace(/,/g, '<br>') + "</td>" +
            "<td>" + data[x].ruta + "</td>" +
            "<td>" + (data[x].estado*1 == 0 ? "Inactivo" : "Activo") + "</td></tr>";
    }
    $("#TablaRespaldo").html(resultado);
}

$('select').select2();
DesactivarLoad();
