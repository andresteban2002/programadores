﻿var AgregarFila = 0;
$("#Tablas").html(CargarCombo(38, 1));

function CargarTablas(tabla) {
    if (tabla == "") {
        $("#DivTablas").html("<h3>SELECCIONE UNA TABLA</h3>");
        $("#TablaGeneral").remove();
        return false;
    }
    $("#TablaGeneral").remove();
    var datos = LlamarAjax("Configuracion","opcion=TablaSistema&tabla=" + tabla).split("|");
    if (datos[0] = "0") {

        $("#DivTablas").html(datos[1]);
        $("#TablaGeneral").SetEditable({ $addButton: $('#but_add') });
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function IterarCamposEdit(t, n) {
    function i(t) {
        if (null == colsEdi) return !0;
        for (var n = 0; n < colsEdi.length; n++)
            if (t == colsEdi[n]) return !0;
        return !1
    }
    var o = 0;
    t.each(function () {
        o++ , "buttons" != $(this).attr("name") && i(o - 1) && n($(this))
    })
}

function FijModoNormal(t) {
    $(t).parent().find("#bAcep").hide(), $(t).parent().find("#bCanc").hide(), $(t).parent().find("#bEdit").show(), $(t).parent().find("#bElim").show(), $(t).parents("tr").attr("id", "")
}

function FijModoEdit(t) {
    $(t).parent().find("#bAcep").show(), $(t).parent().find("#bCanc").show(), $(t).parent().find("#bEdit").hide(), $(t).parent().find("#bElim").hide(), $(t).parents("tr").attr("id", "editing")
}

function ModoEdicion(t) {
    return "editing" == t.attr("id")
}

function rowAcep(t) {
    var posicion = 0;
    var tabla = $("#Tablas").val();
    var id = "";
    var registros = [];
    var n = $(t).parents("tr"),
        i = n.find("td");
    ModoEdicion(n) && (IterarCamposEdit(i, function (t) {
        var n = t.find("input").val();
        if (posicion == 0)
            id = n;
        else
            registros.push(_escape(n));
        posicion++;
        t.html(n)
    }), FijModoNormal(t), params.onEdit(n))

    var datos = LlamarAjax("Configuracion","opcion=ActualizarDatosTabla&id=" + id + "&tabla=" + tabla + "&registros=" + registros).split("|");
    if (datos[0] == "0") {
        if (id == 0)
            CargarTablas(tabla);
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    } 
}

function rowCancel(t) {
    var n = $(t).parents("tr"),
        i = n.find("td");
    ModoEdicion(n) && (IterarCamposEdit(i, function (t) {
        var n = t.find("div").html();
        t.html(n)
    }), FijModoNormal(t))
}

function rowEdit(t) {
    var posicion = 0;
    var n = $(t).parents("tr"),
        i = n.find("td");
    ModoEdicion(n) || (IterarCamposEdit(i, function (t) {
        var n = t.html(),
            i = '<div style="display: none;">' + n + "</div>",
            o = '<input class="form-control input bg-amarillo" value="' + n + '"' + (posicion == 0 ? "disabled" : "") + '>';
        t.html(i + o)
        posicion++;
    }), FijModoEdit(t))
}

function rowElim(t) {

    var id = $(t).parents("tr").find("td").eq(0).html()*1;
    var tabla = $("#Tablas").val();
    
    if (id == 0) {
        $(t).parents("tr").remove(), params.onDelete();
        return false;
    }
    
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar la fila del ID número ' + id + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Configuracion","opcion=EliminarDatosTabla&id=" + id + "&tabla=" + tabla)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            $(t).parents("tr").remove(), params.onDelete();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
    
    
}

function rowAgreg() {
    if (AgregarFila == 1)
        return false;
    if (0 == $tab_en_edic.find("tbody tr").length) {
        var t = "";
        (i = $tab_en_edic.find("thead tr").find("th")).each(function () {
            "buttons" == $(this).attr("name") ? t += colEdicHtml : t += "<td></td>"
            pos++;
        }), $tab_en_edic.find("tbody").append("<tr>" + t + "</tr>")
    } else {
        var n = $tab_en_edic.find("tr:last");
        n.clone().appendTo(n.parent());
        var i = (n = $tab_en_edic.find("tr:last")).find("td");
        i.each(function () {
            "buttons" == $(this).attr("name") || $(this).html("")
        })
    }
    AgregarFila = 1;
}

function TableToCSV(t) {
    var n = "",
        i = "";
    return $tab_en_edic.find("tbody tr").each(function () {
        ModoEdicion($(this)) && $(this).find("#bAcep").click();
        var o = $(this).find("td");
        n = "", o.each(function () {
            "buttons" == $(this).attr("name") || (n = n + $(this).html() + t)
        }), "" != n && (n = n.substr(0, n.length - t.length)), i = i + n + "\n"
    }), i
}
var $tab_en_edic = null,
    params = null,
    colsEdi = null,
    newColHtml = '<div class="btn btn-glow-group pull-right"><button id="bEdit" type="button" class="btn btn-glow btn-sm btn-default" onclick="rowEdit(this);"><span class="glyphicon glyphicon-pencil" > </span></button><button id="bElim" type="button" class="btn btn-glow btn-sm btn-default" onclick="rowElim(this);"><i class="icon-remove4"></i></button><button id="bAcep" type="button" class="btn btn-glow btn-sm btn-default" style="display:none;" onclick="rowAcep(this);"><i class="icon-checkmark3" > </i></button><button id="bCanc" type="button" class="btn btn-glow btn-sm btn-default" style="display:none;" onclick="rowCancel(this);"><i class="icon-close" > </i></button></div>',
    colEdicHtml = '<td name="buttons">' + newColHtml + "</td>";

$.fn.SetEditable = function (t) {
    params = null;
    colsEdi = null;
    $tab_en_edic = null;
    var n = {
        columnsEd: null,
        $addButton: null,
        onEdit: function () { },
        onDelete: function () { },
        onAdd: function () { }
    };
    params = $.extend(n, t), this.find("thead tr").append('<th name="buttons"></th>'), this.find("tbody tr").append(colEdicHtml), $tab_en_edic = this, null != params.$addButton && params.$addButton.click(function () {
        AgregarFila = 0;
        rowAgreg()
    }), null != params.columnsEd && (colsEdi = params.columnsEd.split(","))
};

$('select').select2();
DesactivarLoad();

$("#Tablas").focus();
