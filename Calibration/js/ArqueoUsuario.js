﻿var efectivo = 0;
var tarjeta = 0;
var cheque = 0;
var consignacion = 0;
var chequepos = 0;
var ingresodia = 0;
var saldoanterior = 0;
var otroingresos = 0;
var TIngresos = 0;
var TEgresos = 0;
var TDeposito = 0;
var otroegresos = 0;
var cajamenorefec = 0;
var cajamenorbanc = 0;
var antincipocli = 0;
var numero = 0;
var idusuario = localStorage.getItem("idusuario");
var nomusuario = localStorage.getItem("usuario");
var fecha = "";

var canvafirma = document.getElementById("pad1");
localStorage.setItem("FirmaCertificado", "");
initPad(canvafirma);


d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#fecha_arqueo").html(InvertirFecha(output, "-"));
$("#CFecha").val(output);
fecha = output;
$("#usuario_arqueo").html(nomusuario);
$("#CTercero").html(CargarCombo(42, 1));
$("#CUsuarios").html(CargarCombo(13, 1));
$("#CUsuarios").val(idusuario);
$("#CCuenta").html(CargarCombo(25, 1));


var BPla = ["", "", "", ""];
var BBan = ["", "", "", ""];
var BEfe = [0, 0, 0, 0];
var BChe = [0, 0, 0, 0];

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    id = $(this).attr("id");
    if ((e.keyCode == 13) && ((id != "Codigo") || (id != "CodigoB"))) {
        cb = parseInt($(this).attr('tabindex'));
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();
            return false;
        }
    }
}

CargarTabla(numero, "", idusuario)

function ImprimirArqueo() {
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Arqueo/ImprimirArqueoUsuario", "numero=" + numero);
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("Documpdf/" + datos[1])
        } else {
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion(datos[1]), "error");
        }
        DesactivarLoad();
    }, 15);

}

function ImprimirGastos(){
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Arqueo/ImprimirCajaMenor", "numero=" + numero + "&tipo=1");
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("Documpdf/" + datos[1])
        } else {
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion(datos[1]), "error");
        }
        DesactivarLoad();
    }, 15);

}

function Limpiar() {

    efectivo = 0;
    tarjeta = 0;
    cheque = 0;
    consignacion = 0;
    chequepos = 0;
    ingresodia = 0;
    saldoanterior = 0;
    otroingresos = 0
    
    TDeposito = 0;
    TIngresos = 0;
    TEgresos = 0;
    otroegresos = 0;
    cajamenorefec = 0;
    cajamenorbanc = 0;
    antincipocli = 0;

    BPla = ["", "", "", ""];
    BEfe = [0, 0, 0, 0];
    BChe = [0, 0, 0, 0];

    $("#tefectivo").html("");
    $("#ttarjeta").html("");
    $("#tcheque").html("");
    $("#tconsignacion").html("");
    $("#tchequepost").val("");

    $("#TSaldoAnt").val("");

    for (var x = 0; x <= 3; x++) {
        $("#VBanco" + x).val("");
    }

    $("#TOIngreso").val("");
    $("#Tcajamenorefec").val("");
    $("#TOEgresos").val("");
    
    CalcularTotal();
}

function Consultar() {
    fecha = $("#CFecha").val();
    numero = $("#CNumero").val() * 1;
    usuario = $("#CUsuarios").val()*1;
    $("#modalConsultar").modal("hide");
    CargarTabla(numero, fecha, usuario)
}

function CargarTabla(numarqueo, fecha, usuario) {

    Limpiar();
    usuario = usuario * 1;
    
    $("#Gua_Arqueo").removeClass("hidden");
    $("#Imp_Arqueo").addClass("hidden");
    $("#Imp_Gastos").addClass("hidden");
    $("#Imp_Egresos").addClass("hidden");
                
    ActivarLoad();
    setTimeout(function () {
        var parametros = "arqueo=" + numarqueo + "&fecha=" + fecha + "&idusuarios=" + usuario;
        
        var datos = LlamarAjax("Arqueo/IngresoArqueoUsuario", parametros);
        if (datos == "[]") {
            DesactivarLoad();
            swal("Acción Cancelada", "No hay movimientos de recibos de caja", "warning");
            return false;
        }
        datos = datos.split("|");
        DesactivarLoad();
        if (datos[0] != "[]") {
            
            var datajson = JSON.parse(datos[0]);
            var table = $('#TablaIngreso').DataTable({
                data: datajson,
                bProcessing: true,
                bDestroy: true,
                columns: [
                    { "data": "fila" },
                    { "data": "caja" },
                    { "data": "cliente" },
                    { "data": "efectivo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                    { "data": "tarjeta", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                    { "data": "cheque", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                    { "data": "chequepos", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                    { "data": "consignacion", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                    { "data": "fecha" },
                    { "data": "fechaopera" }
                ],

                "columnDefs": [
                    { "type": "numeric-comma", targets: 3 }
                ],

                "language": {
                    "url": LenguajeDataTable
                },
                dom: 'Bfrtip',
                buttons: [
                    'excel', 'csv', 'copy','colvis'
                ],
                               
            });

            table.rows().data().each(function (datos, index) {
                efectivo += datos.efectivo * 1;
                tarjeta += datos.tarjeta * 1;
                cheque += datos.cheque * 1;
                consignacion += datos.consignacion * 1;
                chequepos += datos.chequepos * 1;
            });

            
            $("#tefectivo").html(formato_numero(efectivo, 0,_CD,_CM,""));
            $("#ttarjeta").html(formato_numero(tarjeta, 0,_CD,_CM,""));
            $("#tcheque").html(formato_numero(cheque, 0,_CD,_CM,""));
            $("#tconsignacion").html(formato_numero(consignacion, 0,_CD,_CM,""));
            $("#tchequepost").val(formato_numero(chequepos, 0,_CD,_CM,""));
                        
            $("#TSaldoAnt").val(formato_numero(saldoanterior, 0,_CD,_CM,""));
                       
            CalcularTotal();
            
        } else {
            swal("Acción Cancelada", "No hay movimientos de recibos de caja", "warning");
        }

        numero = datos[4] * 1;
        $("#NoCierre").html(numero);

        if (numero > 0) {
            $("#Gua_Arqueo").addClass("hidden");
            $("#Imp_Arqueo").removeClass("hidden");
            $("#Imp_Gastos").removeClass("hidden");
            $("#Imp_Egresos").removeClass("hidden");

        } else {
            $("#Gua_Arqueo").removeClass("hidden");
            $("#Imp_Arqueo").addClass("hidden");
            $("#Imp_Gastos").addClass("hidden");
            $("#Imp_Egresos").addClass("hidden");
        }

        if (datos[2] != "[]") {
            var data = JSON.parse(datos[2]);
            saldoanterior = data[0].saldoanterior;
            antincipocli = data[0].antincipocli;
        }

        if (datos[1] != "[]") {
            var total = 0;
            var data = JSON.parse(datos[1]);
            for (var x = 0; x < data.length; x++) {
                $("#banco" + x).html(data[x].banco);
                total = (data[x].efectivo * 1) + (data[x].cheque * 1);
                $("#VBanco" + x).val(formato_numero(total, 0,_CD,_CM,""));
                BBan[x] = data[x].idcuenta;
                if (data[x].efectivo)
                    BEfe[x] = data[x].efectivo;
                if (data[x].cheque)
                    BChe[x] = data[x].cheque;
                if (data[x].planilla)
                    BPla[x] = data[x].planilla;
            }
        }

        
        if (datos[3] != "[]") {
            var data = JSON.parse(datos[3]);
            for (var x = 0; x < data.length; x++) {
                switch (data[x].tipo) {
                    case 1:
                        otroingresos = data[x].valor*1;
                        $("#TOIngreso").val(formato_numero(data[x].valor, 0,_CD,_CM,""));
                        break;
                    case 2:
                        cajamenorefec = data[x].valor*1;
                        $("#TCajaMenorEfec").val(formato_numero(data[x].efectivo, 0,_CD,_CM,""));
                        $("#TCajaMenorBanc").val(formato_numero(data[x].banco, 0,_CD,_CM,""));
                        break;
                    case 3:
                        otroegresos = data[x].valor*1;
                        $("#TOEgresos").val(formato_numero(data[x].valor, 0,_CD,_CM,""));
                        break;
                }

            }
        }
        
        CalcularTotal();
        
    }, 15);

}

function GuardarArqueo() {

    if (TIngresos == 0) {
        swal("Acción Cancelada", "No hay ingresos de caja para realizar el cierre", "warning");
        return false;
    }
    var usuario = $("#CUsuarios").val() * 1;
    var title = ValidarTraduccion("Advertencia");

    if (TConsignar < 0)
        title = ValidarTraduccion("Cierre en Negativo") + " " + formato_numero(TConsignar, 0,_CD,_CM,"");

    var Mensaje = ValidarTraduccion("Seguro que desea realizar el cierre de") + ": " + ValidarTraduccion("Fecha") + ": " + InvertirFecha(fecha, "-") + " \n " + ValidarTraduccion("Usuario") + ": " + nomusuario;

    var Parametros = "NoCierre=" + numero + "&PorConsignar=" + TConsignar + "&BEfe=" + BEfe + "&BChe=" + BChe + "&BPla=" + BPla + "&BBan=" + BBan + "&fecha=" + fecha +
        "&efectivo=" + efectivo + "&tarjeta=" + tarjeta + "&cheque=" + cheque + "&chequepos=" + chequepos + "&consignacion=" + consignacion +
        "&ingreso=" + ingresodia + "&ingresoanterior=" + saldoanterior +
        "&oingresos=" + otroingresos + "&otegresos=" + otroegresos + "&cajamenorefec=" + cajamenorefec + "&cajamenorban=" + cajamenorbanc + "&codusu=" + usuario + "&antincipocli=" + antincipocli;
    
    swal.queue([{
        title: title,
        text: '¿' + Mensaje + "?",
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        preConfirm: function () {
            return new Promise(function (resolve) {
                $.post("Arqueo/GuardarArqueoUsuario", Parametros)
                    .done(function (data) {
                        data = data.split("|");
                        swal.insertQueueStep(ValidarTraduccion(data[1]));
                        if (data[0] * 1 == 0) {
                            $("#Gua_Arqueo").addClass("hidden");
                            $("#Imp_Arqueo").removeClass("hidden");
                            $("#Imp_Gastos").removeClass("hidden");
                            $("#Imp_Egresos").removeClass("hidden");
                            numero = data[2] * 1;
                            $("#NoCierre").html(numero);
                        }
                        resolve()
                    })
            })
        }
    }])

}

function LlamarBanco(banco) {

    if ($("#NoCierre").html() == "" || $("#banco" + banco).html() == "")
        return false;

    $("#NroBanco").val(banco);

    if (numero == 0) {
        $("#CEfectivo").prop("readonly", false);
        $("#CCheque").prop("readonly", false);
        $("#CPlanilla").prop("readonly", false);
    } else {
        $("#CEfectivo").prop("readonly", true);
        $("#CCheque").prop("readonly", true);
        $("#CPlanilla").prop("readonly", true);
    }

    $("#TBancoConsig").html($("#banco" + banco).html());
    $("#CEfectivo").val(formato_numero(BEfe[banco], 0,_CD,_CM,""));
    $("#CCheque").val(formato_numero(BChe[banco], 0,_CD,_CM,""));
    $("#CPlanilla").val(BPla[banco]);
    $("#CTotal").val(formato_numero((BEfe[banco] * 1) + (BChe[banco] * 1), 0,_CD,_CM,""));
    $("#modalBanco").modal("show");
}

function ValidarBanco(caja) {

    ValidarTexto(caja, 1);
    var banco = $("#NroBanco").val() * 1;

    var cheque = NumeroDecimal($("#CCheque").val()) * 1;
    var efectivo = NumeroDecimal($("#CEfectivo").val()) * 1;

    $("#CTotal").val(formato_numero(cheque + efectivo, 0,_CD,_CM,""));
        
}

function CalcularTotal() {
    ingresodia = efectivo + cheque + tarjeta + consignacion;
    TIngresos = saldoanterior + ingresodia + otroingresos;
    TEgresos = cajamenorefec + otroegresos + BEfe[0] + BEfe[1] + BEfe[2] + BEfe[3] + BChe[0] + BChe[1] + BChe[2] + BChe[3] + tarjeta + consignacion;
    TConsignar = TIngresos - TEgresos;
    TDeposito = BEfe[0] + BEfe[1] + BEfe[2] + BEfe[3] + BChe[0] + BChe[1] + BChe[2] + BChe[3];

    $("#TVentas").val(formato_numero(ingresodia, 0,_CD,_CM,""));
    $("#TConsignar").val(formato_numero(TConsignar, 0,_CD,_CM,""));
    $("#TIngresos").val(formato_numero(TIngresos, 0,_CD,_CM,""));
    $("#TEgresos").val(formato_numero(TIngresos, 0,_CD,_CM,""));
    $("#TDeposito").val(formato_numero(TDeposito, 0,_CD,_CM,""));

}

function GuardarBanco() {

    var cheque = NumeroDecimal($("#CCheque").val()) * 1;
    var efectivo = NumeroDecimal($("#CEfectivo").val()) * 1;
    var planilla = $.trim($("#CPlanilla").val());

    var banco = $("#NroBanco").val() * 1;

    if ((efectivo == 0 && cheque == 0) || planilla == "") {
        $("CEfectivo").focus();
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de ingresar todos los valores") + " <br> " + "(" + ValidarTraduccion("efectivo  ó cheque y planilla") + ")", "warning");
        return false;
    }

    BPla[banco] = planilla;
    BEfe[banco] = efectivo;
    BChe[banco] = cheque;
    

    $("#VBanco" + banco).val(formato_numero(efectivo + cheque, 0,_CD,_CM,""));
    $("#modalBanco").modal("hide");

    CalcularTotal();

}


function LimpiarTercero() {

    $("#IdTercero").val("");
    $("#TNit").val("");
    $("#TNombre").val("");
    $("#TDocumento").val("NIT").trigger("value");

    $("#TControl").val("");
}

function GuardarTercero() {

    var id = $("#IdTercero").val() * 1
    var nit = $.trim($("#TNit").val());
    var nombre = $.trim($("#TNombre").val());
    var documento = $.trim($("#TDocumento").val());
    var verificar = $("#TControl").val();


    if (nit == "" || nombre == "") {
        $("#TNit").focus();
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de ingresar todos los valores") + " <br> " + "(" + ValidarTraduccion("Nit/Cédula y Nombre Completo") + ")", "warning");
        return false;
    }

    if ((documento != "CC") && (verificar == "")) {
        $("#TControl").focus();
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de ingresar un dígito de control válido"), "warning");
        return false;
    }

    var parametros = "id=" + id + "&Nit=" + nit + "&Nombre=" + nombre + "&opcion=1" + "&Documento=" + documento + "&Verificador=" + verificar;
    var datos = LlamarAjax("Arqueo/RegistroTerceros", parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        swal("", ValidarTraduccion(datos[1]), "success");
        $("#IdTercero").val(datos[2]);
        CargarTablaTerceros();
        $("#CTercero").html(CargarCombo(42, 1));
    } else
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion(datos[1]), "warning");
}

function LimpiarCajaMenor() {
    $("#IdCajaMenor").val("0");
    $("#CTercero").val("").trigger("change");
    $("#CConcepto").val("").trigger("change");
    $("#CModoPago").val("Efectivo").trigger("change");
    $("#CCuenta").val("").trigger("change");
    $("#CCuenta").prop("disabled",true);

    $("#CRecibo").val(LlamarAjax("Arqueo/ContadorCM",""));
    $("#CObservacion").val("");
    $("#CValor").val("");
    localStorage.setItem("FirmaCertificado", "");
}

$("#CRecibo").val(LlamarAjax("Arqueo/ContadorCM", ""));

function LLamarFirma() {


    var id = $("#IdCajaMenor").val() * 1
    var tipo = $("#NumTipRegistro").val() * 1

    var valor = NumeroDecimal($("#CValor").val()) * 1;
    var disponible = NumeroDecimal($("#TConsignar").val());

    if (id > 0)
        disponible = disponible - valor;

    if (tipo == 2) {
        if (valor > disponible) {
            $("#CValor").focus();
            swal("Acción Cancelada", "Saldo no disponible para realizar esta transacción", "warning");
            return false;
        }
    }
    if (numero > 0) {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Ya se realizó el cierre para esta fecha y usuario"), "warning");
        return false;
    }

    if ($("#usuario_arquelo").html() == "") {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("No hay movimientos en el arqueo para esta fecha y usuario"), "warning");
        return false;
    }
        
    var tercero = $("#CTercero").val();
    var concepto = $("#CConcepto").val();
    var valor = NumeroDecimal($("#CValor").val()) * 1;
    var modopago = $("#CModoPago").val();
    var cuenta = $("#CCuenta").val() * 1;
    
    var mensaje = "";

    if (tercero == "")
        mensaje += "<br>" + ValidarTraduccion("Ingrese un tercero");
    if (concepto == "")
        mensaje += "<br>" + ValidarTraduccion("Ingrese un concepto");
    if (valor == 0)
        mensaje += "<br>" + ValidarTraduccion("Ingrese un valor");

    if (modopago == "Banco") {
        if (cuenta * 1 == 0)
            mensaje += "<br>" + ValidarTraduccion("Seleccione una cuenta");
    }

    if (mensaje != "") {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Verifique los siguientes campos:<br>") + mensaje, "warning");
        return false;
    }

    clearCanvas(canvas, ctx);
    $("#modalFirma").modal("show");

}

function GuardarFirma(cajamenor) {
    var firma = canvafirma.toDataURL("image/png");
    $.ajax({
        type: 'POST',
        url: url + "Arqueo/GuardarFirma",
        data: "firma=" + firma + "&cajamenor=" + cajamenor,
        success: function (msg) {
            respuesta = "si";
        }
    });
}

function GuardarCajaMenor() {

    if (localStorage.getItem("FirmaCertificado") == "") {
        swal("Acción Cancelada", "Debe de firmar el recibo");
        return false;
    }
    
    var id = $("#IdCajaMenor").val() * 1
    var tipo = $("#NumTipRegistro").val() * 1

    var tercero = $("#CTercero").val();
    var concepto = $("#CConcepto").val();
    var valor = NumeroDecimal($("#CValor").val()) * 1;
    var observacion = $.trim($("#CObservacion").val());
    var recibo = NumeroDecimal($("#CRecibo").val()) * 1;
    var usuario = $("#CUsuarios").val() * 1;
    var modopago = $("#CModoPago").val();
    var cuenta = $("#CCuenta").val() * 1;


    var parametros = "id=" + id + "&Concepto=" + concepto + "&Tercero=" + tercero + "&opcion=1" + "&Observacion=" + observacion + "&Valor=" + valor + "&tipo=" + tipo + "&fecha=" + fecha + "&usuario=" + usuario +
        "&recibo=" + recibo + "&modopago=" + modopago + "&cuenta=" + cuenta;
    var datos = LlamarAjax("Arqueo/RegistroCajaMenor", parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        swal("", ValidarTraduccion(datos[1]), "success");
        if (id == 0) {
            GuardarFirma(datos[2])
        }
        CargarTablaCajaMenor(tipo, 0);
        $("#IdCajaMenor").val(datos[2]);
        $("#CRecibo").val(datos[3]);
        
        $("#modalFirma").modal("hide");
        
    } else
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion(datos[1]), "warning");
}

function CambioDocumento(documento) {
    if (documento == "CC" || documento == "CE" || documento == "PS") {
        $("#TControl").prop("disabled", true);
        $("#TControl").val("0");
    }
    else {
        $("#TControl").prop("disabled", false);
        $("#TControl").val("");
    }
        
}


function CargarTablaTerceros() {
    //TablaTerceros
    datos = LlamarAjax("Arqueo/TablaTerceros", "");
    var datajson = JSON.parse(datos);
    var table = $('#TablaTerceros').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "documento" },
            { "data": "nit" },
            { "data": "verificador" },
            { "data": "nombre" }
        ],

        "language": {
            "url": LenguajeDataTable
        }

    });
        
}

$("#TablaTerceros > tbody").on("click", "tr", function () {
    var row = $(this).parents("td").context.cells;
        
    $("#TNit").val(row[2].innerText);
    $("#TNombre").val(row[4].innerText);
    $("#IdTercero").val(row[0].innerText);
    $("#TDocumento").val(row[1].innerText).trigger("change");
    $("#TControl").val(row[3].innerText);
    return false;
});

$("#TablaTerceros > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    $("#CTercero").val(row[0].innerText).trigger("change");
    $("#modalTerceros").modal("hide");
});

function AgregarOpcion(mensaje, id) {

    var tipo = $("#NumTipRegistro").val() * 1
    
    swal({
        title: 'Agregar Opción',
        text: mensaje,
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if ($.trim(value)) {
                    var datos = LlamarAjax("Cotizacion/GuardarOpcion", "tipo=7&magnitud=0&marca=0&descripcion=" + value + "&tipoconcepto=" + tipo);
                    datos = datos.split("|");
                    if (datos[0] != "XX") {
                        $("#" + id).html(datos[1]);
                        $("#" + id).val(datos[0]).trigger("change");
                        resolve();
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar la descripción de ' + mensaje));
                }
            })
        }
    }).then(function (result) {
        $("#" + id).focus();
    })

}

function CargarTablaCajaMenor(tipo, combo) {
    //TablaTerceros
    if (combo == 1)
        $("#CConcepto").html(CargarCombo(46, 1, "", tipo));
    datos = LlamarAjax("Arqueo/TablaCajaMenor", "tipo=" + tipo + "&fecha=" + fecha + "&usuario=" + idusuario + "&numero=" + numero);
    datos = datos.split("|");
    var datajson = JSON.parse(datos[0]);
    $('#TablaEgresos').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "recibo" },
            { "data": "concepto" },
            { "data": "modopago" },
            { "data": "fecha" },
            { "data": "tercero" },
            { "data": "valor", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "vrecaudado", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "vpendiente", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "estado" },
            { "data": "observacion" },
            { "data": "imprimir" },
            { "data": "anular" }
        ],

        "language": {
            "url": LenguajeDataTable
        }

    });

    
    $("#TotalRCaja").html(formato_numero(datos[1] * 1, 0,_CD,_CM,""));
    $("#TotalREfectivo").html(formato_numero(datos[2] * 1, 0,_CD,_CM,""));
    $("#TotalRBanco").html(formato_numero(datos[3] * 1, 0,_CD,_CM,""));
    switch (tipo) {

        case 1:
            otroingresos = datos[1] * 1;
            $("#TOIngreso").val(formato_numero(datos[1] * 1, 0,_CD,_CM,""));

            break;
        case 2:
            cajamenorefec = datos[2] * 1;
            cajamenorbanc = datos[3] * 1;
            $("#TCajaMenorEfec").val(formato_numero(datos[2] * 1, 0,_CD,_CM,""));
            $("#TCajaMenorBanc").val(formato_numero(datos[3] * 1, 0,_CD,_CM,""));

            break;
    }
    CalcularTotal();
}

$("#TablaEgresos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var id = row[0].innerText;
    var datos = LlamarAjax("Arqueo/DatosCajaMenor", "id=" + id);
    var datava = JSON.parse(datos);
    $("#IdCajaMenor").val(id);
    $("#CTercero").val(datava[0].idtercero).trigger("change");
    $("#CConcepto").val(datava[0].concepto).trigger("change");
    $("#CRecibo").val(datava[0].recibo);
    $("#CValor").val(formato_numero(datava[0].valor, 0,_CD,_CM,""));
    $("#CObservacion").val(datava[0].observacion);
});


function AnularReciboCajaMenor(id) {
    swal.queue([{
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Desea anular este movimiento') + '?',
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve) {
                $.post("Arqueo/RegistroCajaMenor", "id=" + id + "&opcion=2&usuario=0&valor=0&tipo=0&recibo=0&fecha=" + fecha)
                    .done(function (data) {
                        data = data.split("|");
                        if (data[0] == "0") {
                            CargarTablaCajaMenor(tipo,0);
                            $("#CTercero").html(CargarCombo(42, 1));
                        }
                        swal.insertQueueStep(ValidarTraduccion(data[1]));
                        resolve()
                    })
            })
        }
    }])
}

function ImprimirCajaMenor(recibo) {
    var id = $("#IdCajaMenor").val() * 1
    if (recibo == 0) {
        recibo = $("#CRecibo").val();
        if (id == 0) {
            return false;
        }
    }
        
    ActivarLoad();
    setTimeout(function () {
        var parametros = "recibo=" + recibo;
        var datos = LlamarAjax("Arqueo/RpReciboCaMenor", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);
}

function LlamarTerceros() {
    if (numero == 0) {
        CargarTablaTerceros();
        $("#modalTerceros").modal("show");
    }
}

function LlamarRCaja(tipo, descripcion) {

    LimpiarCajaMenor();
    
    $("#NumTipRegistro").val(tipo);
    CargarTablaCajaMenor(tipo, 1)
    $("#TipoRegistro").html(descripcion);
    $("#modalRCaja").modal("show");

    if (numero > 0) {
        $("#Gua_CajaMenor").addClass("hidden");
        $("#Lim_CajaMenor").addClass("hidden");
    } else {
        $("#Gua_CajaMenor").removeClass("hidden");
        $("#Lim_CajaMenor").removeClass("hidden");
    }
}

function CambioFormaPago(modo) {
    if (modo == "Efectivo") {
        $("#CCuenta").val("").trigger("change");
        $("#CCuenta").prop("disabled", true);
    } else {
        $("#CCuenta").prop("disabled", false);
    }
}

$('select').select2();
