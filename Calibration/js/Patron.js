
var Patron = 0;
var datos = LlamarAjax("Configuracion", "opcion=CargaComboInicial&tipo=2").split("||");
$("#Magnitud, #CMagnitud").html(datos[0]);
$("#CIntervalo").html(datos[7]);
$("#Medida, #CMedida").html(datos[24]);
$("#TipoServicio").html(datos[3]);
$("#Descripcion, #CDescripcion").html(datos[25]);
$("#Magnitud").focus();
$("#Plantilla").html(datos[26]);

tb = $('select');
$(tb).keypress(enter2tab);
d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))
var AprobarCotiza = "";

output = d.getFullYear() + 1 + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaCertificado").val(output2);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));
        
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function LimpiarTablas(){
	$('#TablaSensores tbody').empty();
	$('#TablaVM tbody').empty();
	$('#TablaVC tbody').empty();
	
}


function LimpiarTodo() {
            
    ActivarLoad();
    setTimeout(function () {
        
        Patron = 0;
        IdPatron = 0;
        LimpiarDatos(); 
        LimpiarTransductor();   
        LimpiarDatosPatronAux(); 
        LimpiarVMedida();
        LimpiarBitacora();  
        LimpiarTablas()  
        DesactivarLoad();
        LimpiarSensor();
        OpcionPatron("2");
        $("#Magnitud").val("");
        $("#Magnitud").focus();
    }, 15);
}

function LimpiarDatos() {

    Patron = 0;
    $("#IdPatron").val("0");
    $("#Magnitud").html(datos[0]);
    $("#Marca").val("");
	$("#Medida").val("").trigger("change");
	$("#Modelo").val("");
	$("#Transductor").val("").trigger("change");
	$("#Laboratorio").trigger("change");
    $("#AlcanceDesde").val("");
    $("#AlcanceHasta").val("");
    $("#Tolerancia").val("");
    $("#Serie").val("");
    $("#Certificado").val("");
    $("#FechaCertificado").val("");
    $("#FechaProxCalibracion").val("");
    $("#Resolucion").val("");
    $("#Derivada").val("");
    $("#Precision").val("");
    $("#Descripcion").val("").trigger("change");
    $("#Grado").val("");
    $("#Titulo").val("");
    $("#Estado").val("ACTIVO").trigger("change");
    
    $("#Coeficiente0").val("");
    $("#Coeficiente1").val("");
    $("#Coeficiente2").val("");
    $("#Coeficiente3").val("");
    $("#Coeficiente4").val("0");
    $("#Coeficiente5").val("0");
    
    $("#Plantilla").val("").trigger("change");
           
}

function LimpiarTransductor() {
	var transductor = $("#Transductor").val() * 1;
	switch(transductor)
	{
		case 1:
			$("#Punto").val("");
			$("#Lectura").val("");
			$("#Error").val("");
			$("#DerivadaT").val("");
			$("#Expandida1").val("");
			$("#Expandida2").val("");
			$("#BEP").val("");
			$("#BEMAX").val("");
			break;
	}
    /*$("#Punto").val("");
    $("#Lectura").val("");
    $("#Error").val("");
    $("#Derivada1").val("");
    $("#Derivada2").val("");
    $("#Derivada3").val("");
    $("#Derivada4").val("");
    $("#Expandida1").val("");
    $("#Expandida2").val("");
    $("#BEP").val("");
    $("#BEMAX").val("");
    $("#Plantilla").val("");
    $("#Total").val("");
    $("#ErrorMed").val("");
    $("#ErrorEstima").val("");
    $("#ErrorResi").val("");
    $("#Expandida").val("");
    $("#UTotal").val("");
    $("#LecturaPatron").val("");
    $("#LecturaIBC").val("");
    $("#UExpandida").val("");
    $("#ErrorAnt").val("");
    $("#ValorNominal").val("");
    $("#Desviacion").val("");
    $("#Variacion").val("");
    $("#UPatron").val("");
    $("#DenominadorDerivada").val("");
    $("#SP").val("");    
    $("#U").val("");*/
}
function LimpiarBitacora() {
    
    $("#TipoServicio").val("");
    $("#Archivo").val("");
    $("#Observacion").val("");
    $("#Reviso").val("");        
}


function ConsultarPatron() {


    var magnitud = $("#CMagnitud").val() * 1;    
    var modelo = $("#CModelo").val();
    var marca = $("#CMarca").val();
    var serie = ($("#CSerie").val());
    var descripcion = $("#CDescripcion").val();   
   

    var grado = $("#CGrado").val();
    var proveedor = $("#CProveedor").val() * 1;
    var certificado = $("#CCertificado").val();
    var fechacertificado = $("#CFechaCalibracion").val();
    var fechadesde = $("#Cfechadesde").val();   
    var fechahasta = $("#Cfechahasta").val();   
    
//Andres
    ActivarLoad();
    setTimeout(function () {
        var parametros = "magnitud=" + magnitud + "&marca=" + marca + "&modelo=" + modelo +
            "&descripcion=" + descripcion + "&serie=" + serie + "&grado=" + grado + "&proveedor=" + proveedor+ "&certificado=" + certificado
            + "&fechacertificado=" + fechacertificado+ "&fechadesde=" + fechadesde+ "&fechahasta=" + fechahasta;
        var datos = LlamarAjax("Patron","opcion=ConsultarPatron&"+parametros).split("||");
        DesactivarLoad();
        //Fin Codigo

        var datajson = JSON.parse(datos[0]);
        $('#TablaPatrones').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [                
                { "data": "patron"},
                { "data": "estado"},
                { "data": "magnitud"},
                { "data": "alcancedesde" },
                { "data": "alcancehasta" },
                { "data": "medida" },
                { "data": "marca" },
                { "data": "modelo" },
                { "data": "serie" },                
                { "data": "certificado" },
                { "data": "fechacertificado" },
                { "data": "proximacalibracion" },
                { "data": "resolucion" },
                { "data": "laboratorio" },
                { "data": "tolerancia" },
                { "data": "derivada" },
                { "data": "precision" },
                { "data": "descripcion" },
                { "data": "grado" },
                { "data": "titulo" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
            
        });       
	},15);		
}

$("#TablaPatrones > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var id = row[0].innerText;
    $('#tab-patron a[href="#tabcrear"]').tab('show')
    BuscarPatron(id);
});

function BuscarPatron(id) {
        
    if (Patron*1 == id*1)
        return false;

    ActivarLoad();
    setTimeout(function () {

        LimpiarDatos();              
        if ((id * 1) == 0)
            return false;
        
        var parametros = "patron=" + id;
        var datos = LlamarAjax('Patron','opcion=BuscarPatron&'+parametros);
        DesactivarLoad();
        
        if (datos != "[]") {
            var datacot = JSON.parse(datos);
            Patron = id;
            $("#IdPatron").val(datacot[0].id); 
            $("#Magnitud").val(datacot[0].magnitud);                                
            $("#Medida").val(datacot[0].medida).trigger("change");            
            $("#AlcanceDesde").val(datacot[0].alcancedesde);
            $("#AlcanceHasta").val(datacot[0].alcancehasta);            
            $("#Marca").val(datacot[0].marca);            
            $("#Modelo").val(datacot[0].modelo);                       
            $("#Serie").val(datacot[0].serie);            
            $("#Certificado").val(datacot[0].certificado);
            $("#FechaCertificado").val(datacot[0].fechacertificado);
            $("#FechaProxCalibracion").val(datacot[0].proximacalibracion);
            $("#Resolucion").val(datacot[0].resolucion);
            $("#Laboratorio").val(datacot[0].laboratorio);
            $("#Transductor").val(datacot[0].transductor).trigger("change");
            $("#Tolerancia").val(datacot[0].tolerancia);
            $("#Derivada").val(datacot[0].derivada);
            $("#Precision").val(datacot[0].precision);
            $("#Descripcion").val(datacot[0].descripcion).trigger("change");
            $("#Grado").val(datacot[0].grado);
            $("#Titulo").val(datacot[0].titulo);  
            $("#Estado").val(datacot[0].estado).trigger("change");
            
        } else {            
            swal("Acción Cancelada", "Patron número " + id + " no registrado", "warning");
        }
    },15);
}

$("#formPatrones").submit(function (e) {
    e.preventDefault();    
    var parametros = $("#formPatrones").serialize();
    var datos = LlamarAjax("Patron","opcion=GuardarPatron&" + parametros).split("|");
    if (datos[0] == "0") {
        ConsultarPatron();
        swal("", datos[1], "success");
        $("#IdPatron").val(datos[2]);                        
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

    return false;

});

function GuardarTransductor(){
    if (Patron == 0){
		swal("Acción Cancelada","Debe de seleccionar un patrón","warning");
		return false;
	}
	
	var Transductor = $("#NTransductor").val()*1;
	var IdTransductor = $("#IdTransductor").val()*1;
	var Punto = $.trim($("#Punto").val());
	var Lectura = $.trim($("#Lectura").val());
	var ErrorT = $.trim($("#Error").val());
	var DerivadaT = $.trim($("#DerivadaT").val());
	var Expandida1 = $.trim($("#Expandida1").val());
	var Expandida2 = $.trim($("#Expandida2").val());
	var BEP = $.trim($("#BEP").val());
	var BEMAX = $.trim($("#BEP").val());
	var Plantilla = $("#Plantilla").val()*1;
	
	if (Punto == ""){
		$("#Punto").focus();
		swal("Acción Cancelada", "Debe de ingresar el valor del punto", "warning");
		return false;
	}
	
	if (Lectura == ""){
		$("#Lectura").focus();
		swal("Acción Cancelada", "Debe de ingresar el valor de la lectura promedio", "warning");
		return false;
	}
	
	if (ErrorT == ""){
		$("#ErrorT").focus();
		swal("Acción Cancelada", "Debe de ingresar el valor del error promedio", "warning");
		return false;
	}
	
	if (Expandida1 == ""){
		$("#Expandida1").focus();
		swal("Acción Cancelada", "Debe de ingresar el valor de la U expandida", "warning");
		return false;
	}
	
	if (DerivadaT == ""){
		$("#DerivadaT").focus();
		swal("Acción Cancelada", "Debe de ingresar el valor de la derivada", "warning");
		return false;
	}
	
	if (BEP == ""){
		$("#BEP").focus();
		swal("Acción Cancelada", "Debe de ingresar el valor de bep", "warning");
		return false;
	}
	
	if (BEMAX == ""){
		$("#BEMAX").focus();
		swal("Acción Cancelada", "Debe de ingresar el valor de bet, max", "warning");
		return false;
	}
	
	if (Expandida2 == ""){
		$("#Expandida2").focus();
		swal("Acción Cancelada", "Debe de ingresar el valor de la U expandida k=2", "warning");
		return false;
	}
		
		
    var parametros = "IdPatron=" + Patron + "&Transductor=" + Transductor + "&IdTransductor=" + IdTransductor + "&Punto=" + Punto + "&Lectura=" + Lectura + "&Error=" + ErrorT + 
					  "&DerivadaT=" + DerivadaT + "&Expandida1=" + Expandida1 + "&Expandida2=" + Expandida2 + "&BEP=" + BEP + "&BEMAX=" + BEMAX + "&Plantilla=" + Plantilla;
    var datos = LlamarAjax("Patron","opcion=GuardarTransductor&" + parametros).split("|");
    if (datos[0] == "0") {
		LimpiarTransductor();
        ConsultarTransductor();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}


function GuardarBitacora() {    
	
    IdPatron = $("#IdPatron").val();
    TipoServicio = $("#TipoServicio").val();
    Observacion = $("#Observacion").val();
    Reviso = $("#Reviso").val();
    Archivo = $("#Archivo").val();
    
    
    
    var parametros = $("#formPatrones").serialize() + "&IdPatron=" + IdPatron + "&TipoServicio=" + TipoServicio + "&Observacion=" + Observacion
     + "&Reviso=" + Reviso + "&Archivo=" + Archivo ;
    var datos = LlamarAjax("Patron","opcion=GuardarBitacora&" + parametros).split("|");
    if (datos[0] == "0") {        
        swal("", datos[1], "success");
        ConsultarBitacora();
            
        
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

    return false;
	
}


function OpcionPatron(opcion){
	switch(opcion)
	{
		case "1":
		$("#opciones-patron").html("<li id='proli1' class='active'><a id='lab_crearmodificar' href='#tabcomerciales'data-toggle='tab'><i class='icon-checkbox-partial '></i><span"+
		"idioma='Certificados Realizados'>Certificados Realizados</span></a></li><li id='proli7'><a href='#tabsedes' data-toggle='tab'><i class='icon-checkmark3'></i><span"+
        "idioma='Estadisticas de valores patrón'>Estadisticas de valores patrón</span></a></li><li id='proli7'><a href='#tabtransductores' data-toggle='tab'><i class='icon-tools'></i><span idioma='Transductores'>Transductores</span></a></li>"+
        "<li id='proli7'><a href='#tabbitacoras' data-toggle='tab'><i class='icon-clipboard'></i><span idioma='Bitácoras'>Bitácoras</span></a></li>");
        $("#bitacoras").show();
        
        
		break;
		
		
		case "2":
		$("#opciones-patron").html("<li id='proli1'><a id='lab_crearmodificar' href='#tabcomerciales'data-toggle='tab'><i class='icon-checkbox-partial'></i><span"+
		"idioma='Certificados Realizados'>Certificados Realizados</span></a></li><li id='proli7'><a href='#tabsedes' data-toggle='tab'><i class='icon-checkmark3'></i><span"+
        "idioma='Estadisticas de valores patrón'>Estadisticas de valores patrón</span></a></li><li id='proli7'><a href='#tabtransductores' data-toggle='tab'><i class='icon-tools'></i><span idioma='Transductores'>Transductores</span></a></li>");
        $("#bitacoras").hide();
        
		break;
	}
	
}

function CamposTransductores(transductor) {    
	transductor = transductor*1;
	if (transductor == 0){
		$("#formtransductor").html("");
		$("#TablaTransductores").html("");
		return false;
	}
	switch(transductor)
	{
		case 1:
			$("#formtransductor").html("<form id='FormTransductor'>" +
			"<input type='hidden' name='IdTransductor' id='IdTransductor'><input type='hidden' name='NTransductor' id='NTransductor' value='1'>" +
			"<div class='form-group row'><div class='col-sm-2'><label class='control-label' idioma='Punto'>Punto</label> " +
			"<input type='text' name='Punto' id='Punto' class='form-control text-XX text-right' onkeyup='ValidarTexto(this,3)'></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Lectura'>Lectura</label> "+
			"<input type='text' name='Lectura' id='Lectura' class='form-control text-XX text-right' onkeyup='ValidarTexto(this,3)'></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Error'>Error</label>  "+
			"<input type='text' name='Error' id='Error' class='form-control text-XX text-right' onkeyup='ValidarTexto(this,3)'></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Expandida 1'>Expandida 1</label>"+
			"<input type='text' name='Expandida1' id='Expandida1' class='form-control text-XX text-right' onkeyup='ValidarTexto(this,3)'></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Derivada'>Derivada</label>"+
			"<input type='text' name='DerivadaT' id='DerivadaT' class='form-control text-XX text-right' onkeyup='ValidarTexto(this,3)'></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='bep'>bep</label>"+
			"<input type='text' name='BEP' id='BEP' class='form-control text-XX text-right' onkeyup='ValidarTexto(this,3)'></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='be, max'>be, max</label>"+
			"<input type='text' name='BEMAX' id='BEMAX' class='form-control text-XX text-right' onkeyup='ValidarTexto(this,3)'></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Expandida 2'>Expandida 2</label>"+
			"<input type='text' name='Expandida2' id='Expandida2' class='form-control text-XX text-right' onkeyup='ValidarTexto(this,3)'></div>"+
			"</div>" +
			"<div class='form-group row text-right'><div class='col-sm-8'></div><div class='col-sm-4'><button class='btn btn-glow btn-default' type='button' onclick='LimpiarTransductor(1)'><span idioma='Limpiar'>Limpiar</span></button>"+
			"<button class='btn btn-glow btn-info' type='button' onclick='DescargarArchivo()'><span idioma='Descargar Formato xlsx'>Descargar Formato xlsx</span></button>" +
			"<button class='btn btn-glow btn-success' type='button' onclick='LlamarImportarTransductor()'><span idioma='Importar/Excel'>Importar/Excel</span></button><button class='btn btn-glow btn-primary' type='button' onclick='GuardarTransductor()'><span idioma='Guardar'>Guardar</span></button></div></div></form>");       
			
			$("#TablaTransductores").html(" <table class='table display  table-bordered hover' id='TablaTransductor'><thead><tr><th idioma='Id'>Id</th><th idioma='Par Aplicado'>Par Aplicado</th><th idioma='Lectura<br>Promedio'>Lectura<br>Promedio</th><th idioma='Error<br>Promedio'>Error<br>Promedio</th><th idioma='U Expandida'>U Expandida</th>"+
			"<th idioma='Derivada'>Derivada</th><th idioma='bep'>bep</th> <th idioma='be, max'>be, max</th><th idioma='U Expandida<br>k=2'>U Expandida<br>k=2</th><th width='3%'>&nbsp;</th>"+
			"</tr></thead><tbody></tbody></table>");
			break;
		case 2:    
			$("#formtransductor").html("<div class='form-group row'><div class='col-sm-2'><label class='control-label' idioma='Tipo'>Tipo</label>"+
			"<input type='text' name='Tipo' id='Tipo' class='form-control' ></div> " +
			"<div class='col-sm-2'><label class='control-label' idioma='Punto'>Punto</label> "+
			"<input type='text' name='Punto' id='Punto' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Lectura'>Lectura</label> "+
			"<input type='text' name='Lectura' id='Lectura' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Error Medición'>Error Medición</label>  "+
			"<input type='text' name='ErrorMed' id='ErrorMed' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Total'>Total</label>"+
			"<input type='text' name='Total' id='Total' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Derivada'>Derivada</label>"+
			"<input type='text' name='Derivada2' id='Derivada2' class='form-control' ></div></div><div class='form-group row'>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Error Estimado'>Error Estimado</label>"+
			"<input type='text' name='ErrorEstima' id='ErrorEstima' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Error Residual'>Error Residual</label>"+
			"<input type='text' name='ErrorResi' id='ErrorResi' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Expandida'>Expandida</label>"+
			"<input type='text' name='Expandida' id='Expandida' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='U Total'>U Total</label>"+
			"<input type='text' name='UTotal' id='UTotal' class='form-control' ></div></div>" +
			"<div class='form-group row text-right'><div class='col-sm-8'></div><div class='col-sm-4'><button class='btn btn-glow btn-default' type='button' onclick='LimpiarTransductor()'><span idioma='Limpiar'>Limpiar</span></button>"+
			"<button class='btn btn-glow btn-success' type='button' onclick='GuardarTransductor()'><span idioma='Importar/Excel'>Importar/Excel</span></button><button class='btn btn-glow btn-primary' type='button' onclick='GuardarTransductor()'><span idioma='Guardar'>Guardar</span></button></div></div>");       
		
			$("#TablaTransductores").html("<table class='table display  table-bordered hover' id='TablaTransductor'><thead><tr><th idioma='Id'>Id</th><th idioma='Tipo'>Tipo</th><th idioma='Punto'>Punto</th> <th idioma='Lectura'>Lectura</th><th idioma='Error Medición'>Error Medición</th><th idioma='Total'>Total</th>"+
			"<th idioma='Derivada'>Derivada</th><th idioma='Error Estimado'>Error Estimado</th> <th idioma='Error Residual'>Error Residual</th><th idioma='Expandida'>Expandida</th><th idioma='U Total'>U Total</th>"+
			"</tr></thead><tbody></tbody></table>");
			break;
		case 3:   		
			$("#formtransductor").html("<div class='form-group row'><div class='col-sm-2'> <label class='control-label' idioma='Lectura Patron'>Lectura Patron</label> "+
			"<input type='text' name='LecturaPatron' id='LecturaPatron' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Lectura IBC'>Lectura IBC</label>  "+
			"<input type='text' name='LecturaIBC' id='LecturaIBC' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Error Medición'>Error Medición</label>  "+
			"<input type='text' name='ErrorMed' id='ErrorMed' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Expandida'>Expandida</label>"+
			"<input type='text' name='UExpandida' id='UExpandida' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Derivada'>Derivada</label>"+
			"<input type='text' name='Derivada3' id='Derivada3' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Error Estimado'>Error Estimado</label>"+
			"<input type='text' name='ErrorEstima' id='ErrorEstima' class='form-control' ></div>"+
			"</div><div class='form-group row'>"+    
			"<div class='col-sm-2'> <label class='control-label' idioma='Error Anterior'>Error Anterior</label>"+
			"<input type='text' name='ErrorAnt' id='ErrorAnt' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Error Residual'>Error Residual</label>"+
			"<input type='text' name='ErrorResi' id='ErrorResi' class='form-control' ></div>"+    
			"<div class='col-sm-2'> <label class='control-label' idioma='U Total'>U Total</label>"+
			"<input type='text' name='UTotal' id='UTotal' class='form-control' ></div></div>" +
			"<div class='form-group row text-right'><div class='col-sm-8'></div><div class='col-sm-4'><button class='btn btn-glow btn-default' type='button' onclick='LimpiarTransductor()'><span idioma='Limpiar'>Limpiar</span></button>"+
			"<button class='btn btn-glow btn-success' type='button' onclick='LlamarImportarTransductor()'><span idioma='Importar/Excel'>Importar/Excel</span></button><button class='btn btn-glow btn-primary' type='button' onclick='GuardarTransductor()'><span idioma='Guardar'>Guardar</span></button></div></div>");       
			
			$("#TablaTransductores").html("<table class='table display  table-bordered hover' id='TablaTransductor'><thead><tr><th idioma='Id'>Id</th><th idioma='Lectura Patron'>Lectura Patron</th><th idioma='Lectura IBC'>Lectura IBC</th><th idioma='Error Medición'>Error Medición</th><th idioma='Expandida'>Expandida</th>"+
			"<th idioma='Derivada'>Derivada</th><th idioma='Error Estimado'>Error Estimado</th><th idioma='Error Anterior'>Error Anterior</th><th idioma='Error Residual'>Error Residual</th><th idioma='U Total'>U Total</th>"+
			"</tr></thead><tbody></tbody></table>");
			break;
		case 4:
			$("#formtransductor").html("<div class='form-group row'><div class='col-sm-2'> <label class='control-label' idioma='Valor Nominal'>Valor Nominal</label> "+
			"<input type='text' name='ValorNominal' id='ValorNominal' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Desviación (Longitud)' >Desviación (Longitud)</label>  "+
			"<input type='text' name='Desviacion' id='Desviacion' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Variación (Longitud)'>Variación (Longitud)</label>  "+
			"<input type='text' name='Variacion' id='Variacion' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='UPatron'>UPatron</label>"+
			"<input type='text' name='UPatron' id='UPatron' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Derivada'>Derivada</label>"+
			"<input type='text' name='Derivada4' id='Derivada4' class='form-control' ></div>"+       
			"<div class='col-sm-2'> <label class='control-label' idioma='Denominador Derivada'>Denominador Derivada</label>"+
			"<input type='text' name='DenominadorDerivada' id='DenominadorDerivada' class='form-control' ></div></div>" +
			"<div class='form-group row text-right'><div class='col-sm-8'></div><div class='col-sm-4'><button class='btn btn-glow btn-default' type='button' onclick='LimpiarTransductor()'><span idioma='Limpiar'>Limpiar</span></button>"+
			"<button class='btn btn-glow btn-success' type='button' onclick='GuardarTransductor()'><span idioma='Importar/Excel'>Importar/Excel</span></button><button class='btn btn-glow btn-primary' type='button' onclick='GuardarTransductor()'><span idioma='Guardar'>Guardar</span></button></div></div>");       
			
			$("#TablaTransductores").html("<table class='table display  table-bordered hover' id='TablaTransductor'><thead><tr><th idioma='Id'>Id</th><th idioma='Valor Nominal'>Valor Nominal</th><th idioma='Desviación (Longitud)'>Desviación (Longitud)</th><th idioma='Variación (Longitud)'>Variación (Longitud)</th><th idioma='UPatron'>UPatron</th>"+
			"<th idioma='Derivada'>Derivada</th><th idioma='Denominador Derivada'>Denominador Derivada</th>"+
			"</tr></thead><tbody></tbody></table>");
		
			break;
		case 5:    
			$("#formtransductor").html("<div class='form-group row'><div class='col-sm-2'> <label class='control-label' idioma='SP'>SP</label> "+
			"<input type='text' name='SP' id='SP' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='Error' >Error</label>  "+
			"<input type='text' name='Error' id='Error' class='form-control' ></div>"+
			"<div class='col-sm-2'> <label class='control-label' idioma='U'>U</label>  "+
			"<input type='text' name='U' id='U' class='form-control' ></div></div>" +
			"<div class='form-group row text-right'><div class='col-sm-8'></div><div class='col-sm-4'><button class='btn btn-glow btn-default' type='button' onclick='LimpiarTransductor()'><span idioma='Limpiar'>Limpiar</span></button>"+
			"<button class='btn btn-glow btn-success' type='button' onclick='GuardarTransductor()'><span idioma='Importar/Excel'>Importar/Excel</span></button><button class='btn btn-glow btn-primary' type='button' onclick='GuardarTransductor()'><span idioma='Guardar'>Guardar</span></button></div></div>");       
			
			$("#TablaTransductores").html("<table class='table display  table-bordered hover' id='TablaTransductor'><thead><tr><th idioma='Id'>Id</th><th idioma='SP'>SP</th><th idioma='Error'>Error</th><th idioma='U'>U</th>"+    
			"</tr></thead><tbody></tbody></table>");
				
			break;	
	}
}


function ConsultarBitacora() {

    var idpatron = $("#IdPatron").val() * 1;    
    ActivarLoad();
    setTimeout(function () {
        var parametros = "idpatron=" + idpatron;
        var datos = LlamarAjax("Patron","opcion=ConsultarBitacora&"+parametros).split("||");
        DesactivarLoad();
        //Fin Codigo
        var datajson = JSON.parse(datos[0]);
        
		$('#TablaBitacora').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [                
                { "data": "id"},
                { "data": "fecha"},
                { "data": "tiposervicio" },
                { "data": "proveedor" },
                { "data": "evidencia" },
                { "data": "observacion" },
                { "data": "responsable" },
                { "data": "reviso" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip'            
        });                               
	}, 15);		
}

function EditarTranductor(id) {
	var transductor = $("#Transductor").val()*1;
    var parametros = "id=" + id + "&transductor=" + transductor;
	var datos = LlamarAjax("Patron","opcion=BuscarTransductor&"+parametros);
	var data = JSON.parse(datos);
	switch(transductor){
		case 1:
			$('#IdTransductor').val(data[0].id);
			$('#Punto').val(data[0].punto);
			$('#Lectura').val(data[0].lectura);
			$('#Error').val(data[0].error);
			$('#Expandida1').val(data[0].expandida1);
			$('#DerivadaT').val(data[0].derivada);
			$('#BEP').val(data[0].bep);
			$('#BEMAX').val(data[0].bemax);
			$('#Expandida2').val(data[0].expandida2);
		break;
	}
}

function EliminarTranductor(id,punto) {
	var transductor = $("#Transductor").val()*1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar los datos del punto ' + punto + ', del valor del certificado del patrón?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor+ "Patron", "opcion=EliminarTransductor&id=" + id + "&Transductor=" + transductor)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            ConsultarTransductor();
                            EliminarTranductor();
                            resolve();
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

function LlamarImportarTransductor(){
	var patron = $("#IdPatron").val() * 1;    
	if (patron == 0)
		return false;
	$("#IPlantilla").html($("#Plantilla option:selected").text());
	$("#IArchivo").val("");
	$("#modalTransductor").modal("show");
	
}

function DescargarArchivo(){
   var transductor = $("#Transductor").val()*1;
   window.open(url_archivo + "/Documentos/Transductor" + transductor + ".xlsx");
}

function ImportarTransductor(){
	var patron = $("#IdPatron").val() * 1;    
    var transductor = $("#Transductor").val()*1;
    var plantilla = $("#Plantilla").val()*1;
    var archivo = $.trim($("#IArchivo").val());
    
    if (archivo == ""){
		$("#IArchivo").focus();
		swal("Acción Cancelada","Debe de examinar un archivo","warning");
		return false;
	}
	
	ActivarLoad();
    setTimeout(function () {
		var parametros = "patron=" + patron + "&transductor=" + transductor + "&plantilla=" + plantilla;
		var datos = LlamarAjax("Patron","opcion=ImportarArchivoTransductor&"+parametros).split("|");
		DesactivarLoad();
		if (datos[0] == "0"){
			ConsultarTransductor();
			swal("",datos[1],"success");
		}else{
			swal("Acción Cancelada",datos[1],"warning");
		}
	},15);
}


function ConsultarTransductor() {


    var idpatron = $("#IdPatron").val() * 1;    
    var transductor = $("#Transductor").val()*1;
    var plantilla = $("#Plantilla").val()*1;
    
    $("#Coeficiente0").val("");
    $("#Coeficiente1").val("");
    $("#Coeficiente2").val("");
    $("#Coeficiente3").val("");
    $("#Coeficiente4").val("0");
    $("#Coeficiente5").val("0");
    BuscarCoeficientes();

    ActivarLoad();
    setTimeout(function () {
        var parametros = "idpatron=" + idpatron + "&transductor=" + transductor + "&plantilla=" + plantilla;
        var datos = LlamarAjax("Patron","opcion=ConsultarTransductor&"+parametros).split("||");
        DesactivarLoad();
        //Fin Codigo
        var datajson = JSON.parse(datos[0]);
        switch(transductor)
		{
			case 1:
				$('#TablaTransductor').DataTable({
					data: datajson,
					bProcessing: true,
					bDestroy: true,
					columns: [                
						{ "data": "id"},
						{ "data": "punto" },
						{ "data": "lectura" },
						{ "data": "error" },
						{ "data": "expandida1" },
						{ "data": "derivada" },
						{ "data": "bep" },
						{ "data": "bemax" },
						{ "data": "expandida2" },                
						{ "data": "eliminar" }
					],

					"language": {
						"url": LenguajeDataTable
					},
					dom: 'Bfrtip',
					buttons: ['excel', 'csv', 'copy', 'colvis']
				});
				break;
			case 2:  
				$('#TablaTransductor').DataTable({
					data: datajson,
					bProcessing: true,
					bDestroy: true,
					columns: [                
						{ "data": "id"},
						{ "data": "punto" },
						{ "data": "lectura" },
						{ "data": "error_med" },
						{ "data": "total" },
						{ "data": "derivada" },
						{ "data": "error_estina" },
						{ "data": "error_reci" },                
						{ "data": "expandida" },                
						{ "data": "utotal" }
					],

					"language": {
						"url": LenguajeDataTable
					},
					dom: 'Bfrtip',
					buttons: ['excel', 'csv', 'copy', 'colvis']
				}); 
				break;
			case 3:   
				$('#TablaTransductor').DataTable({
					data: datajson,
					bProcessing: true,
					bDestroy: true,
					columns: [                
						{ "data": "id"},
						{ "data": "lecturaPatron"},
						{ "data": "lecturaIBC" },
						{ "data": "error_med" },
						{ "data": "uexpandida" },   
						{ "data": "derivada" },             
						{ "data": "errorestimado" },
						{ "data": "erroranterior" },                
						{ "data": "errorresidual" },
						{ "data": "utotal" }
					],

					"language": {
						"url": LenguajeDataTable
					},
					dom: 'Bfrtip',
					buttons: ['excel', 'csv', 'copy', 'colvis']
					
				});
				break;
			case 4:
				$('#TablaTransductor').DataTable({
					data: datajson,
					bProcessing: true,
					bDestroy: true,
					columns: [                
						{ "data": "id"},
						{ "data": "valornominal"},
						{ "data": "desviacionlongitud" },
						{ "data": "variacionlongitud" },
						{ "data": "UPatron" },                
						{ "data": "Derivada" },
						{ "data": "DenominadorDerivada" }
					],

					"language": {
						"url": LenguajeDataTable
					},
					dom: 'Bfrtip',
					buttons: ['excel', 'csv', 'copy', 'colvis']
				});
				break;
			case 5:    
				$('#TablaTransductor').DataTable({
					data: datajson,
					bProcessing: true,
					bDestroy: true,
					columns: [                
						{ "data": "id"},
						{ "data": "sp"},
						{ "data": "error" },
						{ "data": "u" }
					],

					"language": {
						"url": LenguajeDataTable
					},
					dom: 'Bfrtip',
					buttons: ['excel', 'csv', 'copy', 'colvis']
					
				});
				break;	
		}
	},15);		
}

function GuardarCoeficientes(){
	
    if (Patron == 0){
		swal("Acción Cancelada","Debe de seleccionar un patrón","warning");
		return false;
	}
	
	var plantilla = $("#Plantilla").val()*1;
	
	var Coeficiente0 = $.trim($("#Coeficiente0").val());
	var Coeficiente1 = $.trim($("#Coeficiente1").val());
	var Coeficiente2 = $.trim($("#Coeficiente2").val());
	var Coeficiente3 = $.trim($("#Coeficiente3").val());
	var Coeficiente4 = $.trim($("#Coeficiente4").val());
	var Coeficiente5 = $.trim($("#Coeficiente5").val());
			
	if (Coeficiente0 == ""){
		$("#Coeficiente0").focus();
		swal("Acción Cancelada", "Debe de ingresar el valor del coeficiente 0", "warning");
		return false;
	}
	
	if (Coeficiente1 == ""){
		$("#Coeficiente1").focus();
		swal("Acción Cancelada", "Debe de ingresar el valor del coeficiente 1", "warning");
		return false;
	}
	
	if (Coeficiente2 == ""){
		$("#Coeficiente2").focus();
		swal("Acción Cancelada", "Debe de ingresar el valor del coeficiente 2", "warning");
		return false;
	}
	
	if (Coeficiente3 == ""){
		$("#Coeficiente3").focus();
		swal("Acción Cancelada", "Debe de ingresar el valor del coeficiente 3", "warning");
		return false;
	}
	
	if (Coeficiente4 == ""){
		$("#Coeficiente4").focus();
		swal("Acción Cancelada", "Debe de ingresar el valor del coeficiente 4", "warning");
		return false;
	}
	
	if (Coeficiente5 == ""){
		$("#Coeficiente5").focus();
		swal("Acción Cancelada", "Debe de ingresar el valor del coeficiente 5", "warning");
		return false;
	}
		
		
    var parametros = "Patron=" + Patron + "&Coeficiente0=" + Coeficiente0 + "&Coeficiente1=" + Coeficiente1 + "&Coeficiente2=" + Coeficiente2 + 
					  "&Coeficiente3=" + Coeficiente3 + "&Coeficiente4=" + Coeficiente4 + "&Coeficiente5=" + Coeficiente5 + "&Plantilla=" + plantilla;
    var datos = LlamarAjax("Patron","opcion=GuardarCoeficientes&" + parametros).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function EliminarCoeficientes(){
	if (Patron == 0){
		swal("Acción Cancelada","Debe de seleccionar un patrón","warning");
		return false;
	}
	var plantilla = $("#Plantilla").val()*1;
	var parametros = "Patron=" + Patron + "&Plantilla=" + plantilla;
    var datos = LlamarAjax("Patron","opcion=EliminarCoeficientes&" + parametros).split("|");
    if (datos[0] == "0") {
		$("#Coeficiente0").val(data[0].a0);
		$("#Coeficiente1").val(data[0].a1);
		$("#Coeficiente2").val(data[0].a2);
		$("#Coeficiente3").val(data[0].a3);
		$("#Coeficiente4").val(data[0].a4);
		$("#Coeficiente5").val(data[0].a5); 
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function BuscarCoeficientes(){
	if (Patron == 0){
		return false;
	}
	var plantilla = $("#Plantilla").val()*1;
	var parametros = "Patron=" + Patron + "&Plantilla=" + plantilla;
    var datos = LlamarAjax("Patron","opcion=BuscarCoeficientes&" + parametros);
    if (datos != "[]"){ 
        var data = JSON.parse(datos);
        $("#Coeficiente0").val(data[0].a0);
		$("#Coeficiente1").val(data[0].a1);
		$("#Coeficiente2").val(data[0].a2);
		$("#Coeficiente3").val(data[0].a3);
		$("#Coeficiente4").val(data[0].a4);
		$("#Coeficiente5").val(data[0].a5); 
    }
}





$('select').select2();
DesactivarLoad();
