package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Pedido"})
public class Pedido extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String url_archivo = Globales.url_archivo;
    String archivoadjunto = null;
    String correoenvio;
    String clavecorreo;

    public String GuardarDetPedido(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int idpedido = Globales.Validarintnonull(request.getParameter("idpedido"));
        int idarticulo = Globales.Validarintnonull(request.getParameter("idarticulo"));
        int bodega = Globales.Validarintnonull(request.getParameter("bodega"));
        String articulo = Globales.neescape(request.getParameter("articulo"));
        int cantidad = Globales.Validarintnonull(request.getParameter("cantidad"));
        int cuentacontable = Globales.Validarintnonull(request.getParameter("cuentacontable"));

        String mensaje = "";
        try {
            if (Globales.usa_contabilidad.equals("SI")) {
                int cuenta = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from contabilidad.cuenta where cuenta=" + cuentacontable));
                if (cuenta == 0) {
                    return "1|El artículo ó servicio no posee una cuenta contable asignada";
                }
            }
            if (idpedido > 0){
                String estado = Globales.ObtenerUnValor("select estado from pedidos where id=" + idpedido);
                if (!estado.equals("Pendiente")) {
                    return "1|No se puede actualizar un pedido en estado " + estado;
                }
            }
            
            
            int tcantidad = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from pedidos_detalle where idarticulo = " + idarticulo + " and idusuario = " + idusuario + " and idpedido = " + idpedido + " and id <> " + id));
            if (tcantidad > 0) {
                return "1|Este artículo ya se encuentra registrado en el pedido";
            }

            if (id == 0) {

                sql = "INSERT INTO public.pedidos_detalle(idpedido, idarticulo, articulo, cantidad, idusuario) \n"
                        + " VALUES (" + idpedido + "," + idarticulo + ",'" + articulo + "'," + cantidad + "," + idusuario + ")";

                if (Globales.DatosAuditoria(sql, "PEDIDO-DETALLE", "AGREGAR", idusuario, iplocal, this.getClass() + "->GuardarDetPedido")) {
                    mensaje = "0|Detalle agregado con éxito|";
                } else {
                    return "1|Error inesperado.... Revise los archivos de log";
                }
            } else {

                sql = "UPDATE pedidos_detalle "
                        + "        SET cantidad = " + cantidad + ", idusuario = " + idusuario + ", fecha = now() "
                        + " WHERE id = " + id + ";";
                if (Globales.DatosAuditoria(sql, "PEDIDO-DETALLE", "EDITAR", idusuario, iplocal, this.getClass() + "->GuardarDetPedido")) {
                    mensaje = "0|Detalle actualizado con éxito|";
                } else {
                    return "1|Error inesperado.... Revise los archivos de log";
                }
            }

            return mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String TablaPedido(HttpServletRequest request) {

        HttpSession misession = request.getSession();
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String busqueda = "WHERE idpedido=" + id;
        if (id == 0) {
            busqueda += " and nomusu = '" + misession.getAttribute("codusu") + "'";
        }
        sql = "SELECT id, idpedido, idarticulo, articulo, formato_numerico(cantidad,3) as cantidad, idusuario, to_char(fecha,'yyyy/MM/dd HH24:MI') as fecha, u.nombrecompleto as usuario, \n"
                + "false as seleccionado, '' as class_select \n"
                + "FROM pedidos_detalle p inner join seguridad.rbac_usuario u on u.idusu = p.idusuario \n"
                + busqueda + " order by p.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaPedido");

    }

    public String GuardarFactura(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String orden = request.getParameter("orden");
        double total = Globales.ValidardoublenoNull(request.getParameter("total"));
        double subtotal = Globales.ValidardoublenoNull(request.getParameter("subtotal"));
        double descuento = Globales.ValidardoublenoNull(request.getParameter("descuento"));
        double gravable = Globales.ValidardoublenoNull(request.getParameter("gravable"));
        double exento = Globales.ValidardoublenoNull(request.getParameter("exento"));
        double iva = Globales.ValidardoublenoNull(request.getParameter("iva"));
        double reteiva = Globales.ValidardoublenoNull(request.getParameter("reteiva"));
        double retefuente = Globales.ValidardoublenoNull(request.getParameter("retefuente"));
        double reteica = Globales.ValidardoublenoNull(request.getParameter("reteica"));
        String observacion = request.getParameter("observacion");
        String formapago = request.getParameter("formapago");
        String fechafac = request.getParameter("fechafac");
        String a_ingreso = request.getParameter("a_ingreso");
        String a_orden = request.getParameter("a_orden");
        String a_certificado = request.getParameter("a_certificado");
        String a_cotizacion = request.getParameter("a_cotizacion");
        String a_precio = request.getParameter("a_precio");
        String a_cantidad = request.getParameter("a_cantidad");
        String a_descuento = request.getParameter("a_descuento");
        String a_iva = request.getParameter("a_iva");
        String a_subtotal = request.getParameter("a_subtotal");
        String a_descripcion = Globales.neescape(request.getParameter("a_descripcion"));
        String a_centro = request.getParameter("a_centro");
        String a_cuenta = request.getParameter("a_cuenta");
        String a_servicio = request.getParameter("a_servicio");
        String a_articulos = request.getParameter("a_articulos");
        String a_bodegas = request.getParameter("a_bodegas");
        double porreteiva = Globales.ValidardoublenoNull(request.getParameter("porreteiva"));
        double porretefuente = Globales.ValidardoublenoNull(request.getParameter("porretefuente"));
        double porreteica = Globales.ValidardoublenoNull(request.getParameter("porreteica"));
        String a_ccuenta = request.getParameter("a_ccuenta");
        String a_cdebito = request.getParameter("a_cdebito");
        String a_ccredito = request.getParameter("a_ccredito");
        String a_cconcepto = Globales.neescape(request.getParameter("a_cconcepto"));
        String a_ccentro = request.getParameter("a_ccentro");
        double tdebito = Globales.ValidardoublenoNull(request.getParameter("tdebito"));
        double tcredito = Globales.ValidardoublenoNull(request.getParameter("tcredito"));
        try {
            sql = " SELECT * FROM guardar_factura(" + id + ",'" + fechafac + "'," + cliente + ",'" + orden + "'," + total + "," + subtotal + "," + descuento + "," + gravable
                    + "," + exento + "," + iva + "," + reteiva + "," + retefuente + "," + reteica + "," + porreteiva + "," + porretefuente + "," + porreteica + ",'" + observacion
                    + "'," + a_ingreso + "," + a_descripcion + "," + a_orden + "," + a_certificado + "," + a_cotizacion + "," + a_precio + "," + a_cantidad + "," + a_descuento
                    + "," + a_iva + "," + a_subtotal + "," + a_centro + "," + a_cuenta + "," + a_servicio + "," + a_articulos + "," + a_bodegas + "," + idusuario + ","
                    + a_ccuenta + "," + a_cdebito + "," + a_ccredito + "," + a_cconcepto + "," + a_ccentro + "," + tdebito + "," + tcredito + ")";

            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarFactura", 1);
            datos.next();
            if (datos.getString("_factura").equals("0")) {
                return "1|" + datos.getString("_mensaje");
            } else {
                return "0|Factura guardada y contabilizada con el número|" + datos.getString("_factura") + "|" + datos.getString("_idfactura");
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }
    }

    public String EnvioFactura(HttpServletRequest request) {
        try {
            String principal = request.getParameter("principal");
            int factura = Globales.Validarintnonull(request.getParameter("factura"));
            String e_email = request.getParameter("e_email");
            String archivo = request.getParameter("archivo");
            String observacion = request.getParameter("observacion");
            int error = 0;

            sql = "SELECT clave, c.nombrecompleto as cliente, u.nombrecompleto as usuario, u.cargo "
                    + "  from clientes c inner join factura fa on fa.idcliente = c.id "
                    + "                  INNER JOIN seguridad.rbac_usuario u on u.idusu = fa.idusuario "
                    + "  where factura = " + factura;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->EnvioFactura", 1);
            datos.next();
            String ClaveAcceso = datos.getString("clave");
            String cliente = datos.getString("cliente");
            String nomusuario = datos.getString("usuario");
            String cargo = datos.getString("cargo");

            e_email += ";" + correoenvio;
            String[] email = e_email.split(";");

            String correoemp = correoenvio;

            DateFormat formato = new SimpleDateFormat("HH");

            int hora = Globales.Validarintnonull(formato.format(new Date()));
            String saludo = "";
            if (hora <= 11) {
                saludo = "Buenos días";
            } else if (hora <= 18) {
                saludo = "Buenas tardes";
            } else {
                saludo = "Buenas noches";
            }

            String nombrearchivo = "FCS-" + factura + ".pdf";

            if ((correoemp.trim().equals("")) || (clavecorreo.trim().equals(""))) {
                return "1|Debe configurar los parámetros: Correo envio, Clave correo envio";
            }

            String mensaje = saludo + "<br><br><b>SEÑORES</b><br>" + cliente + "<br><br>"
                    + "Reciba un cordial saludo por parte de nuestro equipo de trabajo.<br><br>"
                    + "Se adjunta factura número <b>" + factura + "</b> para su pago oportuno. "
                    + "<br><br>Cordialmente.<br><br>"
                    + "<img src='http://190.144.188.100:82/imagenes/logo.png' width='30%'><br><b>" + nomusuario + "</b><br>Calibration Service SAS<br>" + cargo + "<br><font color='blue'>+57 (1) 2047699 - 7285146 <br>3138141058</font><br><br>";

            if (!observacion.equals("")) {
                mensaje += "<b>OBSERVACIÓN DE ENVÍO:</b><br>" + observacion;
            }

            /*mensaje += "<br><br>Debe de ingresar a su cuenta de <b>CALIBRATION SERVICE SAS</b> para aprobar o no la cotización a través del link <br><br" +
            "<a href='http://190.144.188.100:8080/usuarios/index.php'>http://190.144.188.100:8080/usuarios/index.php</a><br><br>" +
            "Sus datos para ingresar son los siguientes: <br><b>usuario</b> " + principal +
            "<br><b>Clave de acceso</b> " + ClaveAcceso;*/
            String adjunto = Globales.ruta_archivo + "DocumPDF/" + archivo;

            String correo = Globales.EnviarEmail(principal, Globales.Nombre_Empresa, email, "Envío de Factura Número " + factura, mensaje, correoemp, adjunto, correoemp, clavecorreo, nombrearchivo);

            if (!correo.trim().equals("")) {
                return "1|" + correo;
            }

            sql = " UPDATE factura set estado = 'Enviado', fechaenvio = now(),  idusuarioenvia = " + idusuario + ", correoenvia = '" + principal + " " + e_email + "' WHERE factura = " + factura + " and estado = 'Facturado';";

            Globales.DatosAuditoria(sql, "FACTURA", "ENVIAR CORREO", idusuario, iplocal, this.getClass() + "-->EnvioFactura");
            return error + "|Correo enviado a|" + e_email;
        } catch (SQLException ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String ConsultarFactura(HttpServletRequest request) {
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        String orden = request.getParameter("orden");
        int factura = Globales.Validarintnonull(request.getParameter("factura"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = request.getParameter("equipo");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        String intervalo = request.getParameter("intervalo");
        String serie = request.getParameter("serie");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String estado = request.getParameter("estado");

        String busqueda = "WHERE 1=1 ";
        if (factura > 0) {
            busqueda += " AND f.factura = " + factura;
        } else if (!orden.equals("")) {
            busqueda += " AND f.orden = '" + orden + "'";
        } else if (cotizacion > 0) {
            busqueda += " AND fd.cotizacion = '" + cotizacion + "'";
        } else if (ingreso > 0) {
            busqueda += " AND rd.ingreso = " + ingreso;
        } else {
            busqueda += " AND to_char(f.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(f.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
            if (cliente > 0) {
                busqueda += " AND f.idcliente = " + cliente;
            }
            if (magnitud > 0) {
                busqueda += " AND e.idmagnitud = " + magnitud;
            }
            if (!equipo.equals("")) {
                busqueda += " AND e.descripcion = '" + equipo + "'";
            }
            if (marca > 0) {
                busqueda += " AND mo.idmarca = " + marca;
            }
            if (!modelo.equals("")) {
                busqueda += " AND mo.descripcion = '" + modelo + "'";
            }
            if (!intervalo.equals("")) {
                busqueda += " AND '(' || desde || ' a ' || hasta || ') ' || medida = '" + intervalo + "'";
            }
            if (!serie.trim().equals("")) {
                busqueda += " AND rd.serie = '" + serie.trim() + "'";
            }
            if (usuarios > 0) {
                busqueda += " AND f.idusuario = " + usuarios;
            }
            if (!estado.equals("Todos")) {
                busqueda += " AND f.estado = '" + estado + "'";
            }
        }

        sql = "SELECT row_number() OVER(order by f.factura) as fila, "
                + " cli.nombrecompleto || ' (' || cli.documento || ')' AS cliente, factura, f.subtotal, f.descuento, f.iva, f.total,"
                + "   ci.descripcion  as ciudad,"
                + "   f.orden, cli.Telefono || '<br>' || cli.direccion as direccion, cli.email,"
                + "   to_char(f.fecha,'yyyy/MM/dd HH24:MI') as fecha,"
                + "   tiempo(f.vencimiento, now(), 5) as tiempo,"
                + "   to_char(f.vencimiento,'yyyy/MM/dd') as vencimiento,"
                + "   to_char(f.fecharad,'yyyy/MM/dd') as fecharad,"
                + "   to_char(f.fechaenvio,'yyyy/MM/dd HH24:MI')  || '<br>' || ue.nombrecompleto as envio,"
                + "   to_char(f.fechaanula,'yyyy/MM/dd HH24:MI')  || '<br>' || ua.nombrecompleto as anula,"
                + "   u.nombrecompleto AS usuario, tc.descripcion as tipo, diaspago, saldo, f.estado,"
                + "   (select to_char(fr.fechareg,'yyyy/MM/dd HH24:MI')  || '<br>' || urr.nombrecompleto "
                + "       from factura_radicacion fr inner join seguridad.rbac_usuario urr on urr.idusu = fr.idusuario and fr.factura = f.factura) as registrorad,"
                + "   (select to_char(fr.fecharec,'yyyy/MM/dd HH24:MI')  || '<br>' || urc.nombrecompleto "
                + "       from factura_radicacion fr inner join seguridad.rbac_usuario urc on urc.idusu = fr.idusuariorec and fr.factura = f.factura) as recibidorad,"
                + " '<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Factura'' type=''button'' onclick=' || chr(34) || 'ImprimirFactura(' || f.factura || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as imprimir"
                + "   FROM factura f inner join factura_datelle fd on f.id = fd.idfactura"
                + "		   inner JOIN remision_detalle rd ON rd.ingreso = fd.ingreso"
                + "		   inner JOIN clientes cli on cli.id = f.idcliente"
                + "		   inner JOIN equipo e ON e.id = rd.idequipo"
                + "		   inner JOIN modelos mo ON mo.id = rd.idmodelo"
                + "		   inner JOIN ciudad ci ON ci.id = cli.idciudad"
                + "  inner join magnitud_intervalos i on i.id = rd.idintervalo  "
                + "		   inner JOIN seguridad.rbac_usuario u on u.idusu = f.idusuario "
                + "		   inner JOIN seguridad.rbac_usuario ua on ua.idusu = f.idusuarioanula "
                + "		   inner JOIN seguridad.rbac_usuario ue on ue.idusu = f.idusuarioenvia"
                + "   inner join tipocliente tc on tc.id = f.tipopago::integer " + busqueda
                + "   group by cli.nombrecompleto, cli.documento, "
                + "           cli.telefono, ci.descripcion,f.factura, f.subtotal, f.descuento, f.iva, f.total, f.orden,"
                + "           cli.direccion, cli.Telefono, cli.email, f.fecha, f.vencimiento, f.fechaenvio, f.fechaanula,"
                + "           ue.nombrecompleto, ua.nombrecompleto, u.nombrecompleto, f.fecharad, tc.descripcion, diaspago, saldo, f.estado";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarFactura");
    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String ruta_origen = request.getHeader("Origin");

        HttpSession misession = request.getSession();
        idusuario = misession.getAttribute("idusuario").toString();
        iplocal = misession.getAttribute("IPLocal").toString();
        usuario = misession.getAttribute("codusu").toString();
        correoenvio = misession.getAttribute("correoenvio").toString();
        clavecorreo = misession.getAttribute("clavecorreo").toString();
        if (misession.getAttribute("ArchivosAdjuntos") != null) {
            archivoadjunto = misession.getAttribute("ArchivosAdjuntos").toString();
        }
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");

        try (PrintWriter out = response.getWriter()) {
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else if (misession.getAttribute("codusu") == null) {
                out.print("cerrada");
            } else if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                out.print("cambio-ip");
            } else {
                String opcion = request.getParameter("opcion");
                switch (opcion) {
                    case "GuardarDetPedido":
                        out.print(GuardarDetPedido(request));
                        break;
                    case "TablaPedido":
                        out.print(TablaPedido(request));
                        break;
                    default:
                        throw new AssertionError();
                }
            }
        } catch (Exception ex) {
            Globales.GuardarLogger(ex.getMessage(), "");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

}
