angular.module('myApp', ['ui.bootstrap']).controller('ControlChat', function ($scope, $ocLazyLoad, $rootScope, $timeout, General, Socker) {

    $rootScope.CombosChat = {
        usuario: ""
    }


    $rootScope.InicializarFormulario(0);

    $rootScope.CargarComboChat = function () {
        var parametros = "opcion=UsuariosChat&Usuario=" + $rootScope.InicioSesion.idusuario + "&json=1";
        General.LlamarAjax("Seguridad", parametros, false, "", 1).then(function (response) {
            $rootScope.CombosChat.usuario = response;
        });
    }



    $timeout(function () {
        $("#respuestachat").animate({scrollTop: $('#respuestachat')[0].scrollHeight}, 1000);
    }, 1000);


    $rootScope.$watch("InicioSesion.idusuario", function (newValue, oldValue) {
        if (oldValue != 0 || newValue != 0) {
            $rootScope.CargarComboChat();
        }
    });

    $scope.EnviarMensaje = function (mensaje, usuarios) {
        var usuariochat = "";
        mensaje = $.trim(General.escape(mensaje));

        if (mensaje == "")
            return false;

        for (var i = 0; i < usuarios.length; i++) {
            if (usuarios[i] == "0") {
                usuariochat = "0";
                break;
            }
            usuariochat += "/" + usuarios[i];
        }
        if ($.trim(usuariochat) == "" || $.trim(usuariochat) == "0") {
            usuariochat = "0";
        } else {
            usuariochat += "/";
        }
        Socker.sendMessage(1, mensaje, usuariochat);
    }

    $rootScope.Chat.vermensaje = "SI";
    $("#Usuarios_Chat").select2('destroy');
});

