﻿
$("#Dep_Responsables").html(CargarCombo(13, 4));

function TablaDeposito() {
    var datos = LlamarAjax('Inventarios','opcion=TablaConfiguracion&tipo=7&id=0');
    var datasedjson = JSON.parse(datos);
    $('#TablaDeposito').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "descripcion" },
            { "data": "des_responsables" },
            { "data": "desestado" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        },
		dom: 'Bfrtip',
		buttons: [
			'excel', 'csv', 'copy','colvis'
		]
    });
}

function LimpiarDeposito() {
    $("#Dep_Id").val("0");
    $("#Dep_Ubicacion").val("");
    $("#Dep_Responsables").val("").trigger("change");
    $("#Dep_Estado").val("1").trigger("change");
}

$("#TablaDeposito > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Inventarios','opcion=TablaConfiguracion&tipo=7&id=' + numero);
    var data = JSON.parse(datos);
    $("#Dep_Id").val(data[0].id);
    $("#Dep_Ubicacion").val(data[0].descripcion);

    var responsables = data[0].responsables.split(",");
    for (var x = 0; x < responsables.length; x++) {
        $("#Dep_Responsables option[value=" + responsables[x] + "]").prop("selected", true);
    }
    $("#Dep_Responsables").trigger("change");
    $("#Dep_Estado").val(data[0].estado).trigger("change");
    $("#Dep_Ubicacion").focus();
});


$("#formDeposito").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Inventarios","opcion=GuardarDeposito&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaDeposito();
        LimpiarDeposito();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function EliminarDeposito(id, deposito) {
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar la ubicación del depostio ' + deposito + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Inventarios","opcion=EliminarDeposito&Id=" + id + "&IdReemplazo=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve();
                        else {
                            reject(datos[1]);
                        }

                    })
            })
        }
    }]).then(function (data) {
        TablaDeposito();
        LimpiarDeposito();
    });
}

TablaDeposito();

$('select').select2();
DesactivarLoad();
