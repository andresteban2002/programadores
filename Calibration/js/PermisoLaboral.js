﻿var IdEmpleado = localStorage.getItem('EmpleadoSeleccionado') * 1;
$("#IdEmpleado").val(IdEmpleado);

if (IdEmpleado > 0) {
    $(".eliminapermiso").remove();
    BuscarEmpleado(IdEmpleado);
}

function LimpiarDatos() {

}

$("#tablamodalempleados_permiso > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var id = row[8].innerText;
    BuscarEmpleado(id);
    $("#modalEmpleados").modal("hide");
});

function BuscarEmpleado(empleado) {

    var datos = LlamarAjax("Recursohumano","opcion=BuscarEmpleados&Empleado=" + empleado).split("|");
    var data = JSON.parse(datos[0]);
    IdEmpleado = empleado;
    $("#IdEmpleado").val(empleado);
    $("#Documento").val(data[0].documento);
    $("#Nombre").val(data[0].nombrecompleto);
    $("#Cargo").val(data[0].cargo);
    $("#Telefono").val(data[0].telefono);
    $("#Celular").val(data[0].celular);
    $("#Correo").val(data[0].email);
    $("#Direccion").val(data[0].direccion);
}

$("#formPermisolaboral").submit(function (e) {
    e.preventDefault();
    var fechad = $.trim($("#FechaD").val() + " " + $("#HoraD").val());
	var fechah = $.trim($("#FechaH").val() + " " + $("#HoraH").val());
	if (fechad >= fechah){
		$("#FechaD").focus();
		swal("Acción Cancelada","La fecha final no puede mayor a la fecha inicial","warning");
		return false;
	}
	
	var anexos = $("#TablaAnexos").html();
	if (anexos == ""){
		$("#NombreArchivo").focus();
		swal("Acción Cancelada","Debe de examinar por lo mínimo una imagen adjunta","warning");
		return false;
	}
			
    var parametros = $("#formPermisolaboral").serialize();
    var datos = LlamarAjax("Recursohumano","opcion=GuardarPermisoLaboral&" + parametros).split("|");
    if (datos[0] == "0")
        swal("", datos[1], "success");
    else
        swal("Acción Cancelada", datos[1], "warning");
});

function CalcularTiempo(){
	var fechad = $.trim($("#FechaD").val() + " " + $("#HoraD").val());
	var fechah = $.trim($("#FechaH").val() + " " + $("#HoraH").val());
	var tiempo = "";
	if (fechad != "" && fechah != ""){
		var fecha1 = moment(fechad);
		var fecha2 = moment(fechah);
		var calculo = fecha2.diff(fecha1, 'hours');
		var dias = Math.trunc(calculo/24);
		var horas = calculo % 24;
		if (dias > 0)
			tiempo = dias + (dias == 1 ? " dia" : " dias");
		if (horas > 0)
			tiempo += " " + horas + (horas == 1 ? " hora" : " horas");
		tiempo = $.trim(tiempo);

	}
	$("#Calculo").val(tiempo);
}

function ActivarRecuperar(objeto){
	if (objeto.checked)
		$("#FormaRecuperar").prop("readonly",false);
	else
		$("#FormaRecuperar").prop("readonly",true);
}

function GuardarDocumentoAnexo(id){
	if (IdEmpleado == 0){
		$("#DocumentoArchivo").val("");
		swal("Acción Cancelada","Debe de seleccionar un empleado","warning");
		return false;
	}
	var anexos = GuardarDocumentoAdjunto(id,4,"", "");
	if (anexos == ""){
		swal("Acción Cancelada", "No se ha seleccionado un archivo válido","warning");
		return false;
	}
	var resultado = "";
	var arreglo = anexos.split("<br/>");
	for (var x = 0; x<arreglo.length; x++){
		resultado += "<tr><td>" + (x+1) + "</td>" + 
				"<td><a href=\"javascript:window.open('" + url_cliente + "uploads/" + arreglo[x] + "')\">" +  arreglo[x] + "</a></td></tr>";
	}
	$("#TablaAnexos").html(resultado);
}
	


$('select').select2();
DesactivarLoad();
