angular.module('myApp', ['ui', 'ngSanitize', 'ngTable']).controller('ControlCotizaciones', function ($scope, $ocLazyLoad, $rootScope, General, NgTableParams) {


    function initTabs() {
        tabClasses = ["", "", "", "", "", "", ""];
    }



    $scope.getTabClass = function (tabNum) {
        return tabClasses[tabNum];
    };

    $scope.getTabPaneClass = function (tabNum) {
        return "tab-pane " + tabClasses[tabNum];
    }

    $scope.setActiveTab = function (tabNum) {
        initTabs();
        tabClasses[tabNum] = "active";
    };

    function initTabsPre() {
        tabClassespre = ["", ""];
    }

    $scope.getTabPreClass = function (tabNum) {
        return tabClassespre[tabNum];
    };

    $scope.getTabPanePreClass = function (tabNum) {
        return "tab-pane-pre " + tabClassespre[tabNum];
    }

    $scope.setActiveTabPre = function (tabNum) {
        initTabsPre();
        tabClassespre[tabNum] = "active";
    };

    function initTabsSer() {
        tabClassesser = ["", ""];
    }

    $scope.getTabSerClass = function (tabNum) {
        return tabClassesser[tabNum];
    };

    $scope.getTabPaneSerClass = function (tabNum) {
        return "pane-ser " + tabClassesser[tabNum];
    }

    $scope.setActiveSerTab = function (tabNum) {
        initTabsSer();
        tabClassesser[tabNum] = "active";
        $scope.TipoServicio = tabNum;
    };

    $scope.Documento = "";
    $scope.Cotizacion = "";
    $scope.Revision = "1";
    $scope.Remision = "";
    $scope.ErrorBusqueda = 0;
    $scope.OpcionEquipo = 0;
    $scope.TipoServicio = 0;
    $scope.OpcionMagnitud = 0;
    $scope.TituloPrecio = {};
    $scope.VisibleServicio = [true, true];

    $scope.CombosCotizacion = ($rootScope._TIPOCOTIZACION == 1 ? false : true);

    General.DatosEmpresa();

    $scope.$watch("_TIPOCOTIZACION", function (newValue, oldValue) {
        if (oldValue != newValue) {
            $scope.CombosCotizacion = ($rootScope._TIPOCOTIZACION == 1 ? false : true);
        }
    });


    $scope.Cliente = {
        id: 0,
        documento: ""
    }

    $scope.Combos = {
        sede: "",
        contacto: "",
        magnitud: "",
        bequipo: "",
        equipo: "",
        equipo_pesa: "",
        marca: "",
        marca_pesa: "",
        servicio: "",
        servicio_pesa: "",
        medida: "",
        cliente: "",
        modelo: "",
        modelo_pesa: "",
        bmodelo: "",
        bintervalo: "",
        intervalo: "",
        otroservicio: "",
        bmetodo: "",
        metodo: "",
        metodo_pesa: "",
        pais: "",
        departamento: "",
        ciudad: "",
        usuario: "",
        bproveedor: "",
        proveedor: "",
        proveedor_pesa: "",
        moneda: "",
        lineanegocio: "",
        empresa: "",
        grupo: "",
        validez: "",
        clase: "",
        bclase: "",
        condicion_entrega: "",
        plazo_entrega: "",
        forma_pago: "",
        tiposertificado: [{id: "DIGITAL", value: "DIGITAL"}, {id: "IMPRESO", value: "IMPRESO"}]

    }

    $scope.CargarCombos = function () {
        $rootScope.global.CargandoPrinicipal = true;
        var parametros = "opcion=CargaComboInicial&tipo=2&json=1";
        General.LlamarAjax("Configuracion", parametros, true, "", 1).then(function (response) {
            var datos = $.trim(response).split("||");
            $scope.Combos.magnitud = JSON.parse(datos[0]);
            $scope.Combos.empresa = JSON.parse(datos[29]);
            $scope.Combos.bequipo = JSON.parse(datos[1]);
            $scope.Combos.equipo = JSON.parse(datos[31]);
            $scope.Combos.equipo_pesa = JSON.parse(datos[36]);
            $scope.Combos.marca = JSON.parse(datos[2]);
            $scope.Combos.marca_pesa = JSON.parse(datos[2]);
            $scope.Combos.servicio = JSON.parse(datos[3]);
            $scope.Combos.servicio_pesa = JSON.parse(datos[3]);
            $scope.Combos.medida = JSON.parse(datos[4]);
            $scope.Combos.cliente = JSON.parse(datos[5]);
            $scope.Combos.bmodelo = JSON.parse(datos[6]);
            $scope.Combos.bintervalo = JSON.parse(datos[7]);
            $scope.Combos.otroservicio = JSON.parse(datos[8]);
            $scope.Combos.bmetodo = JSON.parse(datos[9]);
            $scope.Combos.pais = JSON.parse(datos[10]);
            $scope.Combos.departamento = JSON.parse(datos[11]);
            $scope.Combos.ciudad = JSON.parse(datos[12]);
            $scope.Combos.usuario = JSON.parse(datos[13]);
            $scope.Combos.bproveedor = JSON.parse(datos[17]);
            $scope.Combos.moneda = JSON.parse(datos[27]);
            $scope.Combos.lineanegocio = JSON.parse(datos[28]);
            $scope.Combos.grupo = JSON.parse(datos[30]);
            $scope.Combos.validez = JSON.parse(datos[32]);
            $scope.Combos.condicion_entrega = JSON.parse(datos[33]);
            $scope.Combos.plazo_entrega = JSON.parse(datos[34]);
            $scope.Combos.bclase = JSON.parse(datos[35]);
            $rootScope.global.CargandoPrinicipal = false;
        });
    }

    $scope.RecargarCargarCombos = function (opcion, tipo) {
        switch (opcion) {
            case 1:
                switch (tipo) {
                    case 1:
                        $scope.CargarIntervalos($scope.Servicio.magnitud, "", "");
                        $scope.Combos.metodo = "";
                        $scope.Servicio.metodo = "";
                        break;
                    case 2:
                        General.CargarCombo(111, null).then(function (response) {
                            $scope.Combos.equipo_pesa = response.data;
                            $scope.Combos.metodo_pesa = "";
                            $scope.Pesa.metodo = "";
                        });
                        break;
                }
                break;
            case 2:
                General.CargarCombo(3, null).then(function (response) {
                    switch (tipo) {
                        case 1:
                            $scope.Combos.marca = response.data;
                            break
                        case 2:
                            $scope.Combos.marca_pesa = response.data;
                            break
                    }
                });
                break;
            case 3:
                var marca = (tipo == 1 ? $scope.Servicio.marca : $scope.Pesa.marca);
                if (marca != "") {
                    General.CargarCombo(5, marca).then(function (response) {
                        switch (tipo) {
                            case 1:
                                $scope.Combos.modelo = response.data;
                                break;
                            case 2:
                                $scope.Combos.modelo_pesa = response.data;
                                break;
                        }
                    });
                }
                break;
            case 4:
                if ($scope.Servicio.magnitud != "") {
                    General.CargarCombo(6, $scope.Servicio.magnitud).then(function (response) {
                        $scope.Combos.intervalo = response.data;
                    });
                }
                break;
            case 5:
                var equipo = (tipo == 1 ? $scope.Servicio.equipo : $scope.Pesa.equipo);
                if (equipo != "") {
                    General.CargarCombo(17, equipo).then(function (response) {
                        switch (tipo) {
                            case 1:
                                $scope.Combos.metodo = response.data;
                                break;
                            case 2:
                                $scope.Combos.metodo_pesa = response.data;
                                break;
                        }
                    });
                }
                break;
            case 6:
                General.CargarCombo(4, null).then(function (response) {
                    switch (tipo) {
                        case 1:
                            $scope.Combos.servicio = response.data;
                            break;
                        case 2:
                            $scope.Combos.servicio_pesa = response.data;
                            break;
                    }
                });
                break;
        }
    }

    $scope.InicializarRegistro = function () {
        $scope.Registro = {
            id: 0,
            cotizacion: 0,
            cliente: 0,
            empresa: "1",
            sede: "",
            solicitud: 0,
            remision: "",
            idremision: 0,
            revision: 0,
            contacto: "",
            estado: "Temporal",
            observaciones: "",
            tabla: {},
            subtotal: 0,
            descuento: 0,
            gravable: 0,
            exento: 0,
            iva: 0,
            retefuente: 0,
            reteiva: 0,
            reteica: 0,
            total: 0,
            subtotalusd: 0,
            descuentousd: 0,
            gravableusd: 0,
            exentousd: 0,
            ivausd: 0,
            retefuenteusd: 0,
            reteivausd: 0,
            reteicausd: 0,
            totalusd: 0,
            moneda: "",
            lineanegocio: "",
            tcantidad: 0,
            buscarprecio: 0,
            Pdf_Cotizacion: "",
            validez: "30 días calendarios desde su emisión.",
            condicion_entrega: "",
            forma_pago: "",
            plazo_entrega: "",
            licitacion: ""


        }
    }


    $scope.InicializarServicio = function () {
        $scope.Servicio = {
            id: 0,
            idsolicitud: 0,
            lote: 0,
            articulo: 0,
            magnitud: "",
            magnitud_pesa: "",
            equipo: "",
            equipo_pesa: "",
            clase: "",
            marca: "",
            marca_pesa: "",
            modelo: "",
            modelo_pesa: "",
            servicio: "",
            servicio_pesa: "",
            otroservicio: "",
            observacion: "",
            observacion_pesa: "",
            serie: "",
            ident_equipo: "",
            ingreso: 0,
            cantidad: 1,
            serie_pesa: "",
            ident_equipo: "",
            ingreso: 0,
            cantidad: 1,
            intervalo: "0",
            intervalo2: "",
            metodo: "",
            metodo_pesa: "",
            costo: 0,
            proveedor: "",
            proveedor_pesa: "",
            precio: 0,
            descuento: 0,
            subtotal: 0,
            entrega: 0,
            calibracionadic: "",
            precioadic: 0,
            iva: 0,
            express: 0,
            sitio: 0,
            direccion: "SEDE",
            punto: "",
            declaracion: "",
            calibracion: "",
            certificado: "",
            certificado_pesa: "",
            nombreca: "",
            preciouni: "",
            pagocertificado: 'NO',
            moneda: "",
            moneda_pesa: "",
            tabla_clase: {},
            dis_punto: false,
            dis_declaracion: false,
            dis_nombreca: false,
            dis_certificado: false,
            dis_direccion: false,
            dis_calibracion: false,
            dis_metodo: false,
            dis_proveedor: true,
            dis_ingreso: false,
            dis_precioadicional: false,

            des_marca: "",
            des_modelo: "",
            des_equipo: "",
            des_servicio: "",
            des_proveedor: "",
            des_magnitud: "",
            idmedida: 0,
            medida: "",
            desde: "",
            hasta: "",

            dis_desde: true,
            dis_hasta: true,
            dis_medida: true,

            idprecio: 0,
            tipoprecio: 0,
            tipopreciomoneda: 1,
            tablaprecios: {
                MPrecio1: "",
                MPrecio2: "0",
                MPrecio3: "0",
                MPrecio4: "0",
                MPrecio5: "0",
                MPrecio6: "0",
                MPrecio7: "0",
                MPrecio8: "0",
                MPrecio9: "0",
                MPrecio10: "0",
                MCosto: "0",
                PrecioUSD1: "",
                PrecioUSD2: "0",
                PrecioUSD3: "0",
                PrecioUSD4: "0",
                PrecioUSD5: "0",
                PrecioUSD6: "0",
                PrecioUSD7: "0",
                PrecioUSD8: "0",
                PrecioUSD9: "0",
                PrecioUSD10: "0",
                PrecioUSD11: "0",
                MCostoUSD: "0",
                dis_MCosto: false,
                posicion: -1,
            }
        }

        $scope.Combos.modelo = "";
        $scope.Combos.intervalo = "";

        $scope.Servicio.nombreca = $scope.Cliente.nombrecompleto;

    }


    $scope.InicializarPesa = function () {
        $scope.Pesa = {
            id: 0,
            idsolicitud: 0,
            lote: 0,
            articulo: 0,
            magnitud: "",
            equipo: "",
            clase: "",
            marca: "",
            modelo: "",
            servicio: "",
            observacion: "",
            serie: "",
            ident_equipo: "",
            ingreso: 0,
            cantidad: 1,
            metodo: "",
            costo: 0,
            precio: 0,
            proveedor: "",
            descuento: 0,
            subtotal: 0,
            entrega: 0,
            iva: 0,
            express: 0,
            sitio: 0,
            direccion: "SEDE",
            declaracion: "",
            calibracion: "",
            certificado: "",
            nombreca: "",
            pagocertificado: 'NO',
            moneda: "",
            tabla_clase: {},
            dis_declaracion: false,
            dis_nombreca: false,
            dis_certificado: false,
            dis_direccion: false,
            dis_calibracion: false,
            dis_metodo: false,
            dis_proveedor: true,
            dis_ingreso: false,
            dis_ingreso: false

        }

        $scope.Combos.modelo_pesa = "";

        $scope.Pesa.nombreca = $scope.Cliente.nombrecompleto;

    }

    $scope.Metodo = {
        equipo: "",
        metodo: ""
    }

    $scope.Intervalo = {
        medida: "",
        desde: "",
        hasta: ""
    }

    $scope.Consulta = {
        remision: "",
        cotizacion: "",
        cliente: "",
        ingreso: "",
        fechad: {
            value: new Date(output + " 00:00"),
            id: output
        },
        fechah: {
            value: new Date(output2 + " 00:00"),
            id: output2
        },
        magnitud: "",
        equipo: "",
        marca: "",
        modelo: "",
        intervalo: "",
        serie: "",
        pais: "",
        departamento: "",
        ciudad: "",
        usuario: "",
        metodo: "",
        servicio: "",
        solicitud: "",
        estado: "Todos",
        cingreso: "T"
    }

    $scope.ConsultarIngreso = {
        remision: "",
        cliente: "",
        ingreso: "",
        fechad: {
            value: new Date(output + " 00:00"),
            id: output
        },
        fechah: {
            value: new Date(output2 + " 00:00"),
            id: output2
        },
        magnitud: "",
        equipo: "",
        marca: "",
        modelo: "",
        intervalo: "",
        serie: "",
        usuario: "",
        recepcion: ""
    }

    $scope.IngresoReportado = {
        estado: "",
        cliente: "",
        ingreso: "",
        fechad: {
            value: new Date(output + " 00:00"),
            id: output
        },
        fechah: {
            value: new Date(output2 + " 00:00"),
            id: output2
        },
        magnitud: "",
        equipo: "",
        marca: "",
        modelo: "",
        intervalo: "",
        serie: "",
        usuario: "",
        cotizado: "",
        asesor: "",
        enviado: "",
        noaprobado: ""
    }

    $scope.ClienteAsesor = {
        cliente: "",
        asesor: "",
        magnitud: "",
        fechad: {
            value: new Date(output + " 00:00"),
            id: output
        },
        fechah: {
            value: new Date(output2 + " 00:00"),
            id: output2
        }
    }

    $rootScope.ObtenerDatatable = function (data, tipo) {
        switch (tipo) {
            case 1:
                var descripcion = "CODIGO: " + data.codigointerno + ", TIPO: " + data.tipo + ", GRUPO: " + data.grupo +
                        ", ARTICULO: " + data.equipo + ", MARCA: " + data.marca + ", MODELO: " + data.modelo + ", PRESENTACION: " + data.presentacion + ", CARACTERISTICA: " + data.caracteristica;
                var idmoneda = $rootScope.Modal.moneda * 1;
                if (idmoneda == 0) {
                    swal("Acción Cancelada", "Debe de seleccionar el tipo de moneda", "warning");
                    return false;
                }

                General.LlamarAjax("Configuracion", "opcion=TablaPrecio_Moneda&moneda=" + idmoneda, false, "", 0).then(function (response) {
                    var moneda = response * 1;
                    var parametro = "opcion=GuardarTmpCotizacion&cliente=" + $scope.Registro.cliente + "&id=0&equipo=0&modelo=0" +
                            "&observacion=" + General.escape(descripcion) + "&serie=&ingreso=&cantidad=1&intervalo=0&intervalo2=0" + "&moneda=" + idmoneda +
                            "&servicio=17&metodo=0&punto=&calibracion=&declaracion=&nombreca=&articulo=" + data.id +
                            "&direccion=&certificado=&descuento=" + $scope.Cliente.descuento + "&precio=" + (moneda == 1 ? data.precio : data.preciousd) + "&entrega=0&proveedor=0&iva=" + ($scope.Cliente.valor > 0 ? data.iva : 0) +
                            "&express=0&sitio=0&cotizacion=" + $scope.Registro.id + "&remision=" + $scope.Registro.remision + "&sede=" + $scope.Registro.sede + "&contacto=" + $scope.Registro.contacto + "&idsolicitud=0&lote=0&costo=" + (moneda == 1 ? data.ultimocosto : data.ultimocostousd) + "&calibracionadic=&precioadic=0";
                    General.LlamarAjax("Cotizacion", parametro, false, "", 1).then(function (response) {
                        var datos = $.trim(response).split("|");
                        if (datos[0] == "0") {
                            $("#modalInventarioCot").modal("hide");
                            $scope.Registro.estado = "Temporal";
                            swal("", datos[1], "success");
                            $scope.TablaCotizacion(1);
                        } else
                            swal("Acción Cancelada", datos[1], "warning");
                    });
                });
                break;

            case 2:
                $scope.Documento = $.trim(data.documento);
                $scope.BuscarClienteCot(1);
                $rootScope.Modal.Close();
                break;
            case 3:
                var parametros = "opcion=AgregarIngreso&ingreso=" + data.ingreso + "&cliente=" + $scope.Registro.cliente + "&sede=" + $scope.Registro.sede + "&contacto=" + $scope.Registro.contacto + "&idcotizacion=" + $scope.Registro.id + "&remision=" + data.remision;
                General.LlamarAjax("Cotizacion", parametros, false, "", 1).then(function (response) {
                    var datos = $.trim(response).split("|");
                    if (datos[0] == "0") {
                        $scope.TablaCotizacion(1);
                        $rootScope.Modal.CargarModal();
                        swal("", datos[1], "success");
                    } else {
                        swal("Acción Cancelada", datos[1], "warning");
                    }
                });
                break;
        }
    };

    $scope.ValidarCliente = function (event) {
        var documento = $.trim($scope.Documento);
        if (documento == "") {
            return false;
        }
        if (documento != $scope.Cliente.documento) {
            if ($scope.Cliente.documento != "") {
                $scope.LimpiarDatos();
            }
            if (event.keyCode == 13) {
                $("#Sede").focus();
            }
        }
    }

    $scope.ValidarCotizacion = function (event) {
        var cotizacion = $scope.Cotizacion * 1;
        var revision = $scope.Revision * 1;
        if (cotizacion == 0) {
            return false;
        }
        if (revision == 0) {
            $scope.Revision = 1;
            revision = 1;
        }
        if ($scope.Cotizacion != $scope.Registro.cotizacion || $scope.Revision != $scope.Registro.revision) {
            if ($scope.Registro.cotizacion != 0) {
                $scope.LimpiarDatos();
            }
            if (event.keyCode == 13) {
                $("#Sede").focus();
            }
        }
    }


    $scope.BuscarClienteCot = function (totalizar, contacto, sede, forma_pago) {
        var documento = $.trim($scope.Documento);
        if (totalizar == 1) {
            if ($.trim(documento) == $scope.Cliente.documento || $.trim(documento) == "") {
                return false;
            }

            if ($scope.Cliente.id > 0) {
                $scope.LimpiarTodo();
                $scope.Documento = documento;
            }
            forma_pago = "";
        }
        var parametros = "opcion=BuscarCliente&documento=" + documento + "&bloquear=1&json=1";
        General.LlamarAjax("Cotizacion", parametros, true, "", 0).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                var data = JSON.parse(datos[1]);
                $scope.Cliente = data[0];
                $scope.Combos.contacto = JSON.parse(datos[2]);
                $scope.Combos.sede = JSON.parse(datos[3]);
                $scope.Registro.cliente = $scope.Cliente.id;

                $scope.Registro.moneda = $.trim($scope.idtipomoneda);
                $scope.CargarForma_Pago(forma_pago);
                $scope.TablaCotizacion(totalizar);

                if (totalizar == 0) {
                    $scope.Registro.sede = sede;
                    $scope.Registro.contacto = contacto;
                }

            } else {
                if (datos[0] == "8") {
                    swal({
                        title: 'Advertencia',
                        text: datos[1] + "<br><br>... ¿Desea desbloquearlo?",
                        type: 'question',
                        showLoaderOnConfirm: true,
                        showCancelButton: true,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        confirmButtonText: $rootScope.ValidarTraduccion('Desbloquear'),
                        cancelButtonText: $rootScope.ValidarTraduccion('Cancelar'),
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        preConfirm: function () {
                            return new Promise(function (resolve, reject) {
                                DesbloquearCliente(documento, 0);
                                $scope.Documento = "";
                                $scope.BuscarClienteCot(1);
                                resolve();
                            })
                        }
                    }).then(function (result) {
                        if (result) {
                            swal({
                                type: 'success',
                                html: "Cliente Desbloqueado"
                            })
                        }
                    }, function (dismiss) {
                        if (dismiss == 'cancel') {
                            $scope.LimpiarTodo();
                            $("#Cliente").focus();
                            return false;
                        }
                    });

                    $(".swal2-content").html(datos[1] + "<br><br>... ¿Desea desbloquearlo?");
                } else {
                    if (datos[0] == "5") {
                        $scope.Documento = "";
                        $("#Cliente").focus();
                        swal("Acción Cancelada", datos[1], "warning");
                    } else {
                        $scope.Documento = "";
                        $("#Cliente").focus();
                        swal("Acción Cancelada", "El documento " + documento + ", no se encuentra registrado como cliente", "warning");
                    }
                }
            }
        });
    }


    $scope.BuscarCotizacion = function () {
        var cotizacion = $scope.Cotizacion * 1;
        var revision = $scope.Revision * 1;
        if (revision == 0) {
            $scope.Revision = 1;
            revision = 1;
        }
        if (cotizacion == $scope.Registro.cotizacion || cotizacion == 0) {
            return false;
        }

        if ($scope.Registro.cotizacion > 0) {
            $scope.LimpiarTodo();
            $scope.Cotizacion = cotizacion;
            $scope.Revision = revision;
        }

        var parametros = "opcion=BuscarCotizacion&cotizacion=" + cotizacion + "&revision=" + revision;
        General.LlamarAjax("Cotizacion", parametros, true, "", 1).then(function (response) {
            if (response.length > 0) {
                var data = response[0];
                $scope.Registro.cotizacion = cotizacion;
                $scope.Registro.revision = revision;
                $scope.Remision = $.trim(data.remision);
                $scope.Registro.id = data.id;
                $scope.Registro.cliente = data.idcliente;
                $scope.Registro.empresa = $.trim(data.idempresa);
                $scope.Registro.solicitud = data.solicitud;
                $scope.Registro.remision = data.remision;
                $scope.Registro.estado = data.estado;
                $scope.Registro.observaciones = data.observacion;
                $scope.Registro.subtotal = data.subtotal;
                $scope.Registro.descuento = data.descuento;
                $scope.Registro.gravable = data.gravable;
                $scope.Registro.exento = data.exemto;
                $scope.Registro.iva = data.iva;
                $scope.Registro.retefuente = data.retefuente;
                $scope.Registro.reteiva = data.reteiva;
                $scope.Registro.reteica = data.reteica;
                $scope.Registro.total = data.total;
                $scope.Registro.subtotalusd = data.subtotalusd;
                $scope.Registro.descuentousd = data.descuentousd;
                $scope.Registro.gravableusd = data.gravableusd;
                $scope.Registro.exentousd = data.exentousd;
                $scope.Registro.ivausd = data.ivausd;
                $scope.Registro.retefuenteusd = data.retefuenteusd;
                $scope.Registro.reteivausd = data.reteivausd;
                $scope.Registro.reteicausd = data.reteicausd;
                $scope.Registro.totalusd = data.totalusd;
                $scope.Registro.lineanegocio = $.trim(data.idlineanegocio);
                $scope.Registro.validez = data.validez;
                $scope.Registro.condicion_entrega = data.condicion_entrega;
                $scope.Registro.plazo_entrega = data.plazo_entrega;
                $scope.Registro.licitacion = data.licitacion;
                $scope.Documento = data.documento;
                $scope.BuscarClienteCot(1, $.trim(data.idcontacto), $.trim(data.idsede), data.forma_pago);

            } else {
                $scope.Cotizacion = "";
                $("#Cotizacion").focus();
                swal("Acción Cancelada", "Cotización número " + cotizacion + " con la revisón número " + revision + " no se encuentra registrada", "warning");
            }
        });
    }



    $scope.DatosContacto = function (contacto) {
        $scope.Cliente.email = "";
        $scope.Cliente.telefono = "";
        $scope.Cliente.celular = "";

        General.LlamarAjax("Cotizacion", "opcion=DatosContacto&id=" + contacto, false, "", 1).then(function (response) {
            if (response.length > 0) {
                $scope.Cliente.email = response[0].email;
                $scope.Cliente.telefono = response[0].telefonos;
                $scope.Cliente.celular = response[0].fax;
            }
        });

    }

    $scope.ModalServicio = function () {
        if (($scope.Registro.estado == 'Temporal' || $scope.Registro.estado == "Cotizado") && ($scope.Registro.cliente > 0)) {
            if ($scope.Registro.sede == "") {
                $("#Sede").focus();
                swal("Acción Cancelada", "Debe seleccionar la sede del cliente", "warning");
                return false;
            }
            if ($scope.Registro.contacto == "") {
                $("#Contacto").focus();
                swal("Acción Cancelada", "Debe seleccionar el contacto del cliente", "warning");
                return false;
            }

            if ($scope.Cliente.req_solicitud == "SI" && $scope.Cliente.Habitual == "NO" && $scope.Registro.tabla.length == 0 && $scope.Registro.solicitud == 0) {
                swal("Acción Cancelada", "Debe ingresar la solicitud del cliente", "warning");
                return false;
            }
            $scope.LimpiarServicio($scope.TipoServicio);

            $("#ModalServicios").modal({backdrop: 'static'}, 'show');
        }
    };

    $scope.ModalClientes = function () {
        $rootScope.Modal.cedula = "";
        $rootScope.Modal.razonsocial = "";
        $rootScope.Modal.tipo = 2;
        $rootScope.Modal.Open("Clientes");
    }

    $scope.ModalArticulos = function () {

        if (($scope.Registro.estado == 'Temporal' || $scope.Registro.estado == "Cotizado") && ($scope.Registro.cliente > 0)) {

            if ($scope.Registro.sede == "") {
                $("#Sede").focus();
                swal("Acción Cancelada", "Debe seleccionar la sede del cliente", "warning");
                return false
            }
            if ($scope.Registro.contacto == "") {
                $("#Contacto").focus();
                swal("Acción Cancelada", "Debe seleccionar el contacto del cliente", "warning");
                return false
            }

            if ($scope.Cliente.req_solicitud == "SI" && $scope.Cliente.Habitual == "NO" && $scope.Registro.tabla.length == 0 && $scope.Registro.solicitud == 0) {
                swal("Acción Cancelada", "Debe ingresar la solicitud del cliente", "warning");
                return false;
            }

            $rootScope.Modal.grupo = "";
            $rootScope.Modal.codigobarras = "";
            $rootScope.Modal.descripcion = "";
            $rootScope.Modal.tipo = 1;
            $rootScope.Modal.moneda = $.trim($scope.Cliente.idtipomoneda);
            $rootScope.Modal.dis_moneda = true;
            $rootScope.Modal.dis_precio = true;
            $rootScope.Modal.dis_ubicacion = false;
            $rootScope.Modal.Open("Inventarios");

        }


    }




    $scope.LimpiarTodo = function () {
        $scope.Documento = "";
        $scope.DocumentoCli = "";
        $scope.Cotizacion = "";
        $scope.Revision = "1";
        $scope.Remision = "";
        $scope.LimpiarDatos();
    }

    $scope.LimpiarServicio = function (tipo) {
        $scope.VisibleServicio = [true, true];
        if (tipo == 1) {
            $scope.InicializarServicio();
            $scope.Servicio.descuento = $scope.Cliente.descuento;
            $scope.Servicio.iva = $scope.Cliente.valor;
            $scope.Servicio.nombreca = $scope.Cliente.nombrecompleto;
            $scope.Servicio.direccion = "SEDE";
            $scope.Servicio.pagocertificado = Cliente.pagcertificado;
        } else {
            $scope.InicializarPesa();
            $scope.Pesa.descuento = $scope.Cliente.descuento;
            $scope.Pesa.iva = $scope.Cliente.valor;
            $scope.Pesa.nombreca = $scope.Cliente.nombrecompleto;
            $scope.Pesa.direccion = "SEDE";
            $scope.Pesa.pagocertificado = Cliente.pagcertificado;
        }
    }

    $scope.LimpiarOtroServicio = function () {
        $scope.InicializarServicio();
        $scope.Servicio.descuento = $scope.Cliente.descuento;
        $scope.Servicio.iva = $scope.Cliente.valor;
        $scope.Servicio.moneda = $.trim($scope.Cliente.preciomoneda);
    }

    $scope.LimpiarDatos = function () {
        $scope.Cliente = {documento: ""};
        $scope.InicializarRegistro();
        $scope.Combos.sede = "";
        $scope.Combos.contacto = "";
    }

    $scope.CargarModelos = function (marca, modelo, tipo) {
        General.CargarCombo(5, marca).then(function (response) {

            if (tipo == 1) {
                $scope.Combos.modelo = response.data;
                if (modelo != "")
                    $scope.Servicio.modelo = modelo;
            } else {
                $scope.Combos.modelo_pesa = response.data;
                if (modelo != "")
                    $scope.Pesa.modelo = modelo;
            }
        });
    }

    $scope.CargarForma_Pago = function (forma) {
        General.CargarCombo(107, $scope.Cliente.tipocliente).then(function (response) {
            $scope.Combos.forma_pago = response.data;
            if (forma != "")
                $scope.Registro.forma_pago = forma;
        });
    }

    $scope.AgregarServicio = function () {

        if ($scope.Servicio.proveedor != "")
            $scope.Servicio.descuento = 0;

        var mensaje = "";

        if ($scope.TipoServicio == 1) {

            General.LlamarAjax("Cotizacion", "opcion=VerificarServicio&servicio=" + $scope.Servicio.servicio, false, "", 0).then(function (response) {
                var tiposervicio = response * 1;

                if ($rootScope.NumeroDecimal($scope.Servicio.precioadic) > 0 && $.trim($scope.Servicio.calibracionadic) == "")
                    mensaje = "<br> Debe de ingresar la descripción de la calibración adicional" + mensaje;

                if ($rootScope.NumeroDecimal($scope.Servicio.precioadic) == 0 && $.trim($scope.Servicio.calibracionadic) != "")
                    mensaje = "<br> Debe de ingresar el precio de la calibración adicional" + mensaje;

                if (tiposervicio == 0 || tiposervicio == 2) {
                    if ($scope.Servicio.certificado == "") {
                        $("#Certificado").focus();
                        mensaje = "<br> Seleccione el tipo de impresión del certificado";
                    }
                    if ($.trim($scope.Servicio.serie) == "" && $scope.Servicio.ingreso > 0) {
                        $("#Serie").focus();
                        mensaje = "<br> Ingrese la serie del equipo " + mensaje;
                    }
                    if ($.trim($scope.Servicio.declaracion) == "") {
                        $("#Declaracion").focus();
                        mensaje = "<br> Ingrese la declaración de conformidad del certificado " + mensaje;
                    }
                    if ($.trim($scope.Servicio.punto) == "") {
                        $("#Punto").focus();
                        mensaje = "<br> Ingrese el punto específico de la calibracion " + mensaje;
                    }

                    if ($scope.Servicio.metodo == "" && tiposervicio == 0) {
                        $("#Metodo").focus();
                        mensaje = "<br> Ingrese el método de la calibracion " + mensaje;
                    }


                } else {
                    if ($.trim($scope.Servicio.observacion) == "") {
                        $("#Observacion").focus();
                        mensaje = "<br> Debe de ingresar la observación del servicio " + mensaje;
                    }
                }

                if ($scope.Servicio.servicio == "") {
                    $("#Servicio").focus();
                    mensaje = "<br> Ingrese el servicio del equipo " + mensaje;
                }


                if ($scope.Servicio.cantidad == 0) {
                    $("#Cantidad").focus();
                    mensaje = "<br> Ingrese la cantidad de equipo(s) " + mensaje;
                }


                switch ($scope.Servicio.tipoprecio) {
                    case 3:
                        if ($scope.Servicio.proveedor == "") {
                            $("#Proveedor").focus();
                            mensaje = "<br> Ingrese el proveedor del servicio " + mensaje;
                        }
                        if ($scope.Servicio.costo == 0) {
                            mensaje = "<br> Debe de configurar el costo del servicio " + mensaje;
                        }
                        if ($scope.Servicio.modelo == "") {
                            $("#Modelo").focus();
                            mensaje = "<br> Ingrese el modelo del equipo " + mensaje;
                        }
                        if ($scope.Servicio.marca == "") {
                            $("#Marca").focus();
                            mensaje = "<br> Ingrese la Marca del Equipo " + mensaje;
                        }
                        break;
                    case 2:
                        if ($scope.Servicio.modelo == "") {
                            $("#Modelo").focus();
                            mensaje = "<br> Ingrese el modelo del equipo " + mensaje;
                        }
                        if ($scope.Servicio.marca == "") {
                            $("#Marca").focus();
                            mensaje = "<br> Ingrese la Marca del Equipo " + mensaje;
                        }
                        break;
                    case 1:
                        if ($scope.Servicio.intervalo * 1 == 0) {
                            $("#Intervalo").focus();
                            mensaje = "<br> Ingrese el intervalo del quipo " + mensaje;
                        }
                        break;
                }


                if ($scope.Servicio.equipo == "") {
                    $("#Equipo").focus();
                    mensaje = "<br> Ingrese el tipo de equipo " + mensaje;
                }

                if ($scope.Servicio.magnitud == "") {
                    $("#Magnitud").focus();
                    mensaje = "<br> Ingrese la magnitud del equipo " + mensaje;
                }

                if (mensaje != "") {
                    swal("Acción Cancelada", mensaje, "warning");
                    return false;
                }

                if ($scope.Servicio.precio == 0 && $scope.Servicio.tipoprecio != 4) {
                    swal("Acción Cancelada", "El servicio no tiene precio configurado", "warning");
                    return false;
                }

                if (tiposervicio == 0) {
                    if ($scope.Servicio.entrega == 0) {
                        swal("Acción Cancelada", "El servicio no tiene día de entregra configurado", "warning");
                        return false;
                    }
                }

                if ($scope.Servicio.metodo == "")
                    $scope.Servicio.metodo = "-1";

                var parametros = General.ConvertirParametros($scope.Servicio);
                parametros += "&opcion=GuardarTmpCotizacion&cliente=" + $scope.Registro.cliente + "&idsolicitud=" + $scope.Registro.solicitud + "&cliente=" + $scope.Registro.cliente +
                        "&remision=" + $scope.Registro.remision + "&cotizacion=" + $scope.Registro.id +
                        "&sede=" + $scope.Registro.sede + "&contacto=" + $scope.Registro.contacto;
                General.LlamarAjax("Cotizacion", parametros, true, "", 0).then(function (response) {
                    datos = $.trim(response).split("|");
                    if (datos[0] == "0") {
                        $scope.Registro.estado = "Temporal";
                        swal("", datos[1], "success");
                        $scope.TablaCotizacion(1);
                        $scope.InicializarServicio();
                        if (datos[1] == "Servicio actualizado") {
                            $("#ModalServicios").modal("hide");
                        }
                    } else
                        swal("Acción Cancelada", datos[1], "warning");
                });
            });
        } else {
            if ($.trim($scope.Pesa.certificado) == "") {
                $("#CertificadoPesa").focus();
                mensaje = "<br> Seleccione el tipo de impresión del certificado";
            }

            if ($.trim($scope.Pesa.declaracion) == "") {
                $("#DeclaracionPesa").focus();
                mensaje = "<br> Ingrese la declaración de conformidad del certificado " + mensaje;
            }

            if ($.trim($scope.Pesa.metodo) == "") {
                $("#MetodoPesa").focus();
                mensaje = "<br> Ingrese el método de la calibracion " + mensaje;
            }

            if ($.trim($scope.Pesa.observacion) == "") {
                $("#ObservacionPesa").focus();
                mensaje = "<br> Debe de ingresar la observación del servicio " + mensaje;
            }

            if ($.trim($scope.Pesa.nombreca) == "") {
                $("#NombrePesa").focus();
                mensaje = "<br> Ingrese a nombre de quien va el certificado certificado " + mensaje;
            }
            if ($.trim($scope.Pesa.direccion) == "") {
                $("#DireccionPesa").focus();
                mensaje = "<br> Ingrese la direccion del certificado " + mensaje;
            }


            if ($scope.Pesa.servicio == "") {
                $("#ServicioPesa").focus();
                mensaje = "<br> Ingrese el servicio del equipo " + mensaje;
            }

            if ($scope.Pesa.tipoprecio == 3) {
                if ($scope.Pesa.proveedor == "") {
                    $("#Proveedor_Pesa").focus();
                    mensaje += "Debe de seleccionar un proveedor " + mensaje;
                }
            }

            if ($scope.Pesa.clase == "") {
                $("#Clase_Pesa").focus();
                mensaje = "<br> Ingrese la clase del equipo " + mensaje;
            }

            if ($scope.Pesa.modelo == "") {
                $("#ModeloPesa").focus();
                mensaje = "<br> Ingrese el modelo del equipo " + mensaje;
            }
            if ($scope.Pesa.marca == "") {
                $("#MarcaPesa").focus();
                mensaje = "<br> Ingrese la Marca del Equipo " + mensaje;
            }

            if ($scope.Pesa.equipo == "") {
                $("#EquipoPesa").focus();
                mensaje = "<br> Ingrese el tipo de equipo " + mensaje;
            }

            if (mensaje != "") {
                swal("Acción Cancelada", mensaje, "warning");
                return false;
            }

            if ($scope.Pesa.entrega == 0) {
                swal("Acción Cancelada", "El servicio no tiene día de entregra configurado", "warning");
                return false;
            }

            var a_subtotal = "";
            var a_cantidad = "";
            var a_nominal = "";
            var a_cmc = "";
            var a_precio = "";
            var a_idequipo = "";

            for (var x = 0; x < $scope.Pesa.tabla_clase.length; x++) {
                if ($rootScope.NumeroDecimal($scope.Pesa.tabla_clase[x].cantidad) * 1 > 0) {
                    if (a_cantidad != "") {
                        a_subtotal += ",";
                        a_cantidad += ",";
                        a_nominal += ",";
                        a_cmc += ",";
                        a_precio += ",";
                        a_idequipo += ",";
                    }

                    a_cantidad += $rootScope.NumeroDecimal($scope.Pesa.tabla_clase[x].cantidad)
                    a_subtotal += $rootScope.NumeroDecimal($scope.Pesa.tabla_clase[x].subtotal)
                    a_precio += $rootScope.NumeroDecimal($scope.Pesa.tabla_clase[x].precio)
                    a_nominal += $rootScope.NumeroDecimal($scope.Pesa.tabla_clase[x].nominal)
                    a_cmc += $rootScope.NumeroDecimal($scope.Pesa.tabla_clase[x].cmc)
                    a_idequipo += $scope.Pesa.tabla_clase[x].ident_equipo;
                }
            }

            if (a_cantidad == "") {
                swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un nominal del alcance", "warning");
                return false;
            }

            if ($scope.Pesa.metodo == "")
                $scope.Pesa.metodo = "-1";


            var parametros = General.ConvertirParametros($scope.Pesa);
            parametros += "&opcion=GuardarTmpCotizacion&cliente=" + $scope.Registro.cliente + "&idsolicitud=" + $scope.Registro.solicitud + "&cliente=" + $scope.Registro.cliente +
                    "&remision=" + $scope.Registro.remision + "&cotizacion=" + $scope.Registro.id +
                    "&sede=" + $scope.Registro.sede + "&contacto=" + $scope.Registro.contacto;
            parametros += "&a_cantidad=" + a_cantidad + "&a_subtotal=" + a_subtotal + "&a_precio=" + a_precio + "&a_nominal=" + a_nominal + "&a_cmc=" + a_cmc + "&a_idequipo=" + a_idequipo;

            General.LlamarAjax("Cotizacion", parametros, true, "", 0).then(function (response) {
                datos = $.trim(response).split("|");
                if (datos[0] == "0") {
                    $scope.Registro.estado = "Temporal";
                    swal("", datos[1], "success");
                    $scope.TablaCotizacion(1);
                    $scope.InicializarPesa();
                    if (datos[1] == "Servicio actualizado") {
                        $("#ModalServicios").modal("hide");
                    }
                } else
                    swal("Acción Cancelada", datos[1], "warning");
            });
        }
    }

    $scope.Totales = function () {

        $scope.Registro.subtotal = 0;
        $scope.Registro.descuento = 0;
        $scope.Registro.gravable = 0;
        $scope.Registro.exento = 0;
        $scope.Registro.iva = 0;
        $scope.Registro.retefuente = 0;
        $scope.Registro.reteiva = 0;
        $scope.Registro.reteica = 0;
        $scope.Registro.total = 0;
        $scope.Registro.tcantidad = 0;

        $scope.Registro.subtotalusd = 0;
        $scope.Registro.descuentousd = 0;
        $scope.Registro.gravableusd = 0;
        $scope.Registro.exentousd = 0;
        $scope.Registro.ivausd = 0;
        $scope.Registro.retefuenteusd = 0;
        $scope.Registro.reteivausd = 0;
        $scope.Registro.reteicausd = 0;
        $scope.Registro.totalusd = 0;
        $scope.Registro.tcantidadusd = 0;

        for (var i = 0; i < $scope.Registro.tabla.length; i++) {
            subtotal = ($rootScope.NumeroDecimal($scope.Registro.tabla[i].precio) + $rootScope.NumeroDecimal($scope.Registro.tabla[i].precioadic)) * $rootScope.NumeroDecimal($scope.Registro.tabla[i].cantidad);
            if ($scope.Registro.tabla[i].idmoneda != 2) {
                $scope.Registro.subtotal += subtotal;
                $scope.Registro.tcantidad += $rootScope.NumeroDecimal($scope.Registro.tabla[i].cantidad);
                $scope.Registro.descuento += (subtotal * $rootScope.NumeroDecimal($scope.Registro.tabla[i].descuento)) / 100;
                if ($rootScope.NumeroDecimal($scope.Registro.tabla[i].iva) > 0) {
                    $scope.Registro.gravable += subtotal;
                    $scope.Registro.iva += (subtotal - ((subtotal * $rootScope.NumeroDecimal($scope.Registro.tabla[i].descuento)) / 100)) * $rootScope.NumeroDecimal($scope.Registro.tabla[i].iva) / 100;
                } else {
                    $scope.Registro.exento += subtotal;
                }
            } else {
                $scope.Registro.subtotalusd += subtotal;
                $scope.Registro.tcantidadusd += $rootScope.NumeroDecimal($scope.Registro.tabla[i].cantidad);
                $scope.Registro.descuentousd += (subtotal * $rootScope.NumeroDecimal($scope.Registro.tabla[i].descuento)) / 100;
                if ($rootScope.NumeroDecimal($scope.Registro.tabla[i].iva) > 0) {
                    $scope.Registro.gravableusd += subtotal;
                    $scope.Registro.ivausd += (subtotal - ((subtotal * $rootScope.NumeroDecimal($scope.Registro.tabla[i].descuento)) / 100)) * $rootScope.NumeroDecimal($scope.Registro.tabla[i].iva) / 100;
                } else {
                    $scope.Registro.exentousd += subtotal;
                }
            }
        }

        $scope.Registro.total = $rootScope.NumeroDecimal($scope.Registro.subtotal) - $rootScope.NumeroDecimal($scope.Registro.descuento) + $rootScope.NumeroDecimal($scope.Registro.iva) - $rootScope.NumeroDecimal($scope.Registro.reteiva) - $rootScope.NumeroDecimal($scope.Registro.retefuente) - $rootScope.NumeroDecimal($scope.Registro.reteica);
        $scope.Registro.totalusd = $rootScope.NumeroDecimal($scope.Registro.subtotalusd) - $rootScope.NumeroDecimal($scope.Registro.descuentousd) + $rootScope.NumeroDecimal($scope.Registro.ivausd) - $rootScope.NumeroDecimal($scope.Registro.reteivausd) - $rootScope.NumeroDecimal($scope.Registro.retefuenteusd) - $rootScope.NumeroDecimal($scope.Registro.reteicausd);

    }

    $scope.TablaCotizacion = function (totalizar) {
        var parametros = "opcion=TemporalIngresoCotiz&cliente=" + $scope.Registro.cliente + "&remision=" + $scope.Registro.idremision + "&buscar=0&cotizacion=" + $scope.Registro.id + "&sede=" + $scope.Registro.sede + "&contacto=" + $scope.Registro.contacto + "&numero=" + $scope.Registro.remision;
        General.LlamarAjax("Cotizacion", parametros, true, "", 0).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                $scope.Registro.tabla = JSON.parse(datos[1]);
                if (totalizar == 1) {
                    if (datos[1] != "[]") {
                        if ($scope.Registro.solicitud == 0)
                            $scope.Registro.solicitud = $scope.Registro.tabla[0].solicitud;
                        if ($scope.Registro.sede == "") {
                            $scope.Registro.sede = $.trim($scope.Registro.tabla[0].idsede);
                            $scope.Registro.contacto = $.trim($scope.Registro.tabla[0].idcontacto);
                        }
                    }
                    $scope.Totales();
                }
            }
        });
    }

    $scope.CargarIntervalos = function (magnitud, intervalo, intervalo2) {
        General.CargarCombo(6, magnitud).then(function (response) {
            $scope.Combos.intervalo = response.data;
            if (intervalo != "") {
                $scope.Servicio.intervalo = intervalo;
            }
            if (intervalo2 != "") {
                $scope.Servicio.intervalo2 = intervalo2;
            }
        });
        if ($scope.OpcionEquipo == 0) {
            $scope.OpcionMagnitud = 1;
            $scope.Servicio.equipo = "";
            if (magnitud * 1 > 0) {
                General.CargarCombo(2, magnitud).then(function (response) {
                    $scope.Combos.equipo = response.data;
                    $scope.OpcionMagnitud = 0;
                });
            } else
            {
                General.CargarCombo(58, "").then(function (response) {
                    $scope.Combos.equipo = response.data;
                    $scope.OpcionMagnitud = 0;
                });
                $scope.Combos.intervalo = "";
            }
        }
        $scope.BuscarPrecio();
    }


    $scope.MagnitudEquipo = function (equipo, intervalo, intervalo2, metodo, clase, tipo) {
        if (tipo == 1) {
            if ($scope.OpcionMagnitud > 0)
                return false;
        }
        General.CargarCombo(17, equipo).then(function (response) {
            if (tipo == 1)
                $scope.Combos.metodo = response.data;
            else
                $scope.Combos.metodo_pesa = response.data;

            if (metodo != "") {
                if (tipo == 1)
                    $scope.Servicio.metodo = metodo;
                else
                    $scope.Pesa.metodo = metodo;
            }
        });

        if (tipo == 1) {
            var parametros = "opcion=MagnitudEquipo&id=" + equipo;
            General.LlamarAjax("Cotizacion", parametros, false, "", 1).then(function (response) {
                if (response.length > 0) {
                    $scope.OpcionEquipo = 1;
                    if (response[0].idmagnitud * 1 != $scope.Servicio.magnitud * 1) {
                        $scope.Servicio.magnitud = $.trim(response[0].idmagnitud);
                        $scope.CargarIntervalos($scope.Servicio.magnitud, intervalo, intervalo2);
                        $scope.OpcionEquipo = 0;
                    } else {
                        $scope.OpcionEquipo = 0;
                    }
                }
                $scope.BuscarPrecio();
            });
        } else {
            General.CargarCombo(110, equipo).then(function (response) {
                $scope.Combos.clase = response.data;
                if (clase != "") {
                    $scope.Pesa.clase = clase;
                    $scope.TablaClases();
                }
            });
            var parametros = "opcion=MagnitudEquipo&id=" + equipo;
            General.LlamarAjax("Cotizacion", parametros, false, "", 1).then(function (response) {
                if (response.length > 0) {
                    $scope.Pesa.magnitud = $.trim(response[0].idmagnitud);
                }
            });
        }
    }

    $scope.CargarProveedores = function (servicio, tipo) {
        $scope.CargarDiaEntrega(servicio, tipo);
        $scope.CargarOtrosIngresos(servicio, tipo);
        $scope.BuscarPrecio(tipo);
    }

    $scope.CargarOtrosIngresos = function (servicio, tipo) {
        datos = 0;
        if (servicio * 1 > 0) {
            General.LlamarAjax("Cotizacion", "opcion=VerificarServicio&servicio=" + servicio, false, "", 0).then(function (response) {
                var datos = response * 1;
                if (datos == 1) {

                    if (tipo == 1) {
                        $scope.Servicio.tipoprecio = 2;
                        $scope.Servicio.metodo = "";
                        $scope.Servicio.punto = "";
                        $scope.Servicio.nombreca = "";
                        $scope.Servicio.direccion = "";
                        $scope.Servicio.calibracion = "";
                        $scope.Servicio.declaracion = "";
                        $scope.Servicio.certificado = "";

                        $scope.Servicio.dis_punto = true;
                        $scope.Servicio.dis_declaracion = true;
                        $scope.Servicio.dis_nombreca = true;
                        $scope.Servicio.dis_certificado = true;
                        $scope.Servicio.dis_direccion = true;
                        $scope.Servicio.dis_calibracion = true;
                        $scope.Servicio.dis_metodo = true;
                    } else {
                        $scope.Pesa.tipoprecio = 2;
                        $scope.Pesa.metodo = "";
                        $scope.Pesa.nombreca = "";
                        $scope.Pesa.direccion = "";
                        $scope.Pesa.calibracion = "";
                        $scope.Pesa.declaracion = "";
                        $scope.Pesa.certificado = "";

                        $scope.Pesa.dis_declaracion = true;
                        $scope.Pesa.dis_nombreca = true;
                        $scope.Pesa.dis_certificado = true;
                        $scope.Pesa.dis_direccion = true;
                        $scope.Pesa.dis_calibracion = true;
                        $scope.Pesa.dis_metodo = true;
                    }
                } else {
                    if (datos == 3) {
                        if (tipo == 1)
                            $scope.Servicio.tipoprecio = 4;
                        else
                            $scope.Pesa.tipoprecio = 4;
                    }
                    if (datos == 2) {
                        if (tipo == 1)
                            $scope.Servicio.tipoprecio = 2;
                        else
                            $scope.Pesa.tipoprecio = 2;
                    }
                    if ($scope.Registro.solicitud == 0) {
                        if (tipo == 1) {
                            $scope.Servicio.direccion = "SEDE"
                            $scope.Servicio.nombreca = $scope.Cliente.nombrecompleto;
                        } else {
                            $scope.Pesa.direccion = "SEDE"
                            $scope.Pesa.nombreca = $scope.Cliente.nombrecompleto;
                        }
                    }
                    if (datos == 0) {
                        if (tipo == 1) {
                            $scope.Servicio.dis_metodo = false;
                        } else {
                            $scope.Pesa.dis_metodo = false;
                        }
                    } else {
                        if (tipo == 1)
                            $scope.Servicio.dis_metodo = true;
                        else
                            $scope.Pesa.dis_metodo = true;
                    }

                    if (tipo == 1) {
                        $scope.Servicio.dis_punto = false;
                        $scope.Servicio.dis_declaracion = false;
                        $scope.Servicio.dis_nombreca = false;
                        $scope.Servicio.dis_certificado = false;
                        $scope.Servicio.dis_direccion = false;
                        $scope.Servicio.dis_calibracion = false;
                    } else {
                        $scope.Pesa.dis_declaracion = false;
                        $scope.Pesa.dis_nombreca = false;
                        $scope.Pesa.dis_certificado = false;
                        $scope.Pesa.dis_direccion = false;
                        $scope.Pesa.dis_calibracion = false;
                    }
                }
            });
        }

    }

    $scope.EliminarCotizacion = function (servicio, index) {

        if ($scope.Registro.tcantidad == 1 && $scope.Registro.estado != "Temporal") {
            swal("Acción Cancelada", "No se puede eliminar el único registro de la cotizacion", "warning");
            return false;
        }

        if ($scope.Registro.estado != "Temporal" && $scope.Registro.estado != "Cotizado") {
            swal("Acción Cancelada", "No se puede eliminar una cotización con estado " + Estado, "warning");
            return false;
        }

        swal.queue([{
                title: $rootScope.ValidarTraduccion('Advertencia'),
                text: $rootScope.ValidarTraduccion('¿Seguro que desea eliminar el item de cotización del equipo ' + (servicio.idequipo > 0 ? servicio.equipo : servicio.observacion) + '?'),
                type: 'question',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                confirmButtonText: $rootScope.ValidarTraduccion('Eliminar'),
                cancelButtonText: $rootScope.ValidarTraduccion('Cancelar'),
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        General.LlamarAjax("Cotizacion", "opcion=EliminarRegCotizacion&id=" + servicio.id + "&cotizacion=" + $scope.Registro.id + "&cantidad=" + $scope.Servicio.cantidad + "&lote=" + $scope.Servicio.lote, false, "", 1).then(function (response) {
                            var datos = response.split("|");
                            if (datos[0] == "0") {
                                $scope.Registro.estado = "Temporal";
                                $scope.Registro.tabla.splice(index, 1);
                                $scope.Totales();
                                resolve();
                            } else {
                                reject(datos[1]);
                            }
                        })
                    })
                }
            }]);
    }

    $scope.CambiarTipoMoneda = function (moneda) {

        General.LlamarAjax("Configuracion", "opcion=TablaPrecio_Moneda&moneda=" + moneda, false, "", 0).then(function (response) {
            $scope.Servicio.tipopreciomoneda = response * 1;
            $scope.BuscarPrecio();
        });
    }

    $scope.BuscarPrecio = function (tipo) {
        if ($scope.Registro.buscarprecio == 1)
            return false;
        var magnitud = 0;
        var servicio = 0;
        if (tipo == 1) {
            magnitud = $scope.Servicio.magnitud;
            servicio = $scope.Servicio.servicio;
        } else {
            magnitud = $scope.Pesa.magnitud;
            servicio = $scope.Pesa.servicio;
        }

        var parametros = "opcion=TipoPrecio&magnitud=" + magnitud + "&servicio=" + servicio;
        General.LlamarAjax("Cotizacion", parametros, false, "", 0).then(function (response) {
            var datos = $.trim(response).split("|");
            $scope.Servicio.tipoprecio = datos[0] * 1;

            switch ($scope.Servicio.tipoprecio) {
                case 0:
                case 1:
                case 2:
                    if (tipo == 1) {
                        $scope.Servicio.proveedor = "";
                        $scope.Combos.proveedor_pesa = "";
                        $scope.Servicio.dis_proveedor = true;
                    } else {
                        $scope.Servicio.proveedor_pesa = "";
                        $scope.Combos.proveedor = "";
                        $scope.Servicio.dis_proveedor_pesa = true;
                    }
                    $scope.Servicio.costo = 0;

                    break;
                case 3:

                    if (tipo == 1 && $scope.Servicio.proveedor == "") {
                        $scope.Combos.proveedor = $scope.Combos.bproveedor;
                        $scope.Servicio.dis_proveedor = false;
                    } else {
                        $scope.Combos.proveedor_pesa = $scope.Combos.bproveedor;
                        $scope.Servicio.dis_proveedor_pesa = false;
                    }

                    $scope.Servicio.tipoprecio = datos[1] * 1;
                    break;
            }

            if (tipo == 2)
                return false;

            $scope.Servicio.precio = 0;
            $scope.Servicio.idprecio = 0;
            $scope.Servicio.descuento = 0;
            $scope.Servicio.costo = 0;
            $scope.Servicio.subtotal = 0;

            if ($scope.Servicio.proveedor == "") {
                $scope.Servicio.descuento = $scope.Cliente.descuento;
            } else {
                $scope.Servicio.descuento = 0
            }

            if ($scope.Servicio.magnitud == "" || $scope.Servicio.servicio == "" || $scope.Servicio.equipo == "")
                return false;

            switch ($scope.Servicio.tipoprecio) {
                case 1:
                    if ($scope.Servicio.intervalo == "")
                        return false;
                    break;
                case 2:
                    if ($scope.Servicio.marca == "" || $scope.Servicio.modelo == "")
                        return false;
                    break;
            }
            var parametros = "opcion=PrecioServicio&magnitud=" + $scope.Servicio.magnitud + "&equipo=" + $scope.Servicio.equipo + "&marca=" + $scope.Servicio.marca + "&modelo=" + $scope.Servicio.modelo + "&intervalo=" + $scope.Servicio.intervalo +
                    "&precio=" + $scope.Cliente.tablaprecio + "&proveedor=" + $scope.Servicio.proveedor + "&servicio=" + $scope.Servicio.servicio + "&tipoprecio=" + $scope.Servicio.tipoprecio + "&moneda=" + $scope.Servicio.moneda;
            General.LlamarAjax("Cotizacion", parametros, false, "", 0).then(function (response) {
                var datos = $.trim(response).split("|");
                if (datos[0] == "0") {
                    $scope.Servicio.precio = datos[2] * 1;
                    $scope.Servicio.idprecio = datos[1] * 1;
                    $scope.Servicio.costo = datos[5];
                } else {
                    swal("Acción Cancelada", datos[1], "warning");
                }
                $scope.CalcularPreServ("");
            });
        });
    }

    $scope.CargarDiaEntrega = function (servicio, tipo) {
        if (tipo == 1) {
            if ($scope.Registro.buscarprecio == 1)
                return false;
        }
        if (tipo == 1) {
            $scope.Servicio.entrega = 0;
            $scope.Servicio.express = 0;
            $scope.Servicio.sitio = 0;
            $scope.Servicio.iva = 0;
        } else {
            $scope.Pesa.entrega = 0;
            $scope.Pesa.express = 0;
            $scope.Pesa.sitio = 0;
            $scope.Pesa.iva = 0;
        }
        if (servicio * 1 > 0) {
            General.LlamarAjax("Cotizacion", "opcion=DiasEntregaSer&servicio=" + servicio, false, "", 0).then(function (response) {
                var datos = $.trim(response).split("|");
                if (tipo == 1) {
                    $scope.Servicio.entrega = datos[0] * 1;
                    $scope.Servicio.iva = $scope.Cliente.idiva > 0 ? (datos[1] * 1) : 0;
                    $scope.Servicio.express = datos[2] * 1;
                    $scope.Servicio.sitio = datos[3] * 1;
                } else {
                    $scope.Pesa.entrega = datos[0] * 1;
                    $scope.Pesa.iva = $scope.Cliente.idiva > 0 ? (datos[1] * 1) : 0;
                    $scope.Pesa.express = datos[2] * 1;
                    $scope.Pesa.sitio = datos[3] * 1;
                }
            });
        }
    }

    $scope.SelecccionarFila = function (registro, index) {
        console.log(registro);
        if (($scope.Registro.estado == 'Temporal' || $scope.Registro.estado == "Cotizado") && ($scope.Registro.cliente > 0) && (registro.idserviciodep == 0 || registro.idserviciodep == 99999)) {
            if (registro.idclase == 0) {
                $scope.LimpiarServicio(1);
                $scope.VisibleServicio = [true, false];
                $scope.Registro.buscarprecio = 1;
                $scope.Servicio.observacion = registro.observacion;
                $scope.Servicio.cantidad = registro.cantidad;
                $scope.Servicio.idsolicitud = registro.idsolicitud;
                $scope.Servicio.articulo = registro.idarticulo;
                $scope.Servicio.id = registro.id;
                $scope.Servicio.descuento = registro.descuento;
                $scope.Servicio.lote = registro.lote;
                $scope.Servicio.moneda = $.trim(registro.idmoneda);
                $scope.Servicio.iva = registro.iva;
                if (registro.idequipo != 0) {
                    $scope.Servicio.ident_equipo = registro.ident_equipo;
                    $scope.Servicio.servicio = $.trim(registro.idservicio);
                    $scope.Servicio.equipo = $.trim(registro.idequipo);
                    $scope.MagnitudEquipo($scope.Servicio.equipo, $.trim(registro.idintervalo), $.trim(registro.idintervalo2), $.trim(registro.idmetodo), '', 1);
                    $scope.Servicio.marca = $.trim(registro.idmarca);
                    $scope.CargarModelos($scope.Servicio.marca, $.trim(registro.idmodelo), 1);
                    $scope.CargarProveedores($scope.Servicio.servicio, 1);
                    $scope.Servicio.proveedor = $.trim(registro.idprpveedor);
                    $scope.Servicio.nombreca = $.trim(registro.nombreca) != "" ? registro.nombreca : $scope.Cliente.nombrecompleto;
                    $scope.Servicio.direccion = $.trim(registro.direccion) != "" ? registro.direccion : "SEDE";
                    $scope.Servicio.ingreso = registro.ingreso;
                    $scope.Servicio.serie = registro.serie;
                    $scope.Servicio.calibracion = registro.proxima;
                    $scope.Servicio.certificado = registro.certificado;
                    $scope.Servicio.declaracion = registro.declaracion;
                    $scope.Servicio.punto = registro.punto;
                    $scope.Servicio.entrega = registro.entrega;
                    $scope.Servicio.calibracionadic = registro.calibracionadic;
                    $scope.Servicio.precioadic = $rootScope.formato_numero(registro.precioadic, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tipopreciomoneda = registro.tipopreciomoneda;
                    if (registro.ingreso > 0)
                        $scope.Servicio.dis_ingreso = true;
                    var parametros = "opcion=TipoPrecio&magnitud=" + registro.idmagnitud + "&servicio=" + registro.idservicio;
                    General.LlamarAjax("Cotizacion", parametros, false, "", 0).then(function (response) {
                        var datos = $.trim(response).split("|");
                        $scope.Servicio.tipoprecio = datos[0] * 1;
                        var parametros = "opcion=PrecioServicio&magnitud=" + registro.idmagnitud + "&equipo=" + registro.idequipo + "&marca=" + registro.idmarca + "&modelo=" + registro.idmodelo + "&intervalo=" + registro.idintervalo +
                                "&precio=1&proveedor=" + registro.idproveedor + "&servicio=" + registro.idservicio + "&tipoprecio=" + $scope.Servicio.tipoprecio + "&moneda=1";
                        General.LlamarAjax("Cotizacion", parametros, false, "", 0).then(function (response) {
                            var datos = $.trim(response).split("|");
                            if (datos[0] == "0") {
                                $scope.Servicio.idprecio = datos[1] * 1;
                            }
                        });
                    });
                    $("#ModalServicios").modal({backdrop: 'static'}, 'show');
                    $scope.setActiveSerTab(1);
                } else {
                    $scope.Servicio.otroservicio = $.trim(registro.idservicio);
                    $scope.Servicio.des_servicio = registro.servicio;
                    $("#ModalOtroServicio").modal("show");
                    setTimeout(function () {
                        $("#OServicio").select2();
                    }, 200);

                }
                $scope.Servicio.precio = $rootScope.formato_numero($rootScope.NumeroDecimal(registro.precio), 2, $rootScope._CD, $rootScope._CM, "");
                $scope.CalcularPreServ("");
            } else {
                $scope.LimpiarServicio(2);

                $scope.Registro.buscarprecio = 1;
                $scope.Pesa.observacion = registro.observacion;
                $scope.Pesa.cantidad = registro.cantidad;
                $scope.Pesa.idsolicitud = registro.idsolicitud;
                $scope.Pesa.articulo = registro.idarticulo;
                $scope.Pesa.id = registro.id;
                $scope.Pesa.descuento = registro.descuento;
                $scope.Pesa.lote = registro.lote;
                $scope.Pesa.moneda = $.trim(registro.idmoneda);
                $scope.Pesa.iva = registro.iva;
                $scope.Pesa.precio = registro.precio;

                $scope.Pesa.ident_equipo = registro.ident_equipo;
                $scope.Pesa.servicio = $.trim(registro.idservicio);
                $scope.Pesa.equipo = $.trim(registro.idequipo);
                $scope.MagnitudEquipo($scope.Pesa.equipo, $.trim(registro.idintervalo), $.trim(registro.idintervalo2), $.trim(registro.idmetodo), $.trim(registro.idclase), 2);
                $scope.Pesa.marca = $.trim(registro.idmarca);
                $scope.CargarModelos($scope.Pesa.marca, $.trim(registro.idmodelo), 2);
                $scope.CargarProveedores($scope.Pesa.servicio, 2);
                $scope.Pesa.nombreca = $.trim(registro.nombreca) != "" ? registro.nombreca : $scope.Cliente.nombrecompleto;
                $scope.Pesa.direccion = $.trim(registro.direccion) != "" ? registro.direccion : "SEDE";
                $scope.Pesa.ingreso = registro.ingreso;
                $scope.Pesa.serie = registro.serie;
                $scope.Pesa.calibracion = registro.proxima;
                $scope.Pesa.certificado = registro.certificado;
                $scope.Pesa.declaracion = registro.declaracion;
                $scope.Pesa.entrega = registro.entrega;
                $scope.Pesa.tipopreciomoneda = registro.tipopreciomoneda;
                $scope.TotalClase();
                if (registro.ingreso > 0)
                    $scope.Pesa.dis_ingreso = true;

                $scope.VisibleServicio = [false, true];
                $scope.setActiveSerTab(2);
                $("#ModalServicios").modal({backdrop: 'static'}, 'show');
            }
        }
    }

    $scope.CargarEditarPrecios = function () {


        $scope.Servicio.des_magnitud = $("#Magnitud option:selected").text();
        $scope.Servicio.des_marca = $("#Marca option:selected").text();
        $scope.Servicio.des_modelo = $("#Modelo option:selected").text();
        $scope.Servicio.des_equipo = $("#Equipo option:selected").text();
        $scope.Servicio.des_servicio = $("#Servicio option:selected").text();
        $scope.Servicio.des_proveedor = $("#Proveedor option:selected").text();
        $scope.Servicio.medida = "";
        $scope.Servicio.desde = "";
        $scope.Servicio.hasta = "";

        $scope.Servicio.tablaprecios.dis_MCosto = true;

        if ($scope.Servicio.magnitud == "" || $scope.Servicio.servicio == "" || $scope.Servicio.equipo == "")
            return false;

        $("#modalPrecios").modal("show");


        switch ($scope.Servicio.tipoprecio) {
            case 1:
                if ($scope.Servicio.intervalo == "") {
                    swal("Acción Cancelada", "Debe seleccionar un intervalo", "warning");
                    return false;
                }
                break;
            case 2:
                if ($scope.Servicio.marca == "" || $scope.Servicio.modelo == "") {
                    swal("Acción Cancelada", "Debe seleccionar una marca y modelo", "warning");
                    return false;
                }
                break;
            case 3:
                if ($scope.Servicio.marca == "" || $scope.Servicio.modelo == "" || $scope.Servicio.proveedor == "") {
                    swal("Acción Cancelada", "Debe seleccionar una marca, modelo y proveedor", "warning");
                    return false;
                }
                $scope.Servicio.tablaprecios.dis_MCosto = false;
                break;
        }

        $scope.setActiveTabPre($scope.Servicio.tipopreciomoneda);

        $scope.Servicio.tablaprecios.MPrecio1 = "";
        $scope.Servicio.tablaprecios.MPrecio2 = "0";
        $scope.Servicio.tablaprecios.MPrecio3 = "0";
        $scope.Servicio.tablaprecios.MPrecio4 = "0";
        $scope.Servicio.tablaprecios.MPrecio5 = "0";
        $scope.Servicio.tablaprecios.MPrecio6 = "0";
        $scope.Servicio.tablaprecios.MPrecio7 = "0";
        $scope.Servicio.tablaprecios.MPrecio8 = "0";
        $scope.Servicio.tablaprecios.MPrecio9 = "0";
        $scope.Servicio.tablaprecios.MPrecio10 = "0";

        $scope.Servicio.tablaprecios.MPrecioUSD1 = "";
        $scope.Servicio.tablaprecios.MPrecioUSD2 = "0";
        $scope.Servicio.tablaprecios.MPrecioUSD3 = "0";
        $scope.Servicio.tablaprecios.MPrecioUSD4 = "0";
        $scope.Servicio.tablaprecios.MPrecioUSD5 = "0";
        $scope.Servicio.tablaprecios.MPrecioUSD6 = "0";
        $scope.Servicio.tablaprecios.MPrecioUSD7 = "0";
        $scope.Servicio.tablaprecios.MPrecioUSD8 = "0";
        $scope.Servicio.tablaprecios.MPrecioUSD9 = "0";
        $scope.Servicio.tablaprecios.MPrecioUSD10 = "0";

        if ($scope.Servicio.idprecio > 0) {
            General.LlamarAjax("Cotizacion", "opcion=TablaPrecioServicio&id=" + $scope.Servicio.idprecio, false, "", 1).then(function (data) {
                if (data.length > 0) {
                    $scope.Servicio.tablaprecios.MPrecio1 = $rootScope.formato_numero(data[0].precio1, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecio2 = $rootScope.formato_numero(data[0].precio2, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecio3 = $rootScope.formato_numero(data[0].precio3, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecio4 = $rootScope.formato_numero(data[0].precio4, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecio5 = $rootScope.formato_numero(data[0].precio5, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecio6 = $rootScope.formato_numero(data[0].precio6, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecio7 = $rootScope.formato_numero(data[0].precio7, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecio8 = $rootScope.formato_numero(data[0].precio8, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecio9 = $rootScope.formato_numero(data[0].precio9, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecio10 = $rootScope.formato_numero(data[0].precio10, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MCosto = $rootScope.formato_numero(data[0].costo, 2, $rootScope._CD, $rootScope._CM, "");

                    $scope.Servicio.tablaprecios.MPrecioUSD1 = $rootScope.formato_numero(data[0].preciousd1, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecioUSD2 = $rootScope.formato_numero(data[0].preciousd2, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecioUSD3 = $rootScope.formato_numero(data[0].preciousd3, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecioUSD4 = $rootScope.formato_numero(data[0].preciousd4, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecioUSD5 = $rootScope.formato_numero(data[0].preciousd5, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecioUSD6 = $rootScope.formato_numero(data[0].preciousd6, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecioUSD7 = $rootScope.formato_numero(data[0].preciousd7, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecioUSD8 = $rootScope.formato_numero(data[0].preciousd8, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecioUSD9 = $rootScope.formato_numero(data[0].preciousd9, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MPrecioUSD10 = $rootScope.formato_numero(data[0].preciousd10, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.tablaprecios.MCostoUSD = $rootScope.formato_numero(data[0].costo, 2, $rootScope._CD, $rootScope._CM, "");
                    $scope.Servicio.desde = data[0].desde;
                    $scope.Servicio.hasta = data[0].hasta;

                    $scope.DescripcionMedida();

                }
            });

        } else {
            $scope.DescripcionMedida();
        }
    }

    $scope.DescripcionMedida = function () {
        if ($scope.Servicio.tipoprecio == 1) {
            General.LlamarAjax("Cotizacion", "opcion=MedidaIntervalo&id=" + $scope.Servicio.intervalo, false, "", 1).then(function (response) {
                $scope.Servicio.idmedida = response[0].id;
                $scope.Servicio.medida = response[0].medida;
                $scope.Servicio.dis_desde = false;
                $scope.Servicio.dis_hasta = false;
            });
        }
    }

    $scope.GuardarPrecio = function () {
        if ($rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecio1) == 0) {
            $("#Precio1").focus();
            swal("Acción Cancelada", "Debe de ingresar el valor del precio 1", "warning");
            return false;
        }

        if (($scope.Servicio.proveedor * 1 > 0) && ($rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MCosto) == 0)) {
            $("#MCosto").focus();
            swal("Acción Cancelada", "Debe de ingresar el valor del costo", "warning");
            return false;
        }

        $scope.Servicio.desde = $.trim($scope.Servicio.desde);
        $scope.Servicio.hasta = $.trim($scope.Servicio.hasta);

        if ($scope.Servicio.tipoprecio == 1) {
            if (($scope.Servicio.desde == "") || ($scope.Servicio.hasta == "")) {
                swal("Acción Cancelada", "Debe de ingresar el rango desde y hasta del intervalo de medida", "warning");
                return false;
            }
        }

        var parametros = "opcion=GuardarPrecios&Precio_Id=" + $scope.Servicio.idprecio + "&MTipoPrecio=" + $scope.Servicio.tipoprecio + "&MServicio=" + $scope.Servicio.servicio + "&MMagnitud=" + $scope.Servicio.magnitud + "&MEquipo=" + $scope.Servicio.equipo + "&MMedida=" + $scope.Servicio.idmedida + "&MMarca=" + $scope.Servicio.marca + "&MModelo=" + $scope.Servicio.modelo +
                "&MDesde=" + $scope.Servicio.desde + "&MHasta=" + $scope.Servicio.hasta + "&MProveedor=" + $scope.Servicio.proveedor + "&MPrecio1=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecio1) + "&MPrecio2=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecio2) + "&MPrecio3=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecio3) + "&MPrecio4=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecio4) + "&MPrecio5=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecio5) +
                "&MPrecio6=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecio6) + "&MPrecio7=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecio7) + "&MPrecio8=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecio8) + "&MPrecio9=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecio9) + "&MPrecio10=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecio10) + "&MCosto=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MCosto) +
                "&MPrecioUSD1=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecioUSD1) + "&MPrecioUSD2=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecioUSD2) + "&MPrecioUSD3=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecioUSD3) + "&MPrecioUSD4=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecioUSD4) + "&MPrecioUSD5=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecioUSD5) +
                "&MPrecioUSD6=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecioUSD6) + "&MPrecioUSD7=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecioUSD7) + "&MPrecioUSD8=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecioUSD8) + "&MPrecioUSD9=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecioUSD9) + "&MPrecioUSD10=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MPrecioUSD10) + "&MCostoUSD=" + $rootScope.NumeroDecimal($scope.Servicio.tablaprecios.MCostoUSD);
        General.LlamarAjax("Configuracion", parametros, true, "", 1).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                swal("", "Precio Actualizado con éxito", "success");
                $("#modalPrecios").modal("hide");
                $scope.BuscarPrecio();
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
        });
    }

    $scope.CalcularPreServ = function (caja) {
        if (caja) {
            $rootScope.ValidarTexto(caja, 3);
        }
        var precio = $rootScope.NumeroDecimal($scope.Servicio.precio) + $rootScope.NumeroDecimal($scope.Servicio.precioadic);
        var descuento = $rootScope.NumeroDecimal($scope.Servicio.descuento);
        $scope.Servicio.subtotal = ($scope.Servicio.cantidad * precio) - (($scope.Servicio.cantidad * precio) * descuento / 100)
    }

    $scope.DescripcionPreciosCo = function () {
        General.LlamarAjax("Cotizacion", "opcion=DescripcionPrecios", false, "", 1).then(function (response) {
            console.log(response);
            $scope.TituloPrecio = response;
        });
    }

    $scope.ActualizarPrecios = function (registro, index) {
        console.log(registro);
        $scope.Servicio.id = registro.id;
        $scope.Servicio.lote = registro.lote;
        $scope.Servicio.cantidad = registro.cantidad;
        $scope.Servicio.subtotal = registro.subtotal;
        $scope.Servicio.descuento = registro.descuento;
        $scope.Servicio.iva = registro.iva;
        $scope.Servicio.moneda = $.trim(registro.idmoneda);
        $scope.Servicio.posicion = index;
        $("#modalCambioPrecio").modal("show");
        $scope.Servicio.precio = $rootScope.formato_numero(registro.precio, 2, $rootScope._CD, $rootScope._CM, "");
        $scope.Servicio.descuento = $rootScope.formato_numero(registro.descuento, 2, $rootScope._CD, $rootScope._CM, "");
        $scope.Servicio.precioadic = $rootScope.formato_numero(registro.precioadic, 2, $rootScope._CD, $rootScope._CM, "");
        $scope.CalcularPreServ();
        console.log($scope.Servicio.equipo);
        if ($scope.Servicio.equipo == "" || $scope.Servicio.equipo == "0")
            $scope.Servicio.dis_precioadicional = true;
        else
            $scope.Servicio.dis_precioadicional = false;

    }

    $scope.CambiarPrecio = function () {
        var parametros = "opcion=ActualizarPrecio&cotizacion=" + $scope.Registro.cotizacion + "&precio=" + $rootScope.NumeroDecimal($scope.Servicio.precio) + "&precioadic=" + $rootScope.NumeroDecimal($scope.Servicio.precioadic) + "&descuento=" + $rootScope.NumeroDecimal($scope.Servicio.descuento) + "&id=" + $scope.Servicio.id + "&lote=" + $scope.Servicio.lote + "&moneda=" + $scope.Servicio.moneda;
        General.LlamarAjax("Cotizacion", parametros, true, "", 1).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                var index = $scope.Servicio.posicion;
                $("#modalCambioPrecio").modal("hide");
                swal("", datos[1], "success");
                $scope.Registro.tabla[index].precio = $rootScope.NumeroDecimal($scope.Servicio.precio);
                $scope.Registro.tabla[index].precioadic = $rootScope.NumeroDecimal($scope.Servicio.precioadic);
                $scope.Registro.tabla[index].descuento = $rootScope.NumeroDecimal($scope.Servicio.descuento);
                $scope.Registro.tabla[index].idmoneda = $scope.Servicio.moneda * 1;
                General.LlamarAjax("Configuracion", "opcion=Simbolo_Moneda&moneda=" + $scope.Servicio.moneda, false, "", 1).then(function (response) {
                    $scope.Registro.tabla[index].moneda = $.trim(response);
                });
                $scope.Registro.estado = "Temporal";
                $scope.Totales();
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
        });
    }

    $scope.ModalOServicio = function () {

        if (($scope.Registro.estado == 'Temporal' || $scope.Registro.estado == "Cotizado") && ($scope.Registro.cliente > 0)) {

            if ($scope.Registro.sede == "") {
                $("#Sede").focus();
                swal("Acción Cancelada", "Debe seleccionar la sede del cliente", "warning");
                return false
            }
            if ($scope.Registro.contacto == "") {
                $("#Contacto").focus();
                swal("Acción Cancelada", "Debe seleccionar el contacto del cliente", "warning");
                return false
            }

            if ($scope.Cliente.req_solicitud == "SI" && $scope.Cliente.Habitual == "NO" && $scope.Registro.tabla.length == 0 && $scope.Registro.solicitud == 0) {
                swal("Acción Cancelada", "Debe ingresar la solicitud del cliente", "warning");
                return false;
            }

            $scope.LimpiarOtroServicio();





            $("#ModalOtroServicio").modal("show");
        }
    }

    $scope.AgregarOtroServicio = function () {
        var mensaje = "";

        if ($.trim($scope.Servicio.observacion) == "") {
            $("#OObservacion").focus();
            mensaje = "<br> Debe de ingresar la observación " + mensaje;
        }

        if ($scope.Servicio.cantidad == 0) {
            $("#OCantidad").focus();
            mensaje = "<br> Debe de ingresar la cantidad del servicios " + mensaje;
        }

        if ($rootScope.NumeroDecimal($scope.Servicio.precio) == 0) {
            $("#OPrecio").focus();
            mensaje = "<br> Debe de ingresar el precio del servicios " + mensaje;
        }

        if ($.trim($scope.Servicio.otroservicio) == "") {
            $("#OServicio").focus();
            mensaje = "<br> Seleccione el servicio " + mensaje;
        }

        if ($.trim($scope.Servicio.ingreso) == "") {
            $("#OIngreso").focus();
            mensaje = "<br> Ingrese el número de ingreso" + mensaje;
        }

        if (mensaje != "") {
            swal("Acción Cancelada", mensaje, "warning");
            return false;
        }


        var parametros = "opcion=GuardarTmpCotizacion&cliente=" + $scope.Registro.cliente + "&id=" + $scope.Servicio.id + "&equipo=0&modelo=0" +
                "&observacion=" + General.escape($.trim($scope.Servicio.observacion)) + "&serie=&ingreso=" + $scope.Servicio.ingreso + "&cantidad=" + $scope.Servicio.cantidad + "&intervalo=0&intervalo2=0" +
                "&servicio=" + $scope.Servicio.otroservicio + "&metodo=0&punto=&calibracion=&declaracion=&nombreca=&moneda=" + $scope.Servicio.moneda +
                "&direccion=&certificado=&descuento=" + $rootScope.NumeroDecimal($scope.Servicio.descuento) + "&precio=" + $rootScope.NumeroDecimal($scope.Servicio.precio) + "&entrega=0&proveedor=0&iva=" + $rootScope.NumeroDecimal($scope.Servicio.iva) +
                "&express=0&sitio=0&cotizacion=" + $scope.Registro.id + "&remision=" + $scope.Registro.remision + "&sede=" + $scope.Registro.sede + "&contacto=" + $scope.Registro.contacto + "&idsolicitud=0&lote=0&costo=0&calibracionadic=&precioadic=0&articulo=0";

        General.LlamarAjax("Cotizacion", parametros, true, "", 1).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                $("#ModalOtroServicio").modal("hide");
                $scope.Registro.estado = "Temporal";
                swal("", datos[1], "success");
                $scope.TablaCotizacion(1);
            } else
                swal("Acción Cancelada", datos[1], "warning");
        });
    }

    $scope.ModalIngreso = function () {
        if (($scope.Registro.estado == 'Temporal' || $scope.Registro.estado == "Cotizado") && ($scope.Registro.cliente > 0)) {

            if ($scope.Registro.sede == "") {
                $("#Sede").focus();
                swal("Acción Cancelada", "Debe seleccionar la sede del cliente", "warning");
                return false
            }
            if ($scope.Registro.contacto == "") {
                $("#Contacto").focus();
                swal("Acción Cancelada", "Debe seleccionar el contacto del cliente", "warning");
                return false
            }

            if ($scope.Cliente.req_solicitud == "SI" && $scope.Cliente.Habitual == "NO" && $scope.Registro.tabla.length == 0 && $scope.Registro.solicitud == 0) {
                swal("Acción Cancelada", "Debe ingresar la solicitud del cliente", "warning");
                return false;
            }
        }

        $rootScope.Modal.tipo = 3;
        $rootScope.Modal.cliente = $scope.Cliente.id;
        $rootScope.Modal.tipo_ingreso = "cot";
        $rootScope.Modal.Open("Ingresos");
        $rootScope.Modal.CargarModal();
    }

    $scope.CambioFecha = function (consulta, tipo) {
        switch (consulta) {
            case 1:
                switch (tipo) {
                    case 1:
                        $scope.Consulta.fechad.id = General.Obtener_Fecha($scope.Consulta.fechad.value);
                        break;
                    case 2:
                        $scope.Consulta.fechah.id = General.Obtener_Fecha($scope.Consulta.fechah.value);
                        break;
                }
                break;
        }

    }

    $scope.ConsultarCotizaciones = function () {
        if ($scope.Consulta.fechad.id == "" || $scope.Consulta.fechah.id == "") {
            swal("Acción Cancelada", ValidarTraduccion("Debe de ingresar fecha de inicio y fecha final"), "warning");
            return false;
        }
        var parametros = "opcion=ConsultarCotizaciones&" + General.ConvertirParametros($scope.Consulta);
        General.LlamarAjax("Cotizacion", parametros, true, "", 0).then(function (response) {
            var datos = $.trim(response).split("||");
            $rootScope.TablaCotizaciones = new NgTableParams({count: 10}, {dataset: JSON.parse(datos[0])});

        });
    }

    $scope.ImprimirCotizacion = function (cotizacion, descargar) {
        $scope.Registro.Pdf_Cotizacion = "";
        if (cotizacion * 1 != 0) {
            var parametros = "opcion=GenerarPDF&tipo=Cotizacion&documento=" + cotizacion;
            General.LlamarAjax("Configuracion", parametros, true, "", 1).then(function (response) {
                var datos = $.trim(response).split("||");
                if (datos[0] == "0") {
                    if (descargar == 0)
                        window.open($rootScope.Conexion.url_archivo + "DocumPDF/" + datos[1]);
                    else {
                        $scope.Registro.Pdf_Cotizacion = datos[1];
                    }
                } else {
                    swal("Acción Cancelada", datos[1], "warning");
                }
            });
        }

    }

    $scope.AgregarOpcion = function (tipo, mensaje, id, servicio) {
        var magnitud = $scope.Servicio.magnitud * 1;
        var marca = (servicio == 1 ? $scope.Servicio.marca : $scope.Pesa.marca) * 1;
        switch (tipo) {
            case 1:
                if (magnitud == 0) {
                    $("#Magnitud").focus();
                    swal("Acción Cancelada", "Debe de seleccionar una magnitud", "warning");
                    return false;
                }
                break;
            case 3:
                if (marca == 0) {
                    if (servicio == 1)
                        $("#Marca").focus();
                    else
                        $("#MarcaPesa").focus();
                    swal("Acción Cancelada", "Debe de seleccionar una marca", "warning");
                    return false;
                }
                break;
        }
        swal({
            title: 'Agregar Opción',
            text: mensaje,
            input: 'text',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: $rootScope.ValidarTraduccion('Agregar'),
            cancelButtonText: $rootScope.ValidarTraduccion('Cancelar'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',

            inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                    if ($.trim(value)) {
                        var parametros = "opcion=GuardarOpcion&tipo=" + tipo + "&magnitud=" + magnitud + "&marca=" + marca + "&descripcion=" + value + "&tipoconcepto=0&json=1";
                        General.LlamarAjax("Cotizacion", parametros, true, "", 0).then(function (response) {
                            var datos = $.trim(response).split("|");
                            if (datos[0] != "XX") {
                                switch (tipo) {
                                    case 1:
                                        $scope.Combos.equipo = JSON.parse(datos[1]);
                                        $scope.Servicio.equipo = $.trim(datos[0]);
                                        $scope.MagnitudEquipo($scope.Servicio.equipo, "", "", "");
                                        break;
                                    case 2:
                                        if (servicio == 1) {
                                            $scope.Combos.marca = JSON.parse(datos[1]);
                                            $scope.Servicio.marca = $.trim(datos[0]);
                                            $scope.CargarModelos($scope.Servicio.marca, "", 1);
                                        } else {
                                            $scope.Combos.marca_pesa = JSON.parse(datos[1]);
                                            $scope.Pesa.marca = $.trim(datos[0]);
                                            $scope.CargarModelos($scope.Pesa.marca, "", 2);
                                        }
                                        break;
                                    case 3:
                                        if (servicio == 1) {
                                            $scope.Combos.modelo = JSON.parse(datos[1]);
                                            $scope.Servicio.modelo = $.trim(datos[0]);
                                        } else {
                                            $scope.Combos.modelo_pesa = JSON.parse(datos[1]);
                                            $scope.Pesa.modelo = $.trim(datos[0]);
                                        }
                                        break;
                                }
                                resolve();
                            } else {
                                reject(datos[1]);
                            }
                        });

                    } else {
                        reject(ValidarTraduccion('Debe de ingresar la descripción de ' + mensaje));
                    }
                })
            }
        }).then(function (result) {
            $("#" + id).focus();
            $scope.BuscarPrecio();
        })
    }

    $scope.GuardarCotizacion = function () {

        console.log($scope.Registro.tabla);

        if ($scope.Registro.cliente == 0 || $scope.Registro.tabla.length == 0) {
            alert("hola");
            ;
            return false;
        }

        if ($scope.Registro.estado != 'Temporal' && $scope.Registro.estado != "Cotizado") {
            swal("Acción Cancelada", "No se puede editar una cotización en estado " + $scope.Registro.estado, "warning");
            return false;
        }

        if ($scope.Registro.sede == "") {
            $("#Sede").focus();
            swal("Acción Cancelada", "Debe seleccionar la sede del cliente", "warning");
            return false
        }
        if ($scope.Registro.contacto == "") {
            $("#Contacto").focus();
            swal("Acción Cancelada", "Debe seleccionar el contacto del cliente", "warning");
            return false
        }
        if ($scope.Registro.lineanegocio == "") {
            $("#LineaNegocio").focus();
            swal("Acción Cancelada", "Debe seleccionar la línea de negocio de la cotización", "warning");
            return false
        }

        if ($scope.Cliente.req_solicitud == "SI" && $scope.Cliente.Habitual == "NO" && $scope.Registro.solicitud == 0) {
            swal("Acción Cancelada", "Debe ingresar la solicitud del cliente", "warning");
            return false;
        }

        if ($rootScope._TIPOCOTIZACION == 2) {

            if ($scope.Registro.forma_pago == "") {
                $("#FormaPago").focus();
                swal("Acción Cancelada", "Debe seleccionar la forma de pago del cliente", "warning");
                return false
            }

            if ($scope.Registro.plazo_entrega == "") {
                $("#PlazoEntrega").focus();
                swal("Acción Cancelada", "Debe seleccionar el plazo de entrega de los equipos/servicios", "warning");
                return false
            }

            if ($scope.Registro.condicion_entrega == "") {
                $("#CondicionEntrega").focus();
                swal("Acción Cancelada", "Debe seleccionar la condición de entrega de la cotización", "warning");
                return false
            }

            if ($scope.Registro.validez == "") {
                $("#Validez").focus();
                swal("Acción Cancelada", "Debe seleccionar la validez de la cotización", "warning");
                return false
            }
        }

        var canequipos = 0;
        for (var x = 0; x < $scope.Registro.tabla.length; x++) {
            if (($scope.Registro.tabla[x].precio == 0) && ($scope.Registro.tabla[x].otro * 1 != 3))
                canequipos += 1;
        }

        if (canequipos > 0) {
            swal("Acción Cancelada", "Hay " + canequipos + " servicios que debe de agregaragregar el precio", "warning");
            return false;
        }
        var parametros = "opcion=GuardarCotizacion&" + General.ConvertirParametros($scope.Registro);
        General.LlamarAjax("Cotizacion", parametros, true, "", 1).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                $scope.Registro.id = datos[3] * 1;
                $scope.Registro.cotizacion = datos[2] * 1;
                $scope.Cotizacion = $scope.Registro.cotizacion;
                $scope.Registro.estado = "Cotizado";
                $scope.ImprimirCotizacion($scope.Cotizacion, 0);
                swal("", datos[1] + " " + datos[2], "success");
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
        });
    }

    $scope.VerSolicitud = function () {

    }

    $scope.AgregarMetodo = function (tipo) {
        if (tipo == 1) {
            if ($scope.Servicio.equipo == "") {
                $("#Equipo").focus();
                swal("Acción Cancelada", "Debe seleccionar primero un equipo", "warning");
                return false;
            }
            $scope.Metodo.equipo = $("#Equipo option:selected").text();
            $scope.Metodo.metodo = "";
        } else {
            if ($scope.Pesa.equipo == "") {
                $("#EquipoPesa").focus();
                swal("Acción Cancelada", "Debe seleccionar primero un equipo", "warning");
                return false;
            }
            $scope.Metodo.equipo = $("#EquipoPesa option:selected").text();
            $scope.Metodo.metodo = "";
        }


        var parametros = 'opcion=TablaConfiguracion&tipo=9&id=0&estado=1';
        General.LlamarAjax("Configuracion", parametros, true, "", 1).then(function (response) {
            console.log(response);
            $rootScope.TablaMetodosCot = new NgTableParams({count: 5}, {dataset: response});
            $("#modalMetodo").modal("show");
        });
    }

    $scope.ObtenerMetodo = function (registro) {
        $scope.Metodo.metodo = registro.descripcion;
    }

    $scope.GuardarMetodo = function () {
        var parametros = "";

        if ($.trim($scope.Metodo.metodo) == "") {
            $("#MMetodo").focus();
            swal("Acción Cancelada", "Debe ingresar el método de calibración del equipo", "warning");
            return false;
        }
        var parametros = "opcion=GuardarMetodo&equipo=" + ($scope.TipoServicio == 1 ? $scope.Servicio.equipo : $scope.Pesa.equipo) + "&descripcion=" + General.escape($scope.Metodo.metodo);
        General.LlamarAjax("Cotizacion", parametros, true, "", 1).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                var metodo = $.trim(datos[1]);
                General.CargarCombo(17, ($scope.TipoServicio == 1 ? $scope.Servicio.equipo : $scope.Pesa.equipo)).then(function (response) {
                    if ($scope.TipoServicio == 1) {
                        $scope.Combos.metodo = response.data;
                        $scope.Servicio.metodo = metodo;
                    } else {
                        $scope.Combos.metodo_pesa = response.data;
                        $scope.Pesa.metodo = metodo;
                    }
                });
                $("#modalMetodo").modal("hide");
                if ($scope.TipoServicio == 1) {
                    $("#Metodo").focus();
                } else {
                    $("#MetodoPesa").focus();
                }
            } else {
                $("#MMetodo").focus();
                swal("Acción Cancelada", datos[1], "warning");
            }
        });
    }

    $scope.AgregarIntervalo = function () {
        if ($scope.Servicio.dis_ingreso == true)
            return false;
        if ($scope.Servicio.magnitud == "") {
            $("#Magnitud").focus();
            swal("Acción Cancelada", "Debe de seleccionar una magnitud", "warning");
            return false;
        }

        $scope.Intervalo.medida = "";
        $scope.Intervalo.desde = "";
        $scope.Intervalo.hasta = "";

        $("#modalIntervalo").modal("show");

    }

    $scope.GuardarIntervalo = function () {

        var mensaje = "";

        if ($.trim($scope.Intervalo.hasta) == "") {
            $("#IHasta").focus();
            mensaje = "<br> *Ingrese el rango hasta" + mensaje;
        }
        if ($.trim($scope.Intervalo.desde) == "") {
            $("#IDesde").focus();
            mensaje = "<br> *Ingrese el rango desde" + mensaje;
        }

        if ($scope.Intervalo.medida == "") {
            $("#IMedida").focus();
            mensaje = "<br> *Seleccione una unidad de medida" + mensaje;
        }

        if (mensaje != "") {
            swal("Verifique los siguientes campos", mensaje, "warning");
            return false;
        }

        var parametros = "opcion=GuardarIntervalo&magnitud=" + $scope.Servicio.magnitud + "&medida=" + General.escape($scope.Intervalo.medida) + "&desde=" + General.escape($scope.Intervalo.desde) + "&hasta=" + General.escape($scope.Intervalo.hasta);
        General.LlamarAjax("Cotizacion", parametros, true, "", 0).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                General.CargarCombo(6, $scope.Servicio.magnitud).then(function (response) {
                    $scope.Combos.intervalo = response.data;
                    $scope.Servicio.intervalo = $.trim(datos[1]);
                    $scope.Servicio.intervalo2 = "";
                    $("#modalIntervalo").modal("hide");
                });
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
        });
    }


    $scope.TablaClases = function () {

        $scope.Pesa.tabla_clase = "";
        if ($scope.Pesa.equipo == "" || $scope.Pesa.clase == "")
            return false;

        var parametros = "opcion=TablaClaseNominal&equipo=" + $scope.Pesa.equipo + "&clase=" + $scope.Pesa.clase + "&id=" + $scope.Pesa.id + "&precio=" + $scope.Cliente.tablaprecio;
        General.LlamarAjax("Cotizacion", parametros, true, "", 1).then(function (response) {
            $scope.Pesa.tabla_clase = response;
            $scope.TotalClase();
        });
    }

    $scope.CalcularPreClase = function (objeto, index) {
        $rootScope.ValidarTexto(objeto, 3);
        $scope.Pesa.tabla_clase[index].subtotal = $rootScope.NumeroDecimal($scope.Pesa.tabla_clase[index].cantidad) * $rootScope.NumeroDecimal($scope.Pesa.tabla_clase[index].precio);
        $scope.TotalClase();
    }
    $scope.TotalClase = function () {
        $scope.Pesa.cantidad = 0;
        $scope.Pesa.subtotal = 0;
        var descuento = $rootScope.NumeroDecimal($scope.Pesa.descuento);
        for (var x = 0; x < $scope.Pesa.tabla_clase.length; x++) {
            $scope.Pesa.cantidad += $rootScope.NumeroDecimal($scope.Pesa.tabla_clase[x].cantidad);
            $scope.Pesa.subtotal += $rootScope.NumeroDecimal($scope.Pesa.tabla_clase[x].subtotal);
        }
        $scope.Pesa.precio = $scope.Pesa.subtotal / $scope.Pesa.cantidad;
    }



    $scope.CargarCombos();
    $scope.InicializarRegistro();
    $scope.InicializarServicio();
    $scope.InicializarPesa();
    $rootScope.InicializarFormulario(2);
    $scope.DescripcionPreciosCo();
    $scope.setActiveTab(1);
    $scope.setActiveTabPre(1);
    $scope.setActiveSerTab(2);
    $("#Cliente").focus();



});

