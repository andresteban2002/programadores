﻿document.onkeydown = function (e) {

    var contenedor = $('#tabcotizacion a[href="#tabcrear"]');
    tecla = (document.all) ? e.keyCode : e.which;
    
    switch (tecla) {
        case 27:
            if ($("#Modal_Precios").css("display") == "block") {
                $("#Modal_Precios").modal("hide");
                return false;
            }
            break;
        case 112://F1
            ManualAyuda(3);
            return false;
            break;
        case 113://F2
            if ($("#Modal_Precios").css("display") == "block") {
                LimpiarPrecio();
                return false;
            }
            NuevoPrecio();
            return false;
            break;
        case 115://F4
            TablaPrecios();
            return false;
            break;
        case 117://F6
            SubirFotoPrecio();
            return false;
            break;
        case 121://F10
            $("#BtnGuardarPrecio").click();
            return false;
            break;
    }
}


var OpcionEquipo = 0;
var TipoPrecio = 0;
var TablaPrecio = null;
var Foto = 0;

$("#Magnitud, #MMagnitud").html(CargarCombo(1, 1));
$("#Equipo, #MEquipo").html(CargarCombo(58, 1));
$("#Marca, #MMarca").html(CargarCombo(3, 1));
$("#Proveedor, #MProveedor").html(CargarCombo(7, 7));
$("#Modelo, #MModelo").html(CargarCombo(5, 1, "", "-1"));
$("#Intervalo, #MIntervalo").html(CargarCombo(6, 1, "", "-1"));
$("#Servicio, #MServicio").html(CargarCombo(4, 1));
$("#MTipoPrecio").html(CargarCombo(36, 1));
$("#MMedida").html(CargarCombo(8, 1));

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Documento") {
            if ($.trim($(this).val()) != "") {

                BuscarCliente($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function DescripcionPrecios() {
    var datos = LlamarAjax('Configuracion','opcion=DescripcionPrecios');
    var data = JSON.parse(datos);
    for (var x = 0; x < data.length; x++) {
        $("#TitPrecio" + data[x].id).html(data[x].descripcion);
        $("#PLaPrecio" + data[x].id).html(data[x].descripcion);
    }
}

DescripcionPrecios();

function TablaPrecios() {

    ActivarLoad();
    setTimeout(function () {

        if (TablaPrecio != null)
            TablaPrecio.colReorder.reset();

        var codigo = $.trim($("#Codigo").val());
        var servicio = $("#Servicio").val() * 1;
        var magnitud = $("#Magnitud").val() * 1;
        var equipo = $("#Equipo").val() * 1;
        var marca = $("#Marca").val() * 1;
        var modelo = $("#Modelo").val();
        var proveedor = $("#Proveedor").val() * 1;

        var parametros = "id=0&codigo=" + codigo + "&servicio=" + servicio + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo + "&proveedor=" + proveedor;
        var datos = LlamarAjax('Configuracion','opcion=TablaPrecios&' + parametros);
        var datasedjson = JSON.parse(datos);
        DesactivarLoad();
        TablaPrecio = $('#TablaPrecios').DataTable({
            data: datasedjson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "eliminar" },
                { "data": "imagen" },
                { "data": "id" },
                { "data": "contador" },
                { "data": "servicio" },
                { "data": "magnitud" },
                { "data": "equipo" },
                { "data": "medida" },
                { "data": "desde" },
                { "data": "hasta" },
                { "data": "marca" },
                { "data": "modelo" },
                { "data": "proveedor" },
                { "data": "costo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio1", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio2", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio3", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio4", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio5", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio6", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio7", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio8", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio9", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio10", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') }
                
            ],
            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ],
            "colReorder": true
        });
    }, 15);
}

function CambiarCostoProveeedor(proveedor) {
    if (proveedor * 1 > 0) {
        $("#MCosto").val("");
        $("#MCosto").prop("readonly", false);
    } else {
        $("#MCosto").val("0");
        $("#MCosto").prop("readonly", true);
    }
}

function LimpiarPrecio() {
    $("#Precio_Id").val("0");
    $("#MCodigo").val("");
    $("#MTipoPrecio").val("").trigger("change");
    $("#MServicio").val("").trigger("change");
    $("#MMagnitud").val("").trigger("change");
    $("#MEquipo").val("").trigger("change");
    $("#MMarca").val("").trigger("change");
    $("#MModelo").val("").trigger("change");
    $("#MMedida").val("").trigger("change");
    $("#MDesde").val("");
    $("#MHasta").val("");
    $("#MProveedor").val("0").trigger("change");
    $("#MPrecio1").val("");
    $("#MPrecio2").val("");
    $("#MPrecio3").val("");
    $("#MPrecio4").val("0");
    $("#MPrecio5").val("0");
    $("#MPrecio6").val("0");
    $("#MPrecio7").val("0");
    $("#MPrecio8").val("0");
    $("#MPrecio9").val("0");
    $("#MPrecio10").val("0");
    TipoPrecio = 0;
        
    $("#MMarca").prop("disabled", true);
    $("#MModelo").prop("disabled", true);
    $("#MMedida").prop("disabled", true);
    $("#MDesde").prop("disabled", true);
    $("#MHasta").prop("disabled", true);
    
    $("#FotoPrecio").attr("src",url_cliente + "Adjunto/imagenes/equipos_patron.jpg");
    
}

function NuevoPrecio() {
    ActivarLoad();
    setTimeout(function () {
        LimpiarPrecio();
        DesactivarLoad();
        $("#Modal_Precios").modal("show");
    }, 15);
}

function CargarEquipos(magnitud) {
    magnitud = magnitud * 1;
    if (OpcionEquipo == 0) {
        if (magnitud * 1 > 0)
            $("#MEquipo").html(CargarCombo(2, 1, "", magnitud));
        else
            $("#MEquipo").html(CargarCombo(58, 1));
    }
    var datos = LlamarAjax("Cotizacion","opcion=TipoPrecio&magnitud=" + magnitud + "&servicio=0").split('|');
    if (datos[0] > 0)
        $("#MTipoPrecio").val(datos[0]).trigger("change");
    TipoPrecio = datos[0]*1;
    
}

function SeleccionarMagnitud(equipo) {
    if (equipo * 1 == 0)
        return false;
    var datos = LlamarAjax("Cotizacion","opcion=MagnitudEquipo&id=" + equipo);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        OpcionEquipo = 1;
        if (data[0].idmagnitud * 1 != $("#MMagnitud").val() * 1)
            $("#MMagnitud").val(data[0].idmagnitud).trigger("change");
        OpcionEquipo = 0;
    }
}

function CargarModelos(marca) {
    $("#MModelo").html(CargarCombo(5, 1, "", marca*1));
}

function CambioPrecios(precio) {
    precio = precio * 1;
    if (precio == 0)
        return false;
    
    $("#MMarca").prop("disabled", true);
    $("#MModelo").prop("disabled", true);
    $("#MMedida").prop("disabled", true);
    $("#MDesde").prop("disabled", true);
    $("#MHasta").prop("disabled", true);
    
    
    $("#MMarca").val("").trigger("change");
    $("#MModelo").val("").trigger("change");
    $("#MMedida").val("").trigger("change");
    $("#MDesde").val("");
    $("#MHasta").val("");
    
    switch (precio) {
        case 1:
            $("#MMedida").prop("disabled", false);
            $("#MDesde").prop("disabled", false);
            $("#MHasta").prop("disabled", false);
            break;
        case 2:
            $("#MMarca").prop("disabled", false);
            $("#MModelo").prop("disabled", false);
            break;
        case 4:
            $("#MMarca").prop("disabled", false);
            $("#MModelo").prop("disabled", false);
            break;
    }
}

$("#TablaPrecios > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    ActivarLoad();
    setTimeout(function () {
        var numero = row[2].innerText;
        var codigo = row[3].innerText;
        var parametros = "id=" + numero + "&codigo=&servicio=0&magnitud=0&equipo=0&intervalo=0&marca=0&modelo=0&proveedor=0";
        var datos = LlamarAjax('Configuracion','opcion=TablaPrecios&' + parametros);
        var data = JSON.parse(datos);
        Foto = data[0].fotos*1;
        if (Foto == 1)
            $("#FotoPrecio").attr("src", url_cliente + "Adjunto/imagenes/Precios/" + data[0].id + "/1.jpg?id=" + NumeroAleatorio());
        else
            $("#FotoEmpleado").attr("src", url_cliente + "Adjunto/imagenes/equipos_patron.jpg");

        $("#Precio_Id").val(data[0].id);
        $("#MCodigo").val(data[0].ncontador);
        $("#MTipoPrecio").val(data[0].tipoprecio).trigger("change");
        $("#MServicio").val(data[0].idservicio).trigger("change");
        $("#MEquipo").val(data[0].idequipo).trigger("change");
        if (data[0].idmarca * 1 > 0)
            $("#MMarca").val(data[0].idmarca).trigger("change");
        else
            $("#MMarca").val("").trigger("change");
        if (data[0].idmodelo * 1 > 0)
            $("#MModelo").val(data[0].idmodelo).trigger("change");
        else
            $("#MModelo").val("").trigger("change");
        if (data[0].idmedida * 1 > 0)
            $("#MMedida").val(data[0].idmedida).trigger("change");
        else
            $("#MMedida").val("").trigger("change");
        if (data[0].idproveedor * 1 > 0)
            $("#MProveedor").val(data[0].idproveedor).trigger("change");
        else
            $("#MProveedor").val("0").trigger("change");

        $("#MDesde").val(formato_numero(data[0].desde, _DE, _CD, _CM, ""));
        $("#MHasta").val(formato_numero(data[0].hasta, _DE, _CD, _CM, ""));

        $("#MCosto").val(formato_numero(data[0].costo, _DE, _CD, _CM, ""));
        $("#MPrecio1").val(formato_numero(data[0].precio1, _DE, _CD, _CM, ""));
        $("#MPrecio2").val(formato_numero(data[0].precio2, _DE, _CD, _CM, ""));
        $("#MPrecio3").val(formato_numero(data[0].precio3, _DE, _CD, _CM, ""));
        $("#MPrecio4").val(formato_numero(data[0].precio4, _DE, _CD, _CM, ""));
        $("#MPrecio5").val(formato_numero(data[0].precio5, _DE, _CD, _CM, ""));
        $("#MPrecio6").val(formato_numero(data[0].precio6, _DE, _CD, _CM, ""));
        $("#MPrecio7").val(formato_numero(data[0].precio7, _DE, _CD, _CM, ""));
        $("#MPrecio8").val(formato_numero(data[0].precio8, _DE, _CD, _CM, ""));
        $("#MPrecio9").val(formato_numero(data[0].precio9, _DE, _CD, _CM, ""));
        $("#MPrecio10").val(formato_numero(data[0].precio10, _DE, _CD, _CM, ""));

        DesactivarLoad();

        $("#Modal_Precios").modal("show");
    }, 15);
});


$("#FormGuardarPrecio").submit(function (e) {
    e.preventDefault();
    var proveedor = $("#MProveedor").val() * 1;
    var tipoprecio = $("#MTipoPrecio").val() * 1;
    var parametros = $(this).serialize().replace(/\%2C/g, "") + "&MTipoPrecio=" + tipoprecio;
    if ($("#MProveedor").prop("disabled") == false) {
        if (proveedor == 0) {
            $("#MProveedor").focus();
            swal("Acción Cancelada", "Debe de seleccionar un proveedor", "warning");
            return false;
        }
    }
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Configuracion","opcion=GuardarPrecios&" + parametros).split("|");
        DesactivarLoad();
        if (datos[0] == "0") {
            swal("", datos[1], "success");
            TablaPrecios();
            $("#Precio_Id").val(datos[2]);
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    }, 15);

});


function EliminarPrecio(id, codigo, magnitud, equipo, marca, modelo, proveedor) {
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar el preocio de código: ' + codigo, ' equipo: ' + equipo + ', marca: ' + marca + ', modelo: ' + modelo + ', proveedor: ' + proveedor  + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,
        
        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Configuracion" , "opcion=EliminarPrecio&Id=" + id)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            resolve()
                        }
                        else {
                            reject(datos[1]);
                        }
                            
                    })
            })
        }
    }]).then(function (data) {
        TablaPrecios();
    });
}

TablaPrecios();

function TipoServicioPrecio(servicio) {
    if (servicio * 1 == 0) {
        $("#MProveedor").val("0").trigger("change");
        $("#MProveedor").prop("disabled", true);
        $("#MCosto").prop("readonly", true);
        return false
    }

    var datos = LlamarAjax("Cotizacion","opcion=TipoPrecio&magnitud=0&servicio=" + servicio).split("|");
    precio = datos[0] * 1;
    if (precio == 3) {
        $("#MCosto").val("");
        $("#MProveedor").prop("disabled", false);
        $("#MCosto").prop("readonly", false);
    }
}

function SubirFotoPrecio() {
    var id = $("#Precio_Id").val() * 1;
    if (id > 0) {
        $("#EmpleadoFoto").html($("#MEquipo option:selected").text());
        $("#NumeroFoto").removeClass("hidden");
        $("#btnguardarfoto").attr("onclick", "GuardarFotoPrecio()");
        $("#btneliminarfoto").attr("onclick", "EliminarFotoPrecio()");
        $("#NumeroFoto").val("1").trigger("change");
        $("#modalImagenes").modal("show");
    } else {
        swal("Acción Cancelada", "Debe primero guardar el precio del servicio", "warning");
        return false;
    }
}


function EliminarFotoPrecio() {
    var id = $("#Precio_Id").val() * 1;
    var numero = $("#NumeroFoto").val() * 1;
    var parametros = "Precio=" + id + "&Numero=" + numero;
    var datos = LlamarAjax("Configuracion","opcion=EliminarFotoPrecio&" + parametros).split("|");
    if (datos[0] == "0") {
        if (numero == 1) {
            Foto = 0; 
            $("#FotoEmpleado").attr("src", url_cliente + "Adjunto/imagenes/equipos_patron.jpg");
            TablaPrecios();
        }
        swal("", datos[1], "success");
    } else
        swal("Acción Cancelada", datos[1], "warning");
}

function GuardarFotoPrecio() {

    var id = $("#Precio_Id").val() * 1;
    var numero = $("#NumeroFoto").val() * 1;
    var cropcanvas = $('#Art_Imagen').cropper('getCroppedCanvas');
    var croppng = cropcanvas.toDataURL("image/png");

    var ruta = cropcanvas.toDataURL();

    var x1 = $('#dataX').val() * 1;
    var y1 = $('#datay').val() * 1;
    var x2 = 0;
    var y2 = 0;
    var w = $('#dataWidth').val() * 1;
    var h = $('#dataHeight').val() * 1;

    $.ajax({
        type: 'POST',
        url: url + 'Configuracion/GuardarFotoPrecio',
        data: {
            documento: croppng,
            Precio: id,
            Numero: numero,
            x1: x1,
            y1: y2,
            x2: x2,
            y2: y2,
            w: w,
            h: h
        },
        success: function (datos) {
            datos = datos.split("|");
            if (datos[0] == "0") {
                if (numero == 1) {
                    $("#FotoPrecio").attr("src", url_cliente + "Adjunto/imagenes/Precios/" + id + "/" + numero + ".jpg?id=" + NumeroAleatorio());
                    Foto = 1;
                    TablaPrecios();
                }
                    
                swal("", datos[1], "success");
            } else
                swal("Acción Cancelada", datos[1], "warning");
        }
    });
}

function CambiarFotoPrecio() {
    var id = $("#Precio_Id").val() * 1;
    if (id == 0 || Foto == 0)
        return false;
    if (Foto == 1)
        Foto = 2
    else {
        if (Foto == 2)
            Foto = 3;
        else
            Foto = 1;
    }
    $("#FotoPrecio").attr("src", url_cliente + "Adjunto/imagenes/Precios/" + id + "/" + Foto + ".jpg?id=" + NumeroAleatorio());
}

function CambiarFotoModal(foto) {
    var id = $("#Precio_Id").val() * 1;
    $("#btndestroy").click();
    $("#Art_Imagen").attr("src", url_cliente + "Adjunto/imagenes/Precios/" + id + "/" + foto + ".jpg?id=" + NumeroAleatorio());
    init_cropper();
}


$("Art_Imagen").error(function() {
    $(this).attr("src", url_cliente + "Adjunto/imagenes/default.jpg");
});


function LlamarFotoPrecio(precio, fotos) {
    if (fotos == 1)
        fotos = 3
    CargarModalFoto(precio, fotos, 4);
}



$('select').select2();
document.getElementById('preloader').style.display = 'none';
