angular.module('myApp', ['ui', 'ngSanitize', 'ngTable']).controller('ControlConcursantes', function ($scope, $ocLazyLoad, $rootScope, General, NgTableParams) {

    function initTabs() {
        tabClasses = ["", "", "", "", "", "", ""];
    }

    $scope.getTabClass = function (tabNum) {
        return tabClasses[tabNum];
    };

    $scope.getTabPaneClass = function (tabNum) {
        return "tab-pane " + tabClasses[tabNum];
    }

    $scope.setActiveTab = function (tabNum) {
        initTabs();
        tabClasses[tabNum] = "active";
    };
    
	
	if ($rootScope.global.movil == 0){
		Webcam.set({
			width: 650,
			height: 510,
			dest_width: 650,
			dest_height: 510,
			image_format: 'jpeg',
			jpeg_quality: 100
		});
	}else{
		Webcam.set({
			width: 400,
			height: 480,
			dest_width: 780,
			dest_height: 960,
			image_format: 'jpeg',
			jpeg_quality: 100
		});           
	}
	
	Webcam.attach('#my_cameraperso');
	
	$scope.SubirFoto = function(){
		$("#modalFotoPer").modal("show");
	}
    
    $rootScope.InicializarFormulario(1);
    $scope.setActiveTab(1);
    
});

