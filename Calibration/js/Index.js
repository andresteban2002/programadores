
    
var Calidad = LlamarAjax("Actividades", "opcion=esCalidad").split("|");
if(parseInt(Calidad[0]) == 0 && String(Calidad[1]) == "true"){
    $("#revisionAct").html($("#revisionAct").html() + "<li><a href=\"javascript:LlamarOpcionMenu('General/Actividades',2,'ul_directortecnico')\">&nbsp;&nbsp;<i class='glyphicon glyphicon-open-file'></i>Revisión de actividades</a></li>");
}


if (localStorage.getItem("alerta_menu") === null) {
    localStorage.setItem("alerta_menu", 1);
}

if (localStorage.getItem("tiempo_sesion") === null) {
    localStorage.setItem("tiempo_sesion", 1);
}

if (localStorage.getItem("alerta_menu") == 0)
    $("#AlertasSistema").prop("checked", "");
else
    $("#AlertasSistema").prop("checked", "checked");
    


PermisosGenerales();
NotificacionesReporte();
NotificacionesCotiApro();
LlenarEstIngresos();
DatosEmpresa();
localStorage.setItem("bloqueopantalla", 0);
RecordatorioEvento();

$("#DivUsuarioChat").html('<h6 class="pull-left"><i class="icon-bubble6"></i>' + localStorage.getItem('usuario') + '<small>&nbsp;/&nbsp; usuario activo</small></h6>');
usuariochat = localStorage.getItem('codusu');
idusuariochat = localStorage.getItem('idusuario');
ComboUsuarioChat(usuariochat);
LlenarChat();
localStorage.setItem('ModuloCotizacion', 0);
$("#dptoCrear").html(CargarCombo(54, 1, "", ""));

function CambioTSesion() {
    
    if (localStorage.getItem("tiempo_sesion") * 1 == 1) {
        localStorage.setItem("tiempo_sesion", 0);
        $("#ContadorSesion").html("[DESAC]");
        $.jGrowl("Contador de Sesión Desactivado", { life: 2500, theme: 'growl-warning', header: '' });
    }
    else {
        localStorage.setItem("tiempo_sesion", 1);
        $.jGrowl("Contador de Sesión Activado", { life: 2500, theme: 'growl-success', header: '' });
        myMoveFunction();
    }
        
}

if (localStorage.getItem("tiempo_sesion") * 1 == 0) {
    $("#ContadorSesion").html("[DESAC]");
}

function CambioAlerta(activo) {
    if (activo)
        alerta_menu = 1;
    else
        alerta_menu = 0;

    localStorage.setItem("alerta_menu", alerta_menu);
}

setInterval(function () {
    if (localStorage.getItem("alerta_menu") == "1") {
        MantenSesion(1);
    }
}, 900000);


setInterval(function () {
    if (localStorage.getItem("alerta_menu") == "1")
        AlertaVueltas();
}, 400000);


setInterval(function () {
    if (PantallaBloqueada == 0)
        LlamarAjax("InicioSesion","opcion=MantenerSesion");
}, 180000);

var foto = $.trim(localStorage.getItem('foto'));
if (foto != "") {
    $(".fotoperfil").attr("src", url_cliente + "Adjunto/imagenes/FotoEmpleados/" + foto + "?id=" + NumeroAleatorio());
    
}

if ((localStorage.getItem('idioma') == null) || (localStorage.getItem('idioma') == "0")) {
    localStorage.setItem('idioma', 1);
}



setInterval(function () {
    TiempoSesion();
    ContadordeAviso();
    
}, 5000);
$("#myIP").html(localStorage.getItem("myIP") + "&nbsp;&nbsp;&nbsp;");
PermisosMenu();

$("#menu_codusu").html(localStorage.getItem('codusu'));
$("#AccUsuario").val(localStorage.getItem('codusu'));
$("#cambio_Usuario").val(localStorage.getItem('codusu'));
$("#menu_usuario").html(localStorage.getItem('usuario') + "<span>" + localStorage.getItem('cargo') + "</span>");
$("#menu_usuario2").html(localStorage.getItem('usuario') + "<br><small>" + localStorage.getItem('cargo') + "</small>");

init_socker();

var movil = 0;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    movil = 1;
}

if (movil == 0){
    Webcam.set({
        width: 650,
        height: 510,
        dest_width: 650,
        dest_height: 510,
        image_format: 'jpeg',
        jpeg_quality: 100
    });
}else{
	Webcam.set({
		width: 400,
		height: 480,
		dest_width: 780,
		dest_height: 960,
		image_format: 'jpeg',
		jpeg_quality: 100
	});           
}
   

var captures = [];
var capturesB64 = [];
var consActividadestpo = [];
window.addEventListener("paste", processEvent);
async function processEvent(e) {
	var reader = new window.FileReader();
	for (var i = 0; i < e.clipboardData.items.length; i++) {
		var clipboardItem = e.clipboardData.items[0];
		var type = clipboardItem.type;
		if (type.indexOf("image") != -1) {
			var blob = clipboardItem.getAsFile();
			var blobUrl = URL.createObjectURL(blob);
			await reader.readAsDataURL(clipboardItem.getAsFile());
			if (captures.length < 4) {
				captures.push(blobUrl);
				capturesB64.push(reader);
				for (var i = 0; i < 4; i++) {
					if ((captures[i] !== undefined && String(captures[i]) !== 'NaN') && document.getElementById("imagen_" + (i + 1)).getAttribute("src") !== undefined && String(document.getElementById("imagen_" + (i + 1)).getAttribute("src")) !== 'NaN') {
						document.getElementById("imagen_" + (i + 1)).setAttribute("src", captures[i]);
						$("#imagen_" + (i + 1)).removeClass("hidden");
						$("#icon_" + (i + 1)).addClass("hidden");
					}
				}
			}
		}
	}
}

    
$('#enviarDatosMT').click(function () {
	try {
            console.log($("#mesaAyuda").hasClass('active'), $("#Actividades").hasClass('active'));
            if ($("#mesaAyuda").hasClass('active')) {
                if (validarCamposNueva() == true) {
                    var usuarios = "/";
                    var incidencia = LlamarAjax("Quejas/IngresarIncidencia", "observacion=" + $("#DescripcionCrear").val()
                        + "&tipo=" + $("#tipoSolcCrear").val()
                        + "&priroridad=" + $("#prioriSolcCrear").val()
                        + "&img_a=" + capturesB64[0]
                        + "&img_b=" + capturesB64[1]
                        + "&img_c=" + capturesB64[2]
                        + "&img_d=" + capturesB64[3]
                        + "&dpto=" + $("#dptoCrear").val()).split("|");
                    if (Number(incidencia[0]) == 0) {
                        var JSONUSR = JSON.parse(incidencia[2]);
                        for (var i = 0; i < JSONUSR.length; i++) {
                            usuarios += JSONUSR[i].idusu + "/";
                        }
                        swal("", incidencia[1], "success");
                        var mensaje = "<b>Buen día... Nueva mesa de ayuda</b><br><br><b>" + $("#tipoSolcCrear").val() + "," + $("#prioriSolcCrear").val() + "</b><br>" +
                            $("#DescripcionCrear").val();
                        chat.server.send(usuariochat, mensaje, "", usuarios, incidencia[2]);
                        $.jGrowl(incidencia[1], { life: 1500, theme: 'growl-success', header: '' });
                        limpiarCamposNueva();
                    } else {
                        swal("Acción Cancelada", incidencia[1], "warning");
                    }
                }
            }
            if ($("#Actividades").hasClass('active')){
                var fechaIni = motActividad = nuDias = nuHoras = tiposAct = tipoActividad = actividades = "" ;
                motActividad = $("#porque").val();
                nuDias = $("#diasApli").val();
                nuHoras = $("#horasApli").val();
                fechaIni = $("#fechaIni").val();
                
                tipoActividad += "|";
                for (var i = 0; i < consActividadestpo.length; i++) {
                    if ($("#Check" + i + "0").prop('checked') == true) {
                        tipoActividad += $("#Check" + i + "0").val() + "|";
                    }
                }

                for (var i = 0; i <= filasAct; i++) {
                    if (String($("#mat_" + i + "1").val()) == "undefined" || ($("#mat_" + i + "1").val()).trim() == "" || (Number($("#mat_" + i + "2").val()) == 0 && Number($("#mat_" + i + "3").val()) == 0)) {
                        swal("Acción Cancelada", "Debes describir los pasos en los que completaras la actividad", "warning");
                        actividades = "";
                        break
                    } else {
                        actividades += "(::, '" + $("#mat_" + i + "1").val() + "', " + $("#mat_" + i + "2").val() + ", " + $("#mat_" + i + "3").val() + ")";
                    }
                }
                
                if (tipoActividad == "|" || tipoActividad == "")
                    swal("Acción Cancelada", "Debes selecionar el tipo de actividad que deseas realizar", "warning");

                if (motActividad.trim() == "")
                    swal("Acción Cancelada", "Debes describir el motivo por el cual quieres realizar la actividad", "warning");

                if (nuDias.trim() == "" && nuHoras.trim() == "")
                    swal("Acción Cancelada", "Debes ingresar el numero de dias y/o horas que te tomara la realizar la actividad", "warning");

                if (fechaIni.trim() == "")
                    swal("Acción Cancelada", "Debes indicar la fecha de inicio de la actividad, recuerda que debe haber una diferencia minima de 5 dias desde el momento que se hace la solicitud", "warning");


                if (motActividad.trim() !== "" && (Number(nuDias) > 0 || Number(nuHoras) > 0) && fechaIni.trim() !== "" && tipoActividad !== "|" && tipoActividad !== "" && actividades !== "") {
                    var regActividad = LlamarAjax("Actividades", "opcion=GuardarActividad&idActividad=" + tipoActividad
                        + "&motActividad=" + motActividad
                        + "&fechaIni=" + fechaIni
                        + "&dias=" + nuDias
                        + "&horas=" + nuHoras
                        + "&actividades=" + actividades).split("|");
                    if (regActividad[0] == 0) {
                        swal("", regActividad[1], "success");
                        sendMessage(3, "Tienes una nueva actividad por ser evaluada", "/" + regActividad[2] + "/");
                        $("#ModalMesaTrabajo").modal("hide");
                    } else {
                        swal("Acción Cancelada", regActividad[1], "warning");
                    }
                }
            }
        } catch (error) {
            console.log("Ha ocurrido un error en [cargarDatos]: " + error);
        }
});
        

async function obtenerArchivos() {
	var documento = document.getElementById("flAttachment");
	var file = documento.files[0];
	var data = new FormData();
	data.append('documento', (file == undefined ? null : file));
	var resultado = '';
	var direccion = url + "Quejas/AdjuntarArchivos";
	$.ajax({
		url: direccion,
		type: 'POST',
		contentType: false,
		data: data,
		processData: false,
		async: false,
		success: function (datos) {
			datos = datos.split("|");
			if (datos[0] != "0") {
				swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
				documento.value = "";
			} else {
				if (datos[2] != "XX")
					swal(ValidarTraduccion(datos[2]), "", "error");
			}
		}
	});
}

function cambiarTipo(value) {
	try {
		if (value == "Error de sistema") {
			$("#archivosCap").removeClass("hidden");
			$("#archivosCrear").addClass("hidden");
			obtenerArchivos();
			document.getElementById("flAttachment").value = "";
		}
		if (value == "Modulo nuevo" || value == "Mejora" || value == "Requerimiento") {
			$("#imagen_1, #imagen_2, #imagen_3, #imagen_4").attr("src", null);
			$("#imagen_1, #imagen_2, #imagen_3, #imagen_4").addClass("hidden");
			$("#icon_1, #icon_2, #icon_3, #icon_4").removeClass("hidden");
			$("#archivosCap").addClass("hidden");
			$("#archivosCrear").removeClass("hidden");
			captures = [];
			capturesB64 = [];
		}
	} catch (error) {
		console.log("Ha ocurrido un error en [cambiarTipo]: " + error);
	}
}

function mostrarImagen(imagen) {
	try {
		if (imagen !== undefined && String(imagen) !== 'NaN' && imagen.length > 0) {
			$("#img_mostrar").attr("src", imagen);
			$("#id_modal_imagenes").modal("show");

		}
	} catch (error) {
		console.log("Ha ocurrido un error en [mostrarImagen]: " + error);
	}
}

    
$('#cerr_mesa_ayuda').click(function () {
	try {
		if (validarCamposCierre() == true) {
			var incidencia = LlamarAjax("Quejas/CerrarIncidencia",
				"identificacion=" + $("#actividadesCerrar").val()
				+ "&conformidad=" + $("#conformidadCerrar").val()
				+ "&calificacion=" + $("#calificacionCerrar").val()
				+ "&accion=" + $("#accionCerrar").val()).split("|");
			if (Number(incidencia[0]) == 0) {
				var usuarios = "/";
				var JSONUSR = JSON.parse(incidencia[2]);
				for (var i = 0; i < JSONUSR.length; i++) {
					usuarios += JSONUSR[i].id_usuario_registro + "/";
				}
				swal("", incidencia[1], "success");
				var mensaje = "<b>Buen día... La mesa de ayuda <strong>" + $("#actividadesCerrar").val() + "</strong> en la que participaste ha sido cerrada</b><br><br><b>" + $("#accionCerrar").val() + "," + $("#calificacionCerrar").val() + "</b><br>" + $("#conformidadCerrar").val();
				chat.server.send(usuariochat, mensaje, "", usuarios);
				$.jGrowl(incidencia[1], { life: 1500, theme: 'growl-success', header: '' });
				limpiarCamposCierre();
			} else {
				swal("Acción Cancelada", incidencia[1], "warning");
			}
		}
	} catch (error) {
		console.log("Ha ocurrido un error en [cerrarIncidencia]: " + error);
	}
});
    

function seguimiento() {
	mostrarSeguimiento($("#actividadesCerrar").val());
}

//validaciones y extras

function removerCapt(captura) {
	try {
		captures.splice((captura - 1), 1);
		capturesB64.splice((captura - 1), 1);
		document.getElementById("imagen_" + captura).setAttribute("src", undefined);
		$("#imagen_" + captura).addClass("hidden");
		$("#icon_" + captura).removeClass("hidden");
	} catch (error) {
		console.log("Ha ocurrido un error en [removerCapt]: " + error);
	}
}

function limpiarCamposNueva() {
	$("#DescripcionCrear").val("");
	$("#tipoSolcCrear").val("0").change();
	$("#prioriSolcCrear").val("0").change();
	$("#dptoCrear").val("");
	$("#imagen_1, #imagen_2, #imagen_3, #imagen_4").removeAttr("src").addClass("hidden").change();
	$("#icon_1, #icon_2, #icon_3, #icon_4").removeClass("hidden").change();
	$("#ModalMesaTrabajo").modal("hide");
	$("#flAttachment").val("");
	captures = [];
	capturesB64 = [];
}

function limpiarCamposCierre() {
	$("#conformidadCerrar").val("");
	$("#calificacionCerrar").val("0").change();
	$("#accionCerrar").val("0").change();

	$("#actividadesCerrar").html(CargarCombo(73, 0, "", ""));

	$("#ModalCerrarMesaTrabajo").modal("hide");
}

function validarCamposCierre() {
	try {
		var conformidad = $("#conformidadCerrar").val();
		var actividad = $("#actividadesCerrar").val();
		var clasificacion = $("#calificacionCerrar").val();
		var accion = $("#accionCerrar").val();
		if (conformidad == "" && actividad == '' && clasificacion == "0" && accion == "0") {
			swal("Acción Cancelada", "Debe completar todos los campos para poder cerrar la actividad", "warning");
			return false;
		}
		if (conformidad == "" || actividad == '' || clasificacion == "0" || accion == "0") {
			swal("Acción Cancelada", "Debe completar todos los campos para poder cerrar la actividad", "warning");
			return false;
		}
		if (conformidad !== "" && actividad !== '' && clasificacion !== "0" && accion !== "0") {
			return true;
		}
	} catch (error) {
		console.log("Ha ocurrido un error en [validarCamposCierre]: " + error);
		return false;
	}
}

function validarCamposNueva() {
	try {
		var descripcion = ($("#DescripcionCrear").val()).trim();
		var tpoSolic = ($("#tipoSolcCrear").val()).trim();
		var prioridad = ($("#prioriSolcCrear").val()).trim();
		var departamento = ($("#dptoCrear").val()).trim();
		var textDep = ($('select[id="dptoCrear"] option:selected').text());

		if (descripcion == '' && tpoSolic == "0" && prioridad == "0" && departamento == "") {
			swal("Acción Cancelada", "Debe completar todos los campos para poder inscribir la mesa mesa de ayuda", "warning");
			return false;
		}
		if (descripcion == '' || tpoSolic == "0" || prioridad == "0" || departamento == "") {
			swal("Acción Cancelada", "Debe completar todos los campos para poder inscribir la mesa de ayuda" + (departamento !== "" ? " al departamento de " + textDep : ""), "warning");
			return false;
		}
		if (descripcion !== '' && prioridad !== "0" && departamento !== "") {
			if (tpoSolic == "Error de sistema" && (capturesB64.length < 1 || (capturesB64[0] == "" && capturesB64[1] == "" && capturesB64[2] && capturesB64[3]))) {
				swal("Acción Cancelada", "Debe insertar al menos un PRINT de pantalla para poder inscribir la mesa de ayuda" + (departamento !== "" ? " al departamento de " + textDep : ""), "warning");
				return false;
			}
			if ((tpoSolic == "Mejora" || tpoSolic == "Modulo nuevo") && document.getElementById("flAttachment").files.length < 1) {
				swal("Acción Cancelada", "Debe insertar al menos un archivo descriptivo para poder inscribir la mesa de ayuda" + (departamento !== "" ? " al departamento de " + textDep : ""), "warning");
				return false;
			}
			if (tpoSolic == "Requerimiento") {
				if (capturesB64[0] !== undefined && capturesB64[0].result !== undefined) {
					capturesB64[0] = capturesB64[0].result;
				} else {
					capturesB64[0] = '';
				}
				if (capturesB64[1] !== undefined && capturesB64[1].result !== undefined) {
					capturesB64[1] = capturesB64[1].result;
				} else {
					capturesB64[1] = '';
				}
				if (capturesB64[2] !== undefined && capturesB64[2].result !== undefined) {
					capturesB64[2] = capturesB64[2].result;
				} else {
					capturesB64[2] = '';
				}
				if (capturesB64[3] !== undefined && capturesB64[3].result !== undefined) {
					capturesB64[3] = capturesB64[3].result;
				} else {
					capturesB64[3] = '';
				}
				return true;
			}
			if (tpoSolic !== "0" && (capturesB64.length > 0 || document.getElementById("flAttachment").files.length > 0)) {
				if (capturesB64[0] !== undefined && capturesB64[0].result !== undefined) {
					capturesB64[0] = capturesB64[0].result;
				} else {
					capturesB64[0] = '';
				}
				if (capturesB64[1] !== undefined && capturesB64[1].result !== undefined) {
					capturesB64[1] = capturesB64[1].result;
				} else {
					capturesB64[1] = '';
				}
				if (capturesB64[2] !== undefined && capturesB64[2].result !== undefined) {
					capturesB64[2] = capturesB64[2].result;
				} else {
					capturesB64[2] = '';
				}
				if (capturesB64[3] !== undefined && capturesB64[3].result !== undefined) {
					capturesB64[3] = capturesB64[3].result;
				} else {
					capturesB64[3] = '';
				}
				return true;
			}
		}
	} catch (error) {
		console.log("Ha ocurrido un error en [validarCamposNueva] : " + error);
		return false;
	}
}
    
var quantitiy = 0;   
var filasAct = 0;   
var identAct = null;

$('#ModalMesaTrabajo').on('show.bs.modal', function () {
        var tiposActividades = LlamarAjax("Actividades", "opcion=ObtenerTiposActividades").split("|");
        console.log(tiposActividades);
        if (String(tiposActividades[0]) !== "undefined" && Number(tiposActividades[0]) == 0) {
            var tipos = JSON.parse(tiposActividades[1]), html = "";
            consActividadestpo = tipos;
            for (var i = 0; i < tipos.length; i++) {
                console.log("En for con i= ", i, " - ", tipos[i].descripcion);
                html += '<div class="form-check"><input type="checkbox" value="' + tipos[i].identificacion + '" class="form-check-input" id="Check' + i + '0"><label class="form-check-label" for="Check' + i + '0">' + tipos[i].descripcion + '</label> </div>';
            }
            var date = new Date();
            date.setDate(date.getDate() + 5);
            if(date.getDay() == 6)
                date.setDate(date.getDate() + 2);
                
            if(date.getDay() == 0)
                date.setDate(date.getDate() + 1);

            html += '<br/><div class="form-group">' +
                '<label>Indique la fecha de inicio</label>' +
                '<input type="date" class="form-control" id="fechaIni" min="' + date.getFullYear() + '-' + (Number(date.getMonth() + 1) < 10 ? '0' + Number(date.getMonth() + 1) : Number(date.getMonth() + 1)) + '-' + (parseInt(date.getDate()) > 9 ? date.getDate() : '0' + date.getDate() ) + '">' +
                '</div>' +
                '<div class="form-group">' +
                '<label>Indique los dias del periodo de aplicación</label>' +
                '<div class="input-group">' +
                '<input type="number" id="diasApli" class="form-control input-number" value="0" min="0" max="100">' +
                '</div>' +
                '<div class="form-group">' +
                '<label>Indique las horas del periodo de aplicación</label>' +
                '<div class="input-group">' +
                '<input type="number" id="horasApli" class="form-control input-number" value="0" min="0" max="100">' +
                '</div>' +
                '</div>';
            $("#planHacer").html(html);
        } else {
            swal("Acción Cancelada", incidencia[1], "warning");
            $("#Actividades").hide();
        }
    }); 

    function agregarActividad() {
        filasAct++;
        var lineaMatriz = '<tr id="mat_' + filasAct + '" class="tabcertr">' +
            '<th class="text-XX" ></th>' +
            '<th class="text-XX"><input type="text" class="form-control" id="mat_' + filasAct + '1"/></th>' +
            '<th class="text-XX"><input type="number" id="mat_' + filasAct + '2" class="form-control input-number" value="0" min="0" max="100" onchange="calcularHoras([' + filasAct + ',2])"></th>' +
            '<th class="text-XX"><input type="number" id="mat_' + filasAct + '3" name="mat_' + filasAct + '3" class="form-control input-number" value="0" min="0" max="100" onchange="calcularHoras([' + filasAct + ',3])"></th>' +
            '</tr>';

        var antMatriz = $("#bdyActividades").html();
        var splitMatriz = antMatriz.split('</tr>');
        for (var i = 0; i < splitMatriz.length; i++) {
            if (String(splitMatriz[i]).includes('mat_')) {
                var limSup = String(splitMatriz[i]).indexOf('mat_');

                if (limSup > -1)
                    limSup += 4;

                if ((String(splitMatriz[i]).substring(limSup, limSup + 2) * 1)) {
                    console.log("IF: ", (String(splitMatriz[i]).substring(limSup, limSup + 2)));
                } else {
                    console.log("ELSE: ", (String(splitMatriz[i]).substring(limSup, limSup + 2)));
                }
            }
        }     
        antMatriz += lineaMatriz;

        $("#bdyActividades").html(antMatriz);
    }
    
    function calcularHoras(elemento) {
        console.log("no manches wey: ", elemento)
        var diasHoras = parseInt($("#diasApli").val());
        var horas = parseInt($("#horasApli").val());
        var acumH = acumD = 0;
        if (!isNaN(diasHoras) && String(diasHoras) !== "undefinied" && Number(diasHoras) >= 0) {
            diasHoras = diasHoras * 24;
        }
        

        for (var i = 0; i <= filasAct; i++) {
            if (String($("#mat_" + i + "2").val()) !== "undefined" && !isNaN($("#mat_" + i + "2").val())) {
                acumD += Number($("#mat_" + i + "2").val());
            }

            if (String($("#mat_" + i + "3").val()) !== "undefined" && !isNaN($("#mat_" + i + "3").val())) {
                acumH += Number($("#mat_" + i + "3").val());
            }
        }

        acumD = acumD * 24;
        console.log((acumD + acumH), (diasHoras + horas));
        if ((acumD + acumH) > (diasHoras + horas)) {
            $("#mat_" + elemento[0] + String(elemento[1])).val(0);
            $.jGrowl("Ha excedido el numero de horas/dias de aplicación", { life: 1500, theme: 'growl-danger', header: '' });
        }
    }

    function abrirModalActivid() {
        $("#ModalSeguimientoActividades").modal("show");
        consActivCrono();
    }

    function consActivCrono() {
        var bodyHTML = '';
        let usrRoot = true;
        var buscarActCrono = LlamarAjax("Actividades", "opcion=BuscarActCrono&actividad=1").split("|");
        let proximaRev;
        var jsonGral = JSON.parse(buscarActCrono[1]);
        if (jsonGral.length > 0) {

            for (var i = 0; i < jsonGral.length; i++) {
                bodyHTML += '<tr onclick="slider(' + i + ', ' + jsonGral[i].id_actividad_info + ')">' +
                    '<th colspan="1" rowspan="1"></th>' +
                    '<th colspan="1" rowspan="1" width="70%">' + jsonGral[i].tx_actividad_motivo + '</th>' +
                    '<th colspan="1" rowspan="1" width="25%"><div class="progress">' +
                    '<div class="progress-bar bg-info" role="progressbar" aria-valuenow="' + jsonGral[i].promedio_activ_rea + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + jsonGral[i].promedio_activ_rea + '%"></div>' +
                    '</div>' +
                    '</th>' +
                    '</tr><tr>' +
                    '<th id="actividades' + i + '" colspan="3" rowspan="1" width="25%" style="display:none"><div class="form-group row" >';
                for (var j = 0; j < (jsonGral[i].actividades).length; j++) {
                    bodyHTML += '<div class="col-md-4">';
                    bodyHTML += '<label>' + (jsonGral[i].actividades)[j].actividad + '</label><br/>';

                    bodyHTML += '<input type="checkbox" value="' + parseInt((jsonGral[i].actividades)[j].chekeado) + '" class="form-check-input" ';
                    if (j > 0) {
                        if (parseInt((jsonGral[i].actividades)[j].chekeado) == 1) {
                            bodyHTML += ' checked disabled ';
                        }
                        if (parseInt((jsonGral[i].actividades)[j - 1].chekeado) == 0) {
                            bodyHTML += ' disabled ';
                        }
                        if(parseInt(jsonGral[i].usuario_root)==0){
                            bodyHTML += ' disabled ';
                            usrRoot = false;
                        }
                    } else {
                        if (parseInt((jsonGral[i].actividades)[j].chekeado) == 1) {
                            bodyHTML += ' checked disabled ';
                        }
                        if(parseInt(jsonGral[i].usuario_root)==0){
                            bodyHTML += ' disabled ';
                            usrRoot = false;
                        }
                    }
                    bodyHTML += ' onchange="finalizarActividad(' + (jsonGral[i].actividades)[j].identif + ', this)">';

                    if (j > 0) {
                        if (Number((jsonGral[i].actividades)[j].chekeado) == 0 && Number((jsonGral[i].actividades)[j - 1].chekeado) == 1) {
                            proximaRev = new Date((jsonGral[i].actividades)[j - 1].fe_verificacion);
                            proximaRev.setDate((proximaRev.getDate()) + parseInt((jsonGral[i].actividades)[j].dias_eval));
                            bodyHTML += '<br/><label>Fecha tope de revisión: ' + (proximaRev.getDate() < 10 ? '0' + proximaRev.getDate() : proximaRev.getDate()) + "/" + ((proximaRev.getMonth() + 1) < 10 ? '0' + (proximaRev.getMonth() + 1) : (proximaRev.getMonth() + 1)) + "/" + proximaRev.getFullYear() + '</label>';
                        }
                        if (Number((jsonGral[i].actividades)[j].chekeado) == 1) {
                            proximaRev = new Date((jsonGral[i].actividades)[j].fe_verificacion);
                            bodyHTML += '<br/><label>Fecha de revisión: ' + (proximaRev.getDate() < 10 ? '0' + proximaRev.getDate() : proximaRev.getDate()) + "/" + ((proximaRev.getMonth() + 1) < 10 ? '0' + (proximaRev.getMonth() + 1) : (proximaRev.getMonth() + 1)) + "/" + proximaRev.getFullYear() + '</label>';
                        }
                        if (Number((jsonGral[i].actividades)[j].chekeado) == 0 && Number((jsonGral[i].actividades)[j - 1].chekeado) == 0) {
                            proximaRev = new Date((jsonGral[i].actividades)[j - 1].fe_verificacion);
                            proximaRev.setDate((proximaRev.getDate()) + parseInt((jsonGral[i].actividades)[j].dias_eval));
                            bodyHTML += '<br/><label>Fecha tope de revisión: En espera</label>';
                        }
                    } else {
                        if (Number((jsonGral[i].actividades)[j].chekeado) == 0) {
                            proximaRev = new Date((jsonGral[i].actividades)[j].fecha_revision);
                            console.log((jsonGral[i].actividades)[j].fecha_revision, " - ", parseInt((jsonGral[i].actividades)[j].dias_eval), proximaRev.getDate());
                            proximaRev.setDate((proximaRev.getDate()) + parseInt((jsonGral[i].actividades)[j].dias_eval));
                            console.log("--", proximaRev);
                            bodyHTML += '<br/><label>Fecha tope de revisión: ' + (proximaRev.getDate() < 10 ? '0' + proximaRev.getDate() : proximaRev.getDate()) + "/" + ((proximaRev.getMonth() + 1) < 10 ? '0' + (proximaRev.getMonth() + 1) : (proximaRev.getMonth() + 1)) + "/" + proximaRev.getFullYear() + '</label>';
                        } else {
                            proximaRev = new Date((jsonGral[i].actividades)[j].fe_verificacion);
                            bodyHTML += '<br/><label>Fecha de revisión: ' + (proximaRev.getDate() < 10 ? '0' + proximaRev.getDate() : proximaRev.getDate()) + "/" + ((proximaRev.getMonth() + 1) < 10 ? '0' + (proximaRev.getMonth() + 1) : (proximaRev.getMonth() + 1)) + "/" + proximaRev.getFullYear() + '</label>';
                        }
                    }

                    bodyHTML += '<div class="progress" style="margin-bottom: 25px"><div class="progress-bar ' + (Number((jsonGral[i].actividades)[j].chekeado) == 0? (parseInt((jsonGral[i].actividades)[j].por_act) < 50 ? 'bg-success' : parseInt((jsonGral[i].actividades)[j].por_act) < 90 ? 'bg-warning' : 'bg-danger') : 'bg-success' )+ '" role="progressbar" aria-valuenow="' + parseInt((jsonGral[i].actividades)[j].por_act) + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + (j > 0 && Number((jsonGral[i].actividades)[j].chekeado) == 0 && Number((jsonGral[i].actividades)[j - 1].chekeado) == 0 ? 0 : parseInt((jsonGral[i].actividades)[j].por_act)) + '%"></div></div>';
                    bodyHTML += '</div>';
                }
                bodyHTML += '</div>';
                if (parseInt(jsonGral[i].promedio_activ_rea) == 100)
                    bodyHTML += '<div class="form-group"><div class="col-md-6"><label class="form-label" for="Check">Pasos de la verificación</label><textarea class="form-control" rows="5" id="pasosRigen" readonly>' + jsonGral[i].pasos + '</textarea></div><div class="col-md-6"><label class="form-label" for="Check">Observaciones de la verificación</label><textarea class="form-control" rows="5" id="observacionAct' + jsonGral[i].id_actividad_info + '" onblur="almacenarPasoslocal(' + jsonGral[i].id_actividad_info + ')">' + (String(localStorage.getItem("observacionAct"+ jsonGral[i].id_actividad_info)) !== "null" && String(localStorage.getItem("observacionAct"+ jsonGral[i].id_actividad_info)) !== ""? localStorage.getItem("observacionAct"+ jsonGral[i].id_actividad_info): '') + '</textarea></div></div>';

                bodyHTML += '</th>' +
                    '</tr>';
                    if(usrRoot == false){
                        $("#fiAct").addClass("collapse");
                    }else{
                        $("#fiAct").removeClass("collapse");
                    }
            }
        } else {
            bodyHTML += '<tr><th colspan="3" class="text-center">No hay actividades programadas</th></tr>';
        }
        
        $("#tBodySegC").html(bodyHTML);
    }

    function slider(i, act) {
        identAct = act;
        $("#actividades" + i).slideToggle("slow");
    }


    async function finalizarActividad(idActividad, elemento) {
        try {
            var bodyHTML = '';
            var confirmacion = await swal.queue([{
                title: ValidarTraduccion('Advertencia'),
                text: ValidarTraduccion('¿Está seguro que la actividad quie desea cerrar cumple con todo lo descrito para su realización?'),
                type: 'question',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                confirmButtonText: ValidarTraduccion('Si'),
                cancelButtonText: ValidarTraduccion('Cancelar'),
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        resolve(true)
                    })
                },
                preCancel: function () {
                    return new Promise(function (resolve, reject) {
                        resolve(false)
                    })
                }
            }]);
            if (confirmacion[0] != undefined && confirmacion[0] == true && parseInt(idActividad) > 0) {
                var buscarActCrono = LlamarAjax("Actividades", "opcion=marcarActividad&idActividad=" + idActividad).split("|");
                console.log(buscarActCrono);
                if (Number(buscarActCrono[0]) == 0) {
                    consActivCrono();
                    swal("", buscarActCrono[1], "success");
                } else {
                    elemento.checked = false;
                }
            }
        } catch (error) {
            elemento.checked = false;
            console.log("Ha ocurrido un error en [validarDatosBuscar]: " + error);
        }
    }

    function cerrarActividad() {
        if (identAct !== undefined && String(identAct) !== "undefined" && Number(identAct) > 0 && $("#observacionAct" + identAct).val().trim() !== "") {
            var finalizarAct = LlamarAjax("Actividades", "opcion=CerrarActividad&idActividad=" + identAct + "&observacion=" + $("#observacionAct" + identAct).val()).split("|");
            if (Number(finalizarAct[0]) == 0) {
                swal("", finalizarAct[1], "success");
                consActivCrono();
                localStorage.setItem("observacionAct"+ identAct, undefined);
            } else {
                swal("Accion cancelada", finalizarAct[1], "warning");
            }
        } else {
            swal("Accion cancelada", "Asegurese de ingresar todos los datos necesarios", "warning");
        }
    }

function almacenarPasoslocal(idActividad){
    localStorage.setItem("observacionAct"+ idActividad, $("#observacionAct" + idActividad).val());

}

function ContadordeAviso() {
	
	if (localStorage.getItem("TiempoAviso")*1 >= 216000){
		localStorage.setItem("TiempoAviso",5);
		$("#ModalLavarManos").modal({ backdrop: 'static' }, 'show');
		EscucharMensaje("El poder del lavado de manos para prevenir el coronavirus");
	}else{
		localStorage.setItem("TiempoAviso", (localStorage.getItem("TiempoAviso")*1)+5);
	}
		        
}

/*$("#ModalLavarManos").modal({ backdrop: 'static' }, 'show');
EscucharMensaje("El poder del lavado de manos para prevenir el coronavirus");*/
 






















