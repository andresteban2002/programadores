﻿//CargarFilas();


d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CFechaDesde, #CIFechaDesde").val(output);
$("#CFechaHasta, #CIFechaHasta").val(output2);

$("#MGrupo, #MenGrupo").html(CargarCombo(47, 1));
$("#MenMarca").html(CargarCombo(49, 1));
$("#MenIva").html(CargarCombo(35, 0));

$("#OIva").html(CargarCombo(55, 0));
var DataPunto = null;

var movil = 0;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    movil = 1;
}

var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(2, 254, 21)', 'rgb(6, 252, 245)', 'rgb(240, 20, 10)', 'rgb(254, 246, 2)'];

var grafica = document.getElementById('GraficaOC').getContext('2d');
var mygrafica = null;

var graficamag = document.getElementById('GraficaMMF').getContext('2d');
var mygraficamag = null;

var graficam = document.getElementById('GraficaM').getContext('2d');
var mygraficam = null;

var graficae = document.getElementById('GraficaE').getContext('2d');
var mygraficae = null;


var datos = LlamarAjax("Configuracion","opcion=CargaComboInicial&tipo=4").split("||");

$("#CMagnitud, #CIMagnitud").html(datos[0]);
$("#CEquipo, #CIEquipo").html(datos[1]);
$("#CMarca, #CIMarca").html(datos[2]);
$("#CCliente, #CICliente").html(datos[3]);
$("#CProveedor, #MenProveedor, #CIProveedor").html(datos[4]);
$("#CModelo, #CIModelo").html(datos[5]);
$("#CIntervalo, #CIIntervalo").html(datos[6]);
$("#CUsuario, #CIUsuario").html(datos[7]);


var IdProveedor = 0;
var totales = 0;
var Orden = 0;
var DocumentoPro = "";
var TotalSeleccion = 0;
var IdOrden = 0;
var Orden = 0;
var totalitems = 0;
var Estado = "Temporal";
var Pdf_Orden = "";

var totales = 0;
var subtotal = 0;
var descuento = 0;
var exento = 0;
var gravable = 0;
var iva = 0;
var CorreoEmpresa = "";

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function CargarOIngreso() {
    var cliente = $("#Cliente").val() * 1;
    if (cliente == 0)
        return false;
    localStorage.setItem("idcliente", cliente);
    CargarModalIngreso(1,"orden");
    $("#modalIngresoOrden").modal("show");
}

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");
        
        cb = parseInt($(this).attr('tabindex'));

        if (id == "Cliente") {
            if ($.trim($(this).val()) != "") {

                BuscarTercero($(this).val());
                $("#Sede").focus();
                return false;
            } 
        }
                
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}




function LimpiarTodo() {
    LimpiarDatos();
    $("#Proveedor").val("");
    $("#Proveedor").focus();
}


function LimpiarDatos() {

    IdProveedor = 0;
    DocumentoPro = "";
    Orden = 0;
    IdOrden = 0;
    totalitems = 0;
    Estado = "Temporal";

    $("#NombreProveedor").val("");
    $("#Orden").val("");
    $("#Direccion").val("");
    $("#Cotizacion").val("");
    $("#seleccionatodo").prop("checked", "");

    $("#Orden").attr("disabled", false);

    $("#Ciudad").val("");
    $("#Departamento").val("");

    $("#Email").val("");
    $("#Telefonos").val("");
    $("#TipoCliente").val("");

    $(".botones").removeClass("hidden");
    $(".botones2").removeClass("hidden");

    $("#bodyOrden").html("");
    totales = 0;
    subtotal = 0;
    descuento = 0;
    iva = 0;
    gravable = 0;

    $("#LugarEntrega").val("CALIBRATION SERVICE S.A.S. Carrera 69 a # 55 - 16 sur Bogotá D.C.");
    $("#DiasEntrega").val("");
    $("#Observacion").val("");
    $("#FormaPago").val("CONTADO").trigger("change");
    $("#Cliente").val("").trigger("change");

    $("#tsubtotal").val("0");
    $("#tdescuento").val("0");
    $("#tbaseimponible").val("0");
    $("#texcento").val("0");
    $("#tiva").val("0");
    $("#ttotalpagar").val("0");
        
        
}

function ValidarProveedor(documento) {
    if ($.trim(DocumentoPro) != "") {
        if (DocumentoPro != documento) {
            DocumentoPro = "";
            LimpiarDatos();
        }
    }
}


function BuscarProveedor(documento, busqueda) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoPro == documento)
        return false;

    DocumentoPro = documento;
    
    var parametros = "documento=" + $.trim(documento);
    var datos = LlamarAjax('Facturacion','opcion=BuscarProveedor&' + parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        var data = JSON.parse(datos[1]);
        DesactivarLoad();
        IdProveedor = data[0].id;
        CorreoEmpresa = data[0].email;
        $("#NombreProveedor").val(data[0].nombrecompleto);
        $("#Ciudad").val(data[0].ciudad + "/" + data[0].departamento);
        $("#Direccion").val(data[0].direccion);
        $("#Email").val(data[0].email);
        $("#Telefonos").val(data[0].telefono + " / " + data[0].celular);
        $("#Cliente").html(datos[2]);
        $("#Estado").val(Estado)
        if (busqueda == 1)
            TablaOrden(11);

    }else{
        DocumentoPro = "";
        $("#Proveedor").val("");
        $("#Proveedor").focus();
        swal("Acción Cancelada", "El documento " + documento + ", no se encuentra registrado como proveedor", "warning");
    }
    
}


function SeleccionarTodo() {

    if ($("#seleccionatodo").prop("checked")) {
        for (var x = 0; x < totalitems; x++) {
            $("#fila-" + x).addClass("bg-gris");
            $("#seleccionado" + x).prop("checked", "checked");
        }
    } else {
        for (var x = 0; x < totalitems; x++) {
            $("#fila-" + x).removeClass("bg-gris");
            $("#seleccionado" + x).prop("checked", "");
        }
    }

    CalcularTotal();
}

function SeleccionarFila(x) {

    if ($("#seleccionado" + x).prop("checked")) {
        $("#fila-" + x).addClass("bg-gris");
    } else {
        $("#fila-" + x).removeClass("bg-gris");
    }

    CalcularTotal();

}

function BuscarOrden(orden) {
    orden = $.trim(orden);
    if (orden == Orden)
        return false;
    if (orden == "") {
        LimpiarTodo();
        return false;
    }
    Orden = orden;
    $("#Orden").val(Orden);
    var datosorden = LlamarAjax("Facturacion","opcion=BuscarOrdenTercero&orden=" + Orden + "&IdProveedor=" + IdProveedor);
    if (datosorden == "[]") {
        LimpiarTodo();
        $("#Orden").focus();
        swal("Acción Cancelada", "Este número de órden de compra no se encuentra registrado", "warning");
        return false;
    }

    eval("dataorden=" + datosorden);
    $("#Proveedor").val($.trim(dataorden[0].documento));
    BuscarProveedor($.trim(dataorden[0].documento),0);
    var cliente = dataorden[0].idcliente;
    $("#Cliente").val(cliente).trigger("change");

    Orden = dataorden[0].orden;
    IdOrden = dataorden[0].id;
    Estado = dataorden[0].estado;
    if (Estado != "Registrado")
        $(".botones").addClass("hidden");
    if (Estado == "Anulado" || Estado == "Reemplazado")
        $(".botones2").addClass("hidden");
    $("#DiasEntrega").val(dataorden[0].diasentrega);
    $("#FormaPago").val(dataorden[0].plazopago).trigger("change");
    $("#LugarEntrega").val(dataorden[0].lugarentrega);
    $("#Observacion").val(dataorden[0].observacion);
    $("#Cotizacion").val(dataorden[0].numcotizacion);
    $("#Orden").val(Orden);

    $("#Estado").val(Estado);
    $("#Orden").prop("disabled", true);
    $("#seleccionatodo").prop("checked", "checked");
    SeleccionarTodo();
}

function TablaOrden(cliente) {

    if (IdProveedor == 0)
        return false;

    totales = 0;
    subtotal = 0;
    descuento = 0;
    gravable = 0;
    exento = 0;
    iva = 0;

    cliente = cliente * 1;

    SaldoActual(cliente, 1);

    var resultado = "";
    totalfila = 0;

    if (cliente == 0) {
        $("#bodyOrden").html("");
    } 
    $("#bodyOrden").html("");
    var datos = LlamarAjax("Facturacion","opcion=TablaOrdenTercer&tipo=2&cliente=" + cliente + "&orden=" + Orden + "&proveedor=" + IdProveedor);

    if (datos != "[]") {
        eval("data=" + datos);
        for (var x = 0; x < data.length; x++) {
            totalitems += 1;
            resultado += "<tr id='fila-" + x + "' " + (data[x].otro == 2 ? "ondblclick='EditarORegistro(" + data[x].id + ")'" : "") + ">" +
                "<td class='text-center'><b>" + (x + 1) + "</b><br><input class='form-control' type='checkbox' value='" + data[x].ingreso + "' name='Ingresos[]' id='seleccionado" + x + "' " + (data[x].recibidocome*1 == 0 ? " disabled " : "") + " onchange='SeleccionarFila(" + x + ")'></td>" +
                "<td><b>" + data[x].ingreso + "</b><br>" + data[x].fechaing + "<br>" + data[x].usuarioing + "</td>" +
                "<td>" + data[x].equipo + "</td>" +
                "<td>" + data[x].observacion + "</td>" +
                "<td align='center' class='text-XX'>" + (data[x].cotizacion ? "<a href='javascript:ImprimirCotizacion(" + data[x].cotizacion + ")'>" + data[x].cotizacion + "</a>" : "0") + "</td>" +
                "<td>" + data[x].servicio + "</label></td> " +
                "<td class='text-right text-XX'><input type='text' class='text-center sinborde' onfocus='FormatoEntrada(this,0)' onblur='FormatoSalida(this, 0)' onkeyup='CalcularPreTabla(this," + x + ")' disabled value='" + data[x].cantidad + "' id='tcantidad" + x + "' name='Cantidades[]' style='width:99%'</td>" +
                "<td class='text-right text-XX'><input type='text' class='text-right sinborde' onfocus='FormatoEntrada(this, 0)' onblur='FormatoSalida(this, 0)' onkeyup='CalcularPreTabla(this," +x + ")' value='" + formato_numero(data[x].precio, 0,_CD,_CM,"") + "' id='tprecio" + x + "' name='Precios[]' style='width:99%'></td>" +
                "<td class='text-right text-XX'><input type='text' class='text-right sinborde' onfocus='FormatoEntrada(this, 0)' onblur='FormatoSalida(this, 0)' onkeyup='CalcularPreTabla(this," + x + ")' value='" + data[x].descuento + "' id='tdescuento" + x + "' name='Descuentos[]' style='width:99%'></td>" +
                "<td class='text-right text-XX'><input type='text' class='text-center sinborde' disabled value='" + data[x].iva + "' id='tiva" + x + "' name='IVA[]' style='width:99%'</td>" +
                "<td class='text-right text-XX'><input type='text' class='text-right sinborde' disabled value='" + formato_numero(data[x].subtotal, 0,_CD,_CM,"") + "' id='tsubtotal" + x + "' name='Subtotales[]' style='width:99%'></td>" +
                "<td align='center'><input type='hidden' value='" + data[x].id + "' name='Ids[]'>" +  (data[x].otro == 2 ? "<button class='verfotos btn btn-danger' title='Eliminar Otro Registro' type='button' onclick=\"EliminarORegistro(" + data[x].id + ")\"><span data-icon='&#xe0d8;'></span></button></td>" :
                    "<button class='verfotos btn btn-success' title='Ver Fotos' type='button' onclick='CargarModalFoto(" + data[x].ingreso + "," + data[x].fotos + ",1)'><span data-icon='&#xe2c7;'></span></button>") +
                "<input type='hidden' value = '" + data[x].cotizacion + "' name='Cotizaciones[]'>" +
                "</td></tr>";

            subtotal += (data[x].precio * data[x].cantidad)
            descuento += (data[x].precio * data[x].cantidad * data[x].descuento / 100);
            iva += ((data[x].precio * data[x].cantidad) - (data[x].precio * data[x].cantidad * data[x].descuento / 100)) * data[x].iva / 100;
            if (data[x].iva * 1 > 0)
                gravable += (data[x].precio * data[x].cantidad) - (data[x].precio * data[x].cantidad * data[x].descuento / 100);
        }
    } 
        
    $("#bodyOrden").html(resultado);
    totales = subtotal - descuento + iva;
    exento = subtotal - gravable - descuento;
        
    $("#tsubtotal").val(formato_numero(subtotal, 0,_CD,_CM,""));
    $("#tdescuento").val(formato_numero(descuento, 0,_CD,_CM,""));
    $("#tbaseimponible").val(formato_numero(gravable, 0,_CD,_CM,""));
    $("#texcento").val(formato_numero(exento, 0,_CD,_CM,""));
    $("#tiva").val(formato_numero(iva, 0,_CD,_CM,""));
    $("#ttotalpagar").val(formato_numero(totales, 0,_CD,_CM,""));

}

function CalcularTotal() {

    totales = 0;
    subtotal = 0;
    descuento = 0;
    gravable = 0;
    exento = 0;
    iva = 0;

    var ingresos = document.getElementsByName("Ingresos[]");
    var precios = document.getElementsByName("Precios[]");
    var cantidades = document.getElementsByName("Cantidades[]");
    var subtotales = document.getElementsByName("Subtotales[]");
    var ivas = document.getElementsByName("IVA[]");

    for (var x = 0; x < ingresos.length; x++) {
                
        if (ingresos[x].checked) {
            subtotal += (NumeroDecimal(precios[x].value) * cantidades[x].value)
            descuento += (NumeroDecimal(precios[x].value) * cantidades[x].value) - NumeroDecimal(subtotales[x].value);
            iva += (NumeroDecimal(subtotales[x].value) * ivas[x].value / 100);
            if (ivas[x].value*1 > 0)
                gravable += NumeroDecimal(subtotales[x].value); 
        }
    }

    totales = subtotal - descuento + iva;
    totales = Math.round(totales);
    subtotal = Math.round(subtotal);
    descuento = Math.round(descuento);
    gravable = Math.round(gravable);
    exento = subtotal - gravable - descuento;
    iva = Math.round(iva);
        
    $("#tsubtotal").val(formato_numero(subtotal, 0,_CD,_CM,""));
    $("#tdescuento").val(formato_numero(descuento, 0,_CD,_CM,""));
    $("#tbaseimponible").val(formato_numero(gravable, 0,_CD,_CM,""));
    $("#texcento").val(formato_numero(exento, 0,_CD,_CM,""));
    $("#tiva").val(formato_numero(iva, 0,_CD,_CM,""));
    $("#ttotalpagar").val(formato_numero(totales, 0,_CD,_CM,""));

}

function EliminarORegistro(id) {
    var descripcion = LlamarAjax("Facturacion","opcion=DescripcionDetalleOrden&id=" + id);
    var cliente = $("#Cliente").val() * 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar el regitro <b>' + descripcion + '</b>?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Facturacion","opcion=EliminarOIngreso&id=" + id + "&orden=" + Orden + "&tipo=1")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            TablaOrden(cliente);
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);

    $(".swal2-content").html('¿Seguro que desea eliminar el regitro <b>' + descripcion + '</b>?');
} 

function EditarORegistro(id) {
    var datos = LlamarAjax("Facturacion","opcion=BuscarOIngreso&tipo=2&id=" + id + "&orden=" + Orden);
    var data = JSON.parse(datos);
    $("#IdRegistro").val(data[0].id);
    $("#ODescripcion").val(data[0].descripcion);
    $("#OCantidad").val(data[0].cantidad);
    $("#ODescuento").val(data[0].descuento);
    $("#OPrecio").val(formato_numero(data[0].precio, 0,_CD,_CM,""));
    $("#OIva").val(data[0].iva).trigger("change");
    
    $("#ModalOtroRegistro").modal("show");
        
} 

function VerOrdenCompra(orden) {
    window.open(url + "OrdenCompra/" + orden + ".pdf");
}

function CalcularPreTabla(caja,x) {
    ValidarTexto(caja, 1);
    var precio = NumeroDecimal($("#tprecio" + x).val()) * 1;
    var cantidad = $("#tcantidad" + x).val() * 1;
    var descuento = $("#tdescuento" + x).val() * 1;

    var subtotal = (precio * cantidad) - (precio * cantidad * descuento / 100);
        
    $("#tsubtotal" + x).val(formato_numero(subtotal, 0,_CD,_CM,""));

    CalcularTotal();

}

function CalcularPOSer(caja) {
    ValidarTexto(caja, 1);
    var cantidad = NumeroDecimal($("#OCantidad").val()) * 1;
    var precio = NumeroDecimal($("#OPrecio").val()) * 1;
    var descuento = NumeroDecimal($("#ODescuento").val()) * 1;
    var iva = $("#OIva").val() * 1;

    var subtotal = (cantidad * precio) - ((cantidad * precio) * descuento / 100);

    $("#OVSubTotal").val(formato_numero(subtotal, 0,_CD,_CM,""));
    $("#OVIva").val(formato_numero(subtotal * iva / 100, 0,_CD,_CM,""));
    $("#OVDescuento").val(formato_numero(cantidad * precio * descuento / 100, 0,_CD,_CM,""));
    $("#OVTotal").val(formato_numero(subtotal + (subtotal * iva / 100), 0,_CD,_CM,""));
}

function LimpiarOtroRegistro() {
    $("#IdRegistro").val("0");
    $("#ODescripcion").val("");
    $("#OCantidad").val("1");
    $("#ODescuento").val("0");
    $("#OPrecio").val("");
    $("#OIva").val("19").trigger("change");
    $("#OSubTotal").val("");
    $("#OVSubTotal").val("");
    $("#OVTotal").val("");
    $("#OVIva").val("");
    $("#OVDescuento").val("");
}

function ModalORegistro() {
    var cliente = $("#Cliente").val() * 1;

    if (IdProveedor == 0 || cliente == 0)
        return false;
    LimpiarOtroRegistro();
    $("#ModalOtroRegistro").modal("show");
}

$("#tablamodalingresoorden > tbody").on("dblclick", "tr", function (e) {

    var cliente = $("#Cliente").val() * 1;
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    var serie = row[4].innerText;
    var a_descripcion = $.trim(row[3].innerText).split("\n");
    var descripcion = "CALIBRACION DE " + a_descripcion[1] + ", MARCA:  " + a_descripcion[2] + ", MODELO: " + a_descripcion[3] +
        ", INTERVALO: " + a_descripcion[4] + ", SERIE: " + serie + ", INGRESO: " + numero;
    var parametro = "ingreso=" + numero + "&cliente=" + cliente + "&proveedor=" + IdProveedor + "&id=0&descripcion=" + _escape(descripcion) +
        "&cantidad=1&descuento=0&precio=0&subtotal=0&tipo=2";
    var datos = LlamarAjax("Facturacion","opcion=GuardarOtroRegistro&" + parametro + "&orden=" + Orden);
    datos = datos.split("|");
    if (datos[0] == "0") {
        $("#modalIngreso").modal("hide");
        Estado = "Temporal";
        $("#Estado").val(Estado);
        swal("", datos[1], "success");
        TablaOrden(cliente)
        IdRegistro = datos[2] * 1;
    } else
        swal("Acción Cancelada", datos[1], "warning");
});

$("#TablaModalArticulosOC > tbody").on("dblclick", "tr", function (e) {

    var cliente = $("#Cliente").val() * 1;
    var row = $(this).parents("td").context.cells;
    var descripcion = "CODIGO: " + row[2].innerText + ", TIPO: " + row[5].innerText + ", GRUPO: " + row[4].innerText +
        ", ARTICULO: " + row[6].innerText + ", MARCA: " + row[7].innerText + ", MODELO: " + row[8].innerText + ", PRESENTACION: " + row[9].innerText; 
    var parametro = "ingreso=0&cliente=" + cliente + " &proveedor=" + IdProveedor + " &id=0&descripcion=" + _escape(descripcion) +
        "&iva=" + row[12].innerText +   
        "&cantidad=1&descuento=0&precio=" + NumeroDecimal(row[10].innerText) + "&subtotal=" + NumeroDecimal(row[10].innerText) + "&tipo=2&articulo=" + row[1].innerText;
    var datos = LlamarAjax("Facturacion","opcion=GuardarOtroRegistro&" + parametro + "&orden=" + Orden);
    datos = datos.split("|");
    if (datos[0] == "0") {
        $("#modalArticulos").modal("hide");
        Estado = "Temporal";
        $("#Estado").val(Estado);
        swal("", datos[1], "success");
        TablaOrden(cliente)
        IdRegistro = datos[2] * 1;
    } else
        swal("Acción Cancelada", datos[1], "warning");
});

function AgregarOtroRegistro() {

    var id = $("#IdRegistro").val() * 1;
    var descripcion = _escape($.trim($("#ODescripcion").val()));
    var cantidad = $("#OCantidad").val() * 1;
    var descuento = $("#ODescuento").val() * 1;
    var iva = $("#OIva").val() * 1;
    var precio = NumeroDecimal($("#OPrecio").val()) * 1;
    var subtotal = NumeroDecimal($("#OVSubTotal").val()) * 1;
    var cliente = $("#Cliente").val() * 1;
            
    var mensaje = "";

    if ($.trim($("#ODescuento").val()) == "") {
        $("#ODescuento").focus();
        mensaje = "<br> Debe de ingresar el descuento del registro" + mensaje;
    }

    if (cantidad == 0) {
        $("#OCantidad").focus();
        mensaje = "<br> Debe de ingresar la cantidad del registro " + mensaje;
    }
    if (precio == 0) {
        $("#OPrecio").focus();
        mensaje = "<br> Debe de ingresar el precio del registro" + mensaje;
    }
    if (descripcion == "") {
        $("#ODescripcion").focus();
        mensaje = "<br> Debe de ingresar la descripción del registro " + mensaje;
    }
    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    descripcion = descripcion.replace(/&/g, '%26');
    
    var parametro = "ingreso=0&cliente=" + cliente + "&proveedor=" + IdProveedor + "&id=" + id + "&descripcion=" + descripcion +
        "&cantidad=" + cantidad + "&descuento=" + descuento + "&precio=" + precio + "&subtotal=" + subtotal + "&tipo=2" + "&iva=" + iva;
    var datos = LlamarAjax("Facturacion","opcion=GuardarOtroRegistro&" + parametro + "&orden=" + Orden);
    datos = datos.split("|");
    if (datos[0] == "0") {
        $("#ModalOtroRegistro").modal("hide");
        Estado = "Temporal";
        $("#Estado").val(Estado);
        swal("", datos[1], "success");
        TablaOrden(cliente)
        IdRegistro = datos[2] * 1;
    } else
        swal("Acción Cancelada", datos[1], "warning");
}
  
$("#tablamodalproveedore > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    $("#modalProveedores").modal("hide");
    $("#Proveedor").val(numero);
    BuscarProveedor(numero,1);
});

function GuardarOrden() {
    if (IdProveedor == 0)
        return false;
    var cliente = $("#Cliente").val() * 1;
    var diasentrega = $("#DiasEntrega").val() * 1;
    var cotizacion = $("#Cotizacion").val();
    var plazopago = $("#FormaPago").val();
    var lugarentrega = $.trim($("#LugarEntrega").val());
    var observacion = $.trim($("#Observacion").val());
    var mensaje = "";

    if (cliente == 0) {
        $("#Cliente").focus();
        swal("Acción Cancelada", "Debe seleccionar un cliente", "warning");
        return false;
    }

    if (lugarentrega == "") {
        $("#LugarEntrega").focus();
        mensaje = "<br> Debe de ingresar el lugar de entrega " + mensaje;
    }

    if ($.trim($("#Cotizacion").val()) == "") {
        $("#Cotizacion").focus();
        mensaje = "<br> Debe de ingresar la cotización del proveedor " + mensaje;
    }

    if (diasentrega == 0) {
        $("#DiasEntrega").focus();
        mensaje = "<br> Debe de ingresar los días de entrega " + mensaje;
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }
        
    var seleccionado = 0;
    var ingresos = document.getElementsByName("Ingresos[]");
    var precios = document.getElementsByName("Precios[]");
    var cantidades = document.getElementsByName("Cantidades[]");
    var descuentos = document.getElementsByName("Descuentos[]");
    var subtotales = document.getElementsByName("Subtotales[]");
    var ivas = document.getElementsByName("IVA[]");
    var cotizaciones = document.getElementsByName("Cotizaciones[]");
    var ids = document.getElementsByName("Ids[]");

    var a_id = "array[";
    var a_ingreso = "array[";
    var a_precio = "array[";
    var a_descuento = "array[";
    var a_subtotal = "array[";
    var a_iva = "array[";
    var a_cotizacion = "array[";
    var a_cantidad = "array[";

    for (var x = 0; x < ingresos.length; x++) {
        if (ingresos[x].checked) {
            if (a_ingreso == "array[") {
                a_ingreso += ingresos[x].value;
                a_id += ids[x].value;
                a_precio += NumeroDecimal(precios[x].value);
                a_descuento += NumeroDecimal(descuentos[x].value);
                a_subtotal += NumeroDecimal(subtotales[x].value);
                a_cotizacion += NumeroDecimal(cotizaciones[x].value);
                a_cantidad += NumeroDecimal(cantidades[x].value);
                a_iva += NumeroDecimal(ivas[x].value);
            } else {
                a_ingreso += "," + ingresos[x].value;
                a_id += "," + ids[x].value;
                a_precio += "," + NumeroDecimal(precios[x].value);
                a_descuento += "," + NumeroDecimal(descuentos[x].value);
                a_subtotal += "," + NumeroDecimal(subtotales[x].value);
                a_cotizacion += "," + NumeroDecimal(cotizaciones[x].value);
                a_cantidad += "," + NumeroDecimal(cantidades[x].value);
                a_iva += "," + NumeroDecimal(ivas[x].value);
            }
            seleccionado += 1;
        }
    }
    a_ingreso += "]";
    a_precio += "]";
    a_descuento += "]";
    a_subtotal += "]";
    a_cotizacion += "]";
    a_id += "]";
    a_cantidad += "]";
    a_iva += "]";

    if (seleccionado == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un ingreso", "warning");
        return false;
    }
        
    var parametros = "id=" + IdOrden + "&proveedor=" + IdProveedor + "&cliente=" + cliente + "&plazopago=" + plazopago + "&diasentrega=" + diasentrega + "&lugarenvio=" + lugarentrega +
        "&subtotal=" + subtotal + "&descuento=" + descuento + "&iva=" + iva + "&total=" + totales + "&observacion=" + observacion + "&cotizacion=" + cotizacion + "&gravable=" + gravable +
        "&a_ingreso=" + a_ingreso + "&a_precio=" + a_precio + "&a_descuento=" + a_descuento + "&a_subtotal=" + a_subtotal + "&a_cotizacion=" + a_cotizacion + "&a_id=" + a_id + "&a_cantidad=" + a_cantidad + "&a_iva=" + a_iva;
    var datos = LlamarAjax("Facturacion","opcion=GuardarOrdenTercero&" + parametros).split("|");
    if (datos[0] == "0") {
        swal("", datos[1] + " " + datos[2], "success");
        Orden = datos[2];
        IdOrden = datos[3];
        $("#Orden").val(Orden);
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

$("#TablaOrdenCompra > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $('#tabtercerizado a[href="#tabcrear"]').tab('show')
    BuscarOrden(numero);
});

function ImprimirOrden(numero, tipo) {
        
    if (numero == 0)
        numero = Orden;
    
    if (numero == 0)
        return false;

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=OrdenCompra&documento=" + numero).split("||");
        DesactivarLoad();
        if (datos[0] == "0") {
            if (tipo == 0)
                window.open(url_archivo + "DocumPDF/" + datos[1]);
            else {
                $("#resultadopdforden").attr("src", url_cliente + "DocumPDF/" + datos[1]);
                Pdf_Orden = datos[1];
            }
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function ReemplazarOrden() {
    if (Orden == 0)
        return false;

    if (Estado == "Anulado") {
        swal("Acción Cancelada", "No se puede reemplazar una Orden de Compra con estado " + Estado, "warning");
        return false;
    }

    var resultado = "";
    var resultado2 = "";
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Desea reemplzar la Orden de Compra') + ' ' + Orden + ' ' + ValidarTraduccion('del cliente') + ' ' + $("#Cliente option:selected").text(),
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Reemplazar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Facturacion","opcion=ReemplazarOrden&Orden=" + Orden + "&observaciones=" + value)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resultado = Orden;
                        resultado2 = datos[2];
                        resolve()
                    } else {
                        reject(datos[1]);
                    }
                } else {
                    reject(ValidarTraduccion('Debe de ingresar una observación'));
                }
            })
        }
    }).then(function (result) {
        LimpiarTodo();
        BuscarOrden(resultado2);
        swal({
            type: 'success',
            html: 'Orden de Compra Nro ' + resultado + ' reemplazada con éxito'
        })
    })
}

function AnularOrden() {
    if (Orden == 0)
        return false;

    if (Estado == "Anulado") {
        swal("Acción Cancelada", "No se puede anular una Orden con estado " + Estado, "warning");
        return false;
    }

    var resultado = "";
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Desea Anular la Orden de Compra') + ' ' + Orden + ' ' + ValidarTraduccion('del cliente') + ' ' + $("#Cliente option:selected").text(),
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Anular'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Facturacion","opcion=AnularOrden&Orden=" + Orden + "&observaciones=" + value)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resultado = Orden;
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar una observación'));
                }
            })
        }
    }).then(function (result) {
        LimpiarTodo();
        swal({
            type: 'success',
            html: 'Orden de Compra Nro ' + resultado + ' anulada con éxito'
        })
    })
}

function ConsularOrdenes() {


    var magnitud = $("#CMagnitud").val() * 1;
    var equipo = $("#CEquipo").val();
    var modelo = $("#CModelo").val();
    var marca = $("#CMarca").val() * 1;
    var intervalo = $("#CIntervalo").val();
    var ingreso = $("#CIngreso").val() * 1;
    var serie = $.trim($("#CSerie").val());

    var recepcion = $("#CRecepcion").val();

    var orden = $("#COrden").val() * 1;
    var cotizacion = $("#CSerie").val() * 1;
    var cliente = $("#CCliente").val() * 1;
    var proveedor = $("#CProveedor").val() * 1;
    var usuario = $("#CUsuario").val() * 1;

    var fechad = $("#CFechaDesde").val();
    var fechah = $("#CFechaHasta").val();

    if (fechad == "" || fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar fecha de inicio y fecha final"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "orden=" + orden + "&cotizacion=" + cotizacion + "&cliente=" + cliente + "&proveedor=" + proveedor + "&ingreso=" + ingreso +
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&usuario=" + usuario;
        var datos = LlamarAjax("Facturacion","opcion=ConsultarOrdenTercero&" + parametros).split("||");
        DesactivarLoad();

        var datajson = JSON.parse(datos[0]);
        var table = $('#TablaOrdenCompra').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "imprimir" },
                { "data": "orden", "className": "text-XX" },
                { "data": "proveedor" },
                { "data": "cliente" },
                { "data": "cantidad" },
                { "data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "iva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "departamento" },
                { "data": "ciudad" },
                { "data": "telefono" },
                { "data": "email" },
                { "data": "diasentrega" },
                { "data": "plazopago" },
                { "data": "numcotizacion" },
                { "data": "lugarentrega" },
                { "data": "estado" },
                { "data": "registro" },
                { "data": "envio" },
                { "data": "anulado" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });

        var subtotal = 0;
        var descuento = 0;
        var iva = 0;
        var total = 0;
        var anulado = 0;

        table.rows().data().each(function (datos, index) {
            if (datos.estado == "Anulado")
                anulado += datos.subtotal * 1;
            else {
                subtotal += datos.subtotal * 1;
                descuento += datos.descuento * 1;
                iva += datos.iva * 1;
                total += datos.total * 1;
            }
        });
        $("#TSubTotal").val(formato_numero(subtotal, 0,_CD,_CM,""));
        $("#TDescuento").val(formato_numero(descuento, 0,_CD,_CM,""));
        $("#TIva").val(formato_numero(iva, 0,_CD,_CM,""));
        $("#TTotal").val(formato_numero(total, 0,_CD,_CM,""));
        $("#TAnuladas").val(formato_numero(anulado, 0,_CD,_CM,""));

        //************************GRAFICAR*******************//

        var etiqueta = ['Subtotal', 'Descuento', 'IVA', 'Total', 'Facturado'];

        var config = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                responsive: true,
                tooltips: {
                    enabled: true
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                }
            }
        }

        if (datos[1] != "[]") {
            var data = JSON.parse(datos[1]);

            var newDataset1 = {
                label: etiqueta[0],
                backgroundColor: colores[0],
                data: [],
            };

            var newDataset2 = {
                label: etiqueta[1],
                backgroundColor: colores[1],
                data: [],
            };

            var newDataset3 = {
                label: etiqueta[2],
                backgroundColor: colores[2],
                data: [],
            };

            var newDataset5 = {
                label: etiqueta[4],
                backgroundColor: colores[4],
                data: [],
            };

            var newDataset4 = {
                type: 'line',
                label: etiqueta[3],
                fill: false,
                backgroundColor: colores[3],
                borderColor: colores[3],
                data: [],
            };

            tabla = "";
            tsubtotal = 0;
            tdescuento = 0;
            tfacturado = 0;
            tiva = 0;

            for (var x = 0; x < data.length; x++) {
                tsubtotal += data[x].subtotal;
                tdescuento += data[x].descuento;
                tiva += data[x].iva;
                tfacturado += data[x].facturado;
                porcentaje = data[x].descuento * 100 / data[x].subtotal;
                tabla += "<tr>" +
                    "<td>" + data[x].fecha + "</td>" +
                    "<td align='right'>" + formato_numero(data[x].subtotal, 0,_CD,_CM,"") + "</td>" +
                    "<td align='right'> <b>(" + formato_numero(porcentaje, _DE,_CD,_CM,"") + " %)</b> " + formato_numero(data[x].descuento, 0,_CD,_CM,"") + "</td>" +
                    "<td align='right'>" + formato_numero(data[x].iva, 0,_CD,_CM,"") + "</td>" +
                    "<td align='right'>" + formato_numero(data[x].subtotal - data[x].descuento + data[x].iva, 0,_CD,_CM,"") + "</td>" +
                    "<td align='right'>" + formato_numero(data[x].facturado, 0,_CD,_CM,"") + "</td></tr>";
                config.data.labels.push(data[x].fecha);

                newDataset1.data.push(data[x].subtotal);
                newDataset2.data.push(data[x].descuento);
                newDataset3.data.push(data[x].iva);
                newDataset4.data.push(data[x].subtotal - data[x].descuento + data[x].iva);
                newDataset5.data.push(data[x].facturado);

            }
            porcentaje = tdescuento * 100 / tsubtotal;
            tabla += "<tr>" +
                "<td align='right' class='text-info text-XX'>TOTALES</td>" +
                "<td align='right' class='text-info text-XX'>" + formato_numero(tsubtotal, 0,_CD,_CM,"") + "</td>" +
                "<td align='right' class='text-info text-XX'>(" + formato_numero(porcentaje, _DE,_CD,_CM,"") + " %) " + formato_numero(tdescuento, 0,_CD,_CM,"") + "</td>" +
                "<td align='right' class='text-info text-XX'>" + formato_numero(tiva, 0,_CD,_CM,"") + "</td>" +
                "<td align='right' class='text-info text-XX'>" + formato_numero(tsubtotal - tdescuento + tiva, 0,_CD,_CM,"") + "</td>" +
                "<td align='right' class='text-info text-XX'>" + formato_numero(tfacturado, 0,_CD,_CM,"") + "</td></tr>";
            $("#TablaOC").html(tabla);

            config.data.datasets.push(newDataset1);
            config.data.datasets.push(newDataset2);
            config.data.datasets.push(newDataset3);
            config.data.datasets.push(newDataset4);
            config.data.datasets.push(newDataset5);


            if (mygrafica != null) {
                mygrafica.destroy();
            }

            mygrafica = new Chart(grafica, config);
            mygrafica.update();
        }

    }, 15);
}

function ConsularOrdIngreso() {


    var magnitud = $("#CIMagnitud").val() * 1;
    var equipo = $("#CIEquipo").val();
    var modelo = $("#CIModelo").val();
    var marca = $("#CIMarca").val() * 1;
    var intervalo = $("#CIIntervalo").val() * 1;
    var serie = $.trim($("#CISerie").val());
    var cliente = $("#CICliente").val() * 1;
    var proveedor = $("#CIProveedor").val() * 1;
    var usuario = $("#CIUsuario").val() * 1;

    var fechad = $("#CIFechaDesde").val();
    var fechah = $("#CIFechaHasta").val();

    if (fechad == "" || fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar fecha de inicio y fecha final"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "cliente=" + cliente + "&proveedor=" + proveedor + "&fechad=" + fechad +
            "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&usuario=" + usuario;
        var datos = LlamarAjax("Facturacion","opcion=ConsultarIngresoOC&" + parametros);
        
        var datajson = JSON.parse(datos);
        $('#TablaOrdCompIngreso').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "orden" },
                { "data": "fecha" },
                { "data": "proveedor" },
                { "data": "cliente" },
                { "data": "ingreso" },
                { "data": "magnitud" },
                { "data": "equipo" },
                { "data": "marca" },
                { "data": "modelo" },
                { "data": "registro" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
        GraficarBarraMagMensual(parametros)
        GraficarBarraMagnitudes(parametros);
        GraficarBarraEquipos(parametros);
        DesactivarLoad();

    }, 15);
}

function BuscarPunto(magnitud, fecha) {
    var punto = 0;

    for (var x = 0; x < DataPunto.length; x++) {
        if (DataPunto[x].magnitud == magnitud && DataPunto[x].fecha == fecha) {
            punto = DataPunto[x].cantidad;
            return punto;
        }
    }
    return punto;
}

function GraficarBarraMagMensual(parametros) {

    var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)', 'rgb(104, 255, 51)', 'rgb(255, 60, 51)', 'rgb(8, 1, 0)', 'rgb(8, 89, 57)', 'rgb(105, 9, 102)'];
    var magnitudes = [];
    var fechas = [];
    var datos = LlamarAjax("Laboratorio","opcion=Magnitudes&graficar=1");
    var data = JSON.parse(datos);
    var encabezado = "<tr><th>Magnitud</th>";
    for (x = 0; x < data.length; x++) {
        magnitudes.push(data[x].descripcion);
    }
        
    datos = LlamarAjax("Facturacion","opcion=GraficaFechaOC&" + parametros);
    data = JSON.parse(datos);
    for (x = 0; x < data.length; x++) {
        encabezado += "<th>" + data[x].fecha + "</th>";
        fechas.push(data[x].fecha);
    }
    
    var config = {
        type: 'bar',
        data: {
            labels: fechas,
            datasets: []
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: ''
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            legend: {
                position: 'top',
                label: {
                    FontSize: '20px'
                }
            }
        },
        lineAtIndex: 2
    }

    var tabla = "";

    DataPunto = JSON.parse(LlamarAjax("Facturacion","opcion=GraficaFechaOCMag&" + parametros));
            
    for (var p = 0; p < magnitudes.length; p++) {
        var newDataset = {
            label: magnitudes[p],
            backgroundColor: colores[p],
            borderColor: colores[p],
            data: [],
        };
        var punto;

        tabla += "<tr>" +
            "<td>" + magnitudes[p] + "</td>";

        for (var i = 0; i < fechas.length; i++) {
            punto = BuscarPunto(magnitudes[p], fechas[i]);
            newDataset.data.push(punto);

            tabla += "<td align='right'>" + formato_numero(punto, 0,_CD,_CM,"") + "</td>";

        }
        tabla += "</tr>";
        config.data.datasets.push(newDataset);
    }

    if (mygraficamag != null) {

        mygraficamag.destroy();
    }

    mygraficamag = new Chart(graficamag, config);
    mygraficamag.update();
    $("#EncTablaMMF").html(encabezado);
    $("#TablaMMF").html(tabla);

}

function GraficarBarraMagnitudes(parametros) {

    var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)', 'rgb(104, 255, 51)', 'rgb(255, 60, 51)', 'rgb(8, 1, 0)', 'rgb(8, 89, 57)', 'rgb(105, 9, 102)'];
    var magnitudes = [];
    var datos = LlamarAjax("Laboratorio","opcion=Magnitudes&graficar=1");
    var data = JSON.parse(datos);
    for (x = 0; x < data.length; x++) {
        magnitudes.push(data[x].descripcion);
    }

    datos = LlamarAjax("Facturacion","opcion=GraficaOCMag&" + parametros);
    
    var etiqueta = ['Ingresos'];

    var config = {
        type: 'bar',
        data: {
            datasets: [],
            labels: []
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            responsive: true,
            tooltips: {
                enabled: true
            },
            legend: {
                position: 'top',
                display: (movil == 1 ? false : true)
            }
        }
    }

    if (datos != "[]") {
        var data = JSON.parse(datos);

        var newDataset1 = {
            label: etiqueta[0],
            backgroundColor: colores,
            data: [],
        };

       
        var tabla = "";
        var tcantidad = 0;
        
        for (var x = 0; x < data.length; x++) {
            tcantidad += data[x].cantidad;
            
            tabla += "<tr>" +
                "<td>" + data[x].magnitud + "</td>" +
                "<td align='right'>" + formato_numero(data[x].cantidad, 0,_CD,_CM,"") + "</td></tr>";
            config.data.labels.push(data[x].magnitud);
            newDataset1.data.push(data[x].cantidad);
        }
        
        tabla += "<tr>" +
            "<td align='right' class='text-info text-XX'>TOTALES</td>" +
            "<td align='right' class='text-info text-XX'>" + formato_numero(tcantidad, 0,_CD,_CM,"") + "</td></tr>";
        $("#TablaM").html(tabla);

        config.data.datasets.push(newDataset1);

        if (mygraficam != null) {
            mygraficam.destroy();
        }

        mygraficam = new Chart(graficam, config);
        mygraficam.update();
    }
}

function GraficarBarraEquipos(parametros) {

    var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)', 'rgb(104, 255, 51)', 'rgb(255, 60, 51)', 'rgb(8, 1, 0)', 'rgb(8, 89, 57)', 'rgb(105, 9, 102)'];
    var magnitudes = [];
    var datos = LlamarAjax("Laboratorio","opcion=Magnitudes&graficar=1");
    var data = JSON.parse(datos);
    for (x = 0; x < data.length; x++) {
        magnitudes.push(data[x].descripcion);
    }

    datos = LlamarAjax("Facturacion","opcion=GraficaOCEquipo&" + parametros);

    var etiqueta = ['Ingresos'];

    var config = {
        type: 'bar',
        data: {
            datasets: [],
            labels: []
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            responsive: true,
            tooltips: {
                enabled: true
            },
            legend: {
                position: 'top',
                display: (movil == 1 ? false : true)
            }
        }
    }

    if (datos != "[]") {
        var data = JSON.parse(datos);

        var newDataset1 = {
            label: etiqueta[0],
            backgroundColor: colores,
            data: [],
        };


        var tabla = "";
        var tcantidad = 0;
        var registros = 0;

        for (var x = 0; x < data.length; x++) {
            tcantidad += data[x].cantidad;
            registros++;

            tabla += "<tr>" +
                "<td>" + data[x].equipo + "</td>" +
                "<td align='right'>" + formato_numero(data[x].cantidad, 0,_CD,_CM,"") + "</td></tr>";
            if (registros <= 20) {
                config.data.labels.push(data[x].equipo);
                newDataset1.data.push(data[x].cantidad);
            }
        }

        tabla += "<tr>" +
            "<td align='right' class='text-info text-XX'>TOTALES</td>" +
            "<td align='right' class='text-info text-XX'>" + formato_numero(tcantidad, 0,_CD,_CM,"") + "</td></tr>";
        $("#TablaE").html(tabla);

        config.data.datasets.push(newDataset1);

        if (mygraficae != null) {
            mygraficae.destroy();
        }

        mygraficae = new Chart(graficae, config);
        mygraficae.update();
    }
}

function EnviarOrden() {
    if (IdOrden == 0) {
        return false;
    }

    if (Estado == "Anulado") {
        swal("Acción Cancelada", "No se puede enviar una órden de compra en estado anulado", "warning");
        return false;
    }

    if (CorreoEmpresa == "") {
        swal("Acción Cancelada", "Cliente no posee ningún correo principal registrado", "warning");
        return false;
    }

    $("#eProveedor").val($("#NombreProveedor").val());
    $("#eCorreo").val($("#Email").val());
    $("#ObserEnvio").val("");

    ImprimirOrden(Orden, 1);

    $("#sporden").html(Orden);

    $("#enviar_tercerizado").modal("show");
}

$("#formenviartercerizado").submit(function (e) {

    e.preventDefault();
    var observacion = $.trim($("#ObserEnvio").val())
    var correo = $.trim($("#eCorreo").val());
    a_correo = correo.split(";");

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#eCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo onválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    
    ActivarLoad();
    setTimeout(function () {
        var parametros = "&principal=" + CorreoEmpresa + "&e_email=" + correo + " &archivo=" + Pdf_Orden + "&id=" + IdOrden + "&observacion=" + observacion;
        var datos = LlamarAjax("Facturacion","opcion=EnvioOrdenCompra&" + parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            $("#enviar_tercerizado").modal("hide");
            Estado = "Enviado";
            $("#Estado").val(Estado);
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);

    return false;
});

function ModalArticulos() {

    var cliente = $("#Cliente").val() * 1;

    if (IdProveedor == 0 || cliente == 0)
        return false;

    $("#MenTipoInventario").val("").trigger("change");
    $("#MenGrupo").val("").trigger("change");
    $("#MenProveedor").val(IdProveedor).trigger("change");
    $("#MenPresentacion").val("");
    $("#MenMarca").val("").trigger("change");
    $("#MenIdArticulo").val("0");
    $("#MenCodigoBarras").val("");
    $("#MenUltimoCosto").val("0");
    $("#MenIva").val("1").trigger("change");
    
    for (var x = 1; x <= 10; x++) {
        $("#MenPrecio" + x).val("0");
    }

    $("#modalArticulos").modal("show");
}

function ModalNuevoArticulo() {
    $("#modalNuevoArticulo").modal("show");
}

function CargarModalArticulos() {

    var codigo = $.trim($("#MCodigo").val());
    var descripcion = $.trim($("#MDescripcion").val());
    var grupo = $("#MGrupo").val() * 1;

    var parametros = "codigo=" + codigo + "&descripcion=" + descripcion + "&grupo=" + grupo;
    var datos = LlamarAjax('Inventarios','opcion=ModalArticulos&' + parametros);
    var datajson = JSON.parse(datos);
    var tabla = $('#TablaModalArticulosOC').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        searching: false,
        columns: [
            { "data": "foto" },
            { "data": "id" },
            { "data": "codigointerno" },
            { "data": "codigobarras" },
            { "data": "grupo" },
            { "data": "tipo" },
            { "data": "equipo" },
            { "data": "marca" },
            { "data": "modelo" },
            { "data": "presentacion" },
            { "data": "ultimocosto", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "existencia" },
            { "data": "iva" },
            { "data": "proveedor" }
        ],
        "lengthMenu": [[5], [5]],
        "language": {
            "url": LenguajeDataTable
        }

    });
}

function MenCargarModelos(marca) {
    $("#MenModelo").html(CargarCombo(50, 1, "", marca));
}

function MenCargarEquipos(grupo) {
    $("#MenEquipo").html(CargarCombo(48, 1, "", grupo));
}

$("#FormAgregarArticulo").submit(function (e) {

    var equipo = $("#MenEquipo option:selected").text();
    var grupo = $("#MenGrupo option:selected").text();
    var marca = $("#MenMarca option:selected").text();
    var modelo = $("#MenModelo option:selected").text();
    var tipo = $("#MenTipoInventario option:selected").text();
    var presentacion = $("#MenPresentacion").val();
        
    e.preventDefault();
    var mensaje = "";
    var ultimocosto = NumeroDecimal($("#MenUltimoCosto").val());
    var iva = $("#MenIva").val() * 1;
    var parametros = $("#FormAgregarArticulo").serialize().replace(/\%2C/g, "") + "&Estado=1";
            
    if ($.trim($("#MenUltimoCosto").val()) == "")
        mensaje += "Debe de ingresar el valor del último costo <br>";
    for (var x = 1; x <= 10; x++) {
        if ($.trim($("#MenPrecio" + x).val()) == "")
            mensaje += "Debe de ingresar el valor del precio número " + x + " <br>";
        parametros += "&Precio" + x + "=" + NumeroDecimal($("#Precio" + x).val());
    }
    parametros += "&UltimoCosto=" + ultimocosto + "&Iva=" + iva;

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }
    var datos = LlamarAjax("Inventarios","opcion=GuardarArticulos&" + parametros).split("|");
    if (datos["0"] == "0") {
        var cliente = $("#Cliente").val() * 1;
        id = datos[2];
        codigo = datos[3];
        iva = datos[4];
        var descripcion = "CODIGO: " + codigo + ", TIPO: " + tipo + ", GRUPO: " + grupo +
            ", ARTICULO: " + equipo + ", MARCA: " + marca + ", MODELO: " + modelo + ", PRESENTACION: " + presentacion;
        var parametro = "ingreso=0&cliente=" + cliente + " &proveedor=" + IdProveedor + " &id=0&descripcion=" + descripcion +
            "&cantidad=1&descuento=0&precio=" + ultimocosto + "&subtotal=" + ultimocosto + "&tipo=2&articulo=" + id + "&iva=" + iva;
        var datos = LlamarAjax("Facturacion","opcion=GuardarOtroRegistro&" + parametro + "&orden=" + Orden);
        datos = datos.split("|");
        if (datos[0] == "0") {
            $("#modalArticulos").modal("hide");
            $("#modalNuevoArticulo").modal("hide");
            Estado = "Temporal";
            $("#Estado").val(Estado);
            swal("", "Artículo agregado con éxito", "success");
            TablaOrden(cliente)
            IdRegistro = datos[2] * 1;
        } else
            swal("Acción Cancelada", datos[1], "warning");

    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function MenCalcularPrecio() {
    var datos = LlamarAjax("Inventarios","opcion=DescripcionPrecio");
    var iva = NumeroDecimal($("#MenIva").val());
    var costo = NumeroDecimal($("#MenUltimoCosto").val());
    var data = JSON.parse(datos);
    var resultado = "";
    for (var x = 0; x < data.length; x++) {
        porcentaje = data[x].porcentaje;
        pos = x + 1;
        precio = (costo * porcentaje / 100) + costo;
        precioventa = precio * (1 + (iva / 100));
        precioventa = Math.round(precioventa / 100, 0) * 100;
        $("#MenPrecio" + pos).val(formato_numero(precioventa, 0,_CD,_CM,""));
    }
}

function MenAgregarOpcion(tipo, mensaje, id) {

    var marca = $("#MenMarca").val() * 1
    var grupo = $("#MenGrupo").val() * 1

    switch (tipo) {

        case 2:
            if (grupo == 0) {
                $("#Grupo").focus();
                swal("Acción Cancelada", "Debe de seleccionar un grupo", "warning");
                return false;
            }
            break;

        case 4:
            if (marca == 0) {
                $("#Marca").focus();
                swal("Acción Cancelada", "Debe de seleccionar una marca", "warning");
                return false;
            }
            break;
    }

    swal({
        title: 'Agregar Opción',
        text: mensaje,
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Agregar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if ($.trim(value)) {
                    var datos = LlamarAjax("Inventarios","opcion=GuardarOpcion&tipo=" + tipo + "&grupo=" + grupo + "&marca=" + marca + " &descripcion=" + value);
                    datos = datos.split("|");
                    if (datos[0] != "XX") {
                        $("#" + id).html(datos[1]);
                        $("#" + id).val(datos[0]).trigger("change");
                        resolve();
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar la descripción de ' + mensaje));
                }
            })
        }
    }).then(function (result) {
        $("#" + id).focus();
    })
}


var AutoORegistro = [];

function AutoCompletarOServicio() {
    var datos = LlamarAjax("Compras","opcion=AutoCompletoOServ")
    AutoORegistro.splice(0, AutoORegistro.length);
    var data = JSON.parse(datos);
    for (var x = 0; x < data.length; x++) {
        AutoORegistro.push(data[x].descripcion);
    }
}

function ImprimirCotizacion(cotizacion) {
	ActivarLoad();
    setTimeout(function () {
		if (cotizacion * 1 != 0) {
			var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=Cotizacion&documento=" + cotizacion).split("||");
			DesactivarLoad();
			if (datos[0] == "0"){
				window.open(url_archivo + "DocumPDF/" + datos[1]);
			}else{
				swal("Acción Cancelada",datos[1],"warning");
			}
		}
	},15);
}

AutoCompletarOServicio();


$("#ODescripcion").autocomplete({
    source: AutoORegistro
});
$("#ODescripcion").autocomplete("option", "appendTo", ".eventInsForm");


$('select').select2();
DesactivarLoad();

