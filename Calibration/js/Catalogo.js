﻿//CargarFilas();



$("#Menu_Empresa").html(localStorage.getItem("Empresa"));
urlfoto = localStorage.getItem("urlfoto");

$("#CMarca").html(CargarCombo(2, 1));
$("#PMarca").html($("#CMarca").html());


var decimales = 0;
var VendedorCalle = 0;
var CodigoSelFoto = "";
var a_Codigo;
var Cli_Correo;
var a_Producto;
var a_Iva;
var a_Descuento;
var a_DescuentoAnt;
var a_VUnitario;
var a_Cant;
var a_CantAnt;
var a_SubTotal;
var a_CantPedido;
var a_Exist;
var a_DescMax;
var a_VDescuento;
var a_Id;
var a_NoManifiesto;
var a_FechaManifiesto;
var a_Descripcion;
var a_CFab;
var a_Foto;
var a_Gravable;
var a_Excento;
var a_Peso;
var a_Pedido;
var a_Verificado;
var a_ExisInici;
var ValorInicial = 0;
var Estado = "";
var PedDespacho = 0;
var PedPlazo = 0;
var PedTipCliente = 0;
var SiglaMoneda = "";

var FormEsf = 0;
var FormCil = 0;
var FormAdi = 0;

var TipoEsf = 0;
var TipoCil = 0;
var TipoAdi = 0;


document.onkeydown = function (e) {
    tecla = (document.all) ? e.keyCode : e.which;
    
    switch (tecla) {
        case 112: //F1
            ModalClientes();
            return false;
            break;
        case 113: //F2
            FilaMalla = TotalFila - 1;
            $("#Codigo" + FilaMalla).focus();
            FBuscarMontura();
            return false;
            break;
        case 114: //F3
            FilaMalla = TotalFila - 1;
            $("#Codigo" + FilaMalla).focus();
            if (($.trim($("#NombreCliente").val()) != "") && (NFactura  == "") && ($("#Codigo" + FilaMalla).prop("disabled") == false))
                ModalMonturasFotos();
            return false;
            break;
        case 115: //F4
            MaximizarFoto();
            break;
        case 117: //F6
            EliminarFila();
            return false;
            break;
        case 118: //f7
            BuscarFila();
            return false;
            break;
        case 119: //f8
            VerEstadoCuenta();
            return false;
            break;
        case 120:
            CambioManifiesto();
            return false;
            break;

    }
}


var cod_moneda = 0;
var NPedido = 0;
var BPedido = 0;
var Pdf_Pedido = "";
var Pdf_Cartera = "";
var Vendedor = "";
var TipCliente = "";
var CaracCliente = 0;
var EstadoCli = "";
var CliDesMon = 0;
var CliDesVar = 0;
var CodCliente = 0
var factor_moneda = 1;
var TabMonPrecio = 0;
var TabVarPrecio = 0;
var OPcionesFacturar = "";

   
tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");
        if ((id == "Pedido") || (id == "Cliente"))
            return false;

        cb = parseInt($(this).attr('tabindex'));

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

var ErrorMalla = 0;
var FilaMalla = 0;
var TotalFila = 0;
var modal = 0;





function LimpiarTodo() {

    var Cliente = CodCliente;
    
    if ((NPedido == 0) && (TotalFila > 1)) {

        swal.queue([{
            title: 'Advertencia',
            text: '¿' + ValidarTraduccion('Desea eliminar los item de Artículos') + '?',
            type: 'question',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Si'),
            cancelButtonText: ValidarTraduccion('No'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
                return new Promise(function (resolve) {
                    $.post(url + "Facturacion/EliminarTemporal", "cliente=" + Cliente)
                        .done(function (data) {
                            swal.insertQueueStep(ValidarTraduccion("Artículos Eliminadas"));
                            resolve()
                        })
                })
            }
        }])
        Limpiar()
        CodCliente = 0;
        $("#Cliente").val("");
        $("#Cliente").focus();
        

    } else {
        
        Limpiar()
        CodCliente = 0;
        $("#Cliente").val("");
        $("#Cliente").focus();
        
    }
}

function Limpiar() {

    ErrorMalla = 0;
    FilaMalla = 0;
    TotalFila = 0;
    modal = 0;
    cod_moneda = 0;
    Vendedor = "";
    TipCliente = "";
    CaracCliente = 0;
    EstadoCli = "";
    CliDesMon = 0;
    factor_moneda = 1;
    TotalVarios = 0;
    SiglaMoneda = "";

    FormEsf = 0;
    FormCil = 0;
    FormAdi = 0;

    
    $("#NombreCliente").val("");
    $("#Direccion").val("");
    $("#Ciudad").val("");
    
    $("#btnenviar").addClass("hidden");
    
        
    $("#Telefono").val("");
        
       
    decimales = 0;

    
    if (BPedido == 0) {
        NPedido = 0;
        NFactura = 0;
        PedDespacho = 0;
        PedPlazo = 0;
        PedVenOfi = 0;
        PedVenCalle = 0;
        PedGuia = 0;
        PedTipCliente = 0;
        
        $("#Pedido").val("");
        $("#Factura").val("");
        $("#NFactura").val("");
        $("#Estado").val("");
        ValorInicial = 0;
        Estado = "";
        $("#tablamodaltoma tbody tr").remove(); 
        $("#Observaciones").val("");
        $("#Obsfactura").val("");
        $("#divAnulaciones").addClass("hidden");
        $('#firmapedido').removeAttr('src');

        $("#btnguardar1").addClass("hidden");
        $("#btnguardar2").addClass("hidden");
        $("#btnguardar3").addClass("hidden");
        $("#btnenviar").addClass("hidden");

    }

    BPedido = 0;
        
    Pdf_Cartera = "";
    Pdf_Pedido = "";

    LimpiarMalla();
}


function BuscarPedido(pedido) {
    pedido = pedido * 1;
    $("#Pedido").val(pedido);
    $("#modalConsultarPedidos").modal("hide");
    $("#modalConsultarFacturas").modal("hide");
    $("#modalConsultarRemisiones").modal("hide");
    if (pedido == 0) {
        Limpiar();
        $("#Cliente").val("");
        CodCliente = 0;
        $("#Cliente").focus();
        NPedido = 0;
        return false;
    }
    
    if (NPedido == pedido)
        return false;

    var datos = LlamarAjax(url + 'Facturacion/BuscarPedido', "pedido=" + pedido);
    if (datos != "[]") {
        eval("datacli=" + datos);
        NPedido = datacli[0].Numero;
        BPedido = NPedido;
        PedTipCliente = datacli[0].tipcli*1;
        PedDespacho = datacli[0].ForEnvio;
        PedPlazo = datacli[0].CodMontador;
        PedVenOfi = datacli[0].VendOfi;
        PedVenCalle = datacli[0].VendCalle;
        PedGuia = datacli[0].Guia;
        CodCliente = 0;
        $("#btnenviar").addClass("hidden")
        $("#Cliente").val(datacli[0].codcli);
        $("#Flete").val(formato_numero(datacli[0].Flete,decimales,".",",", SiglaMoneda));
        $("#Factura").val(datacli[0].NumFactura == 0 ? "" : datacli[0].NumFactura);
        $("#NFactura").val(datacli[0].NumFactura);
        $("#Estado").val(datacli[0].Estado);
        Estado = datacli[0].Estado;
        NFactura = datacli[0].NumFactura;
        $("#Orden").val(datacli[0].Orden);
        $("#Caja").val(datacli[0].Caja);
        $("#Observaciones").val(datacli[0].Observacion);
        $("#Obsfactura").val(datacli[0].ObsFactura);
        
        if ($.trim(datacli[0].obs_anulacion) != "") {
            $("#divAnulaciones").removeClass("hidden");
            $("#ObsAnulacion").html("Usuario que Anuló: " + datacli[0].usu_anula + ", Fecha: " + datacli[0].fecha_anula + ", Observación: " + datacli[0].obs_anulacion);
        }

        $("#Cliente").blur();
        ValorInicial = datacli[0].Valor * 1;

        //alert(Estado);

        if (Estado != "") 
            $("#btnenviar").removeClass("hidden")

        if (Estado == "Pendiente" || Estado == "Temporal" || Estado == "") {

            
            $("#btnguardar1").removeClass("hidden");
            $("#btnguardar2").removeClass("hidden");
            $("#btnanularped2").removeClass("hidden");
            $("#btnanularfac2").addClass("hidden");

            $("#btnguardar3").removeClass("hidden");
            $("#btnanularped3").removeClass("hidden");
            $("#btnanularfac3").addClass("hidden");

            $("#btnfacturar2").removeClass("hidden");
            $("#btnfacturar3").removeClass("hidden");


            $("#Flete").prop("disabled", false);
                        
                        
        } else {

                        
            $("#btnguardar1").addClass("hidden");
            $("#btnguardar2").addClass("hidden");
            $("#btnguardar3").addClass("hidden");

            
            $("#btnanularped2").removeClass("hidden");
            $("#btnanularped3").removeClass("hidden");
            $("#btnfacturar2").removeClass("hidden");
            $("#btnfacturar3").removeClass("hidden");
            
                                    
            $("#Flete").prop("disabled", true);
            $("#TipoRecibo").prop("disabled", true);
            
        }

                        
                
    } else {
        $.jGrowl(ValidarTraduccion("Pedido") + " " + pedido + " " + ValidarTraduccion("no registrado"), { life: 2500, theme: 'growl-warning', header: '' });
        Limpiar();
    }

}

function ValidarCantidadMalla() {
    for (var x = 0; x < TotalFila; x++) {
        if (($("#Descripcion" + x).val() != "") && ($("#Cant" + x).val() * 1 == 0)) {
            $("#Cant" + x).focus();
            swal("", "Escriba la unidades de la montura " + $("#Descripcion" + x).val(), "error");
            return x;
        }
    }
    return 0;
}


function GuardarPedido() {

    var tablaprecio = 1;
    
    if ($.trim($("#NombreCliente").val()) == "")
        return false;
    if (NumeroDecimal($("#ttotalpagar").val()) == 0) {
        swal("Debe seleccionar por lo mínimo una montura")
        return false;
    }

    if ($("#PedDespacho").val()*1 == 0) {
        swal("Debe seleccionar el tipo de despacho del pedido")
        return false;
    }

    if ($("#PedPlazo").val() * 1 == 0) {
        swal("Debe seleccionar el plazo de pago")
        return false;
    }
      

    var pos = ValidarCantidadMalla();
    if (pos != 0) {
        
        return false;
    }

    
       
    parametros = "Cliente=" + CodCliente + "&pedido=" + (NPedido*1)+"&TotalPagar=" + NumeroDecimal($("#ttotalpagar").val());
    parametros += "&Flete=" + NumeroDecimal($("#Flete").val()) * 1 + "&Base=" + NumeroDecimal($("#tbaseimponible").val());
    parametros += "&Iva=" + NumeroDecimal($("#tiva").val()) + "&Descuento=" + NumeroDecimal($("#tdescuento").val());
    parametros += "&FactorCambio=" + factor_moneda + "&Caja=" + $.trim($("#Caja").val()) + "&Orden=" + $.trim($("#Orden").val());
    parametros += "&Observaciones=" + $.trim($("#Observaciones").val()) + "&Obsfactura=" + $.trim($("#Obsfactura").val()) + "&VendCalle=" + VendedorCalle + "&Plazo=0&ForEnvio=0&VendOficina=0";
    parametros += "&Excento=" + NumeroDecimal($("#texcento").val()) + "&Guia=&TablaPrecio=" + tablaprecio;

    
    
    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var datos = LlamarAjax(url + "Facturacion/GuardarPedido", parametros);
        datos = datos.split("|");
        if (datos[0] == "1")
            swal("", ValidarTraduccion(datos[1]) + ' ' + ValidarTraduccion(datos[2]), "error")
        else {
            $("#Pedido").val(datos[2])
            NPedido = datos[2] * 1;
            swal("", ValidarTraduccion(datos[1]) + ' ' + ValidarTraduccion(datos[2]), "success")
            LimpiarMalla();
            TablaTemporalFacturacion(CodCliente, datos[2]);
            AddNuevaFila(TotalFila);
            $("#btnfacturar2").removeClass("hidden");
            $("#btnfacturar3").removeClass("hidden");
            
            $("#btnenviar").removeClass("hidden");
            $("#btnanularped2").removeClass("hidden");
            $("#btnanularped3").removeClass("hidden");
            if ((Estado == "") || (Estado == "Temporal")) {
                $("#Estado").val(ValidarTraduccion("Pendiente"));
                Estado = "Pendiente";
            }
        }
        document.getElementById('preloader').style.display = 'none';
    }, 15);
    
}


function BuscarCliente(codigo) {

    if (codigo * 1 == CodCliente) {
        return false;
    }

    CodCliente = codigo * 1;
    Limpiar();
    if (CodCliente == 0) {
        return false;
    }

    document.getElementById('preloader').style.display = 'block';

    setTimeout(function () {

        var parametros = "codigo=" + codigo+"&web=1";
        var datos = LlamarAjax(url + 'Facturacion/BuscarCliente', parametros);
        datos = datos.split("|");

        if (datos[0] == "1") {
            $("#Cliente").select();
            $("#Cliente").focus();
            document.getElementById('preloader').style.display = 'none';
            swal("", ValidarTraduccion(datos[1]), "warning");
            CodCliente = 0;
            
            return false;
        }

        if (datos[1] != "[]") {

            eval("data=" + datos[1]);
            
            TipCliente = (PedTipCliente == 0 ? data[0].TipCli * 1 : PedTipCliente);
            CaracCliente = data[0].CliCodCar * 1;
            decimales = data[0].decimales;
            EstadoCli = data[0].CliEstado*1;
            CliDesMon = data[0].DescMontura * 1;
            Cli_Correo = data[0].CliMail;
            $("#NombreCliente").val(data[0].NomRazSoc);
            $("#Direccion").val(data[0].CliDesDir ? data[0].CliDesDir : "");
            $("#Ciudad").val(data[0].NomCiu ? data[0].NomCiu : "");
            $("#Telefono").val(data[0].CliDesTel + "  " + (data[0].CliDEsFax ? data[0].CliDEsFax : ""));
                        
            
            if (TipCliente == 2 || TipCliente == 5) {
                if (EstadoCli == 0) {
                    CodCliente = 0;
                    Limpiar();
                    swal("Accion Cancelada", data[0].CliDesEst,"warning")
                }
            }      
            
        } 
        document.getElementById('preloader').style.display = 'none';
    }, 15);
}

function GuardarTemporal(fila) {

    var cant = $("#Cant" + fila).val() * 1;
    var cant_a = $("#CantAnt" + fila).val() * 1;

    if ($("#Codigo" + fila).val() * 1 == 0)
        return;

    if (isNaN(cant)) {
        $("#Cant" + fila).val(cant_a);
        
        return false;
    }
        
    
    var pedido = NPedido;

    var desc = $("#Descuento" + fila).val() * 1;
    var desc_a = $("#DescuentoAnt" + fila).val() * 1;

    if (desc == 0)
        $("#Descuento" + fila).val("0");
    
    if ((cant != cant_a) || (desc != desc_a)) {
                        
        parametros = "id=" + ($("#Id" + fila).val() * 1) + "&Cliente=" + CodCliente + "&fila=" + fila;
        parametros += "&Producto=VAR&Codigo=" + $("#Codigo" + fila).val() + "&Descripcion=" + $("#Descripcion" + fila).val();
        parametros += "&VUnitario=" + NumeroDecimal($("#VUnitario" + fila).val()) + "&Descuento=" + ($("#Descuento" + fila).val() * 1) + "&Iva=" + ($("#Iva" + fila).val() * 1) + "&SubTotal=" + NumeroDecimal($("#SubTotal" + fila).val()) + "&Cant=" + cant;
        parametros += "&Existencia=" + ($("#Exist" + fila).val() * 1) + "&CanPedido=0&FechaPedido=&PedidoOriginal=0&CostoPromedio=" + NumeroDecimal($("#CostoPromedio" + fila).val()) + "&cant_ant=" + cant_a;
        parametros += "&VDescuento=" + NumeroDecimal($("#VDescuento" + fila).val()) + "&foto=" + $("#Foto" + fila).val() + "&cfab=" + ($("#CFab" + fila).val() * 1) + "&pedido=" + pedido + "&NoManifiesto=" + $("#NoManifiesto" + fila).val(); 
                
        var datos = LlamarAjax(url + "Facturacion/TemporalFacturacion", parametros);
        datos = datos.split("|");

        if ($.trim(datos[0]) == "-1") {
            swal("", ValidarTraduccion("No se puede modificar este pedido, porque ya fue Enviado/Impreso/Facturado/Anulado"), "error");
            $("#Cant" + fila).val($("#CantAnt" + fila).val())
            $("#Descuento" + fila).val($("#DescuentoAnt" + fila).val());
            return false;
        }

        if ($("#Producto" + fila).val() != "TRA") {
            if ($.trim(datos[0]) == "-2") {
                $.jGrowl(ValidarTraduccion("Existencia no disponible"), { life: 4500, theme: 'growl-warning', header: '' });
                $("#Cant" + fila).val(cant_a);
                $("#Exist" + fila).val((datos[1] * 1) + (cant_a * 1));
                $("#Cant" + fila).focus();
                TotalMalla();
                return false
            }
        }
        
        if ($.trim(datos[0]) == "0") {
            $.jGrowl(datos[1], { life: 4500, theme: 'growl-warning', header: '' });
            return false
        }

        $("#Id" + fila).val(datos[0]);
        $.jGrowl(ValidarTraduccion("Temporal Guardado"), { life: 2500, theme: 'growl-success', header: '' });
        $("#Estado").val("Temporal");
        $("#Exist" + fila).val((datos[1] * 1) + (cant_a * 1));
        Estado = "Temporal";
        $("#CantAnt" + fila).val($("#Cant" + fila).val())
        $("#DescuentoAnt" + fila).val($("#Descuento" + fila).val());
           
    }
}

function ModalConsultarRemisiones() {
    $("#modalConsultarRemisiones").modal("show");
}



function ImprimirPedido(tipo) {

    var pedido = NPedido;
        
    if ((pedido == 0) && (tipo != 4))
        return false;
    
    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var datos = LlamarAjax(url + "Facturacion/ReportePedido", "pedido=" + pedido);
        if (tipo == 1) {
            $("#resultadopdfpedido").attr("src", url + "DocumPDF/" + datos);
            Pdf_Pedido = datos;
        }
        if (tipo == 2 || tipo == 4) {
            window.open(url + "DocumPDF/" + datos);
        }
        
        document.getElementById('preloader').style.display = 'none';
    }, 15);

}


function TotalMalla() {

    var tsubtotal = 0;
    var tdescuento = 0;
    var tbaseimponible = 0;
    var texcento = 0;
    var nfac = NFactura * 1;
    var tiva = 0;
    var ttotalpagar = 0;
    var clisaldo = NumeroDecimal($("#clisaldo").html()) * 1;
    var cliconsumo = 0;
    var clidisponible = 0;
    var limite = NumeroDecimal($("#clilimite").html()) * 1;
    var flete = NumeroDecimal($("#Flete").val()) * 1;
    var iva = 0;
    var descuento = 0;
    var valordes = 0;
    var existencia = 0;

    var tunidad = 0;
    var tpeso = 0;
                
    for (var x = 0; x < a_Codigo.length; x++) {

        
        if ($.trim(a_Codigo[x].value) == "") 
            break;
        
        if ((a_Codigo[x].id == "Codigo" + FilaMalla) && (NFactura == 0)) {
            
            if ((a_DescMax[x].value * 1 == 0 && DesMaxUsu == 0) && a_Descuento[x].value * 1 > 0) {
                $.jGrowl(ValidarTraduccion("Descuento no permitido"), { life: 2500, theme: 'growl-warning', header: '' });
                a_Descuento[x].value = "";
            }

            if (a_Cant[x].value == "0") {
                a_Cant[x].value = "";
            }

            if (a_Descuento[x].value == "0") {
                a_Descuento[x].value = "";
            }

            if (a_Descuento[x].value * 1 > 100) {
                $.jGrowl(ValidarTraduccion("Descuento no puede ser mayor al") + " 100%", { life: 2500, theme: 'growl-warning', header: '' });
                a_Descuento[x].value = "";
            }

            if ((a_Descuento[x].value * 1 > a_DescMax[x].value * 1) && (a_Descuento[x].value * 1 > DesMaxUsu * 1)) {
                $.jGrowl(ValidarTraduccion("Descuento no puede ser mayor a ") + " " + a_DescMax[x].value + "% ", { life: 2500, theme: 'growl-warning', header: '' });
                a_Descuento[x].value = "";
            }
        }
                
        descuento = NumeroDecimal(a_VUnitario[x].value) * a_Cant[x].value * (100 - (a_Descuento[x].value * 1)) / 100;
        valordes = NumeroDecimal(a_VUnitario[x].value) * a_Cant[x].value - descuento;
        iva = descuento * a_Iva[x].value / 100;
        a_VDescuento[x].value = valordes;

        tunidad += a_Cant[x].value * 1;
        tpeso += NumeroDecimal(a_Peso[x].value) * a_Cant[x].value;

        a_SubTotal[x].value = descuento;

        tiva += iva;
        tdescuento += valordes;
        tsubtotal += descuento
        ttotalpagar += descuento + iva;
        if ((TipCliente == 2 || TipCliente == 5 || TipCliente == 4) && (nfac == 0)) {
            cliconsumo = ttotalpagar;
            clidisponible = limite - clisaldo - cliconsumo;
            if (clidisponible < 0) {
                $.jGrowl(ValidarTraduccion("Límite de crédito no disponible"), { life: 2500, theme: 'growl-warning', header: '' });
                a_Cant[FilaMalla].value = "";
                //TotalMalla()
                return false;
            }
        }



        if (iva > 0) {
            a_Gravable[x].value = descuento * 1;
            tbaseimponible += descuento * 1;
        }
        else {
            texcento += descuento * 1;
            a_Excento[x] += descuento * 1;
        }

        a_SubTotal[x].value = a_SubTotal[x].value == 0 ? "0" : formato_numero(a_SubTotal[x].value, decimales, ".", ",", "");

    }

    ttotalpagar += flete;
        
    $("#tsubtotal").val((tsubtotal == 0 ? "0" : formato_numero(tsubtotal,decimales,".",",", SiglaMoneda)));
    $("#tdescuento").val((tdescuento == 0 ? "0" : formato_numero(tdescuento, decimales, ".", ",", SiglaMoneda)));
    $("#tbaseimponible").val((tbaseimponible == 0 ? "0" : formato_numero(tbaseimponible, decimales, ".", ",", SiglaMoneda)));
    $("#texcento").val((texcento == 0 ? "0" : formato_numero(texcento, decimales, ".", ",", SiglaMoneda)));
    $("#tiva").val((tiva == 0 ? "0" : formato_numero(tiva, decimales, ".", ",", SiglaMoneda)));
    $("#ttotalpagar").val((ttotalpagar == 0 ? "0" : formato_numero(ttotalpagar, decimales, ".", ",", SiglaMoneda)));
    $("#Unidades").val((tunidad == 0 ? "0" : formato_numero(tunidad, "0", ".", ",")));
    $("#Peso").val((tunidad == 0 ? "0" : formato_numero(tpeso, decimales, ".", ",", "")));

    if (nfac == 0) {
        $("#cliconsumo").html((ttotalpagar == 0 ? "0" : formato_numero(ttotalpagar, decimales, ".", ",", SiglaMoneda)));
        $("#clidisponible").html((clidisponible == 0 ? "0" : formato_numero(clidisponible, decimales, ".", ",", SiglaMoneda)));
    }

    TotalFactura();
        
}



function ModalMaximizarFoto(codigo, descripcion, valor) {

    codigo = codigo.split("|");
    CodigoSelFoto = valor;
        
    $("#imagen11").attr("src", urlfoto + codigo[0] + "/" + codigo[1] + "1.jpg");
    $("#imagen12").attr("src", urlfoto + codigo[0] + "/" + codigo[1] + "2.jpg");
    $("#imagen13").attr("src", urlfoto + codigo[0] + "/" + codigo[1] + "3.jpg");

    $("#imagen13").css("display", "none");
    $("#imagen12").css("display", "none");
    $("#imagen11").css("display", "block");

    $("#desmonturafoto2").html(descripcion);

    $("#divmodalmonturafoto").css("display", "none");
    $("#divmodalmaximizar").css("display", "block");

    $("#BtnSelecFoto").removeClass("hidden");
    

}

function CerraMaxFoto() {
    $("#divmodalmonturafoto").css("display", "block");
    $("#divmodalmaximizar").css("display", "none");
    $("#BtnSelecFoto").addClass("hidden");
}

function MaximizarFoto() {

    var codigo = $("#Codigo" + FilaMalla).val() * 1;
    var Foto = $("#Foto" + FilaMalla).val() * 1;
    var CFab = $("#CFab" + FilaMalla).val() * 1;
    var Descripcion = $.trim($("#Descripcion" + FilaMalla).val());

    if (Descripcion == "")
        return false;
    if (Foto == "0") {
        $.jGrowl("Producto sin foto", { life: 2500, theme: 'growl-warning', header: '' });
        return false
    }

    $("#imagen1").attr("src", urlfoto + CFab + "/" + codigo + "1.jpg");
    $("#imagen2").attr("src", urlfoto + CFab + "/" + codigo + "2.jpg");
    $("#imagen3").attr("src", urlfoto + CFab + "/" + codigo + "3.jpg");

    $("#imagen3").css("display", "none");
    $("#imagen2").css("display", "none");
    $("#imagen1").css("display", "block");

    $("#desmonturafoto").html(Descripcion);
    $("#maximizarfoto").modal('show');

}




function CargarModelosFotos(cfab, modelo) {

    var fab = cfab * 1;

    if (cfab * 1 == 0)
        cfab = $("#MarcaF").val() * 1;

    $("#divmodalmonturafoto").html("");
    $("#divmodalmonturafoto").css("display", "block");
    $("#divmodalmaximizar").css("display", "none");

    if (cfab * 1 == 0 && $.trim(modelo) == "") {
        return false;
    }

    

    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {

        var datos = LlamarAjax(url + "Facturacion/ModelosColores", "CFab=" + cfab+"&modelo=" + modelo);
        
        $("#divmodalmonturafoto").html("");
        var opciones = "<option value=''>--SELECCIONE--</option>";
        var pos = 1;
        var detalles = "<div class='row'>"
        if (datos != "[]") {
            eval("data=" + datos);
            for (var x = 0; x < data.length; x++) {
                if (data[x].foto) {
                    foto = urlfoto + data[x].CFab + "/" + data[x].Codigo + "1.jpg";
                    opciones += "<option class='padOptopn' value='" + data[x].Modelo2 + "'>" + data[x].Modelo2 + "</Option>";
                    detalles += "<div class='col-md-3 col-sm-4 col-xs-6'><a href=\"javascript:ModalMaximizarFoto('" + data[x].CFab + "|" + data[x].Codigo + "','" + data[x].Modelo + "','" + data[x].CFab + "|" + data[x].Codigo + "|" + data[x].Modelo + "|" + data[x].Existencia + "')\"><img class='border_img' src='" + foto + "'/><center><span class='text-primary'>" + data[x].Modelo + "</span></center></a></div>";
                }
            }
        }
        if (fab > 0) {
            $("#ModeloF").html(opciones);
        }
            
        $("#divmodalmonturafoto").html(detalles + "</div>");
        
        document.getElementById('preloader').style.display = 'none';
    }, 15);
}

function SeleccionarMonturaFoto() {
    if (CodigoSelFoto == "")
        return false;
    var cantidad = $("#CantFoto").val() * 1;
    if (cantidad == 0) {
        $("#CantFoto").focus();
        swal("Acción Cancelada", "Debe de ingresar la cantidad a vender", "warning");
        return false;
    }

    var codigo = CodigoSelFoto.split("|");

    if ((cantidad * 1) > (codigo[3]*1)) {
        $("#CantFoto").focus();
        swal("Acción Cancelada", "Existencia no disponible. Solo puede pedir "  + codigo[3] + " montura(s)" , "warning");
        return false;
    }

    
    SeleccionarMontura(codigo[1], FilaMalla, codigo[3], cantidad, 1);
    FilaMalla += 1;
    $("#BtnSelecFoto").addClass("hidden");
}

function SeleccionarCliente(cliente) {
    $("#modalClientes").modal("hide");
    CodCliente = 0;
    $("#Cliente").val(cliente);
    $("#Cliente").blur();
}

function SeleccionarMontura(codigo, fila, existencia, cantidad, cerrar) {
    if (existencia * 1 <= 0) {
        $.jGrowl(ValidarTraduccion("Producto sin existencia"), { life: 2500, theme: 'growl-warning', header: '' });
    } else {
        $("#Codigo" + fila).val(codigo);
        if (cerrar == 0) {
            $("#modalmontura").modal("hide");
            $("#modalmonturafoto").modal("hide");
        }
        BuscarItemMontura(codigo);
        $("#Cant" + fila).val(cantidad);
        $("#Cant" + fila).select();
        $("#Cant" + fila).focus();
    }
}

function ValidarPedido() {

    if (Estado != "Pendiente" && Estado != "Impreso") {
        swal("", ValidarTraduccion("No se puede validar un pedido con el estado") + " " + Estado + ", " + ValidarTraduccion("Guarde el pedido"), "warning");
        return false;
    }
    
    if (NPedido == 0)
        return false;

    $("#divtablamodalresultoma").html("");

    var datos = LlamarAjax(url + "Facturacion/ValidarPedido", "pedido=" + NPedido);
    if (datos == "1") {
        swal("", ValidarTraduccion("No se puede Validar este pedido porque ya esta Facturado o Anulado"), "warning");
        return false;
    }
    if (datos == "[]") {
        swal("", ValidarTraduccion("No hay monturas para verificar Pedidos"), "warning");
        return false;
    }
    var diferencia = 0;
    var labestado;
    var resultado = '<table class="table table- bordered" id="tablamodalresultoma"><thead><tr><th width="6%">Nro</th><th width="15%"><span idioma="Código">Código</span></th><th width="64%"><span idioma="Descripción">Descripción</span></th><th width="15%"><span idioma="Resultado">Resultado</span></th></tr></thead><tbody>';                                    
                                
    eval("data=" + datos);
    
    for (var x = 0; x < data.length; x++) {
        diferencia = data[x].Can * 1 - data[x].Veri*1;
        if ((data[x].Can * 1) == 0 && (data[x].Veri * 1) > 0) {
            labestado = "<label class='label label-danger' style='font-size:14px'>" + data[x].Veri + " No Pedido</label>";
        } else {
            if (diferencia == 0) {
                labestado = "<label class='label label-success' style='font-size:14px'>" + data[x].Can + " Verificados</label>";
            } else {
                if (diferencia > 0) {
                    labestado = "<label class='label label-warning' style='font-size:14px'>Faltan " + diferencia + " de " + data[x].Can + "</label >";
                } else {
                    if (diferencia < 0) {
                        labestado = "<label class='label label-info' style='font-size:14px'>Sobra " + (diferencia * -1) + " Pedido " + data[x].Can + "</label > ";
                    } else {
                        if (data[x].Can * 1 == 0 || data[x].Veri > 0) {
                            labestado = "<label class='label label-danger' style='font-size:14px'>" + data[x].Veri + " No Pedido</label>";
                        }
                    }
                }
            }
        }
                            
        resultado += "<tr>" +
            "<td>" + (x+1) + "</a></td>" +
            "<td>" + data[x].Codigo + "</td>" +
            "<td>" + data[x].Descripcion + "</td>" +
            "<td>" + labestado + "</td></tr>";
    }

    resultado += "</tbody></table>";
    $("#divtablamodalresultoma").html(resultado);

    ActivarDataTable("tablamodalresultoma", 6);
    $("#toma1").removeClass("active")
    $("#toma2").addClass("active")

    $("#lectura").removeClass("active in")
    $("#toma").addClass("active in")
    
}

function EliminarTotalToma() {
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: '¿' + ValidarTraduccion('Desea toda la toma del pedido') + ' ' + NPedido + '?',
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve) {
                $.post(url + "Facturacion/EliminarTomaPedido", "id=0&pedido=" + NPedido)
                    .done(function (data) {
                        CargarTablaTomaPedido(NPedido);
                        ValidarPedido();
                        resolve()
                    })
            })
        }
    }])
}

function SeleccionarFila(pos, id, valor) {
        
    if (NFactura > 0)
        return False

            
    if ($("#" + id).prop("checked")) {
        $("#tr-" + pos).addClass("bg-success");
    } else {
        $("#tr-" + pos).removeClass("bg-success");
    }    

    CalcularTotalTra();
    
}


function CargarModelosFotos(cfab) {

    var fab = cfab * 1;
    var modelo = $.trim($("#CModelo").val());

    if (cfab * 1 == 0)
        cfab = $("#CMarca").val() * 1;

        
    if (cfab * 1 == 0 && $.trim(modelo) == "") {
        return false;
    }
    

    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {

        var datos = LlamarAjax(url + "Facturacion/ModelosColoresPrecio", "CFab=" + cfab + "&modelo=" + modelo +"&tablaprecio=1");
        datos = datos.split("|");

        $("#divcatalogo").html("");
        var opciones = "<option value=''>--SELECCIONE--</option>";
        var pos = 1;
        var cant = 0;
        var detalles = "<div class='row'>"
        if (datos[0] != "[]") {
            eval("data=" + datos[0]);
            for (var x = 0; x < data.length; x++) {
                if (cant == 3) {
                    detalles += "</div>";
                    detalles += "<div class='form-group row'>";
                    cant = 1;
                } else
                    cant += 1;

                foto = urlfoto + data[x].CFab + "/" + data[x].Codigo + "1.jpg";
                detalles += "<div class='col-xs-6 col-md-4 col-sm-4'><a href=\"javascript:ModalMaximizarFoto('" + data[x].CFab + "|" + data[x].Codigo + "','" + data[x].Modelo + "','" + data[x].CFab + "|" + data[x].Codigo + "|" + data[x].Modelo + "|" + data[x].Existencia + "')\"><center><img style='width:90%' src='" + foto + "'/></center><div class='bg-default text-center'><div class ='row'>" + data[x].Modelo + "</div><div class='row'>$ 80.000  25%</div><div class='row text-danger'>$80.000 &nbsp; &nbsp<button class='btn btn-primary'>Pedir</button></div></div></a></div>";
            }

            if (cant != 4)
                detalles += "</div>";

        }
        if (fab > 0) {
            eval("data=" + datos[1]);
            for (var x = 0; x < data.length; x++)
                opciones += "<option class='padOptopn' value='" + data[x].Modelo + "'>" + data[x].Modelo + "</Option>";
            $("#CModelo").html(opciones);
        }

        alert(detalles);

        $("#divcatalogo").html(detalles + "</div>");

        document.getElementById('preloader').style.display = 'none';
    }, 15);
}


$("#imagen1").click(function (e) {

    $("#imagen2").css("display", "block");
    $("#imagen1").css("display", "none");
    $("#imagen3").css("display", "none");
});

$("#imagen2").click(function (e) {
    $("#imagen3").css("display", "block");
    $("#imagen2").css("display", "none");
    $("#imagen1").css("display", "none");

});

$("#imagen3").click(function (e) {
    $("#imagen3").css("display", "none");
    $("#imagen2").css("display", "none");
    $("#imagen1").css("display", "block");
      

});


function TotalFactura() {

    var totalpago = NumeroDecimal($("#ttotalpagar").val()) * 1;
    var efectivo = NumeroDecimal($("#fefectivo").val()) * 1;
    var tarjeta1 = NumeroDecimal($("#tvalor1").val()) * 1;
    var tarjeta2 = NumeroDecimal($("#tvalor2").val()) * 1;
    var cheque = 0;
    var recibido = efectivo + tarjeta1 + tarjeta2 + cheque;
    var saldo = totalpago - recibido;


    $("#frecibido").val((recibido == 0 ? "0" : formato_numero(recibido, decimales, ".", ",", SiglaMoneda)));
    $("#ftotalabonado").val((recibido == 0 ? "0" : formato_numero(recibido, decimales, ".", ",", SiglaMoneda)));

    $("#trecibido").val($("#frecibido").val());
    $("#ttotalabonado").val($("#ftotalabonado").val());

    $("#ftarjeta").val(((tarjeta1 + tarjeta2) == 0 ? "0" : formato_numero(tarjeta1 + tarjeta2, decimales, ".", ",", SiglaMoneda)));
    $("#fcheque").val((cheque == 0 ? "0" : formato_numero(cheque, decimales, ".", ",", SiglaMoneda)));

    $("#ttarjeta").val($("#ftarjeta").val());
    $("#tcheque").val($("#fcheque").val());
    
    $("#fsaldo").val((saldo == 0 ? "0" : formato_numero(saldo, decimales, ".", ",", SiglaMoneda)));
    $("#tsaldo").val($("#fsaldo").val());
    $("#tefectivo").val($("#fefectivo").val());
    
}


$("#imagen11").click(function (e) {
    $("#imagen12").css("display", "block");
    $("#imagen11").css("display", "none");;
    $("#imagen13").css("display", "none");;
});

$("#imagen12").click(function (e) {
    $("#imagen13").css("display", "block");
    $("#imagen12").css("display", "none");
    $("#imagen11").css("display", "none");
});

$("#imagen13").click(function (e) {
    $("#imagen13").css("display", "none");
    $("#imagen12").css("display", "none");
    $("#imagen11").css("display", "block");
});

cliente = localStorage.getItem("cliente");
$("#Cliente").val(cliente);
BuscarCliente(cliente);
