﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;


$("#FechaFac").val(output2);
$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);

$("#COFechaDesde").val(output);
$("#COFechaHasta").val(output2);

$("#ICFechaDesde").val(output);
$("#ICFechaHasta").val(output2);

$("#CGrupo, #ComGrupo").html(CargarCombo(47, 1));
$("#ComMarca").html(CargarCombo(49, 1));
$("#ComOIva").html(CargarCombo(55, 0));

$("#CUsuario").html(CargarCombo(13, 1));
$("#CProveedor").html(CargarCombo(7, 1));
$("#ComOServicio").html(CargarCombo(16, 1));

var CentroCostos = CargarCombo(61, 1);
$("#ComCentro").html(CentroCostos);



var IdProveedor = 0;
var IdCompra = 0;
var Compra = 0;
var Orden = 0;
var TipoCompra = "FACTURA"
var totales = 0;
var subtotal = 0;
var descuento = 0;
var exento = 0;
var gravable = 0;
var iva = 0;
var retefuente = 0;
var reteiva = 0;
var reteica = 0;
var porreteiva = 0;
var porreteica = 0;
var porretefuente = 0;
var idreteiva = 0;
var idreteica = 0;
var idretefuente = 0;
var calreteiva = 0;
var calreteica = 0;
var calretefuente = 0;
var Orden = "0";
var DocumentoPro = "";
var totalfila = 0;
var Estado = "Temporal";
var Pdf_Factura = "";
var CorreoEmpresa = "";
var ValorIva = 0;

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Cliente") {
            if ($.trim($(this).val()) != "") {

                BuscarCliente($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function CambioTipoCompra(tipo) {
    if (totalfila == 0)
        TipoCompra = tipo;
}

function GuardarDocumento() {
    var documento = document.getElementById("documento");
    if ($.trim(documento.value) != "") {
        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Cotizacion/AdjuntarExcel";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "error");
                }
            }
        });
    }
}


function SubirArchivo() {
    swal.queue([{
        html: '<h3>' + ValidarTraduccion("Seleccione el archivo de excel") + '</h3><font size="2px">Columnas (Fecha[yyyy-MM-dd], Ingreso, factura(sólo números)</font><br><input type="file" class="form-control" id="documento" onchange="GuardarDocumento()" name="documento" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required />',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Importar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                var datos = LlamarAjax("Compras","opcion=ImportarExcelFactura&", "").split("|");
                if (datos[0] == "0")
                    resolve();
                else
                    reject(datos[1]);
            })
        }
    }]);
}

function Reemplazar() {
    var tipo = $("#Tipo").val();

    if (IdCompra == 0)
        return false;

    var resultado = "";
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Desea Reemplazar la ' + tipo) + ' de Compra  Número ' + Compra + ' ' + ValidarTraduccion('del proveedor') + ' ' + $("#NombreProveedor").val(),
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Reemplazar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Compras","opcion=ReemplazarCompra&"+"id=" + IdCompra + "&observaciones=" + value)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resultado = Compra;
                        resolve();
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar un observación'));
                }
            })
        }
    }).then(function (result) {
        documento = DocumentoPro;
        LimpiarTodo();
        IdProveedor = 0;
        $("#Proveedor").val(documento);
        BuscarProveedor(documento, 1);
        swal({
            type: 'success',
            html: 'Compra Nro ' + resultado + ' anulada y reemplazada con éxito'
        })
    })
}

function AnularCompra() {

    var tipo = $("#Tipo").val();

    if (IdCompra == 0)
        return false;

    var resultado = "";
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Desea Anular la ' + tipo) + ' de Compra  Número ' + Compra +  ' ' + ValidarTraduccion('del proveedor') + ' ' + $("#NombreProveedor").val(),
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Anular'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Compras","opcion=AnularCompra&"+ "id=" + IdCompra + "&observaciones=" + value)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resultado = Compra;
                        resolve();
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar un observación'));
                }
            })
        }
    }).then(function (result) {
        LimpiarTodo();
        IdProveedor = 0;
        $("#Proveedor").val("");
        $("#Proveedor").focus();
        swal({
            type: 'success',
            html: 'Compra Nro ' + resultado + ' anulada con éxito'
        })
    })

}

function LimpiarTodo() {
    LimpiarDatos();
    DocumentoPro = "";
    $("#Compra").val("");
    $("#Orden").val("");
    $("#Proveedor").val("");
    $("#Proveedor").focus();
}

LimpiarTodo();

function LimpiarDatos() {
        
    IdProveedor = 0;
    DocumentoPro = "";
        
    Compra = 0;
    Orden = 0;
    IdCompra = 0;
    total = 0;
    subtotal = 0;
    descuento = 0;
    exento = 0;
    gravable = 0;
    iva = 0;
    retefuente = 0;
    reteiva = 0;
    reteica = 0;
    Estado = "Temporal";

    porreteiva = 0;
    porreteica = 0;
    porretefuente = 0;

    calreteiva = 0;
    calreteica = 0;
    calretefuente = 0;
        
    $("#NombreProveedor").val("");
    $("#Orden").val("").trigger("change");
    $("#Direccion").val("");
    $("#Concepto").val("FACTURA DE COMPRA")
    
    $("#Orden").val("");
    $("#Ciudad").val("");
    $("#Comra").val("");
    $("#bodyCompra").html("");
    $("#Orden").prop("disabled", false);
    $("#Compra").prop("disabled", false);
    $("#Tipo").prop("disabled", false);
    $("#FacturaExterna").prop("disabled", false);
    $("#FechaVencimiento").prop("disabled", false);
    $("#FechaCom").prop("disabled", false);
    $("#Observaciones").prop("disabled", false);
    $("#Concepto").prop("disabled", false);

    $("#FechaCom").val("");

    $("#Email").val("");
    $("#Telefonos").val("");
    
    $("#tsubtotal").val("0");
    $("#tdescuento").val("0");
    $("#tbaseimponible").val("0");
    $("#texcento").val("0");
    $("#tiva").val("0");
    $("#tretefuente").val("0");
    $("#treteiva").val("0");
    $("#treteica").val("0");
    $("#ttotalpagar").val("0");

    Estado = "Temporal";
    $("#Estado").val(Estado);

    $("#FechaVencimiento").val("");
    $("#PlazoPago").val("");
    $("#FacturaExterna").val("");
    $("#Observaciones").val("");

    $("#FechaVencimiento").val("");

    $("#Recibo").val("");
    $("#Tercero").val("");
    $("#Fecha").val("");
    $("#Monto").val("");
    $("#Pendiente").val("");
           
    $("#FechaReg").val("");
    $("#UsuarioReg").val("");
    $("#Saldo").val("0");

    $("#RPorProveedor").html("");
    $("#RPorcentaje").html("");
    $("#RSubTotal").val("");
    $("#RRetencion").val("");

    $(".ocultar").removeClass("hidden");
    
}

function ValidarProveedor(documento) {
    if ($.trim(DocumentoPro) != "") {
        if (DocumentoPro != documento) {
            DocumentoPro = "";
            LimpiarDatos();
            $("#Compra").val("");
            $("#Orden").val("");
        }
    }
}


$("#tablamodalproveedore > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    $("#modalProveedores").modal("hide");
    var numero = row[0].innerText;
    $("#Proveedor").val(numero);
    BuscarProveedor(numero, 1);
});


$("#TablaCompra > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[2].innerText.replace(",","");
    $('#tabcompras a[href="#tabcrear"]').tab('show')
    $("#Compra").val(numero);
    BuscarCompra(numero);
});



function AgregarOtroRegistroCom() {

    var id = $("#ComOIdRegistro").val() * 1;
    var descripcion = $.trim($("#ComODescripcion").val());
    var cantidad = $("#ComOCantidad").val() * 1;
    var descuento = $("#ComODescuento").val() * 1;
    var precio = NumeroDecimal($("#ComOPrecio").val()) * 1;
    var subtotal = NumeroDecimal($("#ComOVSubTotal").val()) * 1;
    var iva = $("#ComOIva").val() * 1;
    
    
    var mensaje = "";

    if ($.trim($("#ComODescuento").val()) == "") {
        $("#ComDescuento").focus();
        mensaje = "<br> Debe de ingresar el descuento del registro" + mensaje;
    }

    if (cantidad == 0) {
        $("#ComOCantidad").focus();
        mensaje = "<br> Debe de ingresar la cantidad del registro " + mensaje;
    }
    if (precio == 0) {
        $("#ComOPrecio").focus();
        mensaje = "<br> Debe de ingresar el precio del registro" + mensaje;
    }
    if (descripcion == "") {
        $("#ComODescripcion").focus();
        mensaje = "<br> Debe de ingresar la descripción del registro " + mensaje;
    }
    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    descripcion = descripcion.replace(/&/, '%26');

    var parametro = "idCompra=" + IdCompra + "&proveedor=" + IdProveedor + "&id=" + id + "&descripcion=" + descripcion + "&iva=" + iva +
        "&cantidad=" + cantidad + "&descuento=" + descuento + "&precio=" + precio + "&subtotal=" + subtotal + "&idarticulo=0";
    var datos = LlamarAjax("Compras","opcion=GuardarDetCompra&"+ parametro);
    datos = datos.split("|");
    if (datos[0] == "0") {
        $("#ModalOtroRegistroCom").modal("hide");
        LimpiarOtroRegistroCom();
        swal("", datos[1], "success");
        TablaCompras();
    } else
        swal("Acción Cancelada", datos[1], "warning");
}

function CalcularPOSerCom(caja) {
    ValidarTexto(caja, 1);
    var cantidad = NumeroDecimal($("#ComOCantidad").val()) * 1;
    var precio = NumeroDecimal($("#ComOPrecio").val()) * 1;
    var descuento = NumeroDecimal($("#ComODescuento").val()) * 1;
    var iva = $("#ComIva").val() * 1;

    var subtotal = (cantidad * precio) - ((cantidad * precio) * descuento / 100);

    $("#ComOVSubTotal").val(formato_numero(subtotal, 0,_CD,_CM,""));
    $("#ComOVIva").val(formato_numero(subtotal * iva / 100, 0,_CD,_CM,""));
    $("#ComOVDescuento").val(formato_numero(cantidad * precio * descuento / 100, 0,_CD,_CM,""));
    $("#ComOVTotal").val(formato_numero(subtotal + (subtotal * iva / 100), 0,_CD,_CM,""));
}

function BuscarOrdenCompra(orden) {
    if (orden == Orden)
        return false;
    if (orden == "") {
        LimpiarTodo();
        return false;
    }
    Orden = orden;
    $("#Orden").val(Orden);
    var datosorden = LlamarAjax("Facturacion","opcion=BuscarOrdenTercero&orden=" + Orden);
    if (datosorden == "[]") {
        LimpiarTodo();
        $("#Orden").focus();
        swal("Acción Cancelada", "Este número de órden de compra no se encuentra registrado", "warning");
        return false;
    }
    eval("dataorden=" + datosorden);
    var estadoorden = dataorden[0].estado;
    IdProveedor = dataorden[0].idproveedor;
    if (estadoorden == "Anulado" || estadoorden == "Reemplazado") {
        LimpiarTodo();
        $("#Orden").focus();
        swal("Acción Cancelada", "No se puede usar una órden de compra en estado " + Estado, "warning");
        return false;
    }
    $("#Proveedor").val($.trim(dataorden[0].documento));
    BuscarProveedor($.trim(dataorden[0].documento), 0);
    Orden = dataorden[0].orden;
    $("#Orden").prop("disabled", true);
        
    datos = LlamarAjax("Compras","opcion=GuardarOrdenCompra&proveedor=" + IdProveedor + "&orden=" + Orden).split("|");

    if (datos[0] == "0") {
        TablaCompras();
    } else {
        LimpiarTodo();
        $("#Orden").prop("disabled", false);
        $("#Orden").focus();
        swal("Acción Cancelada", datos[1], "warning");
    }
       
}

function CalcularFechas() {
    if ($("#FechaCom").val() == "" || $("#FechaVencimiento").val() == "")
        return false;
    let fecha = new Date($("#FechaCom").val());
    let vencimiento = new Date($("#FechaVencimiento").val());
    let resta = vencimiento.getTime() - fecha.getTime()
    $("#PlazoPago").val(Math.round(resta / (1000 * 60 * 60 * 24)));
}

function TablaCompras() {
    totales = 0;
    subtotal = 0;
    descuento = 0;
    gravable = 0;
    exento = 0;
    iva = 0;
    var disabled = (IdCompra > 0 ? 'disabled' : '');
    var resultado = "";
    var datos = LlamarAjax("Compras","opcion=TablaCompra&compra=" + IdCompra + "&proveedor=" + IdProveedor);
    if (datos != "[]") {
        eval("data=" + datos);
        for (var x = 0; x < data.length; x++) {
            totalfila += 1;
            resultado += "<tr id='					" + x + "' " + (data[x].idarticulo == 0 ? "ondblclick='EditarORegistroCom(" + data[x].id + ")'" : "") + ">" +
                "<td class='text-center'><b>" + (x + 1) + "</b><br>" + (IdCompra == 0 ? " <input class='form-control' type='checkbox' name='Seleccion[]' value='" + data[x].id + "' id='seleccionado" + x + "' onchange = 'SeleccionarFilaCom(" + x + ")' >" : "") + "</td>" +
                "<td class='text-center text-XX'>" + (data[x].ingreso > 0 ? "<a href=\"javascript:VerDocumentos(4,'" + data[x].remision + "')\">" + data[x].ingreso + "</a>" : "0") + "</td>" +
                "<td>" + data[x].descripcion + "</td>" +
                "<td><select class='form-control' name='CentroCosto[]' id='tcentro" + x + "' style='width:100%' " + disabled + ">" + CentroCostos + "</select></td>" +
                "<td class='text-right text-XX'><input type='text' class='text-center sinborde' " + disabled + " onfocus='FormatoEntrada(this,0)' onblur='FormatoSalida(this, 0)' onkeyup='CalcularPreTablaCom(this," + x + ")' value='" + data[x].cantidad + "' id='tcantidad" + x + "' name='Cantidades[]' style='width:99%'></td>" +
                "<td class='text-right text-XX'><input type='text' class='text-right sinborde' " + disabled + " onfocus='FormatoEntrada(this,0)' onblur='FormatoSalida(this, 0)' onkeyup='CalcularPreTablaCom(this," + x + ")' value='" + formato_numero(data[x].precio, 0,_CD,_CM,"") + "' id='tprecio" + x + "' name='Precios[]' style='width:99%'></td>" +
                "<td class='text-right text-XX'><input type='text' class='text-right sinborde' " + disabled + " onfocus='FormatoEntrada(this,0)' onblur='FormatoSalida(this, 0)' onkeyup='CalcularPreTablaCom(this," + x + ")' value='" + data[x].descuento + "' id='tdescuento" + x + "' name='Descuentos[]' style='width:99%'></td>" +
                "<td class='text-right text-XX'><input type='text' class='text-center sinborde' disabled value='" + data[x].iva + "' id='tiva" + x + "' name='IVA[]' style='width:99%'</td>" +
                "<td class='text-right text-XX'><input type='text' class='text-right sinborde' disabled value='" + formato_numero(data[x].subtotal, 0,_CD,_CM,"") + "' id='tsubtotal" + x + "' name='Subtotales[]' style='width:99%'></td>" +
                "<td class='text-center text-XX'>" + data[x].orden + "<input type='hidden' name='Articulos[]' value='" + data[x].idarticulo + "'></td>" + 
                "<td align='center'>" + (IdCompra == 0 ? "<button class='Eliminar Registro btn btn-danger' title='Eliminar Otro Registro' type='button' onclick=\"EliminarORegistroCom(" + data[x].id + ")\"><span data-icon='&#xe0d8;'></span></button>" : "&nbsp;") +
                "<input type='hidden' value='" + data[x].idcuecontable + "' name='Cuenta[]'><input type='hidden' value='" + data[x].idservicio + "' name='Servicio[]'</td></tr>";
            subtotal += (data[x].precio * data[x].cantidad)
            descuento += (data[x].precio * data[x].cantidad * data[x].descuento / 100);
            iva += ((data[x].precio * data[x].cantidad) - (data[x].precio * data[x].cantidad * data[x].descuento / 100)) * data[x].iva / 100;
            if (data[x].iva * 1 > 0)
                gravable += (data[x].precio * data[x].cantidad) - (data[x].precio * data[x].cantidad * data[x].descuento / 100);
            if (data[x].orden * 1 > 0) {
                $("#Orden").val(data[x].orden);
                $("#Orden").prop("disabled", true);
            }
        }
    }
    $("#bodyCompra").html(resultado);
    totales = subtotal - descuento + iva - reteiva - reteica - retefuente;
    exento = subtotal - gravable - descuento;
    $("#tsubtotal").val(formato_numero(subtotal, 0,_CD,_CM,""));
    $("#tdescuento").val(formato_numero(descuento, 0,_CD,_CM,""));
    $("#tbaseimponible").val(formato_numero(gravable, 0,_CD,_CM,""));
    $("#texcento").val(formato_numero(exento, 0,_CD,_CM,""));
    $("#tiva").val(formato_numero(iva, 0,_CD,_CM,""));
    $("#ttotalpagar").val(formato_numero(totales, 0,_CD,_CM,""));

    $("#tretefuente").val(formato_numero(retefuente, 0,_CD,_CM,""));
    $("#treteiva").val(formato_numero(reteiva, 0,_CD,_CM,""));
    $("#treteica").val(formato_numero(reteica, 0,_CD,_CM,""));
    var datacentro = data;
    setTimeout(function () {
        for (var x = 0; x < datacentro.length; x++) {
            if (datacentro[x].idcentro * 1 > 0) {
                $("#tcentro" + x).val(datacentro[x].idcentro).trigger("change");
            }
        }
    }, 100);

}

function EliminarORegistroCom(id) {

    if (Estado != "Temporal")
        return false;

    var datos = LlamarAjax("Compras","opcion=EliminarRegistrosSel&seleccion=" + id + "&idCompra=" + IdCompra).split("|");
    if (datos[0] == "0") {
        TablaCompras();
        swal("", "Registros eliminados", "success");
    } else
        swal("Acción Cancelada", datos[1], "warning");
} 

function EliminarRegistrosSel() {
    if (Estado != "Temporal")
        return false;
    var id = document.getElementsByName("Seleccion[]");
    var seleccionados = "";
    for (var x = 0; x < id.length; x++) {
        if (id[x].checked) {
            if (seleccionados != "")
                seleccionados += ",";
            seleccionados += id[x].value;
        }
    }
    if (seleccionados == "") {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un registro", "warning");
        return false;
    }
    var datos = LlamarAjax("Compras","opcion=EliminarRegistrosSel&seleccion=" + seleccionados + "&idCompra=" + IdCompra).split("|");
    if (datos[0] == "0") {
        TablaCompras();
        swal("", "Registros eliminados", "success");
    } else
        swal("Acción Cancelada", datos[1], "warning");
}

function BuscarCompra(compra) {

    if (compra * 1 == 0) {
        return false;
    }

    var tipo = $("#Tipo").val();

    LimpiarDatos();

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Compras","opcion=BuscarCompra&compra=" + compra + "&tipo=" + tipo);
        DesactivarLoad();
        if (datos != "[]") {
            var datac = JSON.parse(datos);
            Compra = compra;
            Orden = datac[0].orden;
            IdCompra = datac[0].id * 1;
            $("#Proveedor").val(datac[0].documento);
            BuscarProveedor(datac[0].documento, 1);
            Estado = datac[0].estado;
            $("#Orden").val(datac[0].orden);
            $("#Estado").val(datac[0].estado);
            $("#DatosAnulacion").html(datac[0].anulado);
            $("#Concepto").val(datac[0].concepto);
            retefuente = datac[0].retefuente;
            reteiva = datac[0].reteiva;
            reteica = datac[0].reteica;
            calretefuente = 1;
            calreteica = 1;
            calreteiva = 1;

            $("#PlazoPago").val(datac[0].plazopago);
            $("#FechaCom").val(datac[0].fecha)
            $("#FechaReg").val(datac[0].fechareg);
            $("#Saldo").val(formato_numero(datac[0].saldo, _DE, _CD, _CM, ""));
            $("#UsuarioReg").val(datac[0].usuario);
            $("#Observaciones").val(datac[0].observacion);

            $("#Observaciones").val(datac[0].observacion);

            $("#Recibo").val(datac[0].recibo);
            $("#Tercero").val(datac[0].tercero);
            $("#Fecha").val(datac[0].fecha);
            $("#Monto").val(formato_numero(datac[0].valor, 0, ".", ",", ""));
            $("#Pendiente").val(formato_numero(datac[0].saldorecibo, 0, _CD, _CM, ""));
            $("#FacturaExterna").val(datac[0].factura);
            $("#FechaVencimiento").val(datac[0].vencimiento);

            $("#Compra").prop("disabled", true);
            $("#Tipo").prop("disabled", true);
            $("#Orden").prop("disabled", true);
            $("#Fecha").val(datac[0].fecha);
            $("#FacturaExterna").prop("disabled", true);
            $("#FechaVencimiento").prop("disabled", true);
            $("#FechaCom").prop("disabled", true);
            $("#Observaciones").prop("disabled", true);
            $("#Concepto").prop("disabled", true);
            $(".ocultar").addClass("hidden");

        } else {
            $("#Compra").val("");
            swal("Acción Cancelada", "El numero " + compra + " no se encuentra registrado como " + tipo + " de compra", "warning");
        }
    }, 15);
}

function BuscarProveedor(documento, busqueda) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoPro == documento)
        return false;

    DocumentoPro = documento;

    ActivarLoad();
    setTimeout(function () {
        var parametros = "documento=" + $.trim(documento);
        var datos = LlamarAjax("Facturacion","opcion=BuscarProveedor&"+ parametros);
        datos = datos.split("|");
        if (datos[0] == "0") {
            var data = JSON.parse(datos[1]);
            DesactivarLoad();
            IdProveedor = data[0].id;
            $("#NombreProveedor").val(data[0].nombrecompleto);
            $("#Ciudad").val(data[0].ciudad + "/" + data[0].departamento);
            $("#Direccion").val(data[0].direccion);
            $("#Email").val(data[0].email);
            $("#Telefonos").val(data[0].telefono + " / " + data[0].celular);

            porreteiva = data[0].porreteiva;
            porreteica = data[0].porreteica;
            porretefuente = data[0].porretefuente;

            idreteiva = data[0].reteiva;
            idreteica = data[0].reteica;
            idretefuente = data[0].retefuente;

            if (busqueda == 1)
                TablaCompras();

            DesactivarLoad();

        } else {
            DesactivarLoad();
            DocumentoPro = "";
            $("#Proveedor").val("");
            $("#Proveedor").focus();
            swal("Acción Cancelada", "El documento " + documento + ", no se encuentra registrado como proveedor", "warning");
        }
    }, 15);

}

function SeleccionarTodo() {

    if ($("#seleccionatodo").prop("checked")) {
        for (var x = 0; x < totalfila; x++) {
            $("#fila-" + x).addClass("bg-gris");
            $("#seleccionado" + x).prop("checked", "checked");
        }
    } else {
        for (var x = 0; x < totalfila; x++) {
            $("#fila-" + x).removeClass("bg-gris");
            $("#seleccionado" + x).prop("checked", "");
        }
    }

    CalcularTotal();

}

function SeleccionarFilaCom(x) {

    if ($("#seleccionado" + x).prop("checked")) {
        $("#fila-" + x).addClass("bg-gris");
    } else {
        $("#fila-" + x).removeClass("bg-gris");
    }

    CalcularTotal();

}

function CalcularTotal() {

    totales = 0;
    subtotal = 0;
    descuento = 0;
    gravable = 0;
    exento = 0;
    iva = 0;

    var id = document.getElementsByName("Seleccion[]");
    var precios = document.getElementsByName("Precios[]");
    var cantidades = document.getElementsByName("Cantidades[]");
    var subtotales = document.getElementsByName("Subtotales[]");
    var ivas = document.getElementsByName("IVA[]");

    for (var x = 0; x < id.length; x++) {

        if (id[x].checked) {
            subtotal += (NumeroDecimal(precios[x].value) * cantidades[x].value)
            descuento += (NumeroDecimal(precios[x].value) * cantidades[x].value) - NumeroDecimal(subtotales[x].value);
            iva += (NumeroDecimal(subtotales[x].value) * ivas[x].value / 100);
            if (ivas[x].value * 1 > 0)
                gravable += NumeroDecimal(subtotales[x].value);
        }
    }

    retefuente = Math.round((subtotal - descuento) * porretefuente / 100);
    reteiva = Math.round((subtotal - descuento) * porreteiva / 100);
    reteica = Math.round((subtotal - descuento) * porreteica / 100);

    totales = subtotal - descuento + iva - retefuente - reteica - reteiva;
    totales = Math.round(totales);
    modalRetencionComp = Math.round(subtotal);
    descuento = Math.round(descuento);
    gravable = Math.round(gravable);
    exento = subtotal - gravable - descuento;
    iva = Math.round(iva);

    $("#tsubtotal").val(formato_numero(subtotal, 0,_CD,_CM,""));
    $("#tdescuento").val(formato_numero(descuento, 0,_CD,_CM,""));
    $("#tbaseimponible").val(formato_numero(gravable, 0,_CD,_CM,""));
    $("#texcento").val(formato_numero(exento, 0,_CD,_CM,""));
    $("#tiva").val(formato_numero(iva, 0,_CD,_CM,""));
    $("#ttotalpagar").val(formato_numero(totales, 0,_CD,_CM,""));
    $("#tretefuente").val(formato_numero(retefuente, 0,_CD,_CM,""));
    $("#treteiva").val(formato_numero(reteiva, 0,_CD,_CM,""));
    $("#treteica").val(formato_numero(reteica, 0,_CD,_CM,""));

}

function EditarORegistroCom(id) {

    if (IdCompra > 0)
        return false;
    
    var datos = LlamarAjax("Compras","opcion=BuscarTempCompra&id=" + id);
    eval("data=" + datos);
    
    $("#ComOIdRegistro").val(data[0].id);
    $("#ComODescripcion").val(data[0].descripcion);
    $("#ComOCantidad").val(data[0].cantidad);
    $("#ComODescuento").val(data[0].descuento);
    $("#ComOPrecio").val(formato_numero(data[0].precio,_DE,_CD,_CM,""));
    $("#ComOSubTotal").val(formato_numero(data[0].subtotal, _DE,_CD,_CM,""));
    $("#ComOIva").val(data[0].iva).trigger("change");
    $("#ModalOtroRegistroCom").modal("show");

}

function CalcularPreTablaCom(caja, x) {
    ValidarTexto(caja, 1);
    var precio = NumeroDecimal($("#tprecio" + x).val()) * 1;
    var cantidad = $("#tcantidad" + x).val() * 1;
    var descuento = $("#tdescuento" + x).val() * 1;

    var subtotal = (precio * cantidad) - (precio * cantidad * descuento / 100);

    $("#tsubtotal" + x).val(formato_numero(subtotal, 0,_CD,_CM,""));

    CalcularTotal();

}

function GuardarCompras() {

    if (IdProveedor == 0 || IdCompra > 0)
        return false;

    var seleccionado = 0;
    var observaciones = $.trim($("#Observaciones").val());
    var orden = $.trim($("#Orden").val());
    var fecha = $("#FechaFac").val();
    var factura = $.trim($("#FacturaExterna").val());
    var fecha = $("#FechaCom").val();
    var vencimiento = $("#FechaVencimiento").val();
    var plazopago = $("#PlazoPago").val();
    var recibo = $("#Recibo").val() * 1;
    var tipo = $("#Tipo").val();
    var orden = $("#Orden").val() * 1;
    var pendiente = NumeroDecimal($("#Pendiente").val());
    var concepto = $.trim($("#Concepto").val());

    if (concepto == "") {
        $("#Concepto").focus();
        swal("Acción Cancelada", "Debe de ingresar el concepto de la compra", "warning");
        return false;
    }

    if (fecha == "") {
        $("#FechaCom").focus();
        swal("Acción Cancelada", "Debe de ingresar la fecha de compra", "warning");
        return false;
    } else {
        if (fecha > output2) {
            $("#FechaCom").focus();
            swal("Acción Cancelada", "La fecha de compra no puede ser mayor a " + output2, "warning");
            return false;
        }
    }

    if (factura == "") {
        $("#FacturaExterna").focus();
        swal("Acción Cancelada", "Debe de ingresar el número de factura de compra", "warning");
        return false;
    }

    if (vencimiento == "") {
        $("#FechaVencimiento").focus();
        swal("Acción Cancelada", "Debe de ingresar la fecha de vencimiento de la factura", "warning");
        return false;
    }

    if (fecha > vencimiento) {
        $("#FechaVencimiento").focus();
        swal("Acción Cancelada", "La fecha de vencimiento debe ser mayor o igual a la fecha de compra", "warning");
        return false;
    }

    

            
    var seleccion = document.getElementsByName("Seleccion[]");
    var precios = document.getElementsByName("Precios[]");
    var cantidades = document.getElementsByName("Cantidades[]");
    var descuentos = document.getElementsByName("Descuentos[]");
    var subtotales = document.getElementsByName("Subtotales[]");
    var ivas = document.getElementsByName("IVA[]");
    var articulos = document.getElementsByName("Articulos[]");
    var centros = document.getElementsByName("CentroCosto[]");

    var a_precio = "array[";
    var a_descuento = "array[";
    var a_iva = "array[";
    var a_cantidad = "array[";
    var a_subtotal = "array[";
    var a_articulo = "array[";
    var a_centro = "array[";
    var a_id = "array[";

    var mensaje = "";

    if (fecha == "") {
        $("#FechaFac").focus();
        swal("Acción Cancelada", "Debe de ingresar la fecha de factura", "warning");
        return false;
    }
    

    for (var x = 0; x < seleccion.length; x++) {
        if (seleccion[x].checked) {
            
            if (NumeroDecimal(subtotales[x].value) == 0) {
                $("#tprecio" + x).focus();
                swal("Acción Cancelada", "Debe de Ingresar la cantidad y el valor unitario del item número " + (x + 1), "warning");
                return false;
            }

            if (centros[x].value * 1 == 0) {
                $("#tcentrol" + x).focus();
                swal("Acción Cancelada", "Debe de seleccionar el centro de costo del ítem número " + (x + 1), "warning");
                return false;
            }
                
            if (a_id == "array[") {
                a_id += seleccion[x].value;
                a_articulo += articulos[x].value;
                a_precio += NumeroDecimal(precios[x].value);
                a_descuento += $.trim(descuentos[x].value);
                a_iva += $.trim(ivas[x].value);
                a_cantidad += NumeroDecimal(cantidades[x].value);
                a_subtotal += NumeroDecimal(subtotales[x].value);
                a_centro += centros[x].value;

            } else {
                a_id += "," + seleccion[x].value;
                a_articulo += "," + articulos[x].value;
                a_precio += "," + NumeroDecimal(precios[x].value);
                a_descuento += "," + $.trim(descuentos[x].value);
                a_iva += "," + $.trim(ivas[x].value);
                a_cantidad += "," + NumeroDecimal(cantidades[x].value);
                a_subtotal += "," + NumeroDecimal(subtotales[x].value);
                a_centro += "," + centros[x].value;
            }
            seleccionado += 1;
        }
    }
    a_articulo += "]";
    a_precio += "]";
    a_cantidad += "]";
    a_descuento += "]";
    a_iva += "]";
    a_id += "]";
    a_subtotal += "]";
    a_centro += "]";
    
    if (seleccionado == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un registro", "warning");
        return false;
    }

    mensaje += "<br><b>¿DESEA GUARDAR LA COMPRA?</b>";

    if (recibo > 0) {
        if (totales > pendiente) {
            mensaje += "<br>El monto a facturar es mayor al monto del saldo del recibo... <b>Se generará un comprobante a favor del tercerizado</b>";
        }
    }

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: mensaje,
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Facturar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                var parametros = "id=" + IdCompra + "&proveedor=" + IdProveedor + "&observacion=" + observaciones +
                    "&subtotal=" + subtotal + "&descuento=" + descuento + "&gravable=" + gravable + "&iva=" + iva + "&total=" + totales + "&exento=" + exento +
                    "&reteiva=" + reteiva + "&retefuente=" + retefuente + "&reteica=" + reteica + "&orden=" + orden +
                    "&a_precio=" + a_precio + "&a_descuento=" + a_descuento + "&a_subtotal=" + a_subtotal + "&a_id=" + a_id + 
                    "&a_cantidad=" + a_cantidad + "&a_iva=" + a_iva + "&a_articulo=" + a_articulo + "&fechacom=" + fecha + "&vencimiento=" + vencimiento + "&factura=" + factura +
                    "&tipo=" + tipo + "&recibo=" + recibo + "&plazopago=" + plazopago + "&a_centro=" + a_centro + "&concepto=" + concepto +
                    "&idreteiva=" + idreteiva + "&idretefuente=" + idretefuente + "&idreteica=" + idreteica
                $.post("Compras/GuardarCompra", parametros)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == 0) {
                            swal("", datos[1] + " " + datos[2], "success");
                            Compra = datos[2];
                            $("#Compra").val(datos[2]);
                            $("#Compra").prop("disabled", true);
                            IdCompra = datos[3];
                            ImprimirCompra(Compra, tipo);
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);

    $(".swal2-content").html(mensaje);
    
}

function LLamarFacturar() {

    var Ordenes = "";
    if (totales == 0 || IdProveedor == 0)
        return false;

    $("#fCliente").val($("#NombreProveedor").val());
    $("#fTipoCliente").val($("#TipoCliente").val());
    $("#fOrden").val(Ordenes);
                                            
    $("#fTablaPrecios").val($("#TablaPrecio").val());
    $("#fPlazoPago").val($("#PlazoPago").val());
    $("#fObsfactura").val($("#Observaciones").val());

    $("#fsubtotal").val(formato_numero(subtotal, 0,_CD,_CM,""));
    $("#fdescuento").val(formato_numero(descuento, 0,_CD,_CM,""));
    $("#ftotalpagar").val(formato_numero(totales, 0,_CD,_CM,""));
    $("#fbaseimponible").val(formato_numero(gravable, 0,_CD,_CM,""));
    $("#fexcento").val(formato_numero(exento, 0,_CD,_CM,""));
    $("#fiva").val(formato_numero(iva, 0,_CD,_CM,""));
    $("#ftarjeta").val(formato_numero(tarjeta, 0,_CD,_CM,""));
    $("#fcheque").val(formato_numero(cheque, 0,_CD,_CM,""));
    $("#fefectivo").val(formato_numero(efectivo, 0,_CD,_CM,""));
    $("#fvuelta").val(formato_numero(vuelta, 0,_CD,_CM,""));
    $("#frecibido").val(formato_numero(recibido, 0,_CD,_CM,""));
    $("#fsaldo").val(formato_numero(saldo, 0,_CD,_CM,""));

    $("#ModalFacturar").modal("show");
}

function ValidarTextoFactura(caja, tipo) {
    ValidarTexto(caja, tipo);
    efectivo = NumeroDecimal($("#fefectivo").val())*1;
    tarjeta = NumeroDecimal($("#tvalor1").val())*1 + NumeroDecimal($("#tvalor2").val())*1;
    recibido = efectivo + tarjeta
    vuelta = (recibido > totales ? recibido - totales : 0);
    saldo = (totales > recibido ? totales - recibido : 0);
    $("#frecibido").val(formato_numero(recibido, 0,_CD,_CM,""));
    $("#fvuelta").val(formato_numero(vuelta, 0,_CD,_CM,""));
    $("#frecibido").val(formato_numero(recibido, 0,_CD,_CM,""));
    $("#fsaldo").val(formato_numero(saldo, 0,_CD,_CM,""));
}

function ImprimirCompra(compra, tipo) {
    if (compra == 0)
        compra = Compra;
    if (tipo == "")
        tipo = $("#Tipo").val();
    if (compra == 0)
        return false;
    ActivarLoad();
    setTimeout(function () {
        var parametros = "compra=" + compra + "&tipo=" + tipo;
        var datos = LlamarAjax("Compras","opcion=RpCompra&"+ parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function ModalRecibos() {
    if (IdProveedor > 0 && IdCompra == 0)
        $("#modalRecibos").modal("show");
}

function CargarModalRecibos() {

    var recibo = $("#MRecibo").val() * 1;
    var documento = $.trim($("#MDocumento").val());
    var tercero = $.trim($("#MTercero").val());
    var datos = LlamarAjax("Compras","opcion=ModalRecibos"+ "recibo=" + recibo + "&documento=" + documento + "&tercero=" + tercero);
    var datajson = JSON.parse(datos);
    var tabla = $('#TablaModalCoRecibo').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        columns: [
            { "data": "recibo" },
            { "data": "tercero" },
            { "data": "concepto" },
            { "data": "fecha" },
            { "data": "valor", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "recaudado", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "pendiente", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') }
        ],
        "lengthMenu": [5],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

$("#TablaModalCoRecibo > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    $("#Recibo").val(row[0].innerText);
    $("#Tercero").val(row[1].innerText);
    $("#Fecha").val(row[3].innerText);
    $("#Monto").val(row[4].innerText);
    $("#Pendiente").val(row[6].innerText);
    $("#modalRecibos").modal("hide");
});

function ConsultarCompras() {
    
    var proveedor = $("#CProveedor").val() * 1;
    var tipo = $("#CTipo").val();
    var orden = $("#COrden").val()*1;
    var compra = $("#CCompra").val() * 1;
    var usuario = $("#CUsuario").val() * 1;
    var estado = $("#CEstado").val();
    var vencida = $("#CVencida").val();
    
    var fechad = $("#CFechaDesde").val();
    var fechah = $("#CFechaHasta").val();

    if (fechad == "" || fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar fecha de inicio y fecha final"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "tipo=" + tipo + "&compra=" + compra + "&orden=" + orden + "&proveedor=" + proveedor + "&estado=" + estado +
            "&fechad=" + fechad + "&fechah=" + fechah + "&vencida=" + vencida + "&usuario=" + usuario;
        var datos = LlamarAjax("Compras","opcion=ConsultarCompra&"+ parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaCompra').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "imprimir" },
                { "data": "tipo" },
                { "data": "compra", "className": "text-right text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "orden" },
                { "data": "fecha" },
                { "data": "vencimiento" },
                { "data": "proveedor" },
                { "data": "plazopago" },
                { "data": "tiempo" },
                { "data": "direccion" },
                { "data": "ciudad" },
                { "data": "email" },
                { "data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "iva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "retefuente", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "reteiva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "reteica", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "saldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "estado" },
                { "data": "registro" },
                { "data": "anula" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

function LimpiarOtroRegistroCom() {
        
    $("#ComOIdRegistro").val("0");
    $("#ComODescripcion").val("");
    $("#ComOCantidad").val("1");
    $("#ComODescuento").val("0");
    $("#ComOPrecio").val("");
    $("#ComOIva").val("19").trigger("change");
    $("#ComOSubTotal").val("");
    $("#ComOVSubTotal").val("");
    $("#ComOVTotal").val("");
    $("#ComOVIva").val("");
    $("#ComOVDescuento").val("");
}

function ModalORegistroCom() {
    if (IdProveedor == 0 || IdCompra > 0)
        return false;
    LimpiarOtroRegistroCom();
    $("#ModalOtroRegistroCom").modal("show");
    
}

function ModalArticulosCom() {

    if (IdProveedor == 0 || IdCompra > 0 )
        return false;

    $("#ComTipoInventario").val("").trigger("change");
    $("#ComGrupo").val("").trigger("change");
    $("#ComProveedor").val(IdProveedor).trigger("change");
    $("#ComPresentacion").val("");
    $("#ComMarca").val("").trigger("change");
    $("#ComIdArticulo").val("0");
    $("#ComCodigoBarras").val("");
    $("#ComUltimoCosto").val("0");
    $("#ComIva").val("1").trigger("change");

    for (var x = 1; x <= 10; x++) {
        $("#ComPrecio" + x).val("0");
    }

    $("#modalArticulosCom").modal("show");
}

function CargarModalArticulosCom() {

    var codigo = $.trim($("#CCodigo").val());
    var descripcion = $.trim($("#CDescripcion").val());
    var grupo = $("#CGrupo").val() * 1;

    var parametros = "codigo=" + codigo + "&descripcion=" + descripcion + "&grupo=" + grupo;
    var datos = LlamarAjax("Inventarios","opcion=ModalArticulos&"+ parametros);
    var datajson = JSON.parse(datos);
    $('#TablaModalArticulosCom').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        searching: false,
        columns: [
            { "data": "foto" },
            { "data": "id" },
            { "data": "codigointerno" },
            { "data": "codigobarras" },
            { "data": "grupo" },
            { "data": "tipo" },
            { "data": "equipo" },
            { "data": "marca" },
            { "data": "modelo" },
            { "data": "presentacion" },
            { "data": "ultimocosto", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "existencia" },
            { "data": "iva" },
            { "data": "cuenta" },
            { "data": "proveedor" }
        ],
        "lengthMenu": [[5], [5]],
        "language": {
            "url": LenguajeDataTable
        }

    });
}

function ComCargarModelos(marca) {
    $("#ComModelo").html(CargarCombo(50, 1, "", marca));
}

function ComCargarEquipos(grupo) {
    $("#ComEquipo").html(CargarCombo(48, 1, "", grupo));
}

$("#FormAgregarArticuloCom").submit(function (e) {

    var equipo = $("#ComEquipo option:selected").text();
    var grupo = $("#ComGrupo option:selected").text();
    var marca = $("#ComMarca option:selected").text();
    var modelo = $("#ComModelo option:selected").text();
    var tipo = $("#ComTipoInventario option:selected").text();
    var presentacion = $("#ComPresentacion").val();

    e.preventDefault();
    var mensaje = "";
    var ultimocosto = NumeroDecimal($("#ComUltimoCosto").val());
    var iva = $("#ComIva").val() * 1;
    var parametros = $("#FormAgregarArticuloCom").serialize().replace(/\%2C/g, "") + "&Estado=1";

    if ($.trim($("#ComUltimoCosto").val()) == "")
        mensaje += "Debe de ingresar el valor del último costo <br>";
    for (var x = 1; x <= 10; x++) {
        if ($.trim($("#ComPrecio" + x).val()) == "")
            mensaje += "Debe de ingresar el valor del precio número " + x + " <br>";
        parametros += "&Precio" + x + "=" + NumeroDecimal($("#Precio" + x).val());
    }
    parametros += "&UltimoCosto=" + ultimocosto + "&Iva=" + iva;

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }
    var datos = LlamarAjax("Inventarios","opcion=GuardarArticulos&"+ parametros).split("|");
    if (datos["0"] == "0") {
        var cliente = $("#Cliente").val() * 1;
        id = datos[2];
        codigo = datos[3];
        iva = datos[4];
        var descripcion = "CODIGO: " + codigo + ", TIPO: " + tipo + ", GRUPO: " + grupo +
            ", ARTICULO: " + equipo + ", MARCA: " + marca + ", MODELO: " + modelo + ", PRESENTACION: " + presentacion;
        var parametro = "idCompra=" + IdCompra + "&proveedor=" + IdProveedor + " &id=0&descripcion=" + descripcion +
            "&cantidad=1&descuento=0&precio=" + ultimocosto + "&subtotal=" + ultimocosto + "&tipo=2&idarticulo=" + id + "&iva=" + iva;
        var datos = LlamarAjax("Compras/GuardarDetCompra", parametro);
        datos = datos.split("|");
        if (datos[0] == "0") {
            $("#modalArticulosCom").modal("hide");
            $("#modalNuevoArticuloCom").modal("hide");
            swal("", "Artículo agregado con éxito", "success");
            TablaCompras();
        } else
            swal("Acción Cancelada", datos[1], "warning");

    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function ComCalcularPrecio() {
    var datos = LlamarAjax("Inventarios","opcion=DescripcionPrecio&");
    var iva = NumeroDecimal($("#ComIva").val());
    var costo = NumeroDecimal($("#ComUltimoCosto").val());
    var data = JSON.parse(datos);
    var resultado = "";
    for (var x = 0; x < data.length; x++) {
        porcentaje = data[x].porcentaje;
        pos = x + 1;
        precio = (costo * porcentaje / 100) + costo;
        precioventa = precio * (1 + (iva / 100));
        precioventa = Math.round(precioventa / 100, 0) * 100;
        $("#ComPrecio" + pos).val(formato_numero(precioventa, 0,_CD,_CM,""));
    }
}

function ComAgregarOpcion(tipo, mensaje, id) {

    var marca = $("#ComMarca").val() * 1
    var grupo = $("#ComGrupo").val() * 1

    switch (tipo) {

        case 2:
            if (grupo == 0) {
                $("#Grupo").focus();
                swal("Acción Cancelada", "Debe de seleccionar un grupo", "warning");
                return false;
            }
            break;

        case 4:
            if (marca == 0) {
                $("#Marca").focus();
                swal("Acción Cancelada", "Debe de seleccionar una marca", "warning");
                return false;
            }
            break;
    }

    swal({
        title: 'Agregar Opción',
        text: mensaje,
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Agregar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if ($.trim(value)) {
                    var datos = LlamarAjax("Inventarios","opcion=GuardarOpcion&tipo=" + tipo + "&grupo=" + grupo + "&marca=" + marca + " &descripcion=" + value);
                    datos = datos.split("|");
                    if (datos[0] != "XX") {
                        $("#" + id).html(datos[1]);
                        $("#" + id).val(datos[0]).trigger("change");
                        resolve();
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar la descripción de ' + mensaje));
                }
            })
        }
    }).then(function (result) {
        $("#" + id).focus();
    })
}

$("#TablaModalArticulosCom > tbody").on("dblclick", "tr", function (e) {

    var row = $(this).parents("td").context.cells;
    var descripcion = "CODIGO: " + row[2].innerText + ", TIPO: " + row[5].innerText + ", GRUPO: " + row[4].innerText +
        ", ARTICULO: " + row[6].innerText + ", MARCA: " + row[7].innerText + ", MODELO: " + row[8].innerText + ", PRESENTACION: " + row[9].innerText;
    var parametro = "idCompra=" + IdCompra + "&proveedor=" + IdProveedor + " &id=0&descripcion=" + descripcion +
        "&iva=" + row[12].innerText + "&cuenta=" + row[13].innerText +
        "&cantidad=1&descuento=0&precio=" + NumeroDecimal(row[10].innerText) + "&subtotal=" + NumeroDecimal(row[10].innerText) + "&idarticulo=" + row[1].innerText;
    var datos = LlamarAjax("Compras/GuardarDetCompra", parametro + "&orden=" + Orden);
    datos = datos.split("|");
    if (datos[0] == "0") {
        $("#modalArticulosCom").modal("hide");
        swal("", datos[1], "success");
        TablaCompras();
    } else
        swal("Acción Cancelada", datos[1], "warning");
});

var AutoORegistro = [];

function AutoCompletarOServicio() {
    var datos = LlamarAjax("Compras","opcion=AutoCompletoOServ&"+ "")
    AutoORegistro.splice(0, AutoORegistro.length);
    var data = JSON.parse(datos);
    for (var x = 0; x < data.length; x++) {
        AutoORegistro.push(data[x].descripcion);
    }
}

AutoCompletarOServicio();


$("#ComODescripcion").autocomplete({
    source: AutoORegistro
});
$("#ComODescripcion").autocomplete("option", "appendTo", ".eventInsForm");

$('select').select2();
DesactivarLoad();

function AgregarRetencioComp() {
    var tipo = $("#RTipoRetencion").val() * 1;
    var tipo = $("#RTipoRetencion").val() * 1;
    switch (tipo) {
        case 1: //FUENTE
            retfuente = NumeroDecimal($("#RRetencion").val());
            ReteFuente = $("#RPorcentaje").val() * 1;
            porretefuente = LlamarAjax("Compras","opcion=PorcentajeRetencion&"+ "retencion=" + ReteFuente) * 1;
            break;
        case 2: //IVA
            retiva = NumeroDecimal($("#RRetencion").val());
            ReteIva = $("#RPorcentaje").val() * 1;
            porreteiva = LlamarAjax("Compras","opcion=PorcentajeRetencion&"+ "retencion=" + ReteIva) * 1;
            break;
        case 3: //ICA
            retica = NumeroDecimal($("#RRetencion").val());
            ReteIca = $("#RPorcentaje").val() * 1;
            porreteica = LlamarAjax("Compras","opcion=PorcentajeRetencion&"+ "retencion=" + ReteIca) * 1;
            break;
    }
    CalcularTotal();
    $("#modalRetencionComp").modal("hide");
}

function CalcularRetencionComp(Caja) {
    if (Caja)
        ValidarTexto(Caja, 3);
    var retencion = NumeroDecimal($("#RPorcentaje").val());
    var porcentaje = LlamarAjax("Compras","opcion=PorcentajeRetencion&"+ "retencion=" + retencion) * 1;
    var valor = NumeroDecimal($("#RSubTotal").val());
    $("#RRetencion").val(formato_numero(Math.trunc(valor * porcentaje / 100), 0,_CD,_CM,""));
}

function CambioRetencionComp(retencion) {
    CalcularRetencionComp();
}

function LlamarRetencionComp(tipo, descripcion) {
    if (IdCompra > 0)
        return false;

    if (Estado != "Temporal" && Estado != "Facturado")
        return false;
            
    if (totales > 0) {

        switch (tipo) {
            case 1: //Fuente
                $("#RSubTotal").val(formato_numero(subtotal - descuento, 0, _CD, _CM, ""));
                $("#RPorProveedor").html(porretefuente);
                $("#RPorcentaje").html(CargarCombo(89, 7, "", "ReteFuente"));
                $("#RPorcentaje").val(idretefuente).trigger("change");
                break;
            case 2: //IVA
                $("#RSubTotal").val(formato_numero(iva, 0, _CD, _CM, ""));
                $("#RPorProveedor").html(porreteiva);
                $("#RPorcentaje").html(CargarCombo(89, 7, "", "ReteIVA"));
                $("#RPorcentaje").val(idreteiva).trigger("change");
                break;
            case 3: //ICA
                $("#RSubTotal").val(formato_numero(subtotal - descuento, 0, _CD, _CM, ""));
                $("#RPorProveedor").html(porreteica);
                $("#RPorcentaje").html(CargarCombo(89, 7, "", "ReteICA"));
                $("#RPorcentaje").val(idreteica).trigger("change");
                break;
        }
        $("#RTipoRetencion").val(tipo);
        $("#desretencion").html(descripcion);
        $("#modalRetencionComp").modal("show");

    }
}

function VerAnexor() {

    if (IdProveedor == 0)
        return false;

    if (IdCompra == 0) {
        swal("Acción Cancelada", "Debe de guardar primero la compra", "warning");
        return false;
    }
    if (Estado == "Anulado") {
        $("#DivAgregarAnexo").addClass("hidden");
    } else {
        $("#DivAgregarAnexo").removeClass("hidden");
    }

    var datos = LlamarAjax("Compras","opcion=TablaDocumentCompra"+ "IdCompra=" + IdCompra);
    var resultado = "";
    if (datos != "[]") {
        var data = JSON.parse(datos);
        for (var x = 0; x < data.length; x++) {
            resultado += "<tr>" +
                "<td>" + (x + 1) + "</td>" +
                "<td><a href='javascript:VerArchivoAnexo(" + data[x].foto + ")'>" + data[x].archivo + "</a></td>" +
                "<td><button class='btn btn-danger' title='Eliminar Registro' type='button' onclick='EliminarAnexo(" + data[x].id + "," + data[x].foto + ")'><span data-icon='&#xe0d8;'></span></button></td></tr>";
        }
    }

    $("#TBodyAnexo").html(resultado);
    $("#anexocompra").html(Compra);
    $("#modalAnexos").modal("show");
}

function AgregarArchivoComp() {

    if (Estado == "Anulado") {
        swal("Acción Cancelada", "No se puede agregar un documento a una compra anulada", "warning");
        return false;
    }

    var archivo = $("#CArchivo").val();
    if (archivo == "") {
        swal("Acción Cancelada", "Debe de seleccionar un archivo", "warning");
        return false;
    }

    var datos = LlamarAjax("Compras","opcion=GuardarDocumentCompra&"+ "IdCompra=" + IdCompra + "&Compra=" + Compra).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        VerAnexor();
    } else
        swal("Acción Cancelada", datos[1], "warning");
}

function EliminarAnexo(id, foto) {

    if (Estado == "Anulado") {
        swal("Acción Cancelada", "No se puede eliminar un documento a una compra anulada", "warning");
        return false;
    }
        
    var datos = LlamarAjax("Compras","opcion=EliminarDocumentCompra&"+ "id=" + id + "&Compra=" + Compra + "&foto=" + foto).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        VerAnexor();
    } else
        swal("Acción Cancelada", datos[1], "warning");
}

function GuardarDocumentoComp() {
    var documento = document.getElementById("CArchivo");
    if ($.trim(documento.value) != "") {

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Compras/AdjuntarArchivo";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
                    documento.value = "";
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "error");
                }
            }
        });
    }
}

function VerArchivoAnexo(foto) {
    window.open(url + "imagenes/ArchivoCompra/" + Compra + "/" +  foto + ".pdf");
}

function ActualizarRetencioComp() {
    var tipo = $("#RTipoRetencion").val() * 1;
    var idretencion = $("#RPorcentaje").val() * 1;
    var datos = LlamarAjax("Compras","opcion=ActualizarRetencion&tipo=" + tipo + "&idretencion=" + idretencion + "&proveedor=" + IdProveedor).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
    }
    else
        swal("Acción Cancelada", datos[1], "warning");
}

function ContabilizarCompra(contabilizar) {

    if (IdProveedor == 0)
        return false;

    ActivarLoad();
    setTimeout(function () {
                
        var seleccionado = 0;
        var TDebito = 0;
        var TCredito = 0;

        if (contabilizar == 1) {

            var cuenta = document.getElementsByName("Cuentas[]");
            for (var x = 0; x < cuenta.length; x++) {
                if (cuenta[x].checked == false) {
                    DesactivarLoad();
                    swal("Acción Cancelada", "Debe de seleccionar todas las cuentas", "warning");
                    return false;
                }
                if (cuenta[x].value * 1 == 0) {
                    DesactivarLoad();
                    swal("Acción Cancelada", "Todas las cuentas deben de estar asignadas", "warning");
                    return false;
                }

            }

            var diferencia = NumeroDecimal($("#TDiferencia").val());
            if (diferencia != 0) {
                DesactivarLoad();
                swal("Acción Cancelada", "El comprobante contable se encuentra descuadrado", "warning");
                return false;
            }

            TDebito = NumeroDecimal($("#TDebitos").val());
            TCredito = NumeroDecimal($("#TCreditos").val());

            DesactivarLoad();

            swal.queue([{
                title: ValidarTraduccion('Advertencia'),
                text: ValidarTraduccion('Antes de contabilizar la factura debe de guardar el documento, ¿Desea guardar y contabilizar la factura?'),
                type: 'question',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                confirmButtonText: ValidarTraduccion('Guardar y Contabilizar'),
                cancelButtonText: ValidarTraduccion('Cancelar'),
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        var datos = GuardarFactura(2).split("|");
                        if (datos[0] == "0") {
                            ContabilizarFactura(2);
                            resolve();
                        } else {
                            reject(datos[1]);
                        }
                    })
                }
            }]);
            return false;
        }
        var ingresos = document.getElementsByName("Seleccion[]");
        var subtotales = document.getElementsByName("Subtotales[]");
        var centros = document.getElementsByName("CentroCosto[]");
        var cuenta = document.getElementsByName("Cuenta[]");

        var a_subtotal = "array[";
        var a_centro = "array[";
        var a_cuenta = "array[";
        var agregar = 0;
        for (var x = 0; x < ingresos.length; x++) {
            agregar = 0;
            console.log(ingresos);
            if (ingresos.length > 0) {
                if (ingresos[x].checked)
                    agregar = 1;
            } else {
                agregar = 1;
            }
            if (agregar == 1) {

                if (a_cuenta == "array[") {
                    a_subtotal += NumeroDecimal(subtotales[x].value);
                    a_centro += (centros[x].value*1);
                    a_cuenta += (cuenta[x].value*1);

                } else {
                    a_subtotal += "," + NumeroDecimal(subtotales[x].value);
                    a_centro += "," + (centros[x].value*1);
                    a_cuenta += "," + (cuenta[x].value*1);
                }
                seleccionado += 1;
            }
        }

        a_subtotal += "]";
        a_centro += "]";
        a_cuenta += "]";

        if (seleccionado == 0) {
            DesactivarLoad();
            swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un registro", "warning");
            return false;
        }
        var faccontabilizar = (contabilizar == 0 ? 0 : Compra);
        var parametros = "factura=" + faccontabilizar + "&proveedor=" + IdProveedor + "&iva=" + iva + "&subtotal=" + subtotal + "&total=" + totales + "&reteiva=" + reteiva + "&retefuente=" + retefuente + "&reteica=" + reteica +
            "&a_centro=" + a_centro + "&a_cuenta=" + a_cuenta + "&a_monto=" + a_subtotal + "&tdebito=" + TDebito + "&tcredito=" + TCredito + "&buscar=" + Compra;
        var datos = LlamarAjax("Compras","opcion=Contabilizar_Compra&"+ parametros).split("||");
        DesactivarLoad();
        if (datos[0] == "0") {
            var data = JSON.parse(datos[1]);
            if (data[0]._error * 1 == 0) {
                if (contabilizar == 0) {
                    var resultado = "";

                    var tdebito = 0;
                    var tcredito = 0;

                    for (var x = 0; x < data.length; x++) {
                        resultado += "<tr " + (data[x]._idcuenta * 1 == 0 ? " class='bg-danger' " : "") + ">" +
                            "<td class='text-center'><input class='form-control' type='checkbox' style='width:50px;' value='" + data[x]._idcuenta + "' name='Cuentas[]'></td>" +
                            "<td>" + data[x]._cuenta + "</div></td>" +
                            "<td class='text-right'>" + formato_numero(data[x]._debito, _DE, _CD, _CM, "") + "</td>" +
                            "<td class='text-right'>" + formato_numero(data[x]._credito, _DE, _CD, _CM, "") + "</td></tr>";
                        tdebito += data[x]._debito;
                        tcredito += data[x]._credito;
                    }

                    $("#TDebitos").val(formato_numero(tdebito, _DE, _CD, _CM, ""))
                    $("#TCreditos").val(formato_numero(tcredito, _DE, _CD, _CM, ""))
                    $("#TDiferencia").val(formato_numero(tdebito - tcredito, _DE, _CD, _CM, ""))

                    $("#TbContabilidadComp").html(resultado);

                    if (Estado == "Facturado" || Estado == "Temporal") {
                        $("#btncontabilizar").removeClass("hidden");
                        $("#btneliminarconta").addClass("hidden");
                    } else {
                        $("#btncontabilizar").addClass("hidden");
                        if (Estado == "Contabilizado")
                            $("#btneliminarconta").removeClass("hidden");
                        else
                            $("#btneliminarconta").addClass("hidden");
                    }
                    $("#modalCotizacion").modal("show");
                } else {

                    Estado = "Contabilizado";
                    $("#Estado").val(Estado);
                    swal("", "Contabilización realizada con éxito", "success");
                    $("#btncontabilizar").addClass("hidden");
                    $("#btneliminarconta").removeClass("hidden");
                }
            } else {
                swal("Acción Cancelada", data[0]._mensaje, "warning");
            }
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
}

function AnularContabilizacion() {
    if (Compra == 0)
        return false;
    var resultado = "";
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea anular la contabilización de la compra número ' + Compra + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Anular'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Facturacion/AnularContabilizacion", "factura=" + Compra)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            resultado = datos[1];
                            resolve();
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]).then(function (result) {
        Estado = "Facturado";
        $("#Estado").val(Estado);
        $("#btneliminarconta").addClass("hidden");
        $("#btncontabilizar").removeClass("hidden");
        BuscarFactura(Compra);
        swal({
            type: 'success',
            html: resultado
        })
    });
} 

