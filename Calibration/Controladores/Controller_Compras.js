AppAngular.controller('ControlCompras', function ($scope, $http) {
    
    console.log($scope.scopeHeader);

    var tabClasses;
    var IvaDefecto = '19';
   
    $scope.Documento = "";
    $scope.Compra = "";
    $scope.Orden = "";
    
    $scope.Proveedor = {
        id: 0,
        documento: ""
    }

    $scope.InicializarRegistro = function () {
        $scope.Registro = {
            allselect: false,
            id: 0,
            proveedor: 0,
            compra: "",
            orden: "",
            disabled: false,
            btnguardar: true,
            btnanular: false,
            btnimprimir: false,
            btnanexo: true,
            estado: "Temporal",
            tipo: "FACTURA",
            fecha: new Date(output2 + " 00:00"),
            concepto: "FACTURA DE COMPRA",
            bodega: "",
            facturaexterna: "",
            fechavencimiento: null,
            diaspago: 0,
            fecharegistro: null,
            usuario: "",
            saldofactura: 0,
            observacion: "",
            anulaciones: "",
            recibo: "",
            tercero: "",
            fecharecibo: null,
            montorecibo: 0,
            saldorecibo: 0,
            tabla: {},
            temporal: 0,
            subtotal: 0,
            descuento: 0,
            gravable: 0,
            exento: 0,
            iva: 0,
            retefuente: 0,
            reteiva: 0,
            reteica: 0,
            total: 0,
            tcantidad: 0

        }

        setTimeout(function () {
            $(".ComboRegistro").trigger("change");
        }, 200);
    }

    $scope.CargarCombos = function () {
        $("#MUbicacion").html("<option value='0'>Todas</option>");
        $scope.Combos = {
            centroscostos: CargarCombo(61, 1, "", "", 1),
            bodega: CargarCombo(101, 0, "", "", 1),
            grupo: CargarCombo(47, 0, "", "", 1),
            marca: CargarCombo(49, 0, "", "", 1),
            iva: CargarCombo(55, 0, "", "", 1),
            usuarios: CargarCombo(13, 0, "", "", 1),
            proveedor: CargarCombo(7, 0, "", "", 1),
            servicio: CargarCombo(16, 0, "", "", 1)
        }
    }

    $scope.Consulta = {
        tipo: 'TODOS',
        compra: '',
        orden: '',
        proveedor: '',
        bodega: '',
        estado: 'Todos',
        vencida: 'Todas',
        usuario: '',
        fechad: {
            value: new Date(output + " 00:00"),
            id: output
        },
        fechah: {
            value: new Date(output2 + " 00:00"),
            id: output2
        }
    }

    $scope.InicializarOtroRegistro = function () {
        $scope.OtroRegistro = {
            id: 0,
            ingreso: 0,
            servicio: '',
            descripcion: '',
            cantidad: 0,
            descuento: 0,
            costo: 0,
            iva: IvaDefecto,
            subtotal: 0,
            total: 0,
            tdescuento: 0,
            tiva: 0,
            centrocosto: '0',
            posicion: -1
        }

        setTimeout(function () {
            $(".ComboOtroRegistro").trigger("change");
        }, 200);

    }

    $scope.Totales = function () {

        $scope.Registro.subtotal = 0;
        $scope.Registro.descuento = 0;
        $scope.Registro.gravable = 0;
        $scope.Registro.exento = 0;
        $scope.Registro.iva = 0;
        $scope.Registro.retefuente = 0;
        $scope.Registro.reteiva = 0;
        $scope.Registro.reteica = 0;
        $scope.Registro.total = 0;
        $scope.Registro.tcantidad = 0;

        for (var i = 0; i < $scope.Registro.tabla.length; i++) {
            if ($scope.Registro.tabla[i].seleccionado) {
                $scope.Registro.subtotal += NumeroDecimal($scope.Registro.tabla[i].subtotal);
                $scope.Registro.tcantidad += NumeroDecimal($scope.Registro.tabla[i].cantidad);
                $scope.Registro.descuento += (NumeroDecimal($scope.Registro.tabla[i].subtotal) * NumeroDecimal($scope.Registro.tabla[i].descuento)) / 100;
                if (NumeroDecimal($scope.Registro.tabla[i].iva) > 0) {
                    $scope.Registro.gravable += NumeroDecimal($scope.Registro.tabla[i].subtotal);
                    $scope.Registro.iva += (NumeroDecimal($scope.Registro.tabla[i].subtotal) - ((NumeroDecimal($scope.Registro.tabla[i].subtotal) * NumeroDecimal($scope.Registro.tabla[i].descuento)) / 100)) * NumeroDecimal($scope.Registro.tabla[i].iva) / 100;
                } else {
                    $scope.Registro.exento += NumeroDecimal($scope.Registro.tabla[i].subtotal);
                }
            }
        }

        $scope.Registro.total = NumeroDecimal($scope.Registro.subtotal) - NumeroDecimal($scope.Registro.descuento) + NumeroDecimal($scope.Registro.iva) - NumeroDecimal($scope.Registro.reteiva) - NumeroDecimal($scope.Registro.retefuente) - NumeroDecimal($scope.Registro.reteica);

    }

    $scope.ActualizarCentro = function () {
        setTimeout(function () {
            var centro = 0;
            for (var i = 0; i < $scope.Registro.tabla.length; i++) {
                centro = $scope.Registro.tabla[i].idcentro;
                $scope.Registro.tabla[i].idcentro = 0;
                console.log(i, centro);
                $scope.Registro.tabla[i].idcentro = "" + centro + "";
            }
            $scope.$apply()
        }, 200);
    }

    function initTabs() {
        tabClasses = ["", ""];
    }

    $scope.setCambioSelect = function (index) {
        if ($scope.Registro.tabla[index].seleccionado) {
            $scope.Registro.tabla[index].class_select = "bg-gris";
        } else {
            $scope.Registro.tabla[index].class_select = "";
        }
        $scope.Totales();
    }

    $scope.SeleccionarTodo = function () {
        for (var i = 0; i < $scope.Registro.tabla.length; i++) {
            if ($scope.Registro.allselect) {
                $scope.Registro.tabla[i].seleccionado = true;
                $scope.Registro.tabla[i].class_select = "bg-gris";
            } else {
                $scope.Registro.tabla[i].seleccionado = false;
                $scope.Registro.tabla[i].class_select = "";
            }
        }
        $scope.Totales();
    }


    $scope.getTabClass = function (tabNum) {
        return tabClasses[tabNum];
    };

    $scope.getTabPaneClass = function (tabNum) {
        return "tab-pane " + tabClasses[tabNum];
    }

    $scope.setActiveTab = function (tabNum) {
        initTabs();
        tabClasses[tabNum] = "active";
    };

    $("#tablamodalproveedore > tbody").on("dblclick", "tr", function (e) {
        var row = $(this).parents("td").context.cells;
        $("#modalProveedores").modal("hide");
        $scope.DocumentoPro = $.trim(row[0].innerText);
        $scope.BuscarProveedor($scope.DocumentoPro, 1, 1);
    });



    $scope.ValidarProveedor = function (documento, event) {
        if ($.trim(documento) == "") {
            return false;
        }
        if ($.trim(documento) != $scope.Documento) {
            if ($scope.Documento != "") {
                $scope.LimpiarDatos();
                $scope.Documento = "";
            }
        }
        if (event.keyCode == 13) {
            $("#TipoFactura").focus();
        }
    }

    $scope.BuscarProveedor = function (documento, busqueda, refrescar) {
        if ($.trim(documento) == $scope.Cliente.documento || $.trim(documento) == "") {
            return false;
        }
        var parametros = "documento=" + $.trim(documento);
        var datos = LlamarAjax("Facturacion", "opcion=BuscarProveedor&" + parametros);
        datos = datos.split("|");
        if (busqueda == 1)
            $scope.LimpiarDatos();
        else
            $scope.DocumentoPro = documento;
        if (datos[0] == "0") {
            var data = JSON.parse(datos[1]);
            $scope.Proveedor = data[0];
            console.log($scope.Proveedor);
            if (refrescar == 1) {
                $scope.$apply();
            }
            $scope.Registro.proveedor = $scope.Proveedor.id;
            
            $scope.TablaCompras(busqueda);
        } else {
            $scope.DocumentoPro = "";
            $("#Documento").focus();
            swal("Acción Cancelada", datos[1], "warning");
        }
    }

    $scope.BuscarOrdenCompra = function (orden) {
        if ($.trim(orden) == $scope.Orden || $.trim(orden) == "") {
            return false;
        }
    }

    $("#TablaCompra > tbody").on("dblclick", "tr", function (e) {
        var row = $(this).parents("td").context.cells;
        var numero = row[2].innerText.replace(",", "");
        $scope.BuscarCompra(numero);
        $scope.setActiveTab(1);
    });


    $scope.BuscarCompra = function (compra) {
        if ($.trim(compra) == $scope.Compra || $.trim(compra) == "") {
            return false;
        }
        ActivarLoad();
        setTimeout(function () {
            var tipo = $scope.Registro.tipo;
            var datos = LlamarAjax("Compras", "opcion=BuscarCompra&compra=" + compra + "&tipo=" + tipo);
            if (datos != "[]") {
                var datac = JSON.parse(datos);
                $scope.LimpiarTodo();
                $scope.Compra = compra;
                $scope.Orden = datac[0].orden
                $scope.Registro.compra = compra;
                $scope.Registro.orden = datac[0].orden;
                $scope.Registro.id = datac[0].id * 1;
                $scope.Registro.disabled = true;
                $scope.Registro.btnguardar = false;
                $scope.Registro.btnanular = true;
                $scope.Registro.btnimprimir = true;
                $scope.Registro.btnanexo = true;
                $scope.Registro.estado = datac[0].estado;
                $scope.Registro.retefuente = formato_numero(datac[0].retefuente);
                $scope.Registro.reteiva = formato_numero(datac[0].reteiva);
                $scope.Registro.reteica = formato_numero(datac[0].reteica);
                $scope.Registro.tipo = tipo;
                $scope.Registro.concepto = datac[0].concepto;
                $scope.Registro.anulaciones = datac[0].anulado;
                $scope.Registro.observacion = datac[0].observacion;
                $scope.Registro.fecha = new Date(datac[0].fecha + " 00:00");
                $scope.Registro.bodega = $.trim(datac[0].idbodega);
                $scope.Registro.facturaexterna = datac[0].factura;
                $scope.Registro.fechavencimiento = new Date(datac[0].vencimiento + " 00:00");
                $scope.Registro.diaspago = datac[0].plazopago;
                $scope.Registro.fecharegistro = datac[0].fechareg;
                $scope.Registro.usuario = datac[0].usuario;
                $scope.Registro.saldofactura = datac[0].saldo;
                $scope.Registro.recibo = datac[0].recibo;
                $scope.Registro.tercero = datac[0].tercero;
                $scope.Registro.fecharecibo = datac[0].fecharecibo;
                $scope.Registro.montorecibo = datac[0].valor;
                $scope.Registro.saldorecibo = datac[0].saldorecibo;
                $scope.Registro.subtotal = datac[0].subtotal;
                $scope.Registro.descuento = datac[0].descuento;
                $scope.Registro.gravable = datac[0].gravable;
                $scope.Registro.exento = datac[0].exento;
                $scope.Registro.iva = datac[0].iva;
                $scope.Registro.retefuente = datac[0].retefuente;
                $scope.Registro.reteiva = datac[0].reteiva;
                $scope.Registro.reteica = datac[0].reteica;
                $scope.Registro.total = datac[0].total;

                $scope.BuscarProveedor(datac[0].documento, 0, 0);

                setTimeout(function () {
                    $(".ComboRegistro").trigger("change");
                }, 200);

                DesactivarLoad();

            } else {
                DesactivarLoad();
                $scope.Compra = "";
                $scope.Registro.compra = "";
                $("#Compra").focus();
                swal("Acción Cancelada", "El numero " + compra + " no se encuentra registrado como " + tipo + " de compra", "warning");
            }
        }, 15);
    }

    $scope.LimpiarTodo = function () {
        $scope.Documento = "";
        $scope.DocumentoPro = "";
        $scope.Compra = "";
        $scope.Orden = "";
        $scope.LimpiarDatos();
    }

    $scope.LimpiarDatos = function () {
        $scope.Proveedor = {documento: ""};
        $scope.InicializarRegistro();
    }

    $scope.ModalArticulosCom = function () {
        if ($scope.Registro.proveedor > 0 && $scope.Registro.id == 0) {
            $("#modalInventarioCom").modal("show");
        }
    }

    $scope.ModalORegistroCom = function () {
        if ($scope.Registro.proveedor > 0 && $scope.Registro.id == 0) {
            $("#ModalOtroRegistroCom").modal("show");
            $scope.InicializarOtroRegistro();
        }
    }

    $("#TablaInventarioCom > tbody").on("dblclick", "tr", function (e) {
        var row = $(this).parents("td").context.cells;
        var descripcion = "CODIGO: " + row[2].innerText + ", TIPO: " + row[5].innerText + ", GRUPO: " + row[4].innerText +
                ", ARTICULO: " + row[6].innerText + ", MARCA: " + row[7].innerText + ", MODELO: " + row[8].innerText + ", PRESENTACION: " + row[9].innerText;

        var parametro = "idCompra=" + $scope.Registro.compra + "&proveedor=" + $scope.Registro.proveedor + " &id=0&descripcion=" + _escape(descripcion) +
                "&iva=" + NumeroDecimal(row[17].innerText) + "&cuenta=" + row[18].innerText + "&idcentro=0" +
                "&cantidad=1&descuento=0&precio=" + NumeroDecimal(row[12].innerText) + "&subtotal=" + NumeroDecimal(row[12].innerText) + "&idarticulo=" + row[1].innerText;
        var datos = LlamarAjax("Compras", "opcion=GuardarDetCompra&" + parametro + "&orden=" + $scope.Registro.compra);
        datos = datos.split("|");
        if (datos[0] == "0") {
            $("#modalInventarioCom").modal("hide");
            swal("", datos[1], "success");
            $scope.TablaCompras(1);
            $scope.$apply();
        } else
            swal("Acción Cancelada", datos[1], "warning");
    });

    $scope.TablaCompras = function (totalizar) {
        var datos = LlamarAjax("Compras", "opcion=TablaCompra&compra=" + $scope.Registro.id + "&proveedor=" + $scope.Registro.proveedor);
        $scope.Registro.tabla = JSON.parse(datos);
        console.log($scope.Registro.tabla);
        if (totalizar == 1) {
            $scope.Totales();
            $scope.ActualizarCentro();
        }
    }

    $scope.CalcularPreTablaCom = function (caja, index) {
        ValidarTexto(caja, 3);
        $scope.Registro.tabla[index].subtotal = formato_numero((NumeroDecimal($scope.Registro.tabla[index].precio) * NumeroDecimal($scope.Registro.tabla[index].cantidad)) - (NumeroDecimal($scope.Registro.tabla[index].precio) * NumeroDecimal($scope.Registro.tabla[index].cantidad) * NumeroDecimal($scope.Registro.tabla[index].descuento) / 100), _DE, _CD, _CM, "");
        $scope.Totales();
    }

    $scope.CalcularPOSerCom = function (caja) {
        if (caja)
            ValidarTexto(caja, 1);

        $scope.OtroRegistro.subtotal = formato_numero(NumeroDecimal($scope.OtroRegistro.cantidad) * NumeroDecimal($scope.OtroRegistro.costo), _DE, _CD, _CM, "");
        $scope.OtroRegistro.tdescuento = formato_numero(NumeroDecimal($scope.OtroRegistro.subtotal) * NumeroDecimal($scope.OtroRegistro.descuento) / 100, _DE, _CD, _CM, "");
        $scope.OtroRegistro.tiva = formato_numero((NumeroDecimal($scope.OtroRegistro.subtotal) - NumeroDecimal($scope.OtroRegistro.tdescuento)) * NumeroDecimal($scope.OtroRegistro.iva) / 100, _DE, _CD, _CM, "");
        $scope.OtroRegistro.total = formato_numero(NumeroDecimal($scope.OtroRegistro.subtotal) - NumeroDecimal($scope.OtroRegistro.tdescuento) + NumeroDecimal($scope.OtroRegistro.tiva), _DE, _CD, _CM, "");

    }

    $scope.CambioTemporal = function () {
        $scope.Registro.temporal = 1;
    }

    $scope.GuardarTempCompra = function (registro) {
        if ($scope.Registro.temporal == 0 || registro.idcompra * 1 > 0) {
            return false;
        }
        var descripcion = _escape(registro.descripcion);
        var parametro = "idCompra=" + $scope.Registro.compra + "&proveedor=" + $scope.Registro.proveedor + " &id=" + registro.id + "&descripcion=" + descripcion +
                "&iva=" + NumeroDecimal(registro.iva) + "&cuenta=" + registro.idcuecontable + "&idcentro=" + registro.idcentro +
                "&cantidad=" + NumeroDecimal(registro.cantidad) + "&descuento=" + NumeroDecimal(registro.descuento) + "&precio=" + NumeroDecimal(registro.precio) + "&subtotal=" + NumeroDecimal(registro.subtotal) + "&idservicio=0&idarticulo=" + registro.idarticulo;
        var datos = LlamarAjaxAsy("Compras", "opcion=GuardarDetCompra&" + parametro + "&orden=", 1, "|");
        $scope.Registro.temporal = 0;

    }

    $scope.EditarORegistroCom = function (registro, index) {
        console.log(registro.idservicio, $scope.Registro.id)
        if ((registro.idservicio > 0) && ($scope.Registro.id == 0)) {
            $("#ModalOtroRegistroCom").modal("show");
            $scope.OtroRegistro.id = registro.id;
            $scope.OtroRegistro.ingreso = registro.ingreso;
            $scope.OtroRegistro.servicio = $.trim(registro.idservicio);
            $scope.OtroRegistro.descripcion = registro.descripcion;
            $scope.OtroRegistro.cantidad = registro.cantidad;
            $scope.OtroRegistro.descuento = registro.descuento;
            $scope.OtroRegistro.costo = registro.precio;
            $scope.OtroRegistro.iva = $.trim(parseInt(registro.iva));
            $scope.OtroRegistro.centrocosto = $.trim(registro.idcentro);
            $scope.OtroRegistro.posicion = index;
            setTimeout(function () {
                $(".ComboOtroRegistro").trigger("change");
            }, 200);
            $scope.CalcularPOSerCom();
        }
    }

    $scope.AgregarOtroRegistroCom = function () {

        var mensaje = "";
        var descripcion = $.trim(_escape($scope.OtroRegistro.descripcion));

        if (NumeroDecimal($scope.OtroRegistro.cantidad) == 0) {
            $("#ComOCantidad").focus();
            mensaje = "<br> Debe de ingresar la cantidad del registro " + mensaje;
        }
        if (NumeroDecimal($scope.OtroRegistro.costo) == 0) {
            $("#ComOCosto").focus();
            mensaje = "<br> Debe de ingresar el costo del registro" + mensaje;
        }
        if (descripcion == "") {
            $("#ComODescripcion").focus();
            mensaje = "<br> Debe de ingresar la descripción del registro " + mensaje;
        }
        if (mensaje != "") {
            swal("Acción Cancelada", mensaje, "warning");
            return false;
        }

        var parametro = "ingreso=" + $scope.OtroRegistro.ingreso + "&idCompra=" + $scope.Registro.compra + "&proveedor=" + $scope.Registro.proveedor + " &id=" + $scope.OtroRegistro.id + "&descripcion=" + descripcion +
                "&iva=" + NumeroDecimal($scope.OtroRegistro.iva) + "&cuenta=&idcentro=" + $scope.OtroRegistro.centrocosto +
                "&cantidad=" + NumeroDecimal($scope.OtroRegistro.cantidad) + "&descuento=" + NumeroDecimal($scope.OtroRegistro.descuento) + "&precio=" + NumeroDecimal($scope.OtroRegistro.costo) + "&subtotal=" + NumeroDecimal($scope.OtroRegistro.subtotal) + "&idarticulo=0&idservicio" + $scope.OtroRegistro.servicio;
        var datos = LlamarAjax("Compras", "opcion=GuardarDetCompra&" + parametro + "&orden=").split("|");
        if (datos[0] == "0") {
            $("#ModalOtroRegistroCom").modal("hide");
            swal("", datos[1], "success");
            if ($scope.OtroRegistro.posicion == -1) {
                $scope.TablaCompras();
            } else {
                var index = $scope.OtroRegistro.posicion;
                $scope.Registro.tabla[index].ingreso = $scope.OtroRegistro.ingreso;
                $scope.Registro.tabla[index].idservicio = $scope.OtroRegistro.servicio;
                $scope.Registro.tabla[index].descripcion = $scope.OtroRegistro.descripcion;
                $scope.Registro.tabla[index].cantidad = formato_numero($scope.OtroRegistro.cantidad, 3, _CD, _CM, "");
                $scope.Registro.tabla[index].descuento = $scope.OtroRegistro.descuento;
                $scope.Registro.tabla[index].precio = $scope.OtroRegistro.costo;
                $scope.Registro.tabla[index].iva = formato_numero($scope.OtroRegistro.iva, 2, _CD, _CM, "");
                $scope.Registro.tabla[index].idcentro = $scope.OtroRegistro.centrocosto;
                $scope.Totales();
            }
        } else
            swal("Acción Cancelada", datos[1], "warning");

    }

    $scope.CalcularFechas = function () {
        $scope.Registro.diaspago = 0;
        if ($.trim($scope.Registro.fecha) == "" || $.trim($scope.Registro.fechavencimiento) == "" || $scope.Registro.fechavencimiento == null)
            return false;
        let fecha = new Date($scope.Registro.fecha);
        let vencimiento = new Date($scope.Registro.fechavencimiento);
        let resta = vencimiento.getTime() - fecha.getTime();
        $scope.Registro.diaspago = Math.round(resta / (1000 * 60 * 60 * 24));
    }

    $scope.GuardarCompras = function () {
        if ($scope.Registro.proveedor == 0 || $scope.Registro.compra > 0) {
            return false;
        }
        if ($scope.Registro.tcantidad == 0) {
            swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un item de compra", "warning");
            return false;
        }
        if ($.trim($scope.Registro.concepto) == "") {
            $("#Concepto").focus();
            swal("Acción Cancelada", "Debe de ingresar el concepto de la compra", "warning");
            return false;
        }
        if ($.trim($scope.Registro.bodega) == "") {
            $("#Bodega").focus();
            swal("Acción Cancelada", "Debe de selecciona una ubicación de depósito", "warning");
            return false;
        }

        if ($scope.Registro.fecha == "") {
            $("#FechaCom").focus();
            swal("Acción Cancelada", "Debe de ingresar la fecha de compra", "warning");
            return false;
        } else {
            if ($scope.Registro.fecha > output2) {
                $("#FechaCom").focus();
                swal("Acción Cancelada", "La fecha de compra no puede ser mayor a " + output2, "warning");
                return false;
            }
        }

        if ($.trim($scope.Registro.facturaexterna) == "") {
            $("#FacturaExterna").focus();
            swal("Acción Cancelada", "Debe de ingresar el número de factura de compra", "warning");
            return false;
        }

        if ($scope.Registro.fechavencimiento == "" || $scope.Registro.fechavencimiento == null) {
            $("#FechaVencimiento").focus();
            swal("Acción Cancelada", "Debe de ingresar la fecha de vencimiento de la factura", "warning");
            return false;
        }

        if (Obtener_Fecha($scope.Registro.fecha) > Obtener_Fecha($scope.Registro.fechavencimiento)) {
            $("#FechaVencimiento").focus();
            swal("Acción Cancelada", "La fecha de vencimiento debe ser mayor o igual a la fecha de compra", "warning");
            return false;
        }

        if ($scope.Registro.tcantidad == 0) {
            swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un articulo/servicio", "warning");
            return false;
        }

        var a_precio = "array[";
        var a_descuento = "array[";
        var a_iva = "array[";
        var a_cantidad = "array[";
        var a_subtotal = "array[";
        var a_articulo = "array[";
        var a_centro = "array[";
        var a_id = "array[";

        for (var i = 0; i < $scope.Registro.tabla.length; i++) {
            if ($scope.Registro.tabla[i].seleccionado) {
                if (NumeroDecimal($scope.Registro.tabla[i].subtotal) == 0) {
                    $("#tprecio" + i).focus();
                    swal("Acción Cancelada", "Debe de Ingresar la cantidad o el valor unitario del item número " + (i + 1), "warning");
                    return false;
                }

                /*if (centros[x].value * 1 == 0) {
                 $("#tcentrol" + x).focus();
                 swal("Acción Cancelada", "Debe de seleccionar el centro de costo del ítem número " + (x + 1), "warning");
                 return false;
                 }*/

                if (a_id != "array[") {
                    a_id += ",";
                    a_articulo += ",";
                    a_precio += ",";
                    a_descuento += ",";
                    a_iva += ",";
                    a_cantidad += ",";
                    a_subtotal += ",";
                    a_centro += ",";
                }
                a_id += $scope.Registro.tabla[i].id;
                a_articulo += $scope.Registro.tabla[i].idarticulo;
                a_precio += NumeroDecimal($scope.Registro.tabla[i].precio);
                a_descuento += NumeroDecimal($scope.Registro.tabla[i].descuento);
                a_iva += NumeroDecimal($scope.Registro.tabla[i].iva);
                a_cantidad += NumeroDecimal($scope.Registro.tabla[i].cantidad);
                a_subtotal += NumeroDecimal($scope.Registro.tabla[i].subtotal);
                a_centro += $scope.Registro.tabla[i].idcentro;
            }
        }
        a_articulo += "]";
        a_precio += "]";
        a_cantidad += "]";
        a_descuento += "]";
        a_iva += "]";
        a_id += "]";
        a_subtotal += "]";
        a_centro += "]";

        var mensaje = "<br><b>¿DESEA GUARDAR LA COMPRA?</b>";

        /*if (recibo > 0) {
         if (totales > pendiente) {
         mensaje += "<br>El monto a facturar es mayor al monto del saldo del recibo... <b>Se generará un comprobante a favor del tercerizado</b>";
         }
         }*/

        swal.queue([{
                title: ValidarTraduccion('Advertencia'),
                text: mensaje,
                type: 'question',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                confirmButtonText: ValidarTraduccion('Facturar Compra'),
                cancelButtonText: ValidarTraduccion('Cancelar'),
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        var parametros = "id=" + $scope.Registro.id + "&proveedor=" + $scope.Registro.proveedor + "&observacion=" + $.trim($scope.Registro.observacion) +
                                "&subtotal=" + $scope.Registro.subtotal + "&descuento=" + $scope.Registro.descuento + "&gravable=" + $scope.Registro.gravable + "&iva=" + $scope.Registro.iva + "&total=" + $scope.Registro.total + "&exento=" + $scope.Registro.exento +
                                "&reteiva=" + $scope.Registro.reteiva + "&retefuente=" + $scope.Registro.retefuente + "&reteica=" + $scope.Registro.reteica + "&orden=" + $scope.Registro.orden +
                                "&a_precio=" + a_precio + "&a_descuento=" + a_descuento + "&a_subtotal=" + a_subtotal + "&a_id=" + a_id + "&bodega=" + $scope.Registro.bodega +
                                "&a_cantidad=" + a_cantidad + "&a_iva=" + a_iva + "&a_articulo=" + a_articulo + "&fechacom=" + Obtener_Fecha($scope.Registro.fecha) + "&vencimiento=" + Obtener_Fecha($scope.Registro.fechavencimiento) + "&factura=" + $scope.Registro.facturaexterna +
                                "&tipo=" + $scope.Registro.tipo + "&recibo=" + $scope.Registro.recibo + "&plazopago=" + $scope.Proveedor.plazopago + "&a_centro=" + a_centro + "&concepto=" + $.trim($scope.Registro.concepto) +
                                "&porreteiva=" + $scope.Proveedor.reteiva + "&porretefuente=" + $scope.Proveedor.retefuente + "&porreteica=" + $scope.Proveedor.reteica;
                        $.post(url_servidor + "Compras", "opcion=GuardarCompra&" + parametros)
                                .done(function (data) {
                                    datos = data.split("|");
                                    if (datos[0] == "0") {
                                        swal("", datos[1] + " " + datos[2], "success");
                                        $scope.Registro.Compra = datos[2];
                                        $scope.Compra = datos[2];
                                        $scope.Registro.id = datos[3] * 1;
                                        for (var i = 0; i < $scope.Registro.tabla.length; i++) {
                                            $scope.Registro.tabla[i].idcompra = $scope.Registro.id
                                            $scope.Registro.tabla[i].class_select = "";
                                        }
                                        $scope.Registro.disabled = true;
                                        $scope.Registro.btnguardar = false;
                                        $scope.Registro.btnanular = true;
                                        $scope.Registro.btnimprimir = true;
                                        $scope.Registro.btnanexo = true;
                                        $scope.$apply();
                                        //ImprimirCompra(Compra, tipo);
                                        resolve();
                                    } else {
                                        reject(datos[1]);
                                    }
                                })
                    })
                }
            }]);

        $(".swal2-content").html(mensaje);





    }

    $scope.EliminarRegistroCom = function (registro, index) {
        console.log(index);
        if ($scope.Registro.id == 0) {
            var datos = LlamarAjax("Compras", "opcion=EliminarRegistrosSel&seleccion=" + registro.id + "&idCompra=" + registro.idcompra).split("|");
            if (datos[0] == "0") {
                $scope.Registro.tabla.splice(index, 1);
                swal("", "Registros eliminados", "success");
            } else
                swal("Acción Cancelada", datos[1], "warning");
        }
    }

    $scope.CambioFecha = function (fecha, tipo) {
        switch (tipo) {
            case 1:
                $scope.Consulta.fechad.id = Obtener_Fecha(fecha);
                break;
            case 2:
                $scope.Consulta.fechah.id = Obtener_Fecha(fecha);
                break;
        }
    }

    $scope.ConsultarCompras = function () {
        console.log($scope.Consulta);
        var parametros = ConvertirParametros($scope.Consulta);
        if ($scope.Consulta.fechad.id == "" || $scope.Consulta.fechah.id == "") {
            swal("Acción Cancelada", ValidarTraduccion("Debe de ingresar fecha de inicio y fecha final"), "warning");
            return false;
        }
        var parametros = ConvertirParametros($scope.Consulta);
        ActivarLoad();
        setTimeout(function () {
            var parametros = ConvertirParametros($scope.Consulta);
            var datos = LlamarAjax("Compras", "opcion=ConsultarCompra&" + parametros);
            DesactivarLoad();

            var datajson = JSON.parse(datos);
            $('#TablaCompra').DataTable({
                data: datajson,
                bProcessing: true,
                bDestroy: true,
                columns: [
                    {"data": "imprimir"},
                    {"data": "tipo"},
                    {"data": "compra", "className": "text-right text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '')},
                    {"data": "orden"},
                    {"data": "fecha"},
                    {"data": "vencimiento"},
                    {"data": "bodega"},
                    {"data": "proveedor"},
                    {"data": "plazopago"},
                    {"data": "tiempo"},
                    {"data": "direccion"},
                    {"data": "ciudad"},
                    {"data": "email"},
                    {"data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '')},
                    {"data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '')},
                    {"data": "iva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '')},
                    {"data": "retefuente", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '')},
                    {"data": "reteiva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '')},
                    {"data": "reteica", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '')},
                    {"data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '')},
                    {"data": "saldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '')},
                    {"data": "estado"},
                    {"data": "registro"},
                    {"data": "anula"}
                ],

                "language": {
                    "url": LenguajeDataTable
                },
                dom: 'Bfrtip',
                buttons: [
                    'excel', 'csv', 'copy', 'colvis'
                ]
            });
        }, 15);
    }
    
    var nombre = "Pedro";
    

    $scope.InicializarRegistro();
    $scope.InicializarOtroRegistro();
    $scope.CargarCombos();
    InicializarFormulario("ul_compra", "men_compra");
    $scope.setActiveTab(1);

});
