﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))

var calreteiva = 0;
var calreteica = 0;
var calretefuente = 0;
var ReteFuente = 0;
var ReteIva = 0;
var ReteIca = 0;
var porreteiva = 0;
var porreteica = 0;
var porretefuente = 0;
var TableRadicacion = null;
var Facturas = "";
var SelecRadi = 0;
var ValorRadi = 0;
var Habitual = "";
var CentroCostos = CargarCombo(61, 1);
$("#FacCentro").html(CentroCostos);

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;


$("#FechaFac").val(output2);
$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);

$("#COFechaDesde").val(output);
$("#COFechaHasta").val(output2);

$("#ICFechaDesde").val(output);
$("#ICFechaHasta, #FechaRadi").val(output2);

$("#RFechaDesde").val(output);
$("#RFechaHasta").val(output2);

$("#RRFechaDesde").val(output);
$("#RRFechaHasta").val(output2);

$("#CRFechaDesde").val(output);
$("#CRFechaHasta").val(output2);

var datos = LlamarAjax("Configuracion","opcion=CargaComboInicial&tipo=5").split("||");

$("#CMagnitud, #ICMagnitud, #COMagnitud").html(datos[0]);
$("#CEquipo, #ICEquipo, #COEquipo").html(datos[1]);
$("#CMarca, #ICMarca, #COMarca").html(datos[2]);
$("#CCliente, #ICliente, #ICCliente, #RCliente, #RRCliente, #COCliente, #CRCliente, #FSCliente, #RICliente").html(datos[3]);
$("#CModelo, #ICModelo, #COModelo").html(datos[4]);
$("#CIntervalo, #ICIntervalo, #COIntervalo").html(datos[5]);
$("#CUsuario, #ICUsuario, #Mensajero, #COUsuario").html(datos[6]);

$("#FacOIva").html(datos[7]);
$("#FacOServicio").html(datos[8]);

$("#FacGrupo, #FGrupo").html(CargarCombo(47, 1));
$("#FUbicacion").html(CargarCombo(68, 0));


var IdCliente = 0;
var IdFactura = 0;
var Factura = 0;
var totales = 0;
var subtotal = 0;
var descuento = 0;
var exento = 0;
var gravable = 0;
var iva = 0;
var retfuente = 0;
var retiva = 0;
var retica = 0;
var Orden = "0";
var DocumentoCli = "";
var totalfila = 0;
var Estado = "Temporal";
var Pdf_Factura = "";
var CorreoEmpresa = "";
var ValorIva = 0;

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Cliente") {
            if ($.trim($(this).val()) != "") {

                BuscarCliente($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}


function GuardarDocumento() {
    var documento = document.getElementById("documento");
    if ($.trim(documento.value) != "") {
        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Cotizacion/AdjuntarExcel";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "error");
                }
            }
        });
    }
}


function SubirArchivo() {
    swal.queue([{
        html: '<h3>' + ValidarTraduccion("Seleccione el archivo de excel") + '</h3><font size="2px">Columnas (Fecha[yyyy-MM-dd], Ingreso, factura(sólo números)</font><br><input type="file" class="form-control" id="documento" onchange="GuardarDocumento()" name="documento" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required />',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Importar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                var datos = LlamarAjax("Facturacion","opcion=ImportarExcelFactura").split("|");
                if (datos[0] == "0")
                    resolve();
                else
                    reject(datos[1]);
            })
        }
    }]);
}

function LimpiarRelacion(tipo) {
    if (tipo == 0)
        $("#ICliente").val("").trigger("change");
    else
        $("#ICliente").val($("#ICliente").val()).trigger("change");
    $("#IId").val("0");
    $("#IFactura").val("");
    $("#IFecha").val("");
    $("#IIngresos").val("").trigger("change");
    $("#ICliente").prop("disabled", false);
    $("#ISubTotal").val("");
    $("#IDescuento").val("");
    $("#IAjuste").val("");
    $("#ISuministro").val("");
}

function EnviarFactura() {
    if (Factura == 0)
        return false;

    if (Estado != "Contabilizado" && Estado != "Anulado") {
        swal("Acción Cancelada", "No se puede enviar una cotización en estado " + Estado, "warning");
        return false;
    }
        
    if (CorreoEmpresa == "") {
        swal("Acción Cancelada", "Cliente no posee ningún correo principal registrado", "warning");
        return false;
    }
    $("#eCliente").val($("#NombreCliente").val());
    $("#eCorreo").val($("#Email").val());
    ImprimirFactura(Factura, 1);
    $("#spfactura").html(Factura);
    $("#enviar_factura").modal("show");
}

$("#formenviarfactura").submit(function (e) {

    e.preventDefault();
    var observacion = $.trim($("#ObserEnvio").val())
    var correo = $.trim($("#eCorreo").val());
    a_correo = correo.split(";");

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#eCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo inválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }
        

    ActivarLoad();
    setTimeout(function () {
        var parametros = "factura=" + Factura + "&principal=" + CorreoEmpresa + "&e_email=" + correo + " &archivo=" + Pdf_Factura + " &cliente=" + $("#eCliente").val() + "&observacion=" + observacion;
        var datos = LlamarAjax("Facturacion","opcion=EnvioFactura&"+ parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            $("#enviar_factura").modal("hide");
            Estado = "Enviado";
            $("#Estado").val(Estado);
            BuscarFactura(Factura);
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);

    return false;
});

$("#FormRelacionarFactura").submit(function (e) {
    e.preventDefault();
    var parametros = $("#FormRelacionarFactura").serialize().replace(/\%2C/g, "") + "&operacion=1";
    var datos = LlamarAjax("Facturacion","opcion=OperacionImportarFactura&"+ parametros).split("|");
    if (datos[0] == "0") {
        LimpiarRelacion(1);
        swal("", datos[1], "success");
        TablaFacturaImportada($("#ICliente").val());
    }
    else {
        swal("Acción Cancelada", datos[1], "warning");
    }
    return false;
});

function EliminarImporFactura(id, ingreso, factura) {
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar la factura ' + factura + ' del ingreso número ' + ingreso + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Facturacion","opcion=OperacionImportarFactura&IId=" + id + "&operacion=2&IIngresos=" + ingreso + "&IFactura=0&ISubTotal=0&IDescuento=0&IAjuste=0&ISuministro=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            TablaFacturaImportada($("#ICliente").val());
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}


$("#TablaRelacionFac > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var id = row[0].innerText;
    var ingreso = row[1].innerText;
    var cliente = row[2].innerText;
    var factura = row[5].innerText;
    var fecha = CambiarCaracter($.trim(row[6].innerText), '/', '-');
    var subtotal = row[7].innerText;
    var descuento = row[8].innerText;
    var ajuste = row[9].innerText;
    var suministro = row[10].innerText;

    $("#ICliente").prop("disabled", true);
    
    $("#ICliente").val(cliente).trigger("change");
    $("#IIngresos").html("<option value=" + ingreso + ">" + ingreso + "</option>");
    $("#IIngresos").val(ingreso).trigger("change");
    $("#IFactura").val(factura);
    $("#IFecha").val(fecha);
    $("#IId").val(id);
    $("#ISubTotal").val(formato_numero(subtotal, 0,_CD,_CM,""));
    $("#IDescuento").val(formato_numero(descuento, 0,_CD,_CM,""));
    $("#IAjuste").val(formato_numero(ajuste, 0,_CD,_CM,""));
    $("#ISuministro").val(formato_numero(suministro, 0,_CD,_CM,""));
    $("#IFactura").select();
    $("#IFactura").focus();
});

function ConsularFacIngresos() {


    var magnitud = $("#ICMagnitud").val() * 1;
    var equipo = $("#ICEquipo").val() * 1;
    var modelo = $("#ICModelo").val() * 1;
    var marca = $("#ICMarca").val() * 1;
    var intervalo = $("#ICIntervalo").val() * 1;
    var ingreso = $.trim($("#ICIngreso").val());
    var serie = $.trim($("#ICSerie").val());

    var recepcion = $("#ICRecepcion").val();

    var remision = $("#ICRemision").val() * 1;
    var cliente = $("#ICCliente").val() * 1;
    var usuario = $("#ICUsuario").val() * 1;

    var fechad = $("#ICFechaDesde").val();
    var fechah = $("#ICFechaHasta").val();

    if (fechad == "" && fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar una fecha o números de días"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "remision=" + remision + "&cliente=" + cliente + "&ingresos=" + ingreso +
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&usuario=" + usuario + "&recepcion=" + recepcion;
        var datos = LlamarAjax("Facturacion","opcion=ConsultarIngresoFac&"+ parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaFacIngresos').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "ingreso" },
                { "data": "remision" },
                { "data": "recepcion" },
                { "data": "cliente" },
                { "data": "direccion" },
                { "data": "contacto" },
                { "data": "magnitud" },
                { "data": "equipo" },
                { "data": "marca" },
                { "data": "modelo" },
                { "data": "rango1" },
                { "data": "rango2" },
                { "data": "rango3" },
                { "data": "serie" },
                { "data": "observacion" },
                { "data": "fecha" },
                { "data": "usuario" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ],
            "colReorder": true
        });
    }, 15);
}



function ClientesIngresos(cliente) {
    $("#IIngresos").html("");
    TablaFacturaImportada(cliente * 1);
    if (cliente * 1 == 0)
        return false;
    var resultado = LlamarAjax("Facturacion","opcion=ClientesIngresos&cliente=" + cliente);
    if (resultado != "XX")
    if (resultado != "XX")
        $("#IIngresos").html(resultado);
}

function TablaFacturaImportada(cliente) {
    var datos = LlamarAjax("Facturacion", "opcion=TablaFacturaImportada&cliente=" + cliente);
    var datajson = JSON.parse(datos);
    $('#TablaRelacionFac').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "ingreso", "className": "text-XX" },
            { "data": "idcliente" },
            { "data": "cliente" },
            { "data": "equipo" },
            { "data": "factura" },
            { "data": "fecha" },
            { "data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "ajuste", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "suministro", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        },
        dom: 'Bfrtip',
        buttons: [
            'excel', 'csv', 'copy','colvis'
        ],
        "colReorder": true
    });
}

TablaFacturaImportada(0);

function AnularFactura() {

    if (IdFactura == 0)
        return false;

    var resultado = "";
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Desea Anular la Factura') + ' ' + Factura + ' ' + ValidarTraduccion('del cliente') + ' ' + $("#NombreCliente").val(),
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Anular'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Facturacion","opcion=AnularFactura&id=" + IdFactura + "&observaciones=" + value)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resultado = Factura;
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar un observación'));
                }
            })
        }
    }).then(function (result) {
        LimpiarTodo();
        IdCliente = 0;
        $("#Cliente").val("");
        $("#Cliente").focus();
        swal({
            type: 'success',
            html: 'Factura Nro ' + resultado + ' anulada con éxito'
        })
    })

}

function LimpiarTodo() {
    LimpiarDatos();
    $("#Cliente").val("");
    $("#Cliente").focus();
}

LimpiarTodo();

function LimpiarDatos() {
    Factura = 0;
    IdFactura = 0;
    IdCliente = 0;

    $("#seleccionatodo").prop("checked", "");
    DocumentoCli = "";
    TipoCliente = 0;
    PlazoPago = 0;
    TipoPrecio = 0;
    totales = 0;
    subtotal = 0;
    descuento = 0;
    exento = 0;
    gravable = 0;
    iva = 0;
    retfuente = 0;
    retiva = 0;
    retica = 0;

    saldo = 0;
    tarjeta = 0;
    efectivo = 0;
    cheque = 0;
    vuelta = 0;
    recibido = 0;

    calreteiva = 0;
    calreteica = 0;
    calretefuente = 0;


    TotalSeleccion = 0;
    Orden = 0;

    $("#NombreCliente").val("");
    $("#Orden").val("").trigger("change");
    $("#Direccion").val("");
    $("#SaldoClienteDev").html("");

    $("#Orden").html("");
    
    $("#Ciudad").val("");
    
    $("#Totales").val("0");
    $("#Factura").val("");

    $("#bodyFactura").html("");

    $("#Email").val("");
    $("#Telefonos").val("");
    $("#TipoCliente").val("");

    $("#tsubtotal").val("0");
    $("#tdescuento").val("0");
    $("#tbaseimponible").val("0");
    $("#texcento").val("0");
    $("#tiva").val("0");
    $("#tretefuente").val("0");
    $("#treteiva").val("0");
    $("#treteica").val("0");
    $("#ttotalpagar").val("0");
    Estado = "Temporal";
    $("#Estado").val(Estado);

    $("#TipoCliente").val("");
    $("#PlazoPago").val("0");
    $("#TablaPrecio").val("");
    $("#FechaReg").val("");
    $("#FechaVen").val("");
    $("#Saldo").val("0");
    $("#UsuarioReg").val("");
    $("#Observaciones").val("");
    $("#Factura").prop("disabled", false);

    $("#TbContabilidad").html("");

}

function ValidarCliente(documento) {
    if ($.trim(DocumentoCli) != "") {
        if (DocumentoCli != documento) {
            DocumentoCli = "";
            LimpiarDatos();
        }
    }
}



$("#TablaFactura > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText.replace(",","");
    $('#tabfacturas a[href="#tabcrear"]').tab('show')
    $("#Factura").val(numero);
    BuscarFactura(numero);
});

$("#tablamodalclientefac > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#modalClientesFac").modal("hide");
    $("#Cliente").val(numero);
    BuscarCliente(numero);
});

function Facturas_Ordenes(cliente) {
    var datos = LlamarAjax("Facturacion","opcion=FacturaOrdenes&cliente=" + cliente + "&habitual=" + Habitual);
    $("#Orden").html(datos);
}

function AgregarOtroRegistro() {

    var id = $("#FacOIdRegistro").val() * 1;
    var descripcion = _escape($.trim($("#FacODescripcion").val()));
    var cantidad = $("#FacOCantidad").val() * 1;
    var descuento = $("#FacODescuento").val() * 1;
    var servicio = $("#FacOServicio").val() * 1;
    var precio = NumeroDecimal($("#FacOPrecio").val()) * 1;
    var subtotal = NumeroDecimal($("#FacOVSubTotal").val()) * 1;
    var orden = $("#Orden").val();
    var iva = $("#FacOIva").val() * 1;
    var ingreso = $.trim($("#FacOIngreso").val());
    var centro = $("#FacCentro").val()*1;

    if (orden == "") {
        $("#Orden").focus();
        swal("Acción Cancelada", "Debe seleccionar una órden de compra", "warning");
        return false;
    }

    var mensaje = "";
    if (centro == 0) {
        $("#FacCentro").focus();
        mensaje = "<br> Debe de seleccionar un centro de costo" + mensaje;
    }
    if ($.trim($("#FacODescuento").val()) == "") {
        $("#FacODescuento").focus();
        mensaje = "<br> Debe de ingresar el descuento del registro" + mensaje;
    }

    if (cantidad == 0) {
        $("#FacOCantidad").focus();
        mensaje = "<br> Debe de ingresar la cantidad del registro " + mensaje;
    }
    if (precio == 0) {
        $("#FacOPrecio").focus();
        mensaje = "<br> Debe de ingresar el precio del registro" + mensaje;
    }
    if (descripcion == "") {
        $("#FacODescripcion").focus();
        mensaje = "<br> Debe de ingresar la descripción del registro " + mensaje;
    }
    if (servicio == 0) {
        $("#FacOServicio").focus();
        mensaje = "<br> Debe de seleccionar un tipo de servicio " + mensaje;
    }
    if (ingreso == "") {
        $("#FacOIngreso").focus();
        mensaje = "<br> Debe de ingresar el número de ingreso " + mensaje;
    }
    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    descripcion = descripcion.replace(/&/g, '%26');

    var parametro = "ingreso=" + ingreso + "&cliente=" + IdCliente + "&orden=" + orden + "&id=" + id + "&descripcion=" + descripcion + "&iva=" + iva +
        "&cantidad=" + cantidad + "&descuento=" + descuento + "&precio=" + precio + "&subtotal=" + subtotal + "&tipo=1" + "&idfactura=" + IdFactura + "&idcentro=" + centro + "&servicio=" + servicio + 
        "&idarticulo=0&cuentacontable=0";
    var datos = LlamarAjax("Facturacion","opcion=GuardarDetFactura&"+ parametro);
    datos = datos.split("|");
    if (datos[0] == "0") {
        $("#ModalOtroRegistroFac").modal("hide");
        Estado = "Temporal";
        $("#Estado").val(Estado);
        swal("", datos[1], "success");
        TablaFacturacion(orden);
        ContabilizarFactura();
    } else
        swal("Acción Cancelada", datos[1], "warning");
}

function CalcularPOSer(caja) {
    ValidarTexto(caja, 1);
    var cantidad = NumeroDecimal($("#FacOCantidad").val()) * 1;
    var precio = NumeroDecimal($("#FacOPrecio").val()) * 1;
    var descuento = NumeroDecimal($("#FacODescuento").val()) * 1;
    var iva = $("#FacOIva").val() * 1;

    var subtotal = (cantidad * precio) - ((cantidad * precio) * descuento / 100);

    $("#FacOVSubTotal").val(formato_numero(subtotal, 0,_CD,_CM,""));
    $("#FacOVIva").val(formato_numero(subtotal * iva / 100, 0,_CD,_CM,""));
    $("#FacOVDescuento").val(formato_numero(cantidad * precio * descuento / 100, 0,_CD,_CM,""));
    $("#FacOVTotal").val(formato_numero(subtotal + (subtotal * iva / 100), 0,_CD,_CM,""));
}

function BuscarCliente(documento) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoCli == documento)
        return false;

    DocumentoCli = documento;

    var parametros = "documento=" + $.trim(documento);
    var datos = LlamarAjax("Cotizacion","opcion=BuscarCliente&"+ parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        var data = JSON.parse(datos[1]);
        DesactivarLoad();
        IdCliente = data[0].id;
        PlazoPago = data[0].plazopago;
        TipoCliente = data[0].tipocliente;
        CorreoEmpresa = data[0].email;
        ValorIva = data[0].valor;
        Habitual = data[0].habitual;

        ReteFuente = data[0].retefuente;
        ReteIva = data[0].reteiva;
        ReteIca = data[0].reteica;

        porreteiva = data[0].porreteiva;
        porreteica = data[0].porreteica;
        porretefuente = data[0].porretefuente;
        
        $("#NombreCliente").val(data[0].nombrecompleto);
        $("#Ciudad").val(data[0].ciudad);
        $("#Departamento").val(data[0].departamento);
        $("#Direccion").val(data[0].direccion);
        $("#Email").val(data[0].email);
        
        $("#Telefonos").val(data[0].telefono + " / " + data[0].celular);
        
        Facturas_Ordenes(IdCliente);
        $("#Orden").val("AUTORIZACIÓN").trigger("change");

        $("#TipoCliente").val(TipoCliente);
        $("#PlazoPago").val(PlazoPago);
        $("#TablaPrecio").val(data[0].precio);

        $("#SaldoClienteDev").html(SaldoTotal(IdCliente));
        
    } else {
        DocumentoCli = "";
        $("#Cliente").val("");
        $("#Cliente").focus();
        swal("Acción Cancelada", "El documento " + documento + ", no se encuentra registrado como cliente", "warning");
    }
}

function SeleccionarTodo() {

    if ($("#seleccionatodo").prop("checked")) {
        for (var x = 0; x < totalfila; x++) {
            $("#fila-" + x).addClass("bg-gris");
            $("#seleccionado" + x).prop("checked", "checked");
        }
    } else {
        for (var x = 0; x < totalfila; x++) {
            $("#fila-" + x).removeClass("bg-gris");
            $("#seleccionado" + x).prop("checked", "");
        }
    }
    CalcularTotal();    
    ContabilizarFactura();
}

function SeleccionarFila(x) {

    if ($("#seleccionado" + x).prop("checked")) {
        $("#fila-" + x).addClass("bg-gris");
    } else {
        $("#fila-" + x).removeClass("bg-gris");
    }
    CalcularTotal();
    ContabilizarFactura();

}


function SeleccionarFilaRad(x, valor) {


    if ($("#seleccionradi" + x).prop("checked")) {
        SelecRadi++;
        ValorRadi += valor;
        $("#filarad-" + x).addClass("bg-gris");
    } else {
        SelecRadi = SelecRadi - 1;
        ValorRadi -= valor;
        $("#filarad-" + x).removeClass("bg-gris");
    }

    $("#SeleccionadoRad").html("<h3>Seleccionado: " + SelecRadi + ", Valor en Pesos: " + formato_numero(ValorRadi, 0,_CD,_CM,""));

}

function CalcularTotal() {

    totales = 0;
    subtotal = 0;
    descuento = 0;
    gravable = 0;
    iva = 0;

    var ingresos = document.getElementsByName("seleccion[]");
    var precios = document.getElementsByName("Precios[]");
    var ivas = document.getElementsByName("IVA[]");
    var cantidades = document.getElementsByName("Cantidades[]");
    var descuentos = document.getElementsByName("Descuentos[]");
    
    for (var x = 0; x < ingresos.length; x++) {
        if (ingresos[x].checked) {
            subtotal += (NumeroDecimal(precios[x].value) * NumeroDecimal(cantidades[x].value));
            descuento += (NumeroDecimal(precios[x].value) * NumeroDecimal(cantidades[x].value) * descuentos[x].value / 100);
            gravable += (NumeroDecimal(precios[x].value) * NumeroDecimal(cantidades[x].value)) - (NumeroDecimal(precios[x].value) * NumeroDecimal(cantidades[x].value) * descuentos[x].value / 100);
            iva += ((NumeroDecimal(precios[x].value) * NumeroDecimal(cantidades[x].value)) - (NumeroDecimal(precios[x].value) * NumeroDecimal(cantidades[x].value) * descuentos[x].value / 100)) * ivas[x].value / 100;
        }
    }
            
    retfuente = Math.trunc((subtotal - descuento) * porretefuente / 100);
    retica = Math.trunc((subtotal - descuento) * porreteica / 100);
    retiva = Math.trunc(iva * porreteiva / 100);
    
    totales = subtotal - descuento + iva - retfuente - retica - retiva;

    $("#tsubtotal").val(formato_numero(subtotal, 0,_CD,_CM,""));
    $("#tdescuento").val(formato_numero(descuento, 0,_CD,_CM,""));
    $("#tbaseimponible").val(formato_numero(gravable, 0,_CD,_CM,""));
    $("#texcento").val(formato_numero(exento, 0,_CD,_CM,""));
    $("#tiva").val(formato_numero(iva, 0,_CD,_CM,""));
    $("#tretefuente").val(formato_numero(retfuente, 0,_CD,_CM,""));
    $("#treteiva").val(formato_numero(retiva, 0,_CD,_CM,""));
    $("#treteica").val(formato_numero(retica, 0,_CD,_CM,""));
    $("#ttotalpagar").val(formato_numero(totales, 0,_CD,_CM,""));

}

function BuscarFactura(factura) {

    if (factura * 1 == 0) {
        return false;
    }

    var datos = LlamarAjax("Facturacion","opcion=BuscarFactura&factura=" + factura).split("|");
    if (datos[0] != "[]") {
        eval("data=" + datos);
        IdCliente = data[0].idcliente;
        PlazoPago = data[0].plazopago;
        Factura = factura;
        TipoCliente = data[0].tipocliente;
        IdFactura = data[0].id;
        CorreoEmpresa = data[0].email;
        Estado = data[0].estado;
        $("#Cliente").val(data[0].documento);
        DocumentoCli = data[0].documento
        $("#TipoCliente").val(TipoCliente);
        $("#NombreCliente").val(data[0].cliente);
        $("#Ciudad").val(data[0].ciudad);
        $("#Direccion").val(data[0].direccion);
        $("#Email").val(data[0].email);
        $("#Telefonos").val(data[0].telefono );
        $("#TablaPrecio").val(data[0].precio);
        $("#Orden").html("<option value='" + data[0].orden + "'>" + data[0].orden + "</option>");

        ReteFuente = data[0].idretefuente;
        ReteIva = data[0].idreteiva;
        ReteIca = data[0].idreteica;

        retfuente = data[0].retefuente;
        retiva = data[0].reteiva;
        retica = data[0].reteica;
        
        porretefuente = data[0].porretefuente;
        porreteica = data[0].porreteica;
        porreteiva = data[0].porreteiva;

        $("#Estado").val(data[0].estado);
        $("#DatosAnulacion").html(data[0].anulado);

        $("#TipoCliente").val(data[0].tipo);
        $("#PlazoPago").val(data[0].plazopago);
        $("#TablaPrecio").val(data[0].precio);
        $("#FechaFac").val(data[0].fecha)
        $("#FechaReg").val(data[0].fechareg);
        $("#FechaVen").val(data[0].vencimiento);
        $("#Saldo").val(formato_numero(data[0].saldo,_DE,_CD,_CM,""));
        $("#UsuarioReg").val(data[0].usuario);
        $("#Observaciones").val(data[0].observacion);

        $("#Factura").prop("disabled", true);
        
        totales = 0;
        subtotal = 0;
        descuento = 0;
        gravable = 0;
        iva = 0;
        totalfila = 0;

        var resultado = "";
                
                        
        eval("data=" + datos[1]);
        for (var x = 0; x < data.length; x++) {
            if (Estado == "Temporal" || Estado == "Facturado") {
                resultado += "<tr id='fila-" + x + "'" + (data[x].tipo == 1 ? "" : "ondblclick='BuscarDetalle(" + data[x].id + ")'") + " class='bg-gris'>" +
                    "<td class='text-center'><b>" + (x + 1) + "</b><br><input class='form-control' type='checkbox' checked value='" + data[x].ingreso + "' name='seleccion[]' id='seleccionado" + x + "' onchange='SeleccionarFila(" + x + ")'></td>" +
                    "<td><div name='Descripcion[]'>" + data[x].descripcion + "</div></td>" +
                    "<td><select class='form-control' name='CentroCosto[]' id='tcentro" + x + "' style='width:100%' onchange='ContabilizarFactura()'>" + CentroCostos + "</select></td>" +
                    "<td class='text-right text-XX'><input type='text' class='text-center sinborde' onfocus='FormatoEntrada(this, 1)' value='" + data[x].cantidad + "' id='tcantidad" + x + "' onchange='GuardarTemporal(" + x + "," + (data[x].tipo == 1 ? 0 : data[x].id) + ")' onblur='FormatoSalida(this,0)' onkeyup='CalcularPreTabla(this," + x + ")' name='Cantidades[]' style='width:99%' onchange='ContabilizarFactura()'></td>" +
                    "<td class='text-right text-XX'><input type='text' class='text-right sinborde' onfocus='FormatoEntrada(this, 1)' onchange='GuardarTemporal(" + x + "," + (data[x].tipo == 1 ? 0 : data[x].id) + ")' onblur='FormatoSalida(this,0)' onkeyup='CalcularPreTabla(this," + x + ")' value='" + formato_numero(data[x].precio, 0, _CD, _CM, "") + "' id='tprecio" + x + "' name='Precios[]' style='width:99%' onchange='ContabilizarFactura()'></td>" +
                    "<td class='text-right text-XX'><input type='text' class='text-right sinborde' onfocus='FormatoEntrada(this, 1)' onchange='GuardarTemporal(" + x + "," + (data[x].tipo == 1 ? 0 : data[x].id) + ")' onblur='FormatoSalida(this,0)' onkeyup='CalcularPreTabla(this," + x + ")' value='" + data[x].descuento + "' id='tdescuento" + x + "' name='Descuentos[]' style='width:99%' onchange='ContabilizarFactura()'></td>" +
                    "<td class='text-right text-XX'><input type='text' class='text-right sinborde' disabled value='" + formato_numero(data[x].iva, 0,_CD,_CM,"") + "' id='tiva" + x + "' name='IVA[]' style='width:99%'></td>" +
                    "<td class='text-right text-XX'><input type='text' class='text-right sinborde' disabled value='" + formato_numero(data[x].subtotal, 0,_CD,_CM,"") + "' id='tsubtotal" + x + "' name='Subtotales[]' style='width:99%'></td>" +
                    "<td class='text-center text-XX'>" + (data[x].ingreso > 0 ? "<a href=\"javascript:VerDocumentos(4,'" + data[x].remision + "')\">" + data[x].ingreso + "</a>" : "&nbsp;") + "</td>" +
                    "<td class='text-center text-XX'>" + (data[x].cotizacion > 0 ? "<a href=\"javascript:VerDocumentos(1,'" + data[x].cotizacion + "')\">" + data[x].cotizacion + "</a>" : "&nbsp;") + "</td>" +
                    "<td class='text-center text-XX'>" + (data[x].certificado ? "<a href=\"javascript:VerDocumentos(2,'" + data[x].ingreso + "')\">" + data[x].certificado + "</a>" : "") + "</td>" +
                    "<td class='text-center text-XX'>" + (data[x].orden ? "<a href=\"javascript:VerDocumentos(3,'" + data[x].orden + "')\">" + data[x].orden + "</a>" : "") +
                    "<input type='hidden' value='" + (data[x].cotizacion * 1) + "' name='Cotizaciones[]'>" +
                    "<input type='hidden' value='" + (data[x].orden ? data[x].orden : "") + "' name='Ordenes[]'>" +
                    "<input type='hidden' value='" + (data[x].certificado ? data[x].certificado : "") + "' name='Certificados[]'><input type='hidden' value='" + data[x].idcuecontable + "' name='Cuenta[]'><input type='hidden' value='" + data[x].idservicio + "' name='Servicio[]'><input type='hidden' value='" + data[x].idarticulo + "' name='Articulos[]'><input type='hidden' value='" + data[x].idbodega + "' name='Ubicacion[]'></td>" +
                    "<td>" + (data[x].tipo == 1 ? "&nbsp;" : "<button class='btn btn-danger' title='Eliminar Detalle' type='button' onclick=\"EliminarDetalle(" + data[x].id + "," + data[x].ingreso + ",'" + data[x].orden + "')\"><span data-icon='&#xe0d8;'></span></button>") + "</td></tr>";
                totalfila += 1;
            } else {
                resultado += "<tr>" +
                    "<td class='text-center'><b>" + (x + 1) + "</td>" +
                    "<td><div name='Descripcion[]'>" + data[x].descripcion + "</div></td>" +
                    "<td><select class='form-control' name='CentroCosto[]' id='tcentro" + x + "' style='width:100%' >" + CentroCostos + "</select></td>" +
                    "<td class='text-center text-XX'>" + data[x].cantidad + "</td>" +
                    "<td class='text-right text-XX'>" + formato_numero(data[x].precio, 0,_CD,_CM,"") + "</td>" +
                    "<td class='text-center text-XX'>" + data[x].descuento + "</td>" +
                    "<td class='text-center text-XX'>" + data[x].iva + "</td>" +
                    "<td class='text-right text-XX'><input type='text' class='text-right sinborde' disabled value='" + formato_numero(data[x].subtotal, 0, _CD, _CM, "") + "' id='tsubtotal" + x + "' name='Subtotales[]' disabled style='width:99%'></td>" +
                    "<td class='text-center text-XX'>" + (data[x].ingreso > 0 ? "<a href=\"javascript:VerDocumentos(4,'" + data[x].remision + "')\">" + data[x].ingreso + "</a>" : "&nbsp;") + "</td>" +
                    "<td class='text-center text-XX'>" + (data[x].cotizacion > 0 ? "<a href=\"javascript:VerDocumentos(1,'" + data[x].cotizacion + "')\">" + data[x].cotizacion + "</a>" : "&nbsp;") + "</td>" +
                    "<td class='text-center text-XX'>" + (data[x].certificado ? "<a href=\"javascript:VerDocumentos(2,'" + data[x].ingreso + "')\">" + data[x].certificado + "</a>" : "") + "</td>" +
                    "<td class='text-center text-XX'>" + (data[x].orden ? "<a href=\"javascript:VerDocumentos(3,'" + data[x].orden + "')\">" + data[x].orden + "</a>" : "") + "<input type='hidden' value='" + data[x].idcuecontable + "' name='Cuenta[]'><input type='hidden' value='" + data[x].idservicio + "' name='Servicio[]'></td>" +
                    "<td>&nbsp;</td></tr>";
            }

            subtotal += (data[x].precio * data[x].cantidad);
            descuento += (data[x].precio * data[x].cantidad * data[x].descuento / 100);
            gravable += (data[x].precio * data[x].cantidad) - (data[x].precio * data[x].cantidad * data[x].descuento / 100);
            iva += ((data[x].precio * data[x].cantidad) - (data[x].precio * data[x].cantidad * data[x].descuento / 100)) * data[x].iva / 100;
        }
    }

    
    
    totales = subtotal - descuento + iva - retfuente - retica - retiva;
    $("#bodyFactura").html(resultado);
        
    $("#tsubtotal").val(formato_numero(subtotal, 0,_CD,_CM,""));
    $("#tdescuento").val(formato_numero(descuento, 0,_CD,_CM,""));
    $("#tbaseimponible").val(formato_numero(gravable, 0,_CD,_CM,""));
    $("#texcento").val(formato_numero(exento, 0,_CD,_CM,""));
    $("#tiva").val(formato_numero(iva, 0,_CD,_CM,""));
            
    $("#tretefuente").val(formato_numero(retfuente, 0,_CD,_CM,""));
    $("#treteiva").val(formato_numero(retiva, 0,_CD,_CM,""));
    $("#treteica").val(formato_numero(retica, 0,_CD,_CM,""));
    $("#ttotalpagar").val(formato_numero(totales, 0, _CD, _CM, ""));

    if (Estado == "Temporal" || Estado == "Facturado") {
        $("#seleccionatodo").prop("checked", "checked");
    }

    for (var x = 0; x < data.length; x++) {
        if (data[x].idcentro * 1 > 0) {
            $("#tcentro" + x).val(data[x].idcentro).trigger("change");
        }
    }

    ContabilizarFactura();
}

function GuardarTemporal(x,id){
     if (id > 0) {
        var precio = NumeroDecimal($("#tprecio" + x).val()) * 1;
        var cantidad = $("#tcantidad" + x).val() * 1;
        var descuento = $("#tdescuento" + x).val() * 1;
        var centro = $("#tcentro" + x).val() * 1;
        var subtotal = NumeroDecimal($("#tsubtotal" + x).val());

        var datos = LlamarAjax("Facturacion","opcion=ActualizarTemporalFac&precio=" + precio + "&cantidad=" + cantidad + "&descuento=" + descuento + "&subtotal=" + subtotal + "&factura=" + Factura + "&id=" + id + "&idcentro=" + centro);
        if (datos == "0") {
            $.jGrowl("Guardando Temporal", { life: 1000, theme: 'growl-success', header: '' });
            Estado = "Temporal";
            $("#Estado").val(Estado);
            ContabilizarFactura()
        } 
        else
            $.jGrowl("No se puede actualizar una factura en estado contabilizada/enviada/anulada/reemplazada", { life: 1000, theme: 'growl-warning', header: '' });
    }
}

function BuscarDetalle(id) {

    var datos = LlamarAjax("Facturacion","opcion=BuscarTempFactura&id=" + id);
    eval("data=" + datos);

    $("#FacOIngreso").val(data[0].ingreso);
    $("#FacOIdRegistro").val(data[0].id);
    $("#FacOServicio").val(data[0].idservicio).trigger("change");
    $("#FacODescripcion").val(data[0].descripcion);
    $("#FacOCantidad").val(data[0].cantidad);
    $("#FacODescuento").val(data[0].descuento);
    $("#FacOIva").val(data[0].iva).trigger("change");
    $("#FacCentro").val(data[0].idcentro).trigger("change");
    $("#FacOPrecio").val(formato_numero(data[0].precio,_DE,_CD,_CM,""));
    
    $("#ModalOtroRegistroFac").modal("show");

}

function CalcularPreTabla(caja, x) {
    ValidarTexto(caja, 1);
    var precio = NumeroDecimal($("#tprecio" + x).val()) * 1;
    var cantidad = $("#tcantidad" + x).val() * 1;
    var descuento = $("#tdescuento" + x).val() * 1;

    var subtotal = (precio * cantidad) - (precio * cantidad * descuento / 100);

    $("#tsubtotal" + x).val(formato_numero(subtotal, 0,_CD,_CM,""));

    CalcularTotal();

}

function TablaFacturacion(orden) {

    $("#seleccionatodo").prop("checked", "");
    $("#bodyFactura").html("");
    var resultado = "";
    totalfila = 0;
    var estado = "";
    if (orden != "") {
        var datos = LlamarAjax("Facturacion","opcion=TablaFacturacion&cliente=" + IdCliente + "&orden=" + orden + "&id=" + IdFactura + "&habitual=" + Habitual);
        if (datos != "[]") {
            eval("data=" + datos);
            for (var x = 0; x < data.length; x++) {
                resultado += "<tr id='fila-" + x + "'" +  (data[x].tipo == 1 ? "" : "ondblclick='BuscarDetalle(" + data[x].id + ")'") + ">" +
                    "<td class='text-center'><b>" + (x + 1) + "</b><br><input class='form-control' type='checkbox' value='" + data[x].ingreso + "' name='seleccion[]' id='seleccionado" + x + "' onchange='SeleccionarFila(" + x + ")'></td>" +
                    "<td><div name='Descripcion[]'>" + data[x].servicio + "</div></td>" +
                    "<td><select class='form-control' name='CentroCosto[]' id='tcentro" + x + "' style='width:100%' onchange='ContabilizarFactura()'>" + CentroCostos + "</select></td>" +
                    "<td class='text-right text-XX'><input type='text' class='text-center sinborde' onfocus='FormatoEntrada(this, 1)' value='" + data[x].cantidad + "' onchange='GuardarTemporal(" + x + "," + (data[x].tipo == 1 ? 0 : data[x].id) + ")' onblur='FormatoSalida(this,0)' id='tcantidad" + x + "' onkeyup='CalcularPreTabla(this," + x + ")' name='Cantidades[]' onchange='ContabilizarFactura()' style='width:99%'></td>" +
                    "<td class='text-right text-XX'><input type='text' class='text-right sinborde' onfocus='FormatoEntrada(this, 1)' onchange='GuardarTemporal(" + x + "," + (data[x].tipo == 1 ? 0 : data[x].id) + ")' onblur='FormatoSalida(this,0)' onkeyup='CalcularPreTabla(this," + x + ")' value='" + formato_numero(data[x].precio, 0, _CD, _CM, "") + "' onchange='ContabilizarFactura()' id='tprecio" + x + "' name='Precios[]' style='width:99%'></td>" +
                    "<td class='text-right text-XX'><input type='text' class='text-right sinborde' onfocus='FormatoEntrada(this, 1)' onchange='GuardarTemporal(" + x + "," + (data[x].tipo == 1 ? 0 : data[x].id) + ")' onblur='FormatoSalida(this,0)' onkeyup='CalcularPreTabla(this," + x + ")' value='" + data[x].descuento + "' id='tdescuento" + x + "' onchange='ContabilizarFactura()' name='Descuentos[]' style='width:99%'></td>" +
                    "<td class='text-right text-XX'><input type='text' class='text-right sinborde' disabled value='" + formato_numero(data[x].iva, 0,_CD,_CM,"") + "' id='tiva" + x + "' name='IVA[]' style='width:99%'></td>" +
                    "<td class='text-right text-XX'><input type='text' class='text-right sinborde' disabled value='" + formato_numero(data[x].subtotal, 0,_CD,_CM,"") + "' id='tsubtotal" + x + "' name='Subtotales[]' style='width:99%'></td>" +
                    "<td class='text-center text-XX'>" + (data[x].ingreso > 0 ? "<a href=\"javascript:VerDocumentos(4,'" + data[x].remision + "')\">" + data[x].ingreso + "</a>" : "&nbsp;") + "</td>" +
                    "<td class='text-center text-XX'>" + (data[x].cotizacion > 0 ? "<a href=\"javascript:VerDocumentos(1,'" + data[x].cotizacion + "')\">" + data[x].cotizacion + "</a>" : "&nbsp;") + "</td>" +
                    "<td class='text-center text-XX'>" + (data[x].certificado ? "<a href=\"javascript:VerCertificados(" + data[x].ingreso + ")\">" + data[x].certificado + "</a>" : "") + "</td>" +
                    "<td class='text-center text-XX'>" + (data[x].orden ? "<a href=\"javascript:VerDocumentos(3,'" + data[x].orden + "')\">" + data[x].orden + "</a>" : "") +
                    "<input type='hidden' value='" + (data[x].cotizacion * 1) + "' name='Cotizaciones[]'>" +
                    "<input type='hidden' value='" + (data[x].orden ? data[x].orden : "") + "' name='Ordenes[]'>" +
                    "<input type='hidden' value='" + (data[x].certificado ? data[x].certificado : "") + "' name='Certificados[]'><input type='hidden' value='" + data[x].idcuecontable + "' name='Cuenta[]'><input type='hidden' value='" + data[x].idservicio + "' name='Servicio[]'><input type='hidden' value='" + data[x].idarticulo + "' name='Articulos[]'><input type='hidden' value='" + data[x].idbodega + "' name='Ubicacion[]'></td>" +
                    "<td>" + (data[x].tipo == 1 ? "&nbsp;" : "<button class='btn btn-danger' title='Eliminar Detalle' type='button' onclick=\"EliminarDetalle(" + data[x].id + "," + data[x].ingreso + ",'" + data[x].orden + "')\"><span data-icon='&#xe0d8;'></span></button>") + "</td></tr>";
                totalfila += 1;
            }

            setTimeout(function () {
                for (var x = 0; x < data.length; x++) {
                    if (data[x].idcentro * 1 > 0) {
                        $("#tcentro" + x).val(data[x].idcentro).trigger("change");
                    }
                }
            }, 100);
        }
    }
    
    $("#bodyFactura").html(resultado);
    CalcularTotal();
}

function VerCertificados(ingreso) {
    $("#titulodetalle").html("CERTIFICADO DEL INGRESO NRO " + ingreso);

    var datos = LlamarAjax("Laboratorio/DetalleIngreso", "opcion=6&ingreso=" + ingreso);
    $("#detallemovimiento").html(datos);

    $("#modalDetalleDevo").modal("show");

}

function GuardarFactura(contabilizar) {
    
    if (IdCliente == 0)
        return false;

    var seleccionado = 0;
    var observaciones = $.trim($("#Observaciones").val());
    var orden = $.trim($("#Orden").val());
    var fecha = $("#FechaFac").val();
        
    var ingresos = document.getElementsByName("seleccion[]");
    var descripcion = document.getElementsByName("Descripcion[]");
    var precios = document.getElementsByName("Precios[]");
    var certificados = document.getElementsByName("Certificados[]");
    var ordenes = document.getElementsByName("Ordenes[]");
    var cotizaciones = document.getElementsByName("Cotizaciones[]");
    var cantidades = document.getElementsByName("Cantidades[]");
    var descuentos = document.getElementsByName("Descuentos[]");
    var subtotales = document.getElementsByName("Subtotales[]");
    var ivas = document.getElementsByName("IVA[]");
    var centros = document.getElementsByName("CentroCosto[]");
    var articulos = document.getElementsByName("Articulos[]");
    var ubicacion = document.getElementsByName("Ubicacion[]");
    var cuenta = document.getElementsByName("Cuenta[]");
    var servicio = document.getElementsByName("Servicio[]");

    var a_ingreso = "array[";
    var a_orden = "array[";
    var a_certificado = "array[";
    var a_cotizacion = "array[";
    var a_precio = "array[";
    var a_descuento = "array[";
    var a_iva = "array[";
    var a_cantidad = "array[";
    var a_subtotal = "array[";
    var a_descripcion = "array[";
    var a_centro = "array[";
    var a_cuenta = "array[";
    var a_servicio = "array[";
    var a_articulos = "array[";
    var a_ubicacion = "array[";

    if (Estado != "Facturado" && Estado != "Temporal") {
        swal("Acción Cancelada", "No se puede actualizar un factura en estado " + Estado, "warning");
        return false;
    }

    var mensaje = "";

    if (fecha == "") {
        $("#FechaFac").focus();
        swal("Acción Cancelada", "Debe de ingresar la fecha de factura", "warning");
        return false;
    }
    

    for (var x = 0; x < ingresos.length; x++) {
        if (ingresos[x].checked) {
            if (ingresos[x].value * 1 > 0) {
                if (NumeroDecimal(subtotales[x].value) == 0) {
                    $("#tprecio" + x).focus();
                    swal("Acción Cancelada", "Debe de Ingresar la cantidad y el valor unitario del item número " + (x + 1), "warning");
                    return false;
                }
                if (($.trim(ordenes[x].value) == "") && ($.trim(certificados[x].value) == "") && ($.trim(cotizaciones[x].value) == "")) {
                    mensaje += "Ingreso número " + ingresos[x].value + " sin certificado, órden de compra y cotización <br>";
                } else {
                    if ($.trim(certificados[x].value) == "")
                        mensaje += "Ingreso número " + ingresos[x].value + " sin certificado";
                    if ($.trim(ordenes[x].value) == "") {
                        if ($.trim(certificados[x].value) == "")
                            mensaje += "Ingreso número " + ingresos[x].value + " sin certificado";
                        else
                            mensaje += ", sin órden de compra";
                    }
                    if (cotizaciones[x].value * 1 == "0") {
                        if (($.trim(ordenes[x].value) == "") && ($.trim(certificados[x].value) == ""))
                            mensaje += "Ingreso número " + ingresos[x].value + " sin cotización";
                        else
                            mensaje += ", sin cotización";
                    }
                    if (($.trim(ordenes[x].value) == "") || ($.trim(certificados[x].value) == "") || ($.trim(cotizaciones[x].value) == "")) 
                        mensaje += "<br>";
                }
            }
            if (centros[x].value * 1 == 0) {
                $("#tcentrol" + x).focus();
                swal("Acción Cancelada", "Debe de seleccionar el centro de costo del ítem número " + (x + 1), "warning");
                return false;
            }
            if (a_ingreso == "array[") {
                a_ingreso += ingresos[x].value;
                a_orden += "'" + $.trim(ordenes[x].value) + "'";
                a_certificado += "'" + $.trim(certificados[x].value) + "'";
                a_descripcion += "'" + $.trim(descripcion[x].innerHTML) + "'";
                a_cotizacion += $.trim(cotizaciones[x].value);
                a_precio += NumeroDecimal(precios[x].value);
                a_descuento += $.trim(descuentos[x].value);
                a_iva += $.trim(ivas[x].value);
                a_cantidad += NumeroDecimal(cantidades[x].value);
                a_subtotal += NumeroDecimal(subtotales[x].value);
                a_centro += centros[x].value;
                a_cuenta += cuenta[x].value;
                a_servicio += servicio[x].value;
                a_articulos += articulos[x].value;
                a_ubicacion += ubicacion[x].value;

            } else {
                a_ingreso += "," + ingresos[x].value;
                a_orden += ",'" + $.trim(ordenes[x].value) + "'";
                a_certificado += ",'" + $.trim(certificados[x].value) + "'";
                a_descripcion += ",'" + $.trim(descripcion[x].innerHTML) + "'";
                a_cotizacion += "," + $.trim(cotizaciones[x].value);
                a_precio += "," + NumeroDecimal(precios[x].value);
                a_descuento += "," + $.trim(descuentos[x].value);
                a_iva += "," + $.trim(ivas[x].value);
                a_cantidad += "," + NumeroDecimal(cantidades[x].value);
                a_subtotal += "," + NumeroDecimal(subtotales[x].value);
                a_centro += "," + centros[x].value;
                a_cuenta += "," + cuenta[x].value;
                a_servicio += "," + servicio[x].value;
                a_articulos += "," + articulos[x].value;
                a_ubicacion += "," + ubicacion[x].value;
            }
            seleccionado += 1;
        }
    }
    a_ingreso += "]";
    a_orden += "]";
    a_certificado += "]";
    a_cotizacion += "]";
    a_precio += "]";
    a_cantidad += "]";
    a_descuento += "]";
    a_iva += "]";
    a_subtotal += "]";
    a_descripcion = _escape(a_descripcion) +  "]";
    a_centro += "]";
    a_cuenta += "]";
    a_servicio += "]";
    a_articulos += "]";
    a_ubicacion += "]";
    
    if (seleccionado == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un registro", "warning");
        return false;
    }

    var ccuenta = document.getElementsByName("Cuentas[]");
    var cdebito = document.getElementsByName("Debitos[]");
    var ccredito = document.getElementsByName("Creditos[]");
    var cconcepto = document.getElementsByName("Conceptos[]");
    var ccentro = document.getElementsByName("Centros[]");

    var a_ccuenta = "array[";
    var a_cdebito = "array[";
    var a_ccredito = "array[";
    var a_cconceptos = "array[";
    var a_ccentro = "array[";

    for (var x = 0; x < ccuenta.length; x++) {
        if (ccuenta[x].value * 1 == 0) {
            DesactivarLoad();
            swal("Acción Cancelada", "Todas las cuentas contables deben de estar asignadas", "warning");
            return false;
        }

        if (a_ccuenta != "array[") {
            a_ccuenta += ",";
            a_cdebito += ",";
            a_ccredito += ",";
            a_cconceptos += ",";
            a_ccentro += ",";
        }

        a_ccuenta += ccuenta[x].value;
        a_cdebito += cdebito[x].value;
        a_ccredito += ccredito[x].value;
        a_ccentro += ccentro[x].value;
        a_cconceptos += "'" + cconcepto[x].value + "'";

    }

    a_ccuenta = a_ccuenta + "]";
    a_cdebito = a_cdebito +  "]";
    a_ccredito = a_ccredito +  "]";
    a_cconceptos = _escape(a_cconceptos) + "]";
    a_ccentro = a_ccentro + "]";

            
    var diferencia = NumeroDecimal($("#TDiferencia").html());
    if (diferencia != 0) {
        DesactivarLoad();
        swal("Acción Cancelada", "El comprobante contable se encuentra descuadrado", "warning");
        return false;
    }

    var tdebito = NumeroDecimal($("#TDebitos").html());
    var tcredito = NumeroDecimal($("#TCreditos").html());
       

    mensaje += "<br><b>¿DESEA GUARDAR y CONTABILIZAR LA FACTURA?</b>";

    var parametros = "id=" + IdFactura + "&cliente=" + IdCliente + "&observacion=" + observaciones +
        "&subtotal=" + subtotal + "&descuento=" + descuento + "&gravable=" + gravable + "&iva=" + iva + "&total=" + totales + "&exento=" + exento +
        "&reteiva=" + retiva + "&retefuente=" + retfuente + "&reteica=" + retica + "&orden=" + orden + "&porreteiva=" + porreteiva + "&porretefuente=" + porretefuente + "&porreteica=" + porreteica +
        "&a_ingreso=" + a_ingreso + "&a_cotizacion=" + a_cotizacion + "&a_certificado=" + a_certificado + "&a_orden=" + a_orden + "&a_precio=" + a_precio + "&a_descuento=" + a_descuento + "&a_subtotal=" + a_subtotal +
        "&a_cantidad=" + a_cantidad + "&a_descripcion=" + a_descripcion + "&a_iva=" + a_iva + "&fechafac=" + fecha + "&a_centro=" + a_centro + "&a_cuenta=" + a_cuenta + "&a_servicio=" + a_servicio + "&a_articulos=" + a_articulos + "&a_bodegas=" + a_ubicacion +
        "&a_ccuenta=" + a_ccuenta + "&a_cdebito=" + a_cdebito + "&a_ccredito=" + a_ccredito + "&a_cconcepto=" + a_cconceptos + "&a_ccentro=" + a_ccentro + "&tdebito=" + tdebito + "&tcredito=" + tcredito;

    if (contabilizar == 0) {
        swal.queue([{
            title: ValidarTraduccion('Advertencia'),
            text: mensaje,
            type: 'question',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Facturar'),
            cancelButtonText: ValidarTraduccion('Cancelar'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    $.post(url_servidor + "Facturacion","opcion=GuardarFactura&"+ parametros)
                        .done(function (data) {
                            datos = data.split("|");
                            if (datos[0] == 0) {
                                swal("", datos[1] + " " + datos[2], "success");
                                Estado = "Contabilizado";
                                $("#Estado").val(Estado);
                                ImprimirFactura(datos[2]);
                                BuscarFactura(datos[2]);
                                resolve()
                            } else {
                                reject(datos[1]);
                            }
                        })
                })
            }
        }]);

        $(".swal2-content").html(mensaje);
    } else {
        var datos = LlamarAjax("Facturacion","opcion=GuardarFactura&"+ parametros);
        var data = datos.split("|");
        BuscarFactura(data[2]);
        return datos;
    }
}

function VerDocumentos(tipo, documento) {
    switch (tipo) {
        case 1:
            ActivarLoad();
            setTimeout(function () {
                var parametros = "cotizacion=" + documento;
                var datos = LlamarAjax("Cotizacion","opcion=RpCotizacion&" + parametros);
                DesactivarLoad();
                datos = datos.split("|");
                if (datos[0] == "0") {
                    window.open("DocumPDF/" + datos[1]);
                }else{
                    swal("", ValidarTraduccion(datos[1]), "warning");
                }
            }, 15);
            break;
        case 2:
            VerCertificado(documento);
            break;
        case 3:
            window.open(url + "OrdenCompra/" + documento + ".pdf");
            break;
        case 4:
            ActivarLoad();
            setTimeout(function () {
                var parametros = "remision=" + documento;
                var datos = LlamarAjax("Cotizacion","opcion=RpRemision&" + parametros);
                DesactivarLoad();
                datos = datos.split("|");
                if (datos[0] == "0") {
                   window.open("DocumPDF/" + datos[1]);
                } else {
                    swal("", ValidarTraduccion(datos[1]), "warning");
                }
            }, 15);
    }
}

function VerCertificados(ingreso) {

    $("#titulodetalle").html("CERTIFICADO DEL INGRESO NRO " + ingreso);

    var datos = LlamarAjax("Laboratorio/DetalleIngreso", "opcion=6&ingreso=" + ingreso);
    $("#detallemovimiento").html(datos);

    $("#modalDetalleDFac").modal("show");
}

function VerCertificado(certificado) {
    window.open(url + "Certificados/" + certificado + ".pdf");
}


function LLamarFacturar() {

    var Ordenes = "";
    if (totales == 0 || IdCliente == 0)
        return false;

    $("#fCliente").val($("#NombreCliente").val());
    $("#fTipoCliente").val($("#TipoCliente").val());
    $("#fOrden").val(Ordenes);
                                            
    $("#fTablaPrecios").val($("#TablaPrecio").val());
    $("#fPlazoPago").val($("#PlazoPago").val());
    $("#fObsfactura").val($("#Observaciones").val());

    $("#fsubtotal").val(formato_numero(subtotal, 0,_CD,_CM,""));
    $("#fdescuento").val(formato_numero(descuento, 0,_CD,_CM,""));
    $("#ftotalpagar").val(formato_numero(totales, 0,_CD,_CM,""));
    $("#fbaseimponible").val(formato_numero(gravable, 0,_CD,_CM,""));
    $("#fexcento").val(formato_numero(exento, 0,_CD,_CM,""));
    $("#fiva").val(formato_numero(iva, 0,_CD,_CM,""));
    $("#ftarjeta").val(formato_numero(tarjeta, 0,_CD,_CM,""));
    $("#fcheque").val(formato_numero(cheque, 0,_CD,_CM,""));
    $("#fefectivo").val(formato_numero(efectivo, 0,_CD,_CM,""));
    $("#fvuelta").val(formato_numero(vuelta, 0,_CD,_CM,""));
    $("#frecibido").val(formato_numero(recibido, 0,_CD,_CM,""));
    $("#fsaldo").val(formato_numero(saldo, 0,_CD,_CM,""));

    $("#ModalFacturar").modal("show");
}

function ValidarTextoFactura(caja, tipo) {
    ValidarTexto(caja, tipo);
    efectivo = NumeroDecimal($("#fefectivo").val())*1;
    tarjeta = NumeroDecimal($("#tvalor1").val())*1 + NumeroDecimal($("#tvalor2").val())*1;
    recibido = efectivo + tarjeta
    vuelta = (recibido > totales ? recibido - totales : 0);
    saldo = (totales > recibido ? totales - recibido : 0);
    $("#frecibido").val(formato_numero(recibido, 0,_CD,_CM,""));
    $("#fvuelta").val(formato_numero(vuelta, 0,_CD,_CM,""));
    $("#frecibido").val(formato_numero(recibido, 0,_CD,_CM,""));
    $("#fsaldo").val(formato_numero(saldo, 0,_CD,_CM,""));
}

function CargarOIngreso() {
    if (IdCliente == 0)
        return false;

    var orden = $("#Orden").val();

    if (orden == "") {
        $("#Orden").focus();
        swal("Acción Cancelada", "Debe seleccionar una órden de compra", "warning");
        return false;
    }
                    
    localStorage.setItem("idcliente", IdCliente);
    CargarModalIngreso(3,"fac");
    $("#modalIngresoFac").modal("show");
}


function ModalORegistro() {
    if (IdCliente == 0)
        return false;

    var orden = $("#Orden").val();

    if (orden == "") {
        $("#Orden").focus();
        swal("Acción Cancelada", "Debe seleccionar una órden de compra", "warning");
        return false;
    }

    var orden = $("#Orden").val();

    if (orden == "") {
        $("#Orden").focus();
        swal("Acción Cancelada", "Debe seleccionar una órden de compra", "warning");
        return false;
    }

    LimpiarOtroRegistro();
    $("#ModalOtroRegistroFac").modal("show");
}

function LimpiarOtroRegistro() {
    $("#FacOIngreso").val("");
    $("#FacOIdRegistro").val("0");
    $("#FacODescripcion").val("");
    $("#FacOCantidad").val("1");
    $("#FacODescuento").val("0");
    $("#FacOPrecio").val("");
    $("#FacOServicio").val("").trigger("change");
    $("#FacOIva").val(ValorIva).trigger("change");
    $("#FacCentro").val("").trigger("change");
    $("#FacOSubTotal").val("");
    $("#FacOVSubTotal").val("");
    $("#FacOVTotal").val("");
    $("#FacOVIva").val("");
    $("#FacOVDescuento").val("");
}

$("#tablamodalingresofac > tbody").on("dblclick", "tr", function (e) {
    var orden = $("#Orden").val();
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    var parametro = "ingreso=" + numero + "&cliente=" + IdCliente + "&orden=" + orden + "&idfactura=" + IdFactura;
    var datos = LlamarAjax('Facturacion','opcion=GuardaTempFactura&' + parametro).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        TablaFacturacion(orden);
        CargarModalIngreso(3,"fac");
    } else
        swal("Acción Cancelada", datos[1], "warning");
});

function EliminarDetalle(id, ingreso, orden) {
    var datos = LlamarAjax("Facturacion","opcion=BuscarTempFactura&id=" + id);
    var data = JSON.parse(datos);
    var descripcion = data[0].descripcion;
    var mensaje = '¿Seguro que desea eliminar ' + descripcion + '?';
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar ' + descripcion + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Facturacion","opcion=EliminarTempFactura", "id=" + id + "&ingreso=" + ingreso)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            Estado = "Temporal";
                            $("#Estado").val(Estado);
                            TablaFacturacion(orden);
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);

    $(".swal2-content").html(mensaje);
};

function ImprimirFactura(factura, tipo) {
    if (factura == 0)
        factura = Factura;

    if (factura == 0)
        return false;
       
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=Factura&documento=" + factura).split("||");
        DesactivarLoad();
        if (datos[0] == "0") {
            if (!tipo)
                window.open(url_archivo + "DocumPDF/" + datos[1]);
            else {
                $("#resultadopdffactura").attr("src", url_cliente + "DocumPDF/" + datos[1]);
                Pdf_Factura = datos[1];
            }
        } else {
            swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function BuscarFatos(fila) {

}

function ConsularOrdenes() {


    var magnitud = $("#COMagnitud").val() * 1;
    var equipo = $("#COEquipo").val();
    var modelo = $("#COModelo").val();
    var marca = $("#COMarca").val() * 1;
    var intervalo = $("#COIntervalo").val();
    var ingreso = $.trim($("#COIngreso").val());
    var serie = $.trim($("#COSerie").val());

    var orden = $.trim($("#COOrden").val());

    var remision = $("#CORemision").val() * 1;
    var cliente = $("#COCliente").val() * 1;
    var usuario = 0;
    var tipo = $("#COTipo").val();
    var fechad = $("#COFechaDesde").val();
    var fechah = $("#COFechaHasta").val();

    if (fechad == "" && fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar una fecha o números de días"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "remision=" + remision + "&cliente=" + cliente + "&ingresos=" + ingreso +
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&usuario=" + usuario + "&orden=" + orden + "&tipo=" + tipo;
        var datos = LlamarAjax("Cotizacion","opcion=ConsultarOrden&"+ parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaOrdenes').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "ingreso", "className": "text-XX" },
                { "data": "remision" },
                { "data": "cliente" },
                { "data": "equipo" },
                { "data": "observacion" },
                { "data": "ncotizacion" },
                { "data": "factura" },
                { "data": "tipo" },
                { "data": "numero" },
                { "data": "fechacom" },
                { "data": "usuariocompra" },
                { "data": "ver" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ],
            "colReorder": true
        });
    }, 15);
}

function ConsularFactura() {
    
    var magnitud = $("#CMagnitud").val() * 1;
    var equipo = $("#CEquipo").val();
    var modelo = $("#CModelo").val();
    var marca = $("#CMarca").val() * 1;
    var intervalo = $("#CIntervalo").val();
    var ingreso = $("#CIngreso").val() * 1;
    var serie = $.trim($("#CSerie").val());
    var orden = $("#COrden").val();
    var cotizacion = $("#CCotizacion").val() * 1;
    var factura = $("#CFactura").val() * 1;
    var cliente = $("#CCliente").val() * 1;
    var usuario = $("#CUsuario").val() * 1;
    var estado = $("#CEstado").val();
    usuario = 0;

    var fechad = $("#CFechaDesde").val();
    var fechah = $("#CFechaHasta").val();

    if (fechad == "" || fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar fecha de inicio y fecha final"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "orden=" + orden + "&factura=" + factura + "&cotizacion=" + cotizacion +  "&cliente=" + cliente + "&ingreso=" + ingreso +
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&usuario=" + usuario + "&estado=" + estado;
        var datos = LlamarAjax("Facturacion","opcion=ConsultarFactura&"+ parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaFactura').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "imprimir" },
                { "data": "factura" , "className": "text-right text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "orden" },
                { "data": "estado" },
                { "data": "fecha" },
                { "data": "vencimiento" },
                { "data": "fecharad" },
                { "data": "cliente" },
                { "data": "tipo" },
                { "data": "diaspago" },
                { "data": "tiempo" },
                { "data": "direccion" },
                { "data": "ciudad" },
                { "data": "email" },
                { "data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "iva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "saldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "usuario" },
                { "data": "registrorad" },
                { "data": "recibidorad" },
                { "data": "anula" },
                { "data": "envio" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ],
            "colReorder": true
        });
    }, 15);
}

function VerEstadoCuenta() {
    if (IdCliente == 0)
        return false;
    var cliente = $("#NombreCliente").val();
    EstadoCuenta(IdCliente, cliente, CorreoEmpresa, TipoCliente, PlazoPago, CorreoEmpresa);
}

$('select').select2();
DesactivarLoad();

var AutoORegistro = [];

function AutoCompletarOServicio() {
    var datos = LlamarAjax("Compras","opcion=AutoCompletoOServ")
    AutoORegistro.splice(0, AutoORegistro.length);
    var data = JSON.parse(datos);
    for (var x = 0; x < data.length; x++) {
        AutoORegistro.push(data[x].descripcion);
    }
}

AutoCompletarOServicio();

$("#FacODescripcion").autocomplete({
    source: AutoORegistro
});
$("#FacODescripcion").autocomplete("option", "appendTo", ".eventInsForm");



function TablaRadicacion() {

    var cliente = $("#RCliente").val() * 1;
    var factura = $("#RFactura").val() * 1;

    var fechad = $("#RFechaDesde").val();
    var fechah = $("#RFechaHasta").val();

    if (fechad == "" || fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar fecha de inicio y fecha final"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "factura=" + factura + "&cliente=" + cliente + "&fechad=" + fechad + "&fechah=" + fechah;
        var datos = LlamarAjax('Facturacion','opcion=ConsultarFacPorRadicar&'+ parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        TableRadicacion = $('#TablaRadicacionFac').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "imprimir" },
                { "data": "fila" },
                { "data": "factura", "className": "text-XX" },
                { "data": "fecha" },
                { "data": "vencimiento" },
                { "data": "tipopago" },
                { "data": "cliente" },
                { "data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "iva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "retefuente", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "reteiva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "reteica", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "saldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "registro"}
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ],
            "colReorder": true
        });
    }, 15);

}

$('#TablaRadicacionFac tbody').on('click', 'tr', function () {
    $(this).toggleClass('selected');
});


function LlamarRadicarFactura() {
    if (TableRadicacion == null)
        return false;
    var datos = TableRadicacion.rows('.selected').data();
    var radicaciones = "";
    var total = 0;
    Facturas = "";
    for (var x = 0; x < datos.length; x++) {
        if (Facturas != "") {
            Facturas += ",";
        }
        total += datos[x].total;
        Facturas += datos[x].factura;
        radicaciones += "<option value='" + datos[x].factura + "'>" + datos[x].factura + " - " + datos[x].cliente + " - (" + formato_numero(datos[x].total,0,".",",","") + ")</option>";
    }
    if (datos.length == 0 || Facturas == "") {
        swal("Acción cancelada", "Debe de seleccionar por lo menos una factura", "warning");
        return false;
    }

    $("#NRadicacion").val("0");
    $("#FacturaSelecc").html(radicaciones);
    $("#Mensajero").val("").trigger("change");
    $("#TotalSeleccioFacRad").html("<h3>SELECCIONADAS: " + datos.length + ",  TOTAL EN PESOS: " + formato_numero(total, 0,_CD,_CM,"") + "</h3");
    $("#ObservacionRadica").val("");

    $("#ModalRadicar").modal("show");
}

function RadicarFactura() {
    var fecha = $("#FechaRadi").val();
    var mensajero = $("#Mensajero").val() * 1;
    var observacion = $.trim($("#ObservacionRadica").val());

    if (mensajero == 0) {
        $("#Mensajero").focus();
        swal("Acción Cancelada", "Debe seleccionar un mensajero", "warning");
        return false;
    }

    if (fecha == "") {
        $("#FechaRadi").focus();
        swal("Acción Cancelada", "Debe de ingresar una fecha de radicación", "warning");
        return false;
    } else {
        if (fecha < output2) {
            $("#FechaRadi").focus();
            swal("Acción Cancelada", "La fecha de radicación no puede ser menor a " + output2, "warning");
            return false;
        }
    }
    var parametros = "facturas=" + Facturas + "&fecha=" + fecha + "&mensajero=" + mensajero + "&observacion=" + observacion;
    var datos = LlamarAjax("Facturacion","opcion=GuardarRadicacion&"+ parametros).split("|");
    if (datos[0] == "0") {
        $("#ModalRadicar").modal("hide");
        TablaRadicacion();
        swal("", datos[1] + " " +  datos[2], "success");
        ImprimirRadicacion(datos[2])
    } else
        swal("Acción Cancelada", datos[1], "warning");

}

function ImprimirRadicacion(radicacion) {
    ActivarLoad();
    setTimeout(function () {
        var parametros = "radicado=" + radicacion;
        var datos = LlamarAjax("Facturacion","opcion=RpRadicacion&" + parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function TablaRadicacionRecibir() {

    var cliente = $("#RRCliente").val() * 1;
    var factura = $("#RRFactura").val() * 1;
    var radicacion = $("#RRRadicacion").val() * 1;

    var fechad = $("#RRFechaDesde").val();
    var fechah = $("#RRFechaHasta").val();

    if (fechad == "" || fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar fecha de inicio y fecha final"), "warning");
        return false;
    }

    SelecRadi = 0;
    ValorRadi = 0;

    $("#SeleccionadoRad").html("<h3>Seleccionado: " + SelecRadi + ", Valor en Pesos: " + formato_numero(ValorRadi, 0,_CD,_CM,""));

    ActivarLoad();
    setTimeout(function () {
        var parametros = "radicacion=" + radicacion +  "&factura=" + factura + "&cliente=" + cliente + "&fechad=" + fechad + "&fechah=" + fechah;
        var datos = LlamarAjax("Facturacion","opcion=ConsultarFacRadicada&"+ parametros);
        DesactivarLoad();
        var data = JSON.parse(datos);
        var Resultado = "";
        for (var x = 0; x < data.length; x++) {
            Resultado += "<tr id='filarad-" + x + "' >" +
                "<td class='text-center'><b>" + (x + 1) + "</b><br><input class='form-control' type='checkbox' value='" + data[x].id + "' name='seleccionradi[]' id='seleccionradi" + x + "' onchange='SeleccionarFilaRad(" + x + "," + data[x].total + ")'></td>" +
                "<td>" + data[x].opciones + "</td>" +
                "<td>" + data[x].radicacion + "</td>" +
                "<td class='text-XX'>" + data[x].factura + "</td>" +
                "<td>" + data[x].fecha + "</td>" +
                "<td>" + data[x].vencimiento + "</td>" +
                "<td>" + data[x].tipopago + "</td>" +
                "<td>" + data[x].cliente + "</td>" +
                "<td align='right'>" + formato_numero(data[x].total,0,".",",","") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].saldo, 0,_CD,_CM,"") + "</td>" +
                "<td><input type='date' class='sinborde text-XX' style='width:175px' value='" + data[x].fecharad + "' name ='FechaRadRec[]'>" +
                "<input type='hidden' value='" + data[x].fecharad + "' name='ValFechaRad[]'>" +
                "<input type='hidden' value='" + data[x].factura + "' name='FacturasRec[]'></td>" +
                "<td>" + data[x].registro + "</td>" +
                "<td>" + data[x].mensajero + "</td></tr>";
        }
        $("#BodyRecRadicFac").html(Resultado);
    }, 15);

}

function RecibirFacRadicada() {
    var seleccion = document.getElementsByName("seleccionradi[]");
    var fecha = document.getElementsByName("FechaRadRec[]");
    var factura = document.getElementsByName("FacturasRec[]");
    var fechaini = document.getElementsByName("ValFechaRad[]");
    
    var a_ids = "";
    var a_fechas = "";
    var a_factura = "";
    var mensaje = "";


    for (var x = 0; x < seleccion.length; x++) {
        if (seleccion[x].checked) {
            if (a_ids != "") {
                a_ids += ",";
                a_fechas += ",";
                a_factura += ",";
            }
            a_ids += seleccion[x].value;
            a_fechas += fecha[x].value;
            a_factura += factura[x].value;

            if (fecha[x].value == "") {
                mensaje += "Debe de ingresar la fecha de radicación de la fila # " + (x + 1) + "<br>";
            } else {
                if (fecha[x].value < fechaini[x].value) {
                    mensaje += "La fecha de radicación no puede ser menor a " + fechaini[x].value + " de la fila # " + (x+1) + "<br>";
                }
            }
        }
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    if (a_ids == "") {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo una factura", "warning");
        return false;
    }

    var parametros = "ids=" + a_ids + "&fechas=" + a_fechas + "&facturas=" + a_factura;
    var datos = LlamarAjax("Facturacion","opcion=GuardarRecibirRadi&"+ parametros).split("|");
    if (datos[0] == "0") {
        TablaRadicacionRecibir();
        swal("", datos[1], "success");
    } else
        swal("Acción Cancelada", datos[1], "warning");

}

function TablaConsRadicacion() {

    var cliente = $("#CRCliente").val() * 1;
    var factura = $("#CRFactura").val() * 1;
    var radicado = $("#CRRadicacion").val() * 1;
    var recibido = $("#CRRecibido").val();

    var fechad = $("#CRFechaDesde").val();
    var fechah = $("#CRFechaHasta").val();

    if (fechad == "" || fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar fecha de inicio y fecha final"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "factura=" + factura + "&cliente=" + cliente + "&fechad=" + fechad + "&fechah=" + fechah + "&radicacion=" + radicado + "&recibido=" + recibido;
        var datos = LlamarAjax("Facturacion","opcion=ConsultarRadicada&"+ parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        TableRadicacion = $('#TablaConsRadicado').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "opciones" },
                { "data": "fila" },
                { "data": "radicacion" },
                { "data": "factura", "className":"text-XX text-right" },
                { "data": "fecha" },
                { "data": "vencimiento" },
                { "data": "tipopago" },
                { "data": "cliente" },
                { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "saldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "fecharad", "className": "text-XX text-right" },
                { "data": "registro" },
                { "data": "recibido" },
                { "data": "mensajero" },
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ],
            "colReorder": true
        });
    }, 15);

}

function CombioFechaRad() {
    var seleccion = document.getElementsByName("seleccionradi[]");
    
    var cantidad = 0;
    for (var x = 0; x < seleccion.length; x++) {
        if (seleccion[x].checked) {
            cantidad++;
        }
    }

    if (cantidad == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo una factura", "warning");
        return false;
    }

}   

function ConsultarRemSinFac() {
    var cliente = $("#RICliente").val() * 1;
    var fechad = $("#RIFechaDesde").val();
    var fechah = $("#RIFechaHasta").val();
    var parametros = "cliente=" + cliente + "&fechad=" + fechad + "&fechah=" + fechah;
    var datos = LlamarAjax("Facturacion","opcion=RemisionNoFacturado&"+ parametros);
    var datajson = JSON.parse(datos);
    $('#TablaRemisionSinFac').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        columns: [
            { "data": "remision", "className": "text-XX" },
            { "data": "cliente" },
            { "data": "cantingreso", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "listos", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "pendiente", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "nofacturado", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "registro" }
        ],
        dom: 'Bfrtip',
        buttons: [
            'excel', 'csv', 'copy'
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function ModalFacturaSinRela() {
    var cliente = $("#FSCliente").val() * 1;
    var fechad = $("#FSFechaDesde").val();
    var fechah = $("#FSFechaHasta").val();
    var parametros = "cliente=" + cliente + "&fechad=" + fechad + "&fechah=" + fechah;
    var datos = LlamarAjax("Facturacion","opcion=TablaFacturaSinRelacionar&"+ parametros);
    if (datos == "[]") {
        swal("Acción Cancelada", "No hay facturas pendientes por relacionar ingresos", "warning");
        return false;
    }
    var datajson = JSON.parse(datos);
    $('#TablaFacturaSinRela').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        columns: [
            { "data": "factura", "className": "text-XX" },
            { "data": "cliente" },
            { "data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "iva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "retenciones", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "registro" }
        ],
        "lengthMenu": [[8], [8]],
        "language": {
            "url": LenguajeDataTable
        }
    });
    $("#ModalFacturaSinRela").modal("show");
}

$("#TablaFacturaSinRela > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var remision = row[0].innerText;
    var datos = LlamarAjax("Facturacion","opcion=DetRemisionNoFacturado&remision=" + remision);
    var datajson = JSON.parse(datos);
    $('#TablaIngRemiSinFac').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        columns: [
            { "data": "factura", "className": "text-XX" },
            { "data": "cliente" },
            { "data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "iva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "retenciones", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "registro" }
        ],
        "lengthMenu": [[8], [8]],
        "language": {
            "url": LenguajeDataTable
        }
    });
    $("#ModalIngresoSinFac").modal("show");
});

function ContabilizarFactura() {
    $("#TbContabilidad").html("");
    if (IdCliente == 0)
        return false;

    ActivarLoad();
    setTimeout(function () {
                
        var seleccionado = 0;
        var TDebito = 0;
        var TCredito = 0;
                
        var ingresos = document.getElementsByName("seleccion[]");
        var subtotales = document.getElementsByName("Subtotales[]");
        var centros = document.getElementsByName("CentroCosto[]");
        var cuenta = document.getElementsByName("Cuenta[]");

        var a_subtotal = "array[";
        var a_centro = "array[";
        var a_cuenta = "array[";
        var agregar = 0;
        for (var x = 0; x < cuenta.length; x++) {
            agregar = 0;
            if (ingresos.length > 0) {
                if (ingresos[x].checked)
                    agregar = 1;
            } else {
                agregar = 1;
            }
            if (agregar == 1) {   
                
                if (a_cuenta == "array[") {
                    a_subtotal += NumeroDecimal(subtotales[x].value);
                    a_centro += (centros[x].value*1);
                    a_cuenta += (cuenta[x].value*1);

                } else {
                    a_subtotal += "," + NumeroDecimal(subtotales[x].value);
                    a_centro += "," + (centros[x].value*1);
                    a_cuenta += "," + (cuenta[x].value*1);
                }
                seleccionado += 1;
            }
        }
        
        a_subtotal += "]";
        a_centro += "]";
        a_cuenta += "]";

        if (seleccionado == 0) {
            DesactivarLoad();
            return false;
        }
                
        var parametros = "cliente=" + IdCliente + "&iva=" + iva + "&subtotal=" + subtotal + "&total=" + totales + "&reteiva=" + retiva + "&retefuente=" + retfuente + "&reteica=" + retica +
            "&a_centro=" + a_centro + "&a_cuenta=" + a_cuenta + "&a_monto=" + a_subtotal + "&tdebito=" + TDebito + "&tcredito=" + TCredito + "&buscar=" + Factura +
            "&idreteiva=" + ReteIva + "&idretefuente=" + ReteFuente + "&idreteica=" + ReteIca;
        var datos = LlamarAjax("Facturacion","opcion=Contabilizar_Factura&"+ parametros).split("||");
        DesactivarLoad();
        if (datos[0] == "0") {
            var data = JSON.parse(datos[1]);
            if (data[0]._error * 1 == 0) {
                
                var resultado = "";

                var tdebito = 0;
                var tcredito = 0;

                for (var x = 0; x < data.length; x++) {
                    resultado += "<tr " + (data[x]._idcuenta * 1 == 0 ? " class='bg-danger' " : "") + ">" +
                        "<td>" + data[x]._cuenta + "</div></td>" +
                        "<td>" + data[x]._centrocosto + "</div></td>" +
                        "<td class='text-right'>" + formato_numero(data[x]._debito, _DE, _CD, _CM, "") + "</td>" +
                        "<td class='text-right'>" + formato_numero(data[x]._credito, _DE, _CD, _CM, "") +
                        "<input name='Debitos[]' type='hidden' value='" + data[x]._debito + "'>" +
                        "<input name='Creditos[]' type='hidden' value='" + data[x]._credito + "'>" +
                        "<input name='Centros[]' type='hidden' value='" + data[x]._idcentro + "'>" +
                        "<input name='Cuentas[]' type='hidden' value='" + data[x]._idcuenta + "'>" +
                        "<input name='Conceptos[]' type='hidden' value='" + data[x]._concepto + "'></td></tr>";
                    tdebito += data[x]._debito;
                    tcredito += data[x]._credito;
                }

                $("#TDebitos").html(formato_numero(tdebito, _DE, _CD, _CM, ""))
                $("#TCreditos").html(formato_numero(tcredito, _DE, _CD, _CM, ""))
                $("#TDiferencia").html(formato_numero(tdebito - tcredito, _DE, _CD, _CM, ""))

                $("#TbContabilidad").html(resultado);
            } else {
                swal("Acción Cancelada", data[0]._mensaje, "warning");
            }
         } else 
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
}

function AnularContabilizacion() {
    if (Factura == 0)
        return false;
    var resultado = "";
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea anular la contabilización de la factura número ' + Factura + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Anular'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Facturacion","opcion=AnularContabilizacion&factura=" + Factura)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            resultado = datos[1];
                            resolve();
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]).then(function (result) {
        Estado = "Facturado";
        $("#Estado").val(Estado);
        $("#btneliminarconta").addClass("hidden");
        $("#btncontabilizar").removeClass("hidden");
        BuscarFactura(Factura);
        swal({
            type: 'success',
            html: resultado
        })
    });
} 

function AgregarRetencioFac() {
    var tipo = $("#RTipoRetencion").val() * 1;
    switch (tipo) {
        case 1: //FUENTE
            retfuente = NumeroDecimal($("#RRetencion").val());
            ReteFuente = $("#RPorcentaje").val() * 1;
            porretefuente = LlamarAjax("Compras","opcion=PorcentajeRetencion&retencion=" + ReteFuente) * 1;
            break;
        case 2: //IVA
            retiva = NumeroDecimal($("#RRetencion").val());
            ReteIva = $("#RPorcentaje").val() * 1;
            porreteiva = LlamarAjax("Compras","opcion=PorcentajeRetencion&retencion=" + ReteIva) * 1;
            break;
        case 3: //ICA
            retica = NumeroDecimal($("#RRetencion").val());
            ReteIca = $("#RPorcentaje").val() * 1;
            porreteica = LlamarAjax("Compras","opcion=PorcentajeRetencion&retencion=" + ReteIca) * 1;
            break;
    }
    CalcularTotal();
    ContabilizarFactura();
    $("#modalRetencionFac").modal("hide");
}



function CalcularRetencionFac(Caja) {
    if (Caja)
        ValidarTexto(Caja, 3);

    var retencion = NumeroDecimal($("#RPorcentaje").val());
    var porcentaje = LlamarAjax("Compras","opcion=PorcentajeRetencion&retencion=" + retencion) * 1;
    var valor = NumeroDecimal($("#RSubTotal").val());
    $("#RRetencion").val(formato_numero(Math.trunc(valor * porcentaje / 100), 0, _CD, _CM, ""));
}

function CambioRetencionFac() {
    CalcularRetencionFac();
}

function LlamarRetencionFac(tipo, descripcion) {
    if (IdCliente == 0)
        return false;

    if (Estado != "Temporal" && Estado != "Facturado")
        return false;

    if (totales > 0) {

        switch (tipo) {
            case 1: //Fuente
                $("#RSubTotal").val(formato_numero(subtotal - descuento, 0, _CD, _CM, ""));
                $("#RPorcentaje").html(CargarCombo(90, 7, "", "ReteFuente"));
                $("#RPorcentaje").val(ReteFuente).trigger("change");
                $("#RPorCliente").html(porretefuente);
                break;
            case 2: //IVA
                $("#RSubTotal").val(formato_numero(iva, 0, _CD, _CM, ""));
                $("#RPorCliente").html(porreteiva);
                $("#RPorcentaje").html(CargarCombo(90, 7, "", "ReteIVA"));
                $("#RPorcentaje").val(ReteIva).trigger("change");

                break;
            case 3: //ICA
                $("#RSubTotal").val(formato_numero(subtotal - descuento, 0, _CD, _CM, ""));
                $("#RPorCliente").html(porreteica);
                $("#RPorcentaje").html(CargarCombo(90, 7, "", "ReteICA"));
                $("#RPorcentaje").val(ReteIca).trigger("change");
                break;
        }
        $("#RTipoRetencion").val(tipo);
        $("#desretencion").html(descripcion);
        $("#modalRetencionFac").modal("show");

    }
}


function ActualizarRetencioFac() {
    var tipo = $("#RTipoRetencion").val() * 1;
    var idretencion = $("#RPorcentaje").val() * 1;
    var datos = LlamarAjax("Facturacion","opcion=ActualizarRetencion&tipo=" + tipo + "&idretencion=" + idretencion + "&cliente=" + IdCliente).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
    }
    else
        swal("Acción Cancelada", datos[1], "warning");
}

function ImprimirCertificado(numero) {
    window.open(url + "Certificados/" + numero + ".pdf");
}

function ModalArticulosFac() {
    if (IdCliente == 0 || Factura > 0)
        return false;
    $("#modalArticulosFac").modal("show");
}

function CargarModalArticulosFac() {

    var codigo = $.trim($("#FCodigo").val());
    var descripcion = $.trim($("#FDescripcion").val());
    var grupo = $("#FGrupo").val() * 1;
    var ubicacion = $("#FUbicacion").val() * 1;

    var parametros = "codigo=" + codigo + "&descripcion=" + descripcion + "&grupo=" + grupo + "&ubicacion=" + ubicacion;
    var datos = LlamarAjax("Inventarios","opcion=ModalArticulos&"+ parametros);
    var datajson = JSON.parse(datos);
    $('#TablaModalArticulosFac').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        searching: false,
        columns: [
            { "data": "foto" },
            { "data": "id" },
            { "data": "codigointerno" },
            { "data": "codigobarras" },
            { "data": "grupo" },
            { "data": "tipo" },
            { "data": "equipo" },
            { "data": "marca" },
            { "data": "modelo" },
            { "data": "presentacion" },
            { "data": "ultimocosto", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "existencia" },
            { "data": "iva" },
            { "data": "cuenta" },
            { "data": "proveedor" }
        ],
        "lengthMenu": [[5], [5]],
        "language": {
            "url": LenguajeDataTable
        }

    });
}

$("#TablaModalArticulosFac > tbody").on("dblclick", "tr", function (e) {

    var row = $(this).parents("td").context.cells;
    var descripcion = "CODIGO: " + row[2].innerText + ", TIPO: " + row[5].innerText + ", GRUPO: " + row[4].innerText +
        ", ARTICULO: " + row[6].innerText + ", MARCA: " + row[7].innerText + ", MODELO: " + row[8].innerText + ", PRESENTACION: " + row[9].innerText;
    var parametro = "idCompra=" + IdCompra + "&proveedor=" + IdProveedor + " &id=0&descripcion=" + descripcion +
        "&iva=" + row[12].innerText + "&cuenta=" + row[13].innerText +
        "&cantidad=1&descuento=0&precio=" + NumeroDecimal(row[10].innerText) + "&subtotal=" + NumeroDecimal(row[10].innerText) + "&idarticulo=" + row[1].innerText;
    var datos = LlamarAjax("Compras","opcion=GuardarDetCompra&"+ parametro + "&orden=" + Orden);
    datos = datos.split("|");
    if (datos[0] == "0") {
        $("#modalArticulosFac").modal("hide");
        swal("", datos[1], "success");
        TablaCompras();
    } else
        swal("Acción Cancelada", datos[1], "warning");
});


$('#Orden').select2({ tags: true });
$("#FacturaSelecc").select2('destroy'); 
