package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Inventarios"})
public class Inventarios extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String archivoadjunto = null;
    String archivoadjunto2 = null;
    String url_archivo = Globales.url_archivo;

    //WILMER
    public String GuardarOpcion(HttpServletRequest request) {
        String descripcion = Globales.neescape(request.getParameter("descripcion"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int json = Globales.Validarintnonull(request.getParameter("json"));
        int id = 0;

        Configuracion config = new Configuracion();

        try {
            String resultado = "";
            switch (tipo) {
                case 1:
                    if (Globales.PermisosSistemas("GRUPO-INVENTARIO GUARDAR", idusuario) == 0) {
                        return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    descripcion = descripcion.trim().toUpperCase();
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.grupos WHERE descripcion = '" + descripcion + "'"));
                    if (id > 0) {
                        return "XX|Descripcíon ya existe como grupo";
                    }
                    sql = "INSERT INTO inventarios.grupos (descripcion) VALUES ('" + descripcion + "')";
                    if (Globales.DatosAuditoria(sql, "GRUPO-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "->GuardarOpcion")) {
                        id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.grupos WHERE descripcion = '" + descripcion + "'"));
                        resultado = id + "|" + config.CargarCombos(null, 47, 1, "", 0, json);
                    } else {
                        return "XX|Erro a guardar los datos";
                    }
                    break;
                case 2:
                    if (Globales.PermisosSistemas("EQUIPOS-INVENTARIO GUARDAR", idusuario) == 0) {
                        return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }

                    descripcion = descripcion.trim().toUpperCase();
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.equipos WHERE descripcion = '" + descripcion + "' and idgrupo = " + grupo));
                    if (id > 0) {
                        return "XX|Descripcíon ya existe como equipo";
                    }
                    sql = "INSERT INTO inventarios.equipos (descripcion, idgrupo) VALUES ('" + descripcion + "'," + grupo + ")";
                    if (Globales.DatosAuditoria(sql, "EQUIPOS-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "->GuardarOpcion")) {
                        id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.equipos WHERE descripcion = '" + descripcion + "' and idgrupo = " + grupo));
                        resultado = id + "|" + config.CargarCombos(null, 48, 1, String.valueOf(grupo), 0, json);
                    } else {
                        return "XX|Erro a guardar los datos";
                    }
                    break;
                case 3:
                    if (Globales.PermisosSistemas("PRESENTACION-INVENTARIO GUARDAR", idusuario) == 0) {
                        return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    descripcion = descripcion.trim().toUpperCase();
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.presentacion WHERE descripcion = '" + descripcion + "'"));
                    if (id > 0) {
                        return "XX|Descripcíon ya existe como presentación";
                    }
                    sql = "INSERT INTO inventarios.presentacion (descripcion) VALUES ('" + descripcion + "')";
                    if (Globales.DatosAuditoria(sql, "PRESENTACION-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "->GuardarOpcion")) {
                        id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.presentacion WHERE descripcion = '" + descripcion + "'"));
                        resultado = id + "|" + config.CargarCombos(null, 98, 1, "", 0, json);
                    } else {
                        return "XX|Erro a guardar los datos";
                    }
                    break;
                case 4:
                    if (Globales.PermisosSistemas("MARCA-INVENTARIO GUARDAR", idusuario) == 0) {
                        return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    descripcion = descripcion.trim().toUpperCase();
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.marcas WHERE descripcion = '" + descripcion + "'"));
                    if (id > 0) {
                        return "XX|Descripcíon ya existe como marca";
                    }
                    sql = "INSERT INTO inventarios.marcas (descripcion) VALUES ('" + descripcion + "')";
                    if (Globales.DatosAuditoria(sql, "MARCA-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "->GuardarOpcion")) {
                        id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.marcas WHERE descripcion = '" + descripcion + "'"));
                        resultado = id + "|" + config.CargarCombos(null, 49, 1, "", 0, json);
                    } else {
                        return "XX|Erro a guardar los datos";
                    }
                    break;
                case 5:
                    if (Globales.PermisosSistemas("MODELO-INVENTARIO GUARDAR", idusuario) == 0) {
                        return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    descripcion = descripcion.trim().toUpperCase();
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.modelos WHERE descripcion = '" + descripcion + "' and idmarca = " + marca));
                    if (id > 0) {
                        return "XX|Descripcíon ya existe modelo";
                    }
                    sql = "INSERT INTO inventarios.modelos (descripcion, idmarca) VALUES ('" + descripcion + "'," + marca + ")";
                    if (Globales.DatosAuditoria(sql, "MODELO-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "->GuardarOpcion")) {
                        id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.modelos WHERE descripcion = '" + descripcion + "' and idmarca = " + marca));
                        resultado = id + "|" + config.CargarCombos(null, 50, 1, String.valueOf(marca), 0, json);
                    } else {
                        return "XX|Erro a guardar los datos";
                    }
                    break;
                case 6:
                    if (Globales.PermisosSistemas("CARACTERISTICA-INVENTARIO GUARDAR", idusuario) == 0) {
                        return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    descripcion = descripcion.trim();
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.caracteristica WHERE descripcion = '" + descripcion + "'"));
                    if (id > 0) {
                        return "XX|Descripcíon ya existe caracteristica";
                    }
                    sql = "INSERT INTO inventarios.caracteristica (descripcion) VALUES ('" + descripcion + "')";
                    if (Globales.DatosAuditoria(sql, "CARACTERISTICA-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "->GuardarOpcion")) {
                        id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.caracteristica WHERE descripcion = '" + descripcion + "'"));
                        resultado = id + "|" + config.CargarCombos(null, 99, 1, String.valueOf(marca), 0, json);
                    } else {
                        return "XX|Erro a guardar los datos";
                    }
                    break;
            }
            return resultado;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String BuscarArticulos(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        sql = "select a.*, idmarca, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigo, to_char(fechafactura,'yyyy-MM-dd') as fechafac,"
                + "      P.id AS idpprecio,  precio1, precio2, precio3, precio4, precio5, idgrupo,"
                + "      precio6, precio7, precio8, precio9, precio10, "
                + " preciousd1, preciousd2, preciousd3, preciousd4, preciousd5,preciousd6, preciousd7, preciousd8, preciousd9, preciousd10"
                + "  from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                          inner join inventarios.equipos e on a.idequipo = e.id "
                + "                          inner join inventarios.grupos g on g.id = e.idgrupo"
                + "                          left JOIN  inventarios.articulos_precios p on p.idarticulo = a.id "
                + " where a.id = " + id;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarArticulos");
    }

    public String CuentaContable_Grupo(HttpServletRequest request) {
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));

        sql = "SELECT idcuecontable"
                + "  FROM inventarios.grupos"
                + "WHERE id = " + grupo;
        return Globales.ObtenerUnValor(sql);

    }

    public String ConsultarArticulos(HttpServletRequest request) {
        int presentacion = Globales.Validarintnonull(request.getParameter("presentacion"));
        int caracteristica = Globales.Validarintnonull(request.getParameter("caracteristica"));
        String tipo = request.getParameter("tipo");
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        String estado = request.getParameter("estado");
        String uso_interno = request.getParameter("uso_interno");
        String referencia = request.getParameter("referencia");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));

        String busqueda = "WHERE 1=1 ";
        if (!estado.equals("Todos")) {
            busqueda += " AND a.tipo  = '" + tipo + "'";
        }
        if (!referencia.equals("")) {
            busqueda += " AND a.referencia  = '" + referencia + "'";
        }
        if (!uso_interno.equals("")) {
            busqueda += " AND a.uso_interno  = '" + uso_interno + "'";
        }
        if (grupo > 0) {
            busqueda += " AND e.idgrupo = " + grupo;
        }
        if (equipo > 0) {
            busqueda += " AND a.idequipo = " + equipo;
        }
        if (marca > 0) {
            busqueda += " AND mo.idmarca = " + marca;
        }
        if (!modelo.equals("")) {
            busqueda += " AND mo.descripcion = '" + modelo + "'";
        }
        if (proveedor > 0) {
            busqueda += " AND a.idproveedor = " + proveedor;
        }
        if (presentacion > 0) {
            busqueda += " AND a.idpresentacion= " + presentacion;
        }
        if (caracteristica > 0) {
            busqueda += " AND a.idcaracteristica = " + proveedor;
        }

        sql = "SELECT a.id, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, codigobarras, a.tipo, g.descripcion as grupo, \n"
                + "  e.descripcion as equipo, te.descripcion as estado, ma.descripcion as marca, mo.descripcion as modelo, pr.descripcion as presentacion, \n"
                + "  ca.descripcion as caracteristica, existencia,p.nombrecompleto as proveedor,ultimocosto, uso_interno, \n"
                + "      ultimafactura,i.descripcion as iva, a.estado, catalogo,referencia, CASE WHEN manual_usuario = 1 THEN 'SI' ELSE 'NO' end as manual, \n"
                + "      CASE WHEN fichatecnica = 1 THEN 'SI' ELSE 'NO' end as ficha, garantia_mes, \n"
                + "     case when a.foto1 > 0 then '<img src=''" + url_archivo + "Adjunto/imagenes/Articulos/' || a.id || '/1.jpg'' width=''80px''/><br>' else '' end as imagen "
                + "  FROM inventarios.articulos a inner join inventarios.equipos e on a.idequipo = e.id  \n"
                + "                        inner join inventarios.grupos g on g.id = e.idgrupo \n"
                + "                        inner join inventarios.modelos mo on mo.id = a.idmodelo \n"
                + "                        inner join inventarios.marcas ma on mo.idmarca = ma.id \n"
                + "                        inner join inventarios.presentacion pr on pr.id = a.idpresentacion \n"
                + "                        inner join inventarios.caracteristica ca on ca.id = a.idcaracteristica \n"
                + "                        inner join iva i on a.idiva = i.id \n"
                + "                        inner join proveedores p on a.idproveedor = p.id \n"
                + "                        inner join tipo_estado te on a.estado = te.id " + busqueda + " ORDER BY a.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarArticulos");
    }

    public int ValorIva(HttpServletRequest request) {
        int Iva = Globales.Validarintnonull(request.getParameter("Iva"));

        sql = "select valor from iva where id = " + Iva;
        return Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
    }

    public String CodigoInterno(HttpServletRequest request) {
        int Grupo = Globales.Validarintnonull(request.getParameter("Grupo"));
        try {
            sql = "SELECT to_char(coalesce(max(a.codigointerno),0)+1,'0000000') as codigo, abreviatura"
                    + "  from inventarios.grupos g left join inventarios.equipos e on e.idgrupo = g.id "
                    + "	                                  left join inventarios.articulos a on e.id = a.idequipo"
                    + "   WHERE g.id=" + Grupo + "group by abreviatura";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->CodigoInterno", 1);
            datos.next();

            return datos.getString("abreviatura") + "-" + datos.getString("codigo").trim();
        } catch (Exception e) {
            Globales.GuardarLogger(e.getMessage(), this.getClass() + "-->CodigoInterno");
            return "";
        }
    }

    public String GuardarArticulos(HttpServletRequest request) {

        int IdArticulo = Globales.Validarintnonull(request.getParameter("IdArticulo"));
        String Referencia = request.getParameter("Referencia");
        String TipoInventario = request.getParameter("TipoInventario");
        int Grupo = Globales.Validarintnonull(request.getParameter("Grupo"));
        int Equipo = Globales.Validarintnonull(request.getParameter("Equipo"));
        String Descripcion = request.getParameter("Descripcion");
        int Modelo = Globales.Validarintnonull(request.getParameter("Modelo"));
        int Iva = Globales.Validarintnonull(request.getParameter("Iva"));
        int IvaUSD = Globales.Validarintnonull(request.getParameter("IvaUSD"));
        int Estado = Globales.Validarintnonull(request.getParameter("Estado"));
        String Catalogo = request.getParameter("Catalogo");
        String UsoInterno = request.getParameter("UsoInterno");
        int Presentacion = Globales.Validarintnonull(request.getParameter("Presentacion"));
        int Caracteristica = Globales.Validarintnonull(request.getParameter("Caracteristica"));
        String CodigoBarras = request.getParameter("CodigoBarras");
        int Garantia = Globales.Validarintnonull(request.getParameter("Garantia"));

        double Precio1 = Globales.ValidardoublenoNull(request.getParameter("Precio1"));
        double Precio2 = Globales.ValidardoublenoNull(request.getParameter("Precio2"));
        double Precio3 = Globales.ValidardoublenoNull(request.getParameter("Precio3"));
        double Precio4 = Globales.ValidardoublenoNull(request.getParameter("Precio4"));
        double Precio5 = Globales.ValidardoublenoNull(request.getParameter("Precio5"));
        double Precio6 = Globales.ValidardoublenoNull(request.getParameter("Precio6"));
        double Precio7 = Globales.ValidardoublenoNull(request.getParameter("Precio7"));
        double Precio8 = Globales.ValidardoublenoNull(request.getParameter("Precio8"));
        double Precio9 = Globales.ValidardoublenoNull(request.getParameter("Precio9"));
        double Precio10 = Globales.ValidardoublenoNull(request.getParameter("Precio10"));

        double PrecioUSD1 = Globales.ValidardoublenoNull(request.getParameter("PrecioUSD1"));
        double PrecioUSD2 = Globales.ValidardoublenoNull(request.getParameter("PrecioUSD2"));
        double PrecioUSD3 = Globales.ValidardoublenoNull(request.getParameter("PrecioUSD3"));
        double PrecioUSD4 = Globales.ValidardoublenoNull(request.getParameter("PrecioUSD4"));
        double PrecioUSD5 = Globales.ValidardoublenoNull(request.getParameter("PrecioUSD5"));
        double PrecioUSD6 = Globales.ValidardoublenoNull(request.getParameter("PrecioUSD6"));
        double PrecioUSD7 = Globales.ValidardoublenoNull(request.getParameter("PrecioUSD7"));
        double PrecioUSD8 = Globales.ValidardoublenoNull(request.getParameter("PrecioUSD8"));
        double PrecioUSD9 = Globales.ValidardoublenoNull(request.getParameter("PrecioUSD9"));
        double PrecioUSD10 = Globales.ValidardoublenoNull(request.getParameter("PrecioUSD10"));

        double UltimoCosto = Globales.ValidardoublenoNull(request.getParameter("UltimoCosto"));
        double UltimoCostoUSD = Globales.ValidardoublenoNull(request.getParameter("UltimoCostoUSD"));
        int CuentaContable = Globales.Validarintnonull(request.getParameter("CuentaContable"));
        int Proveedor = Globales.Validarintnonull(request.getParameter("Proveedor"));

        String Mensaje = "";
        int IdPrecio = 0;
        int CodigoInterno = 0;
        String Codigo;
        int encontrado = 0;
        String Abreviatura = "";
        int ValorIva = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT valor from iva where id = " + Iva));
        CodigoBarras = CodigoBarras.trim();
        try {
            if (!CodigoBarras.equals("")) {
                sql = "SELECT id from inventarios.articulos WHERE codigobarras = '" + CodigoBarras + "' and id <> " + IdArticulo;
                encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                if (encontrado > 0) {
                    return "1|El código de barras " + CodigoBarras + " ya se encuentra asignado a otro artículo en el inventario";
                }
            }

            sql = "SELECT id from inventarios.articulos WHERE idcaracteristica = " + Caracteristica + " and id <> " + IdArticulo + " and idmodelo=" + Modelo + " and idequipo=" + Equipo;
            encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (encontrado > 0) {
                return "1|Este equipo con estas características ya se encuentra registro";
            }

            sql = "SELECT to_char(coalesce(max(a.codigointerno),0)+1,'0000000') as codigo, abreviatura"
                    + "  from inventarios.grupos g left join inventarios.equipos e on e.idgrupo = g.id "
                    + "	                                  left join inventarios.articulos a on e.id = a.idequipo"
                    + "   WHERE g.id=" + Grupo + "group by abreviatura";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarArticulos", 1);
            datos.next();
            Abreviatura = datos.getString("abreviatura");
            CodigoInterno = Globales.Validarintnonull(datos.getString("codigo"));
            Codigo = datos.getString("codigo").trim();

            if (IdArticulo == 0) {
                if (Globales.PermisosSistemas("INVENTARIO AGREGAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO inventarios.articulos(referencia, uso_interno, tipo, idequipo, descripcion, idmodelo,idiva, idproveedor, estado, catalogo, ultimocosto, idpresentacion, idcaracteristica, codigointerno, codigobarras, idcuecontable, ultimocostousd, idivausd, garantia_mes)"
                        + "   VALUES('" + Referencia + "','" + UsoInterno + "','" + TipoInventario + "'," + Equipo + ",'" + Descripcion.trim().toUpperCase() + "'," + Modelo + "," + Iva + "," + Proveedor + "," + Estado + ",'" + Catalogo + "'," + UltimoCosto + "," + Presentacion + "," + Caracteristica + "," + CodigoInterno + ",'" + CodigoBarras + "'," + CuentaContable + "," + UltimoCostoUSD + "," + IvaUSD + "," + Garantia + ");";
                if (Globales.DatosAuditoria(sql, "INVENTARIO", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarArticulos")) {
                    IdArticulo = Globales.Validarintnonull(Globales.ObtenerUnValor(" SELECT MAX(id) FROM inventarios.articulos;"));
                    Mensaje = "Artículo agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {

                if (Globales.PermisosSistemas("INVENTARIO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql
                        = "UPDATE inventarios.articulos "
                        + " SET tipo ='" + TipoInventario + "'  , idequipo=" + Equipo + "  ,descripcion='" + Descripcion.trim().toUpperCase() + "'  " + ",idmodelo=" + Modelo + ",idiva=" + Iva + ",idproveedor=" + Proveedor + ",estado=" + Estado
                        + ", catalogo='" + Catalogo + "',ultimocosto=" + UltimoCosto + ", referencia='" + Referencia + "', uso_interno = '" + UsoInterno + "'"
                        + ", idpresentacion=" + Presentacion + ", idcaracteristica=" + Caracteristica + ", codigoBarras='" + CodigoBarras + "', idcuecontable = " + CuentaContable
                        + ", ultimocostousd = " + UltimoCostoUSD + ",idivausd=" + IvaUSD + ", garantia_mes=" + Garantia + " WHERE id= " + IdArticulo;

                if (Globales.DatosAuditoria(sql, "INVENTARIO", "ACTUALIZAR", idusuario, iplocal, this.getClass() + "-->GuardarArticulos")) {
                    Mensaje = "Artículo actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

            IdPrecio = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.articulos_precios WHERE idarticulo=" + IdArticulo));
            if (IdPrecio == 0) {
                sql = "INSERT INTO inventarios.articulos_precios(idarticulo, precio1, precio2, precio3, precio4, precio5, "
                        + "precio6, precio7, precio8, precio9, precio10, preciousd1, preciousd2, preciousd3, preciousd4, preciousd5, "
                        + "preciousd6, preciousd7, preciousd8, preciousd9, preciousd10)"
                        + "VALUES(" + IdArticulo + ", " + Precio1 + ", " + Precio2 + ", " + Precio3 + ", " + Precio4 + "," + Precio5 + "," + Precio6 + "," + Precio7 + "," + Precio8 + "," + Precio9 + "," + Precio10
                        + "," + PrecioUSD1 + ", " + PrecioUSD2 + ", " + PrecioUSD3 + ", " + PrecioUSD4 + "," + PrecioUSD5 + "," + PrecioUSD6 + "," + PrecioUSD7 + "," + PrecioUSD8 + "," + PrecioUSD9 + "," + PrecioUSD10 + ")";
            } else {
                sql
                        = "UPDATE inventarios.articulos_precios"
                        + "          SET precio1 = " + Precio1 + ", precio2 = " + Precio2 + ", precio3 = " + Precio3 + ", precio4 =" + Precio4
                        + ",precio5=" + Precio5 + ", precio6=" + Precio6 + ", precio7=" + Precio7 + ", precio8=" + Precio8 + ", precio9=" + Precio9 + ", precio10=" + Precio10
                        + ",preciousd1=" + PrecioUSD1 + ", preciousd2 = " + PrecioUSD2 + ", preciousd3 = " + PrecioUSD3 + ", preciousd4 =" + PrecioUSD4
                        + ",preciousd5=" + PrecioUSD5 + ", preciousd6=" + PrecioUSD6 + ", preciousd7=" + PrecioUSD7 + ", preciousd8=" + PrecioUSD8 + ", preciousd9=" + PrecioUSD9 + ", preciousd10=" + PrecioUSD10 + " WHERE id=" + IdPrecio;
            }
            Globales.DatosAuditoria(sql, "INVENTARIO", "ACTUALIZAR PRECIOS", idusuario, iplocal, this.getClass() + "-->GuardarArticulos");

            File uploads;
            File destino;

            if (archivoadjunto != null) {
                uploads = new File(Globales.ruta_archivo + "uploads/" + archivoadjunto);
                destino = new File(Globales.ruta_archivo + "Adjunto/FichaTecnica/" + IdArticulo + ".pdf");
                Files.copy(uploads.toPath(), destino.toPath(), StandardCopyOption.REPLACE_EXISTING);
                sql = "UPDATE inventarios.articulos "
                        + " SET fichatecnica = 1  WHERE id= " + IdArticulo;
                Globales.DatosAuditoria(sql, "INVENTARIO", "ACTUALIZAR FICHA TECNICA", idusuario, iplocal, this.getClass() + "-->GuardarArticulos");
                archivoadjunto = null;
            }

            if (archivoadjunto2 != null) {
                uploads = new File(Globales.ruta_archivo + "uploads/" + archivoadjunto2);
                destino = new File(Globales.ruta_archivo + "Adjunto/ManualUsuario/" + IdArticulo + ".pdf");
                Files.copy(uploads.toPath(), destino.toPath(), StandardCopyOption.REPLACE_EXISTING);
                sql = "UPDATE inventarios.articulos "
                        + " SET manual_usuario = 1  WHERE id= " + IdArticulo;
                Globales.DatosAuditoria(sql, "INVENTARIO", "ACTUALIZAR FICHA TECNICA", idusuario, iplocal, this.getClass() + "-->GuardarArticulos");
                archivoadjunto2 = null;
            }

            return "0|" + Mensaje + "|" + IdArticulo + "|" + Abreviatura + "-" + Codigo + "|" + ValorIva;

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String ModalArticulos(HttpServletRequest request) {
        String codigo = request.getParameter("codigo");
        String descripcion = request.getParameter("descripcion");
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int precio = Globales.Validarintnonull(request.getParameter("precio"));
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));

        String busqueda = " WHERE a.estado = 1 ";
        if (grupo > 0) {
            busqueda += " and g.id=" + grupo;
        }
        if (!codigo.equals("")) {
            busqueda += " and (abreviatura || '-' || trim(to_char(codigointerno,'0000000')) ilike '%" + codigo + "%' or codigobarras ilike '%" + codigo + "%')";
        }
        if (!descripcion.equals("")) {
            busqueda += " and (e.descripcion ilike '%" + descripcion + "%' or ma.descripcion ilike '%" + descripcion + "%' or mo.descripcion ilike '%" + descripcion + "%' or pr.descripcion ilike '%" + descripcion + "%' or ca.descripcion ilike '%" + descripcion + "%')";
        }
        if (ubicacion > 0) {
            busqueda += " and u.idbodega = " + ubicacion;
        }

        sql = "select case when a.foto1 > 0 then '<img src=''" + url_archivo + "Adjunto/imagenes/Articulos/' || a.id || '/1.jpg'' width=''80px''/><br>' else '' end as foto, " 
                + "a.id, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, a.codigobarras, g.descripcion as grupo, a.tipo, e.descripcion as equipo, " 
                + "ma.descripcion as marca, mo.descripcion as modelo, pr.descripcion as presentacion, ca.descripcion as caracteristica, existencia , " 
                + "0 as separadas, 0 as cotizadas, existencia as disponibles, a.ultimocosto, precio" + precio + " as precio, a.ultimocostousd, "
                + "preciousd" + precio + " as preciousd, referencia, uso_interno,  "
                + "i.valor as iva, c.cuenta, p.nombrecompleto as proveedor, a.id as idarticulo "
                + " FROM inventarios.articulos a inner join inventarios.equipos e on a.idequipo = e.id  "
                + "              " + (ubicacion > 0 ? " inner join inventarios.articulo_ubicacion u on u.idarticulo = e.id " : "") + " "
                + "                        inner join inventarios.grupos g on g.id = e.idgrupo"
                + "                        inner join inventarios.modelos mo on mo.id = a.idmodelo  "
                + "                        inner join inventarios.marcas ma on mo.idmarca = ma.id"
                + "                        inner join iva i on a.idiva = i.id"
                + "                        inner join contabilidad.cuenta c on c.id = a.idcuecontable"
                + "                        inner join proveedores p on a.idproveedor = p.id "
                + "                        inner join inventarios.presentacion pr on pr.id = a.idpresentacion \n"
                + "                        inner join inventarios.caracteristica ca on ca.id = a.idcaracteristica \n"
                + "                        inner join inventarios.articulos_precios pre on pre.idarticulo = a.id \n"
                + "                        inner join tipo_estado te on a.estado = te.id " + busqueda + " ORDER BY a.codigointerno";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ModalArticulos");
    }

    public String DescripcionPrecio() {

        sql = "SELECT id, descripcion ||  ' (' || porcentaje || '%)' as precio, porcentaje, descripcion "
                + "  from tablaprecios order by id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DescripcionPrecio");
    }

    public String CodigoTomaFisica(HttpServletRequest request) {
        int opcion = Globales.Validarintnonull(request.getParameter("opcion"));
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        String presentacion = request.getParameter("presentacion");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));

        String combo = "";
        int idgrupo = 0;
        switch (opcion) {
            case 1:
                sql = "SELECT distinct e.id, e.descripcion "
                        + "  FROM inventarios.articulos a INNER JOIN inventarios.equipos e on a.idequipo = e.id "
                        + " WHERE e.idgrupo = " + grupo + " and a.estado = 1 ORDER BY 2";
                combo = Globales.ObtenerCombo(sql, 0, 0, 0,0);
                if (combo.equals("")) {
                    combo = "XX";
                }
                return combo;
            case 2:
                sql = "SELECT distinct presentacion, presentacion "
                        + "  FROM inventarios.articulos a INNER JOIN inventarios.equipos e on a.idequipo = e.id "
                        + " WHERE e.id = " + equipo + " and a.estado = 1 ORDER BY 2";
                combo = Globales.ObtenerCombo(sql, 0, 0, 0,0);
                combo = "<option value=''>NINGUNA</option>" + combo;
                sql = "SELECT idgrupo"
                        + "  FROM inventarios.equipos "
                        + " WHERE id = " + equipo;
                idgrupo = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                return combo + "||" + idgrupo;
            case 3:
                sql = "SELECT distinct ma.id, ma.descripcion FROM inventarios"
                        + ".articulos a INNER JOIN inventarios.modelos mo on a.idmodelo = mo.id "
                        + "INNER JOIN inventarios.marcas ma on mo.idmarca = ma.id "
                        + "WHERE a.idequipo = " + equipo + " and presentacion = '" + presentacion + "'  and a.estado = 1 ORDER BY 2";

                combo = Globales.ObtenerCombo(sql, 0, 0, 0,0);
                if (combo.equals("")) {
                    combo = "XX";
                }
                return combo;
            case 4:
                sql
                        = "SELECT distinct mo.id, mo.descripcion  FROM inventarios"
                        + ".articulos a INNER JOIN inventarios.modelos mo on a"
                        + ".idmodelo = mo.id WHERE mo.idmarca = " + marca + " and a"
                        + ".estado = 1 and a.idequipo = " + equipo + " and a"
                        + ".presentacion = '" + presentacion + "'  ORDER BY 2";

                combo = Globales.ObtenerCombo(sql, 0, 0, 0,0);
                if (combo.equals("")) {
                    combo = "XX";
                }
                return combo;
            case 5:
                sql
                        = "select abreviatura || '-' || trim(to_char(codigointerno,'0000000')), abreviatura || '-' || trim(to_char(codigointerno,'0000000'))"
                        + "FROM inventarios.articulos a inner join inventarios.equipos e on a"
                        + ".idequipo = e.id  inner join inventarios.grupos g on g.id = e.idgrupo"
                        + "WHERE a.idmodelo = " + modelo + " and a"
                        + ".estado = 1 and a.idequipo = " + equipo + " and a"
                        + ".presentacion = '" + presentacion + "'  ORDER BY 2";

                combo = Globales.ObtenerCombo(sql, 0, 0, 0,0);
                if (combo.equals("")) {
                    combo = "XX";
                }
                return combo;
        }

        return "";

    }

    public String BuscarArticulosToma(HttpServletRequest request) {
        String codigo = request.getParameter("codigo");

        sql = "select a.id, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, codigobarras, presentacion, a.estado,  "
                + "      mo.descripcion as modelo, ma.descripcion as marca, g.descripcion as grupo, a.tipo, e.descripcion as equipo, a.existencia, a.ultimocosto,"
                + "     a.idcuecontable  "
                + "from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                              inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                             inner join inventarios.equipos e on a.idequipo = e.id  "
                + "                             inner join inventarios.grupos g on g.id = e.idgrupo"
                + "where abreviatura || '-' || trim(to_char(codigointerno,'0000000'))  = '" + codigo + "' or"
                + "    codigobarras = '" + codigo + "'";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarArticulosToma");
    }

    public String GuardarToma(HttpServletRequest request) {
        int articulo = Globales.Validarintnonull(request.getParameter("articulo"));
        int cantidad = Globales.Validarintnonull(request.getParameter("cantidad"));
        String descripcion = request.getParameter("descripcion");
        int toma = Globales.Validarintnonull(request.getParameter("toma"));
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter("responsable"));

        try {
            if (Globales.PermisosSistemas("TOMA FISICA GUARDAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            int idubicacion = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from inventarios.articulo_ubicacion where idarticulo = " + articulo + " and idbodega=" + ubicacion + " and idresponsable=" + responsable));
            if (idubicacion == 0) {
                sql = "INSERT INTO inventarios.articulo_ubicacion(idarticulo, idbodega, cantidad, idresponsable, "
                        + " tomafisica" + toma + ", fechatoma" + toma + ", idusuariotoma" + toma + ")"
                        + " VALUES(" + articulo + ", " + ubicacion + ", 0, " + responsable + ", " + cantidad + ", now(), " + idubicacion + ")";
            } else {
                sql = "UPDATE inventarios.articulo_ubicacion"
                        + " SET tomafisica" + toma + "=coalesce(tomafisica" + toma + ",0) + " + cantidad + ",fechatoma" + toma + "=now(), idusuariotoma" + toma + "=" + idusuario + " "
                        + " WHERE id = " + idubicacion;
            }
            if (Globales.DatosAuditoria(sql, "TOMA FISICA", "GUARDAR", idusuario, iplocal, this.getClass() + "-->BuscarArticulosToma")) {
                return "0|Toma física agregada con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String TablaTomaFisica(HttpServletRequest request) {
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter("responsable"));
        sql = "select row_number() OVER(order by a.id) as fila, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, codigobarras, "
                + "      '<b>Tipo:</b> ' || tipo || ', <b>Grupo:</b> ' || g.descripcion || ', <b>Presentación:</b> ' || presentacion || "
                + "      '<b>, Marca:</b> ' || ma.descripcion ||', <b>Modelo:</b> ' || mo.descripcion as descripcion,"
                + "      tomafisica1, tomafisica2,  to_char(fechatoma1, 'yyyy/MM/dd HH24:MI') || '<br><b>' || u1.nomusu as fecha1,"
                + "      to_char(fechatoma2, 'yyyy/MM/dd HH24:MI') || '<br><b>' || u2.nomusu as fecha2,"
                + "      '<button class=''btn btn-glow btn-danger'' title=''Eliminar Toma Física'' type=''button'' onclick=' || chr(34) || 'EliminarTomaFisica(' || a.id || ',''' ||  abreviatura || '-' || trim(to_char(codigointerno,'0000000')) || ''')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                + " from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                         inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                         inner join inventarios.equipos e on a.idequipo = e.id  "
                + "                         inner join inventarios.grupos g on g.id = e.idgrupo"
                + "                         inner join inventarios.articulo_ubicacion au on au.idarticulo = a.id and idbodega = " + ubicacion + " and idresponsable = " + responsable + " "
                + "                         inner join seguridad.rbac_usuario u1 on u1.idusu = idusuariotoma1"
                + "                         inner join seguridad.rbac_usuario u2 on u2.idusu = idusuariotoma2"
                + " where tomafisica1 is not null or tomafisica2 is not null ORDER BY fechatoma1 desc, fechatoma2 desc";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaTomaFisica");
    }

    public String InicializarToma(HttpServletRequest request) {
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter("responsable"));

        if (Globales.PermisosSistemas("TOMA FISICA INICIALIZAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        sql = "UPDATE inventarios.articulo_ubicacion"
                + "      SET tomafisica1 = null, fechatoma1 = null, idusuariotoma1=0, tomafisica2 = null, fechatoma2 = null, idusuariotoma2=0"
                + "  where tomafisica1 is not null or tomafisica2 is not null and idubicacion=" + ubicacion + " and idresponsable=" + responsable;
        if (Globales.DatosAuditoria(sql, "TOMA FISICA", "INICIALIZAR", idusuario, iplocal, this.getClass() + "-->BuscarArticulosToma")) {
            return "0|Toma física inicializada con éxito";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }

    }

    public String EliminarToma(HttpServletRequest request) {
        int articulo = Globales.Validarintnonull(request.getParameter("articulo"));
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter(" responsable"));

        if (Globales.PermisosSistemas("TOMA FISICA ELIMINAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        sql = "UPDATE inventarios.articulos"
                + "      SET tomafisica1 = null, fechatoma1 = null, idusuariotoma1=0, tomafisica2 = null, fechatoma2 = null, idusuariotoma2=0"
                + "  where id=" + articulo + "and idubicacion = " + ubicacion + " and idresponsable = " + responsable;

        if (Globales.DatosAuditoria(sql, "TOMA FISICA", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarToma")) {
            return "0|Toma física eliminada con éxito";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }

    }

    public String ConsultaTomaExcel(HttpServletRequest request) {
        String tipo = request.getParameter("tipo");
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        String presentacion = request.getParameter("presentacion");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        int nulo = Globales.Validarintnonull(request.getParameter("nulo"));
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter("responsable"));

        String busqueda = "where a.estado = 1";

        if (nulo == 1) {
            busqueda += " and tomafisica1 is not null";
        } else {
            if (ubicacion != -1) {
                busqueda += " and coalesce(au.idbodega,0) = " + ubicacion;
            }
            if (responsable != -1) {
                busqueda += " and coalesce(au.idresponsable,0) = " + ubicacion;
            }
            if (nulo == 2) {
                busqueda += " and tomafisica1 is not null";
            }
            if (!tipo.equals("")) {
                busqueda += " and a.tipo = '" + tipo + "'";
            }
            if (!presentacion.equals("")) {
                busqueda += " and a.presentacion = '" + presentacion + "'";
            }
            if (grupo > 0) {
                busqueda += " and g.id = " + grupo;
            }
            if (equipo > 0) {
                busqueda += " and e.id = " + equipo;
            }
            if (marca > 0) {
                busqueda += " and ma.id = " + marca;
            }
            if (modelo > 0) {
                busqueda += " and mo.id = " + modelo;
            }
        }

        sql = "select b.descripcion as ubicacion, u.nombrecompleto as responsable,  row_number() OVER(order by a.id) as fila, a.id, au.id as idubicacion, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, presentacion, au.cantidad as existencia,"
                + "      mo.descripcion as modelo, ma.descripcion as marca, g.descripcion as grupo, a.tipo, e.descripcion as equipo, ultimocosto, tomafisica1, au.cantidad - tomafisica1 as diferencia,"
                + "     (au.cantidad - tomafisica1)*ultimocosto as valor"
                + " from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                          inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                          inner join inventarios.equipos e on a.idequipo = e.id "
                + "                          inner join inventarios.grupos g on g.id = e.idgrupo"
                + "                          left join inventarios.articulo_ubicacion au on au.idarticulo = a.id"
                + "			 left join inventarios.bodega b on b.id = au.idbodega"
                + "			 left join seguridad.rbac_usuario u on u.idusu = au.idresponsable " + busqueda + " order by 2";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultaTomaExcel");
    }

    /*
      public String ImportarArchivo()
        {
           
            if (Globales.PermisosSistemas("TOMA FISICA IMPORTAR EXCEL",idusuario) == 0)
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            
            String resultado = "";
            int id = 0;
            int toma = 0;

            
            String ruta = HostingEnvironment.MapPath("~/uploads/" + Session["ArchivoExcel"].ToString());
            
            ExcelWorksheet workSheet;

            try
            {
                sql = "";
                var package1 = new ExcelPackage(new FileInfo(ruta));
                workSheet = package1.Workbook.Worksheets[1];

                for (int i = 3; i <= workSheet.Dimension.End.Row; i++)
                {
                    if (workSheet.Cells[i, 2].Value != null)
                    {
                        id = Globales.Validarintnonull(workSheet.Cells[i, 2].Value.ToString());
                        toma = 0;
                        if (workSheet.Cells[i, 13].Value != null)
                            toma = Globales.Validarintnonull(workSheet.Cells[i, 13].Value.ToString());
                        if (id > 0)
                        {
                            sql += "UPDATE inventarios.articulos"+
                                   "     SET tomafisica1 = " + toma + ", fechatoma1 = now(), idusuariotoma1=" + idusuario + " "+
                                    "where id=" + id + ";";

                        }
                    }
                }

                if (!sql.equals(""))
                {
                    Globales.DatosAuditoria(sql, "TOMA FISICA","IMPORTAR EXCEL", idusuario, iplocal);
                    Session["ArchivoExcel"] = null;
                    resultado = "0|Toma física importada con éxito";
                }
                else
                {
                    resultado = "1|No se encontrarón movimientos en el archivo";
                }

            }
            catch (Exception ex)
            {
                return "1|" + ex.getMessage();
            }

            return resultado;
        }
     */
    public String CerrarTomaFisica(HttpServletRequest request) {
        String tipo = request.getParameter("tipo");
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        String presentacion = request.getParameter(" presentacion");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));

        String busqueda = "where a.estado = 1 and a.tomafisica1 is not null";
        if (!tipo.equals("")) {
            busqueda += " and a.tipo = '" + tipo + "'";
        }
        if (!presentacion.equals("")) {
            busqueda += " and a.presentacion = '" + presentacion + "'";
        }
        if (grupo > 0) {
            busqueda += " and g.id = " + grupo;
        }
        if (equipo > 0) {
            busqueda += " and e.id = " + equipo;
        }
        if (marca > 0) {
            busqueda += " and ma.id = " + marca;
        }
        if (modelo > 0) {
            busqueda += " and mo.id = " + modelo;
        }

        if (Globales.PermisosSistemas("TOMA FISICA CERRAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        sql = "SELECT count(a.id) "
                + " from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                             inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                             inner join inventarios.equipos e on a.idequipo = e.id"
                + "                             inner join inventarios.grupos g on g.id = e.idgrupo " + busqueda + ";";
        int registros = Globales.Validarintnonull(Globales.ObtenerUnValor(sql)) + 1;

        if (registros == 0) {
            return "0|No hay registros de toma física para realizar el cierre";
        }

        int contador = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT tomafisica from contadores")) + 1;

        sql = "INSERT INTO inventarios.kardex(idarticulo, documento, cantidad, costo, tipo, idusuario, es)"
                + "  SELECT a.id, " + contador + ",tomafisica1 - existencia, ultimocosto, 'Toma Física'," + idusuario + ",1"
                + "  from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                          inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                          inner join inventarios.equipos e on a.idequipo = e.id "
                + "                          inner join inventarios.grupos g on g.id = e.idgrupo " + busqueda + ";";

        sql += "INSERT INTO inventarios.toma_fisica(idarticulo, control, idusuario, tomafisica, existencia, diferencia, valor)"
                + " SELECT a.id, " + contador + "," + idusuario + ",tomafisica1, existencia, existencia-tomafisica1, ultimocosto"
                + " from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                         inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                         inner join inventarios.equipos e on a.idequipo = e.id "
                + "                         inner join inventarios.grupos g on g.id = e.idgrupo " + busqueda + ";";

        sql += "UPDATE inventarios.articulos"
                + "     SET existencia = a.tomafisica1, tomafisica1 = null, fechatoma1 = null, idusuariotoma1=0, tomafisica2 = null, fechatoma2 = null, idusuariotoma2=0"
                + " from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                            inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                           inner join inventarios.equipos e on a.idequipo = e.id "
                + "                          inner join inventarios.grupos g on g.id = e.idgrupo " + busqueda + " AND inventarios.articulos.id = a.id;";
        sql += "UPDATE contadores set tomafisica = " + contador + ";";

        if (Globales.DatosAuditoria(sql, "TOMA FISICA", "CERRAR", idusuario, iplocal, this.getClass() + "-->CerrarTomaFisica")) {
            return "0|Toma física cerrada con el número|" + contador;
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }

    }

    public String ConsultaAuditoriaToma(HttpServletRequest request) {
        int codigo = Globales.Validarintnonull(request.getParameter("codigo"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        String tipo = request.getParameter("tipo");
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        String presentacion = request.getParameter("presentacion");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        String busqueda = "where to_char(t.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(t.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
        if (codigo > 0) {
            busqueda += " and t.control = " + codigo;
        } else {
            if (!tipo.equals("")) {
                busqueda += " and a.tipo = '" + tipo + "'";
            }
            if (!presentacion.equals("")) {
                busqueda += " and a.presentacion = '" + presentacion + "'";
            }
            if (grupo > 0) {
                busqueda += " and g.id = " + grupo;
            }
            if (equipo > 0) {
                busqueda += " and e.id = " + equipo;
            }
            if (marca > 0) {
                busqueda += " and ma.id = " + marca;
            }
            if (modelo > 0) {
                busqueda += " and mo.id = " + modelo;
            }
        }

        sql = "select row_number() OVER(order by a.id) as fila, to_char(t.fecha,'yyyy/MM/dd HH24:MI') as fecha, t.control, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, presentacion, "
                + "      mo.descripcion as modelo, ma.descripcion as marca, g.descripcion as grupo, a.tipo, e.descripcion as equipo, "
                + "      t.existencia, t.tomafisica, t.diferencia, t.diferencia*t.valor as valor, u.nombrecompleto as usuario"
                + "  from inventarios.toma_fisica t inner join inventarios.articulos a on a.id = t.idarticulo"
                + "                           INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                           inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                           inner join inventarios.equipos e on a.idequipo = e.id  "
                + "                           inner join inventarios.grupos g on g.id = e.idgrupo "
                + "                           inner join seguridad.rbac_usuario u on u.idusu = t.idusuario " + busqueda + " order by t.control, 3";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultaAuditoriaToma");
    }

    public String TablaTraslado(HttpServletRequest request) {
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter("responsable"));
        int traslado = Globales.Validarintnonull(request.getParameter("traslado"));

        String busqueda = "where td.idoficina = " + ubicacion + " and td.idresponsable = " + responsable + " and td.idusuario=" + idusuario + " and idtraslado = " + traslado;

        sql = "select row_number() OVER(order by a.id) as fila, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, codigobarras, "
                + "      '<b>Tipo:</b> ' || tipo || ', <b>Grupo:</b> ' || g.descripcion || ', <b>Presentación:</b> ' || presentacion || "
                + "      '<b>, Marca:</b> ' || ma.descripcion ||', <b>Modelo:</b> ' || mo.descripcion as descripcion,"
                + "      td.cantidad, td.cantidad * td.costo as valor, to_char(td.fecha, 'yyyy/MM/dd HH24:MI') as fecha,"
                + "      '<button class=''btn btn-glow btn-danger'' title=''Eliminar Traslado Detalle'' type=''button'' onclick=' || chr(34) || 'EliminarTraslado(' || td.id || ',''' ||  abreviatura || '-' || trim(to_char(codigointerno,'0000000')) || ''')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                + "  from inventarios.traslado_detalle td inner join  inventarios.articulos a on td.idarticulo = a.id"
                + "                              INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                              inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                              inner join inventarios.equipos e on a.idequipo = e.id "
                + "                              inner join inventarios.grupos g on g.id = e.idgrupo " + busqueda + " ORDER BY td.id desc";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaTraslado");
    }

    public String GuardarTrasladoDetalle(HttpServletRequest request) {
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter("responsable"));
        int articulo = Globales.Validarintnonull(request.getParameter("articulo"));
        int cantidad = Globales.Validarintnonull(request.getParameter("cantidad"));

        try {
            if (Globales.PermisosSistemas("TRASLADO ARTICULO AGREGAR DETALLE", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "SELECT existencia, ultimocosto from inventarios.articulos where id=" + articulo;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarTrasladoDetalle", 1);

            int existencia = Globales.Validarintnonull(datos.getString("existencia"));
            double costo = Globales.ValidardoublenoNull(datos.getString("ultimocosto"));

            if (existencia <= 0) {
                return "1|El artículo no posee existencias disponibles";
            }
            if (cantidad > existencia) {
                return "1|La cantidad a trasladar [" + cantidad + "] es mayor a la existencia actual del artículo [" + existencia + "]";
            }
            String busqueda = "where td.idoficina = " + ubicacion + " and td.idresponsable = " + responsable + " and td.idusuario=" + idusuario + " and idtraslado = 0";

            sql = "UPDATE inventarios.articulos set existencia = existencia - " + cantidad + " where id = " + articulo + ";";
            sql += "INSERT INTO inventarios.traslado_detalle(idarticulo, idusuario, cantidad, costo, idoficina, idresponsable)"
                    + "      VALUES (" + articulo + "," + idusuario + "," + cantidad + "," + costo + "," + ubicacion + "," + responsable + "); select max(id) from inventarios.traslado_detalle;";
            if (Globales.DatosAuditoria(sql, "TRASLADO ARTICULO", "AGREGAR DETALLE", idusuario, iplocal, this.getClass() + "-->GuardarTrasladoDetalle")) {
                return "0|Artículo agregado con éxito|";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception e) {
            return "1|" + e.getMessage();
        }
    }

    public String EliminarTraslado(HttpServletRequest request) {
        int detalle = Globales.Validarintnonull(request.getParameter("detalle"));

        if (Globales.PermisosSistemas("TRASLADO ELIMINAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        sql = "UPDATE inventarios.articulos SET existencia = existencia  + cantidad "
                + " from inventarios.traslado_detalle td "
                + " where inventarios.articulos.id = td.idarticulo and td.id =" + detalle + ";";
        sql += "DELETE FROM inventarios.traslado_detalle WHERE id = " + detalle;

        if (Globales.DatosAuditoria(sql, "TRASLADO ARTICULO", "ELIMINAR", idusuario, iplocal, this.getClass() + "--> EliminarTraslado")) {
            return "0|Items del traslado eliminado con éxito";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }

    }

    public String InicializarTraslado(HttpServletRequest request) {
        try {
            int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
            int responsable = Globales.Validarintnonull(request.getParameter("responsable"));

            if (Globales.PermisosSistemas("TRASLADO INICIALIZAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "SELECT id, idarticulo, cantidad FROM inventarios.traslado_detalle WHERE idtraslado = 0 and idoficina = " + ubicacion + " and idresponsable = " + responsable + " and idusuario=" + idusuario;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->InicializarTraslado", 1);
            sql = "";

            while (datos.next()) {
                sql += "UPDATE inventarios.articulos SET existencia = existencia  + " + datos.getString("cantidad") + "WHERE id = " + datos.getString("idarticulo") + ";";
                sql += "DELETE FROM inventarios.traslado_detalle WHERE idtraslado = 0 and idoficina = " + ubicacion + " and idresponsable = " + responsable + " and idusuario=" + idusuario;

            }
            if (!sql.equals("")) {
                if (Globales.DatosAuditoria(sql, "TRASLADO", "INICIALIZAR", idusuario, iplocal, this.getClass() + "-->InicializarTraslado")) {
                    return "0|Traslado inicializado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                return "1|Este traslado no contiene items para eliminar";
            }

        } catch (SQLException ex) {
            Logger.getLogger(Inventarios.class.getName()).log(Level.SEVERE, null, ex);
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarTraslado(HttpServletRequest request) {
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter("responsable"));
        int idtraslado;
        try {

            if (Globales.PermisosSistemas("TRASLADO GUARDAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "SELECT count(idarticulo), sum(cantidad) as cantidad, sum(cantidad*costo) as costo FROM inventarios.traslado_detalle WHERE idtraslado = 0 and idoficina = " + ubicacion + " and idresponsable = " + responsable + " and idusuario=" + idusuario;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarTraslado", 1);
            if (!datos.next()) {
                return "1|Este traslado no contiene items para trasladar";
            }
            int cantidades = Globales.Validarintnonull(datos.getString("cantidad"));
            double costos = Globales.ValidardoublenoNull(datos.getString("costo"));
            sql = "";
            int contador = Globales.Validarintnonull(Globales.ObtenerUnValor("select traslado from contadores")) + 1;
            sql += "INSERT INTO inventarios.traslado(traslado, idresponsable, idoficina, idusuario, cantidad, costo)"
                    + "  VALUES (" + contador + "," + responsable + "," + ubicacion + "," + idusuario + "," + cantidades + "," + costos + ");"
                    + " ";
            if (Globales.DatosAuditoria(sql, "INVENTARIO", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarTraslado")) {
                idtraslado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT MAX(id) from inventarios.traslado;"));
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

            sql = "INSERT INTO inventarios.kardex(idarticulo, cantidad, costo, tipo, idusuario, es, documento)"
                    + "   SELECT idarticulo, cantidad, costo, 'TRASLADO INTERNO',2," + contador + ""
                    + "   FROM inventarios.traslado_detalle "
                    + "   WHERE idtraslado = 0 and idoficina = " + ubicacion + " and idresponsable = " + responsable + " and idusuario=" + idusuario + ";"
                    + "  UPDATE contadores SET traslado=" + contador + ";";

            sql += "UPDATE  inventarios.traslado_detalle  SET idtraslado=" + contador + ""
                    + "  WHERE idtraslado = 0 and idoficina = " + ubicacion + " and idresponsable = " + responsable + " and idusuario=" + idusuario + ";";

            if (Globales.DatosAuditoria(sql, "TRASLADO", "AGREGAR DETALLES", idusuario, iplocal, this.getClass() + "-->GuardarTraslado")) {
                return "0|Traslado guardado con éxito con el número|" + contador + "|" + idtraslado;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (SQLException ex) {
            Globales.GuardarLogger(ex.getMessage(), this.getClass() + "-->GuardarTraslado");
            return "1|" + ex.getMessage();
        }
    }

    public String AnularTraslado(HttpServletRequest request) {
        try {
            int traslado = Globales.Validarintnonull(request.getParameter("traslado"));
            String observacion = request.getParameter("observacion");

            if (Globales.PermisosSistemas("TRASLADO ANULAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "SELECT traslado, estado FROM inventarios.traslado WHERE id = " + traslado;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AnularTraslado", 1);
            datos.next();
            String estado = datos.getString("estado").trim();
            String contador = datos.getString("traslado").trim();

            if (estado.equals("Anulado")) {
                return "1|Este traslado ya fue anulado";
            }

            sql = "update inventarios.traslado set estado='Anulado', idusuarioanula = " + idusuario + ", observacionanula ='" + observacion + "', fechaanula = now()"
                    + "where id = " + traslado + ";";

            sql += "INSERT INTO inventarios.kardex(idarticulo, cantidad, costo, tipo, idusuario, es, documento)"
                    + "  SELECT idarticulo, cantidad, costo, 'TRASLADO INTERNO ANULADO',1," + contador + ""
                    + "  FROM inventarios.traslado_detalle"
                    + "  WHERE idtraslado = " + traslado + ";";

            sql += "UPDATE inventarios.articulos  SET existencia = existencia + "
                    + "  (select sum(cantidad) from inventarios.traslado_detalle td where  inventarios.articulos.id = td.idarticulo and td.idtraslado = " + traslado + ") "
                    + " from inventarios.traslado_detalle "
                    + " where inventarios.articulos.id = td.idarticulo and td.idtraslado = " + traslado;
            if (Globales.DatosAuditoria(sql, "TRASLADO", "ANULAR", idusuario, iplocal, this.getClass() + "-->AnularTraslado")) {
                return "0|Traslado anulado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (SQLException ex) {
            Globales.GuardarLogger(ex.getMessage(), this.getClass() + "-->AnularTraslado");
            return "1|" + ex.getMessage();
        }
    }

    public String TablaConfiguracion(HttpServletRequest request) {
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int estado = Globales.Validarintnonull(request.getParameter("estado"));
        switch (tipo) {
            case 1:
                sql = "SELECT g.*, te.descripcion as desestado, cu.nombre as cuenta, "
                        + "      '<button class=''btn btn-glow btn-danger'' title=''Eliminar Grupo'' type=''button'' onclick=' || chr(34) || 'EliminarGrupo(' || g.id || ',''' || g.descripcion  || ''')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + "  FROM inventarios.grupos g INNER JOIN tipo_estado te on te.id = g.estado "
                        + "                            INNER JOIN contabilidad.cuenta cu on cu.id = g.idcuecontable " + (id > 0 ? " WHERE g.id=" + id : "") + ""
                        + "  ORDER BY descripcion";
                break;
            case 2:
                sql = "SELECT e.id, e.descripcion, e.estado, idgrupo, te.descripcion as desestado, g.descripcion as grupo,"
                        + "        '<button class=''btn btn-glow btn-danger'' title=''Eliminar Equipo'' type=''button'' onclick=' || chr(34) || 'EliminarEquipo(' || e.id || ',''' || e.descripcion  || ' (' || g.descripcion || ')''' || ')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + "    FROM inventarios.equipos e INNER JOIN tipo_estado te on te.id = e.estado"
                        + "			       INNER JOIN inventarios.grupos g on g.id = e.idgrupo " + (id > 0 ? " WHERE e.id=" + id : grupo != 0 ? " WHERE idgrupo = " + grupo : "")
                        + "   ORDER BY e.descripcion";
                break;
            case 3:
                sql = "SELECT m.id, m.descripcion, m.estado, te.descripcion as desestado,"
                        + "      '<button class=''btn btn-glow btn-danger'' title=''Eliminar Marca'' type=''button'' onclick=' || chr(34) || 'EliminarMarca(' || m.id || ',''' || m.descripcion || ''')' || chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + "  FROM inventarios.marcas m INNER JOIN tipo_estado te on te.id = m.estado " + (id > 0 ? " WHERE m.id = " + id : "");
                break;

            case 4:
                sql = "SELECT mo.id, mo.idmarca, mo.descripcion, mo.estado, te.descripcion as desestado, m.descripcion as marca,"
                        + "          '<button class=''btn btn-glow btn-danger'' title=''Eliminar Modelo'' type=''button'' onclick=' || chr(34) || 'EliminarModelo(' || mo.id || ',''' || mo.descripcion || '(' || m.descripcion || ')'',' || mo.idmarca || ')' ||  chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + "    FROM inventarios.modelos mo inner join inventarios.marcas m on m.id = mo.idmarca "
                        + "                    INNER JOIN tipo_estado te on te.id = mo.estado " + (id > 0 ? " WHERE mo.id = " + id : "");
                break;
            case 5:
                sql = "SELECT p.id, p.descripcion, p.estado, te.descripcion as desestado,"
                        + "      '<button class=''btn btn-glow btn-danger'' title=''Eliminar Presentación'' type=''button'' onclick=' || chr(34) || 'EliminarPresentacion(' || p.id || ',''' || p.descripcion || ''')' || chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + "  FROM inventarios.presentacion p INNER JOIN tipo_estado te on te.id = p.estado " + (id > 0 ? " WHERE p.id = " + id : "");
                break;
            case 6:
                sql = "SELECT c.id, c.descripcion, c.estado, te.descripcion as desestado,"
                        + "      '<button class=''btn btn-glow btn-danger'' title=''Eliminar Característica'' type=''button'' onclick=' || chr(34) || 'EliminarCaracteristica(' || c.id || ',''' || c.descripcion || ''')' || chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + "  FROM inventarios.caracteristica c INNER JOIN tipo_estado te on te.id = c.estado " + (id > 0 ? " WHERE c.id = " + id : "");
                break;
            case 7:
                sql = "SELECT b.id, b.descripcion, b.estado, responsables, inventarios.obtener_responsables(responsables) AS des_responsables, te.descripcion as desestado, \n"
                        + " '<button class=''btn btn-glow btn-danger'' title=''Eliminar Ubicación  de Depósito'' type=''button'' onclick=' || chr(34) || 'EliminarDeposito(' || b.id || ',''' || b.descripcion || ''')' ||  chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar "
                        + " FROM inventarios.bodega b INNER JOIN tipo_estado te on te.id = b.estado "
                        + (id > 0 ? " where b.id = " + id : "") + " order by b.id";
                break;
        }
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->AnularTraslado");
    }

    public String GuardarGrupos(HttpServletRequest request) {
        int Gru_Id = Globales.Validarintnonull(request.getParameter("Gru_Id"));
        String Gru_Descripcion = request.getParameter("Gru_Descripcion").trim().toUpperCase();
        int Gru_Estado = Globales.Validarintnonull(request.getParameter("Gru_Estado"));
        int Gru_CuentaContable = Globales.Validarintnonull(request.getParameter("Gru_CuentaContable"));
        String Gru_Inicial = request.getParameter("Gru_Inicial");

        String Mensaje = "";
        try {

            int id = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from inventarios.grupos where descripcion = '" + Gru_Descripcion + "' and id <> " + Gru_Id));
            if (id > 0) {
                return "1|La descripción <b>" + Gru_Descripcion + "</b> ya se encuentra registrada como grupo";
            }

            if (Gru_Id == 0) {
                if (Globales.PermisosSistemas("GRUPO-INVENTARIO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO inventarios.grupos(descripcion, estado, abreviatura, idcuecontable) "
                        + "VALUES ('" + Gru_Descripcion.trim() + "'," + Gru_Estado + ",'" + Gru_Inicial + "'," + Gru_CuentaContable + ")";

                if (Globales.DatosAuditoria(sql, "GRUPO-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarGrupos")) {
                    Mensaje = "0|Grupo agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                if (Globales.PermisosSistemas("GRUPO-INVENTARIO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE inventarios.grupos "
                        + " SET descripcion='" + Gru_Descripcion.trim() + "', estado=" + Gru_Estado + ",idcuecontable=" + Gru_CuentaContable
                        + ", abreviatura='" + Gru_Inicial + "' WHERE id=" + Gru_Id;

                if (Globales.DatosAuditoria(sql, "GRUPO-INVENTARIO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarGrupos")) {
                    Mensaje = "0|Grupo actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarEquipos(HttpServletRequest request) {
        int Equ_Grupo = Globales.Validarintnonull(request.getParameter("Equ_Grupo"));
        int Equ_Id = Globales.Validarintnonull(request.getParameter("Equ_Id"));
        String Equ_Descripcion = request.getParameter("Equ_Descripcion");
        int Equ_Estado = Globales.Validarintnonull(request.getParameter("Equ_Estado"));
        String Mensaje = "";
        try {

            int id = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from inventarios.equipos where descripcion = '" + Equ_Descripcion + "' and id <> " + Equ_Id + " and idgrupo=" + Equ_Grupo));
            if (id > 0) {
                return "1|La descripción <b>" + Equ_Descripcion + "</b> ya se encuentra registrada como equipo";
            }

            if (Equ_Id == 0) {
                if (Globales.PermisosSistemas("EQUIPOS-INVENTARIO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO inventarios.equipos(descripcion, estado, idgrupo)"
                        + "VALUES ('" + Equ_Descripcion.trim() + "'," + Equ_Estado + "," + Equ_Grupo + ")";

                if (Globales.DatosAuditoria(sql, "EQUIPOS-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarEquipos")) {
                    Mensaje = "0|Equipo agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                if (Globales.PermisosSistemas("EQUIPOS-INVENTARIO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE inventarios.equipos"
                        + "SET descripcion='" + Equ_Descripcion.trim() + "', estado=" + Equ_Estado + ",idgrupo=" + Equ_Grupo
                        + "WHERE id=" + Equ_Id;

                if (Globales.DatosAuditoria(sql, "EQUIPOS-INVENTARIO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarEquipos")) {
                    Mensaje = "0|Equipo actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarEquipos(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int IdReemplazo = Globales.Validarintnonull(request.getParameter("IdReemplazo"));
        String Mensaje = "";
        try {
            if (Globales.PermisosSistemas("EQUIPOS-INVENTARIO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM inventarios.equipos WHERE id = " + Id;

            if (Globales.DatosAuditoria(sql, "EQUIPOS-INVENTARIO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->GuardarEquipos")) {
                Mensaje = "0|Equipo eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

            return Mensaje;
        } catch (Exception ex) {
            if (IdReemplazo > 0) {
                try {
                    sql = "UPDATE inventarios.articulos SET idequipo = " + IdReemplazo + " WHERE idequipo = " + Id + ";";
                    sql += "DELETE FROM inventarios.equipos WHERE id = " + Id + ";";

                    if (Globales.DatosAuditoria(sql, "EQUIPOS-INVENTARIO", "REEMPLAZAR Y ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarEquipos")) {
                        Mensaje = "0|Equipo reemplazado y eliminado con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }

                    return Mensaje;
                } catch (Exception ex2) {
                    return "1|" + ex2.getMessage();
                }

            }
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarGrupo(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int IdReemplazo = Globales.Validarintnonull(request.getParameter("IdReemplazo"));
        String Mensaje = "";
        try {
            if (Globales.PermisosSistemas("GRUPOS-INVENTARIO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM inventarios.grupos WHERE id = " + Id;

            if (Globales.DatosAuditoria(sql, "GRUPOS-INVENTARIO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->GuardarEquipos")) {
                Mensaje = "0|Grupo eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

            return Mensaje;
        } catch (Exception ex) {
            if (IdReemplazo > 0) {
                try {
                    sql = "UPDATE inventarios.equipos SET idgrupo = " + IdReemplazo + " WHERE idequipo = " + Id + ";";
                    sql += "DELETE FROM inventarios.grupos WHERE id = " + Id + ";";

                    if (Globales.DatosAuditoria(sql, "GRUPOS-INVENTARIO", "REEMPLAZAR Y ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarEquipos")) {
                        Mensaje = "0|Grupo reemplazado y eliminado con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }

                    return Mensaje;
                } catch (Exception ex2) {
                    return "1|" + ex2.getMessage();
                }

            }
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarMarcas(HttpServletRequest request) {
        int Mar_Id = Globales.Validarintnonull(request.getParameter("Mar_Id"));
        String Mar_Descripcion = request.getParameter("Mar_Descripcion").trim().toUpperCase();
        int Mar_Estado = Globales.Validarintnonull(request.getParameter("Mar_Estado"));

        String Mensaje = "";
        try {

            int id = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from inventarios.marcas where descripcion = '" + Mar_Descripcion + "' and id <> " + Mar_Id));
            if (id > 0) {
                return "1|La descripción <b>" + Mar_Descripcion + "</b> ya se encuentra registrada como marca";
            }

            if (Mar_Id == 0) {
                if (Globales.PermisosSistemas("MARCA-INVENTARIO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO inventarios.marcas(descripcion, estado) "
                        + "VALUES ('" + Mar_Descripcion + "'," + Mar_Estado + ")";

                if (Globales.DatosAuditoria(sql, "MARCA-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarMarcas")) {
                    Mensaje = "0|Marca agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                if (Globales.PermisosSistemas("MARCA-INVENTARIO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE inventarios.marcas "
                        + "SET descripcion='" + Mar_Descripcion + "', estado=" + Mar_Estado
                        + " WHERE id=" + Mar_Id;

                if (Globales.DatosAuditoria(sql, "MARCA-INVENTARIO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarMarcas")) {
                    Mensaje = "0|Marca actualizada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarPresentacion(HttpServletRequest request) {
        int Pre_Id = Globales.Validarintnonull(request.getParameter("Pre_Id"));
        String Pre_Descripcion = request.getParameter("Pre_Descripcion").trim().toUpperCase();
        int Pre_Estado = Globales.Validarintnonull(request.getParameter("Pre_Estado"));

        String Mensaje = "";
        try {

            int id = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from inventarios.presentacion where descripcion = '" + Pre_Descripcion + "' and id <> " + Pre_Id));
            if (id > 0) {
                return "1|La descripción <b>" + Pre_Descripcion + "</b> ya se encuentra registrada como presentación";
            }

            if (Pre_Id == 0) {
                if (Globales.PermisosSistemas("PRESENTACION-INVENTARIO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO inventarios.presentacion(descripcion, estado) "
                        + "VALUES ('" + Pre_Descripcion + "'," + Pre_Estado + ")";

                if (Globales.DatosAuditoria(sql, "PRESENTACION-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarPresentacion")) {
                    Mensaje = "0|Presentación agregada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                if (Globales.PermisosSistemas("PRESENTACION-INVENTARIO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE inventarios.presentacion "
                        + " SET descripcion='" + Pre_Descripcion + "', estado=" + Pre_Estado
                        + " WHERE id=" + Pre_Id;

                if (Globales.DatosAuditoria(sql, "PRESENTACION-INVENTARIO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarPresentacion")) {
                    Mensaje = "0|Presentación actualizada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarCaracteristica(HttpServletRequest request) {
        int Car_Id = Globales.Validarintnonull(request.getParameter("Car_Id"));
        String Car_Descripcion = request.getParameter("Car_Descripcion").trim().toUpperCase();
        int Car_Estado = Globales.Validarintnonull(request.getParameter("Car_Estado"));

        String Mensaje = "";
        try {

            int id = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from inventarios.caracteristica where descripcion = '" + Car_Descripcion + "' and id <> " + Car_Id));
            if (id > 0) {
                return "1|La descripción <b>" + Car_Descripcion + "</b> ya se encuentra registrada como característica";
            }

            if (Car_Id == 0) {
                if (Globales.PermisosSistemas("CARACTERISTICA-INVENTARIO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO inventarios.caracteristica(descripcion, estado) "
                        + "VALUES ('" + Car_Descripcion + "'," + Car_Estado + ")";

                if (Globales.DatosAuditoria(sql, "CARACTERISTICA-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarMarcas")) {
                    Mensaje = "0|Característica agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                if (Globales.PermisosSistemas("CARACTERISTICA-INVENTARIO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE inventarios.caracteristica "
                        + "SET descripcion='" + Car_Descripcion + "', estado=" + Car_Estado
                        + " WHERE id=" + Car_Id;

                if (Globales.DatosAuditoria(sql, "CARACTERISTICA-INVENTARIO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarMarcas")) {
                    Mensaje = "0|Característica actualizada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarMarcas(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int IdReemplazo = Globales.Validarintnonull(request.getParameter("IdReemplazo"));

        String Mensaje = "";
        try {
            if (Globales.PermisosSistemas("MARCA-INVENTARIO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM inventarios.marcas WHERE id = " + Id;

            if (Globales.DatosAuditoria(sql, "MARCA-INVENTARIO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarMarcas")) {
                Mensaje = "0|Marca eliminada con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

            return Mensaje;
        } catch (Exception ex) {
            if (IdReemplazo > 0) {
                try {
                    sql = "UPDATE inventarios.modelos SET idmarca = " + IdReemplazo + " WHERE idmarca = " + Id + ";";
                    sql += "DELETE FROM inventarios.marcas WHERE id = " + Id + ";";

                    if (Globales.DatosAuditoria(sql, "MARCA", "REEMPLAZAR Y ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarMarcas")) {
                        Mensaje = "0|Marca reemplazada y eliminada con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }

                    return Mensaje;
                } catch (Exception ex2) {
                    return "1|" + ex2.getMessage();
                }
            }
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarCaracteristica(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int IdReemplazo = Globales.Validarintnonull(request.getParameter("IdReemplazo"));

        String Mensaje = "";
        try {
            if (Globales.PermisosSistemas("CARACTERISTICA-INVENTARIO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM inventarios.caracteristica WHERE id = " + Id;

            if (Globales.DatosAuditoria(sql, "CARACTERISTICA-INVENTARIO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarCaracteristica")) {
                Mensaje = "0|Característica eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

            return Mensaje;
        } catch (Exception ex) {
            if (IdReemplazo > 0) {
                try {
                    sql = "UPDATE inventarios.articulos SET idmarca = " + IdReemplazo + " WHERE idcaracteristica = " + Id + ";";
                    sql += "DELETE FROM inventarios.caracteristica WHERE id = " + Id + ";";

                    if (Globales.DatosAuditoria(sql, "CARACTERISTICA", "REEMPLAZAR Y ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarMarcas")) {
                        Mensaje = "0|Característica reemplazada y eliminada con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }

                    return Mensaje;
                } catch (Exception ex2) {
                    return "1|" + ex2.getMessage();
                }
            }
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarPresentacion(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int IdReemplazo = Globales.Validarintnonull(request.getParameter("IdReemplazo"));

        String Mensaje = "";
        try {
            if (Globales.PermisosSistemas("PRESENTACION-INVENTARIO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM inventarios.presentacion WHERE id = " + Id;

            if (Globales.DatosAuditoria(sql, "PRESENTACION-INVENTARIO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarPresentacion")) {
                Mensaje = "0|Intervalo eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

            return Mensaje;
        } catch (Exception ex) {
            if (IdReemplazo > 0) {
                try {
                    sql = "UPDATE inventarios.articulos SET idpresentacion = " + IdReemplazo + " WHERE idpresentacion = " + Id + ";";
                    sql += "DELETE FROM inventarios.marcas WHERE id = " + Id + ";";

                    if (Globales.DatosAuditoria(sql, "PRESENTACION", "REEMPLAZAR Y ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarPresentacion")) {
                        Mensaje = "0|Presentación reemplazada y eliminada con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }

                    return Mensaje;
                } catch (Exception ex2) {
                    return "1|" + ex2.getMessage();
                }
            }
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarModelos(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int IdReemplazo = Globales.Validarintnonull(request.getParameter("IdReemplazo"));
        String Mensaje = "";
        try {
            if (Globales.PermisosSistemas("MODELO-INVENTARIO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM inventarios.modelos WHERE id = " + Id;
            if (Globales.DatosAuditoria(sql, "MODELO-INVENTARIO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarModelos")) {
                Mensaje = "0|Modelo eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

            return Mensaje;
        } catch (Exception ex) {
            if (IdReemplazo > 0) {
                try {
                    sql = "UPDATE inventarios.articulos SET idmodelo = " + IdReemplazo + " WHERE idmodelo = " + Id + ";";
                    sql += "DELETE FROM inventarios.modelos WHERE id = " + Id + ";";
                    if (Globales.DatosAuditoria(sql, "MODELO-INVENTARIO", "REEMPLAZAR Y EDITAR", idusuario, iplocal, this.getClass() + "-->EliminarModelos")) {
                        Mensaje = "0|Modelo reemplazado y eliminado con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }
                    return Mensaje;
                } catch (Exception ex2) {
                    return "1|" + ex2.getMessage();
                }

            }
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarModelos(HttpServletRequest request) {
        int Mod_Id = Globales.Validarintnonull(request.getParameter("Mod_Idn"));
        String Mod_Descripcion = request.getParameter("Mod_Descripcion").trim().toUpperCase();
        int Mod_Marca = Globales.Validarintnonull(request.getParameter("Mod_Marcan"));
        int Mod_Estado = Globales.Validarintnonull(request.getParameter("Mod_Estadon"));
        String Mensaje = "";
        try {

            int id = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from inventarios.modelos where descripcion = '" + Mod_Descripcion + "' and id <> " + Mod_Id + " and idmarca = " + Mod_Marca));
            if (id > 0) {
                return "1|La descripción <b>" + Mod_Descripcion + "</b> ya se encuentra registrada como modelo";
            }

            if (Mod_Id == 0) {
                if (Globales.PermisosSistemas("MODELO-INVENTARIO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO inventarios.modelos(idmarca, descripcion, estado) "
                        + "VALUES (" + Mod_Marca + ",'" + Mod_Descripcion + "'," + Mod_Estado + ")";
                if (Globales.DatosAuditoria(sql, "MODELO-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarModelos")) {
                    Mensaje = "0|Modelo agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("MODELO-INVENTARIO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE inventarios.modelos "
                        + "SET idmarca=" + Mod_Marca + ",descripcion='" + Mod_Descripcion + "', estado=" + Mod_Estado
                        + " WHERE id=" + Mod_Id;

                if (Globales.DatosAuditoria(sql, "MODELO-INVENTARIO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarModelos")) {
                    Mensaje = "0|Modelo actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }
            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GrupoEquipo(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        sql = "SELECT idgrupo FROM inventarios.equipos WHERE id=" + id;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "--> GrupoEquipo");
    }

    public String PDFArticulos(HttpServletRequest request) {
        int articulo = Globales.Validarintnonull(request.getParameter("articulo"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        sql = "SELECT case when " + tipo + " = 1 then fichatecnica else manual_usuario end as manual FROM inventarios.articulos WHERE id=" + articulo;
        return Globales.ObtenerUnValor(sql);
    }

    public String GuardarDeposito(HttpServletRequest request) {
        int Dep_Id = Globales.Validarintnonull(request.getParameter("Dep_Id"));
        String[] Dep_Responsables = request.getParameterValues("Dep_Responsables");
        String Dep_Ubicacion = request.getParameter("Dep_Ubicacion").trim();
        int Dep_Estado = Globales.Validarintnonull(request.getParameter("Dep_Estado"));

        try {

            int id = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from inventarios.bodega where descripcion = '" + Dep_Ubicacion + "' and id <> " + Dep_Id));
            if (id > 0) {
                return "1|La descripción <b>" + Dep_Ubicacion + "</b> ya se encuentra registrada como ubicación de depósito";
            }

            String a_responsable = "";

            if (Dep_Responsables != null) {
                for (int x = 0; x < Dep_Responsables.length; x++) {
                    if (!a_responsable.equals("")) {
                        a_responsable += ",";
                    }
                    a_responsable += Dep_Responsables[x];
                }
            }

            if (Dep_Id == 0) {
                if (Globales.PermisosSistemas("UBICACION DE DEPOSITO AGREGAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO inventarios.bodega(descripcion, responsables, estado) "
                        + " VALUES ('" + Dep_Ubicacion + "','" + a_responsable + "'," + Dep_Estado + ")";
                if (Globales.DatosAuditoria(sql, "UBICACION DE DEPOSITO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarDeposito")) {
                    return "0|Ubicación de deposito agregada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("UBICACION DE DEPOSITO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE inventarios.bodega "
                        + " SET descripcion = '" + Dep_Ubicacion + "', responsables='" + a_responsable + "'"
                        + ", estado=" + Dep_Estado
                        + " WHERE id=" + Dep_Id;
                if (Globales.DatosAuditoria(sql, "UBICACION DE DEPOSITO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarDeposito")) {
                    return "0|Ubicación de deposito actualizada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarDeposito(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));

        try {
            if (Globales.PermisosSistemas("UBICACION DE DEPOSITO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM inventarios.bodega WHERE id = " + Id;
            if (Globales.DatosAuditoria(sql, "UBICACION DE DEPOSITO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarDeposito")) {
                return "0|Ubicación de deposito eliminada con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarFoto(HttpServletRequest request) {

        int Articulo = Globales.Validarintnonull(request.getParameter("Articulo"));
        int numero = Globales.Validarintnonull(request.getParameter("numero"));

        if (Globales.PermisosSistemas("FOTO ARTICULO ELIMINAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        try {
            if (numero != 5) {
                File archivo = new File(Globales.ruta_archivo + "Adjunto/imagenes/Articulos/" + Articulo + "/" + (numero + 1) + ".jpg");
                if (archivo.exists()) {
                    return "1|Debe de eliminar primero la foto número " + (numero + 1);
                }
            }
            File archivo = new File(Globales.ruta_archivo + "Adjunto/imagenes/Articulos/" + Articulo + "/" + numero + "_copia.jpg");
            if (archivo.exists()) {
                archivo.delete();
            }
            archivo = new File(Globales.ruta_archivo + "Adjunto/imagenes/Articulos/" + Articulo + "/" + numero + ".jpg");
            if (archivo.exists()) {
                archivo.delete();
            } else {
                return "1|No existe la foto número " + numero + " de este artículo";
            }

            if (numero == 1) {
                sql = "UPDATE inventarios.articulos SET foto1 = 0 WHERE id = " + Articulo;
                Globales.DatosAuditoria(sql, "FOTO ARTICULO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->SubirFoto");
            }

            Globales.GuardarAuditoria("ELIMINADO FOTO NÚMERO " + numero + " DEL ARTICULO " + Articulo, "FOTO ARTICULO", "ELIMINAR", idusuario, iplocal);
            return "0|Foto Eliminada";
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarArticuloDependencia(HttpServletRequest request) {

        int id = Globales.Validarintnonull(request.getParameter("id"));
        int articulo = Globales.Validarintnonull(request.getParameter("articulo"));
        int dependencia = Globales.Validarintnonull(request.getParameter("dependencia"));
        int cantidad = Globales.Validarintnonull(request.getParameter("cantidad"));
        double precio = Globales.ValidardoublenoNull(request.getParameter("precio"));
        double preciousd = Globales.ValidardoublenoNull(request.getParameter("preciousd"));

        try {

            sql = "SELECT id from inventarios.articulos_dependencia WHERE idarticulo = '" + articulo + "' and iddependencia = " + dependencia + " and id <> " + id;
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (encontrado > 0) {
                return "1|Esta estructura de costo ya se encuentra asignada a este artículo";
            }

            if (id == 0) {
                if (Globales.PermisosSistemas("INVENTARIO-DEPENDENCIA AGREGAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO inventarios.articulos_dependencia(idarticulo, iddependencia, cantidad, precio, preciousd, idusuario) "
                        + "   VALUES(" + articulo + "," + dependencia + "," + cantidad + "," + precio + "," + preciousd + "," + idusuario + ");";
                if (Globales.DatosAuditoria(sql, "INVENTARIO-DEPENDENCIA", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarArticuloDependencia")) {
                    return "0|Registro agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {

                if (Globales.PermisosSistemas("INVENTARIO-DEPENDENCIA EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql
                        = "UPDATE inventarios.articulos_dependencia "
                        + " SET cantidad =" + cantidad + ", precio=" + precio + "  ,preciousd=" + preciousd + ",idusuario=" + idusuario + ",fecha=now() "
                        + " WHERE id= " + id;

                if (Globales.DatosAuditoria(sql, "INVENTARIO-DEPENDENCIA", "ACTUALIZAR", idusuario, iplocal, this.getClass() + "-->GuardarArticuloDependencia")) {
                    return "0|Registro actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarArticuloDependencia(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));

        String Mensaje = "";
        try {
            if (Globales.PermisosSistemas("INVENTARIO-DEPENDENCIA ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM inventarios.articulos_dependencia WHERE id = " + Id;

            if (Globales.DatosAuditoria(sql, "INVENTARIO-DEPENDENCIA", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarArticuloDependencia")) {
                Mensaje = "0|Registro eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String ConsultarArticuloDependencia(HttpServletRequest request) {
        int articulo = Globales.Validarintnonull(request.getParameter("articulo"));

        sql = "SELECT a.id as idarticulo, ad.id, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, \n"
                + "'<b>TIPO:</b> ' || a.tipo || ', <b>GRUPO:</b> ' || g.descripcion || ', <b>ARTICULO:</b> ' || e.descripcion || \n"
                + "', <b>MARCA:</b> ' || ma.descripcion ||  ', <b>MODELO:</b> ' || mo.descripcion || ', <b>PRESENTACION:</b> ' || pr.descripcion || \n"
                + "', <b>CARACTERISTICA:</b> ' || ca.descripcion as articulo, precio, preciousd, ad.cantidad, \n"
                + " '<label class=''label ' || case when a.estado = 1 then 'label-success' else 'label-danger' end || '''>' || te.descripcion || '</label>' as des_estado, \n"
                + "     case when a.foto1 > 0 then '<img src=''nullAdjunto/imagenes/Articulos/' || a.id || '/1.jpg'' width=''80px''/><br>' else '' end as imagen,   \n"
                + "     to_char(ad.fecha,'yyyy/MM/dd HH24:MI') || '<br>' || u.nombrecompleto as registro, precio*ad.cantidad as subtotal, 	preciousd*ad.cantidad as subtotalusd, \n"
                + "      '<button class=''btn btn-glow btn-danger'' title=''Eliminar Estructura de Costo'' type=''button'' onclick=' || chr(34) || 'EliminarDependencia(' || ad.id || ')' || chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar \n"
                + " FROM inventarios.articulos_dependencia ad inner join  inventarios.articulos a on a.id = ad.iddependencia\n"
                + "			inner join inventarios.equipos e on a.idequipo = e.id  \n"
                + "                        inner join inventarios.grupos g on g.id = e.idgrupo \n"
                + "                        inner join inventarios.modelos mo on mo.id = a.idmodelo \n"
                + "                        inner join inventarios.marcas ma on mo.idmarca = ma.id \n"
                + "                        inner join inventarios.presentacion pr on pr.id = a.idpresentacion \n"
                + "                        inner join inventarios.caracteristica ca on ca.id = a.idcaracteristica \n"
                + "                        inner join iva i on a.idiva = i.id \n"
                + "                        inner join proveedores p on a.idproveedor = p.id \n"
                + "                        inner join seguridad.rbac_usuario u on u.idusu = ad.idusuario \n"
                + "                        inner join tipo_estado te on a.estado = te.id WHERE ad.idarticulo = " + articulo + " ORDER BY ad.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarArticuloDependencia");
    }

    public int BuscarArticuloDependencia(HttpServletRequest request) {

        int dependencia = Globales.Validarintnonull(request.getParameter("dependencia"));
        int articulo = Globales.Validarintnonull(request.getParameter("articulo"));

        sql = "SELECT id from inventarios.articulos_dependencia WHERE idarticulo = '" + articulo + "' and iddependencia = " + dependencia;
        return Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
    }

    public String ConsultarTablaPrecio(HttpServletRequest request) {

        String codigointerno = request.getParameter("codigointerno");
        String codigobarra = request.getParameter("codigobarra");
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        String tipo = request.getParameter("tipo");
        String estado = request.getParameter("estado");
        String referencia = request.getParameter("referencia");
        String uso_interno = request.getParameter("uso_interno");
        String catalogo = request.getParameter("catalogo");
        int iva = Globales.Validarintnonull(request.getParameter("iva"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        int presentacion = Globales.Validarintnonull(request.getParameter("presentacion"));
        int caracteristica = Globales.Validarintnonull(request.getParameter("caracteristica"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));

        String busqueda = "WHERE 1=1 ";
        if (!codigointerno.equals("")) {
            busqueda += " AND a.codigointerno  = '" + codigointerno + "'";
        }
        if (!codigobarra.equals("")) {
            busqueda += " AND a.codigobarras  = '" + codigobarra + "'";
        }
        if (!estado.equals("Todos")) {
            busqueda += " AND a.tipo  = '" + tipo + "'";
        }
        if (!estado.equals("Todos")) {
            busqueda += " AND a.tipo  = '" + tipo + "'";
        }
        if (!referencia.equals("")) {
            busqueda += " AND a.referencia  = '" + referencia + "'";
        }
        if (!uso_interno.equals("")) {
            busqueda += " AND a.uso_interno  = '" + uso_interno + "'";
        }
        if (!catalogo.equals("")) {
            busqueda += " AND a.catalogo  = '" + catalogo + "'";
        }
        if (iva > 0) {
            busqueda += " AND a.idiva = " + iva;
        }
        if (grupo > 0) {
            busqueda += " AND e.idgrupo = " + grupo;
        }
        if (equipo > 0) {
            busqueda += " AND a.idequipo = " + equipo;
        }
        if (marca > 0) {
            busqueda += " AND mo.idmarca = " + marca;
        }
        if (!modelo.equals("")) {
            busqueda += " AND mo.descripcion = '" + modelo + "'";
        }
        if (proveedor > 0) {
            busqueda += " AND a.idproveedor = " + proveedor;
        }
        if (presentacion > 0) {
            busqueda += " AND a.idpresentacion= " + presentacion;
        }
        if (caracteristica > 0) {
            busqueda += " AND a.idcaracteristica = " + proveedor;
        }

        sql = "SELECT a.id, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, codigobarras, a.tipo, g.descripcion as grupo, \n"
                + "  e.descripcion as equipo, te.descripcion as estado, ma.descripcion as marca, mo.descripcion as modelo, pr.descripcion as presentacion, \n"
                + "  ca.descripcion as caracteristica, existencia,p.nombrecompleto as proveedor,ultimocosto, uso_interno, ultimocostousd,\n"
                + "      i.descripcion as iva, a.estado, catalogo,referencia, garantia_mes, iusd.descripcion as ivausd,\n"
                + "      precio1,precio2,precio3,precio4,precio5,precio6,precio7,precio8,precio9,precio10,\n"
                + "      preciousd1,preciousd2,preciousd3,preciousd4,preciousd5,preciousd6,preciousd7,preciousd8,preciousd9,preciousd10,\n"
                + "     case when a.foto1 > 0 then '<img src=''nullAdjunto/imagenes/Articulos/' || a.id || '/1.jpg'' width=''80px''/><br>' else '' end as imagen   FROM inventarios.articulos a inner join inventarios.equipos e on a.idequipo = e.id  \n"
                + "                        inner join inventarios.grupos g on g.id = e.idgrupo \n"
                + "                        inner join inventarios.modelos mo on mo.id = a.idmodelo \n"
                + "                        inner join inventarios.marcas ma on mo.idmarca = ma.id \n"
                + "                        inner join inventarios.presentacion pr on pr.id = a.idpresentacion \n"
                + "                        inner join inventarios.caracteristica ca on ca.id = a.idcaracteristica \n"
                + "                        inner join inventarios.articulos_precios pre on pre.idarticulo = a.id\n"
                + "                        inner join iva i on a.idiva = i.id \n"
                + "                        inner join iva iusd on a.idivausd = iusd.id \n"
                + "                        inner join proveedores p on a.idproveedor = p.id \n"
                + "                        inner join tipo_estado te on a.estado = te.id " + busqueda + " ORDER BY a.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarArticulos");
    }

    //FIN DEL CODIGO
    protected void Servidor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String ruta_origen = request.getHeader("Origin");

        HttpSession misession = request.getSession();
        idusuario = misession.getAttribute("idusuario").toString();
        iplocal = misession.getAttribute("IPLocal").toString();
        usuario = misession.getAttribute("codusu").toString();
        if (misession.getAttribute("ArchivosAdjuntos") != null) {
            archivoadjunto = misession.getAttribute("ArchivosAdjuntos").toString();
        }
        if (misession.getAttribute("ArchivosAdjuntos2") != null) {
            archivoadjunto2 = misession.getAttribute("ArchivosAdjuntos2").toString();
        }
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");

        try (PrintWriter out = response.getWriter()) {
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else if (misession.getAttribute("codusu") == null) {
                out.print("cerrada");
            } else if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                out.print("cambio-ip");
            } else {
                String opcion = request.getParameter("opcion");
                switch (opcion) {

                    //WILMER
                    case "GuardarOpcion":
                        out.print(GuardarOpcion(request));
                        break;
                    case "CuentaContable_Grupo":
                        out.print(CuentaContable_Grupo(request));
                        break;
                    case "BuscarArticulos":
                        out.print(BuscarArticulos(request));
                        break;
                    case "ConsultarArticulos":
                        out.print(ConsultarArticulos(request));
                        break;
                    case "ValorIva":
                        out.print(ValorIva(request));
                        break;
                    case "CodigoInterno":
                        out.print(CodigoInterno(request));
                        break;
                    case "GuardarArticulos":
                        out.print(GuardarArticulos(request));
                        break;
                    case "ModalArticulos":
                        out.print(ModalArticulos(request));
                        break;
                    case "CodigoTomaFisica":
                        out.print(CodigoTomaFisica(request));
                        break;
                    case "BuscarArticulosToma":
                        out.print(BuscarArticulosToma(request));
                        break;
                    case "GuardarToma":
                        out.print(GuardarToma(request));
                        break;
                    case "TablaTomaFisica":
                        out.print(TablaTomaFisica(request));
                        break;
                    case "InicializarToma":
                        out.print(InicializarToma(request));
                        break;
                    case "EliminarToma":
                        out.print(EliminarToma(request));
                        break;
                    case "ConsultaTomaExcel":
                        out.print(ConsultaTomaExcel(request));
                        break;
                    /*
                            case "ImportarArchivo":
                                out.print(ImportarArchivo(request));
                                break;
                     */
                    case "CerrarTomaFisica":
                        out.print(CerrarTomaFisica(request));
                        break;
                    case "ConsultaAuditoriaToma":
                        out.print(ConsultaAuditoriaToma(request));
                        break;
                    case "TablaTraslado":
                        out.print(TablaTraslado(request));
                        break;
                    case "GuardarTrasladoDetalle":
                        out.print(GuardarTrasladoDetalle(request));
                        break;
                    case "EliminarTraslado":
                        out.print(EliminarTraslado(request));
                        break;
                    case "InicializarTraslado":
                        out.print(InicializarTraslado(request));
                        break;
                    case "GuardarTraslado":
                        out.print(GuardarTraslado(request));
                        break;
                    case "AnularTraslado":
                        out.print(AnularTraslado(request));
                        break;
                    case "TablaConfiguracion":
                        out.print(TablaConfiguracion(request));
                        break;
                    case "GuardarGrupos":
                        out.print(GuardarGrupos(request));
                        break;
                    case "GuardarEquipos":
                        out.print(GuardarEquipos(request));
                        break;
                    case "EliminarEquipos":
                        out.print(EliminarEquipos(request));
                        break;
                    case "GuardarMarcas":
                        out.print(GuardarMarcas(request));
                        break;
                    case "EliminarMarcas":
                        out.print(EliminarMarcas(request));
                        break;
                    case "EliminarModelos":
                        out.print(EliminarModelos(request));
                        break;
                    case "GuardarModelos":
                        out.print(GuardarModelos(request));
                        break;
                    case "DescripcionPrecio":
                        out.print(DescripcionPrecio());
                        break;
                    case "GrupoEquipo":
                        out.print(GrupoEquipo(request));
                        break;
                    case "PDFArticulos":
                        out.print(PDFArticulos(request));
                        break;
                    case "EliminarGrupo":
                        out.print(EliminarGrupo(request));
                        break;
                    case "EliminarPresentacion":
                        out.print(EliminarPresentacion(request));
                        break;
                    case "EliminarCaracteristica":
                        out.print(EliminarCaracteristica(request));
                        break;
                    case "GuardarCaracteristica":
                        out.print(GuardarCaracteristica(request));
                        break;
                    case "GuardarPresentacion":
                        out.print(GuardarPresentacion(request));
                        break;
                    case "GuardarDeposito":
                        out.print(GuardarDeposito(request));
                        break;
                    case "EliminarDeposito":
                        out.print(EliminarDeposito(request));
                        break;
                    case "EliminarFoto":
                        out.print(EliminarFoto(request));
                        break;
                    case "GuardarArticuloDependencia":
                        out.print(GuardarArticuloDependencia(request));
                        break;
                    case "EliminarArticuloDependencia":
                        out.print(EliminarArticuloDependencia(request));
                        break;
                    case "ConsultarArticuloDependencia":
                        out.print(ConsultarArticuloDependencia(request));
                        break;
                    case "BuscarArticuloDependencia":
                        out.print(BuscarArticuloDependencia(request));
                        break;
                    case "ConsultarTablaPrecio":
                        out.print(ConsultarTablaPrecio(request));
                        break;
                    //FIN
                    default:
                        throw new AssertionError();
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

}
