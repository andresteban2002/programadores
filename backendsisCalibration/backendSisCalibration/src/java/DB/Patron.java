package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Patron"})
public class Patron extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String archivoadjunto = "";

    public String GuardarPatron(HttpServletRequest request) {

        int IdPatron = Globales.Validarintnonull(request.getParameter("IdPatron"));

        String Magnitud = request.getParameter("Magnitud");
        String Medida = request.getParameter("Medida");
        String AlcanceDesde = request.getParameter("AlcanceDesde");
        String AlcanceHasta = request.getParameter("AlcanceHasta");
        String Marca = request.getParameter("Marca");
        String Modelo = request.getParameter("Modelo");
        String Serie = request.getParameter("Serie");
        String Certificado = request.getParameter("Certificado");
        String FechaCertificado = request.getParameter("FechaCertificado");
        String ProximaCalibracion = request.getParameter("FechaProxCalibracion");
        String Resolucion = request.getParameter("Resolucion");
        String Laboratorio = request.getParameter("Laboratorio");
        String Transductor = request.getParameter("Transductor");
        String Tolerancia = request.getParameter("Tolerancia");
        String Derivada = request.getParameter("Derivada");
        String Precision = request.getParameter("Precision");
        String Descripcion = request.getParameter("Descripcion");
        String Grado = request.getParameter("Grado");
        String Titulo = request.getParameter("Titulo");
        String Estado = request.getParameter("Estado");

        if (ProximaCalibracion.equals("")) {
            ProximaCalibracion = null;
        } else {
            ProximaCalibracion = "'" + ProximaCalibracion + "'";
        }

        String resultado = "";
        try {

            if (IdPatron == 0) {

                if (Globales.PermisosSistemas("PATRON GUARDAR PATRON", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                sql = "INSERT INTO public.patron(idmagnitud, alcancedesde, alcance, medida, marca, modelo, "
                        + " serie, certificado, fechacertificado, proximacalibracion, resolucion, laboratorio, transductor, tolerancia, derivada, precision, "
                        + "descripcion, grado, titulo, estado) "
                        + " VALUES(" + Magnitud + "," + AlcanceDesde + "," + AlcanceHasta + ",'" + Medida + "','" + Marca + "','" + Modelo + "','"
                        + Serie + "','" + Certificado + "','" + FechaCertificado + "'," + ProximaCalibracion + "," + Resolucion + ",'" + Laboratorio + "'," + Transductor + ","
                        + Tolerancia + "," + Derivada + "," + Precision + ",'" + Descripcion + "'," + Grado + ",'" + Titulo + "','" + Estado + "')";
                if (Globales.DatosAuditoria(sql, "PATRON", "GUARDAR PATRON", idusuario, iplocal, this.getClass() + "-->GuardarPatron")) {
                    sql = "SELECT MAX(id) FROM patron;";
                    IdPatron = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    resultado = "0|Patrón agregado con éxito|" + IdPatron;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {

                if (Globales.PermisosSistemas("PATRON EDITAR PATRON", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                sql = "UPDATE public.patron "
                        + " SET idmagnitud=" + Magnitud + ", alcancedesde=" + AlcanceDesde + ", alcance=" + AlcanceHasta + ", medida='" + Medida + "', "
                        + " marca ='" + Marca + "', modelo='" + Modelo + "', serie='" + Serie + "', certificado='" + Certificado + "', fechacertificado='" + FechaCertificado + "', "
                        + " proximacalibracion=" + ProximaCalibracion + ", resolucion =" + Resolucion + ", laboratorio='" + Laboratorio + "', transductor=" + Transductor + ", "
                        + " tolerancia=" + Tolerancia + ",derivada=" + Derivada + ",precision=" + Precision + ",descripcion='" + Descripcion + "', "
                        + " grado=" + Grado + ",titulo='" + Titulo + "', estado='" + Estado + "'"
                        + " WHERE id= " + IdPatron;

                if (Globales.DatosAuditoria(sql, "PATRON", "GUARDAR PATRON", idusuario, iplocal, "PATRON EDITAR PATRON")) {
                    resultado = "0|Patrón actualizado con éxito|" + IdPatron;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

        return resultado;

    }

    public String GuardarPatronAux(HttpServletRequest request) {

        int IdPatron = Globales.Validarintnonull(request.getParameter("IdPatronAux"));

        String Marca = request.getParameter("MarcaPAux");

        String Modelo = request.getParameter("ModeloPAux").trim();
        String Serie = request.getParameter("SeriePAux").trim();
        String Capacidad = request.getParameter("CapacidadPAux").trim();
        String Descripcion = request.getParameter("DescripcionPAux").trim();
        String Piso = request.getParameter("PisoPAux").trim();
        String DireccionPAux = request.getParameter("DireccionPAux").trim();

        String resultado = "";
        try {

            if (IdPatron == 0) {

                if (Globales.PermisosSistemas("PATRON GUARDAR PATRON AUXILIAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                sql = "INSERT INTO public.intrumento_condamb( marca, modelo, capacidad, "
                        + " serie, "
                        + "descripcion, piso, direccion) "
                        + " VALUES('" + Marca + "','" + Modelo + "','" + Capacidad + "','"
                        + Serie + "','" + Descripcion + "'," + Piso + ",'" + DireccionPAux + "')";
                if (Globales.DatosAuditoria(sql, "PATRON", "GUARDAR PATRON AUXILIAR", idusuario, iplocal, this.getClass() + "-->GuardarPatronAux")) {
                    sql = "SELECT MAX(id) FROM intrumento_condamb;";
                    IdPatron = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    resultado = "0|Patron Auxiliar agregado con éxito|" + IdPatron;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {

                if (Globales.PermisosSistemas("PATRON EDITAR PATRON AUXILIAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                sql = "UPDATE public.intrumento_condamb "
                        + " SET marca ='" + Marca + "', modelo='" + Modelo + "', serie='" + Serie + "', "
                        + " descripcion='" + Descripcion + "', direccion='" + DireccionPAux + "', piso=" + Piso
                        + " WHERE id= " + IdPatron;

                if (Globales.DatosAuditoria(sql, "PATRON", "EDITAR PATRON AUXILIAR", idusuario, iplocal, this.getClass() + "-->GuardarPatronAux")) {
                    resultado = "0|Patron Auxiliar actualizado con éxito|" + IdPatron;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

        return resultado;

    }

    public String ConsultarPatron(HttpServletRequest request) {
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String marca = request.getParameter("marca");
        String modelo = request.getParameter("modelo");
        String descripcion = request.getParameter("descripcion");
        String serie = request.getParameter("serie");
        double grado = Globales.ValidardoublenoNull(request.getParameter("grado"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        String certificado = request.getParameter("certificado");

        String fechaCalibracion = request.getParameter("fechacertificado");
        String fechaDesde = request.getParameter("fechadesde");
        String fechaHasta = request.getParameter("fechahasta");

        String busqueda = "WHERE 1 = 1 ";

        if (magnitud > 0) {
            busqueda += " AND p.idmagnitud = " + magnitud;
        }
        if (!marca.equals("")) {
            busqueda += " AND p.marca = '" + marca + "'";
        }
        if (!modelo.equals("")) {
            busqueda += " AND p.modelo = '" + modelo + "'";
        }
        if (!descripcion.equals("")) {
            busqueda += " AND p.descripcion = '" + descripcion + "'";
        }
        if (!serie.trim().equals("")) {
            busqueda += " AND p.serie = '" + serie.trim() + "'";
        }
        if (grado > 0) {
            busqueda += " AND p.grado = " + grado;
        }
        if (proveedor > 0) {
            busqueda += " AND pro.id = " + proveedor;
        }
        if (!certificado.equals("")) {
            busqueda += " AND p.certificado = '" + certificado + "'";
        }
        if (!fechaCalibracion.equals("")) {
            busqueda += " AND p.fechacertificado = '" + fechaCalibracion + "'";
        }
        if (!fechaDesde.equals("")) {
            if (!fechaHasta.equals("")) {
                busqueda += " and to_char(p.proximacalibracion,'yyyy-MM-dd') >= '" + fechaDesde + "'  and to_char(p.proximacalibracion,'yyyy-MM-dd') <= '" + fechaHasta + "' ";
            }
        }

        sql = "select p.id as patron, p.alcancedesde as alcancedesde, p.alcance as alcancehasta, p.medida as medida, marca, modelo, p.serie as serie, p.certificado as certificado,\n"
                + "	p.fechacertificado as fechacertificado, p.proximacalibracion as proximacalibracion, p.resolucion as resolucion, laboratorio,  p.tolerancia as tolerancia, \n"
                + "	p.derivada as derivada, p.precision as precision, p.descripcion as descripcion, p.grado as grado, p.titulo as titulo, m.descripcion as magnitud, p.estado \n"
                + " from patron p inner join magnitudes m on m.id = p.idmagnitud               \n" + busqueda
                + " ORDER BY p.id desc";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarPatron");
    }

    public String ConsultarPatronAux() {

        sql = "SELECT id, marca, modelo, capacidad, descripcion, \n"
                + "       serie, piso, direccion, (SELECT COUNT(*) from intrumento_condamb_sensores s where idinstrumento=i.id) as sensor  \n"
                + "  FROM intrumento_condamb i ";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarPatronAux");
    }

    public String BuscarPatron(HttpServletRequest request) {
        String patron = request.getParameter("patron");
        sql = "select id, transductor, alcancedesde, alcance as alcancehasta, medida as medida, marca, modelo, serie, certificado,\n"
                + "fechacertificado, proximacalibracion, resolucion, laboratorio,  tolerancia, \n"
                + "derivada, precision, coalesce(descripcion,'') as descripcion, grado, coalesce(titulo,'') as titulo, idmagnitud as magnitud, estado \n"
                + "from patron WHERE id=" + patron;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarPatron");
    }

    public String BuscarPatronAux(HttpServletRequest request) {
        String patron = request.getParameter("patron");
        sql = "SELECT id, marca, modelo, capacidad, descripcion, \n"
                + "  serie, piso, direccion "
                + "  FROM intrumento_condamb where id=" + patron;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarPatronAux");
    }

    public String BuscarSensor(HttpServletRequest request) {
        String sensor = request.getParameter("sensor");
        sql = "SELECT ps.id, paux.descripcion as instrumento, serie, idmagnitud, url, lectura, "
                + "coalesce(certificado,'') as certificado, fecha_calibracion, coalesce(to_char(proxima_fecha,'yyyy-MM-dd'),'') as proxima_fecha "
                + "FROM public.intrumento_condamb_sensores ps \n"
                + "inner join public.instcondamb_patron_aux paux on \n"
                + "(select descripcion from public.intrumento_condamb where id = ps.idinstrumento)=paux.descripcion  where ps.id=" + sensor;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarSensor");
    }

    public String BuscarVM(HttpServletRequest request) {
        String vm = request.getParameter("vm");
        sql = "SELECT id, tipo, medida, valor1, valor2, valor3, idsensor, valor4\n"
                + "  FROM public.instcondamb_medida where id=" + vm;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarVM");
    }

    public String BuscarVC(HttpServletRequest request) {
        String vm = request.getParameter("vc");
        sql = "SELECT id, medida, patron, instrumento, variable, orden, idsensor "
                + "  FROM public.instcondamb_certificado where id=" + vm;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarVC");
    }

    public String ConsultarTransductor(HttpServletRequest request) {
        int idPatron = Globales.Validarintnonull(request.getParameter("idpatron"));
        int Transductor = Globales.Validarintnonull(request.getParameter("transductor"));
        int plantilla = Globales.Validarintnonull(request.getParameter("plantilla"));
        if (idPatron > 0) {
            switch (Transductor) {
                case 1:
                    sql = "select t.id, t.punto, t.lectura, t.error, t.expandida1, t.derivada, "
                            + " t.expandida2, t.bep, t.bemax,  "
                            + "  '<button class=''btn btn-glow btn-primary'' title=''Editar Datos'' type=''button'' onclick=' || chr(34) || 'EditarTranductor(' || t.id || ')' ||chr(34) || '><span data-icon=''&#xe220;''></span></button>"
                            + "<button class=''btn btn-glow btn-danger'' title=''Eliminar Datos'' type=''button'' onclick=' || chr(34) || 'EliminarTranductor(' || t.id || ',' || t.punto || ')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar \n"
                            + " from patron_transductor1 t "
                            + " where idpatron=" + idPatron + " and plantilla=" + plantilla + " order by t.punto";
                    break;
                case 2:
                    sql = "select id, tipo, punto, lectura, error_med, total, derivada, error_estina, "
                            + "error_reci, expandida, utotal from patron_transductor2 where idpatron=" + idPatron;
                    break;
                case 3:
                    sql = "select id, lecturapatron, lecturaibc, erromedicion, uexpandida, "
                            + "derivada, erroranterior, errorestimado, errorresidual, utotal from patron_transductor3 where idpatron=" + idPatron;
                    break;
                case 4:
                    sql = "select id, valornominal, desviacionlongitud, variacionlongitud, "
                            + "upatron, derivada, denominadorderivada from patron_transductor4 where idpatron=" + idPatron;
                    break;
                case 5:
                    sql = "select id, sp, error, u from patron_transductor5 where idpatron=" + idPatron;
                    break;
            }

            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarTransductor");
        } else {
            return "[]";
        }

    }

    public String BuscarTransductor(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int Transductor = Globales.Validarintnonull(request.getParameter("transductor"));
        if (id > 0) {
            switch (Transductor) {
                case 1:
                    sql = "select *  "
                            + " from patron_transductor1  "
                            + " where id=" + id;
                    break;
            }
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarTransductor");
        } else {
            return "[]";
        }

    }

    public String GuardarTransductor(HttpServletRequest request) {

        int IdPatron = Globales.Validarintnonull(request.getParameter("IdPatron"));

        int Transductor = Globales.Validarintnonull(request.getParameter("Transductor"));
        int IdTransductor = Globales.Validarintnonull(request.getParameter("IdTransductor"));

        double Punto = Globales.ValidardoublenoNull(request.getParameter("Punto"));
        double Lectura = Globales.ValidardoublenoNull(request.getParameter("Lectura"));
        double Error = Globales.ValidardoublenoNull(request.getParameter("Error"));
        double DerivadaT = Globales.ValidardoublenoNull(request.getParameter("Derivada1"));
        double Expandida1 = Globales.ValidardoublenoNull(request.getParameter("Expandida1"));
        double Expandida2 = Globales.ValidardoublenoNull(request.getParameter("Expandida2"));
        double BEP = Globales.ValidardoublenoNull(request.getParameter("BEP"));
        double BEMAX = Globales.ValidardoublenoNull(request.getParameter("BEMAX"));
        int Plantilla = Globales.Validarintnonull(request.getParameter("Plantilla"));

        if (Globales.PermisosSistemas("PATRON ACTUALIZAR TRANSDUCTOR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        String resultado = "";
        try {
            switch (Transductor) {
                case 1:
                    if (IdTransductor == 0) {
                        sql = "INSERT INTO public.patron_transductor1(idpatron, punto, lectura, error, expandida1, "
                                + " derivada, expandida2, bep, bemax, plantilla) "
                                + " VALUES(" + IdPatron + "," + Punto + "," + Lectura + "," + Error + "," + Expandida1 + ","
                                + DerivadaT + "," + Expandida2 + "," + BEP + "," + BEMAX + "," + Plantilla + ")";
                    } else {
                        sql = "UPDATE public.patron_transductor1 \n"
                                + "   SET punto=" + Punto + ", lectura=" + Lectura + ",error=" + Error + ",expandida1=" + Expandida1 + ", \n"
                                + "       derivada=" + DerivadaT + ", expandida2=" + Expandida2 + ", bep=" + BEP + ", bemax=" + BEMAX + ", plantilla=" + Plantilla + "\n"
                                + " WHERE id=" + IdTransductor;
                    }
                    if (Globales.DatosAuditoria(sql, "PATRON", "ACTUALIZAR TRANSDUCTOR", idusuario, iplocal, this.getClass() + "->GuardarTransductor")) {
                        resultado = "0|Datos actualizados con éxito";
                    } else {
                        resultado = "1|Error inesperado.... Revise los archivos de log";
                    }
                    break;
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
        return resultado;
    }

    public String ImportarArchivoTransductor(HttpServletRequest request) {
        int patron = Globales.Validarintnonull(request.getParameter("patron"));
        int plantilla = Globales.Validarintnonull(request.getParameter("plantilla"));
        int transductor = Globales.Validarintnonull(request.getParameter("transductor"));

        if (Globales.PermisosSistemas("PATRON IMPORTAR VALORES TRANSDUCTOR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        XSSFRow row;
        int rows;

        OPCPackage opcPackage;
        XSSFWorkbook wb;
        XSSFSheet sheet;

        File archivo;

        if (archivoadjunto == null) {
            return "1|Error al subir el archivo";
        }

        if (archivoadjunto.trim().equals("")) {
            return "1|Error al subir el archivo";
        }

        try {

            archivo = new File(Globales.ruta_archivo + "uploads/" + archivoadjunto);
            opcPackage = OPCPackage.open(archivo.getAbsolutePath());
            wb = new XSSFWorkbook(opcPackage);

            sheet = wb.getSheetAt(0);
            String Valor = "";

            rows = sheet.getLastRowNum();
            sql = "";
            switch (transductor) {
                case 1:
                    sql = "DELETE FROM patron_transductor1 WHERE idpatron=" + patron + " and plantilla=" + plantilla + ";";
                    for (int fila = 2; fila <= rows; ++fila) {
                        row = sheet.getRow(fila);
                        Valor = String.valueOf(row.getCell(0).getNumericCellValue());
                        if (!Valor.equals("")) {
                            sql += "INSERT INTO public.patron_transductor1(idpatron, plantilla, punto, lectura, error, expandida1, derivada, bep, bemax, expandida2) "
                                    + " VALUES(" + patron + "," + plantilla
                                    + "," + Globales.NumeroComa(String.valueOf(row.getCell(0).getNumericCellValue()))
                                    + "," + Globales.NumeroComa(String.valueOf(row.getCell(1).getNumericCellValue()))
                                    + "," + Globales.NumeroComa(String.valueOf(row.getCell(2).getNumericCellValue()))
                                    + "," + Globales.NumeroComa(String.valueOf(row.getCell(3).getNumericCellValue()))
                                    + "," + Globales.NumeroComa(String.valueOf(row.getCell(4).getNumericCellValue()))
                                    + "," + Globales.NumeroComa(String.valueOf(row.getCell(5).getNumericCellValue()))
                                    + "," + Globales.NumeroComa(String.valueOf(row.getCell(6).getNumericCellValue()))
                                    + "," + Globales.NumeroComa(String.valueOf(row.getCell(7).getNumericCellValue())) + ");";
                        } else {
                            break;
                        }
                    }
                    break;
            }
            if (!sql.equals("")) {
                if (Globales.DatosAuditoria(sql, "PATRON", "IMPORTAR VALORES TRANSDUCTOR", idusuario, iplocal, this.getClass() + "->ImportarArchivoTransductor")) {
                    return "0|Valores importados con éxito";
                } else {
                    return "1|Error inesperado.... Revise los archivos de log";
                }
            } else {
                return "1|No se encontrarón movimientos en el archivo para este mes y año";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarTransductor(HttpServletRequest request) {
        int Transductor = Globales.Validarintnonull(request.getParameter("NTransductor"));
        int IdTransductor = Globales.Validarintnonull(request.getParameter("IdTransductor"));

        if (Globales.PermisosSistemas("PATRON ELIMINAR TRANSDUCTOR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        try {
            sql = "DELETE FROM public.patron_transductor" + Transductor
                    + " WHERE id = " + IdTransductor;
            if (Globales.DatosAuditoria(sql, "PATRON", "ELIMINAR TRANSDUCTOR", idusuario, iplocal, this.getClass() + "->EliminarTransductor")) {
                return "0|Datos eliminados con éxito";
            } else {
                return "1|Error inesperado.... Revise los archivos de log";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarBitacora(HttpServletRequest request) {

        int IdPatron = Globales.Validarintnonull(request.getParameter("IdPatron"));
        int Responsable = Globales.Validarintnonull(request.getParameter("IdEmpleado"));

        String TipoServicio = request.getParameter("TipoServicio");
        String Observacion = request.getParameter("Observacion");
        String Archivo = request.getParameter("Archivo");
        String Reviso = request.getParameter("Reviso");
        String Laboratorio = request.getParameter("Laboratorio");

        String resultado = "";
        try {

            if (IdPatron == 0) {

                return "1|Error al seleccionar el patrón";

            } else {

                if (TipoServicio.equals("") || Observacion.equals("") || Archivo.equals("")) {
                    return "1|Debe llenar todos los datos de la bitácora";

                } else {
                    sql = "INSERT INTO public.patron_bitacora(idpatron, fecha, tiposervicio, proveedor, evidencia, observaciones, "
                            + " responsable, reviso) "
                            + " VALUES(" + IdPatron + ",'now()'," + TipoServicio + ",'" + Laboratorio + "','" + Archivo + "','" + Observacion + "',"
                            + Responsable + ",'" + Reviso + "')";
                }

                if (Globales.DatosAuditoria(sql, "BITACORA", "GUARDAR BITACORA", idusuario, iplocal, "PATRON EDITAR BITACORA")) {
                    resultado = "0|Bitácora añadida con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

        return resultado;

    }

    public String GuardarSensor(HttpServletRequest request) {

        int sensor = Globales.Validarintnonull(request.getParameter("IdSensor"));
        int IdPatron = Globales.Validarintnonull(request.getParameter("IdPatron"));

        String Serie = request.getParameter("SerieS");
        String Magnitud = request.getParameter("CMagnitudS");
        String Url = request.getParameter("UrlS");
        String Lectura = request.getParameter("LecturaS");
        String CertificadoS = request.getParameter("CertificadoS").trim();
        String FechaCalibracionS = request.getParameter("FechaCalibracionS").trim();
        String ProximaS = request.getParameter("ProximaS").trim();
        String resultado = "";
        try {

            if (sensor == 0) {

                sql = "INSERT INTO public.intrumento_condamb_sensores(idinstrumento, serie, idmagnitud, url, lectura, certificado, fecha_calibracion, proxima_fecha) "
                        + " VALUES(" + IdPatron + ",'" + Serie + "','" + Magnitud + "','" + Url + "','" + Lectura + "','" + CertificadoS + "','" + FechaCalibracionS + "','" + ProximaS + "')";

                if (Globales.DatosAuditoria(sql, "PATRON", "GUARDAR SENSOR", idusuario, iplocal, "PATRON EDITAR SENSOR")) {
                    sensor = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from intrumento_condamb_sensores"));
                    resultado = "0|Sensor añadido con éxito|" + sensor;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("SENSOR EDITAR SENSOR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                sql = "UPDATE public.intrumento_condamb_sensores "
                        + " SET serie='" + Serie + "', idmagnitud=" + Magnitud + ", "
                        + "url='" + Url + "', lectura='" + Lectura + "',"
                        + "certificado='" + CertificadoS + "',fecha_calibracion='" + FechaCalibracionS + "', proxima_fecha='" + ProximaS + "' "
                        + " WHERE id= " + sensor;
                if (Globales.DatosAuditoria(sql, "SENSOR", "GUARDAR SENSOR", idusuario, iplocal, "SENSOR EDITAR SENSOR")) {
                    resultado = "0|Sensor actualizado con éxito|" + sensor;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

        return resultado;

    }

    public String GuardarValoresMedida(HttpServletRequest request) {

        int sensor = Globales.Validarintnonull(request.getParameter("IdSensor"));

        double MinTem1 = Globales.ValidardoublenoNull(request.getParameter("MinTem1"));
        double MinTem2 = Globales.ValidardoublenoNull(request.getParameter("MinTem2"));
        double MinTem3 = Globales.ValidardoublenoNull(request.getParameter("MinTem3"));
        double MinTem4 = Globales.ValidardoublenoNull(request.getParameter("MinTem4"));

        double MaxTem1 = Globales.ValidardoublenoNull(request.getParameter("MaxTem1"));
        double MaxTem2 = Globales.ValidardoublenoNull(request.getParameter("MaxTem2"));
        double MaxTem3 = Globales.ValidardoublenoNull(request.getParameter("MaxTem3"));
        double MaxTem4 = Globales.ValidardoublenoNull(request.getParameter("MaxTem4"));

        double MinHume1 = Globales.ValidardoublenoNull(request.getParameter("MinHume1"));
        double MinHume2 = Globales.ValidardoublenoNull(request.getParameter("MinHume2"));
        double MinHume3 = Globales.ValidardoublenoNull(request.getParameter("MinHume3"));
        double MinHume4 = Globales.ValidardoublenoNull(request.getParameter("MinHume4"));

        double MaxHume1 = Globales.ValidardoublenoNull(request.getParameter("MaxHume1"));
        double MaxHume2 = Globales.ValidardoublenoNull(request.getParameter("MaxHume2"));
        double MaxHume3 = Globales.ValidardoublenoNull(request.getParameter("MaxHume3"));
        double MaxHume4 = Globales.ValidardoublenoNull(request.getParameter("MaxHume4"));

        String resultado = "";
        try {

            if (Globales.PermisosSistemas("PATRON GUARDAR VALORES CERTIFICADO", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "DELETE FROM instcondamb_medida WHERE idsensor = " + sensor + ";";

            sql += "INSERT INTO public.instcondamb_medida(tipo, medida, valor1, valor2, valor3, valor4, idsensor) "
                    + " VALUES('Minimo','Temperatura'," + MinTem1 + "," + MinTem2 + "," + MinTem3 + "," + MinTem4 + "," + sensor + ");";
            sql += "INSERT INTO public.instcondamb_medida(tipo, medida, valor1, valor2, valor3, valor4, idsensor) "
                    + " VALUES('Maximo','Temperatura'," + MaxTem1 + "," + MaxTem2 + "," + MaxTem3 + "," + MaxTem4 + "," + sensor + ");";
            sql += "INSERT INTO public.instcondamb_medida(tipo, medida, valor1, valor2, valor3, valor4, idsensor) "
                    + " VALUES('Minimo','Humedad'," + MinHume1 + "," + MinHume2 + "," + MinHume3 + "," + MinHume4 + "," + sensor + ");";
            sql += "INSERT INTO public.instcondamb_medida(tipo, medida, valor1, valor2, valor3, valor4, idsensor) "
                    + " VALUES('Maximo','Humedad'," + MaxHume1 + "," + MaxHume2 + "," + MaxHume3 + "," + MaxHume4 + "," + sensor + ");";

            if (Globales.DatosAuditoria(sql, "PATRON", "GUARDAR VALORES CERTIFICADO", idusuario, iplocal, "PATRON GUARDAR VALORES CERTIFICADO")) {
                resultado = "0|Valores añadidos con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

        return resultado;

    }

    public String EliminarValoresMedida(HttpServletRequest request) {

        int id = Globales.Validarintnonull(request.getParameter("id"));

        String resultado = "";
        try {

            if (Globales.PermisosSistemas("PATRON AUXILIAR ELIMINAR VALORES CERTIFICADO", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "DELETE FROM public.instcondamb_medida WHERE id = " + id;
            if (Globales.DatosAuditoria(sql, "PATRON", "ELIMINAR VALORES CERTIFICADO", idusuario, iplocal, "PATRON GUARDAR VALORES CERTIFICADO")) {
                resultado = "0|Valores eliminados con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
        return resultado;
    }

    public String GuardarValoresCertificado(HttpServletRequest request) {

        int idVC = Globales.Validarintnonull(request.getParameter("idVC"));
        int sensor = Globales.Validarintnonull(request.getParameter("IdSensor"));

        String Patron = request.getParameter("PatronVC");
        String Instrumento = request.getParameter("InstrumentoVC");
        String Variable = request.getParameter("VariableVC");
        String medida = request.getParameter("MedidaVC");
        String Orden = request.getParameter("OrdenVC");
        String resultado = "";
        try {

            if (idVC == 0) {
                if (Globales.PermisosSistemas("PATRON AUXILIAR AGREGAR VALORES CERTIFICADO", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO public.instcondamb_certificado(medida, patron, instrumento, variable, orden, idsensor) "
                        + " VALUES('" + medida + "'," + Patron + "," + Instrumento + "," + Variable + "," + Orden + "," + sensor + ")";

                if (Globales.DatosAuditoria(sql, "PATRON AUXILIAR", "AGREGAR VALORES CERTIFICADO", idusuario, iplocal, "PATRON GUARDAR VALORES CERTIFICADO")) {
                    resultado = "0|Valores añadidos con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("PATRON AUXILIAR EDITAR VALORES CERTIFICADO", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                sql = "UPDATE public.instcondamb_certificado "
                        + " SET medida = '" + medida + "', patron=" + Patron + ", instrumento=" + Instrumento + ", "
                        + "variable=" + Variable + ", orden=" + Orden + ", "
                        + "idsensor=" + sensor + "WHERE id= " + idVC;
                ;
                if (Globales.DatosAuditoria(sql, "PATRON", "GUARDAR VALORES CERTIFICADO", idusuario, iplocal, "PATRON VALORES CERTIFICADO")) {
                    resultado = "0|Valores actualizados con éxito|" + idVC;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

        return resultado;

    }

    public String EliminarValoresCertificado(HttpServletRequest request) {

        int id = Globales.Validarintnonull(request.getParameter("id"));

        String resultado = "";
        try {

            if (Globales.PermisosSistemas("PATRON AUXILIAR ELIMINAR VALORES CERTIFICADO", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "DELETE FROM public.instcondamb_certificado WHERE id = " + id;
            if (Globales.DatosAuditoria(sql, "PATRON", "ELIMINAR VALORES CERTIFICADO", idusuario, iplocal, "PATRON GUARDAR VALORES CERTIFICADO")) {
                resultado = "0|Valores eliminados con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
        return resultado;
    }

    public String ConsultarBitacora(HttpServletRequest request) {
        int idPatron = Globales.Validarintnonull(request.getParameter("idpatron"));
        if (idPatron > 0) {
            String busqueda = "WHERE 1 = 1 ";

            sql = "select b.id as id, b.fecha as fecha, s.nombre as tiposervicio, b.proveedor as proveedor, b.evidencia as evidencia, b.observaciones as observacion, \n"
                    + "u.nombrecompleto as responsable, b.reviso as reviso from patron_bitacora b\n"
                    + "left join servicio s on s.id=b.tiposervicio \n"
                    + "left join seguridad.rbac_usuario u on u.idusu=b.responsable where idpatron = " + idPatron + " group by s.nombre, u.nombrecompleto, b.id, b.fecha, tiposervicio, b.proveedor, b.evidencia,\n"
                    + "observacion, responsable, b.reviso order by b.id ";

            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarBitacora");
        } else {
            return "0";
        }

    }

    public String ConsultarSensor(HttpServletRequest request) {
        int idPatron = Globales.Validarintnonull(request.getParameter("IdPatron"));

        String busqueda = "WHERE 1 = 1 and idinstrumento=" + idPatron;

        sql = "SELECT ics.id, ic.descripcion, ics.idinstrumento as instrumento, ics.serie, m.descripcion as magnitud, ics.url, ics.lectura, "
                + " certificado, fecha_calibracion, to_char(proxima_fecha,'yyyy-MM-dd') as proxima_fecha "
                + " FROM public.intrumento_condamb_sensores ics \n"
                + " left join intrumento_condamb ic on ic.id=ics.id left join magnitudes m on m.id = ics.idmagnitud  \n" + busqueda
                + " group by ics.id, ic.descripcion, ics.serie, m.descripcion, ics.url, ics.lectura ";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarSensor");

    }

    public String ConsultarVM(HttpServletRequest request) {
        int idsensor = Globales.Validarintnonull(request.getParameter("IdSensor"));

        String busqueda = " WHERE 1 = 1 and idsensor=" + idsensor;

        sql = "SELECT id, tipo, medida, valor1, valor2, valor3, valor4, \n"
                + "'<button class=''btn btn-glow btn-danger'' title=''Eliminar Datos de Medidas'' type=''button'' onclick=' || chr(34) || 'EliminarVM(' || id || ')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar \n"
                + "  FROM public.instcondamb_medida " + busqueda;

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarVM");

    }

    public String ConsultarVC(HttpServletRequest request) {
        int idsensor = Globales.Validarintnonull(request.getParameter("IdSensor"));

        String busqueda = " WHERE 1 = 1 and idsensor=" + idsensor;

        sql = "SELECT id, medida, patron, instrumento, patron-instrumento as correccion, variable, orden\n, "
                + "'<button class=''btn btn-glow btn-danger'' title=''Eliminar Datos del Certificado'' type=''button'' onclick=' || chr(34) || 'EliminarVC(' || id || ')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar \n"
                + "  FROM public.instcondamb_certificado" + busqueda;

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarVC");

    }

    public String GuardarCoeficientes(HttpServletRequest request) {

        int Patron = Globales.Validarintnonull(request.getParameter("Patron"));
        int Plantilla = Globales.Validarintnonull(request.getParameter("Plantilla"));
        double Coeficiente0 = Globales.ValidardoublenoNull(request.getParameter("Coeficiente0"));
        double Coeficiente1 = Globales.ValidardoublenoNull(request.getParameter("Coeficiente1"));
        double Coeficiente2 = Globales.ValidardoublenoNull(request.getParameter("Coeficiente2"));
        double Coeficiente3 = Globales.ValidardoublenoNull(request.getParameter("Coeficiente3"));
        double Coeficiente4 = Globales.ValidardoublenoNull(request.getParameter("Coeficiente4"));
        double Coeficiente5 = Globales.ValidardoublenoNull(request.getParameter("Coeficiente5"));

        try {

            if (Globales.PermisosSistemas("PATRON ACTUALIZAR COEFICIENTE", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            int id = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from patron_coeficiente where idpatron=" + Patron + " and plantilla = " + Plantilla));
            if (id == 0) {
                sql = "INSERT INTO public.patron_coeficiente(idpatron, plantilla, a0, a1, a2, a3, a4, a5) \n"
                        + "VALUES (" + Patron + "," + Plantilla + "," + Coeficiente0 + "," + Coeficiente1 + "," + Coeficiente2
                        + "," + Coeficiente3 + "," + Coeficiente4 + "," + Coeficiente5 + ")";
            } else {
                sql = "UPDATE public.patron_coeficiente "
                        + "   SET a0=" + Coeficiente0 + ",a1=" + Coeficiente1 + ",a2=" + Coeficiente2
                        + ", a3=" + Coeficiente3 + ", a4=" + Coeficiente4 + ", a5=" + Coeficiente5
                        + " WHERE id = " + id;
            }

            if (Globales.DatosAuditoria(sql, "PATRON", "ACTUALIZAR COEFICIENTE", idusuario, iplocal, this.getClass() + "->GuardarCoeficientes")) {
                return "0|Datos actualizados con éxito";
            } else {
                return "1|Error inesperado.... Revise los archivos de log";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }
    
    public String EliminarCoeficientes(HttpServletRequest request) {

        int Patron = Globales.Validarintnonull(request.getParameter("Patron"));
        int Plantilla = Globales.Validarintnonull(request.getParameter("Plantilla"));
        
        try {

            if (Globales.PermisosSistemas("PATRON ELIMINAR COEFICIENTE", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            int id = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from patron_coeficiente where idpatron=" + Patron + " and plantilla = " + Plantilla));
            if (id == 0) {
                return "1|No existen coeficientes guardados para este patron y plantilla";
            } 
            sql = "DELETE FROM public.patron_coeficiente WHERE id = " + id;
            
            if (Globales.DatosAuditoria(sql, "PATRON", "ELIMINAR COEFICIENTE", idusuario, iplocal, this.getClass() + "->EliminarCoeficientes")) {
                return "0|Datos eliminados con éxito";
            } else {
                return "1|Error inesperado.... Revise los archivos de log";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }
    
    public String BuscarCoeficientes(HttpServletRequest request) {

        int Patron = Globales.Validarintnonull(request.getParameter("Patron"));
        int Plantilla = Globales.Validarintnonull(request.getParameter("Plantilla"));
        
        sql = "SELECT  a0, a1, a2, a3, a4, a5 FROM public.patron_coeficiente WHERE idpatron = " + Patron + " and plantilla=" + Plantilla;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarCoeficientes");
        
    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String ruta_origen = request.getHeader("Origin");

        HttpSession misession = request.getSession();
        idusuario = misession.getAttribute("idusuario").toString();
        iplocal = misession.getAttribute("IPLocal").toString();
        usuario = misession.getAttribute("codusu").toString();
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");
        if (misession.getAttribute("ArchivosAdjuntos") != null) {
            archivoadjunto = misession.getAttribute("ArchivosAdjuntos").toString();
        }

        try (PrintWriter out = response.getWriter()) {
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else if (misession.getAttribute("codusu") == null) {
                out.println("cerrada");
            } else if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                out.println("cambio-ip");
            } else {
                String opcion = request.getParameter("opcion");
                switch (opcion) {
                    case "GuardarPatron":
                        out.println(GuardarPatron(request));
                        break;
                    case "GuardarPatronAux":
                        out.println(GuardarPatronAux(request));
                        break;
                    case "ConsultarPatron":
                        out.println(ConsultarPatron(request));
                        break;
                    case "ConsultarPatronAux":
                        out.println(ConsultarPatronAux());
                        break;
                    case "BuscarPatron":
                        out.println(BuscarPatron(request));
                        break;
                    case "BuscarPatronAux":
                        out.println(BuscarPatronAux(request));
                        break;
                    case "BuscarSensor":
                        out.println(BuscarSensor(request));
                        break;
                    case "GuardarTransductor":
                        out.println(GuardarTransductor(request));
                        break;
                    case "GuardarBitacora":
                        out.println(GuardarBitacora(request));
                        break;
                    case "ConsultarTransductor":
                        out.println(ConsultarTransductor(request));
                        break;
                    case "ConsultarBitacora":
                        out.println(ConsultarBitacora(request));
                        break;
                    case "GuardarSensor":
                        out.println(GuardarSensor(request));
                        break;
                    case "ConsultarSensor":
                        out.println(ConsultarSensor(request));
                        break;
                    case "GuardarValoresMedida":
                        out.println(GuardarValoresMedida(request));
                        break;
                    case "ConsultarVM":
                        out.println(ConsultarVM(request));
                        break;
                    case "BuscarVM":
                        out.println(BuscarVM(request));
                        break;
                    case "GuardarValoresCertificado":
                        out.println(GuardarValoresCertificado(request));
                        break;
                    case "ConsultarVC":
                        out.println(ConsultarVC(request));
                        break;
                    case "BuscarVC":
                        out.println(BuscarVC(request));
                        break;
                    case "EliminarValoresCertificado":
                        out.println(EliminarValoresCertificado(request));
                        break;
                    case "EliminarValoresMedida":
                        out.println(EliminarValoresMedida(request));
                        break;
                    case "BuscarTransductor":
                        out.println(BuscarTransductor(request));
                        break;
                    case "EliminarTransductor":
                        out.println(EliminarTransductor(request));
                        break;
                    case "ImportarArchivoTransductor":
                        out.println(ImportarArchivoTransductor(request));
                        break;
                    case "GuardarCoeficientes":
                        out.println(GuardarCoeficientes(request));
                        break;
                    case "EliminarCoeficientes":
                        out.println(EliminarCoeficientes(request));
                        break;
                    case "BuscarCoeficientes":
                        out.println(BuscarCoeficientes(request));
                        break;
                    default:
                        throw new AssertionError();
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

}
