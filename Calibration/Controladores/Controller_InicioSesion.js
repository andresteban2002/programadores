angular.module('myApp', []).controller('ControlInicioSesion', function ($scope, $ocLazyLoad, $rootScope, General) {
	
	$scope.clave = "";
	$scope.usuario = "";
	
	$scope.Desbloquear = {
		usuario : "",
		clave : "",
		documento : ""
	}
	
	$scope.submit = function() {
		
		var parametros = "opcion=BuscarUsuario&usuario=" + $scope.usuario + "&clave=" + $scope.clave;
		General.LlamarAjax("InicioSesion", parametros, false, "",1).then(function (response) {
			var resultado = response[0];
            switch (resultado.error) {
                case "0":
                    $rootScope.barraauxiliar  = true;
                    window.location.href = $rootScope.Conexion.url_cliente + "Inicio/";
                    break;
                case "1":
                case "8":
                case "505":
                    $scope.clave = "";
                    $scope.usuario = "";
                    $("#usuario").focus();
                    swal("Acción Cancelada", resultado.mensaje,"warning");
                    if (resultado.error == "8")
                        $rootScope.IPLocal();
                    break;
                case "9":
                    localStorage.setItem("usuario_cambio",data[0].usuario);
                    window.location.href =$rootScope.Conexion.url_cliente + "InicioSesion/CambioClave.php";
                    break;
              
            }
        }, function errorCallback(response) {
            swal("Error de Peitición", response, "error");
        });
    }
    
    $scope.ModalCerrarSesionIni = function(){
		$scope.Desbloquear.usuario = "";
		$scope.Desbloquear.clave = "";
		$scope.Desbloquear.documento = "";
		$("#modalCerrarSesion").modal("show");
	}
    
    $scope.DesbloquearSesion = function(){
		var parametros = "opcion=BuscarUsuario&usuario=" + $scope.Desbloquear.usuario + "&clave=" + $scope.Desbloquear.clave + "&documento=" + $scope.Desbloquear.documento;
		General.LlamarAjax("InicioSesion", parametros, false, "",1).then(function (response) {
			var resultado = response[0];
            switch (resultado.error) {
                case "0":
                    $rootScope.barraauxiliar  = true;
                    window.location.href = $rootScope.Conexion.url_cliente + "Inicio/";
                    break;
                case "1":
                case "8":
                case "505":
                    $scope.clave = "";
                    $scope.usuario = "";
                    $("#usuario").focus();
                    swal("Acción Cancelada", resultado.mensaje,"warning");
                    if (resultado.error == "8")
                        $rootScope.IPLocal();
                    break;
                case "9":
                    localStorage.setItem("usuario_cambio",data[0].usuario);
                    window.location.href =$rootScope.Conexion.url_cliente + "InicioSesion/CambioClave.php";
                    break;
              
            }
        }, function errorCallback(response) {
            swal("Error de Peitición", response, "error");
        });
	}
	
	$scope.ReenviarClave = function() {
            
		if ($.trim($scope.Usuario) == "") {
			$("#Usuario").focus();
			swal("Acción Cancelada", "Debe de ingresar el usuario", "warning");
			return;
		}
		var datos = LlamarAjax("InicioSesion/EnvioClaveCorreo", "usuario=" + usuario).split("|");
		if (datos[0] == "0") {
			swal("", datos[1], "success");
		} else {
			swal("Acción Cancelada", datos[1], "warning");
		}
	}
    
    $rootScope.MenuCerrarSesion(0);
    $rootScope.TraducirIdioma(1);
    $rootScope.barraauxiliar  = false;
    $rootScope.Chat.tabla = "";
    $rootScope.InicioSesion = {
        idioma: 0,
        estado: "",
        usuario: "",
        empleado: 0,
        cargo: "",
        codusu: "",
        idusuario: 0,
        alertas_mensaje: "",
        foto: "",
        barraauxiliar : false
    };
    $rootScope.global.CargandoPrinicipal = false;
    $rootScope.IPLocal();
});



