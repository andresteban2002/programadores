﻿var IdNomina = 0;
var IdConcepto = 0;
var CodigoNom = "";
var TotalFilaNom = 0;
var TotalFilaConcep = 0;
var TotalFilaConstante = 0;
var CaludadaNom = "NO";
var CodigoConcep = "";

var IdConstante = 0;
var CodigoConstante = "";

$("#NominaConsta, #NominaConcep").html(CargarCombo(27, 1));
$("#TipoConcepto, #BTipoConcepto").html(CargarCombo(70, 1));

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "CodigoNom") {
            if ($.trim($(this).val()) != "") {

                BuscarNominas($(this).val());
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function SeleccionarTodo(caja, fila, tipo) {
    var total = 0;
    switch (tipo) {
        case 1:
            total = TotalFilaNom;
            break;
        case 2:
            total = TotalFilaConcep;
            break;
    }
    for (var x = 0; x < total; x++) {
        $("#" + fila + "-" + x).addClass("bg-gris");
        $("#" + caja + x).prop("checked", "checked");
    }
}

function QuitarTodo(caja, fila, tipo) {
    var total = 0;
    switch (tipo) {
        case 1:
            total = TotalFilaNom;
            break;
        case 2:
            total = TotalFilaConcep;
            break;
    }
    for (var x = 0; x < total; x++) {
        $("#" + fila + "-" + x).removeClass("bg-gris");
        $("#" + caja + x).prop("checked", "");
    }
}

function SeleccionarFila(x,caja,fila) {
    if ($("#" + caja + x).prop("checked")) {
        $("#" + fila + "-" + x).addClass("bg-gris");
    } else {
        $("#" + fila + "-" + x).removeClass("bg-gris");
    }
}

function LimpiarTodoNom() {
    $("#CodNomina").val(LlamarAjax("Recursohumano","opcion=MaxCodigo&tipo=1&nomina=0"));
    CodigoNom = "";
    LimpiarDatosNom();
    TablaNominaEmpleado();
}

//LimpiarTodoNom();

function LimpiarDatosNom() {
    IdNomina = 0;
    CodigoNom = "";
    CaludadaNom = "NO";
    $("#TipoNomina").val("").trigger("change");
    $("#EstadoNom").val("Activo").trigger("change");
    $("#TituloNom").val("");
    $("#IdNomina").val("0");
    $("#DescripcionNom").val("");
    $("#FechaIniNom").val("");
    $("#PeriodoNom").val("");
    $("#TablaPerioNomina").html("");
}

function ValidarNomina(codigo) {
    if ($.trim(CodigoNom) != "") {
        if (CodigoNom != codigo) {
            CodigoNom = "";
            LimpiarDatosNom();
        }
    }
}

function BuscarNomina(codigo) {

    if ($.trim(codigo) == "")
        return false;

    if (CodigoNom == codigo)
        return false;

    LimpiarDatosNom();
    CodigoNom = codigo;
    var parametros = "&tipo=1&codigo=" + codigo;
    var datos = LlamarAjax("Recursohumano","opcion=BuscarNominas&" + parametros);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        IdNomina = data[0].id;
        TablaNominaEmpleado(IdNomina);
        TablaNominaPeriodo(IdNomina);
        CaludadaNom = data[0].calculado;
        $("#TipoNomina").val(data[0].tipo).trigger("change");
        $("#EstadoNom").val(data[0].estado).trigger("change");
        $("#TituloNom").val(data[0].titulo);
        $("#IdNomina").val(IdNomina);
        $("#DescripcionNom").val(data[0].descripcion);
        $("#FechaIniNom").val(data[0].fechainicio);
        $("#PeriodoNom").val(data[0].periodo);
    }
}

function TablaNominaEmpleado() {
    var datos = LlamarAjax("Recursohumano","opcion=Empleados_Nomina&tipo=1&id=" + IdNomina);
    var data = JSON.parse(datos);
    var resultado = "";
    var activado = "";
    var clase = "";
    var asignados = 0;
    TotalFilaNom = 0;
    for (var x = 0; x < data.length; x++) {
        if (data[x].estado == "ACTIVO" || data[x].activo > 0) {
            
            activado = "";
            clase = "";
            if (data[x].activo > 0) {
                activado = "checked";
                clase = " class='bg-gris'";
                asignados++;
            }

            console.log(data[x].documento, data[x].nombrecompleto, data[x].cargo, data[x].estado, data[x].activo);
                
            resultado += "<tr id='filanom-" + TotalFilaNom + "'" + clase + ">" +
                "<td class='text-center'><b>" + (TotalFilaNom + 1) + "</b></td>" +
                "<td><input class='form-control' type='checkbox' " + activado + " value='" + data[x].id + "' name='seleccion_nom' id='seleccionado_nom" + TotalFilaNom + "' onchange=\"SeleccionarFila(" + TotalFilaNom + ",'seleccionado_nom','filanom')\"></td>" +
                "<td>" + data[x].documento + "</td>" +
                "<td>" + data[x].nombrecompleto + "</td>" +
                "<td>" + data[x].cargo + "</td>" +
                "<td class='text-right text-XX'>" + formato_numero(data[x].sueldo, 0, _CD, _CM, "") + "</td></tr>";
            TotalFilaNom++;
        }
    }
    $("#canempleadonom").html(asignados);
    $("#TBEmpleadoNomina").html(resultado);
}

function TablaConceptoEmpleados() {
    var datos = LlamarAjax("Recursohumano","opcion=Empleados_Nomina&tipo=2&id=" + IdConcepto);
    var data = JSON.parse(datos);
    var resultado = "";
    var activado = "";
    var clase = "";
    var asignados = 0;
    TotalFilaConcep = 0;
    for (var x = 0; x < data.length; x++) {
        if (data[x].estado == "ACTIVO" || data[x].activo > 0) {
            activado = "";
            clase = "";
            if (data[x].activo > 0) {
                activado = "checked";
                clase = " class='bg-gris'";
                asignados++;
            }
            resultado += "<tr id='filaconcep-" + TotalFilaConcep + "'" + clase + ">" +
                "<td class='text-center'><b>" + (TotalFilaConcep + 1) + "</b></td>" +
                "<td><input class='form-control' type='checkbox' " + activado + " value='" + data[x].id + "' name='seleccionado_concep' id='seleccionado_concep" + TotalFilaConcep + "' onchange=\"SeleccionarFila(" + TotalFilaConcep + ",'seleccionado_concep','filaconcep')\"></td>" +
                "<td>" + data[x].documento + "</td>" +
                "<td>" + data[x].nombrecompleto + "</td>" +
                "<td>" + data[x].cargo + "</td>" +
                "<td class='text-right text-XX'>" + formato_numero(data[x].sueldo, 0, _CD, _CM, "") + "</td></tr>";
            TotalFilaConcep++;
        }
    }
    $("#canempleadoconcep").html(asignados);
    $("#TBEmpleadoConcepto").html(resultado);
}

function TablaNominaPeriodo() {
    if (IdNomina == 0)
        $("#TablaPerioNomina").html("");
    var datos = LlamarAjax("Recursohumano","opcion=Periodos_Nomina&nomina=" + IdNomina);
    var data = JSON.parse(datos);
    var resultado = "";
    for (var x = 0; x < data.length; x++) {
        resultado += "<tr>" +
            "<td>" + data[x].numero + "</td>" +
            "<td>" + data[x].fechainicio + "</td>" +
            "<td>" + data[x].fechafinal + "</td>" +
            "<td class='text-center text-XX'>" + data[x].calculado + "</td>" + 
            "<td class='text-center text-XX'>" + data[x].cerrado + "</td>" + 
            "<td class='text-center text-XX'>" + data[x].pagado + "</td>" + 
            "<td class='text-center text-XX'>" + data[x].contabilizado + "</td></tr>";;
    }
    $("#TablaPerioNomina").html(resultado);
}

$("#formNominas").submit(function (e) {
        
    e.preventDefault();
    var parametros = $("#formNominas").serialize().replace(/\%2C/g, "");
    var datos = LlamarAjax("Recursohumano","opcion=GuardarNomina&" + parametros).split("|");
    if (datos["0"] == "0") {
        IdNomina = datos[2] * 1;
        TablaNominaPeriodo(IdNomina);
        $("#IdNomina").val(IdNomina);
        swal("", datos[1] + datos[3], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

$("#TablaNominas > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    $("#CodNomina").val(numero);
    BuscarNomina(numero);
    $("#modalNominas").modal("hide");
});


function ModalNominas() {

            
    ActivarLoad();
    setTimeout(function () {
        var parametros = "&tipo=1&codigo=0";
        var datos = LlamarAjax("Recursohumano","opcion=BuscarNominas&" + parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaNominas').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "codigo" },
                { "data": "tipo" },
                { "data": "titulo" },
                { "data": "descripcion" },
                { "data": "fechainicio" },
                { "data": "periodo" },
                { "data": "cantidad", "className": "text-XX12 text-center"},
                { "data": "estado" },
                { "data": "calculado" },
                { "data": "cerrado" },
                { "data": "pagado" },
                { "data": "contabilizado" },
                { "data": "id" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });

        $("#modalNominas").modal("show");

    }, 15);
}


function ModalConcepto() {

    var nomina = $("#NominaConcep").val() * 1;
    if (nomina == 0) {
        $("#NominaConcep").focus();
        swal("Acción Cancelada", "Debe de seleccionar una nómina","warning");
        return false;
    }

    var tipo = $("#BTipoConcepto").val() * 1;

    $("#modnomconcepto").html($("#NominaConcep option:selected").text());

    ActivarLoad();
    setTimeout(function () {
        var parametros = "&tipo=2&codigo=0&tipoconcepto=" + tipo + "&nomina=" + nomina;
        var datos = LlamarAjax("Recursohumano","opcion=BuscarNominas&" + parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaConceptos').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "codigo" },
                { "data": "tipo" },
                { "data": "titulo" },
                { "data": "descripcion" },
                { "data": "cantempleados", "className": "text-XX12 text-center" },
                { "data": "formula" },
                { "data": "formulaaporte" },
                { "data": "periodo" },
                { "data": "estado" },
                { "data": "globales" },
                { "data": "vacaciones" },
                { "data": "cesantia" },
                { "data": "prima" },
                { "data": "id" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ]
        });

        $("#modalConceptos").modal("show");

    }, 15);
}

$("#TablaConceptos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    $("#CodConcepto").val(numero);
    BuscarConcepto(numero);
    $("#modalConceptos").modal("hide");
});



function Cambiocodigo() {
    if (IdNomina == 0) {
        swal("Acción Cancelada", "Debe seleccionar el Nominas", "warning");
        return false;
    }
    $("#SCambioNit").html(codigoCli);
    $("#modalCambioNitCli").modal("show");

}

function Cambiarcodigo() {
    var codigo = $.trim($("#Dcodigo").val());
    if (codigo == "") {
        $("#Dcodigo").focus();
        swal("Acción Cancelada", "Debe de ingresar el nuevo NIT/Cédula", "warning");
        return false;
    }

    var datos = LlamarAjax("Recursohumano","opcion=CambioNit&codigo=" + codigo + "&id=" + IdNominas + "&tipo=1").split("|");
    if (datos[0] == "0") {
        $("#modalCambioNitCli").modal("hide");
        $("#CodigoNom").val(codigo);
        BuscarNominas(codigo);
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function CambioConcepto(concepto) {
    concepto = concepto * 1;
    var idnomina = $("#NominaConcep").val() * 1;
    $("#FormulaAporte").prop("disabled", true);
    $("#FormulaAporte").val("");
    $("#CodConcepto").val("");
    if (concepto == 0) {
        return false;
    }
    if (concepto == 4 || concepto == 3 ) {
        $("#FormulaAporte").prop("disabled", false);
    }
    if (IdConcepto == 0)
        $("#CodConcepto").val(LlamarAjax("Recursohumano","opcion=MaxCodigo&tipo=2&nomina=" + idnomina + "&tipoconcepto=" + concepto));
}


function CambioNomina() {
    var nomina = $("#NominaConcep option:selected").text();
    var idnomina = $("#NominaConcep").val() * 1;
    if (idnomina > 0) {
        TablaConceptoEmpleados();
    } else {
        $("#CodConcepto").val("");
        $("#TBEmpleadoConcepto").html("");
    }
    
    if (nomina.indexOf("Quincenal") >= 0) {
        $("#PeriodoConcepto").prop("disabled", false);
    } else {
        $("#PeriodoConcepto").val("0").trigger("change");
        $("#PeriodoConcepto").prop("disabled", true);
    }
    LimpiarDatosConcep();
    ActivarConcepto();
}

function LimpiarTodoConcep() {
    CodigoConcep = "";
    LimpiarDatosConcep();
}

LimpiarTodoConcep();

function LimpiarDatosConcep() {
    IdConcepto = 0;
    $("#TipoConcepto").val("").trigger("change");
    $("#PeriodoConcepto").val("0").trigger("change");
    $("#EstadoConcep").val("Activo").trigger("change");
    $("#TituloConcep").val("");
    $("#IdConcepto").val("0");
    $("#DescripcionConcep").val("");
    $("#Formula").val("");
    $("#Orden").val("");
    $("#FormulaAporte").val("");
    $("#Global").attr('checked', false);
    $("#Vacaciones").attr('checked', false);
    $("#Cesantia").attr('checked', false);
    $("#Prima").attr('checked', false);
    $("#Visible").attr('checked', false);
}

function DesactivarConcepto() {
    $("#TipoConcepto").prop("disabled", true);
    $("#PeriodoConcepto").prop("disabled", true);
    $("#EstadoConcep").prop("disabled", true);
    $("#TituloConcep").prop("disabled", true);
    $("#DescripcionConcep").prop("disabled", true);
    $("#Formula").prop("disabled", true);
    $("#Orden").prop("disabled", true);
    $("#FormulaAporte").prop("disabled", true);
    $("#Global").prop("disabled", true);
    $("#Vacaciones").prop("disabled", true);
    $("#Cesantia").prop("disabled", true);
    $("#Prima").prop("disabled", true);
}

function ActivarConcepto() {

    $("#TipoConcepto").prop("disabled", false);
    $("#PeriodoConcepto").prop("disabled", false);
    $("#EstadoConcep").prop("disabled", false);
    $("#TituloConcep").prop("disabled", false);
    $("#DescripcionConcep").prop("disabled", false);
    $("#Formula").prop("disabled", false);
    $("#Orden").prop("disabled", false);
    $("#FormulaAporte").prop("disabled", false);
    $("#Global").prop("disabled", false);
    $("#Vacaciones").prop("disabled", false);
    $("#Cesantia").prop("disabled", false);
    $("#Prima").prop("disabled", false);
}

function BuscarConcepto(codigo) {
    var nomina = $("#NominaConcep").val() * 1;
    if (nomina == 0)
        return false;
    if ($.trim(codigo) == "")
        return false;

    if (CodigoConcep == codigo)
        return false;

    LimpiarDatosConcep();
    CodigoConcep = codigo;
    var parametros = "&tipo=2&codigo=" + codigo + "&nomina=" + nomina;
    var datos = LlamarAjax("Recursohumano","opcion=BuscarNominas&" + parametros);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        IdConcepto = data[0].id;
        ActivarConcepto();
        TablaConceptoEmpleados(IdConcepto);
        $("#TipoConcepto").val(data[0].idtipo).trigger("change");
        $("#CodConcepto").val(CodigoConcep);
        $("#PeriodoConcepto").val(data[0].periodo).trigger("change");
        $("#EstadoConcep").val(data[0].estado).trigger("change");
        $("#TituloConcep").val(data[0].titulo);
        $("#IdConcepto").val(IdConcepto);
        $("#DescripcionConcep").val(data[0].descripcion);
        $("#Formula").val(data[0].formula);
        $("#Orden").val(data[0].orden);
        $("#FormulaAporte").val(data[0].formulaaporte);
        if (data[0].globales == "SI")
            $("#Global").prop('checked', true);
        if (data[0].vacaciones == "SI")
            $("#Vacaciones").prop('checked', true);
        if (data[0].cesantia == "SI")
            $("#Cesantia").prop('checked', true);
        if (data[0].prima == "SI")
            $("#Prima").prop('checked', true);
        if (data[0].visible == "SI")
            $("#Visible").prop('checked', true);
    }
}

$("#formConceptos").submit(function (e) {
    e.preventDefault();
    var parametros = $("#formConceptos").serialize();
    var datos = LlamarAjax("Recursohumano","opcion=GuardarConcepto&" + parametros).split("|");
    if (datos["0"] == "0") {
        IdConcepto = datos[2] * 1;
        CodigoConcep = datos[3] * 1;
        $("#IdConcepto").val(IdConcepto);
        $("#CodConcepto").val(CodigoConcep);
        swal("", datos[1] + datos[3], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

$("#formConstantes").submit(function (e) {
    e.preventDefault();
    var parametros = $("#formConstantes").serialize().replace(/\%2C/g, "");
    var datos = LlamarAjax("Recursohumano","opcion=GuardarConstante&" + parametros).split("|");
    if (datos["0"] == "0") {
        IdConstante = datos[2] * 1;
        CodigoConstante = datos[3] * 1;
        $("#IdConstante").val(IdConstante);
        $("#CodConstante").val(CodigoConstante);
        swal("", datos[1] + datos[3], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function TablaConstanteEmpleados() {
    var datos = LlamarAjax("Recursohumano","opcion=Empleados_Nomina&tipo=3&id=" + IdConstante);
    var data = JSON.parse(datos);
    var resultado = "";
    var activado = "";
    var clase = "";
    var asignados = 0;
    TotalFilaConstante = 0;
    for (var x = 0; x < data.length; x++) {
        if (data[x].estado == "ACTIVO" || data[x].activo > 0) {
            clase = "";
            if (data[x].monto > 0) {
                clase = " class='bg-gris'";
                asignados++;
            }
            resultado += "<tr " + clase + ">" +
                "<td class='text-center'><b>" + (TotalFilaConstante + 1) + "</b></td>" +
                "<td><input class='form-control' type='hidden' " + activado + " value='" + data[x].id + "' name='seleccionado_constante')\"></td>" +
                "<td>" + data[x].documento + "</td>" +
                "<td>" + data[x].nombrecompleto + "</td>" +
                "<td>" + data[x].cargo + "</td>" +
                "<td class='text-right text-XX'>" + formato_numero(data[x].sueldo, 0, _CD, _CM, "") + "</td>" + 
                "<td class='text-right text-XX'><input type='text' name='seleccionado_monto' value='" + formato_numero(data[x].monto, 0, ".", ",", 0) + "'  required onfocus='FormatoEntrada(this, 1)' onblur='FormatoSalida(this,0,0)' onkeyup='ValidarTexto(this, 1)' class='form-control sinbordecon text-XX2 text-right' required/></td></tr>";
            TotalFilaConstante++;
        }
    }
    $("#canempleadoconsta").html(asignados);
    $("#TBEmpleadoConstante").html(resultado);
}

function ModalConstante() {
    var nomina = $("#NominaConsta").val() * 1;
    if (nomina == 0) {
        $("#NominaConsta").focus();
        swal("Acción Cancelada", "Debe de seleccionar una nómina", "warning");
        return false;
    }

    $("#modnomconstante").html($("#NominaConsta option:selected").text());
        
    ActivarLoad();
    setTimeout(function () {
        var parametros = "&tipo=3&codigo=0&tipoconcepto=0&nomina=" + nomina;
        var datos = LlamarAjax("Recursohumano","opcion=BuscarNominas&" + parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaConstantes').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "codigo" },
                { "data": "titulo" },
                { "data": "descripcion" },
                { "data": "cantempleados", "className": "text-XX12 text-center" },
                { "data": "monto" },
                { "data": "estado" },
                { "data": "id" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ]
        });

        $("#modalConstantes").modal("show");

    }, 15);
}

$("#TablaConstantes > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    $("#CodConcepto").val(numero);
    BuscarConstante(numero);
    $("#modalConstantes").modal("hide");
});

function AplicarMonto() {
    var a_monto = document.getElementsByName("seleccionado_monto");
    var monto = $("#MontoConstante").val();
    for (var x = 0; x < a_monto.length; x++)
        a_monto[x].value = monto;
}

function CambioConstante(nomina) {
    LimpiarDatosConstante();
    if (nomina * 1 == 0)
        return false;
    if (IdConstante == 0)
        $("#CodConstante").val(LlamarAjax("Recursohumano","opcion=MaxCodigo&tipo=3&nomina=" + nomina + "&tipoconcepto=0"));

}

function LimpiarTodoConstante() {
    $("#NominaConsta").trigger("change");
}

//LimpiarTodoConstante();

function LimpiarDatosConstante() {
    IdConstante = 0;
    CodigoConstante = 0;

    $("#CodConstante").val("");
    $("#EstConstante").val("Activo").trigger("change");
    $("#TitConstante").val("");
    $("#IdConstante").val("0");
    $("#DescConstante").val("");
    $("#MontoConstante").val("0");
    $("#Reiniciar").attr('checked', false);
    TablaConstanteEmpleados();
}



function BuscarConstante(codigo) {
    var nomina = $("#NominaConsta").val() * 1;
    if (nomina == 0)
        return false;
    if ($.trim(codigo) == "")
        return false;

    if (CodigoConstante == codigo)
        return false;

    LimpiarDatosConstante();
    CodigoConstante = codigo;
    var parametros = "&tipo=3&codigo=" + codigo + "&nomina=" + nomina;
    var datos = LlamarAjax("Recursohumano","opcion=BuscarNominas&" + parametros);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        IdConstante = data[0].id;
        $("#TipoConcepto").val(data[0].idtipo).trigger("change");
        $("#CodConstante").val(CodigoConstante);
        $("#EstConstante").val(data[0].estado).trigger("change");
        $("#TitConstante").val(data[0].titulo);
        $("#IdConstante").val(IdConstante);
        $("#DescConstante").val(data[0].descripcion);
        $("#MontoConstante").val(formato_numero(data[0].monto, 0, ",", "."));
        if (data[0].reiniciar == "SI")
            $("#Reiniciar").prop('checked', true);
        TablaConstanteEmpleados();
    }
}

function RepararConcepto() {
    if (IdNomina == 0)
        return false
    var datos = LlamarAjax("Recursohumano","opcion=RepararConceptos&nomina=" + IdNomina);
    swal(datos);
}


$('select').select2();
DesactivarLoad();


