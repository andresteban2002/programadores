﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaDesde").val(output2);
$("#FechaHasta").val(output2);
$("#Magnitud, #CMagnitud").html(CargarCombo(1, 1));
$("#Sensor, #CSensor").html(CargarCombo(74, 1, "", "0"));
$("#CAsesor").html(CargarCombo(13, 1));

$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);

var gratemp = document.getElementById('GraTemperatura').getContext('2d');
var gratempco = document.getElementById('GraTemperaturaCo').getContext('2d');


var grapresion = document.getElementById('GraPresion').getContext('2d');
var grapresionco = document.getElementById('GraPresionCo').getContext('2d');

var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)'];
var puntos;



function Consultar() {
    var magnitud = $("#Magnitud").val() * 1;
    var ingreso = $("#Ingreso").val() * 1;
    var magnitud = $("#Magnitud").val() * 1;
    var sensor = $("#Sensor").val() * 1;

    var fechad =  $.trim($("#FechaDesde").val() + " " + $("#HoraDesde").val());
    var fechah =  $.trim($("#FechaHasta").val() + " " + $("#HoraHasta").val());
        
    var certificado = $.trim($("#Certificado").val());

    if (fechad == "" && fechah == "") {
        swal("", ValidarTraduccion("debe de ingresar una fecha desde y una fecha hasta"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {

        var parametros = "fechaini=" + fechad + "&fechafin=" + fechah + "&ingreso=" + ingreso + "&certificado=" + certificado + "&magnitud=" + magnitud + "&sensor=" + sensor;
        var datos = LlamarAjax("Laboratorio","opcion=TablaCondiciones&" + parametros);
        var datasedjson = JSON.parse(datos);
        var table = $('#TablaCondicion').DataTable({
            data: datasedjson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "punto" },
                { "data": "descripcion" },
                { "data": "piso" },
                { "data": "marca" },
                { "data": "modelo" },
				{ "data": "certificado" },
				{ "data": "serie" },
                { "data": "fecha" },
                { "data": "hora" },
                { "data": "temperatura", "className": "text-right" },
                { "data": "vartemperatura", "className": "text-right" },
                { "data": "humedad", "className": "text-right" },
                { "data": "varhumedad", "className": "text-right" },
                { "data": "presion", "className": "text-right" },
                { "data": "varpresion", "className": "text-right" },
            ],
            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ],
        });
        
        var tempmax = 0;
        var tempmin = 9999999999;
		var tempmaxco = 0;
        var tempminco = 9999999999;	
        
        var humemax = 0;
        var humemin = 9999999999;
		var humemaxco = 0;
        var humeminco = 9999999999;	
        
        var presionmax = 0;
        var presionmin = 9999999999;
		var presionmaxco = 0;
        var presionminco = 9999999999;
        
        var labels_th = [];
        var labels_thco = [];
        
        var config_t = {
			type: 'line',
			stacked: false,
            data: {
				labels: labels_th,
                datasets: []
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Gráfica de Temperatura vs Humedad'
                },
                scales: {
					yAxes: [{
						type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
						display: true,
						position: 'left',
						id: 'y-axis-1',
					}, {
						type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
						display: true,
						position: 'right',
						id: 'y-axis-2',

						// grid line settings
						gridLines: {
							drawOnChartArea: false, // only want the grid lines for one axis to show up
						},
					}],
					xAxes:[]
				}
            },
            legend: {
				display: true
			},
			animation: {
				animateScale: true,
				animateRotate: true
			}
        }
        
        
        
        newDataset_t = {
            label: 'Temperatura',
            fill: false,
            borderColor: colores[1],
            backgroundColor: colores[1],
            data: [],
            yAxisID: 'y-axis-1',
        };
        
        newDataset_h = {
            label: 'Humedad',
            fill: false,
            borderColor: colores[4],
            backgroundColor: colores[4],
            data: [],
            yAxisID: 'y-axis-2',
        };	
        
        var config_tco = {
			type: 'line',
			stacked: false,
            data: {
				labels: labels_thco,
                datasets: []
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Gráfica de Temperatura vs Humedad (Corregida)'
                },
                scales: {
					yAxes: [{
						type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
						display: true,
						position: 'left',
						id: 'y-axis-1',
					}, {
						type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
						display: true,
						position: 'right',
						id: 'y-axis-2',

						// grid line settings
						gridLines: {
							drawOnChartArea: false, // only want the grid lines for one axis to show up
						},
					}],
					xAxes:[]
				}
            },
            legend: {
				display: true
			},
			animation: {
				animateScale: true,
				animateRotate: true
			}
        }
        
        newDataset_tco = {
            label: 'Temperatura',
            fill: false,
            borderColor: colores[1],
            backgroundColor: colores[1],
            data: [],
            yAxisID: 'y-axis-1',
        };
        
        newDataset_hco = {
            label: 'Humedad',
            fill: false,
            borderColor: colores[4],
            backgroundColor: colores[4],
            data: [],
            yAxisID: 'y-axis-2',
        };	
        
        
        
        
        
        table.rows().data().each(function (datos, index) {
			if (datos.temperatura > tempmax)
				tempmax = datos.temperatura
			if (datos.temperatura < tempmin)
				tempmin = datos.temperatura
			if (datos.vartemperatura > tempmaxco)
				tempmaxco = datos.vartemperatura
			if (datos.vartemperatura < tempminco)
				tempminco = datos.vartemperatura
				
			if (datos.humedad > humemax)
				humemax = datos.humedad;
			if (datos.humedad < humemin)
				humemin = datos.humedad
			if (datos.varhumedad > humemaxco)
				humemaxco = datos.varhumedad
			if (datos.varhumedad < humeminco)
				humeminco = datos.varhumedad
				
			if (datos.presion > presionmax)
				presionmax = datos.presion
			if (datos.presion < presionmin)
				presionmin = datos.presion
			if (datos.varpresion > presionmaxco)
				presionmaxco = datos.varpresion
			if (datos.varpresion < presionminco)
				presionminco = datos.varpresion
				
			puntos = {
                    x: datos.punto,
                    y: datos.temperatura
                }
                newDataset_t.data.push(puntos);
                
			puntos = {
                    x: datos.punto,
                    y: datos.humedad
                }
			newDataset_h.data.push(puntos);
			labels_th.push(datos.hora);
			
			puntos = {
                    x: datos.punto,
                    y: datos.vartemperatura
                }
                newDataset_tco.data.push(puntos);
                
			puntos = {
                    x: datos.punto,
                    y: datos.varhumedad
                }
			newDataset_hco.data.push(puntos);
			labels_thco.push(datos.hora);
			
		});
		
		config_t.data.datasets.push(newDataset_t);
		config_t.data.datasets.push(newDataset_h);
		
		config_tco.data.datasets.push(newDataset_tco);
		config_tco.data.datasets.push(newDataset_hco);
		
				
		if (window.myTemp != undefined) {
			window.myTemp.destroy();
		}
						
		window.myTemp = new Chart(gratemp, config_t);
        window.myTemp.update();
        
        if (window.myTempco != undefined) {
			window.myTempco.destroy();
		}
						
		window.myTempco = new Chart(gratempco, config_tco);
        window.myTempco.update();
        
        if (tempmin == 9999999999)
			tempmin = 0;
		if (tempminco == 9999999999)
			tempminco = 0;
		if (humemin == 9999999999)
			humemin = 0;
		if (humeminco == 9999999999)
			humeminco = 0;
		if (presionmin == 9999999999)
			presionmin = 0;
		if (presionminco == 9999999999)
			presionminco = 0;
			
		
		$("#TempMax").val(tempmax);
		$("#TempMin").val(tempmin);
		$("#TempVar").val(formato_numero(Math.abs(tempmax - tempmin),1, _CD, _CM, ""));
		$("#TempMaxCor").val(tempmaxco);
		$("#TempMinCor").val(tempminco);
		$("#TempVarCor").val(formato_numero(Math.abs(tempmaxco - tempminco),1, _CD, _CM, ""));
		
		$("#HumeMax").val(humemax);
		$("#HumeMin").val(humemin);
		$("#HumeVar").val(formato_numero(Math.abs(humemax - humemin),1, _CD, _CM, ""));
		$("#HumeMaxCor").val(humemaxco);
		$("#HumeMinCor").val(humeminco);
		$("#HumeVarCor").val(formato_numero(Math.abs(humemaxco - humeminco),1, _CD, _CM, ""));
		
		$("#PresionMax").val(presionmax);
		$("#PresionMin").val(presionmin);
		$("#PresionVar").val(formato_numero(Math.abs(presionmax - presionmin),1, _CD, _CM, ""));
		$("#PresionMaxCor").val(presionmaxco);
		$("#PresionMinCor").val(presionminco);
		$("#PresionVarCor").val(formato_numero(Math.abs(presionmaxco - presionminco),1, _CD, _CM, ""));

        
        DesactivarLoad();
    }, 15);
}

function GenerarPDF(){
	var sensor = $("#Sensor").val() * 1;
	
	var valormediot = NumeroDecimal($("#ValorMedTemp").val());
	var valormedioh = NumeroDecimal($("#ValorMedHume").val());
	
	var desviaciont = NumeroDecimal($("#DesvTemp").val());
	var desviacionh = NumeroDecimal($("#DesvHume").val());

    var fechad =  $.trim($("#FechaDesde").val() + " " + $("#HoraDesde").val());
    var fechah =  $.trim($("#FechaHasta").val() + " " + $("#HoraHasta").val());
    
    if (sensor == 0){
		swal("Acción Cancelada", ValidarTraduccion("Debe de seleccionar un sensor"), "warning");
        return false;
	}
    if (fechad == "" && fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("Debe de ingresar una fecha desde y una fecha hasta"), "warning");
        return false;
    }
    
    if (valormedioh == 0 || valormediot == 0 || desviacionh == 0 || desviaciont == 0){
		swal("Acción Cancelada", ValidarTraduccion("Debe de ingresar los valores de valor medio y desviación en temperatura y humedad"), "warning");
        return false;
	}
    
    ActivarLoad();
	setTimeout(function () {
		var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=CondicionesAmbientales&documento=0&fechad=" + fechad + "&fechah=" + fechah + 
						"&sensor=" + sensor + "&valormediot=" + valormediot + "&valormedioh=" + valormedioh + "&desviaciont=" + desviaciont + "&desviacionh=" + desviacionh).split("||");
		DesactivarLoad();
		if (datos[0] == "0") {
			window.open(url_archivo + "DocumPDF/" + datos[1]);
		} else {
			swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
		}
		
	}, 15);
    
}

function ConsultarCondiciones() {
    var magnitud = $("#CMagnitud").val() * 1;
	var asesor = $("#CAsesor").val() * 1;
    var sensor = $("#CSensor").val() * 1;

    var fechad =  $("#CFechaDesde").val();
    var fechah =  $("#CFechaHasta").val();
    
    if (fechad == "" && fechah == "") {
        swal("", ValidarTraduccion("debe de ingresar una fecha desde y una fecha hasta"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {

        var parametros = "fechaini=" + fechad + "&fechafin=" + fechah + "&asesor=" + asesor + "&magnitud=" + magnitud + "&sensor=" + sensor;
        var datos = LlamarAjax("Laboratorio","opcion=TablaCondicionesPDF&" + parametros);
        var datasedjson = JSON.parse(datos);
        DesactivarLoad();
        var table = $('#TablaCondicionesPDF').DataTable({
            data: datasedjson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "imprimir" },
                { "data": "magnitud" },
                { "data": "instrumento" },
                { "data": "fechadesde" },
                { "data": "fechahasta" },
                { "data": "valormediot", "className": "text-right" },
                { "data": "desviaciont", "className": "text-right" },
                { "data": "valormedioh", "className": "text-right" },
                { "data": "desviacionh", "className": "text-right" },
                { "data": "usuario" },
                { "data": "fecha" },
            ],
            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ],
        });
        
	},15);
}

function ImprimirCondiciones(pdf){
	window.open(url_archivo + "Adjunto/CondicionesAmbientales/" + pdf + ".pdf");
}
	


$('select').select2();
DesactivarLoad();
