﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))
var PermisioEspecial = PerGeneSistema("PERMISO ADMINISTRADOR LABORATORIO");

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

var FechaCal = d.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;

var Ingreso = 0;
var ErrorCalculo = 0;
var IdOperacion = 0;
var IdInstrumento = 0;
var ErrorEnter = 0;
var Fotos = 0;
var NroReporte = 1;
var RangoHasta = 0;
var RangoDesde = 0;
var GuardaTemp = 0;
var CorreoEmpresa = "";
var AplicarTemporal = 0;
var InfoTecIngreso = 0;
var Factor = 0;
var IdVersion = 7;
var asesor = "";
var Habitual = "NO";
var Sitio = "";
var Garantia = "NO";
var AprobarCotizacion = "SI";
var FechaAproCoti = "";

var EstadoVersion = LlamarAjax("Configuracion","opcion=Estado_VersionDocumento&id=" + IdVersion) * 1;


localStorage.setItem("Ingreso", "0");

function GenerarCertificado() {
    localStorage.setItem("CerIngreso", Ingreso);
    LlamarOpcionMenu('Laboratorio/Par_Torsional/07/PlaCertiParTor_07', 3, '');
}

function LimpiarParcial() {
    LimpiarDatos();
    $("#IngresoParTor").val("");
    GuardaTemp = 1;
    Ingreso = 0;
}



function LimpiarTodo() {
    GuardaTemp = 0;
    ErrorEnter = 0;
    
    var revision = $("#Revision").val() * 1;
    localStorage.removeItem("Pre-" + Ingreso + "-" + revision);
    localStorage.removeItem("Table-" + Ingreso + "-" + revision)
    Ingreso = 0;
    $("#Revision").val("1").trigger("change");
    $("#Puntos").val("5").trigger("change");
    $("#IngresoParTor").val("");
    GuardaTemp = 1;
}

function LimpiarPlantilla() {
    GuardaTemp = 0;
    LimpiarDatos();
    $("#IngresoParTor").val("");
    GuardaTemp = 1;
}

function LimpiarDatos() {
    ErrorCalculo = 0;
    IdRemision = 0;
    AplicarTemporal = 0;
    Ingreso = 0;
    localStorage.setItem("Ingreso", Ingreso);
    Fotos = 0
    InfoTecIngreso = 0;
    NroReporte = 1;
    $("#Solicitante").html("");
    $("#Certificado").html("");
    $("#Direccion").html("");
    $("#FechaIng").html("");
    $("#FechaCal").html("");

    $("#TIngreso").html("");
    $("#Medida").html("");
    $("#ValorCon").html("");
    $("#UnidadCon").html("");

    $(".medida").html("");

    $("#Equipo").html("");
    $("#Marca").html("");
    $("#Modelo").html("");
    $("#Serie").html("");
    $("#Remision").val("");
    $("#Usuario").val("");
    $("#FechaRec").val("");
    $("#Tiempo").val("");
    $("#Proxima").html("");

    $("#CMetodo").val("");
    $("#CPunto").val("");
    $("#CServicio").val("");
    $("#CObservCerti").val("");
    $("#CProxima").val("");
    $("#CEntrega").val("");
    $("#CAsesorComer").val("");

    $("#RangoMedida_sc").html("");
    $("#RangoMedida_sch").html("");
    $("#MedResolucion").html("");
    RangoHasta = 0;

    $("#Descripcion").val("");
    $("#Indicacion").val("").trigger("change");
    $("#Clase").val("").trigger("change");
    $("#Clasificacion").val("").trigger("change");
    $("#Tolerancia").val("4");
    $("#Resolucion").val("");

    $("#Tolerancia_sch").val("");
    $("#Resolucion_sch").val("");

    $("#PorLectura1").val("20");
    $("#Serie11").val("");
    $("#Serie21").val("");
    $("#Serie31").val("");

    $("#PorLectura2").val("60");
    $("#Serie12").val("");
    $("#Serie22").val("");
    $("#Serie32").val("");

    $("#PorLectura3").val("100");
    $("#Serie13").val("");
    $("#Serie23").val("");
    $("#Serie33").val("");

    
    $("#Usuario").val("");
    $("#FechaRec").val("");
    $("#Tiempo").val("");
    
    $("#tbodycondicion").html("");
    $("#TBSerie").html("");
    $("#TBCalculo").html("");
    $("#ValorCero").html("0");

    $("#Puntos").val("5").trigger("change");
           
    IdInstrumento = 0;

    for (var x = 1; x <= 5; x++) {
        $("#IdOperacion" + x).val("0");
        $("#Observaciones" + x).val("");
        $("#Conclusion" + x).val("");
        $("#Ajuste" + x).val("");
        $("#Suministro" + x).val("");
        $("#registroingreso" + x).html("");
        $("#Concepto" + x).val("").trigger("change");
    }
}


function GeneralCertificado() {
    if (GuardaTemp == 1 || IdOperacion == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    if ((concepto != 2) && (concepto != 1)) {
        swal("Acción Cancelada", "No se puede generar certificado cuando no está apto a calibrar o en subcontrataciones", "warning");
        return false;
    }

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: '¿Seguro que desea generar el informe técnico del ingreso número ' + Ingreso + '?',
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Laboratorio","opcion=InformeTecnicoDev&ingreso=" + Ingreso + "&reporte=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            $("#NroInforme" + reporte).val(datos[2]);
                            ImprimirInforme();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

function EnviarCotizacion() {

    var concepto = $("#Concepto").val() * 1;


    if (Ingreso == 0)
        return false;
    if (GuardaTemp == 1 || IdOperacion == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    $("#eCliente").val($("#Solicitante").html());
    $("#eCorreo").val(CorreoEmpresa);


    if (concepto == 2) {
        $("#spcotizacion").html(Ingreso);
        $("#enviar_cotizacion").modal("show");
    } else {
        swal("Acción Cancelada", "Solo se podra enviar cotizaciones cuando el ingreso requiera ajuste o suministro", "warning");
    }
    
}

$("#formenviarcotizacion").submit(function (e) {

    e.preventDefault();
    var observacion = $.trim($("#ObserEnvio").val())
    var correo = $.trim($("#eCorreo").val());
    a_correo = correo.split(";");

    if (ValidarCorreo(CorreoEmpresa) == 2) {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("El correo electrónico del contacto no es válido ") + ":" + CorreoEmpresa, "warning");
        //return false;
    }

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#eCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo electrónico no válido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    
    ActivarLoad();
    setTimeout(function () {
        var parametros = "magnitud=6&ingreso=" + Ingreso + "&principal=" + CorreoEmpresa + "&e_email=" + correo + "&observacion=" + observacion;
        var datos = LlamarAjax("Laboratorio","opcion=EnvioCotizacion&" + parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            $("#enviar_cotizacion").modal("hide");
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);

    return false;
});


function LlamarFotoDet(ingreso, fotos) {
    if (fotos == 0)
        return false
    CargarModalFoto(ingreso, fotos, 1);
}


function ConsularReportesIngresos() {
            
    ActivarLoad();
    setTimeout(function () {
        var cliente = $("#ClienteIngreso").val() * 1;
        var tipo = $("#TipoTabla").val() * 1;
        var parametros = "magnitud=6&cliente=" + cliente + "&tipobusqueda=" + tipo;
        var datos = LlamarAjax("Laboratorio","opcion=TablaReportarIngreso&" + parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaReportar').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "ingreso" },
                { "data": "plantilla" },
                { "data": "equipo" },
                { "data": "fecha" },
                { "data": "tiempo" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}


function CambioCondicion(tipo) {
        
    var resultado = "";
    if ($.trim(tipo) != "") {
        if (tipo == "TIPO I")
            tipo = "/1/";
        else
            tipo = "/2/";

        var datos = LlamarAjax("Laboratorio","opcion=CambioCondicion&ingreso=" + Ingreso + "&magnitud=6" + "&tipo=" + tipo);

        if (datos != "[]") {
            var datacon = JSON.parse(datos);
            var valores = "";
            var opcion = 0;
            var radio = "";
            for (var x = 0; x < datacon.length; x++) {
                valores = datacon[x].opcion.split("!!");
                opcion = valores[0] * 1;
                switch (opcion) {
                    case 0:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 1:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 2:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 3:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                }
                resultado += "<tr>" +
                    "<td class='bg-gris'>" + datacon[x].descripcion + "</td>" + radio +
                    "<td><input class='form-control sinbordecon bg-amarillo' name='CObservacion[]' value='" + valores[1] + "'>" +
                    "<input type='hidden' value='" + datacon[x].id + "' name='Ids[]'><input type='hidden' value='" + datacon[x].descripcion + "' name='Descripciones[]'></td></tr>";
            }
        }
    }
            
    $("#tbodycondicion").html(resultado);
    BuscarDescripcionEquipo();
    GuardarTemporal(1);
}


ConsularReportesIngresos();



$('#IngresoParTor').keyup(function (event) {
	numero = this.value*1;
	if (numero == -1)
		numero = Ingreso;
			
	if ((Ingreso != (numero * 1)) && ($("#Equipo").html() != "")) {
		GuardaTemp = 1;
		LimpiarDatos();
		Ingreso = 0;
		ErrorEnter = 0;
	}
	if (event.which == 13) {
		event.preventDefault();

		if (Ingreso == numero)
			return false;
		BuscarIngreso(numero);
	}
});


function BuscarIngreso(numero) {
    var revision = $("#Revision").val() * 1

    if (ErrorEnter == 0) {
        if (numero * 1 > 0) {
            GuardaTemp = 0;
            var datos = LlamarAjax("Laboratorio","opcion=BuscarIngresoOperacion&magnitud=6&ingreso=" + numero + "&medida=N·m&revision=" + revision + "&version=" + IdVersion).split("|");
            if (datos[0] != "[]") {
                var data = JSON.parse(datos[0]);

                if (data[0].idmagnitud != "6") {
                    swal("Acción Cancelada", "El ingreso número " + numero + " no pertenece a la magnitud <br><b>PAR TORSIONAL</b>", "warning");
                    ErrorEnter = 1;
                    $("#IngresoParTor").select();
                    $("#IngresoParTor").focus();
                    return false;
                }

                IdOperacion = 0;
                NumReporte = 0;
                if (datos[1] != "[]") {
                    var dataope = JSON.parse(datos[1]);
                    IdOperacion = dataope[0].id;
                    $("#Puntos").val(dataope[0].puntos).trigger("change");
                    swal("Advertencia", "Este ingreso ya se le registró la operación previa", "info")
                    $("#registroingreso").html("<b>Registado el día " + dataope[0].fecha + " por el técnico " + dataope[0].usuario + "</b>");
                }

                if (IdOperacion == 0) {
                    if (EstadoVersion == 0) {
                        ErrorEnter = 1;
                        swal("Acción Cancelada", "Esta versión ha caducado, solo se puede usar para consultar reportas ya realizados", "warning");
                        LimpiarTodo();
                        $("#IngresoParTor").select();
                        $("#IngresoParTor").focus();
                        return false;
                    }
                }

                IdRemision = data[0].id * 1;
                Ingreso = data[0].ingreso * 1;
                localStorage.setItem("Ingreso", Ingreso);
                Fotos = data[0].fotos * 1;
                asesor = "/" + data[0].asesor + "/";
                Sitio = data[0].sitio;
                Garantia = data[0].garantia;
                Habitual = data[0].habitual;
                FechaAproCoti = $.trim(data[0].fechaaprocoti);
                AprobarCotizacion = data[0].aprobar_cotizacion;
                $("#Solicitante").html(data[0].cliente);
                $("#Certificado").html(data[0].certificado);
                $("#Direccion").html(data[0].direccion);
                $("#FechaIng").html(data[0].fechaing);
                $("#FechaCal").html(FechaCal);
                CorreoEmpresa = data[0].email;

                $("#TIngreso").html(data[0].ingreso);
                $("#Medida").html(data[0].medida);
                $("#ValorCon").html(data[0].medidacon);
                Factor = data[0].medidacon;
                $("#UnidadCon").html("1 " + data[0].medida + " =");

                $(".medida").html(data[0].medida);

                $("#Equipo").html(data[0].equipo);
                $("#Marca").html(data[0].marca);
                $("#Modelo").html(data[0].modelo);
                $("#Proxima").html(data[0].proxima);
                $("#Serie").html(data[0].serie);
                $("#Remision").val(data[0].remision);
                InfoTecIngreso = data[0].informetecnico;


                $("#RangoMedida_sc").html("(" + data[0].desde + " a " + data[0].hasta + ")" );
                $("#RangoMedida_sch").html("(" + (data[0].desde*-1) + " a " + (data[0].hasta*-1) + ")");
                $("#MedResolucion").html(data[0].medida);
                RangoHasta = data[0].hasta * 1;
                RangoDesde = data[0].desde * 1;

                $("#Descripcion").val((data[0].descripcion ? data[0].descripcion : ""));
                $("#Tolerancia").val((data[0].tolerancia ? data[0].tolerancia : ""));
                $("#Resolucion").val((data[0].resolucion ? data[0].resolucion : ""));
                $("#Tolerancia_sch").val((data[0].tolerancia ? data[0].tolerancia_sch : ""));
                $("#Resolucion_sch").val((data[0].resolucion ? data[0].resolucion_sch : ""));
                $("#Indicacion").val((data[0].indicacion ? data[0].indicacion : "")).trigger("change");
                $("#Clase").val((data[0].clase ? data[0].clase : "")).trigger("change");
                $("#Clasificacion").val((data[0].clasificacion ? data[0].clasificacion : "")).trigger("change");
                
                IdInstrumento = (data[0].idinstrumento ? data[0].idinstrumento : 0);

                $("#Usuario").val(data[0].usuarioing);
                $("#FechaRec").val(data[0].fecharec);
                $("#Tiempo").val(data[0].tiempo);

                $("#Plantilla").html(datos[3]);

                //MODAL DE COTIZACIONES

                if (datos[2] != "[]") {
                    var dataco = JSON.parse(datos[2]);
                    if (IdOperacion == 0) {
                        if (Habitual == "NO" && AprobarCotizacion == "SI" && Sitio == "NO" && Garantia == "NO" && FechaAproCoti == "") {
                            if (dataco[0].estado != "Aprobado" && dataco[0].estado != "POR REEMPLAZAR") {
                                ErrorEnter = 1;
                                swal("Acción Cancelada", "La cotizacíon número " + dataco[0].cotizacion + " no ha sido aprobada por el cliente", "warning");
                                sendMessage(3, "Buen días amigo. se requiere la aprobación de la cotización número " + dataco[0].cotizacion + " para proceder a la calibración", asesor);
                                if (PermisioEspecial == 0) {
                                    LimpiarDatos();
                                    return false;
                                }
                            }
                        }
                    }
                    for (var x = 0; x < dataco.length; x++) {
                        if (x == 0) {
                            $("#CMetodo").val(dataco[x].metodo);
                            $("#CPunto").val(dataco[x].punto);
                            $("#CServicio").val(dataco[x].servicio);
                            $("#CObservCerti").val(dataco[x].observacion);
                            $("#CProxima").val(dataco[x].proxima);
                            $("#CEntrega").val(dataco[x].entrega);
                            $("#CAsesorComer").val(dataco[x].asesor);
                        } else {
                            $("#CMetodo").val("<br>" + dataco[x].metodo);
                            $("#CPunto").val("<br>" + dataco[x].punto);
                            $("#CServicio").val("<br>" + dataco[x].servicio);
                            $("#CObservCerti").val("<br>" + dataco[x].observacion);
                            $("#CProxima").val("<br>" + dataco[x].proxima);
                            $("#CEntrega").val("<br>" + dataco[x].entrega);
                            $("#CAsesorComer").val("<br>" + dataco[x].asesor);
                        }
                    }
                } else {
                    if (IdOperacion == 0) {
                        if (Habitual == "NO" && AprobarCotizacion == "SI" && Sitio == "NO" && Garantia == "NO" && FechaAproCoti == "") {
                            ErrorEnter = 1;
                            swal("Acción Cancelada", "Este ingreso no ha sido cotizado por el asesor comercial", "warning");
                            sendMessage(3, "Buen días amigo. se requiere realizar la cotizacion y aprobación del ingreso número " + Ingreso + " para proceder a la calibración", asesor);
                            if (PermisioEspecial == 0) {
                                LimpiarDatos();
                                return false;
                            }
                        }
                    }
                }

                if (IdOperacion == 0) {
                    if (localStorage.getItem("Pre-" + Ingreso + "-" + revision)) {
                        var data = JSON.parse(localStorage.getItem("Pre-" + Ingreso + "-" + revision));
                        $("#Descripcion").val(data[0].descripcion);
                        $("#Tolerancia").val(data[0].tolerancia);
                        $("#Resolucion").val(data[0].resolucion);
                        $("#Tolerancia_sch").val(data[0].tolerancia_sch);
                        $("#Resolucion_sch").val(data[0].resolucion_sch);
                        $("#Indicacion").val(data[0].indicacion).trigger("change");
                        $("#Clase").val(data[0].clase).trigger("change");
                        $("#Clasificacion").val(data[0].clasificacion).trigger("change");
                        $("#Puntos").val(data[0].puntos).trigger("change");
                    }
                    if (localStorage.getItem("Table-" + Ingreso + "-" + revision))
                        $("#tbodycondicion").html(localStorage.getItem("Table-" + Ingreso + "-" + revision));
                    GuardaTemp = 1;
                    
                } else {
                    AplicarTemporal = 1;
                }



            } else {
                $("#IngresoParTor").select();
                $("#IngresoParTor").focus();
                ErrorEnter = 1;
                swal("Acción Cancelada", "El Ingreso número " + numero + " no se ha recibido en el laboratorio", "warning");
                return false;
            }
        }
    } else
        ErrorEnter = 0;
}
  


function BuscarDescripcionEquipo() {
    var clase = $("#Clase").val();
    var tipo = $("#Clasificacion").val();
    $("#Descripcion").html("");
    $("#Lecturas").val("");
    if (clase != "" && tipo != "") {
        var datos = LlamarAjax("Laboratorio","opcion=BuscarDescripcionEquipo&tipo=" + tipo + "&clase=" + clase).split("||");
        if (datos != "XX") {
            $("#Descripcion").html(datos[0]);
            $("#Lecturas").val(datos[1]);
        }
    }
    GuardarTemporal();
}

function GuardarTemporal(tabla) {
    var puntos = $("#Puntos").val() * 1;
    var revision = $("#Revision").val() * 1;
    if (Ingreso > 0 && GuardaTemp == 1) {
        
        var archivo = "[{";
        archivo += '"tolerancia":"' + $("#Tolerancia").val() + '","resolucion":"' + $("#Resolucion").val() + '",' +
            '"tolerancia_sch":"' + $("#Tolerancia_sch").val() + '","resolucion_sch":"' + $("#Resolucion_sch").val() + '",' +
            '"descripcion":"' + $("#Descripcion").val() + '","clase":"' + $("#Clase").val() + '","indicacion":"' + $("#Indicacion").val() + '",' +
            '"clasificacion":"' + $("#Clasificacion").val() + '",' +
            '"idinstrumento":"' + IdInstrumento + '",' + 
            '"id":"' + IdOperacion + '","ingreso":"' + Ingreso + '","puntos":"' + puntos + '"';
        archivo += '}]';
                
        if (tabla) {
            var id = document.getElementsByName("Ids[]");
            var observacion = document.getElementsByName("CObservacion[]");
            var descripcion = document.getElementsByName("Descripciones[]");

            var opcion = 0;
            var resultado = "";
            var radio = "";

            for (var x = 0; x < id.length; x++) {
                opcion = 0;
                if ($("#CSi" + x).prop("checked"))
                    opcion = 1;
                else {
                    if ($("#CNo" + x).prop("checked"))
                        opcion = 2;
                    else {
                        if ($("#CNA" + x).prop("checked"))
                            opcion = 3;
                    }
                }

                switch (opcion) {
                    case 0:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 1:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 2:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 3:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                }
                resultado += "<tr>" +
                    "<td class='bg-gris'>" + descripcion[x].value + "</td>" + radio +
                    "<td><input class='form-control sinbordecon bg-amarillo' name='CObservacion[]' value='" + observacion[x].value + "'>" +
                    "<input type='hidden' value='" + id[x].value + "' name='Ids[]'><input type='hidden' value='" + descripcion[x].value + "' name='Descripciones[]'></td></tr>";
            }
            localStorage.setItem("Table-" + Ingreso + "-" + revision, resultado);
            console.log(localStorage.getItem("Table-" + Ingreso + "-" + revision))
        }


        localStorage.setItem("Pre-" + Ingreso + "-" + revision, archivo);

        $.jGrowl("Guardando Temporal", { life: 1500, theme: 'growl-warning', header: '' });
    } else {
        if (AplicarTemporal == 1)
            GuardaTemp = 1;
    }
}

function CambioConcepto(concepto, tipo) {
    var reporte = (tipo == 0 ? "" : tipo);
    concepto = concepto*1;
        
    $("#Suministro" + reporte).prop("readonly", true);
    $("#Ajuste" + reporte).prop("readonly", true);
    $("#Observaciones" + reporte).prop("readonly", false);
    $("#Conclusion" + reporte).prop("readonly", true);

    $("#Observacion" + reporte).addClass("bg-amarillo");
    $("#Ajuste" + reporte).removeClass("bg-amarillo");
    $("#Suministro" + reporte).removeClass("bg-amarillo");
    $("#Conclusion" + reporte).removeClass("bg-amarillo");

    $("#Documento" + reporte).removeClass("bg-amarillo");
    $("#NumFoto" + reporte).removeClass("bg-amarillo");
    $("#Documento" + reporte).prop("disabled", true);
    $("#NumFoto" + reporte).prop("disabled", true);
    
    if (concepto == 0)
		return false;
	
	
    switch (concepto){
		case 2:
			$("#Suministro" + reporte).prop("readonly", false);
			$("#Ajuste" + reporte).prop("readonly", false);
			$("#Observacion" + reporte).prop("readonly", true);
			$("#Conclusion" + reporte).prop("readonly", true);

			$("#Ajuste" + reporte).addClass("bg-amarillo");
			$("#Suministro" + reporte).addClass("bg-amarillo");
			$("#Conclusion" + reporte).removeClass("bg-amarillo");
			$("#Observacion" + reporte).removeClass("bg-amarillo");
			break;
		case 3:
		case 5:
            $("#Observacion" + reporte).prop("readonly", false);
            $("#Conclusion" + reporte).prop("readonly", false);

            $("#Observacion" + reporte).addClass("bg-amarillo");
            $("#Conclusion" + reporte).addClass("bg-amarillo");

            $("#Documento" + reporte).addClass("bg-amarillo");
            $("#NumFoto" + reporte).removeClass("bg-amarillo");
            $("#Documento" + reporte).prop("disabled", false);
            $("#NumFoto" + reporte).prop("disabled", false);
			break;
		case 8:
			$("#Suministro" + reporte).prop("readonly", true);
			$("#Ajuste" + reporte).prop("readonly", false);
			$("#Observacion" + reporte).prop("readonly", true);
			$("#Conclusion" + reporte).prop("readonly", true);

			$("#Ajuste" + reporte).addClass("bg-amarillo");
			$("#Suministro" + reporte).removeClass("bg-amarillo");
			$("#Conclusion" + reporte).removeClass("bg-amarillo");
			$("#Observacion" + reporte).removeClass("bg-amarillo");
			break;
		case 7:
			$("#Suministro" + reporte).prop("readonly", false);
			$("#Ajuste" + reporte).prop("readonly", true);
			$("#Observacion" + reporte).prop("readonly", true);
			$("#Conclusion" + reporte).prop("readonly", true);

			$("#Ajuste" + reporte).removeClass("bg-amarillo");
			$("#Suministro" + reporte).addClass("bg-amarillo");
			$("#Conclusion" + reporte).removeClass("bg-amarillo");
			$("#Observacion" + reporte).removeClass("bg-amarillo");
			break;
		
    }

    if (tipo == 0)
        GuardarTemporal();

}



$("#FormPrevia").submit(function (e) {
	e.preventDefault();

	var id = document.getElementsByName("Ids[]");
	var observacion = document.getElementsByName("CObservacion[]");

	var a_id = "";
	var a_observacion = "";
	var a_opcion = "";
	var opcion = 0;
	var nobservacion = "";

	for (var x = 0; x < id.length; x++) {
		nobservacion = ($.trim(observacion[x].value) == "" ? "NINGUNA" : $.trim(observacion[x].value))
		if ($("#CSi" + x).prop("checked"))
			opcion = 1;
		else {
			if ($("#CNo" + x).prop("checked"))
				opcion = 2
			else {
				if ($("#CNA" + x).prop("checked"))
					opcion = 3
			}
		}
		if (a_id == "") {
			a_id += id[x].value;
			a_observacion = nobservacion + "";
			a_opcion += opcion;
		} else {
			a_id += "|" + id[x].value;
			a_observacion += "|" + nobservacion + "";
			a_opcion += "|" + opcion;
		}
	}
	var parametros = $("#FormPrevia").serialize().replace(/\%2C/g, "") + "&a_id=" + a_id + "&a_observacion=" + a_observacion + "&a_opcion=" + a_opcion + "&idinstrumento=" + IdInstrumento +
		"&id=" + IdOperacion + "&concepto=&Descripcion=" + $("#Descripcion").html() + "&idconcepto=10&ValorCero=0&Version=" + IdVersion;
	var datos = LlamarAjax("Laboratorio","opcion=GuarOperParTor&" + parametros).split("|");
	if (datos[0] == "0") {
		IdOperacion = datos[2];
		IdInstrumento = datos[3];
		GuardaTemp = 0;
		var plantilla = $("#Plantilla").val() * 1;
		var revision = $("#Revision").val() * 1;
		localStorage.removeItem("Pre-" + Ingreso + "-" + revision);
		localStorage.removeItem("Table-" + Ingreso + "-" + revision);
		swal("", datos[1], "success");
		ConsularReportesIngresos();
		mensaje = "El ingreso número " + Ingreso + " ha sido registrado en operaciones previas para proceder a realizar el certificado " +
				 "<br><b>Cliente: </b>" + datos[4] + "." + 
				 "<br><b>Asesor Comercial: </b>" + datos[5] + "." +  
				 "<br><b>Cliente Habitual: </b>" + datos[6];
		sendMessage(4,mensaje);
	} else {
		swal("Acción Cancelada", datos[1], "warning");
	}
});


function VistaInforme(reporte) {

    if (Ingreso == 0)
        return false;
	ImprimirInforme(reporte);
}

function ImprimirInforme(reporte) {

    if (Ingreso == 0)
        return false;
        
	var plantilla = $("#Plantilla").val() * 1;
    
    ActivarLoad();
	setTimeout(function () {
		var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=InformeTecnico&documento=0&ingreso=" + Ingreso + "&plantilla=" + plantilla).split("||");
		DesactivarLoad();
		if (datos[0] == "0") {
			window.open(url_archivo + "DocumPDF/" + datos[1]);
		} else {
			swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
		}
		
	}, 15);
}



function InformeTecnico(tipo) {

    var concepto = $("#Concepto").val() * 1;
    var reporte = 0;
    var plantilla = $("#Plantilla").val() * 1;
    var id = IdOperacion;
    var temporal = GuardarTemporal;
    if (tipo > 0) {
        concepto = $("#Concepto" + tipo).val() * 1;
        temporal = 0;
        reporte = NroReporte;
    }  
    if (Ingreso == 0)
        return false;

    if (temporal == 1 || id == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }
        
    if (concepto != 3 && concepto != 5) {
        swal("Acción Cancelada", "Solo se podrá generar informes cuando el ingreso no está apto para calibrar ó para mantenimiento y limpieza", "warning");
        return false;
    }

    if (InfoTecIngreso == 1) {

        ImprimirInforme(0);
        return false;
    }

    var mensaje = '¿Seguro que desea generar el informe técnico del ingreso número ' + Ingreso + '?';
    if (concepto == 5)
        mensaje = '¿Seguro que desea generar el informe de mantenimiento y limpieza del ingreso número ' + Ingreso + '?';

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: mensaje,
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Laboratorio","opcion=InformeTecnicoDev&ingreso=" + Ingreso + "&reporte=" + reporte + "&plantilla=" + plantilla)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            $("#NroInforme" + reporte).val(datos[2]);
                            ImprimirInforme();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};
    
function VerFotos() {
    if (Ingreso == 0)
        return false;
    CargarModalFoto(Ingreso, Fotos,1);
}

function ReportarEquipo() {
    if (Ingreso == 0)
        return false;
    $("#Plantilla").val("").trigger("change");
    $("#rpingreso").html(Ingreso);
    $('#tabreportaringreso a[href="#Reporte1"]').tab('show')
    NroReporte = 1;
    $("#ReportarEquipo").modal("show");
    $("#Concepto1").focus();
}

function CambioPlantilla(plantilla) {
    plantilla = plantilla * 1;

    IdOperacion = 1;
    $("#IdReporte" + IdOperacion).val("0");
    $("#Concepto" + IdOperacion).val("").trigger("change");
    $("#Ajuste" + IdOperacion).val("");
    $("#Suministro" + IdOperacion).val("");
    $("#Observacion" + IdOperacion).val("");
    $("#Conclusion" + IdOperacion).val("");
    $("#NroInforme" + IdOperacion).val("");
    $("#registroingreso" + IdOperacion).html("");
    $("#NumFoto" + IdOperacion).val("0").trigger("change");

    if (plantilla == 0)
        return false;

    var datos = LlamarAjax("Laboratorio","opcion=BuscarReportesLab&ingreso=" + Ingreso + "&plantilla=" + plantilla);
    if (datos != "[]") {
        var dataope = JSON.parse(datos);
        for (var x = 0; x < dataope.length; x++) {
            IdOperacion = dataope[x].nroreporte * 1;
            $("#IdReporte" + IdOperacion).val(dataope[x].id);
            $("#Concepto" + IdOperacion).val(dataope[x].idconcepto).trigger("change");
            $("#Ajuste" + IdOperacion).val(dataope[x].ajuste);
            $("#Suministro" + IdOperacion).val(dataope[x].suministro);
            $("#Observacion" + IdOperacion).val(dataope[x].observaciones);
            $("#Conclusion" + IdOperacion).val(dataope[x].conclusion);
            $("#NroInforme" + IdOperacion).val(dataope[0].informetecnico)
            $("#registroingreso" + IdOperacion).html("<b>Registrado el día " + dataope[x].fecha + " por el técnico " + dataope[x].usuario + "</b>" + dataope[x].aprobado);
        }
    }
}

$("#formreportar").submit(function (e) {
    e.preventDefault();
    var idconcepto = $("#RConcepto").val() * 1;
    var concepto = $("#RConcepto option:selected").text();
    var ajuste = $.trim($("#RAjuste").val());
    var suministro = $.trim($("#RSuministro").val());

    if (idconcepto == 2 && ajuste == "" && suministro == "") {
        $("#RAjuste").focus();
        swal("Acción Cancelada", "Debe ingresar la descripción del ajuste y/ó suministro", "warning");
        return false;
    }

    var parametros = $("#formreportar").serialize() + "&concepto=" + concepto + "&idconcepto=" + idconcepto + "&ingreso=" + Ingreso + "&fecha=" + $("#FechaRec").val();
    var datos = LlamarAjax("Laboratorio","opcion=ReportarIngresoOpera&" + parametros).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        $("#IdOperacion").val(datos[2]);
        var numero = Ingreso;
        Ingreso = 0;
        ConsularReportesIngresos();
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function ModalCotizacion() {
    if (Ingreso == 0)
        return false;
    $("#modalcotizacion").modal("show");
}

function EnviarReporte(reporte) {

    var id = $("#IdOperacion" + reporte).val() * 1;
    if (Ingreso == 0 || id == 0)
        return false;



    if (id == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    var concepto = $("#Concepto").val() * 1;
    if (reporte > 0)
        concepto = $("#Concepto" + reporte).val() * 1;
    if (concepto != 2) {
        swal("Acción Cancelada", "Solo se podrá enviar reportes técnicos cuando el ingreso requiere ajuste o suministro", "warning");
        return false;
    }

    var datos = LlamarAjax("Cotizacion","opcion=ModalEmpresa&ingreso=" + Ingreso);
    var data = JSON.parse(datos);

    CorreoEmpresa = data[0].email;
    Empresa = data[0].cliente;

    if (ValidarCorreo(CorreoEmpresa) == 2) {
        swal("Acción Cancela", "el contacto " + data[0].contacto + " no posee correo válido registrado <br>Llame a comercial para registrar el correo del contacto", "warning");
        return false;
    }
    $("#rCliente").val(Empresa);
    $("#rCorreo").val(CorreoEmpresa);

    $("#rObserEnvio").val("");
    $("#rspreporte").html(Ingreso);
    $("#rReporte").val(reporte);
    $("#enviar_reporte").modal("show");
}



$(".guardarreporte").click(function () {
	var reporte = NroReporte;
	var idreporte = $("#IdReporte" + reporte).val() * 1;
	var idconcepto = $("#Concepto" + reporte).val() * 1;
	var ajuste = $.trim($("#Ajuste" + reporte).val());
	var suministro = $.trim($("#Suministro" + reporte).val());
	var observacion = $.trim($("#Observacion" + reporte).val());
	var conclusion = $.trim($("#Conclusion" + reporte).val());
	var concepto = $("#Concepto" + reporte + " option:selected").text();
	var fecha = $("#FechaRec").val();
	var plantilla = $("#Plantilla").val() * 1;
	if (Ingreso == 0)
		return false;
	var mensaje = "";

	if ((idconcepto == 3 || idconcepto == 4) && observacion == "") {
		$("#Observacion" + reporte).focus();
		mensaje = "<br> Debe de ingresar la observación del por qué no se puede calibrar";
	}
	if (idconcepto == 3 && conclusion == "") {
		$("#Conclusion" + reporte).focus();
		mensaje = "<br> Debe de ingresar la conclusión del por qué no se puede calibrar" + mensaje;
	}

	if (idconcepto == 5 && conclusion == "") {
		$("#Conclusion" + reporte).focus();
		mensaje = "<br> Debe de ingresar la conclusión del informe de mantenimiento" + mensaje;
	}

	if (idconcepto == 2 && ajuste == "" && suministro == "") {
		$("#Ajuste" + reporte).focus();
		mensaje = "<br> Debe de ingresar la observación del juste o suministro" + mensaje;
	}
	if (idconcepto == 0) {
		$("#Concepto" + reporte).focus()
		mensaje = "<br> Debe de seleccionar un concepto " + mensaje;
	}
	if (plantilla == 0) {
		$("#Plantilla").focus()
		mensaje = "<br> Debe de seleccionar una plantilla" + mensaje;
	}


	if (mensaje != "") {
		swal("Acción Cancelada", mensaje, "warning");
		return false;
	}

	var parametros = "ingreso=" + Ingreso + "&idconcepto=" + idconcepto + "&concepto=" + concepto + "&ajuste=" + ajuste + "&suministro=" + suministro + "&id=" + idreporte + "&observacion=" + observacion + "&reporte=" + reporte + "&fecha=" + fecha + "&conclusion=" + conclusion + "&plantilla=" + plantilla;
	var datos = LlamarAjax("Laboratorio","opcion=ReportarIngreso&" + parametros).split("|");
	if (datos[0] == "0") {
		$("#IdReporte" + reporte).val(datos[2]);
		swal("", datos[1], "success");
		mensaje = "El ingreso número " + Ingreso + " ha sido reportado en el laboratorio en las siguientes condiciones: <b>" + concepto + "</b>." + 
				 (ajuste != "" ? "<br><b>Ajuste: </b>" + ajuste + "." : "") +  
				 (suministro != "" ? "<br><b>Suministro: </b>" + suministro + "." : "") +  
				 (observacion != "" ? "<br><b>observacion/Informe: </b>" + observacion + "." : "") +  
				 (conclusion != "" ? "<br><b>Conclusiones:</b>" + conclusion + "." : "") +  
				 "<br><b>Cliente: </b>" + datos[3] + "." + 
				 "<br><b>Asesor Comercial: </b>" + datos[4] + "." +  
				 "<br><b>Cliente Habitual: </b>" + datos[5];
		sendMessage(4,mensaje);
	} else {
		swal("Acción Cancelada", datos[1], "warning");
	}
});



$("#formenviarreporte").submit(function (e) {

    e.preventDefault();
    var observacion = $.trim($("#rObserEnvio").val())
    var correo = $.trim($("#rCorreo").val());
    a_correo = correo.split(";");
    var reporte = $("#rReporte").val() * 1;
    var plantilla = $("#Plantilla").val() * 1;

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#rCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo inválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&reporte=" + reporte + "&principal=" + CorreoEmpresa + " &e_email=" + correo + " &cliente=" + Empresa + " &observacion=" + observacion + "&plantilla=" + plantilla;
        var datos = LlamarAjax("Laboratorio","opcion=EnvioReporte&" + parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            $("#enviar_reporte").modal("hide");
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);

    return false;
});

function MostrarFoto(reporte, numero) {
	$("#Documento" + reporte).val("");
    var informe = $("#NroInforme" + reporte).val() * 1;
    $("#FotoMan" + reporte).attr("src", url_cliente + "Adjunto/imagenes/informemantenimineto/" + informe + "/" + numero + ".jpg?id=" + NumeroAleatorio());
    
}


function EliminarReporte(reporte){
	var idreporte = $("#IdReporte" + reporte).val() * 1;
    var plantilla = $("#Plantilla").val() * 1;
    var fecha = $("#FechaRec").val();

    if (Ingreso == 0)
        return false;

    if (idreporte == 0) {
        return false;
    }

    var mensaje = '¿Seguro que desea eliminar el reporte número ' + reporte + ' del ingreso número ' +  Ingreso + '?';
    swal({
		title: 'Advertencia',
		text: mensaje,
		input: 'text',
		showLoaderOnConfirm: true,
		showCancelButton: true,
		confirmButtonText: ValidarTraduccion('Eliminar'),
		cancelButtonText: ValidarTraduccion('Cancelar'),
		confirmButtonClass: 'btn btn-success',
		cancelButtonClass: 'btn btn-danger',

		inputValidator: function (value) {
			return new Promise(function (resolve, reject) {
				if (value) {
					var datos = LlamarAjax("Laboratorio","opcion=ElimarReporteIngreso&ingreso=" + Ingreso + "&observaciones=" + value + "&reporte=" + reporte + "&plantilla=" + plantilla + "&fecha=" + fecha + "&idreporte=" + idreporte)
					datos = datos.split("|");
					if (datos[0] == "0") {
						resultado = datos[1];
						resolve()
					} else {
						reject(datos[1]);
					}

				} else {
					reject(ValidarTraduccion('Debe de ingresar un observación'));
				}
			})
		}
	}).then(function (result) {
					
		swal({
			type: 'success',
			html: 'Reporte número ' + reporte + ' eliminado'
		});
		$("#Concepto" + reporte).val("").trigger("change");
		$("#Suministro" + reporte).prop("readonly", true);
		$("#Ajuste" + reporte).prop("readonly", true);
		$("#Observacion" + reporte).prop("readonly", false);
		$("#Conclusion" + reporte).prop("readonly", true);

		$("#Observacion" + reporte).addClass("bg-amarillo");
		$("#Ajuste" + reporte).removeClass("bg-amarillo");
		$("#Suministro" + reporte).removeClass("bg-amarillo");
		$("#Conclusion" + reporte).removeClass("bg-amarillo");
		
		$("#Observacion" + reporte).val("");
		$("#Ajuste" + reporte).val("");
		$("#Suministro" + reporte).val("");
		$("#Conclusion" + reporte).val("");
		
	});
}

function GuardarDocumento(reporte) {
	var id = "Documento" + reporte;
    var documento = document.getElementById(id);
    var informe = $("#NroInforme" + reporte).val() * 1;
    var numero = $("#NumFoto" + reporte).val() * 1;

    if ($.trim(documento.value) != "") {

        if (informe == 0) {
            swal("Acción Cancelada", "Debe generar primero el informe técnico", "warning");
            return false;
        }
        
        var archivo = GuardarDocumentoAdjunto(id,4,'');
        if (archivo == "" || !archivo){
			swal("Acción Cancelada", "Error al adjuntar la imagen", "warning");
			return false;
		}
		
		ActivarLoad();
		setTimeout(function () {
			var parametros = "opcion=SubirFotoMultiple&informe=" + informe + "&numero=" + numero + "&tipo_imagen=5";
			var data = LlamarAjax("Configuracion",parametros).split("|");
			if (data[0] == "0"){
				MostrarFoto(reporte, numero);
				DesactivarLoad();
			}else{
				DesactivarLoad();
				swal("Acción Cancelada",data[1],"warning");
			}
		},15);
    }
}

$("img").error(function() {
    $(this).attr("src", url_cliente + "Adjunto/imagenes/default.jpg");
});

$('select').select2();
$("#Concepto").select2('destroy'); 
