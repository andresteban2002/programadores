﻿$("#TEquipo").html(CargarCombo(67, 1));
$("#TGrupo, #EGrupo, #CGrupo, #AGrupo").html(CargarCombo(47, 1));
$("#EEquipo, #CEquipo, #AEquipo").html(CargarCombo(48, 1, "", "-1"));
$("#EModelo, #CModelo, #AModelo").html(CargarCombo(50, 1, "", "-1"));
$("#EMarca, #CMarca, #AMarca").html(CargarCombo(49, 1));
$("#EPresentacion, #CPresentacion, #APresentacion").html(CargarCombo(66, 1));
$("#Ubicacion1, #Ubicacion2, #EUbicacion").html(CargarCombo(68, 9));
$("#Responsable1, #Responsable2, #EResponsable").html(CargarCombo(13, 9));


var OpcionEquipo = 0;
var CodigoToma = "";
var ErrorEnter = 0;
var UltimoCodigo = "";
var TablaToma = null;
var TablaToma2 = null;
var TablaCerrar = null;

d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))
var AprobarCotiza = "";

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#AFechaDesde").val(output);
$("#AFechaHasta").val(output2);

function ActivarBuscar() {
    var opcion = $("#SpanBuscar").html();
    if (opcion == "Buscar") {
        $("#BuscarArticulo").removeClass("hidden");
        $("#SpanBuscar").html("Ocultar")
    } else {
        $("#BuscarArticulo").addClass("hidden");
        $("#SpanBuscar").html("Buscar")
    }
}

function BuscarComboCodigo(opcion) {
    var grupo = $("#TGrupo").val() * 1;
    var equipo = $("#TEquipo").val() * 1;
    var marca = $("#TMarca").val() * 1;
    var modelo = $("#TModelo").val() * 1;
    var presentacion = $.trim($("#TPresentacion").val());

    var datos = LlamarAjax("Inventarios/CodigoTomaFisica", "opcion=" + opcion + "&grupo=" + grupo + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo + "&presentacion=" + presentacion);
    switch (opcion) {
        case 1:
            if (grupo == 0) {
                $("#TEquipo").html("");
                $("#TPresentacion").html("");
                $("#TMarca").html("");
                $("#TModelo").html("");
                $("#TCodigo").html("");
                $("#TEquipo").html(CargarCombo(67, 1));
                return false;
            }
            if (OpcionEquipo == 0) {
                if (datos == "XX")
                    datos = "";
                $("#TEquipo").html(datos);
                $("#TEquipo").trigger("change");
            }
            break;
        case 2:

            if (equipo == 0) {
                $("#TPresentacion").html("");
                $("#TMarca").html("");
                $("#TModelo").html("");
                $("#TCodigo").html("");
                return false;
            }

            datos = datos.split("||");
            var tgrupo = datos[1] * 1;
            if (tgrupo != $("#TGrupo").val() * 1) {
                OpcionEquipo = 1;
                $("#TGrupo").val(datos).trigger("change");
            }
            OpcionEquipo = 0;
            if (OpcionEquipo == 0) {
                if (datos == "XX")
                    datos = "";
                $("#TPresentacion").html(datos[0]);
                $("#TPresentacion").trigger("change");
            }
            break;
        case 3:
            if (datos == "XX")
                datos = "";
            $("#TMarca").html(datos);
            $("#TMarca").trigger("change");
            break;
        case 4:
            if (datos == "XX")
                datos = "";
            $("#TModelo").html(datos);
            $("#TModelo").trigger("change");
            break;
        case 5:
            if (datos == "XX")
                datos = "";
            $("#TCodigo").html(datos);
            $("#TCodigo").trigger("change");
            break;
    }
}

function SeleccionarCodigo(codigo) {
    LimpiarDatos();
    if (codigo == "")
        return false;
    $("#Codigo").val(codigo);
    BuscarCodigoToma(codigo, "", 1); 
}

function BuscarCodigoToma(codigo, code, tipo) {
    codigo = $.trim(codigo);
    if (codigo != CodigoToma && $("#CodigoIntB").html() != "")
        LimpiarDatos();
    if ((code.keyCode == 13 || tipo == 1) && ErrorEnter == 0 && codigo != "") {
        var datos = LlamarAjax("Inventarios/BuscarArticulosToma", "codigo=" + codigo);
        if (datos == "[]") {
            ErrorEnter = 1;
            $("#Codigo").focus();
            swal("Acción Cancelada", "El código " + codigo + " no se encuentra registrado en el inventario", "warning");
            return false;
        } 
        var data = JSON.parse(datos);
        if (data[0].estado * 1 == 0) {
            ErrorEnter = 1;
            $("#Codigo").focus();
            swal("Acción Cancelada", "El código " + codigo + " se encuentra inactivo en el inventario", "warning");
            return false;
        }

        if (codigo == data[0].codigointerno)
            CodigoToma = data[0].codigointerno;
        else
            CodigoToma = data[0].codigobarras;

        $("#TTipo").val(data[0].tipo);
        $("#CodigoIntB").html(data[0].codigointerno);
        $("#TipoB").html(data[0].tipo);
        $("#CodigoBarB").html(data[0].codigobarras);
        $("#GrupoB").html(data[0].grupo);
        $("#EquipoB").html(data[0].equipo);
        $("#PresentacionB").html(data[0].presentacion);
        $("#MarcaB").html(data[0].marca);
        $("#ModeloB").html(data[0].modelo);
        $("#IdArticuloB").val(data[0].id);

        $("#TFisica1").val("1");
        $("#TFisica1").select();
        $("#TFisica1").focus();
    } else
        ErrorEnter = 0;

}


function LimpiarDatos() {
    $("#CodigoIntB").html("");
    $("#TipoB").html("");
    $("#CodigoBarB").html("");
    $("#GrupoB").html("");
    $("#EquipoB").html("");
    $("#PresentacionB").html("");
    $("#MarcaB").html("");
    $("#ModeloB").html("");
    $("#IdArticuloB").val("0");
}

function ActivarGuardar(cantidad, code) {
    var id = $("#IdArticuloB").val() * 1;
    if (cantidad * 1 <= 0 || id == 0)
        return false;
    if (code.keyCode == 13 && ErrorEnter == 0)
        GuardarToma();
    else
        ErrorEnter = 0
}

function GuardarToma() {

    var ubicacion = $("#Ubicacion1").val() * 1;
    var responsable = $("#Responsable1").val() * 1;

    if (ubicacion == 0) {
        swal("Acción Cancelada", "Debe de seleccionar una ubicación", "warning");
        ErrorEnter = 1;
        return false;
    }

    var id = $("#IdArticuloB").val() * 1;
    var cantidad = $("#TFisica1").val() * 1;
    var toma = $("#numero").val() * 1;
    var codigo = $("#CodigoIntB").html();
    var descripcion = '<b>Tipo:</b> ' + $("#TipoB").html() + ', <b>Grupo:</b> ' + $("#GrupoB").html() + ', <b>Equipo:</b> ' + $("#EquipoB").html() +  ', <b>Presentación:</b> ' + $("#PresentacionB").html() +
        '<b>, Marca:</b> ' + $("#MarcaB").html() + ', <b>Modelo:</b> ' + $("#ModeloB").html();
        if (id == 0)
        return false;

    if (cantidad * 1 <= 0) {
        $("#TFisica1").focus();
        swal("Acción Cancelada", "Debe de ingresar una cantidad mayor a cero", "warning");
        ErrorEnter = 1;
        return false;
    }

    if (UltimoCodigo == CodigoToma) {
        $("#Codigo").focus();
        swal("Acción Cancelada", "Este código ya se ingreso en la tóma física", "warning");
        ErrorEnter = 1;
        return false;
    }

    var parametros = "articulo=" + id + "&cantidad=" + cantidad + "&toma=" + toma + "&ubicacion=" + ubicacion + "&responsable=" + responsable; 
    var datos = LlamarAjax("Inventarios/GuardarToma", parametros).split("|");
    if (datos[0] == "0") {

        var fila = TablaToma.data().count() + 1;
                
        d = new Date();
        month = d.getMonth() + 1;
        day = d.getDate();
        var horas = d.getHours();
        var minutos = d.getMinutes();
                        
        fecha = d.getFullYear() + '/' +
            (month < 10 ? '0' : '') + month + '/' +
            (day < 10 ? '0' : '') + day + " " + (horas < 10 ? '0' : '') + horas + ":" + (minutos < 10 ? '0' : '') + minutos;

        TablaToma.row.add({
            "fila": fila,
            "codigointerno": codigo,
            "descripcion": descripcion,
            "tomafisica1": toma == 1 ? cantidad : "",
            "fecha1": toma == 1 ? fecha : "",
            "tomafisica2": toma == 2 ? cantidad : "",
            "fecha2": toma == 2 ? fecha : "",
            "eliminar": "<button class='btn btn-glow btn-danger' title='Eliminar Toma Física' type='button' onclick=\"EliminarTomaFisica(" + id + ",'" + codigo + "')\"><span data-icon='&#xe0d8;'></span></button>"
        }).draw();

        TablaToma2.row.add({
            "fila": fila,
            "codigointerno": codigo,
            "descripcion": descripcion,
            "tomafisica1": toma == 1 ? cantidad : "",
            "fecha1": toma == 1 ? fecha : "",
            "tomafisica2": toma == 2 ? cantidad : "",
            "fecha2": toma == 2 ? fecha : "",
            "eliminar": "<button class='btn btn-glow btn-danger' title='Eliminar Toma Física' type='button' onclick=\"EliminarTomaFisica(" + id + ",'" + codigo + "')\"><span data-icon='&#xe0d8;'></span></button>"
        }).draw();

        //TablaToma.order();

        $("#Codigo").val("");
        $("#Codigo").focus();
        ErrorEnter = 0;
        swal("", datos[1], "success");
        UltimoCodigo = CodigoToma;
        LimpiarDatos();
        CodigoToma = "";
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
 
}

function InicializarToma(tipo) {
    var mensaje = "";
    var ubicacion = -1;
    var responsable = -1;
    switch (tipo) {
        case 1:
            ubicacion = $("#Ubicacion1").val() * 1;
            responsable = $("#Responsable1").val() * 1;
            break;
        case 2:
            ubicacion = $("#Ubicacion2").val() * 1;
            responsable = $("#Responsable2").val() * 1;
            break;
        case 3:
            ubicacion = $("#EUbicacion").val() * 1;
            responsable = $("#EResponsable").val() * 1;
            break;
    }
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea inicializar la toma físca?... Recuerde que perderá todos los datos'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Inicializar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {

            return new Promise(function (resolve, reject) {
                $.post("Inventarios/InicializarToma", "ubicacion=" + ubicacion + "&responsable=" + responsable)
                    .done(function (data) {
                        data = data.split("|");
                        if (data[0] == "0") {
                            mensaje = data[1];
                            UltimoCodigo = "";
                            resolve();
                        } else
                            reject(data[1]);
                    })
            })
        }
    }]).then(function (data) {
        TablaTomaFisica();
        swal("", mensaje, "success");;
    });
}

function EliminarTomaFisica(articulo, codigo) {
    var mensaje = "";
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar la toma físca del código ' + codigo + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {

            return new Promise(function (resolve, reject) {
                $.post("Inventarios/EliminarToma", "articulo=" + articulo)
                    .done(function (data) {
                        data = data.split("|");
                        if (data[0] == "0") {
                            mensaje = data[1];
                            resolve()
                        } else
                            reject(data[1]);
                    })
            })
        }
    }]).then(function (data) {
        TablaTomaFisica();
        swal("", mensaje, "success");;
    });
}


function TablaTomaFisica(tipo) {

    var ubicacion = $("#Ubicacion" + tipo).val() * 1;
    var responsable = $("#Responsable" + tipo).val() * 1;

    if (ubicacion == 0) {
        if (TablaToma != null) {
            TablaToma.clear().draw();
            TablaToma2.clear().draw();
        }
        return false;    
    }

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Inventarios/TablaTomaFisica", "ubicacion=" + ubicacion + "&responsable=" + responsable);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        TablaToma = $('#TablaToma1').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "codigointerno" },
                { "data": "descripcion" },
                { "data": "tomafisica1", "className": "text-center text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "fecha1" },
                { "data": "tomafisica2", "className": "text-center text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "fecha2" },
                { "data": "eliminar" }
            ],

            "order": [[0, 'desc']],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ]
        });

        TablaToma2 = $('#TablaToma2').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "codigointerno" },
                { "data": "descripcion" },
                { "data": "tomafisica1", "className": "text-center text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "fecha1" },
                { "data": "tomafisica2", "className": "text-center text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "fecha2" },
                { "data": "eliminar" }
            ],

            "order": [[0, 'desc']],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ]
        });

    }, 15);
}

function ConsultaExcel() {

    var grupo = $("#EGrupo").val() * 1;
    var equipo = $("#EEquipo").val() * 1;
    var ubicacion = $("#EUbicacion").val() * 1;
    var responsable = $("#EResponsable").val() * 1;
    var marca = $("#EMarca").val() * 1;
    var modelo = $("#EModelo").val() * 1;
    var presentacion = $.trim($("#EPresentacion").val());
    var tipo = $.trim($("#ETipo").val());
    var nulo = 0;
    if ($("#ETomaNula").prop("checked"))
        nulo = 1;

    var existencia = 0;
    var toma = 0;
    var diferencia = 0;
    var valor = 0;

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Inventarios/ConsultaTomaExcel", "grupo=" + grupo + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo + "&presentacion=" + presentacion + "&nulo=" + nulo + "&tipo=" + tipo + "&ubicacion=" + ubicacion + "&responsable=" + responsable);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        var table = $('#TablaTomaExcel').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "id" },
                { "data": "codigointerno" },
                { "data": "ubicacion" },
                { "data": "responsable" },
                { "data": "tipo" },
                { "data": "grupo" },
                { "data": "equipo" },
                { "data": "presentacion" },
                { "data": "marca" },
                { "data": "modelo" },
                { "data": "existencia", "className": "text-center text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "tomafisica1", "className": "text-center text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "ultimocosto", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') }
            ],

            "order": [[0, 'asc']],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ]
        });

        table.rows().data().each(function (datos, index) {
            existencia += datos.existencia * 1;
            toma += datos.tomafisica1 * 1;
            valor = (existencia - toma) * datos.ultimocosto;
        });

        diferencia = existencia - toma;

        $("#eexistencia").val(formato_numero(existencia, 0,_CD,_CM,""));
        $("#etoma").val(formato_numero(toma, 0,_CD,_CM,""));
        $("#ediferencia").val(formato_numero(diferencia, 0,_CD,_CM,""));
        $("#evalor").val(formato_numero(valor, 0,_CD,_CM,""));
    }, 15);
}

function GuardarDocumentoToma() {
    var documento = document.getElementById("documentotoma");
    if ($.trim(documento.value) != "") {
        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Cotizacion/AdjuntarExcel";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "error");
                }
            }
        });
    }
}

function ImportarExcel() {
    var valor = '<h3>' + ValidarTraduccion("Seleccione el archivo de excel") + '</h3><font size="2px">Id(Columna 2) y Toma Física(Columna 13)</font><br><input type="file" class="form-control" id="documentotoma" onchange="GuardarDocumentoToma()" name="documento" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required />';
    var mensaje = "";
    
    swal.queue([{
        html: valor,
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Importar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                var datos = LlamarAjax("Inventarios/ImportarArchivo", "").split("|");
                if (datos[0] == "0") {
                    $("#ETomaNula").prop("checked", "checked");
                    mensaje = datos[1];
                    resolve();
                }
                else
                    reject(datos[1]);
            })
        }
    }]).then(function (data) {
        ConsultaExcel();
        swal("", mensaje, "success");;
    });
}

function LimpiarCerrar() {
    if (TablaCerrar != null) {
        TablaCerrar.clear().draw();
    }
}

function ConsultaCerrar() {

    var grupo = $("#CGrupo").val() * 1;
    var equipo = $("#CEquipo").val() * 1;
    var marca = $("#CMarca").val() * 1;
    var modelo = $("#CModelo").val() * 1;
    var presentacion = $.trim($("#CPresentacion").val());
    var tipo = $.trim($("#CTipo").val());
    var nulo = 2;
    
    var existencia = 0;
    var toma = 0;
    var diferencia = 0;
    var valor = 0;

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Inventarios/ConsultaTomaExcel", "grupo=" + grupo + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo + "&presentacion=" + presentacion + "&nulo=" + nulo + "&tipo=" + tipo);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        TablaCerrar = $('#TablaDiferencia').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "codigointerno" },
                { "data": "tipo" },
                { "data": "grupo" },
                { "data": "equipo" },
                { "data": "presentacion" },
                { "data": "marca" },
                { "data": "modelo" },
                { "data": "existencia", "className": "text-center", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "tomafisica1", "className": "text-center", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                {
                    "data": "diferencia", "className": "text-center text-XX", render: function (data, type, row) {
                        var color = 'black';
                        if (data == 0) {
                            color = 'green';
                        } else {
                            if (data > 0) {
                                color = 'blue';
                            } else {
                                color = 'red';
                            }
                        }
                        return '<span style="color:' + color + '">' + data + '</span>';
                    }
                },
                { "data": "valor", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') }
            ],
                        
            "order": [[0, 'asc']],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ]
        });

        TablaCerrar.rows().data().each(function (datos, index) {
            existencia += datos.existencia * 1;
            toma += datos.tomafisica1 * 1;
            valor = (existencia - toma) * datos.ultimocosto;
        });

        diferencia = existencia - toma;

        $("#cexistencia").val(formato_numero(existencia, 0,_CD,_CM,""));
        $("#ctoma").val(formato_numero(toma, 0,_CD,_CM,""));
        $("#cdiferencia").val(formato_numero(diferencia, 0,_CD,_CM,""));
        $("#cvalor").val(formato_numero(valor, 0,_CD,_CM,""));
    }, 15);
}

function CerrarToma() {
    var mensaje = "";
    var contador = 0;
    var grupo = $("#CGrupo").val() * 1;
    var equipo = $("#CEquipo").val() * 1;
    var marca = $("#CMarca").val() * 1;
    var modelo = $("#CModelo").val() * 1;
    var presentacion = $.trim($("#CPresentacion").val());
    var tipo = $.trim($("#CTipo").val());

    var parametros = "grupo=" + grupo + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo + "&presentacion=" + presentacion + "&tipo=" + tipo;

    var registros = TablaCerrar == null ? 0 : TablaCerrar.data().count();
    if (registros == 0) {
        swal("Acción Cancelada", "Debe de mostrar en pantalla la toma física que desea cerrar", "warning");
        return false;
    }

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea cerrar la toma físca?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Cerrar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {

            return new Promise(function (resolve, reject) {
                $.post("Inventarios/CerrarTomaFisica", parametros)
                    .done(function (data) {
                        data = data.split("|");
                        if (data[0] == "0") {
                            mensaje = data[1] + " " + data[2];
                            contador = data[2];
                            resolve();
                        } else
                            reject(data[1]);
                    })
            })
        }
    }]).then(function (data) {
        ConsultaCerrar();
        swal("", mensaje, "success");;
    });
}

function ConsultaAuditoria(opcion) {

    var grupo = $("#AGrupo").val() * 1;
    var equipo = $("#AEquipo").val() * 1;
    var marca = $("#AMarca").val() * 1;
    var modelo = $("#AModelo").val() * 1;
    var presentacion = $.trim($("#APresentacion").val());
    var tipo = $.trim($("#ATipo").val());
    var codigo = $("#ACodigo").val() * 1;

    var fechad = $("#AFechaDesde").val();
    var fechah = $("#AFechaHasta").val();

    if (fechad == "" && fechah == "") {
        swal("", ValidarTraduccion("debe de ingresar una fecha o números de días"), "warning");
        return false;
    }

    var existencia = 0;
    var toma = 0;
    var diferencia = 0;
    var valor = 0;

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Inventarios/ConsultaAuditoriaToma", "grupo=" + grupo + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo + "&presentacion=" + presentacion + "&tipo=" + tipo + "&codigo=" + codigo + "&fechad=" + fechad + "&fechah=" + fechah);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaAuditoria').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "control", "className": "text-center text-XX" },
                { "data": "fecha" },
                { "data": "codigointerno" },
                { "data": "tipo" },
                { "data": "grupo" },
                { "data": "equipo" },
                { "data": "presentacion" },
                { "data": "marca" },
                { "data": "modelo" },
                { "data": "existencia", "className": "text-center", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "tomafisica", "className": "text-center", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                {
                    "data": "diferencia", "className": "text-center text-XX", render: function (data, type, row) {
                        var color = 'black';
                        if (data == 0) {
                            color = 'green';
                        } else {
                            if (data > 0) {
                                color = 'blue';
                            } else {
                                color = 'red';
                            }
                        }
                        return '<span style="color:' + color + '">' + data + '</span>';
                    }
                },
                { "data": "valor", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "usuario" }
            ],

            "order": [[0, 'asc']],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ]
        });
    }, 15);
}



$('select').select2();
DesactivarLoad();
