<!DOCTYPE html>
<!DOCTYPE html>
<html idioma="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <title>System Office &amp; Metrology</title>
    <link href="Scripts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="Scripts/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="Scripts/build/css/custom.css" rel="stylesheet" />
    <link href="Scripts/css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="Scripts/css/sweetalert2.min.css" rel="stylesheet" />
    <link href="Scripts/css/styles.css" rel="stylesheet" type="text/css">
    <link href="Scripts/css/icons.css" rel="stylesheet" type="text/css">
    <link href="Scripts/css/select2.min.css" rel="stylesheet" />
    <link href="Scripts/cropper/dist/cropper.css" rel="stylesheet" />
    <link href="Scripts/css/lestas.css" rel="stylesheet" />
    <link href="Scripts/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="Scripts/css/buttons.dataTables.min.css" rel="stylesheet" />
    <link href="Scripts/css/tableexport.css" rel="stylesheet" />
    <link href="Scripts/css/colReorder.dataTables.min.css" rel="stylesheet" />
    <link href="Scripts/css/gallery.css" rel="stylesheet" />
    <link href="Scripts/css/email-application.css" rel="stylesheet" />
    <link href="Scripts/css/colors.css" rel="stylesheet" />
    <link href="Scripts/css/jquery.spin.css" rel="stylesheet" />

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <script src="Scripts/js/jquery.min.js"></script>
    <script src="Scripts/js/jquery-ui.min.js"></script>
    <script src="Scripts/jquery.modal.min.js"></script>
    <script src="Scripts/json2.js"></script>
    

    <script type="text/javascript" src="Scripts/js/plugins/charts/sparkline.min.js"></script>

    <script type="text/javascript" src="Scripts/js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/forms/switch.min.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/forms/uploader/plupload.queue.min.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/forms/wysihtml5/toolbar.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/interface/jgrowl.min.js"></script>

    <script src="Scripts/js/datatable/jquery.dataTables.min.js"></script>
    <script src="Scripts/js/datatable/dataTables.buttons.min.js"></script>
    <script src="Scripts/js/datatable/buttons.flash.min.js"></script>
    <script src="Scripts/js/datatable/jszip.min.js"></script>
    <script src="Scripts/js/datatable/pdfmake.min.js"></script>
    <script src="Scripts/js/datatable/vfs_fonts.js"></script>
    <script src="Scripts/js/datatable/buttons.html5.min.js"></script>
    <script src="Scripts/js/datatable/buttons.print.min.js"></script>
    <script src="Scripts/js/datatable/buttons.colVis.min.js"></script>
    <script src="Scripts/js/datatable/dataTables.colReorder.min.js"></script>

    <script type="text/javascript" src="Scripts/js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="Scripts/js/plugins/interface/timepicker.min.js"></script>
    <script type="text/javascript" src="Scripts/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="Scripts/js/application.js"></script>
    <script src="Scripts/js/jquery.spin.js"></script>
    <script src="Scripts/js/tableexport.js"></script>
    <script src="Scripts/padctl.js"></script>
    <script src="Scripts/js/select2.min.js"></script>
    <script src="Scripts/js/sweetalert2.min.js"></script>
    <script src="Scripts/js/jquery.backstretch.min.js"></script>
    <script src="Scripts/js/jquery.elevatezoom.js"></script>
    <script src="Scripts/js/nicetitle.js"></script>
    <script src="Scripts/js/moment.min.js"></script>
</head>
<body ng-app="myApp" ng-controller="personCtrl">

<div>

First Name: <input type="text" ng-model="firstName"><br>
Last Name: <input type="text" ng-model="lastName"><br>
<br>
Full Name: {{fullName()}}
<br>
Edad: {{edad()}}

</div>

</body>
</html>


<script src="js/FuncionesGenerales.js"></script>
<script src="json/TraductorIdiomas.js"></script>
<script src="js/ModalAyudas.js"></script>
<script src="Scripts/Chart.js"></script>
<script src="Scripts/webcam.min.js"></script>
<script src="Scripts/cropper/dist/cropper.js"></script>
<script src="Scripts/cropper/InicializarCropper.js"></script>
<script src="js/Index.js"></script>
<script src="js/Router.js"></script>
