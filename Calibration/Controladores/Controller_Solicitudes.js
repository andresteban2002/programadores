
angular.module('myApp', ['ui', 'ngSanitize', 'ngTable']).controller('ControlSolicitudes', function ($scope, $ocLazyLoad, $rootScope, General, NgTableParams) {

    $scope.Documento = "";
    $scope.Solicitud = "";
    $scope.Remision = "";
    $scope.OpcionMagnitud = 0;
    $scope.OpcionEquipo = 0;
    
    $scope.VisibleServicio = [true,true];
    $scope.TipoServicio = 0;
    
    $scope.Intervalo = {
        medida: "",
        desde: "",
        hasta: ""
    }

    function initTabs() {
        tabClasses = ["", ""];
    }

    $scope.getTabClass = function (tabNum) {
        return tabClasses[tabNum];
    };

    $scope.getTabPaneClass = function (tabNum) {
        return "tab-pane " + tabClasses[tabNum];
    }

    $scope.setActiveTab = function (tabNum) {
        initTabs();
        tabClasses[tabNum] = "active";
    };
    
    function initTabsSer() {
        tabClassesser = ["", ""];
    }

    $scope.getTabSerClass = function (tabNum) {
        return tabClassesser[tabNum];
    };

    $scope.getTabPaneSerClass = function (tabNum) {
        return "pane-ser " + tabClassesser[tabNum];
    }

    $scope.setActiveSerTab = function (tabNum) {
        initTabsSer();
        tabClassesser[tabNum] = "active";
        $scope.TipoServicio = tabNum;
    };
    
    
    $scope.ModalClientes = function () {
        $rootScope.Modal.cedula = "";
        $rootScope.Modal.razonsocial = "";
        $rootScope.Modal.tipo = 2;
        $rootScope.Modal.Open("Clientes");
    }
    function initTabsPre() {
        tabClassespre = ["", ""];
    }

    $scope.getTabPreClass = function (tabNum) {
        return tabClassespre[tabNum];
    };

    $scope.getTabPanePreClass = function (tabNum) {
        return "tab-pane-pre " + tabClassespre[tabNum];
    }

    $scope.setActiveTabPre = function (tabNum) {
        initTabsPre();
        tabClassespre[tabNum] = "active";
    };

    $rootScope.ObtenerDatatable = function (data, tipo) {
        switch (tipo) {
            case 1:
                var descripcion = "CODIGO: " + data.codigointerno + ", TIPO: " + data.tipo + ", GRUPO: " + data.grupo +
                        ", ARTICULO: " + data.equipo + ", MARCA: " + data.marca + ", MODELO: " + data.modelo + ", PRESENTACION: " + data.presentacion + ", CARACTERISTICA: " + data.caracteristica;
                var parametros = "opcion=AgregarItemCotizacion&Cliente=" + $scope.Registro.cliente + "&Id=0&Equipo=0&Modelo=0" +
                        "&Observacion=" + descripcion + "&Serie=&Cantidad=1&Intervalo=0" +
                        "&Metodo=&Punto=&Declaracion=&NombreCa=&Articulo=" + data.id +
                        "&Direccion=&Certificado=&Express=&Sitio=&Solicitud=" + $scope.Registro.solicitud +
                        "&Proxima=&SubContratado=&Acreditacion=";
                General.LlamarAjax("Solicitud", parametros, true, "", 0).then(function (response) {
                    var datos = $.trim(response).split("|");
                    if (datos[0] == "0") {
                        $scope.Registro.estado = "Temporal";
                        swal("", datos[1], "success");
                        $scope.TablaSolicitud(1);
                    } else
                        swal("Acción Cancelada", datos[1], "warning");
                });

                break;
            case 2:
                $scope.Documento = $.trim(data.documento);
                $scope.BuscarClienteSol();
                $rootScope.Modal.Close();
                break;
            case 3:
                var parametros = "opcion=AgregarIngreso&ingreso=" + data.ingreso + "&cliente=" + $scope.Registro.cliente + "&sede=" + $scope.Registro.sede + "&contacto=" + $scope.Registro.contacto + "&idcotizacion=" + $scope.Registro.id + "&remision=" + data.remision;
                General.LlamarAjax("Cotizacion", parametros, false, "", 1).then(function (response) {
                    var datos = $.trim(response).split("|");
                    if (datos[0] == "0") {
                        $scope.TablaSolicitud(1);
                        $rootScope.Modal.CargarModal();
                        swal("", datos[1], "success");
                    } else {
                        swal("Acción Cancelada", datos[1], "warning");
                    }
                });
                break;

        }
    };




    $scope.Cliente = {
        id: 0,
        documento: ""
    }
    $scope.CambioFecha = function (consulta, tipo) {
        switch (consulta) {
            case 1:
                switch (tipo) {
                    case 1:
                        $scope.Consulta.fechad.id = General.Obtener_Fecha($scope.Consulta.fechad.value);
                        break;
                    case 2:
                        $scope.Consulta.fechah.id = General.Obtener_Fecha($scope.Consulta.fechah.value);
                        break;
                }
                break;
        }
    }

    $scope.Combos = {
        sede: "",
        contacto: "",
        magnitud: "",
        bequipo: "",
        equipo: "",
        marca: "",
        modelo: "",
        bmodelo: "",
        bintervalo: "",
        intervalo: "",
        cliente: "",
        usuario: "",
        grupo: "",
        otroservicio: ""
    }
    $scope.Inventario = {
        ubicacion: "0",
        grupo: ""
    }



    $scope.CargarCombos = function () {
		$rootScope.global.CargandoPrinicipal = true;
        var parametros = "opcion=CargaComboInicial&tipo=2&json=1";
        General.LlamarAjax("Configuracion", parametros, true, "", 0).then(function (response) {
            var datos = $.trim(response).split("||");
            $scope.Combos.magnitud = JSON.parse(datos[0]);
            $scope.Combos.bequipo = JSON.parse(datos[1]);
            $scope.Combos.equipo = JSON.parse(datos[31]);
            $scope.Combos.marca = JSON.parse(datos[2]);
            $scope.Combos.bmodelo = JSON.parse(datos[6]);
            $scope.Combos.bintervalo = JSON.parse(datos[7]);
            $scope.Combos.cliente = JSON.parse(datos[5]);
            $scope.Combos.usuario = JSON.parse(datos[13]);
            $scope.Combos.grupo = JSON.parse(datos[30]);
            $scope.Combos.otroservicio = JSON.parse(datos[8]);
            $scope.Combos.medida = JSON.parse(datos[4]);
			$rootScope.global.CargandoPrinicipal = false;
        });


    }

    $scope.Consulta = {
        solicitud: '',
        cotizacion: '',
        cliente: '',
        estado: '0',
        usuario: '',
        magnitud: '',
        equipo: '',
        marca: '',
        modelo: '',
        intervalo: '',
        fechad: {
            value: new Date(output + " 00:00"),
            id: output
        },
        fechah: {
            value: new Date(output2 + " 00:00"),
            id: output2
        }
    }

    $scope.InicializarRegistro = function () {
        $scope.Registro = {
            id: 0,
            documento: "",
            cliente: 0,
            sede: "",
            solicitud: 0,
            remision: "",
            idremision: 0,
            cotizacion: "",
            contacto: "",
            estado: "Temporal",
            observaciones: "",
            ttotalpagar: 0,
            fechareg: "",
            usuarioreg: "",
            observacion: "",
            tcantidad: 0,
            CheRecoger: false,
            CheEntrega: false,
            MRDirecion: "",
            MRContacto: "",
            MRTelefono: "",
            MRCorreo: "",
            MEDirecion: "",
            MEContacto: "",
            METelefono: "",
            MECorreo: "",
            tabla: {}
        }
    }

    $scope.GuardarIntervalo = function () {

        var mensaje = "";

        if ($.trim($scope.Intervalo.hasta) == "") {
            $("#IHasta").focus();
            mensaje = "<br> *Ingrese el rango hasta" + mensaje;
        }
        if ($.trim($scope.Intervalo.desde) == "") {
            $("#IDesde").focus();
            mensaje = "<br> *Ingrese el rango desde" + mensaje;
        }

        if ($scope.Intervalo.medida == "") {
            $("#IMedida").focus();
            mensaje = "<br> *Seleccione una unidad de medida" + mensaje;
        }

        if (mensaje != "") {
            swal("Verifique los siguientes campos", mensaje, "warning");
            return false;
        }

        var parametros = "opcion=GuardarIntervalo&magnitud=" + $scope.Servicio.Magnitud + "&medida=" + General.escape($scope.Intervalo.medida) + "&desde=" + General.escape($scope.Intervalo.desde) + "&hasta=" + General.escape($scope.Intervalo.hasta);
        General.LlamarAjax("Cotizacion", parametros, true, "", 0).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                General.CargarCombo(6, $scope.Servicio.Magnitud).then(function (response) {
                    $scope.Combos.intervalo = response.data;
                    $scope.Servicio.Intervalo = $.trim(datos[1]);
                    $scope.Servicio.Intervalo2 = "";
                    $("#modalIntervalo").modal("hide");
                });
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
        });
    }

    $scope.InicializarServicio = function () {
        $scope.Servicio = {
            Id: 0,
            Lote: 0,
            Magnitud: "",
            Equipo: "",
            Marca: "",
            Modelo: "",
            Intervalo: "",
            Intervalo2: "",
            Serie: "",
            Ingreso: 0,
            Cantidad: 1,
            Acreditacion: "SI",
            Sitio: "NO",
            Express: "NO",
            SubContratado: "NO",
            Punto: "N/A",
            Metodo: "LABORATORIO",
            NombreCa: "",
            Direccion: "SEDE",
            Declaracion: "N/A",
            Observacion: "N/A",
            Proxima: "",
            Certificado: "",
            Servicio: "",
            OtroServicio: "",
            Articulo : 0,
            des_servicio: ""
        }
        $scope.Combos.Modelo = "";
        $scope.Combos.Intervalo = "";

        if ($scope.Servicio.NombreCa)
            $scope.Servicio.NombreCa = $scope.Cliente.nombrecompleto;




    }

    $scope.ModalInventariosSol = function () {
        if (($scope.Registro.estado == 'Temporal' || $scope.Registro.estado == "Cotizado") && ($scope.Registro.cliente > 0)) {
            $("#modalInventarioSol").modal("show");
        }
    }



    $scope.BuscarClienteSol = function (sede, contacto) {
        var documento = $.trim($scope.Documento);
        if (sede == ""){
            if ($.trim(documento) == $scope.Cliente.documento || $.trim(documento) == "") {
                return false;
            }
            $scope.LimpiarTodo();
            $scope.Documento = documento;
        }

        var parametros = "opcion=BuscarCliente&documento=" + documento + "&bloquear=0&json=1";
        General.LlamarAjax("Cotizacion", parametros, true, "", 0).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                var data = JSON.parse(datos[1]);
                $scope.Cliente = data[0];
                $scope.Combos.contacto = JSON.parse(datos[2]);
                $scope.Combos.sede = JSON.parse(datos[3]);
                $scope.Registro.cliente = $scope.Cliente.id;
                
                if (sede != ""){
                    $scope.Registro.sede = sede;
                    $scope.Registro.contacto = contacto;
                }

                $scope.TablaSolicitud();
                console.log($scope.Cliente);

            }
        });
    }


    $scope.ValidarCliente = function (event) {
        var documento = $.trim($scope.Documento);
        if (documento == "") {
            return false;
        }
        if (documento != $scope.Cliente.documento) {
            if ($scope.Cliente.documento != "") {
                $scope.LimpiarDatos();
            }
            if (event.keyCode == 13) {
                $("#Solicitud").focus();
            }
        }
    }


    $scope.DatosContacto = function (contacto) {
        $scope.Cliente.email = "";
        $scope.Cliente.telefono = "";
        $scope.Cliente.celular = "";

        General.LlamarAjax("Cotizacion", "opcion=DatosContacto&id=" + contacto, false, "", 1).then(function (response) {
            if (response.length > 0) {
                $scope.Cliente.email = response[0].email;
                $scope.Cliente.telefono = response[0].telefonos;
                $scope.Cliente.celular = response[0].fax;
            }
        });

    }
    $scope.LimpiarTodo = function () {
        $scope.Documento = "";
        $scope.Remision = "";
        $scope.Solicitud = "";
        $scope.LimpiarDatos();
    }

    $scope.LimpiarDatos = function () {
        $scope.Cliente = {documento: ""};
        $scope.InicializarRegistro();
        $scope.Combos.sede = "";
        $scope.Combos.contacto = "";
    }


    $scope.BuscarServicio = function () {

        if ($scope.Registro.cliente > 0) {
            $scope.InicializarServicio();
            $scope.ModalServicios();
            $scope.Servicio.NombreCa = $scope.Cliente.nombrecompleto;
            $scope.Servicio.Direccion = "SEDE";

        }
    }

    $scope.ModalServicios = function () {
        if (($scope.Registro.estado == 'Temporal' || $scope.Registro.estado == "Cotizado") && ($scope.Registro.cliente > 0)) {
            $("#ModalServicios").modal({backdrop: 'static'}, 'show');
        }
    };

    $scope.ModalArticulos = function () {

        if (($scope.Registro.estado == 'Temporal' || $scope.Registro.estado == "Cotizado") && ($scope.Registro.cliente > 0)) {
            $rootScope.Modal.grupo = "";
            $rootScope.Modal.codigobarras = "";
            $rootScope.Modal.descripcion = "";
            $rootScope.Modal.tipo = 1;
            $rootScope.Modal.moneda = $.trim($scope.Cliente.idtipomoneda);
            $rootScope.Modal.dis_moneda = false;
            $rootScope.Modal.dis_precio = false;
            $rootScope.Modal.dis_ubicacion = false;
            $rootScope.Modal.Open("Inventarios");
        }
    }


    $scope.CargarModelos = function (marca, modelo) {
        General.CargarCombo(5, marca).then(function (response) {
            $scope.Combos.modelo = response.data;
            if (modelo != ""){
                $scope.Servicio.Modelo = modelo;
            }
        });
    }

    $scope.CargarIntervalos = function (magnitud, intervalo, intervalo2, equipo) {
        General.CargarCombo(6, magnitud).then(function (response) {
            $scope.Combos.intervalo = response.data;
            if (intervalo != "") {
                $scope.Servicio.Intervalo = intervalo;
            }
            if (intervalo2 != "") {
                $scope.Servicio.Intervalo2 = intervalo2;
            }
        });
        if ($scope.OpcionEquipo == 0) {
            $scope.OpcionMagnitud = 1;
            $scope.Servicio.Equipo = "";
            if (magnitud * 1 > 0) {
                General.CargarCombo(2, magnitud).then(function (response) {
                    $scope.Combos.equipo = response.data;
                    if (equipo != ""){
                        $scope.Servicio.Equipo = equipo;
                    }
                    $scope.OpcionMagnitud = 0;
                });
            } else{
                General.CargarCombo(58, "").then(function (response) {
                    $scope.Combos.equipo = response.data;
                    $scope.OpcionMagnitud = 0;
                });
                $scope.Combos.intervalo = "";
            }
        }

    }
    $scope.MagnitudEquipo = function (equipo, intervalo, intervalo2, metodo) {
        if ($scope.OpcionMagnitud > 0)
            return false;
        General.CargarCombo(17, equipo).then(function (response) {
            $scope.Combos.metodo = response.data;
            if (metodo != "") {
                $scope.Servicio.Metodo = metodo;
            }
        });
        var parametros = "opcion=MagnitudEquipo&id=" + equipo;
        General.LlamarAjax("Cotizacion", parametros, true, "", 1).then(function (response) {
            if (response.length > 0) {
                $scope.OpcionEquipo = 1;
                if (response[0].idmagnitud * 1 != $scope.Servicio.magnitud * 1) {
                    $scope.Servicio.Magnitud = $.trim(response[0].idmagnitud);
                    $scope.CargarIntervalos($scope.Servicio.magnitud, intervalo, intervalo2);
                    $scope.OpcionEquipo = 0;
                } else {
                    $scope.OpcionEquipo = 0;
                }
            }

        });
    }




    $scope.TablaSolicitud = function () {
        var parametros = "opcion=TablaSolicitudDetalle&cliente=" + $scope.Registro.cliente + "&id=0&solicitud=" + $scope.Registro.solicitud + "&json=1";
        General.LlamarAjax("Solicitud", parametros, true, "", 1).then(function (response) {
            $scope.Registro.tabla = response;
            $scope.Totales();
        });
    }

    $("#FormArticuloSol").submit(function (e) {
        e.preventDefault();
        var parametros = General.ConvertirParametros($scope.Servicio);
        parametros += "&opcion=AgregarItemCotizacion&Cliente=" + $scope.Registro.cliente + "&Solicitud=" + $scope.Registro.solicitud;


        General.LlamarAjax("Solicitud", parametros, true, "", 0).then(function (response) {
            datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                swal("", datos[1], "success");
                $scope.InicializarServicio()
                $scope.TablaSolicitud(1);
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }

            return false;

        });

    });

    $scope.Totales = function () {
        $scope.Registro.tcantidad = 0;
        for (var i = 0; i < $scope.Registro.tabla.length; i++) {

            $scope.Registro.tcantidad += $rootScope.NumeroDecimal($scope.Registro.tabla[i].cantidad);


        }
    }



    $scope.SelecccionarFila = function (registro, index) {


        if (registro.idotroservicio > 0 || registro.idarticulo > 0)
        {
            $scope.InicializarServicio();
            $scope.Servicio.Id = registro.id;            
            $scope.Servicio.des_servicio = registro.otroservicio;
            $scope.Servicio.Observacion = registro.observacion;
            $scope.Servicio.OtroServicio = $.trim(registro.idotroservicio);
            $scope.Servicio.Articulo = registro.idarticulo*1;
            $("#ModalOtroServicio").modal("show");

        }
        if (registro.idequipo > 0)
        {
            $scope.InicializarServicio();
            $scope.Servicio.Id = registro.id;
            $scope.Servicio.Magnitud = $.trim(registro.idmagnitud);



            $scope.Servicio.Marca = $.trim(registro.idmarca);
            $scope.CargarModelos($scope.Servicio.Marca, $.trim(registro.idmodelo));
            $scope.CargarIntervalos($scope.Servicio.Magnitud, $.trim(registro.idintervalo), $.trim(registro.idintervalo2), $.trim(registro.idequipo));
            $scope.Servicio.Serie = registro.serie;
            $scope.Servicio.Acreditacion = $.trim(registro.acreditado);
            $scope.Servicio.Sitio = $.trim(registro.sitio);
            $scope.Servicio.Express = $.trim(registro.express);
            $scope.Servicio.SubContratado = $.trim(registro.subcontratado);
            $scope.Servicio.Punto = registro.punto;
            $scope.Servicio.Metodo = registro.metodo;
            $scope.Servicio.NombreCa = registro.nombreca;
            $scope.Servicio.Direccion = registro.direccion;
            $scope.Servicio.Proxima = registro.proxima;
            $scope.Servicio.Certificado = $.trim(registro.certificado);
            $scope.Servicio.Declaracion = registro.declaracion;
            $scope.Servicio.Cantidad = registro.cantidad;
            $scope.Servicio.Observacion = registro.observacion;
            $("#ModalServicios").modal("show");
        }
        console.log(registro);
        console.log($scope.Servicio);
        console.log($scope.Servicio.Magnitud);


    }
    $scope.RecargarCargarCombos = function (opcion) {
        switch (opcion) {
            case 1:
                $scope.CargarIntervalos($scope.Servicio.magnitud, "", "");
                $scope.Combos.metodo = "";
                $scope.Servicio.metodo = "";
                break;
            case 2:
                General.CargarCombo(3, null).then(function (response) {
                    $scope.Combos.marca = response.data;
                });
                break;
            case 3:
                if ($scope.Servicio.marca != "")
                    General.CargarCombo(5, $scope.Servicio.marca).then(function (response) {
                        $scope.Combos.modelo = response.data;
                    });
                break;
            case 4:
                if ($scope.Servicio.magnitud != "")
                    General.CargarCombo(6, $scope.Servicio.magnitud).then(function (response) {
                        $scope.Combos.intervalo = response.data;
                    });
                break;
            case 5:
                if ($scope.Servicio.equipo != "")
                    General.CargarCombo(17, $scope.Servicio.equipo).then(function (response) {
                        $scope.Combos.intervalo = response.data;
                    });
                break;
            case 6:
                General.CargarCombo(4, null).then(function (response) {
                    $scope.Combos.intervalo = response.data;
                });
                break;
        }
    }
    $scope.AgregarIntervalo = function () {

        if ($scope.Servicio.Magnitud == "") {
            $("#Magnitud").focus();
            swal("Acción Cancelada", "Debe de seleccionar una magnitud", "warning");
            return false;
        }

        $scope.Intervalo.medida = "";
        $scope.Intervalo.desde = "";
        $scope.Intervalo.hasta = "";

        $("#modalIntervalo").modal("show");

    }
    $scope.AgregarOpcion = function (tipo, mensaje, id, elemento) {
        var magnitud = $scope.Servicio.Magnitud * 1;
        var marca = $scope.Servicio.Marca * 1;
        console.log(elemento);
        switch (tipo) {
            case 1:
                if (magnitud == 0) {
                    $("#Magnitud").focus();
                    swal("Acción Cancelada", "Debe de seleccionar una magnitud", "warning");
                    return false;
                }
                break;
            case 3:
                if (marca == 0) {
                    $("#Marca").focus();
                    swal("Acción Cancelada", "Debe de seleccionar una marca", "warning");
                    return false;
                }
                break;
        }
        swal({
            title: 'Agregar Opción',
            text: mensaje,
            input: 'text',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: $rootScope.ValidarTraduccion('Agregar'),
            cancelButtonText: $rootScope.ValidarTraduccion('Cancelar'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',

            inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                    if ($.trim(value)) {
                        var parametros = "opcion=GuardarOpcion&tipo=" + tipo + "&magnitud=" + magnitud + "&marca=" + marca + "&descripcion=" + value + "&tipoconcepto=0&json=1";
                        General.LlamarAjax("Cotizacion", parametros, true, "", 0).then(function (response) {
                            var datos = $.trim(response).split("|");
                            if (datos[0] != "XX") {
                                switch (tipo) {
                                    case 1:
                                        $scope.Combos.equipo = JSON.parse(datos[1]);
                                        $scope.Servicio.Equipo = $.trim(datos[0]);
                                        $scope.MagnitudEquipo($scope.Servicio.equipo, "", "", "");
                                        break;
                                    case 2:
                                        $scope.Combos.marca = JSON.parse(datos[1]);
                                        $scope.Servicio.Marca = $.trim(datos[0]);
                                        $scope.CargarModelos($scope.Servicio.Marca, "");
                                        break;
                                    case 3:
                                        $scope.Combos.modelo = JSON.parse(datos[1]);
                                        $scope.Servicio.Modelo = $.trim(datos[0]);
                                        break;
                                }
                                resolve();
                            } else {
                                reject(datos[1]);
                            }
                        });

                    } else {
                        reject(ValidarTraduccion('Debe de ingresar la descripción de ' + mensaje));
                    }
                })
            }
        }).then(function (result) {
            $("#" + id).focus();

        })
    }





    $scope.ConsultarSolicitud = function () {
        if ($scope.Consulta.fechad.id == "" || $scope.Consulta.fechah.id == "") {
            swal("Acción Cancelada", ValidarTraduccion("Debe de ingresar una fecha o números de días"), "warning");
            return false;
        }
        var parametros = "opcion=ConsultarSolicitud&" + General.ConvertirParametros($scope.Consulta);
        General.LlamarAjax("Solicitud", parametros, true, "", 0).then(function (response) {
            $rootScope.TablaSolicitudes = new NgTableParams({count: 10}, {dataset: response});

        });
    }

    $scope.ImprimirSolicitud = function (solicitud) {
        if (solicitud == 0)
        {
            return false;
        }
        var parametros = "opcion=GenerarPDF&tipo=Solicitud&documento=" + solicitud;
        General.LlamarAjax("Configuracion", parametros, true, "", 1).then(function (response) {
            datos = $.trim(response).split("||");
            if (datos[0] == "0") {
                window.open($rootScope.Conexion.url_archivo + "DocumPDF/" + datos[1]);
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
        });

    }

    ImprimirSolicitud = function (solicitud) {
        $scope.ImprimirSolicitud(solicitud);
    }

    $scope.Totales = function () {
        $scope.Registro.tcantidad = 0;
        for (var i = 0; i < $scope.Registro.tabla.length; i++) {

            $scope.Registro.tcantidad += $rootScope.NumeroDecimal($scope.Registro.tabla[i].cantidad);


        }
    }


    $scope.EliminarItem = function (registro, index) {
        if ($scope.Registro.estado != "Temporal" && $scope.Registro.estado != "Pendiente") {
            swal("Acción cancelada", "No se puede editar una solicitud en estado " + $scope.Registro.estado, "warning");
            return false;
        }
        if ($scope.Registro.tabla.length == 1 && $scope.Registro.estado == "Pendiente") {
            swal("Acción cancelada", "No se puede elminar el único item de la solicitud", "warning");
            return false;
        }
        swal.queue([{
                title: 'Advertencia',
                text: '¿Seguro que desea eliminar el item del equipo ' + registro.equipo + '?',
                type: 'question',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        $.post(url_servidor + "Solicitud", "opcion=EliminarItemSolicitud&id=" + registro.id + "&Solicitud=" + $scope.Registro.solicitud)
                                .done(function (data) {
                                    datos = data.split("|");
                                    if (datos[0] == "0") {
                                        $scope.Registro.tabla.splice(index, 1);
                                        $.jGrowl(datos[1], {life: 1500, theme: 'growl-success', header: ''});
                                        $scope.$apply();
                                        resolve();
                                    } else {
                                        reject(datos[1]);
                                        swal("Acción Cancelada", datos[1], "warning");
                                    }
                                })
                    })
                }
            }]);
    }

    $scope.EliminarSolicitud = function (registro, index) {
        if ($scope.Registro.cliente == 0)
            return false;
        if ($scope.Registro.estado != "Temporal") {
            swal("Acción cancelada", "No se puede editar una solicitud en estado " + $scope.Registro.estado, "warning");
            return false;
        }
        swal.queue([{
                title: 'Advertencia',
                text: '¿Seguro que desea eliminar todos los items de la solicitud de cotización?',
                type: 'question',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        $.post(url_servidor + "Solicitud", "opcion=EliminarSolicitud&cliente=" + $scope.Registro.cliente + "&Solicitud=" + $scope.Registro.solicitud)
                                .done(function (data) {
                                    datos = data.split("|");
                                    if (datos[0] == "0") {
                                        $scope.TablaSolicitud();
                                        $.jGrowl(datos[1], {life: 1500, theme: 'growl-success', header: ''});
                                        resolve();
                                    } else {
                                        reject(datos[1]);
                                    }
                                })
                    })
                }
            }]);
    }

    $scope.BuscarRemisiom = function () {
        var remision = $.trim($scope.Remision);
        if (remision == "" || remision == $scope.Registro.remision) {
            return false;
        }
        if ($scope.Registro.remision != "")
            $scope.InicializarRegistro();
        var parametros = "opcion=BuscarRemision&remision=" + remision;
        General.LlamarAjax("Solicitud", parametros, true, "", 0).then(function (response) {
            datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                var data = JSON.parse(datos[1]);
                var sede = $.trim(data[0].idsede);
                var contacto = $.trim(data[0].idcontacto);
                $scope.Registro.documento = "";
                $scope.Remision = remision;
                $scope.Registro.remision = remision;
                $scope.Documento = data[0].documento;
                $scope.BuscarClienteSol(0);
                $scope.Registro.sede = sede;
                $scope.Registro.contacto = contacto;
                swal("", datos[2], "success");
            } else {
                $scope.Remision = "";
                $scope.Registro.remision = "";
                $("#Remision").focus();
                swal("Acción Cancelada", datos[1], "warning");
            }
        });

    }

    $("#TablaSolicitudes > tbody").on("dblclick", "tr", function () {
        var row = $(this).parents("td").context.cells;
        $scope.Solicitud = row[1].innerText;
        $scope.BuscarSolicitud(1);
        console.log($scope.Solicitud);
        $scope.setActiveTab(1);

    });



    $scope.BuscarSolicitud = function () {


        var solicitud = $scope.Solicitud * 1;
        if (solicitud == 0 || solicitud == $scope.Registro.solicitud)
            return false;
        $scope.LimpiarTodo();
        var parametros = "opcion=BuscarSolicitud&solicitud=" + solicitud;
        General.LlamarAjax("Solicitud", parametros, true, "", 1).then(function (data) {
            if (data.length > 0) {
                $scope.Registro.id = data[0].id;
                $scope.Registro.solicitud = data[0].solicitud;
                $scope.Solicitud = data[0].solicitud;
                $scope.Registro.estado = data[0].estado;
                $scope.Documento = data[0].documento;
                $scope.BuscarClienteSol($.trim(data[0].idsede), $.trim(data[0].idcontacto));
                $scope.Registro.fechareg = $.trim(data[0].fecha);
                $scope.Registro.usuarioreg = $.trim(data[0].usuario);
                $scope.Registro.observacion = $.trim(data[0].observacion);
                if (data[0].fechaanulado) {
                    Anulacion = "<span class='text-XX text-danger'>Solicitud Anulada el día: " + data[0].fechaanulado + ", por el <b>Asesor</b>: " + data[0].usuarioanula + ", <b>Observación:</b> " + data[0].observacionanula + "</span>";
                    $("#divAnulaciones").html(Anulacion);
                }

                if ($.trim(data[0].rdireccion) == "")
                    $scope.Registro.CheRecoger = false;
                else {
                    $scope.Registro.CheRecoger = true;
                }
                $scope.Registro.MRDirecion = data[0].rdireccion;
                $scope.Registro.MRContacto = data[0].rcontacto;
                $scope.Registro.MRTelefono = data[0].rtelefonos;
                $scope.Registro.MRCorreo = data[0].rcorreo;

                if ($.trim(data[0].edireccion) == "")
                    $scope.Registro.CheEntrega = false;
                else {
                    $scope.Registro.CheEntrega = true;
                }
                $scope.Registro.MEDirecion = data[0].edireccion;
                $scope.Registro.MEContacto = data[0].econtacto;
                $scope.Registro.METelefono = data[0].etelefonos;
                $scope.Registro.MECorreo = data[0].ecorreo;
                
            } else {
                $scope.Solicitud = "";
                $("#Solicitud").focus();
                swal("Acción Cancelada", "Solicitud número " + solicitud + " no registrada", "warning");
            }
        });



    }

    $("#TablaInventarioSol > tbody").on("dblclick", "tr", function (e) {
        var row = $(this).parents("td").context.cells;

        var descripcion = "CODIGO:" + row[2].innerText + ", TIPO: " + row[5].innerText + ", GRUPO: " + row[4].innerText +
                ", ARTICULO: " + row[6].innerText + ", MARCA: " + row[7].innerText + ", MODELO: " + row[8].innerText + ", PRESENTACION: " + row[9].innerText + ", CARACTERISTICA: " + row[10].innerText;

        var parametros = "opcion=AgregarItemCotizacion&Cliente=" + $scope.Registro.cliente + "&Id=0&Equipo=0&Modelo=0" +
                "&Observacion=" + descripcion + "&Serie=&Cantidad=1&Intervalo=0" +
                "&Metodo=&Punto=&Declaracion=&NombreCa=&Articulo=" + row[1].innerText +
                "&Direccion=&Certificado=&Express=&Sitio=&Solicitud=" + $scope.Registro.solicitud +
                "&Proxima=&SubContratado=&Acreditacion=";
        General.LlamarAjax("Solicitud", parametros, true, "", 0).then(function (response) {
            datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                $("#modalInventarioSol").modal("hide");
                $scope.Registro.estado = "Temporal";
                swal("", datos[1], "success");
                $scope.TablaSolicitud();
            } else
                swal("Acción Cancelada", datos[1], "warning");
        });
    });




    $("#formsolicitud").submit(function (e) {
        e.preventDefault();
        if ($scope.Registro.estado != "Temporal" && $scope.Registro.estado != "Pendiente") {
            swal("Acción cancelada", "No se puede editar una solicitud en estado " + $scope.Registro.estado, "warning");
            return false;
        }
        if ($scope.Registro.tabla.length == 0) {
            swal("Acción Cancelada", "Debe de ingresar por lo mínimo un instrumento o equipo para cotizar", "warning");
            return false;
        }
        var parametros = $("#formsolicitud").serialize() + "&IdSolicitud=" + $scope.Solicitud + "&Cliente=" + $scope.Cliente.id + "&opcion=GenerarSolicitud";
        General.LlamarAjax("Solicitud", parametros, true, "", 0).then(function (response) {
            datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                if ($scope.Solicitud == 0) {
                    $scope.Solicitud = datos[2] * 1;
                    $scope.Solicitud = datos[3] * 1;
                }



                swal("", datos[1] + " " + $scope.Solicitud, "success");
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
        });

        return false;
    });



    $scope.AgregarOtroServicio = function () {
        var mensaje = "";

        if ($.trim($scope.Servicio.Observacion) == "") {
            $("#OObservacion").focus();
            mensaje = "<br> Debe de ingresar la observación " + mensaje;
        }

        if ($scope.Servicio.Cantidad == 0) {
            $("#OCantidad").focus();
            mensaje = "<br> Debe de ingresar la cantidad del servicios " + mensaje;
        }
        if ($.trim($scope.Servicio.OtroServicio) == "") {
            $("#OServicio").focus();
            mensaje = "<br> Seleccione el servicio " + mensaje;
        }

        if ($.trim($scope.Servicio.Ingreso) == "") {
            $("#OIngreso").focus();
            mensaje = "<br> Ingrese el número de ingreso" + mensaje;
        }

        if (mensaje != "") {
            swal("Acción Cancelada", mensaje, "warning");
            return false;
        }

        console.log($scope.Servicio);

        var parametros = "opcion=AgregarItemCotizacion&Cliente=" + $scope.Registro.cliente + "&Id=" + $scope.Servicio.Id + "&Equipo=0&Modelo=0" +
                "&Observacion=" + General.escape($.trim($scope.Servicio.Observacion)) + "&Serie=&Cantidad=" + $scope.Servicio.Cantidad + "&intervalo=0" +
                "&Metodo=&Punto=&Declaracion=&NombreCa=&Articulo=0" +
                "&Direccion=&Certificado=&Express=0&Sitio=0&Solicitud=" + $scope.Registro.solicitud + "&Proxima=&SubContratado=&Acreditacion=&Servicio=" + $scope.Servicio.OtroServicio;

        General.LlamarAjax("Solicitud", parametros, true, "", 0).then(function (response) {
            datos = $.trim(response).split("|");
            if (datos[0] == "0") {

                $scope.Registro.estado = "Temporal";
                swal("", datos[1], "success");
                $scope.TablaSolicitud();
            } else
                swal("Acción Cancelada", datos[1], "warning");
        });
    }

    $scope.ModalIngreso = function () {
        if (($scope.Registro.estado == 'Temporal' || $scope.Registro.estado == "Cotizado") && ($scope.Registro.cliente > 0)) {




            $rootScope.Modal.tipo = 3;
            $rootScope.Modal.cliente = $scope.Cliente.id;
            $rootScope.Modal.tipo_ingreso = "cot";
            $rootScope.Modal.Open("Ingresos");
            $rootScope.Modal.CargarModal();
        }
    }



    $scope.ModalOServicio = function () {

        if (($scope.Registro.estado == 'Temporal' || $scope.Registro.estado == "Cotizado") && ($scope.Registro.cliente > 0)) {
            $scope.InicializarServicio();

            $("#ModalOtroServicio").modal('show');

            console.log($scope.Combos.otroservicio);
        }
    }


    $scope.CargarOIngreso = function () {
        console.log("Hola");
        if ($scope.Registro.cliente == 0)
            return false;
        if ($scope.Registro.estado != "Temporal" && $scope.Registro.estado != "Pendiente") {
            swal("Acción cancelada", "No se puede editar una solicitud en estado " + $scope.Registro.estado, "warning");
            return false;
        }
        localStorage.setItem("idcliente", $scope.Registro.cliente);
        CargarModalIngreso(6, "sol");
        $("#modalIngresoSol").modal("show");
    }

    $scope.CargarCombos();
    $scope.InicializarRegistro();
    $scope.InicializarServicio();
    $rootScope.InicializarFormulario(2);
    $scope.setActiveTab(1);
    $scope.setActiveSerTab(1);
    
});
