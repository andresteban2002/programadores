﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))
var AprobarCotiza = "";
var PermisioEspecial = PerGeneSistema("PERMISO ADMINISTRADOR LABORATORIO");

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

var FechaCal = d.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;

var IdVersion = 9;//Aqui modifique LW

$("#NumCertificado").html(CargarCombo(72, 1, "", "'DID-150'"));

var Ingreso = 0;
var PlaIngreso = 0;
var ErrorCalculo = 0;
var IdReporte = 0;
var IdInstrumento = 0;
var ErrorEnter = 0;
var Fotos = 0;
var NroReporte = 1;
var RangoHasta = 0;
var RangoDesde = 0;
var GuardaTemp = 0;
var CorreoEmpresa = "";
var AplicarTemporal = 0;
var InfoTecIngreso = 0;
var Factor = 0;
var NumCertificado = 1;
var ErrorPermitido = 0;

$("#CSerie1, #CSerie2, #CSerie3, #CSerie4, #CSerie5, #CSerie6").html(CargarCombo(40, 1, "", "5"));
console.log(CargarCombo(40, 1, "", "5"));

localStorage.setItem("Ingreso", "0");

function obtenerInformacionCertificado() {

}

function GenerarCertificado() {
    localStorage.setItem("CerIngreso", Ingreso);
    LlamarOpcionMenu('Laboratorio/PlaCertiPresion', 3, '');

}

function LimpiarTodo() {
    GuardaTemp = 0;
    LimpiarDatos();
    GuardaTemp = 1;
    $("#IngresoLong").val("");
}

function LimpiarDatos() {
    
    PlaIngreso = 0;
    ErrorCalculo = 0;
    IdRemision = 0;
    AplicarTemporal = 0;
    Ingreso = 0;
    localStorage.setItem("Ingreso", Ingreso);
    Fotos = 0
    InfoTecIngreso = 0;
    NroReporte = 1;
    ErrorPermitido = 0;
    $("#Solicitante").html("");
    $("#Certificado").html("");
    $("#Direccion").html("");
    $("#FechaIng").html("");
    $("#FechaCal").html("");

    $("#TIngreso").html("");
    $("#Medida").html("");
    $("#ValorCon").html("");
    $("#UnidadCon").html("");

    $(".medida").html("");

    $("#Equipo").html("");
    $("#Marca").html("");
    $("#Modelo").html("");
    $("#Serie").html("");
    $("#Remision").val("");
    $("#Usuario").val("");
    $("#FechaRec").val("");
    $("#Tiempo").val("");
    $("#Proxima").html("");

    $("#CMetodo").val("");
    $("#CPunto").val("");
    $("#CServicio").val("");
    $("#CObservCerti").val("");
    $("#CProxima").val("");
    $("#CEntrega").val("");
    $("#CAsesorComer").val("");

    $("#RangoMedida").html("");
    $("#MedResolucion").html("");
    RangoHasta = 0;

    $("#DivEscala").val("");
    $("#AlturaPatron").val("");
    $("#Resolucion").val("");
    $("#AlturaIBC").val("").trigger("change");
    $("#Clase").val("");
    $("#RangoPatron").val("").trigger("change");
    $("#Fluido").val("ACEITE").trigger("change");
    $("#TopeCero").val("").trigger("change");
    $("#Gliserina").val("").trigger("change");
    $("#ProximaCali").val("");

    for (var x = 1; x <= 6; x++) {
        $("#CSerie" + x).val("").trigger("change");
    }

    $("#Usuario").val("");
    $("#FechaRec").val("");
    $("#Tiempo").val("");

    $("#tbodycondicion").html("");
    
}

$("#TablaReportar > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#IngresoLong").val(numero);
    BuscarIngreso(numero,0,"0");
});

function CondicionExterna() {
    var tipo = $("#Plantilla").val() * 1;
    var resultado = "";
    var datos = LlamarAjax("Laboratorio/CambioCondicion", "ingreso=" + Ingreso + "&magnitud=5&tipo=/1/&plantilla=" + NumCertificado);
    if (datos != "[]") {
        var datacon = JSON.parse(datos);
        var valores = "";
        var opcion = 0;
        var radio = "";
        for (var x = 0; x < datacon.length; x++) {
            valores = datacon[x].opcion.split("!!");
            opcion = valores[0] * 1;
            switch (opcion) {
                case 0:
                    radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                        "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>";
                    break;
                case 1:
                    radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                        "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>";
                    break;
                case 2:
                    radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                        "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>";
                    break;
            }
            resultado += "<tr>" +
                "<td class='bg-gris'>" + datacon[x].descripcion + "</td>" + radio +
                "<td><input class='form-control sinbordecon bg-amarillo' name='CObservacion[]' value='" + valores[1] + "'>" +
                "<input type='hidden' value='" + datacon[x].id + "' name='Ids[]'><input type='hidden' value='" + datacon[x].descripcion + "' name='Descripciones[]'></td></tr>";
        }
    }
    console.log(resultado);
    $("#tbodycondicion").html(resultado);
}

function ConsularReportesIngresos() {

    ActivarLoad();
    setTimeout(function () {
        var cliente = $("#ClienteIngreso").val() * 1;
        var aprobadas = 0;
        if ($("#Aprobadas").prop("checked"))
            aprobadas = 1
        var parametros = "magnitud=5&cliente=" + cliente + "&aprobadas=" + aprobadas;
        var datos = LlamarAjax("Laboratorio/TablaReportarIngreso", parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaReportar').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "ingreso" },
                { "data": "equipo" },
                { "data": "fecha" },
                { "data": "tiempo" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

ConsularReportesIngresos();

$.connection.hub.start().done(function () {

    $('#IngresoLong').keyup(function (event) {
        numero = this.value * 1;
        if (numero == -1)
            numero = Ingreso;


        if ((Ingreso != (numero * 1)) && ($("#Equipo").html() != "")) {
            GuardaTemp = 1;
            LimpiarDatos();
            Ingreso = 0;
            ErrorEnter = 0;
        }
        if (event.which == 13) {
            event.preventDefault();

            if (Ingreso == numero)
                return false;
            BuscarIngreso(numero);
        }
    });
});

function BuscarIngreso(numero) {
    var revision = $("#Revision").val() * 1;
    NumCertificado = $("#NumCertificado").val() * 1;//modifque esto LW
    if (ErrorEnter == 0) {
        if (numero * 1 > 0) {
            GuardaTemp = 0;
            var datos = LlamarAjax("Laboratorio/BuscarIngresoOperacion", "magnitud=5&ingreso=" + numero + "&medida=mm&revision=" + revision + "&version=" + IdVersion).split("|");
            if (datos[0] != "[]") {
                var data = JSON.parse(datos[0]);

                if (data[0].idmagnitud != "5") {
                    swal("Acción Cancelada", "El ingreso número " + numero + " no pertenece a la magnitud <br><b>DIMENSIONAL</b>", "warning");
                    ErrorEnter = 1;
                    $("#IngresoLong").select();
                    $("#IngresoLong").focus();
                    return false;
                }
                
                IdReporte = 0;
                NumReporte = 0;
                if (datos[1] != "[]") {
                    var datarep = JSON.parse(datos[1]);

                    for (var x = 0; x < datarep.length; x++) {
                        NumReporte = datarep[x].nroreporte;
                        if (datarep[x].nroreporte * 1 == 0) {
                            GuardaTemp = 0;
                            IdReporte = datarep[x].id;
                            $("#Concepto").val(datarep[x].idconcepto).trigger("change");
                            $("#Ajuste").val(datarep[x].ajuste);
                            $("#Suministro").val(datarep[x].suministro);
                            $("#Observacion").val(datarep[x].observaciones);
                            $("#Conclusion").val(datarep[x].conclusion);
                            
                            $("#registroingreso").html("<h3>Registrado el día " + datarep[x].fecha + " por el técnico " + datarep[x].usuario + "</h3>");
                        }
                        if (datarep[x].idconcepto == 3) {
                            InfoTecIngreso = 1;
                            break;
                        }
                    }
                }

                IdRemision = data[0].id * 1;
                Ingreso = data[0].ingreso * 1;
                localStorage.setItem("Ingreso", Ingreso);
                Fotos = data[0].fotos * 1;
                $("#Solicitante").html(data[0].cliente);
                $("#Certificado").html("PT-" + data[0].contador + "-" + Anio);
                $("#Direccion").html(data[0].direccion);
                $("#FechaIng").html(data[0].fechaing);
                $("#FechaCal").html(FechaCal);
                CorreoEmpresa = data[0].email;
                
                $("#TIngreso").html(data[0].ingreso);
                $("#Medida").html(data[0].medida);
                $("#ValorCon").html(data[0].medidacon);
                Factor = data[0].medidacon;
                $("#UnidadCon").html("1 " + data[0].medida + " =");

                $(".medida").html(data[0].medida);

                $("#Equipo").html(data[0].equipo);
                $("#Marca").html(data[0].marca);
                $("#Modelo").html(data[0].modelo);
                $("#Proxima").html(data[0].proxima);
                $("#Serie").html(data[0].serie);
                $("#Remision").val(data[0].remision);
                InfoTecIngreso = data[0].informetecnico;
                                
                $("#Metodo").html(CargarCombo(17, 1, "", data[0].idequipo));
                $("#Metodo").val(data[0].metodo_cot).trigger("change");


                CondicionExterna();
                                
                
                $("#RangoMedida").html("(" + data[0].desde + " a " + data[0].hasta + ") " + data[0].medida);
                $("#MedResolucion").html(data[0].medida);
                RangoHasta = data[0].hasta*1;
                RangoDesde = data[0].desde * 1;

                $("#PrLectura").val(formato_numero(RangoHasta,0,".",",",""));

                $("#DivEscala").val((data[0].division_escala ? data[0].division_escala : ""));
                $("#AlturaPatron").val((data[0].altura_patron ? data[0].altura_patron : ""));
                $("#Resolucion").val((data[0].resolucion ? data[0].resolucion : ""));
                $("#AlturaIBC").val((data[0].altura_ibc ? data[0].altura_ibc : ""));
                $("#Clase").val((data[0].clase ? data[0].clase : ""));
                $("#Fluido").val((data[0].fluido_usado ? data[0].fluido_usado : "ACEITE")).trigger("change");
                $("#TopeCero").val((data[0].tope_cero ? data[0].tope_cero : "")).trigger("change");
                $("#Gliserina").val((data[0].glicerina ? data[0].glicerina : "")).trigger("change");
                IdInstrumento = (data[0].idinstrumento ? data[0].idinstrumento : 0);
                                                                
                $("#Usuario").val(data[0].usuarioing);
                $("#FechaRec").val(data[0].fecharec);
                $("#Tiempo").val(data[0].tiempo);

                //MODAL DE COTIZACIONES

                if (datos[3] != "[]") {
                    var dataco = JSON.parse(datos[3]);
                    for (var x = 0; x < dataco.length; x++) {
                        if (x == 0) {
                            $("#CMetodo").val(dataco[x].metodo);
                            $("#CPunto").val(dataco[x].punto);
                            $("#CServicio").val(dataco[x].servicio);
                            $("#CObservCerti").val(dataco[x].observacion);
                            $("#CProxima").val(dataco[x].proxima);
                            $("#CEntrega").val(dataco[x].entrega);
                            $("#CAsesorComer").val(dataco[x].asesor);
                        } else {
                            $("#CMetodo").val("<br>" + dataco[x].metodo);
                            $("#CPunto").val("<br>" + dataco[x].punto);
                            $("#CServicio").val("<br>" + dataco[x].servicio);
                            $("#CObservCerti").val("<br>" + dataco[x].observacion);
                            $("#CProxima").val("<br>" + dataco[x].proxima);
                            $("#CEntrega").val("<br>" + dataco[x].entrega);
                            $("#CAsesorComer").val("<br>" + dataco[x].asesor);
                        }
                    }
                } else {
                    ErrorEnter = 1;
                    $("#IngresoLong").select();
                    $("#IngresoLong").focus();
                    swal("Acción Cancelada", "Este ingreso no ha sido cotizado por el asesor comercial", "warning");
                    if (PermisioEspecial == 0) {
                        LimpiarDatos();
                        return false;
                    }
                }
                
                                                
            } else {
                $("#IngresoLong").select();
                $("#IngresoLong").focus();
                ErrorEnter = 1;
                swal("Acción Cancelada", "El Ingreso número " + numero + " no se ha recibido en el laboratorio", "warning");
                return false;
            }
        }
    } else
        ErrorEnter = 0;
}


function CambioPatron(patron, fila) {
    fila = fila * 1;
    patron = patron * 1;
    if (patron  == 0) {
        $("#CTitulo" + fila).html("");
        $("#CInstrumento" + fila).html("");
        $("#CMarca" + fila).html("");
        $("#CGrado" + fila).html("");
        $("#CIntervalo" + fila).html("");
        $("#CCertificado" + fila).html("");
        $("#CFechaCal" + fila).html("");
        $("#CId" + fila).val("0");
        return false;
    }

    for (var x = 1; x <= 6; x++) {
        if (x != fila) {
            if (($("#CId" + x).val() * 1) == patron) {
                $("#CSerie" + fila).val("").trigger("change");
                $("#CSerie" + fila).focus();
                swal("Acción Cancelada", "Este patron ya fue seleccionado", "warning");
                return false;
            }
        }
    }

    var datos = LlamarAjax("Laboratorio/DatosGenPatron", "patron=" + patron);
    var data = JSON.parse(datos);
    $("#CTitulo" + fila).html(data[0].titulo);
    $("#CInstrumento" + fila).html(data[0].descripcion);
    $("#CMarca" + fila).html(data[0].marca);
    $("#CGrado" + fila).html($.trim(data[0].grado) == "" ? "NO APLICA" : data[0].grado);
    $("#CIntervalo" + fila).html(data[0].intervalop);
    $("#CCertificado" + fila).html(data[0].certificado);
    $("#CFechaCal" + fila).html(data[0].fechacertificado);
    $("#CId" + fila).val(data[0].id);
}

$("#FormPrevia").submit(function (e) {
    e.preventDefault();

    var id = document.getElementsByName("Ids[]");
    var observacion = document.getElementsByName("CObservacion[]");
    var concepto = $("#Concepto").val() * 1;
    var suministro = $.trim($("#Suministro").val());
    var ajuste = $.trim($("#Ajuste").val());
                
    if (concepto == 2) {
        if (suministro == "" && ajuste == "") {
            $("#Ajuste").focus();
            swal("Acción Cancelada", "Debe ingresar el ajusto ó suministro", "warning");
            return false;
        }
    }

    var a_id = "";
    var a_observacion = "";
    var a_opcion = "";
    var opcion = 0;

    for (var x = 0; x < id.length; x++) {
        if ($("#CSi" + x).prop("checked"))
            opcion = 1;
        else {
            if ($("#CNo" + x).prop("checked"))
                opcion = 2
            else {
                if ($("#CNA" + x).prop("checked"))
                    opcion = 3
            }
        }
        if (a_id == "") {
            a_id += id[x].value;
            a_observacion = observacion[x].value + "";
            a_opcion += opcion;
        } else {
            a_id += "|" + id[x].value;
            a_observacion += "|" + observacion[x].value + "";
            a_opcion += "|" + opcion;
        }
    }
    var patron = "";
    for (var i = 1; i <= 6; i++) {
        if ($('#CSerie' + i).val() !== null && $('#CSerie' + i).val() !== "") {
            patron += $('#CSerie' + i).val() + ",";
        }        
    }
    var parametros = $("#FormPrevia").serialize().replace(/\%2C/g, "") + "&a_id=" + a_id + "&a_observacion=" + a_observacion + "&a_opcion=" + a_opcion + "&idinstrumento=" + IdInstrumento +
        "&id=" + IdReporte + "&plantilla=" + $("#NumCertificado").val() + "&revision=" + $("#Revision").val() + "&idversion=" + IdVersion+ "&patron=" + patron;
    
    console.log(parametros);
    var datos = LlamarAjax("Laboratorio/GuarOperPrevLongitud", parametros).split("|");
    console.log(datos);
    if (datos[0] == "0") {
        IdReporte = datos[2];
        IdInstrumento = datos[3];
        GuardaTemp = 0;
        localStorage.removeItem("Pre" + NumCertificado + "-" + Ingreso);
        localStorage.removeItem("Table" + NumCertificado + "-" + Ingreso)
        localStorage.removeItem("Puntos" + NumCertificado + "-" + Ingreso)
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});
    
function VerFotos() {
    if (Ingreso == 0)
        return false;
    CargarModalFoto(Ingreso, Fotos,1);
}


function ModalCotizacion() {
    if (Ingreso == 0)
        return false;
    $("#modalcotizacion").modal("show");
}

function CalcularError(Caja) {
    ValidarTexto(Caja, 3);
    var medida = $(".medida").html();
    ErrorPermitido = 0
    if (Ingreso > 0 && Caja.value * 1 > 0) {
        var datos = $.trim(LlamarAjax("Laboratorio/CalcularErrorLongitud", "division=" + Caja.value + "&medida=" + medida + "&lecturamax=" + RangoHasta));        
        if (datos == "XX")
            ErrorPermitido = null;
        else {
            ErrorPermitido = datos*1;
            $("#ErrorPositivo").val(ErrorPermitido);
            $("#ErrorNegativo").val(ErrorPermitido * -1);
        }
    }
}

function ReportarEquipo() {
    if (Ingreso == 0)
        return false;
    var datos = LlamarAjax("Laboratorio/BuscarReportesLab", "ingreso=" + Ingreso);
    if (datos != "[]") {
        var datarep = JSON.parse(datos);
        for (var x = 0; x < datarep.length; x++) {
            IdReporte = datarep[x].nroreporte * 1;
            $("#IdReporte" + IdReporte).val(datarep[x].id);
            $("#Concepto" + IdReporte).val(datarep[x].idconcepto).trigger("change");
            $("#Ajuste" + IdReporte).val(datarep[x].ajuste);
            $("#Suministro" + IdReporte).val(datarep[x].suministro);
            $("#Observacion" + IdReporte).val(datarep[x].observaciones);
            $("#Conclusion" + IdReporte).val(datarep[x].conclusion);
            $("#NroInforme" + IdReporte).val(datarep[0].informetecnico)
            $("#registroingreso" + IdReporte).html("<b>Registrado el día " + datarep[x].fecha + " por el técnico " + datarep[x].usuario + "</b>");
            if (datarep[x].idconcepto * 1 == 5 || datarep[x].idconcepto * 1 == 3)
                MostrarFoto(IdReporte, 1);
        }
    }
    $('#tabreportaringreso a[href="#Reporte1"]').tab('show')
    NroReporte = 1;
    $("#ReportarEquipo").modal("show");
    $("#Concepto1").focus();
}

function CambioConcepto(concepto, tipo) {
    var prefijo = (tipo == 0 ? "" : tipo);

    $("#Suministro" + prefijo).prop("disabled", true);
    $("#Ajuste" + prefijo).prop("disabled", true);
    $("#Conclusion" + prefijo).prop("disabled", true);
    $("#Observacion" + prefijo).prop("disabled", false);

    $("#Observacion" + prefijo).addClass("bg-amarillo");
    $("#Ajuste" + prefijo).removeClass("bg-amarillo");
    $("#Suminstro" + prefijo).removeClass("bg-amarillo");
    $("#Conclusion" + prefijo).removeClass("bg-amarillo");

    $("#Documento" + prefijo).removeClass("bg-amarillo");
    $("#NumFoto" + prefijo).removeClass("bg-amarillo");
    $("#Documento" + prefijo).prop("disabled", true);
    $("#NumFoto" + prefijo).prop("disabled", true);

    if (concepto == "2") {
        $("#Suministro" + prefijo).prop("disabled", false);
        $("#Ajuste" + prefijo).prop("disabled", false);
        $("#Observacion" + prefijo).prop("disabled", true);
        $("#Conclusion" + prefijo).prop("disabled", true);

        $("#Observacion" + prefijo).removeClass("bg-amarillo");
        $("#Ajuste" + prefijo).addClass("bg-amarillo");
        $("#Suministro" + prefijo).addClass("bg-amarillo");
        $("#Conclusion" + prefijo).removeClass("bg-amarillo");

    } else {
        if (concepto == "3" || concepto == "5") {
            $("#Suministro" + prefijo).prop("disabled", true);
            $("#Ajuste" + prefijo).prop("disabled", true);
            $("#Observacion" + prefijo).prop("disabled", false);
            $("#Conclusion" + prefijo).prop("disabled", false);

            $("#Observacion" + prefijo).addClass("bg-amarillo");
            $("#Ajuste" + prefijo).removeClass("bg-amarillo");
            $("#Suministro" + prefijo).removeClass("bg-amarillo");
            $("#Conclusion" + prefijo).addClass("bg-amarillo");

            $("#Documento" + prefijo).addClass("bg-amarillo");
            $("#NumFoto" + prefijo).removeClass("bg-amarillo");
            $("#Documento" + prefijo).prop("disabled", false);
            $("#NumFoto" + prefijo).prop("disabled", false);
        }
    }

    if (tipo == 0)
        GuardarTemporal();

}

DesactivarLoad();
$('select').select2();
