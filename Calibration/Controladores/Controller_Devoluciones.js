angular.module('myApp',['ui', 'ngSanitize', 'ngTable']).controller('ControlDevoluciones', function ($scope, $ocLazyLoad, $rootScope, General, NgTableParams) {
	
    function initTabs() {
        tabClasses = ["", ""];
    }

    $scope.getTabClass = function (tabNum) {
        return tabClasses[tabNum];
    };

    $scope.getTabPaneClass = function (tabNum) {
        return "tab-pane " + tabClasses[tabNum];
    }

    $scope.setActiveTab = function (tabNum) {
        initTabs();
        tabClasses[tabNum] = "active";
    };
    
    if ($rootScope.global.movil == 0){
		Webcam.set({
			width: 650,
			height: 510,
			dest_width: 650,
			dest_height: 510,
			image_format: 'jpeg',
			jpeg_quality: 100
		});
	}else{
		Webcam.set({
			width: 400,
			height: 480,
			dest_width: 780,
			dest_height: 960,
			image_format: 'jpeg',
			jpeg_quality: 100
		});           
	}
	
	Webcam.attach('#camaraweb');
    
    
    $scope.Documento = "";
    $scope.DocumentoCli = "";
    $scope.Devolucion = "";
    $scope.CorreoEmpresa="";
    
    
    
    
     $scope.Cliente = {
        id: 0,
        documento: ""
    }
    
     $scope.Combos = {
        sede: "",
        contacto: "",
        magnitud: "",
        bequipo: "",
        equipo: "",
        marca: "",
        ingreso: "",
        medida: "",
        cliente: "",
        modelo: "",
        bmodelo: "",
        bintervalo: "",
        intervalo: "",
        pais: "",
        departamento: "",
        ciudad: "",
        usuario: "",
        accesorio: "",
        bproveedor: "",
        proveedor: "",
        moneda: "",
        observacion: "",
        grupo: ""

    }
    
    
    
    
     $rootScope.ObtenerDatatable = function (data) {
          $scope.Documento = $.trim(data.documento);
          $scope.BuscarCliente();
          $rootScope.Modal.Close();
     }
    
    
    
     $scope.ActualizarCombos = function (tipo) {
        var clase = "";
        switch (tipo) {
            case 1:
                clase = "ComboRegistro";
                break;
            case 2:
                clase = "ComboDevolucion";
                break;
            case 3:
                clase = "ComboCliente";
                break;
        }
        setTimeout(function () {
            $("." + clase).trigger("change");
        }, 200);
    }
    
    
    
     $scope.InicializarRegistro = function () {
        $scope.Registro = {
            id: 0,
            cliente: 0,
            devolucion:"",
            sede: "",
            empresaenv: "",
            contacto:"",
            sitio:"",
            fotodev: "",
            fotoentrega:"",
            contacto: "",
            observaciones: "",
            Pdf_Devolucion:"",
            sitio:"NO",
            devolucion:"",
            usuario: "",
            fechareg: "",
            totales: "",
            usuarioreg: "",
            observaciones:"",
            aentrega:"",
            btnguardar: true,
            tabla: {},
            tequipo: 0
        }
    }
    
    
    
      $scope.InicializarDevolucion = function () {
        $scope.Devolucion = {
            id: 0,
            magnitud: "",
            equipo: "",
            marca: "",
            modelo: "",
            cliente: 0,
            contacto: "",
            sede:"",
            observacion: "",
            remision: "",
            cantidad: "1",
            cotizacion: "",
            serie: "",
            ingreso: "",
            tcantidad: 0,
            totales:"",
            robservacion: "",
            rpersona:"",
            rfecha:"",
            rguia:"",
            rempresa:""
      
        }

        $scope.ActualizarCombos(2);
    }



 $scope.CargarCombos = function () {
     var parametros = "opcion=CargaComboInicial&tipo=2&json=1";
        General.LlamarAjax("Configuracion", parametros, true, "",1).then(function (response){
        var datos = $.trim(response).split("||");
            sede= "",
            contacto="",
            $scope.Combos.magnitud= JSON.parse(datos[0]);
            $scope.Combos.bequipo= JSON.parse(datos[1]);
            $scope.Combos.equipo= JSON.parse(datos[31]);
            $scope.Combos.marca= JSON.parse(datos[2]);
            $scope.Combos.medida= JSON.parse(datos[4]);
            $scope.Combos.cliente= JSON.parse(datos[5]);
            modelo= "";
            $scope.Combos.bmodelo= JSON.parse(datos[6]);
            $scope.Combos.bintervalo= JSON.parse(datos[7]);
            intervalo="",
            $scope.Combos.pais= JSON.parse(datos[10]);
            $scope.Combos.departamento= JSON.parse(datos[11]);
            $scope.Combos.ciudad= JSON.parse(datos[12]);
            $scope.Combos.usuario= JSON.parse(datos[13]);
            $scope.Combos.accesorio= JSON.parse(datos[14]);
            $scope.Combos.observacion= JSON.parse(datos[15]);
        
    });
    }    
    
    
     $scope.Consulta= {
        devolucion:"",
        cliente: "",
        ingreso: "",
        magnitud:"",
        equipo:"",
        sede: "",
        marca: "",
        modelo: "",
        intervalo: "",
        serie: "",
        entregado:"",
        usuario: "",
        fechad: {
            value: new Date(output + " 00:00"),
            id: output
        },
        fechah: {
            value: new Date(output2 + " 00:00"),
            id: output2
        }
    }
    
    
     $scope.BuscarCliente = function () {
        var documento = $.trim($scope.Documento);
        if ($.trim(documento) == $scope.Cliente.documento || $.trim(documento) == "") {
            return false;
        }
        $scope.LimpiarTodo();
        $scope.Documento = documento;

        var parametros = "opcion=BuscarCliente&documento=" + documento + "&bloquear=1&json=1";
        General.LlamarAjax("Cotizacion", parametros, true, "", 0).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                var data = JSON.parse(datos[1]);
                $scope.Cliente = data[0];
                $scope.Combos.contacto = JSON.parse(datos[2]);
                $scope.Combos.sede = JSON.parse(datos[3]);
                $scope.Registro.cliente = $scope.Cliente.id;
                $scope.TablaTemporalDevolucion();
                console.log($scope.Cliente);
            }
            });  
        }
    
    
    $scope.BuscarDevolucion= function() {
        var numero = $.trim($scope.Devolucion);
    if ($scope.Registro.devolucion == numero || numero == "")
        return false;
    $scope.LimpiarTodo();
    if ((numero * 1) == 0)
        return false;



    var parametros = "opcion=BuscarDevolucion&devolucion=" + numero;
    General.LlamarAjax('Cotizacion',parametros,true, "",0)
    var Anulacion = "";

    if (datos != "[]") {
        var data = JSON.parse(datos);
        $scope.Registro.id  = data[0].id;
        Devolucion = data[0].devolucion;
        $scope.Registro.entregado = data[0].entregado;
        $scope.Registro.sede = data[0].idsede;
        $scope.Registro.empresaenv = data[0].idempresa;
        $scope.Registro.fotodev = data[0].fotos * 1;
        $scope.Registro.fotoentrega = data[0].fotoentrega*1;

        if (data[0].fechaanulacion) {
            Anulacion = "<span class='text-XX text-danger'>Devolución Anulada el día: " + data[0].fechaanulacion + ", por el <b>Asesor</b>: " + data[0].usuarioanula + ", <b>Observación:</b> " + data[0].observacionanula + "</span>";
            $("#divAnulaciones").html(Anulacion);
        }
       
                
        $scope.Devolucion.rpersona.val((data[0].nombreentrega ? data[0].nombreentrega : ""));
        $scope.Devolucion.robservacion.val((data[0].observacionentrega ? data[0].observacionentrega : ""));
        $scope.Devolucion.rfecha.val((data[0].fechaentrega ? data[0].fechaentrega : ""));
        $scope.Devolucion.rguia.val((data[0].fechaentrega ? data[0].guia : ""));
        $scope.Devolucion.rempresa.val((data[0].fechaentrega ? data[0].empresa_envia : ""));

        $scope.Registro.contacto = $.trim(data[0].idcontacto);
        $scope.Registro.cliente.val(data[0].documento);
        $scope.BuscarCliente(data[0].documento);
        $scope.Registro.sede=$.trim.val(IdSede);
        $scope.Registro.contacto=$trim.val(IdContacto);
        $scope.Registro.sitio.val(data[0].sitio);
        $scope.Registro.devolucion.val(Devolucion);
        $scope.Registro.fechareg.val(data[0].fecha);
        $scope.Registro.usuarioreg.val(data[0].usuario);
        $scope.Registro.observaciones.val(data[0].observacion);
        $("#seleccionatodo").prop("checked", "checked");
        $("#Devolucion").attr("disabled", true);
        
        SeleccionarTodo();

        SeguimientoCartera(numero);

    } else {
        $("#Devolucion").focus();
        swal("Acción Cancelada", "Devolución número " + numero + " no registrada", "warning");
    }
}
    
    
    
       $scope.DatosContacto = function (contacto) {
        $scope.Cliente.email = "";
        $scope.Cliente.telefono = "";
        $scope.Cliente.celular = "";

        General.LlamarAjax("Cotizacion", "opcion=DatosContacto&id=" + contacto, false, "", 1).then(function (response) {
            if (response.length > 0) {
                $scope.Cliente.email = response[0].email;
                $scope.Cliente.telefono = response[0].telefonos;
                $scope.Cliente.celular = response[0].fax;
            }
        });

    }
    
     $scope.ValidarCliente = function (event) {
        var documento = $.trim($scope.Documento);
        if (documento == "") {
            return false;
        }
        if (documento != $scope.Cliente.documento) {
            if ($scope.Cliente.documento != "") {
                $scope.LimpiarDatos();
            }
            if (event.keyCode == 13) {
                $("#Sede").focus();
            }
        }
    }
    
    
     $scope.LimpiarTodo = function () {
        $scope.Documento = "";
        $scope.DocumentoCli = "";
        $scope.Ingreso = "";
        $scope.LimpiarDatos();
    }

    $scope.LimpiarDatos = function () {
        $scope.Cliente = {documento: ""};
        $scope.InicializarRegistro();
        $scope.Combos.sede = "";
        $scope.Combos.contacto = "";
    }





$scope.ConsultarDevoluciones = function () {
        if ($scope.Consulta.fechad.id == "" || $scope.Consulta.fechah.id == "") {
            swal("Acción Cancelada", ValidarTraduccion("Debe de ingresar fecha de inicio y fecha final"), "warning");
            return false;
        }
        var parametros = "opcion=ConsultarDevoluciones&" + General.ConvertirParametros($scope.Consulta);
        General.LlamarAjax("Cotizacion", parametros, true, "", 1).then(function (response) {
            $rootScope.TablaDevoluciones = new NgTableParams({count: 10}, {dataset: response});

        });
    }



  $scope.TablaTemporalDevolucion = function () {
        var parametros = "opcion=TemporalDevolucion&cliente=" + $scope.Registro.cliente + "&devolucion=" + $scope.Registro.devolucion + "&sitio=" + $scope.Registro.sitio;
        General.LlamarAjax("Cotizacion",parametros,true,"",1).then(function (response) {
        var datos = $.trim(response).split("|");
        if (datos[0] == "0") {
        $scope.Registro.tabla = JSON.parse(datos[1]);
        console.log($scope.Registro.tabla);
        if (datos[1] != "[]") {
             if ($scope.Registro.sede == "") {
                        $scope.Registro.sede = $.trim($scope.Registro.tabla[0].idsede);
                        $scope.Registro.contacto = $.trim($scope.Registro.tabla[0].idcontacto);
                    }
                }
            }
     });
    }
    
    
    
    $scope.SeleccionarTodo= function() {

    if ($("#seleccionatodo").prop("checked")) {
        
        for (var x = 0; x < totales; x++) {
            if ($("#seleccionado" + x).prop("disabled") == false) {
                $("#fila-" + x).addClass("bg-gris");
                $("#seleccionado" + x).prop("checked", "checked");
            }
        }
    } else {
        for (var x = 0; x < $scope.Devolucion.totales; x++) {
            $("#fila-" + x).removeClass("bg-gris");
            $("#seleccionado" + x).prop("checked", "");
        }
    }
}










$scope.ImprimirDevolucion = function (numero,tipo) {
        $scope.Registro.Pdf_Devolucion = "";
        if (numero * 1 != 0) {
            var parametros = "opcion=GenerarPDF&tipo=Remision&documento=" + numero;
            General.LlamarAjax("Configuracion", parametros, true, "", 1).then(function (response) {
                var datos = $.trim(response).split("||");
                if (datos[0] == "0") {
                    if (tipo == 0)
                        window.open($rootScope.Conexion.url_archivo + "DocumPDF/" + datos[1]);
                    else {
                        $scope.Registro.Pdf_Devolucion = datos[1];
                    }
                } else {
                    swal("Acción Cancelada", datos[1], "warning");
                }
            });
        }

    }




$scope.GuardarDevolucion=function() {
    var sede = $("#Sede").val() * 1;
    var contacto = $("#Contacto").val() * 1;
    var mensaje = "";

    if (IdCliente == 0)
        return false;

    if (contacto == 0) {
        $("#Contacto").focus();
        mensaje = "<br> Debe de seleccionar un contacto del cliente"
    }
    if (sede == 0) {
        $("#Sede").focus();
        mensaje = "<br> Debe de seleccionar una sede del cliente " + mensaje;
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var seleccionado = 0;
    var observaciones = $.trim($("#Observaciones").val());
    var ingresos = document.getElementsByName("seleccion[]");
    var observacion = document.getElementsByName("Observaciones[]");
    var certificado = document.getElementsByName("Certificados[]");
    var informe = document.getElementsByName("Informes[]");
    var sitio = $("#Sitio").val();
    var a_ingreso = "array[";
    var a_observacion = "array[";
    var a_certificado = "array[";
    for (var x = 0; x < ingresos.length; x++) {
        if (ingresos[x].checked) {
            if ($.trim(observacion[x].value) == "") {
                swal("Acción Cancelada", "Debe de ingresar la observación del ingreso número " + ingresos[x].value, "warning");
                return false;
            }
            if ($.trim(certificado[x].value) == "" && $.trim(informe[x].value) == "") {
                swal("Acción Cancelada", "Debe de ingresar el certificado ó informe del ingreso número " + ingresos[x].value, "warning");
                return false;
            }
            if (a_ingreso == "array[") {
                a_ingreso += ingresos[x].value;
                a_observacion += "'" + $.trim(observacion[x].value) + "'";
                a_certificado += "'" + $.trim(certificado[x].value) + "'"; 
            } else {
                a_ingreso += "," + ingresos[x].value;
                a_observacion += ", '" + $.trim(observacion[x].value) + "'"; 
                a_certificado += ",'" + $.trim(certificado[x].value) + "'"; 
            }
            seleccionado += 1;
        }
    }
    a_ingreso += "]";
    a_observacion += "]";
    a_certificado += "]";

    if (seleccionado == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un ingreso", "warning");
        return false;
    }
    var mensaje = "";
    var parametros = "id=" + IdDevolucion + "&cliente=" + IdCliente + "&a_ingreso=" + a_ingreso + "&a_observacion=" + a_observacion + "&observacion=" + observaciones +
        "&sede=" + sede + "&contacto=" + contacto + "&a_certificados=" + a_certificado + "&sitio=" + sitio;
    var datos = LlamarAjax("Cotizacion", "opcion=GuardarDevolucion&"+parametros).split("|");
    if (datos[0] == 0) {
        Devolucion = datos[2] * 1;
        ImprimirDevolucion(datos[2], 0);
        if (Enviar_Certificado == "SI") {
            EnviarCertificado();
            mensaje = "<br><b>Debe de enviar el certificado por correo</b>";
        }
        swal("", datos[1] + " " + datos[2] + mensaje, "success");
        LimpiarTodo();
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}




$scope.VerFotosDev= function() {
	$scope.CargarModalFoto(Devolucion, 0, 2);
}



$scope.AnularDevolucion= function() {

    if ($scope.Devolucion == 0)
        return false;

    var resultado = "";
    swal({
        title: 'Advertencia',
        text: '¿' + $rootScope.ValidarTraduccion('Desea Anular la Devolución') + ' ' + $scope.Devolucion + ' ' + $rootScope.ValidarTraduccion('del cliente') + ' ' + $("#NombreCliente").val(),
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: $rootScope.ValidarTraduccion('Si'),
        cancelButtonText: $rootScope.ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var parametros= "opcion=AnularDevolucion&id=" + IdDevolucion + "&observaciones=" + value;
                    General.LlamarAjax("Cotizacion",parametros,true,"",1).then(function (response) {
                    var datos = $.trim(response).split("|");
                    if (datos[0] == "0") {
                        resultado = Devolucion;
                        resolve()
                    
                    } else {
                        reject(datos[1]);
                    }
                    });
                } else {
                    reject(ValidarTraduccion('Debe de ingresar un correo electrónico'));
                }
            })
        }
    }).then(function (result) {
        LimpiarTodo();
        IdCliente = 0;
        $("#Cliente").val("");
        $("#Cliente").focus();
        swal({
            type: 'success',
            html: 'Devolución Nro ' + resultado + ' anulada con éxito'
        })
    })
    

}



$scope.CambiarFoto2= function(numero) {
    $("#FotoDevolucion2").attr("src", url_cliente + "imagenes/Devoluciones/" + $scope.Devolucion + "/" + numero + ".jpg?id=" + NumeroAleatorio());
}


//Webcam.attach('#camaraweb'); 

$scope.CambioFecha = function (consulta, tipo) {
        switch (consulta) {
            case 1:
                switch (tipo) {
                    case 1:
                        $scope.Consulta.fechad.id = General.Obtener_Fecha($scope.Consulta.fechad.value);
                        break;
                    case 2:
                        $scope.Consulta.fechah.id = General.Obtener_Fecha($scope.Consulta.fechah.value);
                        break;
                }
                break;
        }

    }

$scope.CargarFotos= function() {
    ganeral.LlamarAjax("Cotizacion","opcion=CantidadFotoDev&devolucion=" + $scope.Devolucion) * 1;
    var contenedor = "";
    if (fotos > 0) {
        contenedor = "<div class='row'><div class='col-xs-12 col-sm-12'><label>Número de Foto</label><select id='ComboFoto' class='form-control text-center' style='width:120px' onchange='CambiarFoto2(this.value)'> ";
        for (var x = 1; x <= fotos; x++) {
            contenedor += "<option value='" + x + "'>" + x + "</option>";
        }
        contenedor += "</select></div><hr/><br><div class='col-xs-12 col-sm-12 text-center'>" +
            "<img id='FotoDevolucion2' src='" + url_cliente + "imagenes/Devoluciones/" + $scope.Devolucion + "/1.jpg?id=" + NumeroAleatorio() + "' width='90%'></div></div>";
    }
    $("#fotocamdevo").html(contenedor);
}




/*$scope.EliminarFotoDev= function() {
    
	ActivarLoad();
	setTimeout(function () {
                var parametros="opcion=EliminarFotoDev&devolucion=" + $scope.Devolucion;
		General.LlamarAjax("Cotizacion",parametros,true,"",0).then(function (response) {
                var datos = $.trim(response).split("|");
		if (datos[1] != "X") {
			Fotos = datos[1]*1;
			CargarFotos();
			//CambiarFoto2(datos[1]);
			$("#ComboFoto").val(datos[1]).trigger("change");
                
		}else{
               
                swal("Acción Cancelada",datos[0],"warning");   
		});
                }
            }
        }  
 }
 
*/        

$scope.EntregarDevolucion= function() {
    var saldo = SaldoActual($scope.Registro.cliente, 0);
    var gestion = $("#IdMenSeguiCart").val() * 1;
    if (saldo > 0 && RetirarSinFac == "NO" && PermisioEspecial == 0 && gestion == 0) {
        swal("Acción Cancelada", "No se puede entregar los equipos porque el Cliente posee un saldo de <b>" + formato_numero(saldo, 0,_CD,_CM,"") + "</b> por facturas vencidas... Diríjase a cartera para gestionar el pago", "warning");
        return false
    }

    $("#ACliente").val($("#NombreCliente").val());
    $("#ASede").val($("#Sede option:selected").text());
    $("#AContacto").val($("#Contacto option:selected").text());
    $("#ADevolucion").val(Devolucion);
    $("#AAsesor").val($("#UsuarioReg").val());
    $("#AFecha").val($("#FechaReg").val());

    $("#AEntrega").val("");
    $("#AObservacion").val("");
    
    if ($scope.Registro.sede == "") {
                    $("#Sede").focus();
                    swal("Acción Cancelada", "Debe seleccionar la sede del cliente", "warning");
                    return false;
                }
                if ($scope.Registro.contacto == "") {
                    $("#Contacto").focus();
                    swal("Acción Cancelada", "Debe seleccionar el contacto del cliente", "warning");
                    return false;
                }
    

    $("#entregar_devolucion").modal("show");
}


 $scope.ModalEntregarDevolucion = function () {
        setTimeout(function () {
                if ($scope.Registro.sede == "") {
                    $("#Sede").focus();
                    swal("Acción Cancelada", "Debe seleccionar la sede del cliente", "warning");
                    return false;
                }
                if ($scope.Registro.contacto == "") {
                    $("#Contacto").focus();
                    swal("Acción Cancelada", "Debe seleccionar el contacto del cliente", "warning");
                    return false;
                }
                //$scope.LimpiarServicio();
                $("#entregar_devolucion").modal({backdrop: 'static'}, 'show');
                /*setTimeout(function () {
                 $("#Magnitud").focus();
                 }, 500);*/
        }, 15);
    };




 $scope.ModalClientes = function () {
        $rootScope.Modal.cedula = "";
        $rootScope.Modal.razonsocial = "";
        $rootScope.Modal.tipo = 2;
        $rootScope.Modal.Open("Clientes");
    }


$scope.EnviarCertificado=function () {
    if ($scope.Devolucion == 0)
        return false;
    var saldo = SaldoActual($scope.Registro.cliente, 0);
    var gestion = $("#IdMenSeguiCart").val() * 1;
    if (saldo > 0 && RetirarSinFac == "NO" && gestion == 0) {
        swal("Acción Cancelada", "No se puede entregar los equipos porque el Cliente posee un saldo de <b>" + formato_numero(saldo, 0,_CD,_CM,"") + "</b> por facturas vencidas... Diríjase a cartera para gestionar el pago", "warning");
        return false
    }
    ArchivosPDF = "";
    var cantidad = 0;
    var resultado = '<div class="tabbable page-tabs">' +
        '<ul class="nav nav-tabs">';
    var resultado2 = "";

    if (CorreoEmpresa == "") {
        swal("Acción Cancelada", "Cliente no posee ningún correo principal registrado", "warning");
        return false;
    }

    var ingresos = document.getElementsByName("seleccion[]");
    var informe = document.getElementsByName("Informes[]");
    var certificadopdf = document.getElementsByName("Certificadospdf[]");
    var certificado = document.getElementsByName("Certificados[]");
    a_Certificados = "";
    var valor = "";
    var valor2 = "";

    var datos;
    var datacer;
    var impreso;
    var numcertificado;
    
    for (var x = 0; x < ingresos.length; x++) {
        if (ingresos[x].checked) {
            if (informe[x].value * 1 == 0) {
                arreglo = $.trim(certificadopdf[x].value) + "//";
                arreglo = arreglo.split("//");
                arreglo2 = $.trim(certificado[x].value) + "/";
                arreglo2 = arreglo2.split("/");
                for (var y = 0; y < arreglo.length; y++) {
                    valor = $.trim(arreglo[y]);
                    valor2 = $.trim(arreglo2[y]);
                    if (valor2 != "") {
                        datos = LlamarAjax("Cotizacion","opcion=TipoCertificado&Certificado=" + valor2);
                        if (datos != "[]") {
                            datacer = JSON.parse(datos);
                            impreso = datacer[0].impreso * 1;
                            numcertificado = datacer[0].numero;
                            if (impreso == 1)
                                valor = numcertificado;
                            if (cantidad == 0) {

                                resultado += '<li class="active"><a href="#tabcertificado' + x + '" data-toggle="tab"><i class="icon-checkbox-partial"></i>' + valor2 + '</a></li>'
                                resultado2 += '<div class="tab-pane active fade in" id="tabcertificado' + x + '">'
                                ArchivosPDF = valor;
                                a_Certificados = valor2 + ".pdf";
                            } else {
                                resultado += '<li><a href="#tabcertificado' + x + '" data-toggle="tab"><i class="icon-checkmark3"></i>' + valor2 + '</a></li>';
                                resultado2 += '<div class="tab-pane fade in" id="tabcertificado' + x + '">'
                                ArchivosPDF += "," + valor;
                                a_Certificados += "," + valor2 + ".pdf";
                            }
                            cantidad++;
                            resultado2 += '<iframe src="' + url_cliente + 'Adjunto/Certificados/' + valor + '.pdf?id=' + NumeroAleatorio() + '" width="100%" height="350px"></iframe>'
                            resultado2 += "</div>";
                        }
                    }
                }
            }
        }
    }

    if (cantidad == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un certificado", "warning");
        return false;
    }

    resultado = resultado + '</ul><div class="tab-content">' + resultado2 + '</div></div>';

    $("#detcertificados").html(resultado);

    $("#CeCliente").val($("#NombreCliente").val());
    $("#CeCorreo").val($("#Email").val());
    $("#CObservacion").val("")
    
    $("#cespremision").html(Devolucion);
    $("#enviar_certificado").modal("show");
}


$("#formenviardevolucion").submit(function (e) {

    e.preventDefault();

    var correo = $.trim($("#eCorreo").val());
    a_correo = correo.split(";");
    var observacion = $("#ObserEnvio").val();

    if (Devolucion == 0)
        return false;

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#eCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo onválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "devolucion=" + Devolucion + "&principal=" + CorreoEmpresa + "&e_email=" + correo + "&archivo=" + Pdf_Remision + "&observacion=" + observacion;
        var datos = LlamarAjax("Cotizacion","opcion=EnvioDevolucion&" + parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + CorreoEmpresa + " " + correo, "success");
            $("#enviar_devolucion").modal("hide");
        } else
            swal("Error", datos[1], "error");
    }, 15);

    return false;
})

$("#formenviarcertificado").submit(function (e) {

    e.preventDefault();

    var correo = $.trim($("#CeCorreo").val());
    a_correo = correo.split(";");
    var observacion = $("#CObserEnvio").val();

    if (Devolucion == 0)
        return false;

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#CeCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo onválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    setTimeout(function () {
        var parametros = "opcion=EnvioCertificado&devolucion=" + Devolucion + "&principal=" + CorreoEmpresa + "&e_email=" + correo + "&archivos=" + ArchivosPDF + "&pdf=" + a_Certificados + "&observacion=" + observacion;
        General.LlamarAjax("Cotizacion", parametros,true,"",);
        var datos = $.trim(response).split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + CorreoEmpresa + " " + correo, "success");
            $("#enviar_devolucion").modal("hide");
        } else
            swal("Error", datos[1], "error");
    });

    return false;
})



$scope.LlamarImprimir = function (numero) {
        $scope.Devolucion="";
        $scope.ImprimirDevolucion(numero, tipo, 0);
    }



$scope.EnviarDevolucion= function() {

    if ($scope.Devolucion == 0)
        return false;

    
    if ($scope.CorreoEmpresa == "") {
        swal("Acción Cancelada", "Cliente no posee ningún correo principal registrado", "warning");
        return false;
    }

    $("#eCliente").val($("#NombreCliente").val());
    $("#eCorreo").val($("#Email").val());
    ImprimirDevolucion(Devolucion, 1);

    $("#spremision").html(Devolucion);
    $("#enviar_devolucion").modal("show");
}



$scope.GuardarDevolucion= function() {
    var sede = $("#Sede").val() * 1;
    var contacto = $("#Contacto").val() * 1;
    var mensaje = "";

    if ($scope.Devolucion.cliente == 0)
        return false;

    if ($scope.Devolucion.contacto == 0) {
        $("#Contacto").focus();
        mensaje = "<br> Debe de seleccionar un contacto del cliente"
    }
    if ($scope.Devolucion.sede == 0) {
        $("#Sede").focus();
        mensaje = "<br> Debe de seleccionar una sede del cliente " + mensaje;
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var seleccionado = 0;
    var observaciones = $.trim($("#Observaciones").val());
    var ingresos = document.getElementsByName("seleccion[]");
    var observacion = document.getElementsByName("Observaciones[]");
    var certificado = document.getElementsByName("Certificados[]");
    var informe = document.getElementsByName("Informes[]");
    var sitio = $("#Sitio").val();
    var a_ingreso = "array[";
    var a_observacion = "array[";
    var a_certificado = "array[";
    for (var x = 0; x < ingresos.length; x++) {
        if (ingresos[x].checked) {
            if ($.trim(observacion[x].value) == "") {
                swal("Acción Cancelada", "Debe de ingresar la observación del ingreso número " + ingresos[x].value, "warning");
                return false;
            }
            if ($.trim(certificado[x].value) == "" && $.trim(informe[x].value) == "") {
                swal("Acción Cancelada", "Debe de ingresar el certificado ó informe del ingreso número " + ingresos[x].value, "warning");
                return false;
            }
            if (a_ingreso == "array[") {
                a_ingreso += ingresos[x].value;
                a_observacion += "'" + $.trim(observacion[x].value) + "'";
                a_certificado += "'" + $.trim(certificado[x].value) + "'"; 
            } else {
                a_ingreso += "," + ingresos[x].value;
                a_observacion += ", '" + $.trim(observacion[x].value) + "'"; 
                a_certificado += ",'" + $.trim(certificado[x].value) + "'"; 
            }
            seleccionado += 1;
        }
    }
    a_ingreso += "]";
    a_observacion += "]";
    a_certificado += "]";

    if (seleccionado == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un ingreso", "warning");
        return false;
    }
    var mensaje = "";
    var parametros = "id=" + $scope.Devolucion.id + "&cliente=" + $scope.Devolucion.cliente + "&a_ingreso=" + a_ingreso + "&a_observacion=" + a_observacion + "&observacion=" + observaciones +
        "&sede=" + sede + "&contacto=" + contacto + "&a_certificados=" + a_certificado + "&sitio=" + sitio;
    var datos = LlamarAjax("Cotizacion", "opcion=GuardarDevolucion&"+parametros).split("|");
    if (datos[0] == 0) {
        Devolucion = datos[2] * 1;
        ImprimirDevolucion(datos[2], 0);
        if (Enviar_Certificado == "SI") {
            EnviarCertificado();
            mensaje = "<br><b>Debe de enviar el certificado por correo</b>";
        }
        swal("", datos[1] + " " + datos[2] + mensaje, "success");
        LimpiarTodo();
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}



$scope.GuardarDocumento=function(id) {
    var documento = document.getElementById(id);
    if ($.trim(documento.value) != "") {
		var archivo = GuardarDocumentoAdjunto(id,4,'');
        if (archivo == "" || !archivo){
			swal("Acción Cancelada", "Error al adjuntar la imagen", "warning");
			return false;
		}
	}
}


$scope.GuardarEntDevolucion= function() {
    var persona = $("#AEntrega option:selected").text();
    var fecha = $("#AFechaEnt").val();
    var archivo = $("#AArchivo").val();
    var empresa = $("#AEmpresaEnv").val() * 1;
    var guia = $.trim($("#AGuia").val());
    
    var observacion = $("#AObservacion").val();

    if ($scope.Registro.persona == "--Seleccione--") {
        $("#AEntrega").focus();
        swal("Acción Cancelada", "Debe de ingresar la persona que se el entrega la devolución", "warning");
        return false;
    }

    if (persona == "ENVIO") {
        if (empresa == "" && guia == "") {
            $("#AEmpresaEnv").focus();
            swal("Acción Cancelada", "Debe de ingresar la empresa y guía de envío", "warning");
            return false;
        }
    }

    if (fecha == "") {
        $("#AFechaEnt").focus();
        swal("Acción Cancelada", "Debe de ingresar la fecha de entrega", "warning");
        return false;
    }

    if (archivo == "") {
        $("#AArchivo").focus();
        swal("Acción Cancelada", "Debe de examinar la devolución firmada", "warning");
        return false;
    }

    var datos = LlamarAjax("Cotizacion","opcion=EntregarDevolucion&devolucion=" + Devolucion + "&persona=" + persona + "&observacion=" + observacion + "&fecha=" + fecha +
        "&empresa=" + empresa + "&guia=" + guia).split("|");
    if (datos[0] == "0") {
        $("#entregar_devolucion").modal("hide");
        $("#RPersona").val(persona);
        $("#RObservacion").val(observacion);
        $("#RFecha").val(datos[2]);
        $("#RGuia").val(guia);
        IdEmpresaEnv = empresa;
        $("#REmpresa").val($("#AEmpresaEnv option:selected").text());
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}



/*$scope.GuardarFotoDevo= function(archivo) {
    ActivarLoad();
	setTimeout(function () {
		var parametros = "opcion=SubirFoto&archivo=" + archivo + "&devolucion=" + Devolucion + "&tipo_imagen=3";
		General.LlamarAjax("Configuracion",parametros,true,"",0).then(function (response) {
                var datos = $.trim(response).split("|");
		if (data[0] == "0"){
			$scope.CargarFotos();
			$("#ComboFoto").val(data[3]);
		}else{
			swal("Acción Cancelada",data[1],"warning");   
                    }
	});
    }
    }
}
 */   



$scope.ImprimirCertificado= function(numero) {
    window.open(url_archivo + "Adjunto/Certificados/" + numero + ".pdf?id=" + NumeroAleatorio());
}



/*$scope.ImprimirEtiqueta= function() {
    if ($scope.Devolucion * 1 != 0) {
        setTimeout(function () {
            var parametros= "opcion=GenerarPDF&tipo=EtiquetaDevolucion&documento=0&devolucion=" + Devolucion;
            General.LlamarAjax("Configuracion",parametros, true,"",1 ).then(function (response) {
           var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                window.open(url_archivo + "DocumPDF/" + datos[1]);
            } else {
                swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
            }
  
    });
}

}
*/


$scope.InicializarCamaraDev= function() {
    if (Devolucion == 0)
        return false;

    if ($("#RPersona").val() != "") {
        swal("Acción Cancelada", "Esta devolución ya fue entregada", "warning");
        return false;
    }

    $scope.CargarFotos();
    $("#FotoNumDevolucion").html(Devolucion);
    $("#modal_camara_web").modal("show");
}



$scope.SeleccionarFila= function(x) {
    
    if ($("#seleccionado" + x).prop("checked")) {
        $("#fila-" + x).addClass("bg-gris");
    } else {
        $("#fila-" + x).removeClass("bg-gris");
    }

}



$scope.TomarFotoDev= function() {
    Webcam.snap(function (data_uri) {
        //var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
        GuardarFotoDevo(data_uri);
    });
}




$scope.VerCertificado= function(certificado) {
    window.open(url_archivo + "Adjunto/Certificados/" + certificado + ".pdf?id=" + NumeroAleatorio());
}




$scope.VerCertificados= function(ingreso) {
    $("#titulodetalle").html("CERTIFICADO DEL INGRESO NRO " + ingreso);
    
    var datos = LlamarAjax("Laboratorio","opcion=DetalleIngreso&opciones=6&ingreso=" + ingreso);
    $("#detallemovimiento").html(datos);

    $("#modalDetalleDevo").modal("show");
    
}



$scope.VerEstadoCuenta= function() {
    var cliente = $("#NombreCliente").val();
    EstadoCuenta(IdCliente, cliente, CorreoEmpresa, TipoCliente, PlazoPago, CorreoEmpresa);
}




/*$scope.VerGuia= function() {
    var guia = $("#RGuia").val();
    if (guia == "") {
        return false;
    }
    var parametros = "opcion=PagEmpresaEnvia&id=" + IdEmpresaEnv;
    General.LlamarAjax("Cotizacion",parametros,true,"",0).then(function (response) {
    var pagina = $.trim(response).split("|");
        setTimeout(function () {
        window.open(pagina + guia);
    j
        }
    });
}
*/


$scope.VerDevolucionFirma= function() {
    if ($scope.Devolucion.rpersona == "") {
        return false;
    }

    
    if ($scope.Registro.fotoentrega > 0) {
        swal("Acción Cancelada", "Esta devolución fue entregada por la móvil", "warning");
        return false;
    }

    window.open(url_archivo + "Adjunto/imagenes/DevoFirma/" + $scope.Devolucion + ".pdf?id=" + NumeroAleatorio());
}


/*$scope.GuardarFotoDevo= function(archivo) {
	setTimeout(function () {
		var parametros = "opcion=SubirFoto&archivo=" + archivo + "&devolucion=" + Devolucion + "&tipo_imagen=3";
		General.LlamarAjax("Configuracion",parametros,true,"",0).then(function (response) {
                var data = $.trim(response).split("|"); 
		if (data[0] == "0"){
			$scope.CargarFotos();
			$("#ComboFoto").val(data[3]).trigger("change");
			 });
		}else{
			
			swal("Acción Cancelada",data[1],"warning");
                   
                    }
                }
            }
*/
   
    $scope.CargarCombos();
    $scope.InicializarRegistro();
    $scope.InicializarDevolucion();
    $rootScope.InicializarFormulario(1);
    $scope.setActiveTab(1);
});
