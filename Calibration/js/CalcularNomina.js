﻿var PeriodoActual = 0;
var NominaCalculada = "";
var NominaCerrada = "";

$("#Nomina").html(CargarCombo(79, 1));

function LimpiarTodo() {

}

function CambioPeriodo(nomina) {
    $("#Periodo").html("");
    $("#Empleados").html("");
    LimpiarTodo();
    if (nomina * 1 == 0)
        return false;
    $("#Periodo").html(CargarCombo(28, 1, "", nomina));
    PeriodoActual = LlamarAjax("Recursohumano","opcion=PeriodoActual&nomina=" + nomina) * 1;
    $("#Periodo").val(PeriodoActual).trigger("change");
}

function BuscarNomina(periodo) {
    PeriodoActual = periodo * 1;
    $("#Empleados").html(CargarCombo(78, 1, "", PeriodoActual));
    LimpiarTodo();
    LlenarTablas();

}

function CalcularNomEmpleado(empleado) {
    $("#TBCalEmpleadoNom").html("");
    if (empleado * 1 == 0 || PeriodoActual == 0)
        return false;
    var datos = LlamarAjax("Recursohumano","opcion=CalNominaEmpleado&empleado=" + empleado + "&periodo=" + PeriodoActual);
    var data = JSON.parse(datos);
    var resultado = "";
    var asignaciones = 0;
    var deducciones = 0;
    
    for (var x = 0; x < data.length; x++) {
        resultado += "<tr>" +
            "<td>" + data[x]._numero + "</td>" +
            "<td>" + data[x]._codigo + "</td>" +
            "<td>" + data[x]._concepto + "</td>" +
            "<td>" + data[x]._formula + "</td>" +
            "<td>" + data[x]._formulaapo + "</td>" +
            "<td class='text-right text-XX2'>" + formato_numero(data[x]._asignaciones, 0,_CD,_CM,"") + "</td>" +
            "<td class='text-right text-XX2'>" + formato_numero(data[x]._deducciones, 0,_CD,_CM,"") + "</td>" +
            "<td class='text-right text-XX2'>" + formato_numero(data[x]._aporteemp, 0,_CD,_CM,"") + "</td>" +
            "<td class='text-right text-XX2'>" + formato_numero(data[x]._aportepat, 0,_CD,_CM,"") + "</td></tr>";
        asignaciones += data[x]._asignaciones;
        deducciones += data[x]._deducciones + data[x]._aporteemp;
    }
    $("#TBCalEmpleadoNom").html(resultado);
    $("#EAsignaciones").val(formato_numero(asignaciones, 0,_CD,_CM,""));
    $("#EDeducciones").val(formato_numero(deducciones, 0,_CD,_CM,""));
    $("#ETotalPagar").val(formato_numero(asignaciones - deducciones, 0,_CD,_CM,""));
}

function CalcularNomina() {
    if (PeriodoActual == 0)
        return false;
    if (NominaCalculada == "SI") {
        swal("Acción Cancelada", "Esta nómina ya se encuentra calculada", "warning");
        return false;
    }
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Recursohumano","opcion=CalcularNomina&periodo=" + PeriodoActual).split("|");
        DesactivarLoad();
        if (datos[0] == "0") {
            swal("", datos[1], "success");
            LlenarTablas();
            $("#Empleados").trigger("change");
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    }, 15);
}

function CerrarPeriodo() {

    if (PeriodoActual == 0)
        return false;
    if (NominaCalculada == "NO") {
        swal("Acción Cancelada", "Esta nómina no se encuentra calculada", "warning");
        return false;
    }
    if (NominaCerrada == "SI") {
        swal("Acción Cancelada", "Este periodo ya se encuentra cerrado", "warning");
        return false;
    }

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea cerrar el periodo de esta nómina?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor +  "Recursohumano","opcion=CerrarPeriodo&periodo=" + PeriodoActual)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            nomina = $("#Nomina").val();
                            CambioPeriodo(nomina);
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

function AbrirPeriodo() {

    if (PeriodoActual == 0)
        return false;
    if (NominaCalculada == "NO") {
        swal("Acción Cancelada", "Esta nómina no se encuentra calculada", "warning");
        return false;
    }
    if (NominaCerrada == "NO") {
        swal("Acción Cancelada", "Este periodo no se encuentra cerrado", "warning");
        return false;
    }

    var nomina = $("#Nomina").val() * 1;

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea abrir el periodo de esta nómina?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Recursohumano","opcion=AbrirPeriodo&periodo=" + PeriodoActual + "&nomina=" + nomina)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            $("#TEstado").val("NÓMINA CALCULADA");
                            NominaCerrada = "NO";
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

function ReversarNomina() {
    if (PeriodoActual == 0)
        return false;
    if (NominaCalculada == "NO") {
        swal("Acción Cancelada", "Esta nómina no se encuentra calculada", "warning");
        return false;
    }
    if (NominaCerrada == "SI") {
        swal("Acción Cancelada", "No se puede reversar una nómina cerrada", "warning");
        return false;
    }
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Recursohumano","opcion=ReversarNomina&periodo=" + PeriodoActual).split("|");
        DesactivarLoad();
        if (datos[0] == "0") {
            swal("", datos[1], "success");
            LlenarTablas();
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    }, 15);
}

function LlenarTablas() {
    NominaCalculada = "NO";
    NominaCerrada = "NO";

    var asignacion = 0;
    var deduccion = 0;
    var aporteemp = 0;
    var aportepat = 0;

        
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Recursohumano","opcion=ResumenNomina&periodo=" + PeriodoActual).split("|");
        DesactivarLoad();
                        
        var datajson = JSON.parse(datos[0]);
        var table = $('#TablaResuConcepto').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "orden" },
                { "data": "codigo" },
                { "data": "concepto" },
                { "data": "formula" },
                { "data": "formulaaporte" },
                { "data": "asignacion", "className": "text-right text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "deduccion", "className": "text-right text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "aporteemp", "className": "text-right text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "aportepat", "className": "text-right text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });

        table.rows().data().each(function (datos) {

            asignacion += datos.asignacion;
            deduccion += datos.deduccion;
            aporteemp += datos.aporteemp;
            aportepat += datos.aportepat;
        });


        $("#TAsignacion").val(formato_numero(asignacion, 0,_CD,_CM,""));
        $("#TDeduccion").val(formato_numero(deduccion, 0,_CD,_CM,""));
        $("#TTotalPagar").val(formato_numero(asignacion - deduccion - aporteemp, 0,_CD,_CM,""));
        $("#TAporteEmp").val(formato_numero(aporteemp, 0,_CD,_CM,""));
        $("#TAportePat").val(formato_numero(aportepat, 0,_CD,_CM,""));
                
        var datajson = JSON.parse(datos[1]);
        var tableemp = $('#TablaResuEmpleado').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "documento" },
                { "data": "empleado" },
                { "data": "cargo" },
                { "data": "sueldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "asignacion", "className": "text-right text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "deduccion", "className": "text-right text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "aporteemp", "className": "text-right text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "aportepat", "className": "text-right text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "totalpagar", "className": "text-right text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });

        
        $("#TEmpleados").val(formato_numero(tableemp.data().count(), 0,_CD,_CM,""));
        if (datos[2] != "[]") {
            var data = JSON.parse(datos[2]);
            NominaCalculada = data[0].calculado;
            NominaCerrada = data[0].cerrado;
        }
        if (NominaCalculada == "SI") {
            $("#TEstado").val("NÓMINA CALCULADA");
            if (NominaCerrada == "SI")
                $("#TEstado").val("NÓMINA CALCULADA Y CERRADA");
        } else {
            $("#TEstado").val("NÓMINA SIN CALCULAR");
        }
    }, 15);
}

function AdjuntarAsistencia() {
    if (PeriodoActual == 0)
        return false;
    if (NominaCalculada == "SI") {
        swal("Acción Cancelada", "No se puede modificar una nómina calculada", "warning");
        return false;
    }
}

function GuardarDocumento() {
    var documento = document.getElementById("documento");
    if ($.trim(documento.value) != "") {
        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Cotizacion/AdjuntarExcel";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "error");
                }
            }
        });
    }
}

function AdjuntarAsistencia() {

    if (PeriodoActual == 0)
        return false;

    if (NominaCalculada == "SI") {
        swal("Acción Cancelada", "No se puede modificar una nómina calculada", "warning");
        return false;
    }

    swal.queue([{
        html: '<h3>' + ValidarTraduccion("Seleccione el archivo de texto de asistencia") + '</h3><input type="file" class="form-control" id="documento" onchange="GuardarDocumento()" name="documento" accept="text/*" required />',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Importar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                var datos = LlamarAjax("Recursohumano","opcion=ImportarAsistencia&periodo=" + PeriodoActual).split("|");
                if (datos[0] == "0") {
                    swal(datos[1]);
                    resolve();
                }
                else
                    reject(datos[1]);
            })
        }
    }]);
}


function Imprimir(tipo, empleado, concepto) {
    if (NominaCalculada == "NO") {
        swal("Acción Cancelada", "Esta nómina no se encuentra calculada", "warning");
        return false;
    }
    if (empleado == -1) {
        empleado = $("#Empleados").val() * 1;
        if (empleado == 0)
            return false
    }
    var parametros = "";
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=" + tipo + "&documento=0&periodo=" + PeriodoActual + "&empleado=" + empleado + "&concepto=" + concepto).split("||");
        DesactivarLoad();
        if (datos[0] == "0") {
            window.open(url_archivo + "DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

$('select').select2();
DesactivarLoad();
