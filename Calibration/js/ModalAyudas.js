﻿document.write('\
\
    <div id="modalayudas" class="modal fade" tabindex="-1" role="dialog">\
    <div class="modal-dialog modal-lg">\
        <div class="modal-content">\
            <div class="modal-header bg-info">\
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
                <h4 class="modal-title"><i class="icon-accessibility"></i> <span id="moduloayuda">Prueba de Modulo</span></h4>\
            </div>\
            <div class="modal-body with-padding">\
                <video width="100%" id="archivoayuda" controls autoplay loop muted preload="auto">\
            </div>\
        </div>\
    </div>\
</div>\
    ');

function LlamarModalAyudas(Modulo) {
    var titulo = "";
    var archivo = url + "Ayuda/Video/";
    switch (Modulo) {
        
        case 10:
            titulo = "Registro de Ingresos";
            archivo += "Remision.mp4";
            break;
        case 11:
            archivo += "Fotos por Ingresos.mp4";
            titulo = "Fotos por Ingreso";
            break;
        case 12:
            titulo = "Recibir Ingreso";
            archivo += "Recibir Ingreso Logistica.mp4";
            break;
        case 13:
            archivo += "Devolucion.mp4";
            titulo = "Devoluciones";
            break;
        case 14:
            titulo = "Fotos por Devoluciones";
            archivo += "Foto Devolucion.mp4";
            break;
        case 15:
            archivo += "Fotos por Ingresos.mp4";
            titulo = "Ubicación del Equipo";
            break;
        case 16:
            archivo += "Foto Ingresos.mp4";
            titulo = "Fotos por Ingreso Celular";
            break;
        case 17:
            archivo += "Fotos por Ingresos.mp4";
            titulo = "Fotos por Ingreso Camara";
            break;
       

        case 20:
            titulo = "Solicitud";
            archivo += "Solicitud Cotizacion.mp4";
            break;
        case 21:
            archivo += "Cotizacion por Solicitud.mp4";
            titulo = "Cotizaciones por solicitud";
            break;
        case 22:
            titulo = "Cotizaciones externas";
            archivo += "Cotizacion Externa.mp4";
            break;
        case 23:
            archivo += "Reemplazar Cotizacion.mp4";
            titulo = "Reemplazar Cotización";
            break;
        case 24:
            titulo = "Órden de Compra del Cliente";
            archivo += "Orden Compra Cliente.mp4";
            break;
        case 25:
            archivo += "Salida y Entrada Equipos.mp4";
            titulo = "Salida y Entrada de Equipo";
            break;

        case 30:
            titulo = "Recibir Ingreso";
            archivo += "Recibir Ingreso Laboratorio.mp4";
            break;
        case 31:
            archivo += "Reportar Ingresos.mp4";
            titulo = "Reportar Ingreso";
            break;
        case 32:
            titulo = "Certificados";
            archivo += "Certificados.mp4";
            break;
        case 33:
            archivo += "Reemplazar Cotizacion.mp4";
            titulo = "Consultar Reportes Cotizados";
            break;
        case 34:
            titulo = "Operaciones Previas Par Torsional";
            archivo += "Operaciones Previas Par Torsional.mp4";
            break;
        case 35:
            archivo += "Operaciones Previas Par Presion.mp4";
            titulo = "Operaciones Previas Presión";
            break;
        case 36:
            archivo += "Certificados Par Torsional.mp4";
            titulo = "Certificado Par Torsional";
            break;
        case 37:
            archivo += "Certificados Presion.mp4";
            titulo = "Certificados Presión";
            break;

        case 40:
            archivo += "Relacionar Remision.mp4";
            titulo = "Relacionar Remisón";
            break;

        case 50:
            archivo += "Informe Tecnico.mp4";
            titulo = "Informes Técnicos";
            break;
        case 51:
            archivo += "Informe Magnitudes.mp4";
            titulo = "Informes por las Magnitudes Técnicas";
            break;

        case 60:
            archivo += "Certificados Presion.mp4";
            titulo = "Programación de Diligencias";
            break;
        
        case 70:
            archivo += "Recibir Ingreso para Tercerizar.mp4";
            titulo = "Recibir Ingreso en Comercial";
            break;
        case 71:
            archivo += "Orden Compra Tercerizar.mp4";
            titulo = "Registro de Orden de Compra a Proveedores";
            break;

        case 80:
            archivo += "Facturacion.mp4";
            titulo = "Facturación";
            break;
        case 81:
            archivo += "Certificados Presion.mp4";
            titulo = "Reporte de Facturas";
            break;

        case 90:
            archivo += "Recibo Caja.mp4";
            titulo = "Recibo de Caja";
            break;
        case 91:
            archivo += "Gestion Cartera.mp4";
            titulo = "Gestión de Cartera";
            break;
        case 92:
            archivo += "Reporte Cartera.mp4";
            titulo = "Reporte de Cartera";
            break;

        case 100:
            archivo += "Nomina.mp4";
            titulo = "Nóminas";
            break;

        case 110:
            archivo += "Crear Clientes.mp4";
            titulo = "Configuración de Clientes";
            break;
        case 111:
            archivo += "Crear Proveedores.mp4";
            titulo = "Configuración de Proveedores";
            break;

        case 120:
            archivo += "Seguridad.mp4";
            titulo = "Seguridad";
            break;

        case 130:
            archivo += "Trazabiilidad Ingreso.mp4";
            titulo = "Trazabilidad del Ingreso";
            break;
        case 131:
            archivo += "Consulta Movimientos.mp4";
            titulo = "Movimientos del Ingreso";
            break;
    }
    
    $("#archivoayuda").attr("src", archivo);
    $("#moduloayuda").html(titulo);

    $("#modalayudas").modal("show");
}

