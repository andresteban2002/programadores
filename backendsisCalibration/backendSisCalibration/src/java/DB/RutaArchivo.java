package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.sql.ResultSet;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/RutaArchivo"})

public class RutaArchivo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
        
    protected void Servidor(HttpServletRequest request, HttpServletResponse response) {
        String Archivo = "";
        try {

            response.setContentType("text/html;charset=UTF-8");
            response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
            response.setHeader("Access-Control-Allow-Credentials", "true");
            PrintWriter out = response.getWriter();
            if (Globales.url_db != null){
                out.print("0||");
            }else{
                String Ruta = RutaArchivo.class.getProtectionDomain().getCodeSource().getLocation().toString().replace("build/web/WEB-INF/classes/DB/RutaArchivo.class", "").replace("file:", ""); // traigo dirreccion
                Ruta = Ruta.replace("domain1/applications/backendSisCalibration/WEB-INF/classes/DB/RutaArchivo.class", "");
                Ruta = Ruta.replaceAll("\\\\", "/");      
                Ruta = Ruta.replace("%20", " "); 
                Archivo = Ruta + "conexion.json";
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(new FileReader(Archivo));
                if (obj != null){
                    JSONObject jsonObject = (JSONObject) obj;

                    String ip = (String) jsonObject.get("ip");
                    String puerto = (String) jsonObject.get("puerto");
                    String base_datos = (String) jsonObject.get("base_datos");
                    String clave = (String) jsonObject.get("clave");
                    String usuario = (String) jsonObject.get("usuario");

                    Globales.url_db = "jdbc:postgresql://" + ip + ":" + puerto + "/" + base_datos;
                    Globales.usuario_db = usuario;
                    Globales.contrasena_db = clave;
                    
                    String sql = "select usa_contabilidad, url_archivo, ruta_archivo, ruta_gestion, ruta_respaldo, ruta_media_linux, smtp_host, " +
                                 " smtp_port, razonsocial " +
                                 " from empresa WHERE principal = 1";
                    ResultSet datos = Globales.Obtenerdatos(sql, "RutaArchivo", 1);
                    if (datos.next()){
                        Globales.url_archivo = datos.getString("url_archivo");
                        Globales.ruta_archivo = datos.getString("ruta_archivo");
                        Globales.ruta_gestion = datos.getString("ruta_gestion");
                        Globales.ruta_respaldo = datos.getString("ruta_respaldo");
                        Globales.ruta_media_linux = datos.getString("ruta_media_linux");
                        Globales.smtp_host = datos.getString("smtp_host");
                        Globales.smtp_port = datos.getString("smtp_port");
                        Globales.Nombre_Empresa = datos.getString("razonsocial");
                        Globales.usa_contabilidad  = datos.getString("usa_contabilidad");
                    }else{
                        out.print("1||No se ha definido una empresa principal");
                    }
                                     
                    

                    out.print("0||" + Archivo);
                }else{
                    out.print("1||Debe de configurar el archivo de conexión a la base de datos||" + Archivo);
                }
            }
        } catch (Exception ex) {
            try {
                PrintWriter out = response.getWriter();
                out.print("1||" + ex.getMessage() + "||" + Archivo);
            } catch (IOException exl) {
                Globales.GuardarLogger(exl.getMessage(), RutaArchivo.class.getName());
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

}
