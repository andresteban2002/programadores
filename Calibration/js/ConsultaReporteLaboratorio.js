d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))
var CorreoEmpresa;
var Empresa;

output = d.getFullYear() + '-' +
	(month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
	(month < 10 ? '0' : '') + month + '-' +
	(day < 10 ? '0' : '') + day;

$("#CRFechaDesde").val(output);
$("#CRFechaHasta").val(output2);

$("#CRMagnitud").html(CargarCombo(1, 1));
$("#CREquipo").html(CargarCombo(2, 1));
$("#CRMarca").html(CargarCombo(3, 1));
$("#CRCliente").html(CargarCombo(9, 1));
$("#CRModelo").html(CargarCombo(5, 1, "", "-1"));
$("#CRIntervalo").html(CargarCombo(6, 1, "", "-1"));
$("#CRUsuario, #CAsesor").html(CargarCombo(13, 1));
	
$("#TablaReportes > tbody").on("dblclick", "tr", function (e) {
	var row = $(this).parents("td").context.cells;
	var id = row[21].innerText;
	var datos = LlamarAjax("Laboratorio","opcion=DatosReporte&id=" + id).split("|");
	if (datos[0] == "0") {
		var data = JSON.parse(datos[1]);
		$("#AIngreso").val(data[0].ingreso);
		$("#AContacto").val(data[0].contacto);
		$("#ACliente").val(data[0].cliente);
		$("#ADireccion").val(data[0].sede);
		$("#AAjuste").val(data[0].ajuste);
		$("#ASuministro").val(data[0].suministro);
		$("#AObservacion").val(data[0].observacion);
		$("#IdReporte").val(data[0].idreporte);

		$("#OpcionAproRepor").val("1").trigger("change");
		$("#UsuarioRepor").val("");
		$("#CedulaRepor").val("");
		$("#CargoRepor").val("");
		$("#ObservacionRepor").val("");
		
		$("#modalAprobarRepor").modal("show");
	} else {
		swal("Acción Cancelada", datos[1], "warning");
	}
		

});

function EnvioAjuste(ingreso, reporte, principal, cliente, correocontacto, plantilla, magnitud) {
	CorreoEmpresa = principal;
	Empresa = cliente;
	$("#NroReporte").val(reporte);
	$("#spcotizacion").html(ingreso);
	$("#eCliente").val(cliente);
	$("#ePlantilla").val(plantilla);
	$("#eMagnitud").val(magnitud);
	$("#eCorreo").val(principal + (correocontacto != "" ? ";" + correocontacto : ""));
	$("#ObserEnvio").val("");
	$("#enviar_reporte").modal("show");
}

$("#formenviarreporte").submit(function (e) {

	e.preventDefault();
	var observacion = $.trim($("#ObserEnvio").val())
	var correo = $.trim($("#eCorreo").val());
	var ingreso = $("#spcotizacion").html();
	a_correo = correo.split(";");
	var reporte = $("#NroReporte").val() * 1;
	var plantilla = $("#ePlantilla").val();
	var magnitud = $("#eMagnitud").val()*1;
	for (var x = 0; x < a_correo.length; x++) {
		if (ValidarCorreo($.trim(a_correo[x])) == 2) {
			$("#eCorreo").focus();
			swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo inválido") + ":" + a_correo[x], "warning");
			return false;
		}
	}
	var version = 0;
	if (magnitud == 6)
		version = 7;
	if (magnitud == 2)
		version = 6;
	ActivarLoad();
	setTimeout(function () {
		var parametros = "ingreso=" + ingreso + "&reporte=" + reporte + "&principal=" + CorreoEmpresa + " &e_email=" + correo + " &cliente=" + Empresa + " &observacion=" + observacion + "&plantilla=" + plantilla + "&magnitud=" + magnitud + "&version=" + version;
		var datos;
		if (reporte == 0)
			datos = LlamarAjax("Laboratorio/EnvioCotizacion", parametros);
		else
			datos = LlamarAjax("Laboratorio/EnvioReporte", parametros);
		DesactivarLoad();
		datos = datos.split("|");
		if (datos[0] == "0") {
			swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
			$("#enviar_reporte").modal("hide");
		} else
			swal("Acción Cancelada", datos[1], "warning");
	}, 15);

	return false;
});


function ConsularReportes() {
	var magnitud = $("#CRMagnitud").val() * 1;
	var equipo = $("#CREquipo").val() * 1;
	var modelo = $("#CRModelo").val() * 1;
	var marca = $("#CRMarca").val() * 1;
	var intervalo = $("#CRIntervalo").val() * 1;
	var ingreso = $("#CRIngreso").val() * 1;
	var serie = $.trim($("#CRSerie").val());
	var asesor = $("#CAsesor").val() * 1;
	var noaprobado = 0;
	var enviado = 0;

	if ($("#CNAprobado").prop("checked"))
		noaprobado = 1;

	if ($("#CEnviado").prop("checked"))
		enviado = 1;

	var cotizado = $("#CRCotizado").val() * 1;

	var remision = 0;
	var estado = $("#CRAprobado").val(); 
	var cliente = $("#CRCliente").val() * 1;
	var usuario = $("#CRUsuario").val() * 1;

	var fechad = $("#CRFechaDesde").val();
	var fechah = $("#CRFechaHasta").val();

	if (fechad == "" && fechah == "") {
		swal("", ValidarTraduccion("debe de ingresar una fecha o números de días"), "error");
		return false;
	}

	ActivarLoad();
	setTimeout(function () {
		var parametros = "remision=" + remision + "&cliente=" + cliente + "&ingreso=" + ingreso +
			"&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
			"&intervalo=" + intervalo + "&serie=" + serie + "&usuario=" + usuario + "&cotizado=" + cotizado + "&estado=" + estado + "&asesor=" + asesor + "&enviado=" + enviado + "&noaprobado=" + noaprobado;
		var datos = LlamarAjax("Cotizacion","opcion=ConsulIngReportado&" + parametros);
		DesactivarLoad();

		var datajson = JSON.parse(datos);
		$('#TablaReportes').DataTable({
			data: datajson,
			bProcessing: true,
			bDestroy: true,
			columns: [
				{ "data": "enviar" },
				{ "data": "ingreso" },
				{ "data": "plantilla" },
				{ "data": "remision" },
				{ "data": "cotizado" },
				{ "data": "cliente" },
				{ "data": "telefono" },
				{ "data": "equipo" },
				{ "data": "marca" },
				{ "data": "rango" },
				{ "data": "fecha" },
				{ "data": "observacion" },
				{ "data": "reporte" },
				{ "data": "fechaenvio" },
				{ "data": "correoenvio" },
				{ "data": "estado" },
				{ "data": "aprobadopor" },
				{ "data": "fecaprobacion" },
				{ "data": "observacionapro" },
				{ "data": "usuario" },
				{ "data": "tiempo" },
				{ "data": "idreporte" }
			],

			"language": {
				"url": LenguajeDataTable
			},
			dom: 'Bfrtip',
			buttons: [
				'excel', 'csv', 'copy','colvis'
			]
		});
	}, 15);
}

function GuardarDocumento(id) {
	var documento = document.getElementById(id);
	if ($.trim(documento.value) != "") {

		var file = documento.files[0];
		var data = new FormData();
		data.append('documento', file);
		var resultado = '';
		var direccion = url + "Cotizacion/AdjuntarExcel";
		$.ajax({
			url: direccion,
			type: 'POST',
			contentType: false,
			data: data,
			processData: false,
			async: false,
			success: function (datos) {
				datos = datos.split("|");
				if (datos[0] != "0") {
					swal(ValidarTraduccion("Error al cargar el archivo"), "", "warning");
				} else {
					if (datos[2] != "XX")
						swal(ValidarTraduccion(datos[2]), "", "warning");
				}
			}
		});
	}
}

function GuardarAproRepo() {

	var ingreso = $("#AIngreso").val();
	var id = $("#IdReporte").val();
	var tipo = $("#OpcionAproRepor").val();
	var observacion = $.trim($("#ObservacionRepor").val());

	var usuario = $.trim($("#UsuarioRepor").val());
	var cedula = $.trim($("#CedulaRepor").val());
	var cargo = $.trim($("#CargoRepor").val());

	var fecha = $("#FechaRepor").val();
	var hora = $("#HoraRepor").val();

	var archivo = $.trim($("#ArchivoApro").val());

   
	if (fecha == "") {
		$("#FechaRepor").focus();
		swal("Acción Cancelada", "Debe de ingresar la fecha que aprueba o reprueba el ajuste/suministro", "warning");
		return false;
	} else {
		if (fecha > output2) {
			$("#FechaRepor").focus();
			swal("Acción Cancelada", "La fecha no puede ser mayor a la fecha actual", "warning");
			return false;
		}
	}

	if (hora == "") {
		$("#HoraRepor").focus();
		swal("Acción Cancelada", "Debe de ingresar la hora que aprueba o reprueba el ajuste/suministro", "warning");
		return false;
	}
	
	if (usuario == "") {
		$("#UsuarioRepor").focus();
		swal("Acción Cancelada", "Debe de ingresar el usuario que aprueba o reprueba el ajuste/suministro", "warning");
		return false;
	}

	if (cargo == "") {
		$("#CargoRepor").focus();
		swal("Acción Cancelada", "Debe de ingresar el cargo que aprueba o reprueba el ajuste/suministro", "warning");
		return false;
	}

	if (observacion == "") {
		$("#ObservacionRepor").focus();
		swal("Acción Cancelada", "Debe de ingresar una observación", "warning");
		return false;
	}

	if (archivo == "") {
		$("#ArchivoApro").focus();
		swal("Acción Cancelada", "Debe de ingresar el archivo pdf de la aprobación del cliente", "warning");
		return false;
	}

	var datos = LlamarAjax("Laboratorio","opcion=AprobarReporte&ingreso=" + ingreso + "&id=" + id + "&opciones=" + tipo + "&observacion=" + observacion +
		"&usuario=" + usuario + "&cargo=" + cargo + "&cedula=" + cedula + "&fecha=" + fecha + "&hora=" + hora)
	datos = datos.split("|");
	if (datos[0] == "0") {
		ConsularReportes();
		NotificacionesCotiApro();
		$("#modalAprobarRepor").modal("hide");
		swal("", datos[1], "success");
	} else {
		swal("Acción Cancelada", datos[1], "warning");
	}
}

$('select').select2();
DesactivarLoad();
