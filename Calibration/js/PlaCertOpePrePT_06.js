﻿var d = new Date();
var month = d.getMonth() + 1;
var day = d.getDate();
var DecimalesP = [20];
var IdVersion = 6;
var HoraInicio = "";
var HoraFinal = "";
var HoraCondicion = "";
var CerImpreso = 0;
var ErrorHume = 0;
var ErrorTemp = 0;
var IntervaloTiempo = null;
var PuntosCalibracion = 0;
var FirmaCertificado = "";
var SensorTemperatura = 0;

var canvafirma = document.getElementById("padfirmar");
initPad(canvafirma);

var Solicitante = "";
var Direccion = "";

var PermisioEspecial = PerGeneSistema("PERMISO ADMINISTRADOR LABORATORIO");
$("#Sensor").html(CargarCombo(74, 0, "", "6"));

var ctxgra = document.getElementById('myChart1').getContext('2d');
var gratemp = document.getElementById('GraTemperatura').getContext('2d');
var grahume = document.getElementById('GraHumedad').getContext('2d');
var ctdesviacion = document.getElementById('myCharDesviacion').getContext('2d');

var datastuden = JSON.parse(LlamarAjax("Laboratorio/TStuden", ""));

var COLORS = [
    '#4dc9f6',
    '#f67019',
    '#f53794',
    '#537bc4',
    '#acc236',
    '#166a8f',
    '#00a950',
    '#58595b',
    '#8549ba'
];



DatosTemperatura(6, SensorTemperatura);


var Aniofull = $.trim(d.getFullYear());
var Anio = $.trim(Aniofull.substr(2, 2))
var AprobarCotiza = "";

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

var FechaCal = d.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;
var FechaActual = d.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;

var Ingreso = 0;
var IdReporte = 0;
var IdInstrumento = 0;
var ErrorEnter = 0;
var Foto = 0;
var NroReporte = 1;
var RangoHasta = 0;
var GuardaTemp = 0;
var CorreoEmpresa = "";
var AplicarTemporal = 0;
var InfoTecIngreso = 0;
var Factor = 0;
var TipoInst = "";
var Descripcion = "";
var Resolucion = 0;
var CantRegitro = 3;
var DesPerm1 = 0;
var DesPerm2 = 0;
var Certificado = "";
var IdCalibracion = 0;
var NumCertificado = 1;
var Revision = 1;
var Foto = Foto;

var TempMax = 0;
var TempMin = 100000000;
var HumeMax = 0;
var HumeMin = 100000000;
var TempVar = 0;
var HumeVar = 0;

function FormatoSalida2(Caja, decimal) {
    FormatoSalida(Caja, decimal, NumCertificado);
}

function CambioSensor(sensor) {
    SensorTemperatura = sensor;
}

$("#Sensor").trigger("change");

function CambioCertificado(tipo) {
    NumCertificado = tipo;
    LimpiarTodo();
    if (tipo*1 == 2)
        $(".negativo").html("<h3>VALORES EN NEGATIVO CUANDO LA CALIBRACION ES EN SENTIDO ANTIHORARIO</h3>");
    else
        $(".negativo").html("");
}

localStorage.setItem("Ingreso", "0");

function LimpiarTodo() {
    GuardaTemp = 0;
    localStorage.removeItem(NumCertificado + "PlaPT1" + Ingreso);
    LimpiarDatos();
    $("#Ingreso").val("");
    GuardaTemp = 1;
}

function LimpiarDatos() {
    FechaCal = d.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;
    IdCalibracion = 0;
    AplicarTemporal = 0;
    IntervaloTiempo = null;
    Ingreso = 0;
    Foto = 0
    HoraInicio = "";
    HoraFinal = "";
    CerImpreso = 0;
    ErrorHume = 0;
    ErrorTemp = 0;
    Revision = 1;
    Certificado = "";

    TempMax = 0;
    TempMin = 100000000;
    HumeMax = 0;
    HumeMin = 100000000;
    TempVar = 0;
    HumeVar = 0;


    InfoTecIngreso = 0;
    $("#Solicitante").html("");
    $("#Certificado").html("");
    $("#Direccion").html("");
    $("#FechaIng").html("");
    $("#FechaCal").html("");
    $("#FechaExp").html("");
    $("#RangoMaximo").html("")
    $("#TIngreso").html("");
    $("#Medida").html("");
    $("#ValorCon").html("");
    $("#UnidadCon").html("");
    $("#UnidadCon").html("");
    $("#ProximaCali").val("");
    $("#Observacion").val("La calibración se realizó en las instalaciones permanentes de Calibration Service S.A.S.");
    localStorage.setItem("FirmaCertificado", "");
    $(".medida").html("");
    $("#CertificadoAnulado").html("");
    $("#TiempoCali").val("0").trigger("change");

    $("#Equipo").html("");
    $("#Marca").html("");
    $("#Modelo").html("");
    $("#Serie").html("");
    $("#Remision").val("");
    $("#Usuario").val("");
    $("#FechaRec").val("");
    $("#Tiempo").val("");

    $("#RangoMedida").html("");
    $("#MedResolucion").html("");
    RangoHasta = 0;

    $("#Descripcion").html("");
    $("#Tolerancia").html("");
    $("#Resolucion").html("");
    $("#Indicacion").html("");
    $("#Clase").html("");
    $("#Clasificacion").html("");

    $("#ButCertFirMan").addClass("hidden");

    $("#CMetodo").val("");
    $("#CPunto").val("");
    $("#CServicio").val("");
    $("#CObservCerti").val("");
    $("#CProxima").val("");
    $("#CEntrega").val("");
    $("#CAsesorComer").val("");

    DesPerm1 = 0;
    $("#DevPermitida1").html(DesPerm1);
    DesPerm2 = 0;
    $("#DevPermitida2").html(DesPerm2);

    $("#PrSerie1").val("");
    $("#PrSerie2").val("");
    $("#PrSerie3").val("");
    $("#PrSerie4").val("");
    $("#PrSerie5").val("");
    $("#PrPosicion").val("");

    $("#CaHoraIni").val(""); 
    $("#CaHoraFin").val(""); 
    $("#CaTiempo").val(""); 

    for (var x = 1; x <= CantRegitro; x++) {
        $("#PaPorLectura" + x).val("");
        $("#Patron" + x).val("").trigger("change");
    }

    $("#ProximaCali").prop("disabled", false);
    $("#Observacion").prop("disabled", false);
    $("#TiempoCali").prop("disabled", false);
    
    $("#PrSerie1").prop("disabled", false);
    $("#PrSerie2").prop("disabled", false);
    $("#PrSerie3").prop("disabled", false);
    $("#PrSerie4").prop("disabled", false);
    $("#PrSerie5").prop("disabled", false);
    $("#PrPosicion").prop("disabled", false);

    ConfigurarTablas(0);
        
    IdInstrumento = 0;

    Graficar();
    GraficarDesviacion();
    CalcularTemperatura(0);

}

function GuardarDocumento() {
    var documento = document.getElementById("documento");
    if ($.trim(documento.value) != "") {

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Cotizacion/AdjuntarPDF";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "error");
                }
            }
        });
    }
}

function CambioTiempoCali(tiempo) {
    if (tiempo * 1 == 0)
        $("#ProximaCali").val("");
    else {
        var fecha = (d.getFullYear()*1 + tiempo*1) + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;
        $("#ProximaCali").val(fecha);
    }
}

function GeneralPlantilla() {
    var concepto = $("#Concepto").val() * 1;
    if (Ingreso == 0)
        return false;

    if (GuardaTemp == 1 || IdReporte == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    if ((concepto != 2) && (concepto != 1)) {
        swal("Acción Cancelada", "No se puede generar la planilla del certificado cuando no está apto a calibrar o en subcontrataciones", "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Laboratorio/GenerarPlantilla", "ingreso=" + Ingreso + "&magnitud=6").split("|");
        DesactivarLoad();
        if (datos[0] == "0")
            swal("", datos[1], "success");
        else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
}

function EnviarCotizacion() {

    var concepto = $("#Concepto").val() * 1;


    if (Ingreso == 0)
        return false;
    if (GuardaTemp == 1 || IdReporte == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    $("#eCliente").val($("#Solicitante").html());
    $("#eCorreo").val(CorreoEmpresa);


    if (concepto == 2) {
        $("#spcotizacion").html(Ingreso);
        $("#enviar_cotizacion").modal("show");
    } else {
        swal("Acción Cancelada", "Solo se podra enviar cotizaciones cuando el ingreso requiera ajuste o suministro", "warning");
    }
    
}

function BuscarIngreso(numero, code, tipo) {
    if ((Ingreso != (numero * 1)) && ($("#Equipo").html() != "")) {
        GuardaTemp = 1;
        LimpiarDatos();
    }
        
    if ((code.keyCode == 13 || tipo == "0") && ErrorEnter == 0) {
        if (numero * 1 > 0) {
            GuardaTemp = 0;
            ActivarLoad();
            setTimeout(function () {
                var datos = LlamarAjax("Laboratorio/BuscarIngresoPlaCert", "magnitud=6&ingreso=" + numero + "&medida=N·m" + "&numero=" + NumCertificado + "&version=" + IdVersion).split("|");
                DesactivarLoad();
                if (datos[0] != "[]") {
                    var data = JSON.parse(datos[0]);

                    if (data[0].idmagnitud != "6") {
                        swal("Acción Cancelada", "El ingreso número " + numero + " no pertenece a la magnitud <br><b>PAR TORSIONAL</b>", "warning");
                        ErrorEnter = 1;
                        $("#Ingreso").select();
                        $("#Ingreso").focus();
                        return false;
                    }

                   
                       
                    Ingreso = data[0].ingreso * 1;
                    localStorage.setItem("Ingreso", Ingreso);
                    Foto = data[0].fotos * 1;
                    Solicitante = data[0].cliente;
                    $("#Solicitante").html(Solicitante);
                    Direccion = data[0].direccion;
                    $("#Direccion").html(Direccion);
                    $("#FechaIng").html(data[0].fechaing);
                    $("#FechaCal").html(FechaCal);
                    CorreoEmpresa = data[0].email;
                    CantRegitro = data[0].puntos * 1;
                    Revision = data[0].revision + 1;

                    FirmaCertificado = data[0].firmacertificado;
                    $("#firmacertificado").html("FIRMA " + FirmaCertificado);
                    if (FirmaCertificado == "MANUAL")
                        $("#ButCertFirMan").removeClass("hidden");
                    
                    $("#TIngreso").html(data[0].ingreso);
                    $("#Medida").html(data[0].medida);
                    $("#ValorCon").html(data[0].medidacon);
                    Factor = data[0].medidacon;
                    $("#UnidadCon").html("1 " + data[0].medida + " =");

                    $(".medida").html(data[0].medida);

                    $("#Equipo").html(data[0].equipo);
                    $("#Marca").html(data[0].marca);
                    $("#Modelo").html(data[0].modelo);
                    $("#Serie").html(data[0].serie);
                    $("#Remision").val(data[0].remision);
                    InfoTecIngreso = data[0].informetecnico;

                    $("#RangoMedida").html("(" + data[0].desde + " a " + data[0].hasta + ") " + data[0].medida);
                    $("#MedResolucion").html(data[0].medida);
                    RangoHasta = data[0].hasta;
                    $("#RangoMaximo").html(RangoHasta);

                    DesPerm1 = data[0].desviacion * 1;
                    if ((DesPerm1 = 4) && ((RangoHasta * Factor) <= 10))
                        DesPerm1 = 6;
                    $("#DevPermitida1").html(DesPerm1);
                    DesPerm2 = DesPerm1 * -1;
                    $("#DevPermitida2").html(DesPerm2);

                    $("#Descripcion").html(data[0].descripcion);
                    $("#Tolerancia").html(data[0].tolerancia);
                    $("#Resolucion").html(data[0].resolucion);
                    Resolucion = data[0].resolucion;
                    $("#Indicacion").html(data[0].indicacion);
                    $("#Clase").html(data[0].clase);
                    $("#Clasificacion").html(data[0].clasificacion);
                    TipoInst = data[0].clasificacion;
                    Descripcion = data[0].descripcion1;

                    $("#Usuario").val(data[0].usuarioing);
                    $("#FechaRec").val(data[0].fecharec);
                    $("#Tiempo").val(data[0].tiempo);

                    if (TipoInst == "TIPO I") {
                        for (var x = 1; x <= CantRegitro; x++) {
                            $("#NmTiempo" + x).addClass("bg-lineas-diagonales");
                            $("#CNmTiempo" + x).addClass("bg-lineas-diagonales");
                            $("#CMeTiempo" + x).addClass("bg-lineas-diagonales");
                            $("#ReTiempo" + x).addClass("bg-lineas-diagonales");
                            $("#CoTiempo" + x).addClass("bg-lineas-diagonales");

                            $("#Resolucion1" + x).removeClass("bg-lineas-diagonales");
                        }
                        $("#PrSerie4").addClass("bg-lineas-diagonales");
                        $("#PrSerie5").addClass("bg-lineas-diagonales");
                        $("#PrSerie4").val("0");
                        $("#PrSerie5").val("0");
                        $("#PrSerie4").prop("readonly", true);
                        $("#PrSerie5").prop("readonly", true);

                    } else {
                        for (var x = 1; x <= CantRegitro; x++) {
                            $("#NmTiempo" + x).removeClass("bg-lineas-diagonales");
                            $("#CNmTiempo" + x).removeClass("bg-lineas-diagonales");
                            $("#CMeTiempo" + x).removeClass("bg-lineas-diagonales");
                            $("#ReTiempo" + x).removeClass("bg-lineas-diagonales");
                            $("#CoTiempo" + x).removeClass("bg-lineas-diagonales");
                            $("#Resolucion1" + x).addClass("bg-lineas-diagonales");
                        }
                        $("#PrSerie4").removeClass("bg-lineas-diagonales");
                        $("#PrSerie5").removeClass("bg-lineas-diagonales");
                        $("#PrSerie4").prop("readonly", false);
                        $("#PrSerie5").prop("readonly", false);
                        $("#PrSerie4").val("");
                        $("#PrSerie5").val("");
                    }

                    if (datos[2] != "[]") {

                        var datacer = JSON.parse(datos[2]);
                        IdCalibracion = datacer[0].id * 1;
                        FechaCal = datacer[0].fecha;
                        $("#FechaCal").html(FechaCal);
                        $("#ProximaCali").val(datacer[0].proximacali);
                        Revision = datacer[0].revision * 1;
                        $("#Observacion").val(datacer[0].observacion)
                        HoraInicio = datacer[0].horaini;
                        HoraFinal = datacer[0].horafin;

                        $("#CaHoraIni").val(HoraInicio);
                        $("#CaHoraFin").val(HoraFinal);
                        $("#CaTiempo").val(datacer[0].tiempotrans);
                        
                        CerImpreso = datacer[0].impreso * 1;

                        var datapunto = JSON.parse(datos[3]);
                        PuntosCalibracion = datapunto.length;
                        ConfigurarTablas(datapunto.length);
                        for (var x = 0; x < datapunto.length; x++) {
                                                                                                                
                            $("#Patron" + datapunto[x].fila).val(datapunto[x].patron).trigger("change");
                            $("#PaPorLectura" + datapunto[x].fila).val(formato_numero(datapunto[x].porlectura, 3, ".", ",", ""));
                            $("#PrSerie1").val(formato_numero(datapunto[x].prserie1, 3, ".", ",", ""));
                            $("#PrSerie2").val(formato_numero(datapunto[x].prserie2, 3, ".", ",", ""));
                            $("#PrSerie3").val(formato_numero(datapunto[x].prserie3, 3, ".", ",", ""));
                            $("#PrSerie4").val(formato_numero(datapunto[x].prserie4, 3, ".", ",", ""));
                            $("#PrSerie5").val(formato_numero(datapunto[x].prserie5, 3, ".", ",", ""));
                            $("#PrPosicion").val(formato_numero(datapunto[x].prposicion, 3, ".", ",", ""));
                            DecimalesP[datapunto[x].fila] = datapunto[x].decimalpatron;
                            $("#DecimalPatron" + datapunto[x].fila).val(DecimalesP[datapunto[x].fila]);
                            CambioDecimales(DecimalesP[datapunto[x].fila], datapunto[x].fila);

                            $("#Serie1" + datapunto[x].fila).val(formato_numero(datapunto[x].serie1, 3, ".", ",", ""));
                            $("#Serie2" + datapunto[x].fila).val(formato_numero(datapunto[x].serie2, 3, ".", ",", ""));
                            $("#Serie3" + datapunto[x].fila).val(formato_numero(datapunto[x].serie3, 3, ".", ",", ""));
                            $("#Serie4" + datapunto[x].fila).val(formato_numero(datapunto[x].serie4, 3, ".", ",", ""));
                            $("#Serie5" + datapunto[x].fila).val(formato_numero(datapunto[x].serie5, 3, ".", ",", ""));
                            $("#Brazo" + datapunto[x].fila).val(formato_numero(datapunto[x].brazo, 3, ".", ",", ""));
                            $("#Tiempo" + datapunto[x].fila).val(formato_numero(datapunto[x].tiempo, 3, ".", ",", ""));
                            $("#Posicion" + datapunto[x].fila).val(formato_numero(datapunto[x].posicion, 3, ".", ",", ""));
                        }
                        CalcularTemperatura(0);

                    } else {
                        if (datos[1] != "[]") {
                            var dataser = JSON.parse(datos[1]);
                            ConfigurarTablas(dataser.length);
                            for (var x = 0; x < dataser.length; x++) {
                                $("#PaPorLectura" + dataser[x].numero).val(dataser[x].porlectura);
                                $("#Patron" + dataser[x].numero).val(dataser[x].patron).trigger("change");
                            }
                        } else {
                            ErrorEnter = 1;
                            $("#Ingreso").select();
                            $("#Ingreso").focus();
                            swal("Acción Cancelada", "El ingreso número " + Ingreso + " no posee operaciones previas", "warning");
                            if (PermisioEspecial == 0) {
                                LimpiarTodo();
                                return false;
                            }
                        }
                    }

                    if (datos[5] != "[]") {
                        var dataco = JSON.parse(datos[5]);
                        for (var x = 0; x < dataco.length; x++) {
                            if (x == 0) {
                                $("#CMetodo").val(dataco[x].metodo);
                                $("#CPunto").val(dataco[x].punto);
                                $("#CServicio").val(dataco[x].servicio);
                                $("#CObservCerti").val(dataco[x].observacion);
                                $("#CProxima").val(dataco[x].proxima);
                                $("#CEntrega").val(dataco[x].entrega);
                                $("#CAsesorComer").val(dataco[x].asesor);
                            } else {
                                $("#CMetodo").val("<br>" + dataco[x].metodo);
                                $("#CPunto").val("<br>" + dataco[x].punto);
                                $("#CServicio").val("<br>" + dataco[x].servicio);
                                $("#CObservCerti").val("<br>" + dataco[x].observacion);
                                $("#CProxima").val("<br>" + dataco[x].proxima);
                                $("#CEntrega").val("<br>" + dataco[x].entrega);
                                $("#CAsesorComer").val("<br>" + dataco[x].asesor);
                            }
                        }
                    } else {
                        if (IdCalibracion == 0) {
                            ErrorEnter = 1;
                            swal("Acción Cancelada", "Este ingreso no ha sido cotizado por el asesor comercial", "warning");
                        }
                    }

                    if (IdCalibracion == 0) {
                        if (localStorage.getItem(NumCertificado + "PlaPT1" + Ingreso)) {
                            var data = JSON.parse(localStorage.getItem(NumCertificado + "PlaPT1" + Ingreso));
                            $("#PrSerie1").val(data[0].PrSerie1);
                            $("#PrSerie2").val(data[0].PrSerie2);
                            $("#PrSerie3").val(data[0].PrSerie3);
                            $("#PrSerie4").val(data[0].PrSerie4);
                            $("#PrSerie5").val(data[0].PrSerie5);
                            $("#PrPosicion").val(data[0].PrPosicion);
                            $("#ProximaCali").val(data[0].ProximaCali);

                            HoraInicio = data[0].HoraInicio;
                            HoraCondicion = HoraInicio;
                            LeerCondiciones(0);
                            IntervaloTiempo = null;
                            IntervaloTiempo = setInterval(TiempoTrascurrudi, 60000);
                            $("#CaHoraIni").val(HoraInicio);
                            if (HoraInicio != "") {
                                d = new Date();
                                horaactual = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() * 1 + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
                                $("#CaTiempo").val(RestarHoras(HoraInicio, horaactual));
                            }

                            for (var x = 1; x <= CantRegitro; x++) {
                                switch (x) {
                                    case 1:
                                        $("#DecimalPatron" + x).val(data[0].Decimal1);
                                        CambioDecimales(data[0].Decimal1, x);
                                        $("#Serie1" + x).val(data[0].Serie11);
                                        $("#Serie2" + x).val(data[0].Serie21);
                                        $("#Serie3" + x).val(data[0].Serie31);
                                        $("#Serie4" + x).val(data[0].Serie41);
                                        $("#Serie5" + x).val(data[0].Serie51);
                                        $("#Posicion" + x).val(data[0].Posicion1);
                                        $("#Brazo" + x).val(data[0].Brazo1);
                                        $("#Tiempo" + x).val(data[0].Tiempo1);
                                        $("#PaPorLectura" + x).val(data[0].PaPorLectura1);
                                        $("#Patron" + x).val(data[0].Patron1).trigger("change");
                                        break;
                                    case 2:
                                        $("#DecimalPatron" + x).val(data[0].Decimal2);
                                        CambioDecimales(data[0].Decimal2, x);
                                        $("#Serie1" + x).val(data[0].Serie12);
                                        $("#Serie2" + x).val(data[0].Serie22);
                                        $("#Serie3" + x).val(data[0].Serie32);
                                        $("#Serie4" + x).val(data[0].Serie42);
                                        $("#Serie5" + x).val(data[0].Serie52);
                                        $("#Posicion" + x).val(data[0].Posicion2);
                                        $("#Brazo" + x).val(data[0].Brazo2);
                                        $("#Tiempo" + x).val(data[0].Tiempo2);
                                        $("#PaPorLectura" + x).val(data[0].PaPorLectura2);
                                        $("#Patron" + x).val(data[0].Patron2).trigger("change");
                                        break;
                                    case 3:
                                        $("#DecimalPatron" + x).val(data[0].Decimal3);
                                        CambioDecimales(data[0].Decimal3, x);
                                        $("#Serie1" + x).val(data[0].Serie13);
                                        $("#Serie2" + x).val(data[0].Serie23);
                                        $("#Serie3" + x).val(data[0].Serie33);
                                        $("#Serie4" + x).val(data[0].Serie43);
                                        $("#Serie5" + x).val(data[0].Serie53);
                                        $("#Posicion" + x).val(data[0].Posicion3);
                                        $("#Brazo" + x).val(data[0].Brazo3);
                                        $("#Tiempo" + x).val(data[0].Tiempo3);
                                        $("#PaPorLectura" + x).val(data[0].PaPorLectura3);
                                        $("#Patron" + x).val(data[0].Patron3).trigger("change");
                                        break;
                                    case 4:
                                        $("#DecimalPatron" + x).val(data[0].Decimal4);
                                        CambioDecimales(data[0].Decimal4, x);
                                        $("#Serie1" + x).val(data[0].Serie14);
                                        $("#Serie2" + x).val(data[0].Serie24);
                                        $("#Serie3" + x).val(data[0].Serie34);
                                        $("#Serie4" + x).val(data[0].Serie44);
                                        $("#Serie5" + x).val(data[0].Serie54);
                                        $("#Posicion" + x).val(data[0].Posicion4);
                                        $("#Brazo" + x).val(data[0].Brazo4);
                                        $("#Tiempo" + x).val(data[0].Tiempo4);
                                        $("#PaPorLectura" + x).val(data[0].PaPorLectura4);
                                        $("#Patron" + x).val(data[0].Patron4).trigger("change");
                                        break;
                                    case 5:
                                        $("#DecimalPatron" + x).val(data[0].Decimal5);
                                        CambioDecimales(data[0].Decimal5, x);
                                        $("#Serie1" + x).val(data[0].Serie15);
                                        $("#Serie2" + x).val(data[0].Serie25);
                                        $("#Serie3" + x).val(data[0].Serie35);
                                        $("#Serie4" + x).val(data[0].Serie45);
                                        $("#Serie5" + x).val(data[0].Serie55);
                                        $("#Posicion" + x).val(data[0].Posicion5);
                                        $("#Brazo" + x).val(data[0].Brazo5);
                                        $("#Tiempo" + x).val(data[0].Tiempo5);
                                        $("#PaPorLectura" + x).val(data[0].PaPorLectura5);
                                        $("#Patron" + x).val(data[0].Patron5).trigger("change");
                                        break;
                                    case 6:
                                        $("#DecimalPatron" + x).val(data[0].Decimal6);
                                        CambioDecimales(data[0].Decimal6, x);
                                        $("#Serie1" + x).val(data[0].Serie16);
                                        $("#Serie2" + x).val(data[0].Serie26);
                                        $("#Serie3" + x).val(data[0].Serie36);
                                        $("#Serie4" + x).val(data[0].Serie46);
                                        $("#Serie5" + x).val(data[0].Serie56);
                                        $("#Posicion" + x).val(data[0].Posicion6);
                                        $("#Brazo" + x).val(data[0].Brazo6);
                                        $("#Tiempo" + x).val(data[0].Tiempo6);
                                        $("#PaPorLectura" + x).val(data[0].PaPorLectura6);
                                        $("#Patron" + x).val(data[0].Patron6).trigger("change");
                                        break;
                                }
                            }
                            
                        }
                        CalcularTotal();
                        GuardaTemp = 1;                                                                   
                    } else {
                        CalcularTotal();
                        AplicarTemporal = 1; 
                    }

                    if (datos[4] != "[]") {
                        var datagen = JSON.parse(datos[4]);
                        var revisioncer = datagen[0].revision * 1;
                        var numcer = datagen[0].item * 1;
                        Certificado = datagen[0].numero;

                        if (datagen[0].nombrecer != "")
                            $("#Solicitante").html(datagen[0].nombrecer);
                        if (datagen[0].direccioncer != "")
                            $("#Direccion").html(datagen[0].direccioncer);

                        /*if (datagen[0].idusuarioanula * 1 > 0) {
                            Revision = revisioncer + 1;
                            return false;
                        }*/
                        
                        $("#FechaExp").html(datagen[0].fechaexp);
                        $("#ProximaCali").prop("disabled", true);
                        $("#Observacion").prop("disabled", true);
                        $("#TiempoCali").prop("disabled", true);
                        if (revisioncer == Revision && NumCertificado == numcer) {
                            $("#PrSerie1").prop("disabled", true);
                            $("#PrSerie2").prop("disabled", true);
                            $("#PrSerie3").prop("disabled", true);
                            $("#PrSerie4").prop("disabled", true);
                            $("#PrSerie5").prop("disabled", true);
                            $("#PrPosicion").prop("disabled", true);
                            Certificado = datagen[0].numero;
                            if (datagen[0].idusuarioanula * 1 > 0) {
                                
                            }
                            for (var x = 1; x <= CantRegitro; x++) {
                                $("#Serie1" + x).prop("disabled", true);
                                $("#Serie2" + x).prop("disabled", true);
                                $("#Serie3" + x).prop("disabled", true);
                                $("#Serie4" + x).prop("disabled", true);
                                $("#Serie5" + x).prop("disabled", true);
                                $("#Posicion" + x).prop("disabled", true);
                                $("#Brazo" + x).prop("disabled", true);
                                $("#Tiempo" + x).prop("disabled", true);
                                $("#PaPorLectura" + x).prop("disabled", true);
                                $("#Patron" + x).prop("disabled", true);
                                $("#DecimalPatron" + x).prop("disabled", true);
                            }
                            $("#Certificado").html(Certificado);
                            ErrorEnter = 1;
                            swal("Advertencia", "El certificado ya fue generado con el número " + Certificado, "warning")
                            EscucharMensaje("El certificado ya fue generado");
                        }
                    }

                    

                    if (IdCalibracion > 0 && Certificado == "") {

                        $("#PrSerie1").prop("disabled", false);
                        $("#PrSerie2").prop("disabled", false);
                        $("#PrSerie3").prop("disabled", false);
                        $("#PrSerie4").prop("disabled", false);
                        $("#PrSerie5").prop("disabled", false);
                        $("#PrPosicion").prop("disabled", false);
                        $("#ProximaCali").prop("disabled", false);
                        $("#Observacion").prop("disabled", false);
                        $("#TiempoCali").prop("disabled", false);
                        for (var x = 1; x <= CantRegitro; x++) {
                            $("#Serie1" + x).prop("disabled", false);
                            $("#Serie2" + x).prop("disabled", false);
                            $("#Serie3" + x).prop("disabled", false);
                            $("#Serie4" + x).prop("disabled", false);
                            $("#Serie5" + x).prop("disabled", false);
                            $("#Posicion" + x).prop("disabled", false);
                            $("#Brazo" + x).prop("disabled", false);
                            $("#Tiempo" + x).prop("disabled", false);
                            $("#PaPorLectura" + x).prop("disabled", false);
                            $("#Patron" + x).prop("disabled", false);
                        }


                        /*if (FechaCal != FechaActual) {
                            var datos = LlamarAjax("Laboratorio/EliminarCalibTemporal", "numcertificado=" + NumCertificado + "&revision=" + Revision + "&ingreso=" + Ingreso);
                            swal("Advertencia", "Temporal Eliminado porque fue guardado un día diferente a la fecha actual", "warning");
                            EscucharMensaje("Temporal Eliminado, porque fue guardado un día diferente a la fecha actual");

                            $("#PrSerie1").val("");
                            $("#PrSerie2").val("");
                            $("#PrSerie3").val("");
                            $("#PrSerie4").val("");
                            $("#PrSerie5").val("");
                            $("#PrPosicion").val("");
                            $("#ProximaCali").val("");

                            HoraInicio = "";
                            $("#CaHoraIni").val(HoraInicio);
                            $("#CaTiempo").val("");
                            HoraFinal = ""; 
                            $("#CaHoraFin").val("");
                            
                            for (var x = 1; x <= CantRegitro; x++) {
                                $("#Serie1" + x).val(data[0].Serie11);
                                $("#Serie2" + x).val(data[0].Serie21);
                                $("#Serie3" + x).val(data[0].Serie31);
                                $("#Serie4" + x).val(data[0].Serie41);
                                $("#Serie5" + x).val(data[0].Serie51);
                                $("#Posicion" + x).val(data[0].Posicion1);
                                $("#Brazo" + x).val(data[0].Brazo1);
                                $("#Tiempo" + x).val(data[0].Tiempo1);
                                $("#PaPorLectura" + x).val(data[0].PaPorLectura1);
                                $("#Patron" + x).val(data[0].Patron1).trigger("change");
                            }

                            return false;
                        } else {
                            swal("Advertencia", "El certificado ya fue guardado el día " + datacer[0].fecha + ", pero no ha sido generado", "warning");
                            EscucharMensaje("El certificado ya fue guardado, pero no ha sido generado");
                        }*/
                    }
                } else {
                    $("#Ingreso").select();
                    $("#Ingreso").focus();
                    ErrorEnter = 1;
                    swal("Acción Cancelada", "El Ingreso número " + numero + " no se ha recibido en el laboratorio", "warning");
                    return false;
                }
                                
                if (ErrorTemp == 1 || ErrorHume == 1)
                    $("#documento").focus();

                
            }, 15);
        }
    } else
        ErrorEnter = 0;
}

function EliminarTemporal() {
    if (IdCalibracion > 0 && Certificado == "") {
        var numero = Ingreso;

        swal.queue([{
            title: ValidarTraduccion('Advertencia'),
            text: ValidarTraduccion('¿Seguro que desea eliminar el temporal del certificado del ingreso número ' + Ingreso + '?'),
            type: 'question',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Si'),
            cancelButtonText: ValidarTraduccion('No'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    $.post("Laboratorio/EliminarCalibTemporal", "numcertificado=" + NumCertificado + "&revision=" + Revision + "&ingreso=" + Ingreso)
                        .done(function (data) {
                            datos = data.split("|");
                            if (datos[0] == "0") {
                                LimpiarTodo();
                                $("#Ingreso").val(numero);
                                BuscarIngreso(numero, "", "0");
                                swal("", datos[1], "");
                                resolve()
                            } else {
                                reject(datos[1]);
                            }
                        })
                })
            }
        }]);
    }
}

function CalcularSubTotal(Caja) {
    ValidarTexto(Caja, 3);
    CalcularTotal();
}


function CalcularTotal() {
    for (var x = 1; x <= PuntosCalibracion; x++) {
        CalcularSerie("", x);
    }
}

function GuardarTemporal() {

    if (Certificado != "")
        return false;

    if (Ingreso > 0 && GuardaTemp == 1) {

        if (IdCalibracion == 0) {
            if (HoraInicio == "") {
                d = new Date();
                HoraInicio = (d.getHours()*1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes()*1 < 10 ? "0" : "") + d.getMinutes();
                $("#CaHoraIni").val(HoraInicio);
                HoraCondicion = HoraInicio;
                horaactual = (d.getHours()*1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes()*1 < 10 ? "0" : "") + d.getMinutes();
                $("#CaTiempo").val(RestarHoras(HoraInicio, horaactual));
                IntervaloTiempo = null;
                IntervaloTiempo = setInterval(TiempoTrascurrudi, 60000);
            }
        }
        
        var archivo = "[{";
        archivo += '"PrSerie1":"' + $("#PrSerie1").val() + '","PrSerie2":"' + $("#PrSerie2").val() + '","PrSerie3":"' + $("#PrSerie3").val() + '",' +
            '"PrSerie4":"' + $("#PrSerie4").val() + '","PrSerie5":"' + $("#PrSerie5").val() + '","PrPosicion":"' + $("#PrPosicion").val() + '",' +
            '"HoraInicio":"' + HoraInicio + '","ProximaCali":"' + $("#ProximaCali").val() + '"';
        
        for (var x = 1; x <= CantRegitro; x++) {
            archivo += ',"Patron' + x + '":"' + $("#Patron" + x).val() + '"';
            archivo += ',"Decimal' + x + '":"' + $("#DecimalPatron" + x).val() + '"';
            archivo += ',"PaPorLectura' + x + '":"' + $("#PaPorLectura" + x).val() + '"';
            archivo += ',"Serie1' + x + '":"' + $("#Serie1" + x).val() + '","Serie2' + x + '":"' + $("#Serie2" + x).val() + '","Serie3' + x + '":"' + $("#Serie3"+x).val() + '",' +
                '"Serie4' + x + '":"' + $("#Serie4" + x).val() + '","Serie5' + x + '":"' + $("#Serie5" + x).val() + '",' + 
                '"Tiempo' + x + '":"' + $("#Tiempo" + x).val() + '","Brazo' + x + '":"' + $("#Brazo" + x).val() + '","Posicion' + x + '":"' + $("#Posicion" + x).val() + '"'
        }
               
                
        archivo += '}]';

        
        localStorage.setItem(NumCertificado + "PlaPT1" + Ingreso, archivo);

        $.jGrowl("Guardando Temporal", { life: 1500, theme: 'growl-warning', header: '' });
    } 
}


function CalcularSerie(Caja, fila, tipo) {
        
    if (Caja != "")
        ValidarTexto(Caja, 3);
        
    var serie1 = NumeroDecimal($("#Serie1" + fila).val());
    var serie2 = NumeroDecimal($("#Serie2" + fila).val());
    var serie3 = NumeroDecimal($("#Serie3" + fila).val());
    var serie4 = NumeroDecimal($("#Serie4" + fila).val());
    var serie5 = NumeroDecimal($("#Serie5" + fila).val());
    var tiempo = NumeroDecimal($("#Tiempo" + fila).val());
    var posicion = NumeroDecimal($("#Posicion" + fila).val());
    var brazo = NumeroDecimal($("#Brazo" + fila).val());
    var tolerancia = NumeroDecimal($("#Tolerancia").html());
    var patron = $("#Patron" + fila).val() * 1;
    
    if (tolerancia == 0)
        return false;

    var porlectura = 0;
    var lectura = 0;

    if (tipo == 1) {
        porlectura = NumeroDecimal($("#PaPorLectura" + fila).val()) * 1;
        lectura = RangoHasta * porlectura / 100;
    } else {

        porlectura = NumeroDecimal($("#PaPorLectura" + fila).val()) * 1;
        lectura = RangoHasta * porlectura / 100;
    }
        
    
    //var promedio = (serie1 + serie2 + serie3 + serie4 + serie5) / 5;
    //var desviacion = promedio - lectura;
    //var porcentaje = desviacion / promedio * 100;

    $("#SePorcentaje" + fila).html(porlectura);
    $("#NnPorcencate" + fila).html(porlectura);
    $("#CNmPorcencate" + fila).html(porlectura);
    $("#CMePorcencate" + fila).html(porlectura);
    $("#DePorcentaje" + fila).html(porlectura);
    $("#InOPorcentaje" + fila).html(porlectura);
    $("#InNPorcentaje" + fila).html(porlectura);
                       
    $("#PaLectura" + fila).val(formato_numero(lectura, 1, ".", ",", ""));
    $("#SeLectura" + fila).html(formato_numero(lectura, 1, ".", ",", ""));

    $("#NnLectura" + fila).html(formato_numero(lectura * Factor, 1, ".", ",", ""));
    $("#DeLectura" + fila).html(formato_numero(lectura * Factor, 1, ".", ",", ""));

    $("#CNmLectura" + fila).html(formato_numero(lectura * Factor, 1, ".", ",", ""));
    $("#CMeLectura" + fila).html(formato_numero(lectura, 1, ".", ",", ""));

    $("#InOLectura" + fila).html(formato_numero(lectura, 1, ".", ",", ""));
    $("#InNLectura" + fila).html(formato_numero(lectura * Factor, 1, ".", ",", ""));
    $("#ReLectura" + fila).html(formato_numero(lectura * Factor, 1, ".", ",", ""));

    $("#PValor1" + fila).html("");
    $("#PValor2" + fila).html("");
    $("#PValor3" + fila).html("");
    $("#PValor4" + fila).html("");
    $("#PValor5" + fila).html("");
    $("#PValor6" + fila).html("");
    $("#PValor7" + fila).html("");
    $("#PValor8" + fila).html("");
    $("#PValor9" + fila).html("");

    $("#PaPunto" + fila).val("");
    $("#UPatron" + fila).val("");
    $("#DPatron" + fila).val("");
    $("#RPatron" + fila).val("");
    $("#IPatron" + fila).val("");

    /*$("#CNmSerie1" + fila).html("");
    $("#CNmSerie2" + fila).html("");
    $("#CNmSerie3" + fila).html("");
    $("#CNmSerie4" + fila).html("");
    $("#CNmSerie5" + fila).html("");
    $("#CNmTiempo" + fila).html("");
    $("#CNmPosicion" + fila).html("");
    $("#CNmBrazo" + fila).html("");

    $("#CMeSerie1" + fila).html("");
    $("#CMeSerie2" + fila).html("");
    $("#CMeSerie3" + fila).html("");
    $("#CMeSerie4" + fila).html("");
    $("#CMeSerie5" + fila).html("");
    $("#CMeTiempo" + fila).html("");
    $("#CMePosicion" + fila).html("");
    $("#CMeBrazo" + fila).html("");*/
        
    //tabla corregida
    if (patron > 0) {
        var parametros = "patron=" + patron + "&factor=" + Factor + "&serie1=" + serie1 + "&serie2=" + serie2 + "&serie3=" + serie3 + "&serie4=" + serie4 + "&serie5=" + serie5 +
            "&tiempo=" + tiempo + "&posicion=" + posicion + "&lectura=" + (lectura * Factor) + "&factor=" + Factor;
        var datos = LlamarAjax("Laboratorio/DatosPatronCer", parametros).split("|");


        
        if (datos[0] == "0") {

            var data = JSON.parse(datos[1]);
            $("#PValor1" + fila).html(data[0].alcance);
            $("#PValor2" + fila).html(data[0].marca);
            $("#PValor3" + fila).html(data[0].modelo);
            $("#PValor4" + fila).html(data[0].serie);
            $("#PValor5" + fila).html(data[0].certificado);
            $("#PValor6" + fila).html(data[0].laboratorio);
            $("#PValor7" + fila).html(data[0].fechacertificado);
            $("#PValor8" + fila).html(data[0].proximacalibracion);
            $("#PValor9" + fila).html(data[0].resolucion);

            $("#PaPunto" + fila).html(formato_numero(lectura * Factor, 3, ".", ",", ""));
            $("#UPatron" + fila).val(formato_numero(datos[9], 3, ".", ",", ""));
            $("#DPatron" + fila).val(formato_numero(datos[10], 3, ".", ",", ""));
            $("#RPatron" + fila).val(data[0].resolucion);
            $("#IPatron" + fila).val("");
            if ($("#DecimalPatron" + fila).val() * 1 == 0) {
                numdecimal = $.trim(data[0].resolucion).split(".");
                DecimalesP[fila] = numdecimal[1].length;
                CambioDecimales(DecimalesP[fila], fila);
                $("#DecimalPatron" + fila).val(DecimalesP[fila]); 
            }
            $("#C1Patron" + fila).val(data[0].a0); 
            $("#C2Patron" + fila).val(data[0].a1); 
            $("#C3Patron" + fila).val(data[0].a2); 
            $("#C4Patron" + fila).val(data[0].a3); 

            var dserie = 0;
            var despromedio = 0;
            var despromedio2 = 0;
            for (var i = 1; i <= 5; i++) {
                dserie = ((lectura * Factor) - datos[(i + 1)]) * 100 / datos[(i + 1)];
                despromedio += dserie;
                despromedio2 += (datos[(i + 1)] * 1);
                if (dserie >= DesPerm2 && dserie <= DesPerm1) {
                    $("#DeSerie" + i + fila).addClass("bg-default");
                    $("#DeSerie" + i + fila).removeClass("bg-danger");
                } else {
                    $("#DeSerie" + i + fila).removeClass("bg-default");
                    $("#DeSerie" + i + fila).addClass("bg-danger");
                }
            }

            despromedio = despromedio / 5;
            despromedio2 = despromedio2 / 5;


            $("#CNmSerie1" + fila).val(formato_numero(datos[2], DecimalesP[fila], ".", ",", ""));
            $("#CNmSerie2" + fila).val(formato_numero(datos[3], DecimalesP[fila], ".", ",", ""));
            $("#CNmSerie3" + fila).val(formato_numero(datos[4], DecimalesP[fila], ".", ",", ""));
            $("#CNmSerie4" + fila).val(formato_numero(datos[5], DecimalesP[fila], ".", ",", ""));
            $("#CNmSerie5" + fila).val(formato_numero(datos[6], DecimalesP[fila], ".", ",", ""));
            $("#CNmTiempo" + fila).val(formato_numero(datos[7], DecimalesP[fila], ".", ",", ""));
            $("#CNmPosicion" + fila).val(formato_numero(datos[8], DecimalesP[fila], ".", ",", ""));
            $("#CNmBrazo" + fila).val(formato_numero(despromedio2, DecimalesP[fila], ".", ",", ""));

            $("#CMeSerie1" + fila).val(formato_numero(datos[2] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMeSerie2" + fila).val(formato_numero(datos[3] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMeSerie3" + fila).val(formato_numero(datos[4] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMeSerie4" + fila).val(formato_numero(datos[5] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMeSerie5" + fila).val(formato_numero(datos[6] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMeTiempo" + fila).val(formato_numero(datos[7] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMePosicion" + fila).val(formato_numero(datos[8] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMeBrazo" + fila).val(formato_numero(despromedio2 / Factor, DecimalesP[fila], ".", ",", ""));



            if (despromedio >= DesPerm2 && despromedio <= DesPerm1) {
                $("#Promedio1" + fila).addClass("bg-default");
                $("#Promedio1" + fila).removeClass("bg-danger");
            } else {
                $("#Promedio1" + fila).removeClass("bg-default");
                $("#Promedio1" + fila).addClass("bg-danger");
            }

            $("#DeSerie1" + fila).val(formato_numero(((lectura * Factor) - datos[2]) * 100 / datos[2], DecimalesP[fila], ".", ",", ""));
            $("#DeSerie2" + fila).val(formato_numero(((lectura * Factor) - datos[3]) * 100 / datos[3], DecimalesP[fila], ".", ",", ""));
            $("#DeSerie3" + fila).val(formato_numero(((lectura * Factor) - datos[4]) * 100 / datos[4], DecimalesP[fila], ".", ",", ""));
            $("#DeSerie4" + fila).val(formato_numero(((lectura * Factor) - datos[5]) * 100 / datos[5], DecimalesP[fila], ".", ",", ""));
            $("#DeSerie5" + fila).val(formato_numero(((lectura * Factor) - datos[6]) * 100 / datos[6], DecimalesP[fila], ".", ",", ""));
            $("#Promedio1" + fila).val(formato_numero(despromedio, DecimalesP[fila], ".", ",", ""));
            $("#Promedio2" + fila).val(formato_numero(despromedio2, DecimalesP[fila], ".", ",", ""));
            $("#Promedio3" + fila).val(formato_numero(despromedio2 / Factor, DecimalesP[fila], ".", ",", ""));

            var fuerza = 0;
            var montaje = 0;
            /*if (Descripcion == "Llave dinamométrica") {
             preguntar
            }*/
            var ctiempo = (TipoInst == "TIPO II" ? Math.abs(despromedio2 - datos[7]) : 0);
            var longitud = despromedio2 - despromedio2;
            var Reproducibilidad = Math.abs(despromedio2 - datos[8]);

            $("#Repetibilidad" + fila).val(formato_numero(datos[11], DecimalesP[fila], ".", ",", ""));
            $("#ReTiempo" + fila).val(formato_numero(ctiempo, DecimalesP[fila], ".", ",", ""));
            $("#ReLongitud" + fila).val(formato_numero(despromedio2 - despromedio2, DecimalesP[fila], ".", ",", ""));
            $("#Reproducibilidad" + fila).val(formato_numero(Reproducibilidad, DecimalesP[fila], ".", ",", ""));
            $("#Resolucion" + fila).val(formato_numero(Resolucion * Factor, DecimalesP[fila], ".", ",", ""));
            $("#ReFuerza" + fila).val(formato_numero(fuerza, DecimalesP[fila], ".", ",", ""));
            $("#ReMontaje" + fila).val(formato_numero(montaje, DecimalesP[fila], ".", ",", ""));
            $("#RePatron" + fila).val(formato_numero(datos[9], DecimalesP[fila], ".", ",", ""));
            $("#ReDerivada" + fila).val(formato_numero(datos[10], DecimalesP[fila], ".", ",", ""));

            var CoRepetibilidad = datos[11] / Math.sqrt(5);
            var CoTiempo = ctiempo / Math.sqrt(3);
            var CoLongitud = longitud / Math.sqrt(3);
            var CoReproducibilidad = Reproducibilidad / Math.sqrt(3);
            var CoResolucion1 = (Resolucion * Factor) / Math.sqrt(12);
            var CoResolucion2 = (TipoInst == "TIPO I" ? CoResolucion1 : 0);
            var CoFuerza = fuerza / Math.sqrt(3);
            var CoMontaje = montaje / Math.sqrt(3);
            var CoPatron = datos[9] / 2;
            var CoDerivada = datos[10] / Math.sqrt(3);
            var CoPorcenjate = 0.0051 / Math.sqrt(3);

            var sumapotencia = (Math.pow(CoRepetibilidad, 4) / 4) + (Math.pow(CoTiempo, 4) / 400) + (Math.pow(CoLongitud, 4) / 400) + (Math.pow(CoReproducibilidad, 4) / 400) +
                (Math.pow(CoResolucion1, 4) / 400) + (Math.pow(CoResolucion2, 4) / 400) + (Math.pow(CoFuerza, 4) / 400) + (Math.pow(CoMontaje, 4) / 400) + (Math.pow(CoPatron, 4) / 400) +
                (Math.pow(CoDerivada, 4) / 400) + (Math.pow(CoPorcenjate, 4) / 400);

            var sumacuadra = (CoRepetibilidad * CoRepetibilidad) + (CoTiempo * CoTiempo) + (CoLongitud * CoLongitud) + (CoReproducibilidad * CoReproducibilidad) +
                (CoResolucion1 * CoResolucion1) + (CoResolucion2 * CoResolucion2) + (CoFuerza * CoFuerza) + (CoMontaje * CoMontaje) + (CoPatron * CoPatron) + (CoDerivada * CoDerivada) + (CoPorcenjate * CoPorcenjate);
            var sumacuadra2 = sumacuadra;
            sumacuadra = Math.sqrt(sumacuadra);

            var GradoLibertad = Math.pow(sumacuadra, 4) / sumapotencia;
            var GradoLibertad2 = sumapotencia;

            var mensaje = (Math.pow(CoRepetibilidad, 4) / 4) + " + " + (Math.pow(CoTiempo, 4) / 400) + " + " + (Math.pow(CoLongitud, 4) / 400) + " + " + (Math.pow(CoReproducibilidad, 4) / 400) + " + " +
                (Math.pow(CoResolucion1, 4) / 400) + " + " + (Math.pow(CoResolucion2, 4) / 400) + " + " + (Math.pow(CoFuerza, 4) / 400) + " + " + (Math.pow(CoMontaje, 4) / 400) + " + " + (Math.pow(CoPatron, 4) / 400) + " + " +
                (Math.pow(CoDerivada, 4) / 400) + " + " + (Math.pow(CoPorcenjate, 4) / 400);
                                                
            $("#CoRepetibilidad" + fila).val(formato_numero(CoRepetibilidad, DecimalesP[fila], ".", ",", ""));
            $("#CoTiempo" + fila).val(formato_numero(CoTiempo, DecimalesP[fila], ".", ",", ""));
            $("#CoLongitud" + fila).val(formato_numero(CoLongitud, DecimalesP[fila], ".", ",", ""));
            $("#CoReproducibilidad" + fila).val(formato_numero(CoReproducibilidad, DecimalesP[fila], ".", ",", ""));
            $("#CoResolucion1" + fila).val(formato_numero(CoResolucion1, DecimalesP[fila], ".", ",", ""));
            $("#CoResolucion2" + fila).val(formato_numero(CoResolucion2, DecimalesP[fila], ".", ",", ""));
            $("#CoFuerza" + fila).val(formato_numero(CoFuerza, DecimalesP[fila], ".", ",", ""));
            $("#CoMontaje" + fila).val(formato_numero(CoMontaje, DecimalesP[fila], ".", ",", ""));
            $("#CoPatron" + fila).val(formato_numero(CoPatron, DecimalesP[fila], ".", ",", ""));
            $("#CoDerivada" + fila).val(formato_numero(CoDerivada, DecimalesP[fila], ".", ",", ""));
            $("#CoPorcenjate" + fila).val(formato_numero(CoPorcenjate, DecimalesP[fila], ".", ",", ""));

            $("#InNCombinada" + fila).val(formato_numero(sumacuadra, DecimalesP[fila], ".", ",", ""));
            $("#InNGrados" + fila).val(formato_numero(GradoLibertad, DecimalesP[fila], ".", ",", ""));
            TStuden = Calcular_TStudent(GradoLibertad);
            $("#InNFactor" + fila).val(formato_numero(TStuden, DecimalesP[fila], ".", ",", ""));
            $("#InNExpandida1" + fila).val(formato_numero(sumacuadra * TStuden, DecimalesP[fila], ".", ",", ""));

            var expandida2 = sumacuadra * TStuden;

            $("#InNExpandida2" + fila).val(formato_numero(expandida2, calculardecimal(expandida2), ".", ",", ""));
                        
            var promeestima = (datos[2] * 1 + datos[3] * 1 + datos[4] * 1 + datos[5] * 1 + datos[6] * 1) / 5;
            $("#InNPromedio" + fila).val(formato_numero(promeestima, 3, ".", ",", ""));

            var expandida3 = NumeroDecimal($("#InNExpandida2" + fila).val()) * 100 / promeestima;
            $("#InNExpandida3" + fila).val(formato_numero(expandida3, calculardecimal(expandida3), ".", ",", ""));
            $("#InNDesviacion" + fila).val(formato_numero(despromedio, _DE,_CD,_CM,""));


            $("#InOCombinada" + fila).val(formato_numero(sumacuadra / Factor, DecimalesP[fila], ".", ",", ""));
            $("#InOGrados" + fila).val(formato_numero(GradoLibertad, 1, ".", ",", ""));
            $("#InOFactor" + fila).val(formato_numero(TStuden, 1, ".", ",", ""));
            $("#InOExpandida1" + fila).val(formato_numero((sumacuadra * TStuden) / Factor, DecimalesP[fila], ".", ",", ""));

            expandida2 = (sumacuadra * TStuden) / Factor;
            //$.jGrowl(expandida2, { life: 2500, theme: 'growl-success', header: '' });

            $("#InOExpandida2" + fila).val(formato_numero(expandida2, calculardecimal(expandida2), ".", ",", ""));
            $("#InOPromedio" + fila).val(formato_numero(promeestima / Factor, 3, ".", ",", ""));
            
            expandida3 = NumeroDecimal($("#InOExpandida2" + fila).val()) * 100 / (promeestima / Factor);
            $("#InOExpandida3" + fila).val(formato_numero(expandida3, calculardecimal(expandida3), ".", ",", ""));
            $("#InODesviacion" + fila).val(formato_numero(despromedio, 1, ".", ",", ""));
                       

        } else {
            swal("Acción Cancelada", datos[1], "warning");
            despatron = "";
            $("#Patron" + fila).val("").trigger("change");
        }
    }

    $("#NmSerie1" + fila).val(formato_numero(serie1 * Factor, DecimalesP[fila], ".", ",", ""));
    $("#NmSerie2" + fila).val(formato_numero(serie2 * Factor, DecimalesP[fila], ".", ",", ""));
    $("#NmSerie3" + fila).val(formato_numero(serie3 * Factor, DecimalesP[fila], ".", ",", ""));
    $("#NmSerie4" + fila).val(formato_numero(serie4 * Factor, DecimalesP[fila], ".", ",", ""));
    $("#NmSerie5" + fila).val(formato_numero(serie5 * Factor, DecimalesP[fila], ".", ",", ""));
    $("#NmTiempo" + fila).val(formato_numero(tiempo * Factor, DecimalesP[fila], ".", ",", ""));
    $("#NmBrazo" + fila).val(formato_numero((Descripcion == "Llave dinamométrica" ? brazo * Factor : 0), DecimalesP[fila], ".", ",", ""));
    $("#NmPosicion" + fila).val(formato_numero(posicion * Factor, DecimalesP[fila], ".", ",", ""));

    if (AplicarTemporal == 1)
        GuardaTemp = 1;

    

    Graficar();
    GraficarDesviacion();
        
        
}


function calculardecimal(numero) {
    numero = $.trim(numero);
    var contador = 0;
    var punto = 0;
    decimal = 0;
    for (var i = 0; i < numero.length; i++) {
        if (numero.charAt(i) != "." && numero.charAt(i) != ",") {
            if (numero.charAt(i) * 1 > 0 || contador > 0) {
                contador++;
                if (punto > 0)
                    decimal++;
                if (contador == 2)
                    break;
            }
        } else
            punto++;
    }
    //$.jGrowl(numero, { life: 5500, theme: 'growl-success', header: '' });
    return decimal;
}


function Graficar() {
    
    var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)'];
    var varserie = 0;
    var TablaGrafica = "";

    var config = {
        type: 'scatter',
        data: {
            datasets: []
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'PAR TORSIONAL NOMINAL (' + $("#Medida").html() + ') VS DESVIACIONES (%)'
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            legend: {
                position: 'right',
                label: {
                    FontSize: '20px'
                }
            }
        },
        lineAtIndex: 2
    }

    TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">EPM(+)</th>';
    
    var newDataset = {
        label: 'EPM(+)',
        fill: false,
        backgroundColor: colores[0],
        borderColor: colores[0],
        borderDash: [5, 5],
        data: [],
    };
    var punto;
    for (var i = 1; i <= CantRegitro; i++) {
        TablaGrafica += "<td>" + NumeroDecimal($("#PaLectura" + i).val()) + " ; " + DesPerm1 + "</td>"; 
        punto = {
            x: NumeroDecimal($("#PaLectura" + i).val()),
            y: DesPerm1
        }
        newDataset.data.push(punto);
    }
    config.data.datasets.push(newDataset);

    
    for (var serie = 1; serie <= 5; serie++) {
        TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">Serie' + serie + '</th>';
        newDataset = {
            label: 'Serie ' + serie,
            fill: false,
            borderColor: colores[serie],
            backgroundColor: colores[serie],
            data: [],
        };
        for (var i = 1; i <= CantRegitro; i++) {
            varserie = ($("#DeSerie" + serie + i).val() != "Infinity" ? NumeroDecimal($("#DeSerie" + serie + i).val()) : 0);
            TablaGrafica += "<td>" + NumeroDecimal($("#PaLectura" + i).val()) + " ; " + varserie + "</td>"; 
            punto = {
                x: NumeroDecimal($("#PaLectura" + i).val()),
                y: varserie
            }
            newDataset.data.push(punto);
        }
        config.data.datasets.push(newDataset);
    }

    TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">EPM(-)</th>';
    
    newDataset = {
        label: 'EPM(-)',
        fill: false,
        borderColor: colores[0],
        backgroundColor: colores[0],
        borderDash: [5, 5],
        data: [],
    };
    for (var i = 1; i <= CantRegitro; i++) {
        TablaGrafica += "<td>" + NumeroDecimal($("#PaLectura" + i).val()) + " ; " + DesPerm2 + "</td>"; 
        punto = {
            x: NumeroDecimal($("#PaLectura" + i).val()),
            y: DesPerm2
        }
        newDataset.data.push(punto);
    }
    TablaGrafica += "</tr>";
    config.data.datasets.push(newDataset);

    if (window.myLine != undefined) {
        window.myLine.destroy();
    }

    window.myLine = new Chart(ctxgra, config);   
    window.myLine.update();

    $("#TBGrafica1").html(TablaGrafica);

    

}

function GraficarDesviacion() {

    var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)', 'rgb(156, 156, 156)'];
    var varserie = 0;
    var TablaGrafica;
    var config = {
        type: 'scatter',
        data: {
            datasets: []
        },
        options: {
            legendCallback: function (chart) {
                '<b>Prueba de leyenda en html</b>'
            },
            
            responsive: true,
            
            title: {
                display: true,
                text: 'PAR TORSIONAL NOMINAL (' + $("#Medida").html() + ') VS DESVIACION PROMEDIO (%)'
            },
            scales: {
                xAxes: [{
                    display: true,
                    labelString: 'Par torsional nominal en lbf·ft'

                }],
                yAxes: [{
                    display: true,
                    labelString: 'Desviación ± U expandida en %'
                }]
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            legend: {
                position: 'right'
            }
        },
        lineAtIndex: 2
    }

    TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">EPM(+)</th>';

    var newDataset = {
        label: 'EPM(+)',
        fill: false,
        backgroundColor: colores[0],
        borderColor: colores[0],
        borderDash: [5, 5],
        data: [],
    };
    var punto;
    for (var i = 1; i <= CantRegitro; i++) {
        TablaGrafica += "<td>" + NumeroDecimal($("#PaLectura" + i).val()) + " ; " + DesPerm1 + "</td>"; 
        punto = {
            x: NumeroDecimal($("#InNLectura" + i).html()),
            y: DesPerm1
        }
        newDataset.data.push(punto);
    }
    config.data.datasets.push(newDataset);

    TablaGrafica += "</tr>";
    TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">Desviación Promedi</th>';

    var newDataset = {
        label: 'Desviación Promedio',
        fill: false,
        backgroundColor: colores[4],
        borderColor: colores[4],
        data: [],
    };
    var punto;
    for (var i = 1; i <= CantRegitro; i++) {
        TablaGrafica += "<td>" + NumeroDecimal($("#InNLectura" + i).html()) + " ; " + NumeroDecimal($("#InNDesviacion" + i).val()) + "</td>"; 
        punto = {
            x: NumeroDecimal($("#InNLectura" + i).html()),
            y: NumeroDecimal($("#InNDesviacion" + i).val())
        }
        newDataset.data.push(punto);
    }
    config.data.datasets.push(newDataset);

    TablaGrafica += "</tr>";

    TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">EPM(-)</th>';

    newDataset = {
        label: 'EPM(-)',
        fill: false,
        borderColor: colores[0],
        backgroundColor: colores[0],
        borderDash: [5, 5],
        data: [],
    };
    for (var i = 1; i <= CantRegitro; i++) {
        TablaGrafica += "<td>" + NumeroDecimal($("#PaLectura" + i).val()) + " ; " + DesPerm2 + "</td>"; 
        punto = {
            x: NumeroDecimal($("#InNLectura" + i).html()),
            y: DesPerm2
        }
        newDataset.data.push(punto);
    }
    config.data.datasets.push(newDataset);

    TablaGrafica += "</tr>";

    if (window.myDesviacion != undefined) {
        window.myDesviacion.destroy();
    }

    window.myDesviacion = new Chart(ctdesviacion, config);
    window.myDesviacion.update();

   
    for (var fila = 1; fila <= CantRegitro; fila++) {
        newDataset = {
            display: false,
            label:'Error',
            borderColor: colores[7],
            backgroundColor: colores[7],
            data: [],
        };
        varserie = NumeroDecimal($("#InNDesviacion" + fila).val()) - NumeroDecimal($("#InNExpandida3" + fila).val());
        punto = {
            x: NumeroDecimal($("#InNLectura" + fila).html()),
            y: varserie
        }
        newDataset.data.push(punto);

        punto = {
            x: NumeroDecimal($("#InNLectura" + fila).html()),
            y: NumeroDecimal($("#InNDesviacion" + fila).val())
        }
        newDataset.data.push(punto);

        varserie = NumeroDecimal($("#InNDesviacion" + fila).val()) + NumeroDecimal($("#InNExpandida3" + fila).val());
        punto = {
            x: NumeroDecimal($("#InNLectura" + fila).html()),
            y: varserie
        }
        newDataset.data.push(punto);

        config.data.datasets.push(newDataset);
    }
    

    if (window.myDesviacion != undefined) {
        window.myDesviacion.destroy();
    }

    window.myDesviacion = new Chart(ctdesviacion, config);
    window.myDesviacion.update();

    $("#TBGrafica2").html(TablaGrafica);
    

}


function CambioPatron(fila) {
    CalcularSerie("", fila);
    GuardarTemporal();
}


//$.connection.hub.start().done(function () {
$("#FormPrevia").submit(function (e) {
    e.preventDefault();

    if (Certificado != "") {
        swal("Acción Cancelada", "Ya se generó el certificado de este ingreso", "warning");
        return false;
    }

    LeerCondiciones(1);

    if (TempMax == 0 || TempMin == 100000000 || HumeMax == 0 || HumeMin == 100000000) {
        swal("Acción Cancelada", "No hay condiciones ambientales cargadas", "warning");
        return false;
    }


    var mensaje = "";
    if (TempVar > 1 || HumeMax >= 75)
        mensaje = "temperatura y humedad";
    else {
        if (TempVar == 1)
            mensaje = "temperatura";
        if (HumeMax >= 75)
            mensaje = "humedad";
    }

    if (mensaje != "") {
        $("#documento").focus();
        swal("Acción Cancelada", "Condiciones de " + mensaje + " no adecuadas, para la calibración... No podrá General el certificado", "warning");
        EscucharMensaje("Condiciones de " + mensaje + " no adecuadas, para la calibración. No podrá General el certificado");
        return false;
    }


    ActivarLoad();
    setTimeout(function () {
        d = new Date();
        HoraFinal = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
        tiempo = RestarHoras(HoraInicio, HoraFinal);
        var parametros = $("#FormPrevia").serialize().replace(/\%2C/g, "") + "&id=" + IdCalibracion + "&HoraIni=" + HoraInicio + "&HoraFinal=" + HoraFinal + "&TiempoTrans=" + tiempo + "&magnitud=6" +
            "&FechaRec=" + $("#FechaIng").html() + "&Factor=" + Factor + "&revision=" + Revision;

        var datos = LlamarAjax("Laboratorio/GuardaCertificadoPT1", parametros).split("|");
        DesactivarLoad();
        if (datos[0] == "0") {
            if (IdCalibracion == 0) {
                $("#Certificado").html(datos[3]);
                parametros = "tempmax=" + $("#CoTempMax").val().replace(".", ",") + "&tempmin=" + $("#CoTempMin").val().replace(".", ",") + "&tempvar=" + formato_numero(TempVar, 1, ",", ".", "") + " °C" +
                    "&humemax=" + $("#CoHumeMax").val().replace(".", ",") + "&humemin=" + $("#CoHumeMin").val().replace(".", ",") + "&humevar=" + formato_numero(HumeVar, 1, ",", ".", "") + " %HR&ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision;
                LlamarAjax("Laboratorio/GuardarTemperatura", parametros);
            }
            
            clearInterval(IntervaloTiempo);
            IdCalibracion = datos[2];
            GuardaTemp = 0;
            $("#CaTiempo").val(tiempo);
            $("#CaHoraFin").val(HoraFinal);
            localStorage.removeItem(NumCertificado + "PlaPT1" + Ingreso);
            swal("", datos[1], "success");
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    }, 15);
});

function CambioDecimales(decimal, fila) {
    DecimalesP[fila] = decimal
    for (var x = 0; x <= 5; x++) {
        if (fila == (CantRegitro-1))
            $("#PrSerie" + x).attr("onblur", "FormatoSalida2(this," + decimal + ")");

        $("#Serie" + x + fila).attr("onblur", "FormatoSalida(this," + decimal + "," + NumCertificado + ")");
        $("#Serie" + x + fila).attr("onblur", "FormatoSalida(this," + decimal + "," + NumCertificado + ")");
    }
    $("#Tiempo" + fila).attr("onblur", "FormatoSalida(this," + decimal + "," + NumCertificado + ")");
    $("#Posicion" + fila).attr("onblur", "FormatoSalida(this," + decimal + "," + NumCertificado + ")");
    $("#Brazo" + fila).attr("onblur", "FormatoSalida(this," + decimal + "," + NumCertificado + ")");
    if (fila == (CantRegitro - 1))
        $("#PrPosicion").attr("onblur", "FormatoSalida(this," + decimal + "," + NumCertificado + ")");

    GuardarTemporal();
}

function TablaCMC() {
    var datos = LlamarAjax("Laboratorio/TablaCMC", "magnitud=6");
    var resultado = "";
    var data = JSON.parse(datos);
    for (var x = 0; x < data.length; x++) {
        resultado += "<tr>" +
            "<td>></td>" +
            "<td align='center'>" + data[x].desde + "</td>" +
            "<td align='center'>" + data[x].hasta + "</td>" +
            "<td align='center'>" + data[x].valor + "</td></tr>";
    }
    $("#TBPublicadaONAC").html(resultado);
}

function ConfigurarTablas(can) {
    var resultado = "";
    var resultado2 = "";
    var resultado3 = "";
    var resultado4 = "";
    var resultado5 = "";
    var resultado6 = "";
    var resultado7 = "";
    var resultado8 = "";
    var resultado9 = "";
    var resultado10 = "";

    var resultado20 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >&nbsp;</th>';
    var resultado21 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Alcance en N·m:</th>';
    var resultado22 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Marca:</th>';
    var resultado23 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Modelo:</th>';
    var resultado24 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Serie:</th>';
    var resultado25 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Número de Certificado:</th>';
    var resultado26 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Laboratorio:</th>';
    var resultado27 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Fecha de  Calibración:</th>';
    var resultado28 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Próxima  Calibración:</th>';
    var resultado29 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Resolución:</th>';
    
    var patrones = CargarCombo(20, 1, "", "6");
    for (var x = 1; x <= can; x++) {
        resultado += '<tr class="tabcertr">' +
            '<th class="text-center" > <input type="text" name="PaPorLectura"  id="PaPorLectura' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,1)" onkeyup="CalcularSerie(this,' + x + ')" class="form-control sinbordecon text-XX2 text-center bg-amarillo"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="PaLectura" id="PaLectura' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2" id="PaPunto' + x + '">&nbsp;</th>' +
            '<th>' +
            '<select style="width:100%" id="Patron' + x + '" required name="Patron" onchange="CambioPatron(' + x + ')">' +
            patrones +
            '</select>' +
            '</th>' +
            '<th class="text-center text-XX2"><input type="text" name="UPatron" id="UPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="DPatron" id="DPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="RPatron" id="RPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="IPatron" id="IPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="DecimalPatron" id="DecimalPatron' + x + '" required onfocus="FormatoEntrada(this, 1)" onchange="CambioDecimales(this.value,' + x + ')" onblur="FormatoSalida(this,0)" onkeyup="ValidarTexto(this,3)" class="form-control sinbordecon text-XX text-center bg-amarillo"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="C1Patron" id="C1Patron' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="C2Patron" id="C2Patron' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="C3Patron" id="C3Patron' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="C4Patron" id="C4Patron' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '</tr>';

        resultado2 += '<tr class="tabcertr">' +
            '<th class="text-center bg-gris" id="SePorcentaje' + x + '">&nbsp;</th>' +
            '<td class="text-center bg-gris text-XX2" id="SeLectura' + x + '">&nbsp;</td>' +
            '<th class="text-center"><input type="text" onchange="GuardarTemporal()" name="Serie1" id="Serie1' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(this, ' + x + ')" class="form-control sinbordecon text-XX2 text-center bg-amarillo" /></th>' +
            '<th class="text-center"><input type="text" onchange="GuardarTemporal()" name="Serie2" id="Serie2' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(this,' + x + ')" class="form-control sinbordecon text-XX2 text-center bg-amarillo" /></th>' +
            '<th class="text-center"><input type="text" onchange="GuardarTemporal()" name="Serie3" id="Serie3' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(this,' + x + ')" class="form-control sinbordecon text-XX2 text-center bg-amarillo" /></th>' +
            '<th class="text-center"><input type="text" onchange="GuardarTemporal()" name="Serie4" id="Serie4' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(this,' + x + ')" class="form-control sinbordecon text-XX2 text-center bg-amarillo" /></th>' +
            '<th class="text-center"><input type="text" onchange="GuardarTemporal()" name="Serie5" id="Serie5' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(this,' + x + ')" class="form-control sinbordecon text-XX2 text-center bg-amarillo" /></th>' +
            '<th class="text-center"><input type="text" onchange="GuardarTemporal()" name="Tiempo" id="Tiempo' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(this,' + x + ')" class="form-control sinbordecon text-XX2 text-center bg-amarillo" /></th>' +
            '<th class="text-center"><input type="text" onchange="GuardarTemporal()" name="Brazo" id="Brazo' + x + '" required onfocus="FormatoEntrada(this, 1)"   onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(this,' + x + ')" class="form-control sinbordecon text-XX2 text-center bg-amarillo" /></th>' +
            '<th class="text-center"><input type="text" onchange="GuardarTemporal()" name="Posicion" id="Posicion' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(this,' + x + ')" class="form-control sinbordecon text-XX2 text-center bg-amarillo" /></th>' +
            '</tr>';
        resultado3 += '<tr class="tabcertr">' +
            '<th class="text-center bg-gris text-XX2" id="NnPorcencate' + x + '"></th>' +
            '<td class="text-center bg-gris text-XX2" id="NnLectura' + x + '"></td>' +
            '<th class="text-center text-XX2"><input type="text" name="NmSerie1" id="NmSerie1' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="NmSerie2" id="NmSerie2' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="NmSerie3" id="NmSerie3' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="NmSerie4" id="NmSerie4' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="NmSerie5" id="NmSerie5' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="NmTiempo" id="NmTiempo' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="NmBrazo" id="NmBrazo' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="NmPosicion" id="NmPosicion' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '</tr>';
        resultado4 += '<tr class="tabcertr">' +
            '<th class="text-center bg-gris text-XX2" id="CMePorcencate' + x + '" ></th>' +
            '<td class="text-center bg-gris text-XX2" id="CMeLectura' + x + '"></td>' +
            '<th class="text-center text-XX2"><input type="text" name="CMeSerie1" id="CMeSerie1' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CMeSerie2" id="CMeSerie2' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CMeSerie3" id="CMeSerie3' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CMeSerie4" id="CMeSerie4' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CMeSerie5" id="CMeSerie5' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CMeTiempo" id="CMeTiempo' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CMeBrazo" id="CMeBrazo' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CMePosicion" id="CMePosicion' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '</tr>';
        resultado5 += '<tr class="tabcertr">' +
            '<th class="text-center bg-gris text-XX2" id="CNmPorcencate' + x + '" ></th>' +
            '<td class="text-center bg-gris text-XX2" id="CNmLectura' + x + '"></td>' +
            '<th class="text-center text-XX2"><input type="text" name="CNmSerie1" id="CNmSerie1' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CNmSerie2" id="CNmSerie2' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CNmSerie3" id="CNmSerie3' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CNmSerie4" id="CNmSerie4' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CNmSerie5" id="CNmSerie5' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CNmTiempo" id="CNmTiempo' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CNmBrazo" id="CNmBrazo' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CNmPosicion" id="CNmPosicion' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '</tr>';
        resultado6 += '<tr class="tabcertr">' +
            '<th class="text-center bg-gris text-XX2" id = "DePorcentaje' + x + '" >&nbsp;</th>' +
            '<td class="text-center bg-gris text-XX" id="DeLectura' + x + '"></td>' +
            '<th class="text-center text-XX2"><input type="text" name="DeSerie1" id="DeSerie1' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="DeSerie2" id="DeSerie2' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="DeSerie3" id="DeSerie3' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="DeSerie4" id="DeSerie4' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="DeSerie5" id="DeSerie5' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="Promedio1" id="Promedio1' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="Promedio2" id="Promedio2' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="Promedio3" id="Promedio3' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '</tr>';
        resultado7 += '<tr class="tabcertr">' +
            '<th class="text-center bg-gris text-XX2" id = "ReLectura' + x + '" >&nbsp;</th>' +
            '<td class="text-center text-XX2"><input type="text" name="Repetibilidad" id="Repetibilidad' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="ReTiempo" id="ReTiempo' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="ReLongitud" id="ReLongitud' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="Reproducibilidad" id="Reproducibilidad' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="Resolucion" id="Resolucion' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="ReFuerza" id="ReFuerza' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="ReMontaje" id="ReMontaje' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="RePatron" id="RePatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="ReDerivada" id="ReDerivada' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '</tr>'; 

        resultado8 += '<tr class="tabcertr">' +
            '<th class="text-center text-XX2"><input type="text" name="CoRepetibilidad" id="CoRepetibilidad' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<td class="text-center text-XX2"><input type="text" name="CoTiempo" id="CoTiempo' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CoLongitud" id="CoLongitud' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CoReproducibilidad" id="CoReproducibilidad' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CoResolucion1" id="CoResolucion1' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CoResolucion2" id="CoResolucion2' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CoFuerza" id="CoFuerza' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CoMontaje" id="CoMontaje' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CoPatron" id="CoPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CoDerivada" id="CoDerivada' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="CoPorcenjate" id="CoPorcenjate' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '</tr>'; 

        resultado9 += '<tr class="tabcertr">' +
            '<th class="text-center bg-gris text-XX2" id = "InOPorcentaje' + x + '" >&nbsp;</th>' +
            '<td class="text-center bg-gris text-XX2" id="InOLectura' + x + '">&nbsp;</td>' +
            '<th class="text-center text-XX2"><input type="text" name="InOPromedio" id="InOPromedio' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="InODesviacion" id="InODesviacion' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="InOCombinada" id="InOCombinada' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="InOGrados" id="InOGrados' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="InOFactor" id="InOFactor' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="InOExpandida1" id="InOExpandida1' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="InOExpandida2" id="InOExpandida2' + x + '" readonly class="form-control sinbordecon text-XX2 text-center text-info"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="InOExpandida3" id="InOExpandida3' + x + '" readonly class="form-control sinbordecon text-XX2 text-center text-info"/></th>' +
            '</tr>'; 
        resultado10 += '<tr class="tabcertr">' +
            '<th class="text-center bg-gris text-XX2" id = "InNPorcentaje' + x + '" >&nbsp;</th>' +
            '<td class="text-center bg-gris text-XX2" id="InNLectura' + x + '">&nbsp;</td>' +
            '<th class="text-center text-XX2"><input type="text" name="InNPromedio" id="InNPromedio' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="InNDesviacion" id="InNDesviacion' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="InNCombinada" id="InNCombinada' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="InNGrados" id="InNGrados' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="InNFactor" id="InNFactor' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="InNExpandida1" id="InNExpandida1' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="InNExpandida2" id="InNExpandida2' + x + '" readonly class="form-control sinbordecon text-XX2 text-center text-info"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="InNExpandida3" id="InNExpandida3' + x + '" readonly class="form-control sinbordecon text-XX2 text-center text-info"/></th>' +
            '</tr>'; 
        resultado20 += '<th width="25%" class="text-center bg-gris">Transductor' + x + '</th>';
        resultado21 += '<th width="25%" class="text-info" id="PValor1' + x + '">&nbsp;</th>';
        resultado22 += '<th width="25%" class="text-info" id="PValor2' + x + '">&nbsp;</th>';
        resultado23 += '<th width="25%" class="text-info" id="PValor3' + x + '">&nbsp;</th>';
        resultado24 += '<th width="25%" class="text-info" id="PValor4' + x + '">&nbsp;</th>';
        resultado25 += '<th width="25%" class="text-info" id="PValor5' + x + '">&nbsp;</th>';
        resultado26 += '<th width="25%" class="text-info" id="PValor6' + x + '">&nbsp;</th>';
        resultado27 += '<th width="25%" class="text-info" id="PValor7' + x + '">&nbsp;</th>';
        resultado28 += '<th width="25%" class="text-info" id="PValor8' + x + '">&nbsp;</th>';
        resultado29 += '<th width="25%" class="text-info" id="PValor9' + x + '">&nbsp;</th>';
    }

    resultado20 += '</tr>';
    resultado21 += '</tr>';
    resultado22 += '</tr>';
    resultado23 += '</tr>';
    resultado24 += '</tr>';
    resultado25 += '</tr>';
    resultado26 += '</tr>';
    resultado27 += '</tr>';
    resultado28 += '</tr>';
    resultado29 += '</tr>';

    
    $("#TBpatron").html(resultado);
    $("#TBSerie").html(resultado2);
    $("#LecturaNm").html(resultado3);
    $("#LecturaCMe").html(resultado4);
    $("#LecturaCNm").html(resultado5);
    $("#TBDesviacion").html(resultado6);
    $("#TBRepetibilidad").html(resultado7);
    $("#TBContribucion").html(resultado8);
    $("#TBEstimacion0").html(resultado9);
    $("#TBEstimacionN").html(resultado10);
    
    $("#TBodyPatron").html(resultado20 + resultado21 + resultado22 + resultado23 + resultado24 + resultado25 + resultado26 + resultado27 + resultado28 + resultado29);
    $('select').select2();
}
    
function VerFotos() {
    if (Ingreso == 0)
        return false;
    CargarModalFoto(Ingreso, Foto,1);
}

$('select').select2();

if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
    (function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-28909194-3', 'auto');
    ga('send', 'pageview');
}

function CalcularTemperatura(guardar) {

    if (SensorLectura == "Excel") {
        $.jGrowl("Condiciones de temperatura y humedad obtenidas de forma manual", { life: 2000, theme: 'growl-success', header: '' });
        return false;
    }

    if (window.myTemp != undefined) {
        window.myTemp.destroy();
    }

    if (window.myHume != undefined) {
        window.myHume.destroy();
    }

    TempMax = 0;
    TempMin = 100000000;
    HumeMax = 0;
    HumeMin = 100000000;
    TempVar = 0;
    HumeVar = 0;

    $("#TempMax").val("");
    $("#TempMin").val("");

    $("#HumeMax").val("");
    $("#HumeMin").val("");

    $("#ForMinTemp1").html("");
    $("#ForMinTemp2").html("");
    $("#ForMinTemp3").html("");

    $("#ForMaxTemp1").html("");
    $("#ForMaxTemp2").html("");
    $("#ForMaxTemp3").html("");

    $("#ForMinHume1").html("");
    $("#ForMinHume2").html("");
    $("#ForMinHume3").html("");

    $("#ForMaxHume1").html("");
    $("#ForMaxHume2").html("");
    $("#ForMaxHume3").html("");

    $("#CoTempMax").val("");
    $("#CoTempMin").val("");

    $("#CoTempMax").removeClass("bg-danger");
    $("#CoTempMin").removeClass("bg-danger");

    $("#CoHumeMax").removeClass("bg-danger");
    $("#CoHumeMin").removeClass("bg-danger");

    $("#CoHumeMax").val("");
    $("#CoHumeMin").val("");

    //********************************************************


    var fechaactual = "";
    var datos = LlamarAjax("Laboratorio/DatosGraTemperatura", "ingreso=" + Ingreso + "&revision=" + Revision + "&horai=" + HoraInicio + "&horaf=" + (HoraFinal == "" ? HoraCondicion : HoraFinal) + "&fecha=" + (FechaCal == "" ? FechaActual : FechaCal));


    /*$.ajax({
        url: 'https://www.webstorage-service.com/data/viewcur.html?bsn=429A00BF&rsn=42BC0232',
        dataType: 'json'
    }).then((resultado) => {
        console(resultado);
    });*/

    if (datos != "[]") {
        var data = JSON.parse(datos);

        var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)'];
        var puntos;
        var newDataset;

        fechaactual = data[0].fecha;

        var config = {
            type: 'scatter',
            data: {
                datasets: []
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Gráfica de Temperatura ' + fechaactual
                },
                scales: {
                    xAxes: [{
                        display: true,
                        labelString: 'Par torsional nominal en lbf·ft'

                    }],
                    yAxes: [{
                        display: true,
                        labelString: 'Desviación ± U expandida en %'
                    }]
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
            }
        }
        var config2 = {
            type: 'scatter',
            data: {
                datasets: []
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Gráfica de Humedad ' + fechaactual
                },
                scales: {
                    xAxes: [{
                        display: true,
                        labelString: 'Par torsional nominal en lbf·ft'

                    }],
                    yAxes: [{
                        display: true,
                        labelString: 'Desviación ± U expandida en %'
                    }]
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
            }
        }

        newDataset = {
            label: 'Temperatura',
            fill: false,
            borderColor: colores[1],
            backgroundColor: colores[1],
            data: [],
        };
        for (var i = 0; i < data.length; i++) {

            if (data[i].hora >= HoraInicio && data[i].hora <= (HoraFinal == "" ? HoraCondicion : HoraFinal)) {

                if (data[i].temperatura > TempMax * 1)
                    TempMax = data[i].temperatura;

                if (data[i].temperatura < TempMin * 1)
                    TempMin = data[i].temperatura;

                puntos = {
                    x: data[i].punto,
                    y: data[i].temperatura
                }
                newDataset.data.push(puntos);
            }
        }

        config.data.datasets.push(newDataset);
        if (TempMin == 100000000)
            TempMin = 0;

        $("#TempMax").html(TempMax);
        $("#TempMin").html(TempMin);

        newDataset = {
            label: 'Humedad',
            fill: false,
            borderColor: colores[4],
            backgroundColor: colores[4],
            data: [],
        };
        for (var i = 0; i < data.length; i++) {
            if (data[i].hora >= HoraInicio && data[i].hora <= (HoraFinal == "" ? HoraCondicion : HoraFinal)) {
                if (data[i].humedad > HumeMax * 1)
                    HumeMax = data[i].humedad;

                if (data[i].humedad < HumeMin * 1)
                    HumeMin = data[i].humedad;

                puntos = {
                    x: data[i].punto,
                    y: data[i].humedad
                }
                newDataset.data.push(puntos);
            }
        }

        if (HumeMin * 1 == 100000000)
            HumeMin = 0;


        $("#HumeMax").html(HumeMax);
        $("#HumeMin").html(HumeMin);

        config2.data.datasets.push(newDataset);

        window.myTemp = new Chart(gratemp, config);
        window.myTemp.update();

        window.myHume = new Chart(grahume, config2);
        window.myHume.update();

        ForMinTemp1 = TempMin * TempMin;
        ForMinTemp2 = TempMin;
        ForMinTemp3 = (parseFloat($("#MinTemp1").html()) * (TempMin * TempMin)) + (parseFloat($("#MinTemp2").html()) * TempMin) + parseFloat($("#MinTemp3").html());

        $("#ForMinTemp1").html(formato_numero(ForMinTemp1, 1, ".", ",", ""));
        $("#ForMinTemp2").html(formato_numero(ForMinTemp2, 1, ".", ",", ""));
        $("#ForMinTemp3").html(formato_numero(ForMinTemp3, 4, ".", ",", ""));

        ForMaxTemp1 = TempMax * TempMax;
        ForMaxTemp2 = TempMax;
        ForMaxTemp3 = (parseFloat($("#MaxTemp1").html()) * (TempMax * TempMax)) + (parseFloat($("#MaxTemp2").html()) * TempMax) + parseFloat($("#MaxTemp3").html());

        $("#ForMaxTemp1").html(formato_numero(ForMaxTemp1, 1, ".", ",", ""));
        $("#ForMaxTemp2").html(formato_numero(ForMaxTemp2, 1, ".", ",", ""));
        $("#ForMaxTemp3").html(formato_numero(ForMaxTemp3, 4, ".", ",", ""));

        ForMinHume1 = HumeMin * HumeMin;
        ForMinHume2 = HumeMin;
        ForMinHume3 = (parseFloat($("#MinHume1").html()) * (HumeMin * HumeMin)) + (parseFloat($("#MinHume2").html()) * HumeMin) + parseFloat($("#MinHume3").html());

        $("#ForMinHume1").html(formato_numero(ForMinHume1, 1, ".", ",", ""));
        $("#ForMinHume2").html(formato_numero(ForMinHume2, 1, ".", ",", ""));
        $("#ForMinHume3").html(formato_numero(ForMinHume3, 4, ".", ",", ""));

        ForMaxHume1 = HumeMax * HumeMax;
        ForMaxHume2 = HumeMax;
        ForMaxHume3 = (parseFloat($("#MaxHume1").html()) * (HumeMax * HumeMax)) + (parseFloat($("#MaxHume2").html()) * HumeMax) + parseFloat($("#MaxHume3").html());

        $("#ForMaxHume1").html(formato_numero(ForMaxHume1, 1, ".", ",", ""));
        $("#ForMaxHume2").html(formato_numero(ForMaxHume2, 1, ".", ",", ""));
        $("#ForMaxHume3").html(formato_numero(ForMaxHume3, 4, ".", ",", ""));

        $("#CoTempMax").val(formato_numero(TempMax + ForMaxTemp3, 1, ".", ",", "") + " °C");
        $("#CoTempMin").val(formato_numero(TempMin + ForMinTemp3, 1, ".", ",", "") + " °C");
        TempVar = Math.abs((TempMax + ForMaxTemp3) - (TempMin + ForMinTemp3));
        HumeVar = Math.abs((HumeMax + ForMaxHume3) - (HumeMin + ForMinHume3));
        if (TempVar >= 1) {
            $("#CoTempMax").addClass("bg-danger");
            $("#CoTempMin").addClass("bg-danger");
            ErrorTemp = 1;


        } else {
            ErrorTemp = 0;
            $("#CoTempMax").removeClass("bg-danger");
            $("#CoTempMin").removeClass("bg-danger");
            ErrorTemp = 0;
        }

        if (((HumeMax + ForMaxHume3) >= 75) || ((HumeMin + ForMinHume3) >= 75)) {
            $("#CoHumeMax").addClass("bg-danger");
            $("#CoHumeMin").addClass("bg-danger");
            ErrorHume = 1;
        } else {
            $("#CoHumeMax").removeClass("bg-danger");
            $("#CoHumeMin").removeClass("bg-danger");
            ErrorHume = 0;
        }

        $("#CoHumeMax").val(formato_numero(HumeMax + ForMaxHume3, 1, ".", ",", "") + " %HR");
        $("#CoHumeMin").val(formato_numero(HumeMin + ForMinHume3, 1, ".", ",", "") + " %HR");

        $("#VarTemp").html(formato_numero(TempVar, 1, ".", ",", ""));
        $("#VarHume").html(formato_numero(HumeVar, 1, ".", ",", ""));

        if (guardar == 1) {
            var parametros = "tempmax=" + $("#CoTempMax").val().replace(".", ",") + "&tempmin=" + $("#CoTempMin").val().replace(".", ",") + "&tempvar=" + formato_numero(TempVar, 1, ",", ".", "") + " °C" +
                "&humemax=" + $("#CoHumeMax").val().replace(".", ",") + "&humemin=" + $("#CoHumeMin").val().replace(".", ",") + "&humevar=" + formato_numero(HumeVar, 1, ",", ".", "") + " %HR&ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision;

            LlamarAjax("Laboratorio/GuardarTemperatura", parametros);
        }

        if (Certificado == "") {
            if (ErrorTemp == 0 && ErrorHume == 0) {
                if (guardar == 1) {
                    EscucharMensaje("Condiciones de temperatura y humedad adecuadas para la calibración... Ya puede General el certificado");
                    $.jGrowl("Condiciones de temperatura y humedad adecuadas para la calibración... Ya puede General el certificado", { life: 5500, theme: 'growl-success', header: '' });
                } else {
                    $.jGrowl("Condiciones de temperatura y humedad adecuadas para la calibración", { life: 5500, theme: 'growl-success', header: '' });
                }

            } else {
                var mensaje = "";
                if (ErrorTemp == 1 && ErrorHume == 1)
                    mensaje = "temperatura y humedad";
                else {
                    if (ErrorTemp == 1)
                        mensaje = "temperatura";
                    else
                        mensaje = "humedad";
                }
                EscucharMensaje("Condiciones de " + mensaje + " no adecuadas, para la calibración. No podrá General el certificado");

                swal("Acción Cancelada", "Condiciones de " + mensaje + " no adecuadas, para la calibración... No podrá General el certificado", "warning");
                $("#documento").focus();
            }
        }
    }
}

function VerCondiciones() {
    $("#documento").focus();
}

function AbrirGrafica() {
    var direccion = $("#medidor").val();
    window.open(direccion);

}

function CargarTemperatura() {
    if (Ingreso == 0)
        return
    
    if (Certificado != "") {
        swal("Acción Cancelada", "No se puede editar la temperatura de un certificado", "warning");
        return false;
    }
        
    var documento = $("#documento").val();
    if (documento == "") {
        $("#documento").focus();
        swal("Acción Cancelada", "Debe de seleccionar el archivo CSV de temperatura", "warning");
        return false;
    }

    d = new Date();
    HoraFinal = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
    $("#CaHoraFin").val(HoraFinal);

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision;
        var datos = LlamarAjax("Laboratorio/Temperatura", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]), "success");
            $("#documento").val("");
            CalcularTemperatura(1);
        } else {
            $("#documento").val("");
            swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function ImprimirCertificado(tipo) {
    Ingreso = $("#Ingreso").val() * 1;
    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&tipo=" + tipo;
        var datos = LlamarAjax("RpPlantillas/RpPlanillaPT1", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open(url +  "DocumPDF/" + datos[1]);
        } else {
            if (datos[0] == "9")
                window.open(url + "Certificados/" + datos[1] + ".pdf");
            else
                swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function ImprimirPrevia() {

    if (Ingreso == 0)
        return false;

    if (GuardaTemp == 1) {
        swal("Acción Cancelada", "Debe primero Guardar el informe", "warning");
        return false;
    }

    if (Certificado != "") {
        swal("Acción Cancelada", "No se puede ver la vista previa de un certificado generado", "warning");
        return false;
    }
    

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision;
        var datos = LlamarAjax("RpPlantillas/RpVPPlanillaPT1", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open(url + "DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
    
}


function LlamarGenerarCertificado() {

    if (Ingreso == 0 || IdCalibracion == 0)
        return false;

    if (GuardaTemp == 1) {
        swal("Acción Cancelada", "Debe primero Guardar el informe", "warning");
        return false;
    }

    if (Certificado != "") {
        swal("Acción Cancelada", "Ya se generó el certificado de este ingreso", "warning");
        return false;
    }

    if (TempMax == 0 || TempMin == 100000000 || HumeMax == 0 || HumeMin == 100000000) {
        swal("Acción Cancelada", "Debe cargar el archivo de temperatura y humedad", "warning");
        return false;
    }

    var mensaje = "";
    if (TempVar > 1 || HumeMax >= 75) 
        mensaje = "temperatura y humedad";
    else {
        if (TempVar == 1)
            mensaje = "temperatura";
        if (HumeMax >= 75)
            mensaje = "humedad";
    }

    if (mensaje != "") {
        $("#documento").focus();
        swal("Acción Cancelada", "Condiciones de " + mensaje + " no adecuadas, para la calibración... No podrá General el certificado", "warning");
        EscucharMensaje("Condiciones de " + mensaje + " no adecuadas, para la calibración. No podrá General el certificado");
        return false;
    }

    $("#TipoAccesso").val("GENERAR CERTIFICADO CON FIRMA");
    $("#modalControlAcceso").modal("show");
    $("#AccClave").val("");
    $("#AccClave").focus();
    
}

function GuardarFirma(certificado) {
    var firma = canvafirma.toDataURL("image/png");
    $.ajax({
        type: 'POST',
        url: url + "Laboratorio/GuardarFirma",
        data: "firma=" + firma + "&certificado=" + certificado + "&usuario=" + localStorage.getItem("UsuarioCertificado"),
        success: function (msg) {
            respuesta = "si";
        }
    });
}


function GenerarCertificado() {
        
    if (Ingreso == 0 || IdCalibracion == 0)
        return false;

    if (GuardaTemp == 1) {
        swal("Acción Cancelada", "Debe primero Guardar el informe", "warning");
        return false;
    }

    if (Certificado != "") {
        swal("Acción Cancelada", "Ya se generó el certificado de este ingreso", "warning");
        return false;
    }

    if (TempMax == 0 || TempMin == 100000000 || HumeMax == 0 || HumeMin == 100000000) {
        swal("Acción Cancelada", "Debe cargar el archivo de temperatura y humedad", "warning");
        return false;
    }

    var mensaje = "";
    if (TempVar > 1 || HumeMax >= 75)
        mensaje = "temperatura y humedad";
    else {
        if (TempVar == 1)
            mensaje = "temperatura";
        if (HumeMax >= 75)
            mensaje = "humedad";
    }

    if (mensaje != "") {
        $("#documento").focus();
        swal("Acción Cancelada", "Condiciones de " + mensaje + " no adecuadas, para la calibración... No podrá General el certificado", "warning");
        EscucharMensaje("Condiciones de " + mensaje + " no adecuadas, para la calibración. No podrá General el certificado");
        return false;
    }
        
    if (localStorage.getItem("FirmaCertificado") == "") {
        swal("Acción Cancelada", "Debe primero firmar el certificado", "warning");
        return false;
    }

    alert(IdVersion);

    Solicitante = Solicitante.replace(/&/g, '%26');
    Direccion = Direccion.replace(/&/g, '%26');
        
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea generar el certificado del ingreso número ' + Ingreso + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Laboratorio/GenerarCertificado", "ingreso=" + Ingreso + "&revision=" + Revision + "&numcertificado=" + NumCertificado + "&NombreCer=" + Solicitante + "&DireccionCer=" + Direccion +
                    "&usuario=" + localStorage.getItem("UsuarioCertificado") + "&version=" + IdVersion)
                    .done(function (data) {
                        var datos = data.split("|");

                        if (datos[0] == "0") {
                            Certificado = datos[2];
                            GuardarFirma(Certificado);
                            localStorage.setItem("FirmaCertificado", "");
                            clearCanvas(canvas, ctx);
                            $("#FirmarCertificadoPT").addClass("hidden");
                            $("#PrSerie1").prop("disabled", true);
                            $("#PrSerie2").prop("disabled", true);
                            $("#PrSerie3").prop("disabled", true);
                            $("#PrSerie4").prop("disabled", true);
                            $("#PrSerie5").prop("disabled", true);
                            $("#PrPosicion").prop("disabled", true);
                            $("#ProximaCali").prop("disabled", true);
                            $("#Observacion").prop("disabled", true);
                            $("#TiempoCali").prop("disabled", true);
                            for (var x = 1; x <= CantRegitro; x++) {
                                $("#Serie1" + x).prop("disabled", true);
                                $("#Serie2" + x).prop("disabled", true);
                                $("#Serie3" + x).prop("disabled", true);
                                $("#Serie4" + x).prop("disabled", true);
                                $("#Serie5" + x).prop("disabled", true);
                                $("#Posicion" + x).prop("disabled", true);
                                $("#Brazo" + x).prop("disabled", true);
                                $("#Tiempo" + x).prop("disabled", true);
                                $("#PaPorLectura" + x).prop("disabled", true);
                                $("#Patron" + x).prop("disabled", true);
                            }
                            $("#Certificado").html(Certificado);
                            $("#FechaExp").html(FechaActual);
                            $.jGrowl(datos[1] + " " + datos[2], { life: 1500, theme: 'growl-success', header: '' });
                            EscucharMensaje(datos[1]);
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

TablaCMC();

function TiempoTrascurrudi() {
    if (Ingreso == 0 || IdCalibracion > 0 || HoraInicio == "")
        return false;
    d = new Date();
    horaactual = (d.getHours()*1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes()*1 < 10 ? "0" : "") + d.getMinutes();
    $("#CaTiempo").val(RestarHoras(HoraInicio, horaactual));
    LeerCondiciones(0);
}


if (localStorage.getItem("CerIngreso") * 1 > 0) {
    LimpiarTodo();
    $("#Ingreso").val(localStorage.getItem("CerIngreso"));
    BuscarIngreso(localStorage.getItem("CerIngreso"), "", 0);
}

$(document).ready(function () {
    $("input").on('paste', function (e) {
        e.preventDefault();
        swal('Esta acción está prohibida');
    })

})

function pulsar(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    return (tecla != 13);
}

function LeerCondiciones(tipo) {
    if (Ingreso == 0 || Certificado != "")
        return false;
    if (tipo == 0 && HoraFinal != "") {
        return false;
    }
    if (tipo == 1 && HoraFinal != "")
        HoraCondicion = HoraFinal;

    var datos = LlamarAjax("Laboratorio/LeerArchivoTemperatura", "magnitud=6&fecha=" + FechaActual + "&hora=" + HoraCondicion + "&ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision).split("|");
    if (datos[0] == "0") {
        HoraCondicion = datos[1];
        CalcularTemperatura(tipo);
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function ModalCotizacion() {
    if (Ingreso == 0)
        return false;
    $("#modalcotizacion").modal("show");
}

function ModalActualizarInstr() {
    if (Ingreso == 0 || Certificado != "") {
        return false;
    }
    var datos = LlamarAjax("Laboratorio/BuscarInstrumentoPT", "ingreso=" + Ingreso);
    var data = JSON.parse(datos);
    $("#MResolucion").val(data[0].resolucion);
    $("#MTolerancia").val(data[0].tolerancia);
    $("#MInstrunmento").val(data[0].descripcion);
    $("#MIndicacion").val(data[0].indicacion).trigger("change");
    $("#MClasificacion").val(data[0].clasificacion).trigger("change");
    $("#MClase").val(data[0].clase).trigger("change");
    $("#ingresoinst").html(Ingreso);
    $("#modalInstrumento").modal("show");
}

function BuscarDescripcionEquipo() {
    var clase = $("#MClase").val();
    var tipo = $("#MClasificacion").val();
    $("#MInstrumento").val("");

    if (clase != "" && tipo != "") {
        var datos = LlamarAjax("Laboratorio/BuscarDescripcionEquipo", "tipo=" + tipo + "&clase=" + clase);
        if (datos != "XX")
            $("#MInstrumento").val(datos);
    }
}

function ActualizarInstrumento() {
    var resolucion = NumeroDecimal($("#MResolucion").val());
    var tolerancia = NumeroDecimal($("#MTolerancia").val());
    var instrumento = $.trim($("#MInstrumento").val())
    var indicacion = $("#MIndicacion").val();
    var clasificacion = $("#MClasificacion").val();
    var clase = $("#MClase").val();
    

    var mensaje = "";

    if (tolerancia == 0) {
        $("#MTolerancia").focus();
        mensaje += "Debe de ingresar la tolerancia del instrumento <br>";
    }
    if (resolucion == 0) {
        $("#MResolucion").focus();
        mensaje += "Debe de ingresar la resolucion del instrumento <br>";
    }
    if (clasificacion == "") {
        $("#MClasificacion").focus();
        mensaje += "Debe de seleccionar la clasificacion del instrumento <br>";
    }
    if (clase== "") {
        $("#MTipo").focus();
        mensaje += "Debe de seleccionar el tipo de instrumento <br>";
    }
    if (indicacion == "") {
        $("#MIndicacion").focus();
        mensaje += "Debe de seleccionar la indicacion del instrumento <br>";
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&resolucion=" + resolucion + "&tolerancia=" + tolerancia +
            "&indicacion=" + indicacion + "&clase=" + clase + "&clasificacion=" + clasificacion + "&instrumento=" + instrumento;
        var datos = LlamarAjax("Laboratorio/EditarInstrumentoPT", parametros).split("|");
        if (datos[0] == "0") {
            $("#modalInstrumento").modal("hide");
            $("#Descripcion").html(instrumento);
            $("#Indicacion").html(indicacion);
            $("#Clasificacion").html(clasificacion);
            $("#Clase").html(clase);
            $("#Tolerancia").html(tolerancia);
            Resolucion = resolucion;
            $("#Resolucion").html(Resolucion);
            TipoInst = clasificacion;
            if (TipoInst == "TIPO I") {
                for (var x = 1; x <= CantRegitro; x++) {
                    $("#NmTiempo" + x).addClass("bg-lineas-diagonales");
                    $("#CNmTiempo" + x).addClass("bg-lineas-diagonales");
                    $("#CMeTiempo" + x).addClass("bg-lineas-diagonales");
                    $("#ReTiempo" + x).addClass("bg-lineas-diagonales");
                    $("#CoTiempo" + x).addClass("bg-lineas-diagonales");

                    $("#Resolucion1" + x).removeClass("bg-lineas-diagonales");
                }
                $("#PrSerie4").addClass("bg-lineas-diagonales");
                $("#PrSerie5").addClass("bg-lineas-diagonales");
                $("#PrSerie4").val("0");
                $("#PrSerie5").val("0");
                $("#PrSerie4").prop("readonly", true);
                $("#PrSerie5").prop("readonly", true);

            } else {
                for (var x = 1; x <= CantRegitro; x++) {
                    $("#NmTiempo" + x).removeClass("bg-lineas-diagonales");
                    $("#CNmTiempo" + x).removeClass("bg-lineas-diagonales");
                    $("#CMeTiempo" + x).removeClass("bg-lineas-diagonales");
                    $("#ReTiempo" + x).removeClass("bg-lineas-diagonales");
                    $("#CoTiempo" + x).removeClass("bg-lineas-diagonales");
                    $("#Resolucion1" + x).addClass("bg-lineas-diagonales");
                }
                $("#PrSerie4").removeClass("bg-lineas-diagonales");
                $("#PrSerie5").removeClass("bg-lineas-diagonales");
                $("#PrSerie4").prop("readonly", false);
                $("#PrSerie5").prop("readonly", false);
            }
            CalcularTotal();
            DesactivarLoad();
            $("#FormPrevia").submit();
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
}

$('select').select2();
DesactivarLoad();
