AppAngular.controller('ControlRecibirLaboratorio', function ($scope, $http) {

    $scope.Ingreso = "";

    $scope.InicializarRegistro = function () {
        $scope.Registro = {
            bingreso: "",
        }
        $scope.Botones = {
            btnguardar: false,
            btnregistro: false
        }
        console.log($scope.Registro);
    }

    $scope.BuscarIngreso = function () {
        console.log($scope.Ingreso, $scope.Registro.bingreso);
        if ($scope.Ingreso == $scope.Registro.bingreso)
            return false;

        if ($scope.Ingreso != "") {
            var datos = LlamarAjax("Laboratorio", "opcion=BuscarIngresoRecbLab&ingreso=" + $scope.Ingreso).split("||");
            if (datos[0] != "[]") {
                var data = JSON.parse(datos[0]);
                $scope.Registro = data[0];
                $scope.Registro.bingreso = $scope.Ingreso;
                console.log($scope.Registro);
                $scope.Botones.btnregistro = true;

                if ($scope.Registro.foto == 0 && $scope.Registro.sitio == "NO") {
                    $scope.LimpiarTodo();
                    swal("Acción Cancelada", "No se puede recibir un ingreso sin fotos", "warning");
                    return false;
                }

                if ($scope.Registro.idremision == 0) {
                    $scope.LimpiarTodo();
                    swal("Acción Cancelada", "Este ingreso no posee una remision guardada", "warning");
                    return false;
                }

                localStorage.setItem("Ingreso", $scope.Ingreso);


                if ($scope.Registro.id) {
                    swal("Acción Cancelada", "Este ingreso ya fue recibido", "warning");
                } else {
                    $("#Asesor").focus();
                    $scope.Registro.observaciones = "";

                    if (datos[1] == "[]") {
                        $scope.LimpiarTodo();
                        swal("Acción Cancelada", "Debe de configurar las plantillas del equipo... Consulte con su director técnico", "warning");
                        return false;
                    }

                    $scope.Botones.btnguardar = true;

                }

                //BuscarPlantillas(1, datos[1], IdRecibido);

            } else {
                $("#Ingreso").select();
                $("#Ingreso").focus();
                $scope.Ingreso = "";
                swal("Acción Cancelada", "El Ingreso número " + $scope.Ingreso + " no se encuentra registrado", "warning");
                return false;
            }
        }
    }

    $scope.LimpiarTodo = function () {
        $scope.Ingreso = "";
        $scope.InicializarRegistro();
    }

    $scope.VerFotos = function () {
        if ($scope.Ingreso == "")
            return false;
        CargarModalFoto($scope.Ingreso, $scope.Registro.foto, 1);
    }


    $scope.InicializarRegistro();
    InicializarFormulario("ul_laboratorio", "men_laboratorio");

});
