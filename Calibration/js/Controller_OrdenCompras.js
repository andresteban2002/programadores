AppAngular.controller('ControlOrdenCompras', function ($scope, $http) {
	
	var tabClasses;
    var IvaDefecto = '19';

    d = new Date();
    month = d.getMonth() + 1;
    day = d.getDate();

    var Anio = $.trim(d.getFullYear());
    Anio = $.trim(Anio.substr(2, 2))


    var output = d.getFullYear() + '-' +
            (month < 10 ? '0' : '') + month + '-01';

    var output2 = d.getFullYear() + '-' +
            (month < 10 ? '0' : '') + month + '-' +
            (day < 10 ? '0' : '') + day;
            
	function initTabs() {
        tabClasses = ["", "", ""];
    }
    
    $scope.getTabClass = function (tabNum) {
        return tabClasses[tabNum];
    };

    $scope.getTabPaneClass = function (tabNum) {
        return "tab-pane " + tabClasses[tabNum];
    }
    
    $scope.setActiveTab = function (tabNum) {
        initTabs();
        tabClasses[tabNum] = "active";
    };

    $scope.Documento = "";
    $scope.DocumentoPro = "";
    $scope.Orden = "";
    $scope.decimales = _DE;
    
    $scope.Proveedor = {
        id: 0,
        documento: ""
    }
    
    $scope.InicializarRegistro = function () {
        $scope.Registro = {
			allselect : false,
            id: 0,
            proveedor: 0,
            orden: "",
            disabled : false,
            btnguardar : true,
            btnanular : false,
            btnimprimir : false,
            btnanexo : true,
            estado: "Temporal",
            cliente: "",
            fecharegistro: null,
            usuario: "",
            observacion: "",
            anulaciones: "",
            tabla: {},
            temporal: 0,
            subtotal: 0,
            descuento: 0,
            gravable: 0,
            exento: 0,
            iva: 0,
            retefuente: 0,
            reteiva: 0,
            reteica: 0,
            total: 0,
            tcantidad: 0
        }

        setTimeout(function () {
            $(".ComboRegistro").trigger("change");
        }, 200);
    }
    
    $scope.LimpiarTodo = function () {
        $scope.Documento = "";
        $scope.DocumentoPro = "";
		$scope.Orden = "";
        $scope.LimpiarDatos();
    }

    $scope.LimpiarDatos = function () {
        $scope.Proveedor = {documento: ""};
        $scope.InicializarRegistro();
    }
    
     $("#tablamodalproveedore > tbody").on("dblclick", "tr", function (e) {
        var row = $(this).parents("td").context.cells;
        $("#modalProveedores").modal("hide");
        $scope.DocumentoPro = $.trim(row[0].innerText);
        $scope.BuscarProveedor($scope.DocumentoPro, 1, 1);
    });
    
    $scope.BuscarProveedor = function (documento, busqueda, refrescar) {
        if ($.trim(documento) == $scope.Documento || $.trim(documento) == "") {
            return false;
        }
        var parametros = "documento=" + $.trim(documento);
        var datos = LlamarAjax("Facturacion", "opcion=BuscarProveedor&" + parametros);
        datos = datos.split("|");
        if (busqueda == 1)
			$scope.LimpiarDatos();
		else
			$scope.DocumentoPro = documento;
        if (datos[0] == "0") {
            var data = JSON.parse(datos[1]);
            $scope.Proveedor = data[0];
            console.log($scope.Proveedor);
            if (refrescar == 1) {
                $scope.$apply();
            }
            $scope.Registro.proveedor = $scope.Proveedor.id;
            $scope.Documento = $.trim(documento);
            
            //$scope.TablaCompras(busqueda);
        } else {
            $scope.DocumentoPro = "";
            $("#Documento").focus();
            swal("Acción Cancelada", datos[1], "warning");
        }
    }
    

	$scope.InicializarRegistro();
	InicializarFormulario("ul_tercerizado");
	$scope.setActiveTab(1);
});
