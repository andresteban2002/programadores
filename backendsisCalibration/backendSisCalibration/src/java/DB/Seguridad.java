package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Seguridad"})
public class Seguridad extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String url_archivo = Globales.url_archivo;
    String archivoadjunto = null;

    public String PermisosMenu() {
        sql = "SELECT control, coalesce((select max(idsubmenugrupo) from seguridad.rbac_submenu_grupo gm inner join seguridad.rbac_grupo g using (idgru) inner join seguridad.rbac_usuario_grupo ug using(idgru)  where m.idsubmenu = gm.idsubmenu and idusu = " + idusuario + " and estgru = 'ACTIVO'),0) as permiso "
                + "      FROM seguridad.rbac_submenu m "
                + "  UNION "
                + "  SELECT control,  "
                + "          coalesce((select max(idmenugrupo) from seguridad.rbac_menu_grupo gm inner join seguridad.rbac_grupo g using (idgru) inner join seguridad.rbac_usuario_grupo ug using(idgru) where m.idmenu = gm.idmenu and idusu = " + idusuario + " and estgru = 'ACTIVO'),0) as permiso "
                + "  FROM seguridad.rbac_menu m";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->PermisosMenu");
    }

    public String TablaUsuarios(HttpServletRequest request) {
        String usuarios = request.getParameter("usuario");
        String nombre = request.getParameter("nombre");
        String documento = request.getParameter("documento");
        String estado = request.getParameter("estado");

        String busqueda = "";
        if (!usuarios.equals("")) {
            busqueda += " and nomusu ilike '%" + usuario + "%' ";
        }
        if (!nombre.equals("")) {
            busqueda += " and nombrecompleto ilike '%" + nombre + "%' ";
        }
        if (!documento.equals("")) {
            busqueda += " and documento ilike '%" + documento + "%' ";
        }
        if (!estado.equals("TODOS")) {
            busqueda += " and estusu = '" + estado + "' ";
        }

        sql = "SELECT idusu, nomusu, estusu, nombrecompleto, documento, estusu, "
                + "             telefono || '<br>' || celular as telefono, email, cargo, correoenvio, seguridad.obtener_grupos(idusu) as grupo "
                + "  FROM seguridad.rbac_usuario "
                + "  WHERE idusu > 0 " + busqueda + " ORDER BY 1";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaUsuarios");
    }

    public String TablaGrupos(HttpServletRequest request) {

        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        if (usuarios == 0) {
            sql = "SELECT idgru, codgru, desgru, estgru "
                    + " FROM seguridad.rbac_grupo "
                    + " ORDER BY 3";
        } else {
            sql = "SELECT idgru, codgru, desgru, estgru, "
                    + "      case when coalesce((select idusugru from seguridad.rbac_usuario_grupo gu where gu.idgru = g.idgru and  idusu = " + usuarios + "),0) = 0 then "
                    + "    '<input type=''checkbox'' class=''form-control'' onchange=''ActivarGrupo(' || g.idgru || '," + usuarios + ",this)''>' else  "
                    + "    '<input type=''checkbox'' class=''form-control'' checked onchange=''ActivarGrupo(' || g.idgru || '," + usuarios + ",this)''>' end  as activo "
                    + "  FROM seguridad.rbac_grupo g "
                    + "  ORDER BY 3";
        }

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaGrupos");
    }

    public String TablaModulos(HttpServletRequest request) {

        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));

        if (grupo == 0) {
            sql = "SELECT idmod, codmod, desmod, estmod "
                    + "  FROM seguridad.rbac_modulo "
                    + "  ORDER BY 3";
        } else {
            sql = "select codmod, desmod, estmod, idmod "
                    + "  from seguridad.rbac_modulo m ORDER BY 2";
        }
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaModulos");
    }

    public String BuscarUsuario(HttpServletRequest request) {
        String documento = request.getParameter("documento");
        sql = "SELECT u.*, coalesce(e.foto, '0') as foto "
                + "  FROM seguridad.rbac_usuario u left join rrhh.empleados e on u.idempleado = e.id "
                + "  WHERE u.documento = '" + documento + "'";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarUsuario");
    }

    public String TablaMenu(HttpServletRequest request) {
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        sql = "SELECT idmenu, desmenu, "
                + "  case when coalesce((select idmenugrupo from seguridad.rbac_menu_grupo gm where m.idmenu = gm.idmenu and  idgru = " + grupo + "),0) = 0 then "
                + "        '<input type=''checkbox'' class=''form-control'' onchange=''ActivarMenu(' || m.idmenu || '," + grupo + ",this)''>' else "
                + "        '<input type=''checkbox'' class=''form-control'' checked onchange=''ActivarMenu(' || m.idmenu || '," + grupo + ",this)''>' end  as activo "
                + "  FROM seguridad.rbac_menu m "
                + "  ORDER BY orden";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaMenu");
    }

    public String TablaSubMenu(HttpServletRequest request) {
        int menu = Globales.Validarintnonull(request.getParameter("grupo"));
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));

        sql = "SELECT idsubmenu, dessubmenu, "
                + "  case when coalesce((select idsubmenugrupo from seguridad.rbac_submenu_grupo gm where m.idsubmenu = gm.idsubmenu and  idgru = " + grupo + "),0) = 0 then "
                + "        '<input type=''checkbox'' class=''form-control'' onchange=''ActivarSubMenu(' || m.idsubmenu || '," + grupo + ",this)''>' else  "
                + "        '<input type=''checkbox'' class=''form-control'' checked onchange=''ActivarSubMenu(' || m.idsubmenu || '," + grupo + ",this)''>' end  as activo "
                + "  FROM seguridad.rbac_submenu m "
                + "  WHERE m.idmenu = " + menu
                + "  ORDER BY orden";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaSubMenu");
    }

    public String GuardarUsuarios(HttpServletRequest request) {

        int IdUsuario = Globales.Validarintnonull(request.getParameter("IdUsuario"));
        String Documento = request.getParameter("Documento");
        String Nombres = request.getParameter("Nombres");
        String Cargo = request.getParameter("Cargo");
        String Usuario = request.getParameter("Usuario");
        String Telefonos = request.getParameter("Telefonos");
        String Celular = request.getParameter("Celular");
        String CorreoEnvio = request.getParameter("CorreoEnvio");
        String ClaveEnvio = request.getParameter("ClaveEnvio");
        String CorreoPersonal = request.getParameter("CorreoPersonal");
        String Direccion = request.getParameter("Direccion");
        String[] Magnitudes = request.getParameterValues("Magnitudes");
        String Estado = request.getParameter("Estado");
        int Super = Globales.Validarintnonull(request.getParameter("Super"));
        int Empleado = Globales.Validarintnonull(request.getParameter("Empleado"));

        String resultado = "";
        try {
            String magnitud = "0";
            if (Magnitudes != null) {
                for (int x = 0; x < Magnitudes.length; x++) {
                    if (magnitud.equals("0")) {
                        magnitud = Magnitudes[x];
                    } else {
                        magnitud += "," + Magnitudes[x];
                    }
                }
            }

            if (ClaveEnvio == null || ClaveEnvio.isEmpty()) {
                ClaveEnvio = "";
            }
            if (CorreoEnvio == null || CorreoEnvio.isEmpty()) {
                CorreoEnvio = "";
            }

            if (IdUsuario == 0) {

                if (Globales.PermisosSistemas("SEGURIDAD GUARDAR USUARIO", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                sql = "INSERT INTO seguridad.rbac_usuario(nomusu, clausu, supusu, estusu, nombrecompleto, documento, "
                        + " telefono, email, celular, direccion, cargo, correoenvio, magnitud, clavecorreo, idempleado) "
                        + " VALUES('" + Usuario + "','" + Globales.EncryptKey("123456") + "'," + Super + ",'" + Estado + "','" + Nombres + "','" + Documento + "','"
                        + Telefonos + "','" + CorreoPersonal + "','" + Celular + "','" + Direccion + "','" + Cargo + "','" + CorreoEnvio + "','" + magnitud + "','" + ClaveEnvio + "'," + Empleado + ")";
                
                
                if (Globales.DatosAuditoria(sql, "SEGURIDAD", "GUARDAR USUARIO", idusuario, iplocal, this.getClass() + "-->GuardarUsuarios")) {
                    sql = "SELECT MAX(idusu) FROM seguridad.rbac_usuario;";
                    IdUsuario = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    resultado = "0|Usuario agregado con éxito|" + IdUsuario;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {

                if (Globales.PermisosSistemas("SEGURIDAD EDITAR USUARIO", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                sql = "UPDATE seguridad.rbac_usuario "
                        + " SET nomusu='" + Usuario + "', supusu=" + Super + ", estusu='" + Estado + "', nombrecompleto='" + Nombres + "', "
                        + " documento ='" + Documento + "', telefono='" + Telefonos + "', email='" + CorreoPersonal + "', celular='" + Celular + "', direccion='" + Direccion + "', "
                        + " cargo='" + Cargo + "', correoenvio ='" + CorreoEnvio + "', magnitud='" + magnitud + "', clavecorreo='" + ClaveEnvio + "',idempleado=" + Empleado
                        + " WHERE idusu= " + IdUsuario;
                if (Globales.DatosAuditoria(sql, "SEGURIDAD", "GUARDAR USUARIO", idusuario, iplocal, "SEGURIDAD EDITAR USUARIO")) {
                    resultado = "0|Usuario actualizado con éxito|" + IdUsuario;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }

            if (archivoadjunto != null) {
                File archivo = new File(Globales.ruta_archivo + "Adjunto/imagenes/FirmaUsuario/" + String.valueOf(IdUsuario) + ".png");
                File file_origen = new File(Globales.ruta_archivo + "uploads/" + archivoadjunto);
                Files.copy(file_origen.toPath(), archivo.toPath(), StandardCopyOption.REPLACE_EXISTING);
                archivoadjunto = null;
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

        return resultado;

    }

    public String TablaPermisos(HttpServletRequest request) {
        int modulo = Globales.Validarintnonull(request.getParameter("modulo"));
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        if (grupo == 0) {
            sql = "SELECT idmodacc, idmod, desacc "
                    + "FROM seguridad.rbac_modulo_accion "
                    + "WHERE idmod = " + modulo + " ORDER BY 3";
        } else {
            sql = "select desacc, "
                    + "    case when coalesce((select idgruper from seguridad.rbac_grupo_permiso gp inner join seguridad.rbac_modulo_accion ma using(idmodacc)  where a.idmodacc = ma.idmodacc and  ma.idmod = " + modulo + " and idgru = " + grupo + "),0) = 0 then "
                    + "    '<input type=''checkbox'' class=''form-control'' onchange=''ActivarPermisos(' || idmodacc || '," + grupo + ",this)''>' else  "
                    + "    '<input type=''checkbox'' class=''form-control'' checked onchange=''ActivarPermisos(' || idmodacc || '," + grupo + ",this)''>' end  as activo "
                    + "  from seguridad.rbac_modulo_accion a where idmod = " + modulo + " order by 1, 2";
        }

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaPermisos");
    }

    public String ActivarPermisos(HttpServletRequest request) {
        int permiso = Globales.Validarintnonull(request.getParameter("permiso"));
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int accion = Globales.Validarintnonull(request.getParameter("accion"));

        if (Globales.PermisosSistemas("SEGURIDAD ACTIVAR PERMISOS", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        if (accion == 1) {
            sql = "INSERT INTO seguridad.rbac_grupo_permiso(idgru, idmodacc) VALUES(" + grupo + "," + permiso + ")";
        } else {
            sql = "DELETE FROM seguridad.rbac_grupo_permiso WHERE idgru = " + grupo + " and idmodacc = " + permiso;
        }
        Globales.DatosAuditoria(sql, "SEGURIDAD", "ACTIVAR PERMISOS", idusuario, iplocal, this.getClass() + "-->ActivarPermisos");
        return "1";
    }

    public String ActivarGrupo(HttpServletRequest request) {
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        int accion = Globales.Validarintnonull(request.getParameter("accion"));
        if (Globales.PermisosSistemas("SEGURIDAD ACTIVAR GRUPO", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        if (accion == 1) {
            sql = "INSERT INTO seguridad.rbac_usuario_grupo(idgru, idusu) VALUES(" + grupo + "," + usuarios + ")";
        } else {
            sql = "DELETE FROM seguridad.rbac_usuario_grupo WHERE idgru = " + grupo + " and idusu = " + usuario;
        }
        Globales.DatosAuditoria(sql, "SEGURIDAD", "ACTIVAR PERMISOS", idusuario, iplocal, this.getClass() + "-->ActivarPermisos");
        return "1";
    }

    public String ActivarMenu(HttpServletRequest request) {
        int menu = Globales.Validarintnonull(request.getParameter("menu"));
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int accion = Globales.Validarintnonull(request.getParameter("accion"));
        if (grupo == 0) {
            return "1";
        }
        if (Globales.PermisosSistemas("SEGURIDAD ACTIVAR MENU", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        if (accion == 1) {
            sql = "INSERT INTO seguridad.rbac_menu_grupo(idmenu, idgru) VALUES(" + menu + "," + grupo + ")";
        } else {
            sql = "DELETE FROM seguridad.rbac_menu_grupo WHERE idmenu = " + menu + " and idgru = " + grupo;
        }
        Globales.DatosAuditoria(sql, "SEGURIDAD", "ACTIVAR PERMISOS", idusuario, iplocal, this.getClass() + "-->ActivarPermisos");
        return "1";
    }

    public String ActivarSubMenu(HttpServletRequest request) {
        int submenu = Globales.Validarintnonull(request.getParameter("submenu"));
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int accion = Globales.Validarintnonull(request.getParameter("accion"));
        if (grupo == 0) {
            return "1";
        }
        if (Globales.PermisosSistemas("SEGURIDAD ACTIVAR SUBMENU", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        if (accion == 1) {
            sql = "INSERT INTO seguridad.rbac_submenu_grupo(idsubmenu, idgru) VALUES(" + submenu + "," + grupo + ")";
        } else {
            sql = "DELETE FROM seguridad.rbac_submenu_grupo WHERE idsubmenu = " + submenu + " and idgru = " + grupo;
        }
        Globales.DatosAuditoria(sql, "SEGURIDAD", "ACTIVAR PERMISOS", idusuario, iplocal, this.getClass() + "-->ActivarPermisos");
        return "1";
    }

    public String GuardarModulo(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String codigo = request.getParameter("codigo");
        String modulo = request.getParameter("modulo");
        String estado = request.getParameter("estado");
        String resultado = "";
        try {
            if (id == 0) {
                if (Globales.PermisosSistemas("SEGURIDAD GUARDAR MODULO", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO seguridad.rbac_modulo(codmod, desmod, estmod) "
                        + "VALUES ('" + codigo + "','" + modulo + "','" + estado + "')";
                resultado = "0|Módulo agregado con éxito";
            } else {
                if (Globales.PermisosSistemas("SEGURIDAD EDITAR MODULO", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE seguridad.rbac_modulo SET codmod ='" + codigo + "', desmod='" + modulo + "', estmod='" + estado + "' WHERE idmod = " + id;
                resultado = "0|Módulo actualizado con éxito";
            }
            if (!Globales.DatosAuditoria(sql, "SEGURIDAD", "GUARDAR MODULO", idusuario, iplocal, this.getClass() + "-->GuardarModulo")) {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
        return resultado;
    }

    public String GuardarPermisos(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int modulo = Globales.Validarintnonull(request.getParameter("modulo"));
        String accion = request.getParameter("accion");
        String resultado = "";
        try {
            if (id == 0) {
                if (Globales.PermisosSistemas("SEGURIDAD GUARDAR PERMISOS", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO seguridad.rbac_modulo_accion(idmod, desacc) "
                        + "VALUES (" + modulo + ",'" + accion + "')";
                resultado = "0|Permiso agregado con éxito";
            } else {
                if (Globales.PermisosSistemas("SEGURIDAD EDITAR PERMISOS", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE seguridad.rbac_modulo_accion SET idmod =" + modulo + ", desacc='" + accion + "' WHERE idmodacc = " + id;
                resultado = "0|Permiso actualizado con éxito";
            }
            if (!Globales.DatosAuditoria(sql, "SEGURIDAD", "GUARDAR PERMISOS", idusuario, iplocal, this.getClass() + "-->GuardarModulo")) {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
        return resultado;
    }

    public String GuardarGrupo(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String codigo = request.getParameter("codigo");
        String grupo = request.getParameter("grupo");
        String estado = request.getParameter("estado");
        String resultado = "";

        try {
            if (id == 0) {
                if (Globales.PermisosSistemas("SEGURIDAD GUARDAR GRUPO", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO seguridad.rbac_grupo(codgru, desgru, estgru) "
                        + " VALUES ('" + codigo + "','" + grupo + "','" + estado + "')";
                resultado = "0|Módulo agregado con éxito";
            } else {
                if (Globales.PermisosSistemas("SEGURIDAD EDITAR GRUPO", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE seguridad.rbac_grupo SET codgru ='" + codigo + "', desgru='" + grupo + "', estgru='" + estado + "' WHERE idgru = " + id;
                resultado = "0|Módulo actualizado con éxito";
            }
            if (!Globales.DatosAuditoria(sql, "SEGURIDAD", "GUARDAR GRUPO", idusuario, iplocal, this.getClass() + "-->GuardarGrupo")) {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
        return resultado;
    }

    public String TablaAuditoria(HttpServletRequest request) {

        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String modulo = request.getParameter("modulo");
        String accion = request.getParameter("accion");
        String consulta = request.getParameter("consulta");
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        String busqueda = "WHERE to_char(a.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(a.fecha,'yyyy-MM-dd') <= '" + fechah + "' ";
        if (usuarios != 0) {
            busqueda += " and u.idusu = " + usuarios;
        }
        if (!modulo.equals("")) {
            busqueda += " and modulo ilike '%" + modulo + "%' ";
        }
        if (!accion.equals("")) {
            busqueda += " and accion ilike '%" + accion + "%' ";
        }
        if (!consulta.equals("")) {
            busqueda += " and consulta ilike '%" + consulta + "%' ";
        }

        sql = "SELECT  to_char(fecha,'yyyy/MM/dd HH24:MI') as fecha, modulo, accion, consulta, ip, u.nomusu as usuario "
                + "FROM seguridad.auditoria a INNER JOIN seguridad.rbac_usuario u on a.idususario = u.idusu " + busqueda + " ORDER BY 1";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaAuditoria");
    }

    public String ReiniciarClave(HttpServletRequest request) {
        if (Globales.PermisosSistemas("SEGURIDAD REINICIAR CLAVE", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String clave = Globales.EncryptKey("123456");
        sql = "UPDATE seguridad.rbac_usuario SET clave_java = '" + clave + "' WHERE idusu = " + usuarios;
        if (Globales.DatosAuditoria(sql, "SEGURIDAD", "REINICIAR CLAVE", idusuario, iplocal, this.getClass() + "-->ReiniciarClave")) {
            return "0|";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }
    }

    public int PermisosSistemas(HttpServletRequest request) {
        String Accion = request.getParameter("Accion");
        return Globales.PermisosSistemas(Accion, idusuario);
    }

    public String UsuariosChat(HttpServletRequest request) {
        String Usuario = request.getParameter("Usuario");
        int json = Globales.Validarintnonull(request.getParameter("json"));
        if (usuario == null) {
            return "<option value=''></option>";
        }
        try {
            String resultado = "<optionvalue=''></option>";
            sql = "SELECT idusu, nomusu, nombrecompleto as usuario, chat "
                    + " FROM seguridad.rbac_usuario "
                    + " where estusu = 'ACTIVO' AND idusu <> " + Usuario
                    + " ORDER BY 2";
            if (json == 0){
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->UsuariosChat", 1);
                if (datos != null) {
                    while (datos.next()) {
                        resultado += "<option " + (datos.getString("chat").equals("0") ? "style='color:red'" : "style='color:green'") + " value='" + datos.getString("idusu") + "' title='" + datos.getString("nomusu") + "'>" + datos.getString("usuario") + "</option>";
                    }
                }
            }else{
                return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->UsuariosChat");
            }

            return resultado;
        } catch (SQLException ex) {
            Globales.GuardarLogger(ex.getMessage(), this.getClass() + "-->UsuariosChat ");
            return "<option value=''></option>";
        }
    }

    public String UsuarioEmpleado(HttpServletRequest request) {

        int empleado = Globales.Validarintnonull(request.getParameter("empleado"));

        sql = "SELECT  idusu "
                + "  FROM seguridad.rbac_usuario where idempleado = " + empleado;
        String id = Globales.ObtenerUnValor(sql);
        if (id.equals("")) {
            id = "0";
        }
        return id;
    }

    public String ActivarUsuariosChat(HttpServletRequest request) {
        String Usuario = request.getParameter("Usuario");

        sql = "update seguridad.rbac_usuario set chat = 1 "
                + " where idusu = " + Usuario;
        datos = Globales.Obtenerdatos(sql, this.getClass() + "-->UsuariosChat", 2);
        return "Usuario Activo";
    }

    public String ControlAcceso(HttpServletRequest request) {

        try {

            String usuarios = request.getParameter("usuario");
            String clave = request.getParameter("clave");
            String tipo = request.getParameter("tipo");

            usuario = Globales.AntiJaquer(usuario);
            clave = Globales.AntiJaquer(clave);
            String claveini = clave;
            clave = Globales.EncryptKey(clave);

            sql = "SELECT u.*, g.desgru, g.idgru "
                    + "  FROM seguridad.rbac_usuario u LEFT JOIN seguridad.rbac_usuario_grupo ug using (idusu) "
                    + "                                LEFT JOIN seguridad.rbac_grupo g USING (idgru) "
                    + " WHERE nomusu = '" + usuarios + "'  and clave_java = '" + clave + "'";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "->ControlAcceso", 1);
            if (!datos.next()) {
                return "1|Combinacion de usuario y/o clave incorrecto";
            }

            String idusuarios = datos.getString("idusu");

            if (Globales.PermisosSistemas(tipo, idusuarios) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            return "0|Acceso concedido|" + idusuarios;
        } catch (SQLException ex) {
            return "1|" + ex.getMessage();
        }
    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response) {
        try {

            response.setContentType("text/html;charset=UTF-8");
            String ruta_origen = request.getHeader("Origin");
            if (ruta_origen == null) {
                ruta_origen = "http://localhost:8090";
            }

            HttpSession misession = request.getSession();
            idusuario = misession.getAttribute("idusuario").toString();
            iplocal = misession.getAttribute("IPLocal").toString();
            usuario = misession.getAttribute("codusu").toString();
            if (misession.getAttribute("ArchivosAdjuntos") != null) {
                archivoadjunto = misession.getAttribute("ArchivosAdjuntos").toString();
            }

            response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
            response.setHeader("Access-Control-Allow-Credentials", "true");

            PrintWriter out = response.getWriter();
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else {
                if (misession.getAttribute("codusu") == null) {
                    out.print("cerrada");
                } else {
                    if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                        out.print("cambio-ip");
                    } else {
                        String opcion = request.getParameter("opcion");
                        switch (opcion) {
                            case "PermisosMenu":
                                out.print(PermisosMenu());
                                break;
                            case "TablaUsuarios":
                                out.print(TablaUsuarios(request));
                                break;
                            case "TablaGrupos":
                                out.print(TablaGrupos(request));
                                break;
                            case "TablaModulos":
                                out.print(TablaModulos(request));
                                break;
                            case "BuscarUsuario":
                                out.print(BuscarUsuario(request));
                                break;
                            case "TablaMenu":
                                out.print(TablaMenu(request));
                                break;
                            case "TablaSubMenu":
                                out.print(TablaSubMenu(request));
                                break;
                            case "GuardarUsuarios":
                                out.print(GuardarUsuarios(request));
                                break;
                            case "TablaPermisos":
                                out.print(TablaPermisos(request));
                                break;
                            case "ActivarPermisos":
                                out.print(ActivarPermisos(request));
                                break;
                            case "ActivarGrupo":
                                out.print(ActivarGrupo(request));
                                break;
                            case "ActivarMenu":
                                out.print(ActivarMenu(request));
                                break;
                            case "ActivarSubMenu":
                                out.print(ActivarSubMenu(request));
                                break;
                            case "GuardarModulo":
                                out.print(GuardarModulo(request));
                                break;
                            case "GuardarPermisos":
                                out.print(GuardarPermisos(request));
                                break;
                            case "GuardarGrupo":
                                out.print(GuardarGrupo(request));
                                break;
                            case "TablaAuditoria":
                                out.print(TablaAuditoria(request));
                                break;
                            case "ReiniciarClave":
                                out.print(ReiniciarClave(request));
                                break;
                            case "PermisosSistemas":
                                out.print(PermisosSistemas(request));
                                break;
                            case "UsuariosChat":
                                out.print(UsuariosChat(request));
                                break;
                            case "ActivarUsuariosChat":
                                out.print(ActivarUsuariosChat(request));
                                break;
                            case "UsuarioEmpleado":
                                out.print(UsuarioEmpleado(request));
                                break;
                            case "ControlAcceso":
                                out.print(ControlAcceso(request));
                                break;
                            default:
                                throw new AssertionError();

                        }
                    }
                }
            }
        } catch (Exception ex) {
            try {
                PrintWriter out = response.getWriter();
                out.print(ex.getMessage());
            } catch (IOException exl) {
                Globales.GuardarLogger(exl.getMessage(), Laboratorio.class.getName());
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

}
