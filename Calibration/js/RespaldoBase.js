﻿var url = localStorage.getItem("urlprincipal");
CargarTabla();

function Seleccionar(id) {
    var selec = document.getElementsByName("seleccionar[]");
    for (x = 0; x < selec.length; x++) {

        if (id.checked)
            selec[x].checked = true;
        else
            selec[x].checked = false;
    }
}

function Respaldar() {
    var selec = document.getElementsByName("seleccionar[]");
    var nfilas = 0;
    ActivarLoad();
    setTimeout(function () {
        for (x = 0; x < selec.length; x++) {
            nfilas++;
            $("#resultado" + nfilas).html("");
            $("#archivo" + nfilas).html("");
            if (selec[x].checked) {
                $("#resultado" + nfilas).html("Cargando");
                var datos = LlamarAjax(url + "Base/RespaldarBaseDatos", "basedatos=" + selec[x].value);
                datos = datos.split("|");
                if (datos[0] == "9") {
                    swal("Alerta", datos[1], "warning");
                    return false;
                }
                $("#resultado" + nfilas).html(datos[1]);
                $("#archivo" + nfilas).html("<a  href=\"javascript:DescargarArchivo('" + url + datos[2] + "')\">" + datos[2] + "</a>");
            }
        }
        DesactivarLoad();
    }, 15);
           

}

function DescargarArchivo(archivo) {
    window.open(archivo);
}

function CargarTabla() {
    var nfilas = 0;
    var resultado = "";
    var datos = LlamarAjax(url + "Base/CargarBaseDatos", "");
    eval("data=" + datos);
    for (var x = 0; x < data.length; x++) {
        nfilas++;
        resultado += "<tr><td>" + (x + 1) + "</td>" +
            "<td><input type='checkbox' name='seleccionar[]' value='" + data[x].Name + "' class='form-control' checked /></td>" +
            "<td>" + data[x].Name + "</td>" +
            "<td><div id='resultado" + nfilas + "'</td>" +
            "<td><div id='archivo" + nfilas + "'</td></tr>";
    }
    $("#detalle_tabla").html(resultado);
}