package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.BasicStroke;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import net.sf.jasperreports.engine.JasperRunManager;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.StatisticalLineAndShapeRenderer;
import org.jfree.data.statistics.DefaultStatisticalCategoryDataset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.filechooser.FileSystemView;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Configuracion"})
public class Configuracion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String url_archivo = Globales.url_archivo;
    String archivoadjunto = null;
    String correoenvio;
    String clavecorreo;

    public String DatosEmpresa() {

        sql = "select * from empresa";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DatosEmpresa");
    }

    public String EventosCalendarios(HttpServletRequest request) {

        //VARIABLES RECIBIDA DESDE AJAX
        Date fechad = Globales.ValidarFecha(request.getParameter("fechad"));
        Date fechah = Globales.ValidarFecha(request.getParameter("fechad"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int recordatoria = Globales.Validarintnonull(request.getParameter("recordatoria"));
        //

        String[] sql2 = new String[4];
        String resultado = "";

        SimpleDateFormat formateador = new SimpleDateFormat("MM-dd");
        SimpleDateFormat formateador2 = new SimpleDateFormat("yyyy-MM-dd");

        /*String sql2 = "select  case when proximo = 0 then  (now() - interval '1 hour') else (now() + interval '4 hour') end as fecha " +
         " from eventos_recordatorios " +
         " where idusuario = 3 and to_char(fecha,'yyyy-MM-dd') = '" + DateTime.Now.ToString("yyyy-MM-dd") + "'";

         //String fecha_recordatorio = DateTime.Parse(Globales.ObtenerUnValor(sql2, conexion, 0)).ToString("yyyy-MM-dd hh:mm"); 
         //String fechaactual = DateTime.Now.ToString("yyyy-MM-dd hh:mm");*/
        sql2[0] = "SELECT nombrecompleto as evento, to_char(fechanacimiento,'MM-dd') as fecha, id, to_char(fechanacimiento,'MM/dd') as fechacumple "
                + "  from rrhh.empleados "
                + "  where estado = 'ACTIVO' and to_char(fechanacimiento,'MM-dd') >= '" + formateador.format(fechad) + "' "
                + "   and to_char(fechanacimiento,'MM-dd') <= '" + formateador.format(fechah) + "'";

        sql2[1] = "SELECT nombrecompleto as evento, to_char(fechafin,'yyyy-MM-dd') as fecha "
                + "  from rrhh.empleados "
                + "  where estado = 'ACTIVO' and to_char(fechafin,'yyyy-MM-dd') >= '" + formateador2.format(fechad) + "'"
                + "   and to_char(fechafin,'yyyy-MM-dd') <= '" + formateador2.format(fechah) + "'";

        sql2[2] = "select descripcion as evento, to_char(dia,'yyyy-MM-dd') as fecha from festivos "
                + "where to_char(dia,'yyyy-MM-dd') >= '" + formateador2.format(fechad) + "' "
                + "   and to_char(dia,'yyyy-MM-dd') <= '" + formateador2.format(fechah) + "'";

        if (recordatoria == 0) {
            sql2[3] = "select '[' || e.id || ']' || titulo as evento, e.tipo, e.descripcion, e.id, idcliente, c.nombrecompleto as cliente, idusuario, " + idusuario + " as usuario, to_char(fechainicio,'yyyy-MM-dd') as fechainicio, to_char(fechainicio,'HH24:MI') as horainicio, "
                    + "   to_char(fechafinal,'yyyy-MM-dd') as fechafinal, to_char(fechafinal,'HH24:MI') as horafinal, to_char(fecharecordatorio,'dd/MM/yyyy') as fecharecordatorio "
                    + "   from eventos e inner join clientes c on c.id = e.idcliente "
                    + "  where e.estado = 'Activo' and  (to_char(fechainicio,'yyyy-MM-dd') >= '" + formateador2.format(fechad)
                    + "' or to_char(fechafinal,'yyyy-MM-dd') <= '" + formateador2.format(fechah) + "') and (visitas = '/0/' OR visitas like '%/" + idusuario + "/%') " + (cliente > 0 ? " and idcliente=" + cliente : "");
        } else {
            sql2[3] = "select '[' || e.id || ']' || titulo as evento, e.tipo, e.descripcion, e.id, idcliente, c.nombrecompleto as cliente, idusuario, " + idusuario + " as usuario, to_char(fechainicio,'yyyy-MM-dd') as fechainicio, to_char(fechainicio,'HH24:MI') as horainicio, "
                    + "   to_char(fechafinal,'yyyy-MM-dd') as fechafinal, to_char(fechafinal,'HH24:MI') as horafinal, to_char(fecharecordatorio,'dd/MM/yyyy') as fecharecordatorio, "
                    + "   to_char(fecha,'yyyy/MM/dd HH24:MI') || ' ' || u.nomusu as registro "
                    + "   from eventos e inner join clientes c on c.id = e.idcliente "
                    + "                  inner join seguridad.rbac_usuario u on u.idusu = e.idusuario "
                    + "  where e.estado = 'Activo' and  '" + formateador2.format(fechad) + "' >= to_char(fecharecordatorio,'yyyy-MM-dd') "
                    + "    and '" + formateador2.format(fechad) + "' <=  to_char(fechafinal,'yyyy-MM-dd') and (visitas = '/0/' OR visitas like '%/" + idusuario + "/%') " + (cliente > 0 ? " and idcliente=" + cliente : "");
        }

        for (int x = 0; x < 4; x++) {
            if (!resultado.equals("")) {
                resultado += "||";
            }
            resultado += Globales.ObtenerDatosJSon(sql2[x], this.getClass() + "-->EventosCalendarios");
        }

        return resultado;
    }

    public String CargarCombos(HttpServletRequest request, Integer tipo_i, Integer inicial_i, String opcion_i, Integer retorsql_i, Integer _json) {

        //VARIABLES RECIBIDA DESDE AJAX
        int tipo = 0;
        int inicial = 0;
        String opciones = null;
        int json = 0;
        int retorsql = 0;
        if (request != null) {
            tipo = Globales.Validarintnonull(request.getParameter("tipo"));
            inicial = Globales.Validarintnonull(request.getParameter("inicial"));
            opciones = request.getParameter("opciones");
            retorsql = Globales.Validarintnonull(request.getParameter("retorsql"));
            json = Globales.Validarintnonull(request.getParameter("vjson"));
        } else {
            tipo = tipo_i;
            inicial = inicial_i;
            if (opcion_i == null) {
                opcion_i = "0";
            }
            opciones = opcion_i;
            retorsql = retorsql_i;
            json = _json;
        }

        String combos = "";
        int titulo = 0;
        int inactivo = 0;
        switch (tipo) {
            case 1:
                sql = "SELECT id, descripcion, '', case when estado = 1  then 0 else 1 end FROM magnitudes where id > 0 ORDER BY 2 ";
                inactivo = 1;
                break;
            case 2:
                if (opciones.equals("0")) {
                    sql = "SELECT DISTINCT e.descripcion, e.descripcion, '', case when estado = 1 then 0 else 1 end  FROM equipo e WHERE id > 0  ORDER BY 2 ";
                } else {
                    sql = "SELECT e.id, e.descripcion || ' (' || m.descripcion || ')', '', case when e.estado = 1 and m.estado = 1 then 0 else 1 end FROM equipo e inner join magnitudes m on m.id = e.idmagnitud  where e.id > 0 " + (opciones.equals("0") ? "" : " and idmagnitud = " + opciones) + "  ORDER BY 2 ";
                }
                inactivo = 1;
                break;
            case 3:
                sql = "SELECT id, descripcion, '', case when estado = 1  then 0 else 1 end from marcas where id > 0  ORDER BY 2";
                inactivo = 1;
                break;
            case 4:
                sql = "SELECT id, nombre, '',  case when estado = 1  then 0 else 1 end from servicio where id > 0 ORDER BY id";
                inactivo = 1;
                break;

            case 5:
                if (opciones.equals("-1")) {
                    sql = "SELECT DISTINCT descripcion, descripcion, '', CASE WHEN estado = 1 then 0 else 1 end from modelos where id > 0 ORDER BY 2";
                } else {
                    sql = "SELECT id, descripcion,  '', CASE WHEN estado = 1 then 0 else 1 end from modelos where id > 0  and idmarca = " + opciones + " ORDER BY 2";
                }
                inactivo = 1;
                break;
            case 6:
                if (opciones.equals("-1")) {
                    sql = "sELECT DISTINCT '(' || desde || ' a ' || hasta || ') ' || medida,  '(' || desde || ' a ' || hasta || ') ' || medida FROM magnitud_intervalos ORDER BY 2";
                } else {
                    sql = "sELECT DISTINCT id, '(' || desde || ' a ' || hasta || ') ' || medida FROM magnitud_intervalos WHERE idmagnitud = " + opciones + " ORDER BY 2";
                }
                break;
            case 7:
                sql = "select po.id, nombrecompleto || ' (' || p.descripcion || ')' "
                        + " from proveedores po inner join ciudad c on c.id = po.idciudad "
                        + "                     inner join departamento d on d.id = c.iddepartamento "
                        + "                     inner join pais p on p.id = d.idpais where po.id <> 0 ORDER BY 2";
                break;
            case 8:
                sql = "SELECT id, descripcion from medidas order by 2";
                break;
            case 9:
                sql = "SELECT id, nombrecompleto || ' (' || documento || ')' from clientes where id <> 0 order by 2";
                break;
            case 10:
                sql = "SELECT id, descripcion from pais where id <> 0 order by 2";
                break;
            case 11:
                sql = "SELECT id, descripcion from departamento WHERE id <> 0 " + (!opciones.equals("0") ? " AND idpais = " + opciones : "") + " order by 2";
                break;
            case 12:
                sql = "SELECT id, descripcion from ciudad where id <> 0 " + (!opciones.equals("0") ? " AND iddepartamento = " + opciones : "") + " ORDER BY 2";
                break;
            case 13:
                sql = "SELECT idusu, nombrecompleto,nomusu, CASE WHEN estusu = 'ACTIVO' THEN 0 ELSE 1 END  from seguridad.rbac_usuario WHERE idusu > 0 " + (opciones.equals("1") ? " and idusu <> " + idusuario : "") + " order by 2";
                inactivo = 1;
                break;
            case 14:
                sql = "select id, descripcion from tipocliente ORDER BY 2";
                break;
            case 15:
                sql = "select id, descripcion from tablaprecios WHERE estado = 1 ORDER BY 2";
                break;
            case 16:
                sql = "select id, descripcion, descripcion, case when estado = 1 then '' else 'false' end from otros_servicios where id > 0 ORDER BY 2";
                break;
            case 17:
                sql = "SELECT min(m.id), m.descripcion "
                        + " from metodo m inner join metodo_equipo me on me.idmetodo = m.id "
                        + " where m.estado = 1 " + (!opciones.equals("-1") ? " and idequipo = " + opciones : "")
                        + " group by m.descripcion "
                        + " ORDER BY 2";
                break;
            case 18:
                sql = "select id, descripcion from pais where id <> 0 ORDER BY 2";
                break;
            case 19:
                sql = "SELECT idusu, nomusu from seguridad.rbac_usuario WHERE estusu = 'ACTIVO' order by 2";
                break;
            case 20:
                sql = "SELECT id, alcance || ' ' || medida from patron WHERE idmagnitud = " + opciones + " ORDER BY alcance ";
                break;
            case 21:
                sql = "SELECT id, empresa, '', case when estado = 1 then '' else 'false' end from empresa_envio WHERE estado = 1  ORDER BY 2 ";
                break;
            case 22:
                sql = "SELECT idgru, desgru FROM seguridad.rbac_grupo where estgru = 'ACTIVO' ORDER BY 2";
                break;
            case 23:
                sql = "SELECT t.id, b.descripcion || ' - ' || t.descripcion "
                        + "  FROM bancos_tarjetas t inner join bancos b on t.idbanco = b.id "
                        + "  WHERE t.estado = 1 and b.estado = 1 ORDER BY 2";
                break;
            case 24:
                sql = "SELECT id, descripcion FROM bancos WHERE estado = 1 ORDER BY 2";
                break;
            case 25:
                sql = "SELECT c.id, b.descripcion || ' - ' || tc.descripcion || ' ' || subString(c.cuenta,length(cuenta)-4,6) "
                        + "  FROM bancos_cuenta c inner join bancos b on c.idbanco = b.id "
                        + "	                  inner join tipo_cuenta tc on tc.id = c.idtipocuenta "
                        + "  WHERE b.estado = 1 and c.estado = 1 ORDER BY 2";
                break;
            case 26:
                sql = "SELECT id, descripcion FROM rrhh.cargo WHERE id <> 0  ORDER BY 2";
                break;
            case 27:
                sql = "SELECT p.id, codigo || ' ' || titulo || ' (' || n.tipo || ') ' || p.numero || ' - Inicio: ' || to_char(p.fechainicio,'dd/MM/yyyy') || ' Fin: ' || to_char(p.fechafinal,'dd/MM/yyyy') || "
                        + "      case when calculado = 'SI' THEN ' Calculada' else '' end || case when cerrado = 'SI' THEN ' Cerrada' else '' end as descripcion "
                        + "  FROM rrhh.nominas n inner join  rrhh.nominas_periodos p on p.numero = n.periodo and n.id = p.idnomina "
                        + "  WHERE n.estado = 'Activo' "
                        + "  ORDER BY 2";
                break;
            case 28:
                sql = "SELECT id, numero || ' - Inicio: ' || to_char(fechainicio,'dd/MM/yyyy') || ' Fin: ' || to_char(fechafinal,'dd/MM/yyyy') || "
                        + "      case when calculado = 'SI' THEN ' Calculada' else '' end || case when cerrado = 'SI' THEN ' Cerrada' else '' end as descripcion "
                        + "  FROM rrhh.nominas_periodos "
                        + "  WHERE idnomina = " + opciones + " ORDER BY 1";
                break;
            case 29:
                sql = "select e.id, nombrecompleto || ' | ' || tipodocumento || ' | ' || documento || ' | ' || c.descripcion || ' | ' || sueldo as descripcion "
                        + "  from rrhh.empleados e inner join rrhh.cargo c on c.id = e.idcargo "
                        + "  order by nombrecompleto";
                break;
            case 30:
                sql = "SELECT id, descripcion  FROM rrhh.horario where id <> 0 order by id;";
                break;
            case 31:
                sql = "SELECT descripcion, descripcion  FROM tipomovimiento ORDER BY 1";
                break;
            case 32:
                sql = "SELECT id, descripcion  FROM motivo_salida ORDER BY 2";
                break;
            case 33:
                sql = "SELECT descripcion, descripcion  FROM accesorios ORDER BY 2";
                break;
            case 34:
                sql = "SELECT descripcion, descripcion  FROM obser_ingreso ORDER BY 2";
                break;
            case 35:
                sql = "SELECT id, descripcion  FROM iva ORDER BY 1 desc";
                break;
            case 36:
                sql = "SELECT id, descripcion  FROM tipo_precio ORDER BY 1";
                break;
            case 37:
                sql = "SELECT id, descripcion  FROM tipo_estado ORDER BY 1";
                break;
            case 38:
                sql = "SELECT tabla, descripcion FROM tablas order by 1";
                break;
            case 40:
                sql = "SELECT id, titulo || ' (' || serie || ')' "
                        + "FROM patron where idmagnitud = " + opciones + " and estado = 1 order by 2";
                break;
            case 41:
                sql = "SELECT descripcion, descripcion FROM medidas order by 1";
                break;
            case 42:
                sql = "SELECT id, nombre || ' (' || nit ||  case when documento = 'NIT' THEN '-' || verificador when documento = 'RUT' THEN '-'  || verificador else '' end || ')' "
                        + "  FROM terceros "
                        + "  WHERE estado = 1 ORDER BY 2";
                break;
            case 43:
                sql = "SELECT id, descripcion FROM eps order by 2";
                break;
            case 44:
                sql = "SELECT id, descripcion FROM afp order by 2";
                break;
            case 45:
                sql = "SELECT descripcion, descripcion FROM contabilidad.tipocomprobante order by id";
                break;
            case 46:
                sql = "SELECT descripcion, descripcion, FROM concepto_cm where tipo = " + opciones + " order by id";
                break;
            case 47:
                sql = "SELECT id, descripcion, '', case when estado = 1  then '' else 'false' end from inventarios.grupos where id > 0 " + (opciones != null ? " and id <> " + opciones : "") + " ORDER BY 2";
                inactivo = 1;
                break;
            case 48:
                sql = "SELECT e.id, e.descripcion || ' (' || g.descripcion || ')', '', case when e.estado = 1  then 0 else 1 end  FROM inventarios.equipos e inner join inventarios.grupos g on g.id = e.idgrupo where e.id > 0 " + (opciones.equals("-1") ? "" : " and idgrupo = " + opciones) + "  ORDER BY 2 ";
                break;
            case 49:
                sql = "SELECT id, descripcion from inventarios.marcas where estado = 1 ORDER BY 2";
                break;
            case 50:
                sql = "SELECT id, descripcion from inventarios.modelos where estado = 1 " + (!opciones.equals("-1") ? " and idmarca = " + opciones : "") + " ORDER BY 2";
                break;
            case 51:
                sql = "SELECT p.id, p.descripcion "
                        + "  FROM procedimiento p INNER JOIN procedimiento_magnitud pm on p.id = pm.idprocedimiento "
                        + "  WHERE pm.idmagnitud = " + opciones + " and p.estado = 1 "
                        + "  ORDER BY 2";
                break;
            case 52:
                sql = "SELECT id, descripcion FROM procedimiento ORDER BY 2";
                break;
            case 53:
                sql = "SELECT id, descripcion FROM tipo_documentos_cliente  ORDER BY 1";
                break;
            case 54:
                sql = "SELECT id, descripcion FROM rrhh.area WHERE estado = 1  ORDER BY 2";
                break;
            case 55:
                sql = "SELECT valor, descripcion  FROM iva ORDER BY 1 desc";
                break;
            case 56:
                sql = "SELECT id, descripcion FROM afp order by 2";
                break;
            case 57:
                sql = "sELECT mi.id,  '(' || desde || ' a ' || hasta || ') ' || medida || ' [' || m.abreviatura || ']' FROM magnitud_intervalos mi inner join magnitudes m on m.id = mi.idmagnitud ORDER BY 2";
                break;
            case 58:
                sql = "SELECT e.id, e.descripcion || ' (' || m.descripcion || ')'  FROM equipo e inner join magnitudes m on m.id = e.idmagnitud  where e.estado = 1 ORDER BY 2 ";
                break;
            case 59:
                sql = "SELECT id, descripcion from metodo where estado = 1 ORDER BY 2";
                break;
            case 60:
                sql = "SELECT id, descripcion from curso where estado = 1 ORDER BY 2";
                break;
            case 61:
                sql = "SELECT id, codigo || ' ' ||  descripcion FROM contabilidad.centro_costo order by 1;";
                break;
            case 62:
                sql = "SELECT id, descripcion FROM contabilidad.cuenta_tipo order by 2;";
                break;
            case 63:
                sql = "SELECT id, descripcion FROM contabilidad.cuenta_grupo order by 2;";
                break;
            case 64:
                sql = "SELECT id, descripcion FROM contabilidad.cuenta_clasificacion order by 2;";
                break;
            case 65:
                sql = "SELECT codigo, codigo || ' ' || descripcion  FROM contabilidad.cuenta_tipo order by 2;";
                break;
            case 67:
                sql = "SELECT distinct e.id, e.descripcion FROM inventarios.articulos a INNER JOIN inventarios.equipos e on a.idequipo = e.id "
                        + (opciones != null ? " WHERE id <> " + opciones : "") + " ORDER BY 2";
                break;
            case 68:
                sql = " SELECT id, descripcion, '', case when estado = 1 then 0 else 1 end, responsables \n"
                        + " FROM inventarios.bodega b\n"
                        + " WHERE id > 0  " + (opciones != null ? " and in_contenido(responsables,'" + opciones + "') = true " : "")
                        + "ORDER BY 2";
                break;
            case 69:
                sql = "SELECT id, descripcion FROM cesantia order by 2";
                break;
            case 70:
                sql = "SELECT id, descripcion FROM rrhh.tipo_concepto order by 1";
                break;
            case 71:
                sql = "select id, descripcion from plantillas where id <> 0 and estado = 1 ORDER BY 2";
                break;
            case 72:
                sql = "select id, descripcion from plantillas where codigo in (" + opciones + ") and estado = 1 ORDER BY 2";
                break;
            case 73:
                sql = "select id, descripcion from rrhh.documentos ORDER BY orden";
                break;
            case 74:
                sql = "SELECT i.id,  i.serie || ' (' || m.descripcion || ')', url "
                        + "  FROM public.intrumento_condamb_sensores i inner join magnitudes m on i.idmagnitud = m.id "
                        + (!opciones.equals("0") ? " where i.idmagnitud = " + opciones : "") + " order by 2";
                titulo = 1;
                break;
            case 75:
                sql = "SELECT id, 'Versión ' || version || ', Fecha ' || to_char(revision,'yyyy-MM-dd') || ' Documento ' || codigo  "
                        + "  FROM version_documento "
                        + "  WHERE documento = 'CERTIFICADO' "
                        + "  order by 2";
                break;
            case 76:
                sql = "SELECT id, descripcion FROM public.contadores_ot ORDER BY 2";
                break;
            case 77:
                sql = "SELECT id, descripcion FROM public.contadores_ot ORDER BY 2";
                break;
            case 78:
                sql = "select e.id, nombrecompleto || ' | ' || tipodocumento || ' | ' || documento || ' | ' || c.descripcion || ' | ' || sueldo as descripcion "
                        + "  from rrhh.empleados e inner join rrhh.cargo c on c.id = e.idcargo "
                        + "                                INNER JOIN rrhh.nominas_empleados ne on ne.idempleado = e.id "
                        + "  where idperiodo = " + opciones
                        + "  order by nombrecompleto";
                break;
            case 79:
                sql = "SELECT n.id, codigo || ' ' || titulo || ' (' || n.tipo || ') ' || p.numero || ' - Inicio: ' || to_char(p.fechainicio,'dd/MM/yyyy') || ' Fin: ' || to_char(p.fechafinal,'dd/MM/yyyy') || "
                        + "      case when calculado = 'SI' THEN ' Calculada' else '' end || case when cerrado = 'SI' THEN ' Cerrada' else '' end as descripcion "
                        + "  FROM rrhh.nominas n inner join  rrhh.nominas_periodos p on p.numero = n.periodo and n.id = p.idnomina "
                        + "  WHERE n.estado = 'Activo'  "
                        + "  ORDER BY 2";
                break;

            case 80:
                sql = "SELECT id, nivel || ' (' || porcentaje || ')' FROM rrhh.clase_riesgo order by 1";
                break;

            case 81:
                sql = "SELECT descripcion, descripcion, descripcion, case when estado = 1 then '' else 'false' end FROM public.validez_oferta order by 1";
                break;
            case 82:
                sql = "SELECT id_mesa_trabajo as identificacion, fecha_registro::date || ' - ' || descripcion FROM public.mesa_trabajo WHERE est_incidencia in (5,10) AND (tipo_especialista = 'Capacitacion' OR tipo_especialista = 'Error de sistema') AND id_padre = 0 AND id_usuario_registro =  " + idusuario
                        + " UNION  "
                        + "      SELECT id_mesa_trabajo as identificacion, fecha_registro::date || ' - ' || descripcion FROM public.mesa_trabajo WHERE est_incidencia in (0, 5,10) AND id_padre in (SELECT id_mesa_trabajo FROM public.mesa_trabajo WHERE id_padre = 0 AND id_usuario_registro = " + idusuario + ")";
                break;
            case 83:
                sql = "SELECT id, '<b>' || cuenta || '</b> ' || nombre as cuenta, 0, inactiva "
                        + " FROM contabilidad.cuenta "
                        + " where visible = 1 "
                        + " order by cuenta ";
                inactivo = 1;
                break;
            case 84:
                sql = "select 1 || '-' || id as id, nombrecompleto || ' (' || documento || ') [C]' as nombre "
                        + " from clientes "
                        + "  where estado = 1 "
                        + "  union "
                        + "  select 2 || '-' || id as id, nombrecompleto || ' (' || documento || ') [P]' as nombre "
                        + "  from proveedores "
                        + "  where estado = 1 "
                        + "  union "
                        + "  select 3 || '-' || id as id,  nombre || ' (' || nit ||  case when documento = 'NIT' THEN '-' || verificador when documento = 'RUT' THEN '-'  || verificador else '' end || ') [T]' as nombre "
                        + "  from terceros where id > 0 "
                        + "  order by 2";
                break;
            case 85:
                sql = "SELECT id, descripcion FROM contabilidad.cuenta_tipo where id > 0 order by id";
                break;
            case 86:
                sql = "SELECT id, descripcion FROM contabilidad.cuenta_grupo where id > 0 order by id";
                break;
            case 87:
                sql = "SELECT id, descripcion FROM contabilidad.cuenta_clasificacion where id > 0 order by id";
                break;
            case 88:
                sql = "SELECT id, descripcion FROM tipo_cuenta where id > 0 order by 2";
                break;
            case 89:
                sql = "SELECT id, descripcion "
                        + "FROM contabilidad.retenciones "
                        + " WHERE operacion = 'Compras' and tipo = '" + opciones + "' and estado = 1 "
                        + "  ORDER BY 2";
                break;
            case 90:
                sql = "SELECT id, descripcion "
                        + "  FROM contabilidad.retenciones "
                        + "  WHERE operacion = 'Ventas' and tipo = '" + opciones + "' and estado = 1 "
                        + "  ORDER BY 2";
                break;
            case 91:
                sql = "SELECT DISTINCT id_actividad_tipos, tx_actividad_tipo FROM public.actividad_tipos \n"
                        + "WHERE \n"
                        + "ind_area = 1 \n"
                        + "and \n"
                        + "(actividad_tipos.id_cargo LIKE \n"
                        + "CASE WHEN (SELECT COUNT(*) FROM seguridad.rbac_usuario WHERE rbac_usuario.idusu = " + idusuario + " AND lower(cargo) like '%calidad%') < 1  THEN\n"
                        + "	'%|' || (SELECT rrhh.empleados.idarea\n"
                        + "	FROM\n"
                        + "	seguridad.rbac_usuario\n"
                        + "	LEFT JOIN rrhh.empleados ON rrhh.empleados.id = seguridad.rbac_usuario.idempleado\n"
                        + "	WHERE rbac_usuario.idusu = " + idusuario + ") || '|%' \n"
                        + "ELSE\n"
                        + "'%|%'\n"
                        + "END\n"
                        + "OR\n"
                        + "actividad_tipos.id_cargo LIKE '%|0|%') \n"
                        + "AND id_status = 0 ORDER BY 2;";
                break;
            case 93:
                sql = "select descripcion, descripcion from instcondamb_patron_aux where estado=1  ORDER BY 2";
                break;
            case 94:
                sql = "SELECT c.id, c.descripcion||' ('||c.serie||') - piso: ' || piso || ', ' || capacidad\n"
                        + "FROM intrumento_condamb c inner join instcondamb_patron_aux a on c.descripcion = a.descripcion \n"
                        + "where a.sensor = 1 ORDER BY 2";
                break;
            case 95:
                sql = "SELECT distinct descripcion, descripcion from medidas order by 2";
                break;
            case 96:
                sql = "SELECT distinct descripcion, descripcion from patron_descripcion order by 2";
                break;
            case 97:
                sql = "SELECT distinct descripcion, descripcion from patron_descripcion order by 2";
                break;
            case 98:
                sql = "SELECT id, descripcion, '', case when estado = 1  then 0 else 1 end from inventarios.presentacion where id > 0  " + (opciones != null ? " and id <> " + opciones : "") + " order by 2";
                inactivo = 1;
                break;
            case 99:
                sql = "SELECT id, descripcion, '', case when estado = 1 then 0 else 1 end from inventarios.caracteristica where id > 0 " + (opciones != null ? " and id <> " + opciones : "") + " order by 2";
                inactivo = 1;
                break;
            case 100:
                sql = "SELECT distinct descripcion , descripcion from inventarios.modelos where estado = 1 and id > 0 ORDER BY 2";
                break;
            case 101:
                sql = " SELECT id, descripcion, '', case when estado = 1 then 0 else 1 end, responsables \n"
                        + " FROM inventarios.bodega b\n"
                        + " WHERE principal = 1 "
                        + " ORDER BY 2";
                break;
            case 102:
                sql = "SELECT id, descripcion || case when cambio > 0 then '(' || cambio || ')' else '' end || ' ' || simbolo, '', case when estado = 1 then '' else 'false' end from tipo_moneda where id > 0 order by 1";
                inactivo = 1;
                break;
            case 103:
                sql = "SELECT id, descripcion, descripcion, case when estado = 1 then '' else 'false' end from tipo_lineanegocio where id > 0 order by 1";
                inactivo = 1;
                break;
            case 104:
                sql = "SELECT id, razonsocial, razonsocial, case when estado = 1 then '' else 'false' end from empresa  WHERE id > 0  ORDER BY 2 ";
                inactivo = 1;
                break;
            case 105:
                sql = "SELECT descripcion, descripcion, descripcion, case when estado = 1 then '' else 'false' end FROM condicion_entrega_cotizacion order by 1";
                break;
            case 106:
                sql = "SELECT descripcion, descripcion, descripcion, case when estado = 1 then '' else 'false' end FROM plazo_entrega_cotizacion order by 1";
                break;
            case 107:
                sql = "SELECT descripcion, descripcion, descripcion, case when estado = 1 then '' else 'false' end FROM forma_pago_cotizacion where tipo = '" + opciones + "' order by 1";
                inactivo = 1;
                break;
            case 108:
                sql = "SELECT descripcion, descripcion, descripcion, '' from medidas order by 2";
                break;
            case 109:
                sql = "SELECT id, descripcion, descripcion, case when estado = 1 then '' else 'false' end  FROM public.clase order by 2";
                break;
            case 110:
                sql = "SELECT distinct c.id, c.descripcion, c.descripcion, case when c.estado = 1 then '' else 'false' end  "
                        + " FROM clase c inner join clase_nominal cn on cn.idclase = c.id "
                        + "  where idequipo = " + opciones + " order by 2";
                break;
            case 111:
                sql = "SELECT e.id, e.descripcion || ' (' || m.descripcion || ')', e.descripcion || ' (' || m.descripcion || ')',  case when e.estado = 1 then '' else 'false' end \n"
                        + "FROM equipo e inner join magnitudes m on m.id = e.idmagnitud  \n"
                        + "where m.pesa = 1 ORDER BY 2 ";
        }
        if (retorsql == 0) {
            combos = Globales.ObtenerCombo(sql, inicial, titulo, inactivo, json);
            if (combos.equals("")) {
                combos = "<option value''>SIN RESULTADOS</option>";
            }
            return combos;
        } else {
            return sql;
        }
    }

    public String CargaComboInicial(HttpServletRequest request) {
        String resultado = "";

        String[] sqlcombo = null;
        int[] iniciales = null;
        int[] inactivo = null;
        int[] title = null;

        int tipo_inicial = Globales.Validarintnonull(request.getParameter("tipo"));
        int json = Globales.Validarintnonull(request.getParameter("json"));

        switch (tipo_inicial) {
            case 1: //Devolucion

                sqlcombo = new String[8];
                iniciales = new int[8];
                inactivo = new int[8];
                for (int i = 0; i < 8; i++) {
                    inactivo[i] = 0;
                }

                sqlcombo[0] = CargarCombos(null, 1, 1, null, 1, 0);
                iniciales[0] = 1;

                sqlcombo[1] = CargarCombos(null, 2, 1, "0", 1, 0);
                iniciales[1] = 1;
                sqlcombo[2] = CargarCombos(null, 3, 1, null, 1, 0);
                iniciales[2] = 1;
                sqlcombo[3] = CargarCombos(null, 9, 1, null, 1, 0);
                iniciales[3] = 1;
                sqlcombo[4] = CargarCombos(null, 5, 1, "-1", 1, 0);
                iniciales[4] = 1;
                sqlcombo[5] = CargarCombos(null, 6, 1, "-1", 1, 0);
                iniciales[5] = 1;
                sqlcombo[6] = CargarCombos(null, 13, 1, null, 1, 0);
                iniciales[6] = 1;
                sqlcombo[7] = CargarCombos(null, 21, 1, null, 1, 0);
                iniciales[7] = 1;
                break;
            case 2: //COMERCIAL
                if (json == 0) {
                    sqlcombo = new String[27];
                    iniciales = new int[27];
                    inactivo = new int[27];
                    sqlcombo[0] = CargarCombos(null, 1, 1, null, 1, 0); //magnitud 0
                    iniciales[0] = 1;
                    inactivo[0] = 0;
                    sqlcombo[1] = CargarCombos(null, 2, 1, "0", 1, 0); //equipo 1 sin id
                    iniciales[1] = 1;
                    inactivo[1] = 0;
                    sqlcombo[2] = CargarCombos(null, 3, 1, null, 1, 0); //marca 2
                    iniciales[2] = 1;
                    inactivo[2] = 0;
                    sqlcombo[3] = CargarCombos(null, 4, 1, null, 1, 0); //servicio 3
                    iniciales[3] = 1;
                    inactivo[3] = 0;
                    sqlcombo[4] = CargarCombos(null, 8, 1, "-1", 1, 0); //medidas 4
                    iniciales[4] = 1;
                    inactivo[4] = 0;
                    sqlcombo[5] = CargarCombos(null, 9, 1, "-1", 1, 0); //clientes 5
                    iniciales[5] = 1;
                    inactivo[5] = 0;
                    sqlcombo[6] = CargarCombos(null, 5, 1, "-1", 1, 0); //modelos 6
                    iniciales[6] = 1;
                    inactivo[6] = 0;
                    sqlcombo[7] = CargarCombos(null, 6, 1, "-1", 1, 0); //intervalos 7
                    iniciales[7] = 1;
                    inactivo[7] = 0;
                    sqlcombo[8] = CargarCombos(null, 16, 1, null, 1, 0); //otro servicio 8
                    iniciales[8] = 1;
                    inactivo[8] = 0;
                    sqlcombo[9] = CargarCombos(null, 17, 1, "-1", 1, 0); //metodo 9
                    iniciales[9] = 1;
                    inactivo[9] = 0;
                    sqlcombo[10] = CargarCombos(null, 10, 1, null, 1, 0); //pais 10 
                    iniciales[10] = 1;
                    inactivo[10] = 0;
                    sqlcombo[11] = CargarCombos(null, 11, 1, "0", 1, 0); //departamento 11
                    iniciales[11] = 1;
                    inactivo[11] = 0;
                    sqlcombo[12] = CargarCombos(null, 12, 1, "0", 1, 0); //ciudad 12
                    iniciales[12] = 1;
                    inactivo[12] = 0;
                    sqlcombo[13] = CargarCombos(null, 13, 1, null, 1, 0);  //usuarios 13
                    iniciales[13] = 1;
                    inactivo[13] = 0;
                    sqlcombo[14] = CargarCombos(null, 33, 1, null, 1, 0); //accesorios 14
                    iniciales[14] = 1;
                    inactivo[14] = 0;
                    sqlcombo[15] = CargarCombos(null, 34, 1, null, 1, 0); //observaciones 15
                    iniciales[15] = 1;
                    inactivo[15] = 0;
                    sqlcombo[16] = CargarCombos(null, 91, 1, null, 1, 0); //transductor 16
                    iniciales[16] = 1;
                    inactivo[16] = 0;
                    sqlcombo[17] = CargarCombos(null, 7, 1, null, 1, 0); //proveedores 17
                    iniciales[17] = 1;
                    inactivo[17] = 0;
                    sqlcombo[18] = CargarCombos(null, 92, 1, "-1", 1, 0); //laboratorio 18
                    iniciales[18] = 1;
                    inactivo[18] = 0;
                    sqlcombo[19] = CargarCombos(null, 8, 1, "-1", 1, 0); //medidas 4
                    iniciales[19] = 1;
                    inactivo[19] = 0;
                    sqlcombo[20] = CargarCombos(null, 3, 1, "-1", 1, 0); //marca 2
                    iniciales[20] = 1;
                    inactivo[20] = 0;
                    sqlcombo[21] = CargarCombos(null, 19, 1, null, 1, 0); //empleado 19
                    iniciales[21] = 1;
                    inactivo[21] = 0;
                    sqlcombo[22] = CargarCombos(null, 93, 1, null, 1, 0); //Descripcion Instrumento 93
                    iniciales[22] = 1;
                    inactivo[22] = 0;
                    sqlcombo[23] = CargarCombos(null, 94, 1, null, 1, 0); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[23] = 1;
                    inactivo[13] = 0;
                    sqlcombo[24] = CargarCombos(null, 95, 1, null, 1, 0); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[24] = 1;
                    inactivo[24] = 0;
                    sqlcombo[25] = CargarCombos(null, 96, 1, null, 1, 0); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[25] = 1;
                    inactivo[25] = 0;
                    sqlcombo[26] = CargarCombos(null, 71, 1, null, 1, 0); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[26] = 1;
                    inactivo[26] = 0;

                } else {
                    sqlcombo = new String[37];
                    iniciales = new int[37];
                    title = new int[37];
                    inactivo = new int[37];
                    sqlcombo[0] = CargarCombos(null, 1, 1, null, 1, 1); //magnitud 0
                    iniciales[0] = 1;
                    inactivo[0] = 0;
                    sqlcombo[1] = CargarCombos(null, 2, 1, "0", 1, 1); //equipo 1 sin id
                    iniciales[1] = 1;
                    inactivo[1] = 0;
                    sqlcombo[2] = CargarCombos(null, 3, 0, null, 1, 1); //marca 2
                    iniciales[2] = 1;
                    inactivo[2] = 0;
                    sqlcombo[3] = CargarCombos(null, 4, 0, null, 1, 1); //servicio 3
                    iniciales[3] = 1;
                    inactivo[3] = 0;
                    sqlcombo[4] = CargarCombos(null, 108, 0, "-1", 1, 1); //medidas 4
                    iniciales[4] = 1;
                    inactivo[4] = 1;
                    sqlcombo[5] = CargarCombos(null, 9, 0, "-1", 1, 1); //clientes 5
                    iniciales[5] = 1;
                    inactivo[5] = 0;
                    sqlcombo[6] = CargarCombos(null, 5, 0, "-1", 1, 1); //modelos 6
                    iniciales[6] = 1;
                    inactivo[6] = 0;
                    sqlcombo[7] = CargarCombos(null, 6, 0, "-1", 1, 1); //intervalos 7
                    iniciales[7] = 1;
                    inactivo[7] = 0;
                    sqlcombo[8] = CargarCombos(null, 16, 0, null, 1, 1); //otro servicio 8
                    iniciales[8] = 1;
                    inactivo[8] = 1;
                    sqlcombo[9] = CargarCombos(null, 17, 0, "-1", 1, 1); //metodo 9
                    iniciales[9] = 1;
                    inactivo[9] = 0;
                    sqlcombo[10] = CargarCombos(null, 10, 0, null, 1, 1); //pais 10 
                    iniciales[10] = 1;
                    inactivo[10] = 0;
                    sqlcombo[11] = CargarCombos(null, 11, 0, "0", 1, 1); //departamento 11
                    iniciales[11] = 1;
                    inactivo[11] = 0;
                    sqlcombo[12] = CargarCombos(null, 12, 0, "0", 1, 1); //ciudad 12
                    iniciales[12] = 1;
                    inactivo[12] = 0;
                    sqlcombo[13] = CargarCombos(null, 13, 0, null, 1, 1);  //usuarios 13
                    iniciales[13] = 1;
                    inactivo[13] = 0;
                    sqlcombo[14] = CargarCombos(null, 33, 0, null, 1, 1); //accesorios 14
                    iniciales[14] = 1;
                    inactivo[14] = 0;
                    sqlcombo[15] = CargarCombos(null, 34, 0, null, 1, 1); //observaciones 15
                    iniciales[15] = 1;
                    inactivo[15] = 0;
                    sqlcombo[16] = CargarCombos(null, 91, 0, null, 1, 1); //transductor 16
                    iniciales[16] = 1;
                    inactivo[16] = 0;
                    sqlcombo[17] = CargarCombos(null, 7, 0, null, 1, 1); //proveedores 17
                    iniciales[17] = 1;
                    inactivo[17] = 0;
                    sqlcombo[18] = CargarCombos(null, 92, 0, "-1", 1, 1); //laboratorio 18
                    iniciales[18] = 1;
                    inactivo[18] = 0;
                    sqlcombo[19] = CargarCombos(null, 8, 0, "-1", 1, 1); //medidas 4
                    iniciales[19] = 1;
                    inactivo[19] = 0;
                    sqlcombo[20] = CargarCombos(null, 3, 0, "-1", 1, 1); //marca 2
                    iniciales[20] = 1;
                    inactivo[20] = 0;
                    sqlcombo[21] = CargarCombos(null, 19, 0, null, 1, 1); //empleado 19
                    iniciales[21] = 1;
                    inactivo[21] = 0;
                    sqlcombo[22] = CargarCombos(null, 93, 0, null, 1, 1); //Descripcion Instrumento 93
                    iniciales[22] = 1;
                    inactivo[22] = 0;
                    sqlcombo[23] = CargarCombos(null, 94, 0, null, 1, 1); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[23] = 1;
                    inactivo[23] = 0;
                    sqlcombo[24] = CargarCombos(null, 95, 0, null, 1, 1); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[24] = 1;
                    inactivo[24] = 0;
                    sqlcombo[25] = CargarCombos(null, 96, 0, null, 1, 1); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[25] = 1;
                    inactivo[25] = 0;
                    sqlcombo[26] = CargarCombos(null, 71, 0, null, 1, 1); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[26] = 1;
                    inactivo[26] = 0;
                    sqlcombo[27] = CargarCombos(null, 102, 0, null, 1, 1); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[27] = 1;
                    inactivo[27] = 1;
                    sqlcombo[28] = CargarCombos(null, 103, 0, null, 1, 1); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[28] = 1;
                    inactivo[28] = 1;
                    sqlcombo[29] = CargarCombos(null, 104, 0, null, 1, 1); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[29] = 1;
                    inactivo[29] = 1;
                    sqlcombo[30] = CargarCombos(null, 47, 0, null, 1, 1); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[30] = 1;
                    inactivo[30] = 1;
                    sqlcombo[31] = CargarCombos(null, 58, 0, null, 1, 1); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[31] = 1;
                    inactivo[31] = 0;
                    sqlcombo[32] = CargarCombos(null, 81, 0, null, 1, 1); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[32] = 1;
                    inactivo[32] = 1;
                    sqlcombo[33] = CargarCombos(null, 105, 0, null, 1, 1); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[33] = 1;
                    inactivo[33] = 1;
                    sqlcombo[34] = CargarCombos(null, 106, 0, null, 1, 1); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[34] = 1;
                    inactivo[34] = 1;
                    sqlcombo[35] = CargarCombos(null, 81, 0, null, 1, 1); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[35] = 1;
                    inactivo[35] = 1;
                    sqlcombo[36] = CargarCombos(null, 81, 0, null, 1, 1); //Descripcion Instrumento Serial Capacidad  93
                    iniciales[36] = 1;
                    inactivo[36] = 0;
                }
                break;
            case 3: //Solicitud

                sqlcombo = new String[8];
                iniciales = new int[8];
                inactivo = new int[8];
                for (int i = 0; i < 8; i++) {
                    inactivo[i] = 0;
                }

                sqlcombo[0] = CargarCombos(null, 1, 1, null, 1, 0); //magnitud 0
                iniciales[0] = 1;
                sqlcombo[1] = CargarCombos(null, 2, 1, "0", 1, 0); //equipo 1
                iniciales[1] = 1;
                sqlcombo[2] = CargarCombos(null, 3, 1, null, 1, 0); //marca 2
                iniciales[2] = 1;
                sqlcombo[3] = CargarCombos(null, 9, 1, null, 1, 0); //clientes 3
                iniciales[3] = 1;
                sqlcombo[4] = CargarCombos(null, 5, 1, "-1", 1, 0); //modelos 4
                iniciales[4] = 1;
                sqlcombo[5] = CargarCombos(null, 6, 1, "-1", 1, 0); //intervalos 5
                iniciales[5] = 1;
                sqlcombo[6] = CargarCombos(null, 13, 1, null, 1, 0); //usuarios 6
                iniciales[6] = 1;
                sqlcombo[7] = CargarCombos(null, 8, 1, null, 1, 0); //medidas 7
                iniciales[7] = 1;
                break;
            case 4: //Salida

                sqlcombo = new String[9];
                iniciales = new int[9];
                inactivo = new int[9];
                for (int i = 0; i < 9; i++) {
                    inactivo[i] = 0;
                }

                sqlcombo[0] = CargarCombos(null, 1, 1, null, 1, 0); //magnitud 0
                iniciales[0] = 1;
                sqlcombo[1] = CargarCombos(null, 2, 1, "0", 1, 0); //equipo 1
                iniciales[1] = 1;
                sqlcombo[2] = CargarCombos(null, 3, 1, null, 1, 0); //marca 2
                iniciales[2] = 1;
                sqlcombo[3] = CargarCombos(null, 9, 1, null, 1, 0); //clientes 3
                iniciales[3] = 1;
                sqlcombo[4] = CargarCombos(null, 7, 1, null, 1, 0); //proveedores 4
                iniciales[4] = 1;
                sqlcombo[5] = CargarCombos(null, 5, 1, "-1", 1, 0); //modelos 5
                iniciales[5] = 1;
                sqlcombo[6] = CargarCombos(null, 6, 1, "-1", 1, 0); //intervalos 6
                iniciales[6] = 1;
                sqlcombo[7] = CargarCombos(null, 13, 1, null, 1, 0); //usuarios 7
                iniciales[7] = 1;
                sqlcombo[8] = CargarCombos(null, 32, 1, null, 1, 0); //motivo_salida 8
                iniciales[8] = 1;
                break;
            case 5: //Facturacion

                sqlcombo = new String[9];
                iniciales = new int[9];
                inactivo = new int[9];
                for (int i = 0; i < 9; i++) {
                    inactivo[i] = 0;
                }

                sqlcombo[0] = CargarCombos(null, 1, 1, null, 1, 0); //magnitud 0
                iniciales[0] = 1;
                sqlcombo[1] = CargarCombos(null, 2, 1, "0", 1, 0); //equipo 1
                iniciales[1] = 1;
                sqlcombo[2] = CargarCombos(null, 3, 1, null, 1, 0); //marca 2
                iniciales[2] = 1;
                sqlcombo[3] = CargarCombos(null, 9, 1, null, 1, 0); //clientes 3
                iniciales[3] = 1;
                sqlcombo[4] = CargarCombos(null, 5, 1, "-1", 1, 0); //modelos 4
                iniciales[4] = 1;
                sqlcombo[5] = CargarCombos(null, 6, 1, "-1", 1, 0); //intervalos 5
                iniciales[5] = 1;
                sqlcombo[6] = CargarCombos(null, 13, 1, null, 1, 0); //usuarios 6
                iniciales[6] = 1;
                sqlcombo[7] = CargarCombos(null, 55, 0, null, 1, 0); //IVA 7
                iniciales[7] = 0;
                sqlcombo[8] = CargarCombos(null, 16, 1, null, 1, 0); //otro servicio 8
                iniciales[8] = 1;
                break;
        }
        for (int x = 0; x < sqlcombo.length; x++) {
            if (!resultado.equals("")) {
                resultado += "||";
            }
            resultado += Globales.ObtenerCombo(sqlcombo[x], iniciales[x], 0, inactivo[x], json);
        }
        return resultado;
    }

    public String ObtenerChat() {
        sql = "SELECT m.*, coalesce(e.foto,'') as foto \n"
                + "from mensajes m inner join seguridad.rbac_usuario u on u.nomusu = m.codusu\n"
                + "		left join rrhh.empleados e on e.id = u.idempleado\n"
                + "ORDER BY id desc limit 500";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ObtenerChat");
    }

    public String AlertaVueltas() {
        sql = "SELECT * FROM alerta_vueltas(" + idusuario + ")";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->AlertaVueltas");
    }

    public String TablaConfiguracion(HttpServletRequest request) {
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int estado = 0;
        int banco = 0;

        switch (tipo) {
            case 1:
                sql = "SELECT m.*, te.descripcion as desestado, tp.descripcion as desprecio "
                        + " FROM magnitudes m INNER JOIN tipo_estado te on te.id = m.estado "
                        + "INNER JOIN tipo_precio tp on tp.id = m.tipoprecio " + (id > 0 ? " WHERE m.id=" + id : "")
                        + "ORDER BY descripcion";
                break;
            case 2:
                sql = "SELECT e.id, e.descripcion, e.estado, idmagnitud, codigo, te.descripcion as desestado, m.descripcion as magnitud,\n"
                        + "'<button class=''btn btn-glow btn-danger'' title=''Eliminar Equipo'' type=''button'' onclick=' || chr(34) || 'EliminarEquipo(' || e.id || ',''' || e.descripcion  || ' (' || m.descripcion || ')''' || ')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar\n"
                        + "FROM equipo e INNER JOIN tipo_estado te on te.id = e.estado\n"
                        + "INNER JOIN magnitudes m on m.id = e.idmagnitud " + (id > 0 ? " WHERE e.id=" + id : magnitud != 0 ? " WHERE idmagnitud = " + magnitud : "")
                        + "ORDER BY e.descripcion";
                break;
            case 3:
                sql = "SELECT id, descripcion, "
                        + "'<button class=''btn btn-glow btn-danger'' title=''Eliminar Medida'' type=''button'' onclick=' || chr(34) || 'EliminarMedida(' || id || ',''' || descripcion  || ''')'  ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar  "
                        + "FROM public.medidas " + (id > 0 ? " WHERE id=" + id : "");
                break;
            case 4:
                sql = "SELECT mi.id, idmagnitud, medida, desde, hasta, te.descripcion as desestado, m.descripcion as magnitud, mi.estado, "
                        + "'<button class=''btn btn-glow btn-danger'' title=''Eliminar Rango'' type=''button'' onclick=' || chr(34) || 'EliminarIntervalos(' || mi.id || ',''' || mi.medida  || ' ' || desde || ' a ' ||  hasta || '(' || m.descripcion || ')''' || ')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar "
                        + "FROM public.magnitud_intervalos mi INNER JOIN tipo_estado te on te.id = mi.estado "
                        + "INNER JOIN magnitudes m on m.id = mi.idmagnitud " + (id > 0 ? " WHERE mi.id = " + id : magnitud != 0 ? " WHERE idmagnitud = " + magnitud : "");
                break;
            case 5:
                sql = "SELECT m.id, m.descripcion, m.estado, te.descripcion as desestado, "
                        + " '<button class=''btn btn-glow btn-danger'' title=''Eliminar Marca'' type=''button'' onclick=' || chr(34) || 'EliminarMarca(' || m.id || ',''' || m.descripcion || ''')' || chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar "
                        + "FROM public.marcas m INNER JOIN tipo_estado te on te.id = m.estado " + (id > 0 ? " WHERE m.id = " + id : "");
                break;

            case 6:
                sql = "SELECT mo.id, mo.idmarca, mo.descripcion, mo.estado, te.descripcion as desestado, m.descripcion as marca,  "
                        + "'<button class=''btn btn-glow btn-danger'' title=''Eliminar Modelo'' type=''button'' onclick=' || chr(34) || 'EliminarModelo(' || mo.id || ',''' || mo.descripcion || '(' || m.descripcion || ')'',' || mo.idmarca || ')' ||  chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar  "
                        + "FROM modelos mo inner join marcas m on m.id = mo.idmarca  "
                        + "INNER JOIN tipo_estado te on te.id = mo.estado " + (id > 0 ? " WHERE mo.id = " + id : "");
                break;
            case 7:
                if (id == 0) {
                    sql = "SELECT s.id, codigo, s.nombre, te.descripcion as estado, case when proveedor = 0 then 'NO' ELSE 'SI' END AS proveedor, case when express = 0 then 'NO' ELSE 'SI' END AS express, case when sitio = 0 then 'NO' ELSE 'SI' END as sitio,  "
                            + "iv.descripcion as iva, diaentrega, "
                            + "case when otro = 0 then 'NINGUNO' "
                            + "WHEN otro = 1 then 'SOLO OBSERVACIONES' "
                            + "WHEN otro = 2 then 'TODOS LOS DATOS DEL CERTIFICADO'  "
                            + "WHEN otro = 3 then 'SIN PRECIOS' end as otro, case when descuento = 1 then 'SI' else 'NO' END AS descuento, case when acreditadoser = 0 then 'NO' ELSE 'SI' END AS acreditado, "
                            + "'<button class=''btn btn-glow btn-danger'' title=''Eliminar Servicio'' type=''button'' onclick=' || chr(34) || 'EliminarServicio(' || s.id || ',''' || s.nombre || ''')' ||  chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar, "
                            + "cu.cuenta || ' ' || cu.nombre as cuenta "
                            + "FROM servicio s INNER JOIN iva iv on iv.id = s.idiva "
                            + "INNER JOIN tipo_estado te on te.id = s.estado "
                            + "inner join contabilidad.cuenta cu on cu.id = s.idcuecontable";
                } else {
                    sql = "SELECT * FROM servicio WHERE id=" + id;
                }
                break;
            case 8:
                if (id == 0) {
                    sql = "SELECT s.id, s.descripcion, porcentaje, precio, iv.descripcion as iva, te.descripcion as estado, inicial,  "
                            + "'<button class=''btn btn-glow btn-danger'' title=''Eliminar Otro Servicio'' type=''button'' onclick=' || chr(34) || 'EliminarOtroServicio(' || s.id || ',''' || s.descripcion || ''')' ||  chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar,  "
                            + "cu.cuenta || ' ' || cu.nombre as cuenta "
                            + "FROM otros_servicios s INNER JOIN iva iv on iv.id = s.idiva "
                            + "INNER JOIN tipo_estado te on te.id = s.estado "
                            + "inner join contabilidad.cuenta cu on cu.id = s.idcuecontable";
                } else {
                    sql = "SELECT * FROM otros_servicios WHERE id = " + id;
                }
                break;
            case 9:
                if (id == 0) {
                    sql = "SELECT m.id, m.descripcion, obtener_equipomet(m.id) as equipos, te.descripcion as estado,   "
                            + "'<button class=''btn btn-glow btn-danger'' title=''Eliminar Metodo'' type=''button'' onclick=' || chr(34) || 'EliminarMetodo(' || m.id || ',''' || m.descripcion || ''')' ||  chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar "
                            + "FROM metodo m INNER JOIN tipo_estado te on te.id = m.estado where m.id > 0 " + (estado > 0 ? " and m.estado=" + estado : "");
                } else {
                    sql = "SELECT *, obtener_equipometid(id) as equipos FROM metodo WHERE id = " + id;
                }
                break;
            case 10:
                if (id == 0) {
                    sql = "SELECT p.id, p.codigo, p.descripcion, obtener_equipoplantilla(p.id) as equipos, te.descripcion as estado,  "
                            + "case when p.acreditado = 0 then 'NO' else 'SI' end acreditada, "
                            + "'<button class=''btn btn-glow btn-danger'' title=''Eliminar Plantilla'' type=''button'' onclick=' || chr(34) || 'EliminarPlantilla(' || p.id || ',''' || p.descripcion || ''')' ||  chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' ||  "
                            + "case when excel > 0 then '<button class=''btn btn-glow btn-info'' title=''Descargar Excel'' type=''button'' onclick=' || chr(34) || 'DescargarExcel(' || p.id || ',''' || p.tipoarchivo || ''')' ||  chr(34) || '><span data-icon=''&#xe230;''></span></button>' else '' end as eliminar "
                            + "FROM plantillas p INNER JOIN tipo_estado te on te.id = p.estado where p.id <> 0";
                } else {
                    sql = "SELECT *, obtener_equipoplantillaid(id) as equipos FROM plantillas WHERE id = " + id;
                }
                break;
            case 11:
                sql = "SELECT b.id, b.descripcion, b.estado, b.tipoarchivo, b.consignar, te.descripcion as desestado,  "
                        + "'<button class=''btn btn-glow btn-danger'' title=''Eliminar Banco'' type=''button'' onclick=' || chr(34) || 'EliminarBanco(' || b.id || ',''' || b.descripcion || ''')' ||  chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar "
                        + "FROM bancos b INNER JOIN tipo_estado te on te.id = b.estado "
                        + "where b.id > 0 " + (id > 0 ? " and b.id = " + id : "") + " order by 2 ";
                break;
            case 12:
                sql = "SELECT bc.id, bc.idbanco, bc.cuenta, bc.estado, bc.idtipocuenta, b.descripcion as banco, tc.descripcion as tipocuenta, te.descripcion as desestado, bc.idcuecontable, c.id as cuentacontable, \n"
                        + "'<button class=''btn btn-glow btn-danger'' title=''Eliminar Cuenta Bancaria'' type=''button'' onclick=' || chr(34) || 'EliminarCuentaBanco(' || bc.id || ',''' || tc.descripcion || ' ' || bc.cuenta || ''')' ||  chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar\n"
                        + "FROM bancos_cuenta bc inner join bancos b on b.id = bc.idbanco\n"
                        + "inner join tipo_cuenta tc on tc.id = idtipocuenta\n"
                        + "INNER JOIN contabilidad.cuenta c on c.id = bc.idcuecontable\n"
                        + "INNER JOIN tipo_estado te on te.id = bc.estado\n"
                        + "WHERE bc.id > 0 " + (id > 0 ? " and bc.id = " + id : "") + (banco > 0 ? " and bc.idbanco = " + banco : "")
                        + "ORDER BY b.descripcion, bc.cuenta";
                break;
            case 13:
                sql = "SELECT l.id, leyenda, obtener_comboleyenda(1,servicios) AS a_servicios, obtener_comboleyenda(2,equipo_cal) as a_equipo_cal, te.descripcion as desestado, \n"
                        + "	  obtener_comboleyenda(3,grupos) as a_grupos, obtener_comboleyenda(4,equipo_inv) as a_equipo_inv, to_char(fecha,'yyyy/MM/dd HH24:MI') as fecha, \n"
                        + "   l.estado, servicios, equipo_cal,  grupos, equipo_inv, l.estado,  \n"
                        + " '<button class=''btn btn-glow btn-danger'' title=''Eliminar Leyenda de Cotización'' type=''button'' onclick=' || chr(34) || 'EliminarLeyenda(' || l.id || ')' ||  chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar "
                        + " FROM leyenda_cotizacion l INNER JOIN tipo_estado te on te.id = l.estado "
                        + (id > 0 ? " where l.id = " + id : "") + " order by l.id";
                break;
        }
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaConfiguracion");
    }

    public String GuardarPrecios(HttpServletRequest request) {
        int Precio_Id = Globales.Validarintnonull(request.getParameter("Precio_Id"));
        int MTipoPrecio = Globales.Validarintnonull(request.getParameter("MTipoPrecio"));
        int MServicio = Globales.Validarintnonull(request.getParameter("MServicio"));
        int MMagnitud = Globales.Validarintnonull(request.getParameter("MMagnitud"));
        int MEquipo = Globales.Validarintnonull(request.getParameter("MEquipo"));
        int MMedida = Globales.Validarintnonull(request.getParameter("MMedida"));
        int MMarca = Globales.Validarintnonull(request.getParameter("MMarca"));
        int MModelo = Globales.Validarintnonull(request.getParameter("MModelo"));
        double MDesde = Globales.ValidardoublenoNull(request.getParameter("MDesde"));
        double MHasta = Globales.ValidardoublenoNull(request.getParameter("MHasta"));
        int MProveedor = Globales.Validarintnonull(request.getParameter("MProveedor"));
        double MPrecio1 = Globales.ValidardoublenoNull(request.getParameter("MPrecio1"));
        double MPrecio2 = Globales.ValidardoublenoNull(request.getParameter("MPrecio2"));
        double MPrecio3 = Globales.ValidardoublenoNull(request.getParameter("MPrecio3"));
        double MPrecio4 = Globales.ValidardoublenoNull(request.getParameter("MPrecio4"));
        double MPrecio5 = Globales.ValidardoublenoNull(request.getParameter("MPrecio5"));
        double MPrecio6 = Globales.ValidardoublenoNull(request.getParameter("MPrecio6"));
        double MPrecio7 = Globales.ValidardoublenoNull(request.getParameter("MPrecio7"));
        double MPrecio8 = Globales.ValidardoublenoNull(request.getParameter("MPrecio8"));
        double MPrecio9 = Globales.ValidardoublenoNull(request.getParameter("MPrecio9"));
        double MPrecio10 = Globales.ValidardoublenoNull(request.getParameter("MPrecio10"));
        double MCosto = Globales.ValidardoublenoNull(request.getParameter("MCosto"));
        double MPrecioUSD1 = Globales.ValidardoublenoNull(request.getParameter("MPrecioUSD1"));
        double MPrecioUSD2 = Globales.ValidardoublenoNull(request.getParameter("MPrecioUSD2"));
        double MPrecioUSD3 = Globales.ValidardoublenoNull(request.getParameter("MPrecioUSD3"));
        double MPrecioUSD4 = Globales.ValidardoublenoNull(request.getParameter("MPrecioUSD4"));
        double MPrecioUSD5 = Globales.ValidardoublenoNull(request.getParameter("MPrecioUSD5"));
        double MPrecioUSD6 = Globales.ValidardoublenoNull(request.getParameter("MPrecioUSD6"));
        double MPrecioUSD7 = Globales.ValidardoublenoNull(request.getParameter("MPrecioUSD7"));
        double MPrecioUSD8 = Globales.ValidardoublenoNull(request.getParameter("MPrecioUSD8"));
        double MPrecioUSD9 = Globales.ValidardoublenoNull(request.getParameter("MPrecioUSD9"));
        double MPrecioUSD10 = Globales.ValidardoublenoNull(request.getParameter("MPrecioUSD10"));
        double MCostoUSD = Globales.ValidardoublenoNull(request.getParameter("MCostoUSD"));
        String Mensaje = "";
        try {

            int Contador = 0;

            sql = "select id "
                    + " FROM servicio_precios  "
                    + " WHERE idmodelo = " + MModelo + " and  idproveedor = " + MProveedor + " and idequipo = " + MEquipo + " and idmedida = " + MMedida
                    + " and desde = '" + MDesde + "' and hasta = '" + MHasta + "' and idservicio=" + MServicio + " and id <> " + Precio_Id;
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (encontrado > 0) {
                return "1|Ya se encuentra registrado un precio con estas características (Servicio, Modelo, Inrtervalo, Proveedor, Medida, Intervalo)";
            }

            sql = "select max(sp.contador) "
                    + " FROM servicio_precios sp INNER JOIN magnitudes m on m.id = sp.idmagnitud "
                    + " WHERE m.id=" + MMagnitud;
            Contador = Globales.Validarintnonull(Globales.ObtenerUnValor(sql)) + 1;

            if (Precio_Id == 0) {
                if (Globales.PermisosSistemas("PRECIOS GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO servicio_precios(contador, idservicio, idequipo, idmagnitud, idmarca, idmodelo, idproveedor, tipoprecio, "
                        + "                              idmedida, desde, hasta, costo, "
                        + "                           precio1, precio2, precio3, precio4, precio5,  "
                        + "                           precio6, precio7, precio8, precio9, precio10,"
                        + "                           costousd,preciousd1, preciousd2, preciousd3, preciousd4, preciousd5,  "
                        + "                           preciousd6, preciousd7, preciousd8, preciousd9, preciousd10) "
                        + "VALUES (" + Contador + "," + MServicio + "," + MEquipo + "," + MMagnitud + "," + MMarca + "," + MModelo + "," + MProveedor + "," + MTipoPrecio
                        + "," + MMedida + ",'" + MDesde + "','" + MHasta + "'," + MCosto
                        + "," + MPrecio1 + "," + MPrecio2 + "," + MPrecio3 + "," + MPrecio4 + "," + MPrecio5
                        + "," + MPrecio6 + "," + MPrecio7 + "," + MPrecio8 + "," + MPrecio9 + "," + MPrecio10
                        + "," + MCostoUSD
                        + "," + MPrecioUSD1 + "," + MPrecioUSD2 + "," + MPrecioUSD3 + "," + MPrecioUSD4 + "," + MPrecioUSD5
                        + "," + MPrecioUSD6 + "," + MPrecioUSD7 + "," + MPrecioUSD8 + "," + MPrecioUSD9 + "," + MPrecioUSD10 + ")";
                if (Globales.DatosAuditoria(sql, "PRECIOS", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarPrecios")) {
                    Precio_Id = Globales.Validarintnonull(Globales.ObtenerUnValor("select max(id) from servicio_precios"));
                    Mensaje = "0|Precios agregado con éxito|" + Precio_Id;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                if (Globales.PermisosSistemas("PRECIOS EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE servicio_precios "
                        + "      SET  tipoprecio=" + MTipoPrecio + ",idservicio=" + MServicio + ",idequipo=" + MEquipo + ",idmagnitud=" + MMagnitud + ",idmarca=" + MMarca + ",idmodelo=" + MModelo + ",idproveedor=" + MProveedor
                        + ",idmedida=" + MMedida + ",desde=" + MDesde + ",hasta=" + MHasta + ", costo=" + MCosto
                        + ",precio1=" + MPrecio1 + ",precio2=" + MPrecio2 + ",precio3=" + MPrecio3 + ",precio4=" + MPrecio4 + ",precio5=" + MPrecio5
                        + ",precio6=" + MPrecio6 + ",precio7=" + MPrecio7 + ",precio8=" + MPrecio8 + ",precio9=" + MPrecio9 + ",precio10=" + MPrecio10
                        + ",costousd=" + MCostoUSD
                        + ",preciousd1=" + MPrecioUSD1 + ",preciousd2=" + MPrecioUSD2 + ",preciousd3=" + MPrecioUSD3 + ",preciousd4=" + MPrecioUSD4 + ",preciousd5=" + MPrecioUSD5
                        + ",preciousd6=" + MPrecioUSD6 + ",preciousd7=" + MPrecioUSD7 + ",preciousd8=" + MPrecioUSD8 + ",preciousd9=" + MPrecioUSD9 + ",preciousd10=" + MPrecioUSD10
                        + " WHERE id = " + Precio_Id;
                if (Globales.DatosAuditoria(sql, "PRECIOS", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarPrecios")) {
                    Mensaje = "0|Precios actualizados con éxito|" + Precio_Id;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String TablaSede(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        sql = "SELECT cs.*, c.descripcion as ciudad, d.descripcion as departamento, c.iddepartamento, d.idpais,  "
                + "          '<button class=''btn btn-glow btn-danger'' title=''Eliminar Sede'' type=''button'' onclick=' || chr(34) || 'EliminarSede(' || cs.id || ',''' || cs.nombre  || '(' || cli.nombrecompleto || ')''' || ')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar "
                + "          FROM clientes_sede cs inner join ciudad c on cs.idciudad = c.id "
                + "                                INNER JOIN clientes cli on cli.id = cs.idcliente  "
                + "                                inner join departamento d on d.id = c.iddepartamento  "
                + "          where idcliente = " + cliente + " ORDER BY 2";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaSede");
    }

    //
    public String TablaContacto(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        sql = "SELECT cc.*, "
                + "  '<button class=''btn btn-glow btn-danger'' title=''Eliminar Contacto'' type=''button'' onclick=' || chr(34) || 'EliminarContacto(' || cc.id || ',''' || cc.nombres  || '(' || c.nombrecompleto || ')''' || ')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar, "
                + "  '<button class=''btn btn-glow btn-success'' title=''Enviar Clave'' type=''button'' onclick=' || chr(34) || 'ClaveContacto(' || cc.id || ',''' || cc.email || ''',''' || cc.nombres || ''')' ||chr(34) || '><span data-icon=''&#xe03c;''></span></button>' as correo, "
                + "  '<button class=''btn btn-glow btn-primary'' title=''Permisos del Contacto'' type=''button'' onclick=' || chr(34) || 'LlamarPermisos(' || cc.id || ',''' || cc.email || ''',''' || cc.nombres || ''')' ||chr(34) || '><span data-icon=''&#xe1a3;''></span></button>' as permiso "
                + "  FROM clientes_contac cc INNER JOIN clientes c on c.id = cc.idcliente  "
                + "  where idcliente = " + cliente;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaContacto");
    }

    public String GuardarCliente(HttpServletRequest request) {
        int IdCliente = Globales.Validarintnonull(request.getParameter("IdCliente"));
        int TipoCliente = Globales.Validarintnonull(request.getParameter("TipoCliente"));
        String Documento = request.getParameter("Documento");
        String Nombres = request.getParameter("Nombres");
        String Comercial = request.getParameter("Comercial");
        String Telefonos = request.getParameter("Telefonos");
        String Celular = request.getParameter("Celular");
        String Email = request.getParameter("Email");
        int Ciudad = Globales.Validarintnonull(request.getParameter("Ciudad"));
        String Direccion = request.getParameter("Direccion");
        String Entregasinfac = request.getParameter("Entregasinfac");
        String EmailCartera = request.getParameter("EmailCartera");
        int TablaPrecios = Globales.Validarintnonull(request.getParameter("TablaPrecios"));
        String Certificado = request.getParameter("Certificado");
        int Descuento = Globales.Validarintnonull(request.getParameter("Descuento"));
        int Estado = Globales.Validarintnonull(request.getParameter("Estado"));
        String EnviarCertificado = request.getParameter("EnviarCertificado");
        int ReteFuente = Globales.Validarintnonull(request.getParameter("ReteFuente"));
        int ReteIva = Globales.Validarintnonull(request.getParameter("ReteIva"));
        int ReteIca = Globales.Validarintnonull(request.getParameter("ReteIca"));
        int Asesor = Globales.Validarintnonull(request.getParameter("Asesor"));
        String Habitual = request.getParameter("Habitual");
        String InformeTecnico = request.getParameter("InformeTecnico");
        String Solicitud = request.getParameter("Solicitud");
        String Cotizacion = request.getParameter("Cotizacion");
        String Ajuste = request.getParameter("Ajuste");
        int iva = Globales.Validarintnonull(request.getParameter("Iva"));
        String FirmaCertificado = request.getParameter("FirmaCertificado");
        int CuentaContable = Globales.Validarintnonull(request.getParameter("CuentaContable"));
        double Limite = Globales.ValidardoublenoNull(request.getParameter("Limite"));
        int PlazoPago = Globales.Validarintnonull(request.getParameter("PlazoPago"));
        int DiasPre = Globales.Validarintnonull(request.getParameter("DiasPre"));
        int DiasBlo = Globales.Validarintnonull(request.getParameter("DiasBlo"));

        if (InformeTecnico == null) {
            InformeTecnico = "SI";
        }
        try {
            if (IdCliente == 0) {
                if (Globales.PermisosSistemas("CLIENTE AGREGAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO clientes(idtipcli, documento, nombrecompleto, telefono, celular, "
                        + "  email, limitecre, diasbloqueo, diasprebloqueo, tablaprecio, "
                        + "  descuento, plazopago, pagcertificado, nombrecomer, retirarsinfac, direccion, idciudad, "
                        + "  reteiva, reteica, retefuente, req_solicitud, aprobar_cotizacion, aprobar_ajuste, emailcartera, asesor, habitual, "
                        + "  idiva, firmacertificado, paginformetecnico,idcuecontable, enviar_certificado) "
                        + "      VALUES (" + TipoCliente + ",'" + Documento.trim() + "','" + Nombres.trim().toUpperCase() + "','" + Telefonos + "','" + Celular
                        + "','" + Email + "'," + Limite + "," + DiasBlo + "," + DiasPre + "," + TablaPrecios
                        + "," + Descuento + "," + PlazoPago + ",'" + Certificado + "','" + Comercial.toUpperCase() + "','" + Entregasinfac + "',"
                        + "'" + Direccion + "'," + Ciudad
                        + "," + ReteIva + ",'" + ReteIca + "'," + ReteFuente
                        + ",'" + Solicitud + "','" + Cotizacion + "','" + Ajuste + "','" + EmailCartera + "'," + Asesor
                        + ",'" + Habitual + "'," + iva + ",'" + FirmaCertificado + "','" + InformeTecnico + "',"
                        + CuentaContable + ",'" + EnviarCertificado + "');";
                if (Globales.DatosAuditoria(sql, "CLIENTE", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarCliente")) {
                    sql = "SELECT MAX(id) FROM Clientes;";
                    IdCliente = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    sql = "INSERT INTO clientes_sede (nombre, idciudad, idcliente, direccion) "
                            + " VALUES ('SEDE PRINCIPAL'," + Ciudad + "," + IdCliente + ",'" + Direccion + "')";
                    Globales.DatosAuditoria(sql, "CLIENTE", "AGREGAR SEDE", idusuario, iplocal, this.getClass() + "-->GuardarCliente");
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("CLIENTE EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE clientes "
                        + "      SET idtipcli=" + TipoCliente + ", documento='" + Documento.trim() + "', nombrecompleto='" + Nombres.toUpperCase()
                        + "', telefono='" + Telefonos + "', celular='" + Celular + "', email='" + Email + "',"
                        + "limitecre=" + Limite + ",diasbloqueo=" + DiasBlo + ",diasprebloqueo=" + DiasBlo
                        + ",tablaprecio=" + TablaPrecios + ",descuento=" + Descuento + ",estado=" + Estado
                        + ", plazopago=" + PlazoPago + ",pagcertificado='" + Certificado + "', direccion  = '" + Direccion + "'"
                        + ",nombrecomer ='" + Comercial.toUpperCase() + "',retirarsinfac='" + Entregasinfac + "' "
                        + ",idciudad=" + Ciudad + ",emailcartera='" + EmailCartera
                        + "',reteiva=" + ReteIva + ",reteica='" + ReteIca + "',retefuente=" + ReteFuente
                        + ",req_solicitud='" + Solicitud + "', aprobar_cotizacion='" + Cotizacion + "', aprobar_ajuste='" + Ajuste + "'"
                        + ",asesor=" + Asesor + ",habitual='" + Habitual + "',idiva=" + iva + ",firmacertificado='" + FirmaCertificado
                        + "', paginformetecnico='" + InformeTecnico + "', idcuecontable=" + CuentaContable
                        + ", enviar_certificado = '" + EnviarCertificado + "' WHERE id=" + IdCliente;
                if (Globales.DatosAuditoria(sql, "CLIENTE", "ACTUALIZAR CLIENTE", idusuario, iplocal, this.getClass() + "-->GuardarCliente")) {
                    sql = "UPDATE clientes_sede SET idciudad = " + Ciudad + ", direccion='" + Direccion + "' "
                            + "WHERE idcliente = " + IdCliente + " and nombre = 'SEDE PRINCIPAL'";
                    Globales.DatosAuditoria(sql, "CLIENTE", "ACTUALIZAR SEDE", idusuario, iplocal, this.getClass() + "-->GuardarCliente");
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
        return "0|" + String.valueOf(IdCliente);
    }

    public String CambioNit(HttpServletRequest request) {
        String documento = request.getParameter("documento");
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        String Persona = "";
        try {
            if (tipo == 1) {
                if (Globales.PermisosSistemas("CLIENTE CAMBIO NIT", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE clientes set documento ='" + documento.trim() + "' WHERE id = " + id;
                Persona = "CLIENTE";
            } else {
                if (Globales.PermisosSistemas("PROVEEDOR CAMBIO NIT", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE proveedores set documento ='" + documento.trim() + "' WHERE id = " + id;
                Persona = "PROVEEDOR";
            }

            if (Globales.DatosAuditoria(sql, Persona, "CAMBIO NIT", idusuario, iplocal, this.getClass() + "-->CambioNit")) {
                return "0|Cambio de NIT/Cédula realizado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String BuscarCliente(HttpServletRequest request) {
        String documento = request.getParameter("documento");
        String Busqueda = "WHERE documento = '" + documento.trim() + "' ";
        sql = "select c.*, ci.iddepartamento, d.idpais "
                + "  from Clientes c inner join ciudad ci on c.idciudad = ci.id "
                + "                  inner join departamento d on d.id = ci.iddepartamento " + Busqueda;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarCliente");
    }

    public String ConsultarClientes(HttpServletRequest request) {
        String documento = request.getParameter("documento");
        String nombrecomp = request.getParameter("nombrecomp");
        String nombrecomer = request.getParameter("nombrecomer");
        String estado = request.getParameter("estado");
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int pais = Globales.Validarintnonull(request.getParameter("pais"));
        int departamento = Globales.Validarintnonull(request.getParameter("departamento"));
        int ciudad = Globales.Validarintnonull(request.getParameter("ciudad"));
        int asesor = Globales.Validarintnonull(request.getParameter("asesor"));
        String habitual = request.getParameter("habitual");
        String aprobar_cotizacion = request.getParameter("aprobar_cotizacion");
        String aprobar_ajuste = request.getParameter("aprobar_ajuste");

        String Busqueda = "WHERE documento ilike '%" + documento + "%' and nombrecompleto ilike '%" + nombrecomp + "%' and nombrecomer ilike '%" + nombrecomer + "%'";
        if (asesor > 0) {
            Busqueda += " AND c.asesor = " + asesor;
        }
        if (pais > 0) {
            Busqueda += " AND p.id = " + pais;
        }
        if (departamento > 0) {
            Busqueda += " AND d.id = " + departamento;
        }
        if (ciudad > 0) {
            Busqueda += " AND ci.id = " + ciudad;
        }
        if (tipo > 0) {
            Busqueda += " AND c.idtipcli = " + tipo;
        }
        if (!habitual.equals("T")) {
            Busqueda += " AND c.habitual = '" + habitual + "'";
        }
        if (!aprobar_cotizacion.equals("T")) {
            Busqueda += " AND c.aprobar_cotizacion = '" + aprobar_cotizacion + "'";
        }
        if (!aprobar_ajuste.equals("T")) {
            Busqueda += " AND c.aprobar_ajuste = '" + aprobar_ajuste + "'";
        }
        if (!estado.equals("Todos")) {
            Busqueda += " AND c.estado = " + estado;
        }

        sql = "select documento, nombrecompleto, nombrecomer, direccion, telefono, celular, email, estado, habitual || '<br><b>' || aprobar_cotizacion || '</b><br>' || aprobar_ajuste as habitual, "
                + "  tc.descripcion as tipocliente, ci.descripcion as ciudad, d.descripcion as departamento, p.descripcion as pais, cu.cuenta || ' ' || cu.nombre as cuenta "
                + "  from clientes c inner join  tipocliente tc on c.idtipcli = tc.id "
                + "                  inner join ciudad ci on ci.id = c.idciudad "
                + "                  inner join departamento d on d.id = ci.iddepartamento "
                + "                  inner join contabilidad.cuenta cu on cu.id = c.idcuecontable "
                + "                  inner join pais p on p.id = d.idpais " + Busqueda
                + "  order by 2";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarClientes");
    }

    public String DocumentosCliente(HttpServletRequest request) {
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        int Documento = Globales.Validarintnonull(request.getParameter("Documento"));

        sql = "SELECT to_char(fecha,'yyyy/MM/dd HH24:MI') as fecha, u.nombrecompleto as usuario "
                + "  FROM clientes_documentos cd INNER JOIN seguridad.rbac_usuario u on cd.idusuario = u.idusu "
                + "  where idcliente = " + Cliente + " and iddocumento = " + Documento;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DocumentosCliente");
    }

    public String GuardarDocumentosCliente(HttpServletRequest request) {
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        int Documento = Globales.Validarintnonull(request.getParameter("Documento"));
        try {
            if (Globales.PermisosSistemas("CLIENTES GUARDAR DOCUMENTOS", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "SELECT id "
                    + " FROM clientes_documentos "
                    + " where idcliente = " + Cliente + " and iddocumento = " + Documento;
            int id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

            if (archivoadjunto == null) {
                return "1|Error al subir el archivo";
            }

            File directorio = new File(Globales.ruta_archivo + "DocumentosClientes/" + Cliente);
            if (!directorio.exists()) {
                directorio.mkdirs();
            }

            File filename = new File(Globales.ruta_archivo + "uploads/" + archivoadjunto);
            File destino = new File(Globales.ruta_archivo + "DocumentosClientes/" + Cliente + "/" + Documento + "." + Globales.ExtensionArchivo(archivoadjunto));
            if (destino.exists()) {
                destino.delete();
            }

            Files.copy(filename.toPath(), destino.toPath());
            archivoadjunto = null;
            if (id == 0) {
                sql = "INSERT INTO clientes_documentos(idcliente, iddocumento, idusuario) "
                        + "  VALUES (" + Cliente + "," + Documento + "," + idusuario + ");";
                if (Globales.DatosAuditoria(sql, "CLIENTES", "AGREGAR DOCUMENTO", idusuario, iplocal, this.getClass() + "-->GuardarDocumentosCliente")) {
                    return "0|Documento agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                sql = "UPDATE clientes_documentos "
                        + "  SET idusuario=" + idusuario + ",fecha=now() "
                        + "  WHERE id=" + id;
                if (Globales.DatosAuditoria(sql, "CLIENTES", "ACTUALIZAR DOCUMENTO", idusuario, iplocal, this.getClass() + "-->GuardarDocumentosCliente")) {
                    return "0|Documento actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String BuscarSede(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        sql = "SELECT cs.*, c.descripcion as ciudad, d.descripcion as departamento, c.iddepartamento "
                + "FROM clientes_sede cs left join ciudad c on cs.idciudad = c.id "
                + "                      left join departamento d on d.id = c.iddepartamento  "
                + "where cs.id = " + id;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarSede");
    }

    public String BuscarContacto(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        sql = "SELECT * FROM clientes_contac where id = " + id;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarContacto");
    }

    public String GuardarSede(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String nombre = request.getParameter("nombre");
        String direccion = request.getParameter("direccion");
        int ciudad = Globales.Validarintnonull(request.getParameter("ciudad"));

        try {

            sql = "SELECT id FROM clientes_sede where idcliente = " + cliente + " and nombre ='" + nombre + "' and id <> " + id;
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (encontrado > 0) {
                return "1|Ya existe una sede con este nombre registrada a este cliente";
            }
            if (id == 0) {
                if (Globales.PermisosSistemas("SEDE AGREGAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO clientes_sede (nombre, idciudad, idcliente, direccion) "
                        + " VALUES ('" + nombre.toUpperCase() + "'," + ciudad + "," + cliente + ",'" + direccion + "');";
                if (Globales.DatosAuditoria(sql, "SEDE", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarSede")) {
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("select max(id) from clientes_sede"));
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("SEDE EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE clientes_sede SET idciudad = " + ciudad + ", direccion='" + direccion + "', nombre = '" + nombre.toUpperCase() + "' "
                        + "WHERE id = " + id;
                if (!Globales.DatosAuditoria(sql, "SEDE", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarSede")) {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
            return "0|";
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarContacto(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String nombres = request.getParameter("nombres");
        String correo = request.getParameter("correo");
        String telefono = request.getParameter("telefono");
        String celular = request.getParameter("celular");

        try {
            sql = "SELECT id FROM clientes_contac where idcliente = " + cliente + " and nombres ='" + nombres + "' and id <> " + id;
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (encontrado > 0) {
                return "1|Ya existe una contacto con este nombre registrado a este cliente";
            }
            if (id == 0) {
                if (Globales.PermisosSistemas("CONTACTO AGREGAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO clientes_contac(idcliente, nombres, email, telefonos, fax) "
                        + " VALUES (" + cliente + ",'" + nombres.toUpperCase() + "','" + correo + "','" + telefono + "','" + celular + "');";
                if (Globales.DatosAuditoria(sql, "CONTACTO", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarContacto")) {
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("select max(id) from clientes_contac"));
                    sql = "INSERT INTO seguridad.rbac_permiso_pagina(idcontacto, cotizacion, ingreso, estado_cuenta, solicitudes, "
                            + " certificado, inventario, queja) "
                            + " VALUES (" + id + ",1,1,1,1,1,1,1)";
                    Globales.DatosAuditoria(sql, "CONTACTO", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarContacto");
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("CONTACTO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE clientes_contac SET nombres = '" + nombres.toUpperCase() + "', email='" + correo + "', telefonos = '" + telefono + "', fax='" + celular + "'"
                        + " WHERE id = " + id;
                if (!Globales.DatosAuditoria(sql, "CONTACTO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarContacto")) {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
            return "0|";
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String BuscarPermisosContac(HttpServletRequest request) {
        int contacto = Globales.Validarintnonull(request.getParameter("contacto"));
        sql = "SELECT *  FROM seguridad.rbac_permiso_pagina where idcontacto = " + contacto;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarPermisosContac");
    }

    public String AsignarPermisos(HttpServletRequest request) {

        int IdContacPermiso = Globales.Validarintnonull(request.getParameter("IdContacPermiso"));
        String PermCotizacion = request.getParameter("PermCotizacion");
        String PermIngreso = request.getParameter("PermIngreso");
        String PermEstadoCuenta = request.getParameter("PermEstadoCuenta");
        String PermSolicitudes = request.getParameter("PermSolicitudes");
        String PermCertificados = request.getParameter("PermCertificados");
        String PermInventarios = request.getParameter("PermInventarios");
        String PermQuejas = request.getParameter("PermQuejas");

        int cotizacion = PermCotizacion == null ? 0 : 1;
        int ingreso = PermIngreso == null ? 0 : 1;
        int estadocuenta = PermEstadoCuenta == null ? 0 : 1;
        int solicitudes = PermSolicitudes == null ? 0 : 1;
        int certificados = PermCertificados == null ? 0 : 1;
        int inventarios = PermInventarios == null ? 0 : 1;
        int quejas = PermQuejas == null ? 0 : 1;

        try {
            int id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id from seguridad.rbac_permiso_pagina where idcontacto =" + IdContacPermiso));
            if (id == 0) {
                if (Globales.PermisosSistemas("CLIENTE ASIGNAR PERMISO", idusuario) == 0) {
                    return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO seguridad.rbac_permiso_pagina(idcontacto, cotizacion, ingreso, estado_cuenta, solicitudes, "
                        + " certificado, inventario, queja) "
                        + "  VALUES (" + IdContacPermiso + "," + cotizacion + "," + ingreso + "," + estadocuenta
                        + "," + solicitudes + "," + certificados + "," + inventarios + "," + quejas + ")";
                if (Globales.DatosAuditoria(sql, "CLIENTE", "AGREGAR PERMISOS", idusuario, iplocal, this.getClass() + "-->AsignarPermisos")) {
                    return "0|Permiso agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("CLIENTE EDITAR PERMISO", idusuario) == 0) {
                    return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "update seguridad.rbac_permiso_pagina "
                        + "      set cotizacion = " + cotizacion + ",ingreso=" + ingreso + ",estado_cuenta=" + estadocuenta
                        + ",solicitudes=" + solicitudes + ",certificado=" + certificados + ",inventario=" + inventarios
                        + ",queja=" + quejas + " WHERE id = " + id;
                if (Globales.DatosAuditoria(sql, "CLIENTE", "ACTUALIZAR PERMISOS", idusuario, iplocal, this.getClass() + "-->AsignarPermisos")) {
                    return "0|Permiso actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarContacto(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int IdReemplazo = Globales.Validarintnonull(request.getParameter("IdReemplazo"));
        String Mensaje = "";
        try {
            if (Globales.PermisosSistemas("CONTACTO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM clientes_contac WHERE id = " + Id;
            if (Globales.DatosAuditoria(sql, "CLIENTE", "CONTACTO ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarContacto")) {
                return "0|Equipo eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            if (IdReemplazo > 0) {
                try {
                    sql = "UPDATE cotizacion SET idcontacto = " + IdReemplazo + " WHERE idcontacto = " + Id + ";";
                    sql += "UPDATE cotizacion_detalle SET idcontacto = " + IdReemplazo + " WHERE idcontacto = " + Id + ";";
                    sql += "UPDATE remision SET idcontacto = " + IdReemplazo + " WHERE idcontacto = " + Id + ";";
                    sql += "UPDATE web.solicitud SET idcontacto = " + IdReemplazo + " WHERE idcontacto = " + Id + ";";
                    sql += "DELETE FROM clientes_contac WHERE id = " + Id + ";";
                    if (Globales.DatosAuditoria(sql, "CONTACTO", "REEMPLAZAR Y ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarContacto")) {
                        return "0|Equipo reemplazado y eliminado con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }
                } catch (Exception ex2) {
                    return "1|" + ex2.getMessage();
                }
            }
            return "1|" + ex.getMessage();
        }
    }

    public String BuscarProveedor(HttpServletRequest request) {
        String documento = request.getParameter("documento");
        sql = "select po.*, d.id as iddepartamento, d.idpais, riva.porcentaje as porreteiva, rica.porcentaje as porreteica, "
                + "      rfuente.porcentaje as porretefuente "
                + "  from proveedores po inner join ciudad c on c.id = po.idciudad "
                + "                      inner join departamento d on d.id = c.iddepartamento "
                + "                      inner join contabilidad.retenciones riva on riva.id = reteiva "
                + "                      inner join contabilidad.retenciones rica on rica.id = reteica "
                + "                      inner join contabilidad.retenciones rfuente on rfuente.id = retefuente "
                + "  WHERE documento = '" + documento.trim() + "'";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarProveedor");
    }

    public String GuardarProveedor(HttpServletRequest request) {
        int IdProveedor = Globales.Validarintnonull(request.getParameter("IdProveedor"));
        String Documento = request.getParameter("Documento");
        String Nombres = request.getParameter("Nombres");
        String Comercial = request.getParameter("Comercial");
        String Telefonos = request.getParameter("Telefonos");
        String Celular = request.getParameter("Celular");
        String Email = request.getParameter("Email");
        int Ciudad = Globales.Validarintnonull(request.getParameter("Ciudad"));
        String Direccion = request.getParameter("Direccion");
        String Contacto = request.getParameter("Contacto");
        String Estado = request.getParameter("Estado");
        String FormaPago = request.getParameter("FormaPago");
        int PlazoPago = Globales.Validarintnonull(request.getParameter("PlazoPago"));
        int ReteIva = Globales.Validarintnonull(request.getParameter("ReteIva"));
        int ReteIca = Globales.Validarintnonull(request.getParameter("ReteIca"));
        int ReteFuente = Globales.Validarintnonull(request.getParameter("ReteFuente"));
        int CuentaContable = Globales.Validarintnonull(request.getParameter("CuentaContable"));

        try {
            if (IdProveedor == 0) {
                if (Globales.PermisosSistemas("PROVEEDOR AGREGAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO proveedores(documento, nombrecompleto, nombrecom, telefono, celular, email, direccion, contacto, idciudad, estado, formapago, plazopago, reteiva, reteica, retefuente,idcuecontable) "
                        + "  VALUES ('" + Documento.trim() + "','" + Nombres.trim().toUpperCase() + "','" + Comercial + "','" + Telefonos
                        + "','" + Celular + "','" + Email + "','" + Direccion + "','" + Contacto + "'," + Ciudad + "," + Estado + ",'" + FormaPago + "'," + PlazoPago + "," + ReteIva + "," + ReteIca + "," + ReteFuente + "," + CuentaContable + ");";
                if (Globales.DatosAuditoria(sql, "PROVEEDOR", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarProveedor")) {
                    IdProveedor = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT MAX(id) FROM proveedores"));
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("PROVEEDOR EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE proveedores "
                        + "      SET documento='" + Documento + "', nombrecompleto='" + Nombres.toUpperCase()
                        + "', telefono='" + Telefonos + "', celular='" + Celular + "', email='" + Email + "',"
                        + "estado=" + Estado + ", direccion  = '" + Direccion + "', contacto = '" + Contacto + "'"
                        + ",nombrecom ='" + Comercial.toUpperCase() + "',idciudad=" + Ciudad + ",formapago='" + FormaPago + "', plazopago=" + PlazoPago
                        + ", reteiva=" + ReteIva + ",reteica=" + ReteIca + ",retefuente=" + ReteFuente + ", idcuecontable = " + CuentaContable + " WHERE id=" + IdProveedor;
                if (!Globales.DatosAuditoria(sql, "PROVEEDOR", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarProveedor")) {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

        return "0|" + IdProveedor;

    }

    public String ConsultarProveedores(HttpServletRequest request) {
        String documento = request.getParameter("documento");
        String nombrecomp = request.getParameter("nombrecomp");
        String nombrecomer = request.getParameter("nombrecomer");
        int estado = Globales.Validarintnonull(request.getParameter("estado"));
        int pais = Globales.Validarintnonull(request.getParameter("pais"));

        String busqueda = " WHERE nombrecompleto ilike '%" + nombrecomp + "%' and nombrecom ilike '%" + nombrecomer + "%'";

        if (!documento.equals("")) {
            busqueda = " WHERE documento = '" + documento + "'";
        } else {
            if (estado != -1) {
                busqueda += " AND po.estado = " + estado;
            }
            if (pais > 0) {
                busqueda += " AND p.id = " + pais;
            }
        }

        sql = "select documento, nombrecompleto, nombrecom, direccion, telefono, celular, email, case when estado = 1 then 'Activo' else 'Inactivo' end as estado, "
                + "  contacto, c.descripcion as ciudad, d.descripcion as departamento, p.id as pais, cu.cuenta "
                + "  from proveedores po inner join ciudad c on c.id = po.idciudad  "
                + "                      inner join departamento d on d.id = c.iddepartamento "
                + "                      inner join contabilidad.cuenta cu on cu.id = po.idcuecontable "
                + "                      inner join pais p on p.id = d.idpais " + busqueda + " order by 2";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarProveedores");
    }

    public String DescripcionPrecios() {
        sql = "SELECT id, descripcion from tablaprecios order by id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DescripcionPrecios");
    }

    public String TablaPrecios(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String codigo = request.getParameter("codigo");
        int servicio = Globales.Validarintnonull(request.getParameter("servicio"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));

        if (id == 0) {
            String busqueda = "WHERE 1 = 1";
            if (!codigo.equals("")) {
                busqueda += " AND m.abreviatura || '-' || trim(to_char(sp.contador,'0000')) = '" + codigo + "'";
            }
            if (servicio > 0) {
                busqueda += " AND sp.idservicio = " + servicio;
            }
            if (magnitud > 0) {
                busqueda += " AND e.idmagnitud = " + magnitud;
            }
            if (equipo > 0) {
                busqueda += " AND sp.idequipo = " + equipo;
            }
            if (marca > 0) {
                busqueda += " AND sp.idmarca = " + marca;
            }
            if (!modelo.equals("")) {
                busqueda += " AND mo.descripcion = '" + modelo + "'";
            }
            if (proveedor > 0) {
                busqueda += " AND sp.idproveedor = " + proveedor;
            }

            sql = "SELECT sp.id, e.descripcion as equipo, m.descripcion as magnitud, me.descripcion as medida, sp.desde, sp.hasta, "
                    + "              ma.descripcion as marca, mo.descripcion as modelo, p.nombrecompleto as proveedor, costo, precio1, "
                    + "              precio2, precio3, precio4, precio5, precio6, precio7, precio8, s.nombre as servicio, "
                    + "              precio9, precio10,  m.abreviatura || '-' || trim(to_char(sp.contador,'0000')) as contador, fotos, "
                    + "              '<button class=''btn btn-glow btn-danger'' title=''Eliminar Precio'' type=''button'' onclick=' || chr(34) || 'EliminarPrecio(' || sp.id || ',''' || m.abreviatura || '-' || trim(to_char(sp.contador,'0000')) || ''',''' || m.descripcion || ''',''' || e.descripcion || ''',''' || ma.descripcion || ''',''' || mo.descripcion || ''',''' || p.nombrecompleto || ''')' ||  chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar, "
                    + "              case when sp.fotos > 0 then '<img src=''" + url_archivo + "Adjunto/imagenes/precios/' || sp.id || '/1.jpg'' onclick=''LlamarFotoPrecio(' || sp.id || ',' || sp.fotos || ')'' width=''120px''/>' else '' end  as imagen "
                    + " FROM servicio_precios sp INNER JOIN equipo e on e.id = sp.idequipo "
                    + "	                                INNER JOIN marcas ma on ma.id = sp.idmarca "
                    + "                          INNER JOIN medidas me on me.id = sp.idmedida "
                    + "		                                INNER JOIN modelos mo on mo.id = sp.idmodelo "
                    + "		                                INNER JOIN proveedores p on p.id = sp.idproveedor "
                    + "		                                INNER JOIN servicio s on s.id = idservicio "
                    + "		                                INNER JOIN magnitudes m on m.id = sp.idmagnitud " + busqueda;
        } else {
            sql = "select sp.*, m.abreviatura || '-' || trim(to_char(sp.contador,'0000')) as ncontador "
                    + "  from servicio_precios sp INNER JOIN magnitudes m on m.id = sp.idmagnitud where sp.id = " + id;
        }
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaPrecios");
    }

    public String EliminarFotoPrecio(HttpServletRequest request) {
        int Precio = Globales.Validarintnonull(request.getParameter("Precio"));
        int Numero = Globales.Validarintnonull(request.getParameter("Numero"));

        if (Globales.PermisosSistemas("TABLA PRECIOS ELIMINAR FOTOS", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        try {

            File archivo = new File(Globales.ruta_archivo + "Adjunto/imagenes/Precios/" + Precio + "/" + Numero + ".jpg");
            if (archivo.exists()) {
                archivo.delete();
            }

            if (Numero == 1) {
                sql = "UPDATE servicio_precios set foto = 0 WHERE id = " + Precio;
                Globales.DatosAuditoria(sql, "TABLA DE PRECIOS", "ELIMINAR FOTO", idusuario, iplocal, this.getClass() + "-->EliminarFotoPrecio");
            }

            return "0|Foto número " + Numero + " eliminada";

        } catch (Exception ex) {

            return "1|" + ex.getMessage();
        }
    }

    public String EliminarPrecio(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        try {
            if (Globales.PermisosSistemas("PRECIOS ELIMINAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "delete from servicio_precios where id = " + Id;

            if (Globales.DatosAuditoria(sql, "PRECIOS", "EDITAR", idusuario, iplocal, this.getClass() + "-->GenerarSolicitud")) {
                return "0|Precio eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarMagnitudes(HttpServletRequest request) {
        int Mag_Id = Globales.Validarintnonull(request.getParameter("Mag_Id"));
        String Mag_Descripcion = request.getParameter("Mag_Descripcion");
        int Mag_Estado = Globales.Validarintnonull(request.getParameter("Mag_Estado"));
        int Mag_CodInicial = Globales.Validarintnonull(request.getParameter("Mag_CodInicial"));
        int Mag_Precio = Globales.Validarintnonull(request.getParameter("Mag_Precio"));
        String Mag_Inicial = request.getParameter("Mag_Inicial");
        String Mensaje = "";

        try {
            if (Mag_Id == 0) {
                if (Globales.PermisosSistemas("MAGNITUDES GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO public.magnitudes(descripcion, estado, codigoini, tipoprecio, abreviatura) "
                        + "VALUES ('" + Mag_Descripcion.trim() + "'," + Mag_Estado + "," + Mag_CodInicial + "," + Mag_Precio + ",'" + Mag_Inicial + "')";
                if (Globales.DatosAuditoria(sql, "MAGNITUDES", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarMagnitudes")) {
                    Mensaje = "0|Magnitud agregada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("MAGNITUDES EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE public.magnitudes "
                        + " SET descripcion='" + Mag_Descripcion.trim() + "', estado=" + Mag_Estado + ",codigoini=" + Mag_CodInicial + ",tipoprecio=" + Mag_Precio
                        + ", abreviatura='" + Mag_Inicial + "' WHERE id=" + Mag_Id;
                if (Globales.DatosAuditoria(sql, "MAGNITUDES", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarMagnitudes")) {
                    Mensaje = "0|Magnitud actualizada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    private int MaximoCodigo(int magnitud) {
        sql = "SELECT max(codigo) FROM equipo where idmagnitud = " + magnitud;
        int codigo = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (codigo == 0) {
            sql = "select codigoini from magnitudes where id = " + magnitud;
            int inicial = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            codigo = inicial;
        } else {
            codigo = codigo + 1;
        }
        return codigo;
    }

    public int MaximoServicio() {
        sql = "SELECT max(codigo) FROM servicio";
        return Globales.Validarintnonull(Globales.ObtenerUnValor(sql)) + 1;
    }

    public String GuardarEquipos(HttpServletRequest request) {
        int Equ_Magnitud = Globales.Validarintnonull(request.getParameter("Equ_Magnitud"));
        int Equ_Id = Globales.Validarintnonull(request.getParameter("Equ_Id"));
        int Equ_Codigo = Globales.Validarintnonull(request.getParameter("Equ_Codigo"));
        String Equ_Descripcion = request.getParameter("Equ_Descripcion");
        int Equ_Estado = Globales.Validarintnonull(request.getParameter("Equ_Estado"));
        String Mensaje = "";
        try {
            if (Equ_Id == 0) {
                Equ_Codigo = MaximoCodigo(Equ_Magnitud);
                if (Globales.PermisosSistemas("EQUIPOS GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO equipo(descripcion, estado, idmagnitud, codigo) "
                        + " VALUES ('" + Equ_Descripcion.trim() + "'," + Equ_Estado + "," + Equ_Magnitud + "," + Equ_Codigo + ")";
                if (Globales.DatosAuditoria(sql, "EQUIPOS", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarEquipos")) {
                    Mensaje = "0|Equipo agregada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("EQUIPOS EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                int idmagant = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT idmagnitud from equipo where id = " + Equ_Id));
                if (idmagant != Equ_Magnitud) {
                    Equ_Codigo = MaximoCodigo(Equ_Magnitud);
                }

                sql = "UPDATE equipo "
                        + " SET descripcion='" + Equ_Descripcion.trim() + "', estado=" + Equ_Estado + ",codigo=" + Equ_Codigo + ",idmagnitud=" + Equ_Magnitud
                        + "WHERE id=" + Equ_Id;
                if (Globales.DatosAuditoria(sql, "EQUIPOS", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarEquipos")) {
                    Mensaje = "0|Equipo actualizada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarServicio(HttpServletRequest request) {
        int Ser_Id = Globales.Validarintnonull(request.getParameter("Ser_Id"));
        int Ser_Codigo = Globales.Validarintnonull(request.getParameter("Ser_Codigo"));
        String Ser_Descripcion = request.getParameter("Ser_Descripcion");
        int Ser_Acreditado = Globales.Validarintnonull(request.getParameter("Ser_Acreditado"));
        int Ser_Proveedor = Globales.Validarintnonull(request.getParameter("Ser_Proveedor"));
        int Ser_Sitio = Globales.Validarintnonull(request.getParameter("Ser_Sitio"));
        int Ser_Express = Globales.Validarintnonull(request.getParameter("Ser_Express"));
        int Ser_IVA = Globales.Validarintnonull(request.getParameter("Ser_IVA"));
        int Ser_Otro = Globales.Validarintnonull(request.getParameter("Ser_Otro"));
        int Ser_Estado = Globales.Validarintnonull(request.getParameter("Ser_Estado"));
        int Ser_Descuento = Globales.Validarintnonull(request.getParameter("Ser_Descuento"));
        int Ser_Dias = Globales.Validarintnonull(request.getParameter("Ser_Dias"));
        int Ser_CuentaContable = Globales.Validarintnonull(request.getParameter("Ser_CuentaContable"));

        String Mensaje = "";
        try {
            if (Ser_Id == 0) {
                Ser_Codigo = MaximoServicio();
                if (Globales.PermisosSistemas("SERVICIO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO servicio(codigo, nombre, estado, proveedor, express, sitio, diaentrega, "
                        + "                      idiva, otro, descuento, acreditadoser, idcuecontable) "
                        + " VALUES (" + Ser_Codigo + ",'" + Ser_Descripcion + "'," + Ser_Estado + "," + Ser_Proveedor
                        + "," + Ser_Express + "," + Ser_Sitio + "," + Ser_Dias + "," + Ser_IVA + "," + Ser_Otro + "," + Ser_Descuento + "," + Ser_Acreditado + "," + Ser_CuentaContable + ")";
                if (Globales.DatosAuditoria(sql, "SERVICIO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarServicio")) {
                    Mensaje = "0|Servicio agregada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("SERVICIO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE servicio "
                        + " SET nombre='" + Ser_Descripcion + "', estado=" + Ser_Estado + ",proveedor=" + Ser_Proveedor + ",express=" + Ser_Express + ", idcuecontable=" + Ser_CuentaContable
                        + ",sitio=" + Ser_Sitio + ",idiva=" + Ser_IVA + ",otro=" + Ser_Otro + ",descuento=" + Ser_Descuento + ", acreditadoser=" + Ser_Acreditado + ",diaentrega=" + Ser_Dias
                        + " WHERE id=" + Ser_Id;
                if (Globales.DatosAuditoria(sql, "SERVICIO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarServicio")) {
                    Mensaje = "0|Servicio actualizada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarServicio(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        String Mensaje = "";
        try {
            if (Globales.PermisosSistemas("SERVICIO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM servicio WHERE id = " + Id;
            Mensaje = "0|Servicio eliminado con éxito";
            if (Globales.DatosAuditoria(sql, "SERVICIO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarServicio")) {
                return Mensaje;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarOtroServicio(HttpServletRequest request) {
        int OSer_Id = Globales.Validarintnonull(request.getParameter("OSer_Id"));
        String OSer_Iniciales = request.getParameter("OSer_Iniciales");
        String OSer_Descripcion = request.getParameter("OSer_Descripcion");
        double OSer_Precio = Globales.ValidardoublenoNull(request.getParameter("OSer_Precio"));
        double OSer_Descuento = Globales.ValidardoublenoNull(request.getParameter("OSer_Descuento"));
        int OSer_IVA = Globales.Validarintnonull(request.getParameter("OSer_IVA"));
        int OSer_Estado = Globales.Validarintnonull(request.getParameter("OSer_Estado"));
        int OSer_CuentaContable = Globales.Validarintnonull(request.getParameter("OSer_CuentaContable"));

        String Mensaje = "";
        try {
            if (OSer_Id == 0) {
                if (Globales.PermisosSistemas("OTRO SERVICIO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO otros_servicios(descripcion, porcentaje, precio, idiva, estado, inicial, idcuecontable) "
                        + " VALUES ('" + OSer_Descripcion + "'," + OSer_Descuento + "," + OSer_Precio + "," + OSer_IVA
                        + "," + OSer_Estado + ",'" + OSer_Iniciales + "'," + OSer_CuentaContable + ")";
                if (Globales.DatosAuditoria(sql, "OTRO SERVICIO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarOtroServicio")) {
                    Mensaje = "0|Otro Servicio agregada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                if (Globales.PermisosSistemas("OTRO SERVICIO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE otros_servicios "
                        + " SET descripcion='" + OSer_Descripcion + "',porcentaje=" + OSer_Descuento + ",precio=" + OSer_Precio
                        + ",idiva=" + OSer_IVA + ",estado=" + OSer_Estado + ",inicial='" + OSer_Iniciales + "', idcuecontable = " + OSer_CuentaContable
                        + " WHERE id=" + OSer_Id;
                if (Globales.DatosAuditoria(sql, "OTRO SERVICIO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarOtroServicio")) {
                    Mensaje = "0|Otro Servicio actualizada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarOtroServicio(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        String Mensaje = "";
        try {
            if (Globales.PermisosSistemas("OTRO SERVICIO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM otros_servicios WHERE id = " + Id;
            if (Globales.DatosAuditoria(sql, "OTRO SERVICIO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarOtroServicio")) {
                Mensaje = "0|Otro Servicio eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarMedidas(HttpServletRequest request) {
        int Med_Id = Globales.Validarintnonull(request.getParameter("Med_Id"));
        String Med_Descripcion = request.getParameter("Med_Descripcion");
        String Mensaje = "";
        try {
            if (Med_Id == 0) {
                if (Globales.PermisosSistemas("MEDIDA GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO medidas(descripcion) "
                        + " VALUES ('" + Med_Descripcion.trim() + "')";
                if (Globales.DatosAuditoria(sql, "MEDIDA", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarMedidas")) {
                    Mensaje = "0|Medida agregada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                if (Globales.PermisosSistemas("MEDIDA EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE medidas "
                        + " SET descripcion='" + Med_Descripcion.trim() + "' "
                        + "WHERE id=" + Med_Id;
                if (Globales.DatosAuditoria(sql, "MEDIDA", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarMedidas")) {
                    Mensaje = "0|Medida actualizada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarMedida(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        try {
            if (Globales.PermisosSistemas("MEDIDA ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM medidas WHERE id = " + Id;
            if (Globales.DatosAuditoria(sql, "MEDIDA", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarMedida")) {
                return "0|Medida eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarIntervalos(HttpServletRequest request) {
        int Int_Id = Globales.Validarintnonull(request.getParameter("Int_Id"));
        String Int_Medida = request.getParameter("Int_Medida");
        String Int_Hasta = request.getParameter("Int_Hasta");
        String Int_Desde = request.getParameter("Int_Desde");
        int Int_Estado = Globales.Validarintnonull(request.getParameter("Int_Estado"));
        int Int_Magnitud = Globales.Validarintnonull(request.getParameter("Int_Magnitud"));

        try {
            if (Int_Id == 0) {
                if (Globales.PermisosSistemas("INTERVALO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO magnitud_intervalos(idmagnitud, medida, desde, hasta, estado) "
                        + " VALUES (" + Int_Magnitud + ",'" + Int_Medida + "','" + Int_Desde + "','" + Int_Hasta + "'," + Int_Estado + ")";
                if (Globales.DatosAuditoria(sql, "INTERVALO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarIntervalos")) {
                    return "0|Intervalo agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("INTERVALO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE magnitud_intervalos "
                        + " SET idmagnitud = " + Int_Magnitud + ",medida='" + Int_Medida + "', desde='" + Int_Desde + "',hasta='" + Int_Hasta + "' "
                        + "WHERE id=" + Int_Id;
                if (Globales.DatosAuditoria(sql, "INTERVALO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarIntervalos")) {
                    return "0|Intervalo actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarIntervalo(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int IdReemplazo = Globales.Validarintnonull(request.getParameter("IdReemplazo"));

        try {
            if (Globales.PermisosSistemas("INTERVALO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM magnitud_intervalos WHERE id = " + Id;
            if (Globales.DatosAuditoria(sql, "INTERVALO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->GenerarSolicitud")) {
                return "0|Intervalo eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            if (IdReemplazo > 0) {
                try {
                    sql = "UPDATE remision_detalle SET idintervalo = " + IdReemplazo + " WHERE idintervalo = " + Id + ";";
                    sql += "UPDATE cotizacion_detalle SET idintervalo = " + IdReemplazo + " WHERE idintervalo = " + Id + ";";
                    sql += "UPDATE web.solicitud_detalle SET idintervalo = " + IdReemplazo + " WHERE idintervalo = " + Id + ";";
                    sql += "UPDATE servicio_precios SET idintervalo = " + IdReemplazo + " WHERE idintervalo = " + Id + ";";
                    sql += "DELETE FROM magnitud_intervalos WHERE id = " + Id + ";";
                    if (Globales.DatosAuditoria(sql, "INTERVALO", "REEMPLAZAR Y ELIMINAR", idusuario, iplocal, this.getClass() + "-->GenerarSolicitud")) {
                        return "0|Intervalo reemplazado y eliminado con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }
                } catch (Exception ex2) {
                    return "1|" + ex2.getMessage();
                }

            }
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarMarcas(HttpServletRequest request) {
        int Mar_Id = Globales.Validarintnonull(request.getParameter("Mar_Id"));
        String Mar_Descripcion = request.getParameter("Mar_Descripcion");
        int Mar_Estado = Globales.Validarintnonull(request.getParameter("Mar_Estado"));

        try {
            if (Mar_Id == 0) {
                if (Globales.PermisosSistemas("MARCA GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO marcas(descripcion, estado) "
                        + " VALUES ('" + Mar_Descripcion + "'," + Mar_Estado + ")";
                if (Globales.DatosAuditoria(sql, "MARCA", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarMarcas")) {
                    return "0|Marca agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("MARCA EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE marcas "
                        + " SET descripcion='" + Mar_Descripcion + "', estado=" + Mar_Estado
                        + " WHERE id=" + Mar_Id;
                if (Globales.DatosAuditoria(sql, "MARCA", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarMarcas")) {
                    return "0|Marca actualizada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarMarcas(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int IdReemplazo = Globales.Validarintnonull(request.getParameter("IdReemplazo"));

        try {
            if (Globales.PermisosSistemas("MARCA ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM marcas WHERE id = " + Id;
            if (Globales.DatosAuditoria(sql, "MARCA", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarMarcas")) {
                return "0|Intervalo eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            if (IdReemplazo > 0) {
                try {
                    sql = "UPDATE modelos SET idmarca = " + IdReemplazo + " WHERE idmarca = " + Id + ";";
                    sql += "UPDATE web.solicitud_detalle SET idmarca = " + IdReemplazo + " WHERE idmarca = " + Id + ";";
                    sql += "UPDATE servicio_precios SET idmarca = " + IdReemplazo + " WHERE idmarca = " + Id + ";";
                    sql += "DELETE FROM marcas WHERE id = " + Id + ";";
                    if (Globales.DatosAuditoria(sql, "MARCA", "REEMPLAZAR Y ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarMarcas")) {
                        return "0|Marca reemplazada y eliminada con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }
                } catch (Exception ex2) {
                    return "1|" + ex2.getMessage();
                }

            }
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarLeyenda(HttpServletRequest request) {
        int Ley_Id = Globales.Validarintnonull(request.getParameter("Ley_Id"));
        String[] Ley_Servicios = request.getParameterValues("Ley_Servicios");
        String[] Ley_EquiposCal = request.getParameterValues("Ley_EquiposCal");
        String[] Ley_Grupos = request.getParameterValues("Ley_Grupos");
        String[] Ley_EquiposInv = request.getParameterValues("Ley_EquiposInv");
        String Ley_Descripcion = request.getParameter("Ley_Descripcion").trim();
        int Ley_Estado = Globales.Validarintnonull(request.getParameter("Ley_Estado"));

        try {

            int id = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from leyenda_cotizacion where leyenda = '" + Ley_Descripcion + "' and id <> " + Ley_Id));
            if (id > 0) {
                return "1|La descripción <b>" + Ley_Descripcion + "</b> ya se encuentra registrada como leyenda de cotización";
            }

            String a_servicio = "";
            String a_grupo = "";
            String a_equipocal = "";
            String a_equipoinv = "";

            if (Ley_Servicios != null) {
                for (int x = 0; x < Ley_Servicios.length; x++) {
                    if (!a_servicio.equals("")) {
                        a_servicio += ",";
                    }
                    a_servicio += Ley_Servicios[x];
                }
            }

            if (Ley_EquiposCal != null) {
                for (int x = 0; x < Ley_EquiposCal.length; x++) {
                    if (!a_equipocal.equals("")) {
                        a_equipocal += ",";
                    }
                    a_equipocal += Ley_EquiposCal[x];
                }
            }

            if (Ley_EquiposInv != null) {
                for (int x = 0; x < Ley_EquiposInv.length; x++) {
                    if (!a_equipoinv.equals("")) {
                        a_equipoinv += ",";
                    }
                    a_equipoinv += Ley_EquiposInv[x];
                }
            }

            if (Ley_Grupos != null) {
                for (int x = 0; x < Ley_Grupos.length; x++) {
                    if (!a_grupo.equals("")) {
                        a_grupo += ",";
                    }
                    a_grupo += Ley_Grupos[x];
                }
            }

            if (Ley_Id == 0) {
                if (Globales.PermisosSistemas("LEYENDA COTIZACION GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO leyenda_cotizacion(leyenda, servicios, equipo_cal, grupos, equipo_inv, estado) "
                        + " VALUES ('" + Ley_Descripcion + "','" + a_servicio + "','" + a_equipocal + "','" + a_grupo + "','" + a_equipoinv + "'," + Ley_Estado + ")";
                if (Globales.DatosAuditoria(sql, "LEYENDA COTIZACION", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarLeyenda")) {
                    return "0|Leyenda de cotización agregada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("LEYENDA COTIZACION EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE leyenda_cotizacion "
                        + " SET leyenda = '" + Ley_Descripcion + "', servicios='" + a_servicio + "', equipo_cal='" + a_equipocal + "', grupos='" + a_grupo + "', equipo_inv='" + a_equipoinv + "', fecha=now() "
                        + ", estado=" + Ley_Estado
                        + " WHERE id=" + Ley_Id;
                if (Globales.DatosAuditoria(sql, "LEYENDA COTIZACION", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarLeyenda")) {
                    return "0|Leyenda de cotización actualizada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarLeyenda(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));

        try {
            if (Globales.PermisosSistemas("LEYENDA COTIZACION ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM leyenda_cotizacion WHERE id = " + Id;
            if (Globales.DatosAuditoria(sql, "LEYENDA COTIZACION", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarLeyenda")) {
                return "0|Leyenda de cotización eliminada con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarModelos(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int IdReemplazo = Globales.Validarintnonull(request.getParameter("IdReemplazo"));

        try {
            if (Globales.PermisosSistemas("MODELO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM modelos WHERE id = " + Id;
            if (Globales.DatosAuditoria(sql, "MODELO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarModelos")) {
                return "0|Intervalo eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            if (IdReemplazo > 0) {
                try {
                    sql = "UPDATE remision_detalle SET idmodelo = " + IdReemplazo + " WHERE idmodelo = " + Id + ";";
                    sql += "UPDATE cotizacion_detalle SET idmodelo = " + IdReemplazo + " WHERE idmodelo = " + Id + ";";
                    sql += "UPDATE web.solicitud_detalle SET idmodelo = " + IdReemplazo + " WHERE idmodelo = " + Id + ";";
                    sql += "UPDATE servicio_precios SET idmodelo = " + IdReemplazo + " WHERE idmodelo = " + Id + ";";
                    sql += "DELETE FROM modelos WHERE id = " + Id + ";";
                    if (Globales.DatosAuditoria(sql, "MODELO", "REEMPLAZAR Y ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarModelos")) {
                        return "0|Modelo reemplazado y eliminado con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }
                } catch (Exception ex2) {
                    return "1|" + ex2.getMessage();
                }

            }
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarModelos(HttpServletRequest request) {
        int Mod_Id = Globales.Validarintnonull(request.getParameter("Mod_Id"));
        String Mod_Descripcion = request.getParameter("Mod_Descripcion");
        int Mod_Marca = Globales.Validarintnonull(request.getParameter("Mod_Marca"));
        int Mod_Estado = Globales.Validarintnonull(request.getParameter("Mod_Estado"));

        try {
            if (Mod_Id == 0) {
                if (Globales.PermisosSistemas("MODELO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO modelos(idmarca, descripcion, estado) "
                        + " VALUES (" + Mod_Marca + ",'" + Mod_Descripcion + "'," + Mod_Estado + ")";
                if (Globales.DatosAuditoria(sql, "MODELO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarModelos")) {
                    return "0|Modelo agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("MODELO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE modelos "
                        + " SET idmarca=" + Mod_Marca + ",descripcion='" + Mod_Descripcion + "', estado=" + Mod_Estado
                        + " WHERE id=" + Mod_Id;
                if (Globales.DatosAuditoria(sql, "MODELO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarModelos")) {
                    return "0|Modelo actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarMetodo(HttpServletRequest request) {
        int Met_Id = Globales.Validarintnonull(request.getParameter("Met_Id"));
        String Met_Descripcion = request.getParameter("Met_Descripcion");
        String Met_Estado = request.getParameter("Met_Estado");
        String[] Met_Equipos = request.getParameterValues("Met_Equipos");
        String Mensaje = "";
        try {
            if (Met_Id == 0) {
                if (Globales.PermisosSistemas("METODO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO metodo(descripcion, estado) "
                        + " VALUES ('" + Met_Descripcion + "'," + Met_Estado + ");";
                if (Globales.DatosAuditoria(sql, "METODO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarMetodo")) {
                    Met_Id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT MAX(id) from metodo"));
                    Mensaje = "0|Método agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("METODO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE metodo "
                        + " SET descripcion='" + Met_Descripcion + "',estado=" + Met_Estado
                        + " WHERE id=" + Met_Id;
                if (Globales.DatosAuditoria(sql, "METODO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarMetodo")) {
                    Mensaje = "0|Método actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
            sql = "delete from metodo_equipo where idmetodo = " + Met_Id + ";";
            for (int x = 0; x < Met_Equipos.length; x++) {
                sql += "INSERT INTO metodo_equipo(idequipo, idmetodo) "
                        + "  VALUES (" + Met_Equipos[x] + "," + Met_Id + ");";
            }
            Globales.DatosAuditoria(sql, "METODO", "AGREGAR EQUIPO", idusuario, iplocal, this.getClass() + "-->GuardarMetodo");

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarMetodo(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int IdReemplazo = Globales.Validarintnonull(request.getParameter("IdReemplazo"));

        try {
            if (Globales.PermisosSistemas("METODO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "DELETE FROM metodo_equipo where idmetodo=" + Id + ";DELETE FROM metodo WHERE id = " + Id;
            if (Globales.DatosAuditoria(sql, "METODO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarMetodo")) {
                return "0|Método eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            if (IdReemplazo > 0) {
                try {
                    sql = "UPDATE cotizacion_detalle SET metodo = " + IdReemplazo + " WHERE metodo = " + Id + ";";
                    sql += "DELETE FROM metodo_equipo where idmetodo=" + Id + ";DELETE FROM metodo WHERE id = " + Id;
                    if (Globales.DatosAuditoria(sql, "METODO", "REEMPLAZAR Y ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarMetodo")) {
                        return "0|Metodo reemplazado y eliminado con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }
                } catch (Exception ex2) {
                    return "1|" + ex2.getMessage();
                }

            }
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarProcedimiento(HttpServletRequest request) {
        int Pro_Id = Globales.Validarintnonull(request.getParameter("Pro_Id"));
        String Pro_Descripcion = request.getParameter("Pro_Descripcion");
        String Pro_Estado = request.getParameter("Pro_Estado");
        String[] Pro_Magnitudes = request.getParameterValues("Pro_Magnitudes");
        String Mensaje;
        try {
            if (Pro_Id == 0) {
                if (Globales.PermisosSistemas("PROCEDIMIENTO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO procedimiento(descripcion, estado) "
                        + " VALUES ('" + Pro_Descripcion + "'," + Pro_Estado + ")";
                if (Globales.DatosAuditoria(sql, "PROCEDIMIENTO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarProcedimiento")) {
                    Mensaje = "0|Procedimiento agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("PROCEDIMIENTO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE procedimiento "
                        + " SET descripcion='" + Pro_Descripcion + "',estado=" + Pro_Estado
                        + " WHERE id=" + Pro_Id;
                if (Globales.DatosAuditoria(sql, "PROCEDIMIENTO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarProcedimiento")) {
                    Mensaje = "0|Procedimiento actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

            sql = "delete from procedimiento_magnitud where idprocedimiento = " + Pro_Id + ";";
            for (int x = 0; x < Pro_Magnitudes.length; x++) {
                sql += "INSERT INTO procedimiento_magnitud(idmagnitud, idprocedimiento) "
                        + " VALUES (" + Pro_Magnitudes[x] + "," + Pro_Id + ");";
            }
            Globales.DatosAuditoria(sql, "PROCEDIMIENTO", "AGREGAR MAGNITUD", idusuario, iplocal, this.getClass() + "-->GuardarProcedimiento");

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarPlantillas(HttpServletRequest request) {
        int Pla_Id = Globales.Validarintnonull(request.getParameter("Pla_Id"));
        String Pla_Codigo = request.getParameter("Pla_Codigo");
        String Pla_Descripcion = request.getParameter("Pla_Descripcion");
        String Pla_Estado = request.getParameter("Pla_Estado");
        String[] Pla_Equipos = request.getParameterValues("Pla_Equipos");
        int Pla_Acreditado = Globales.Validarintnonull(request.getParameter("Pla_Acreditado"));
        String Mensaje = "";
        try {
            if (Pla_Id == 0) {
                if (Globales.PermisosSistemas("PLANTILLA GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO plantillas(codigo, descripcion, estado, acreditado) "
                        + " VALUES ('" + Pla_Codigo + "','" + Pla_Descripcion + "'," + Pla_Estado + "," + Pla_Acreditado + ");";
                if (Globales.DatosAuditoria(sql, "PLANTILLA", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarPlantillas")) {
                    Pla_Id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT MAX(id) from plantillas"));
                    Mensaje = "0|Plantilla agregada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("PLANTILLA EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE plantillas "
                        + " SET descripcion='" + Pla_Descripcion + "',estado=" + Pla_Estado + ", acreditado=" + Pla_Acreditado + ",codigo='" + Pla_Codigo + "' "
                        + " WHERE id=" + Pla_Id;
                if (Globales.DatosAuditoria(sql, "PLANTILLA", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarPlantillas")) {
                    Mensaje = "0|Plantilla actualizada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

            sql = "delete from plantilla_equipo where idplantilla = " + Pla_Id + ";";
            for (int x = 0; x < Pla_Equipos.length; x++) {
                sql += "INSERT INTO plantilla_equipo(idequipo, idplantilla) "
                        + " VALUES (" + Pla_Equipos[x] + "," + Pla_Id + ");";
            }
            Globales.DatosAuditoria(sql, "PLANTILLA", "AGREGAR EQUIPO", idusuario, iplocal, this.getClass() + "-->GuardarPlantillas");

            if (archivoadjunto != null) {

                File destino = new File(Globales.ruta_archivo + "PlantillasCertificados/" + Pla_Id + "." + Globales.ExtensionArchivo(archivoadjunto));
                if (destino.exists()) {
                    destino.delete();
                }
                File uploads = new File(Globales.ruta_archivo + "uploads/" + archivoadjunto);
                Files.copy(uploads.toPath(), destino.toPath());
                archivoadjunto = null;
            }

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String FechaServidor(HttpServletRequest request) {
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        return Globales.FechaActual(tipo);
    }

    private String extencionesArc(String tipo) {
        switch (tipo) {
            case "data:image/jpeg;base64":
                return "jpg";
            case "data:image/png;base64":
                return "png";
            case "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64":
                return "xlsx";
            case "data:application/msword;base64":
                return "doc";
            case "data:application/pdf;base64":
                return "pdf";
            default:
                return "-";
        }
    }

    public String SubirFoto(HttpServletRequest request) {
        String datosB64 = request.getParameter("archivo");
        int tipo_imagen = Globales.Validarintnonull(request.getParameter("tipo_imagen"));
        int Movil = Globales.Validarintnonull(request.getParameter("movil"));
        String ruta = Globales.ruta_archivo;
        String ruta2 = Globales.ruta_archivo;
        String ruta3 = Globales.ruta_archivo;
        String tipo;
        String valorB64;
        String extencion;
        byte[] decodedImg = null;
        Path destinationFile;
        int ingreso = 0;
        int devolucion = 0;
        int numero = 0;
        int IdEmpleado = 0;
        int Comprimir = 1;

        String Archivo;
        File original;
        File destino;
        int articulo = 0;

        String certificado = "";
        int usuarios = 0;

        File path;
        String usuario_aud = (Movil == 0 ? "0" : idusuario);
        String iplocal_aud = (Movil == 0 ? "0" : iplocal);

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhss");
        String fecha = dateFormat.format(new Date());
        tipo = datosB64.split(",")[0].trim();
        valorB64 = datosB64.split(",")[1].trim().replaceAll(" ", "+");
        extencion = extencionesArc(tipo);
        if (extencion.equals("-")) {
            return "1|La extención del archivo es incorrecto";
        }
        switch (tipo_imagen) {
            case 1: //Foto Visita
                int opcion = Globales.Validarintnonull(request.getParameter("opciones"));
                String documento = request.getParameter("documento");
                String visita = request.getParameter("visita");
                if (opcion == 1) {
                    ruta += "Adjunto/imagenes/FotoVisita/" + documento + ".jpg";
                    ruta2 += "Adjunto/imagenes/FotoVisita/" + documento + "_copia.jpg";
                } else {
                    ruta += "Adjunto/imagenes/FotoDocumento/" + visita + ".jpg";
                    ruta2 += "Adjunto/imagenes/FotoDocumento/" + visita + "_copia.jpg";
                }
                break;
            case 2: //Fotos Ingresos
                ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
                sql = "select fotos from remision_detalle where ingreso = " + ingreso;
                numero = Globales.Validarintnonull(Globales.ObtenerUnValor(sql)) + 1;
                path = new File(ruta + "Adjunto/imagenes/ingresos/" + ingreso + "/");
                if (!path.exists()) {
                    path.mkdirs();
                }
                ruta += "Adjunto/imagenes/ingresos/" + ingreso + "/" + numero + ".jpg";
                ruta2 += "Adjunto/imagenes/ingresos/" + ingreso + "/" + numero + "_copia.jpg";
                break;
            case 3: // Fotos Devolucion
                devolucion = Globales.Validarintnonull(request.getParameter("devolucion"));
                sql = "select fotos, entregado from devolucion where devolucion = " + devolucion;
                datos = Globales.Obtenerdatos(sql, this.getClass() + "->SubirFoto", 1);

                try {
                    datos.next();
                    numero = Globales.Validarintnonull(datos.getString("fotos")) + 1;
                    int entregado = Globales.Validarintnonull(datos.getString("entregado"));
                    if (entregado == 1) {
                        return "1|No se pueden tormar fotos porque esta devolución ya fue entregada al cliente";
                    }
                } catch (SQLException ex) {
                    return "1|" + ex.getMessage();
                }

                path = new File(ruta + "Adjunto/imagenes/Devoluciones/" + devolucion + "/");
                if (!path.exists()) {
                    path.mkdirs();
                }
                ruta += "Adjunto/imagenes/Devoluciones/" + devolucion + "/" + numero + ".jpg";
                ruta2 += "Adjunto/imagenes/Devoluciones/" + devolucion + "/" + numero + "_copia.jpg";
                break;
            case 4: //Fotos Empleados
                IdEmpleado = Globales.Validarintnonull(request.getParameter("IdEmpleado"));
                ruta += "Adjunto/imagenes/FotoEmpleados/" + IdEmpleado + ".jpg";
                ruta2 += "Adjunto/imagenes/FotoEmpleados/" + IdEmpleado + "_copia.jpg";
                break;
            case 5: //Firma Certificado
                certificado = request.getParameter("certificado");
                usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
                Comprimir = 0;
                ruta2 += "Adjunto/imagenes/FirmaCertificado/" + certificado + ".jpg";
                break;
            case 6: //Firma Certificado
                articulo = Globales.Validarintnonull(request.getParameter("Articulo"));
                numero = Globales.Validarintnonull(request.getParameter("numero"));
                if (numero > 1) {
                    path = new File(ruta + "Adjunto/imagenes/Articulos/" + articulo + "/" + (numero - 1) + ".jpg");
                    if (!path.exists()) {
                        return "1|Debe de guardar primero la foto número " + (numero - 1);
                    }
                }
                path = new File(ruta + "Adjunto/imagenes/Articulos/" + articulo + "/");
                if (!path.exists()) {
                    path.mkdirs();
                }
                ruta += "Adjunto/imagenes/Articulos/" + articulo + "/" + numero + ".jpg";
                ruta2 += "Adjunto/imagenes/Articulos/" + articulo + "/" + numero + "_copia.jpg";
                break;

        }
        destinationFile = Paths.get(ruta2);
        decodedImg = Base64.getMimeDecoder().decode(valorB64.trim().getBytes(StandardCharsets.UTF_8));
        original = new File(ruta);
        destino = new File(ruta2);
        boolean comprimido = true;
        try {
            Files.write(destinationFile, decodedImg);
            if (Comprimir == 1) {
                comprimido = Globales.ComprimirIamgen(ruta2, ruta);
            }
            if (comprimido) {

                switch (tipo_imagen) {
                    case 2:
                        sql = "update remision_detalle set fotos = " + numero + " where ingreso = " + ingreso;
                        Globales.DatosAuditoria(sql, "INGRESO", "SUBIR FOTO DE INGRESO NUMERO " + ingreso + " FOTO = " + numero, usuario_aud, iplocal_aud, this.getClass() + "-->SubirFoto");
                        break;
                    case 3:
                        sql = "update devolucion set fotos = " + numero + " where devolucion = " + devolucion;
                        Globales.DatosAuditoria(sql, "INGRESO", "SUBIR FOTO DE DEVOLUCION NUMERO " + devolucion + " FOTO = " + numero, usuario_aud, iplocal_aud, this.getClass() + "-->SubirFoto");
                        break;
                    case 4:
                        sql = "UPDATE rrhh.empleados set foto = '" + IdEmpleado + ".jpg" + "' WHERE id = " + IdEmpleado;
                        Globales.DatosAuditoria(sql, "EMPLEADOS", "ACTUALIZAR FOTO", idusuario, iplocal, this.getClass() + "-->SubirFoto");
                        break;
                    case 5:
                        sql = "INSERT INTO public.certificados_firmas(certificado, idusuario) "
                                + "VALUES('" + certificado + "'," + usuarios + ")";
                        Globales.DatosAuditoria(sql, "CERTIFICADOR", "GUARDAR FIRMA", idusuario, iplocal, this.getClass() + "-->SubirFoto");
                    case 6:
                        if (numero == 1) {
                            sql = "UPDATE inventarios.articulos SET foto1 = 1 WHERE id = " + articulo;
                            Globales.DatosAuditoria(sql, "INVENTARIO-ARTICULO", "SUBIR FOTO", idusuario, iplocal, this.getClass() + "-->SubirFoto");
                        }
                }

                return "0|Foto subida con éxito|" + fecha + "|" + numero;
            } else {
                return "1|Error a comprimir las fotos";
            }

        } catch (IOException ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String SubirFotoMultiple(HttpServletRequest request) {
        if (archivoadjunto == null) {
            return "1|Error al subir los archivos";
        }
        String[] archivos = archivoadjunto.split("\\|");
        int tipo_imagen = Globales.Validarintnonull(request.getParameter("tipo_imagen"));
        int valor = Globales.Validarintnonull(request.getParameter("valor"));
        int numero = 0;
        int informe = 0;
        int ajuste = 0;

        String Archivo;

        String ruta;
        String ruta2;
        String extencion;

        File original;
        File destino;
        File path;

        String FotoSubidas = "";
        String FotoError = "";

        for (int i = 0; i < archivos.length; i++) {

            ruta2 = Globales.ruta_archivo;
            ruta = Globales.ruta_archivo;

            try {
                Archivo = Globales.ruta_archivo + "uploads/" + archivos[i];
                extencion = Globales.ExtensionArchivo(Archivo);
                if (!extencion.equals("png") && !extencion.equals("jpg") && !extencion.equals("jpeg")) {
                    return "1|La extención del archivo es incorrecto";
                }
                switch (tipo_imagen) {
                    case 2:
                        sql = "select fotos from remision_detalle where ingreso = " + valor;
                        numero = Globales.Validarintnonull(Globales.ObtenerUnValor(sql)) + 1;

                        path = new File(ruta + "Adjunto/imagenes/ingresos/" + valor + "/");
                        if (!path.exists()) {
                            path.mkdirs();
                        }

                        ruta += "Adjunto/imagenes/ingresos/" + valor + "/" + numero + ".jpg";
                        ruta2 += "Adjunto/imagenes/ingresos/" + valor + "/" + numero + "_copia.jpg";
                        break;
                    case 3:
                        sql = "select fotos, entregado from devolucion where devolucion = " + valor;
                        datos = Globales.Obtenerdatos(sql, this.getClass() + "->SubirFoto", 1);
                        try {
                            datos.next();
                            numero = Globales.Validarintnonull(datos.getString("fotos")) + 1;
                            int entregado = Globales.Validarintnonull(datos.getString("entregado"));
                            if (entregado == 1) {
                                return "1|No se pueden tormar fotos porque esta devolución ya fue entregada al cliente";
                            }
                        } catch (SQLException ex) {
                            return "1|" + ex.getMessage();
                        }

                        path = new File(ruta + "Adjunto/imagenes/Devoluciones/" + valor + "/");
                        if (!path.exists()) {
                            path.mkdirs();
                        }
                        ruta += "Adjunto/imagenes/Devoluciones/" + valor + "/" + numero + ".jpg";
                        ruta2 += "Adjunto/imagenes/Devoluciones/" + valor + "/" + numero + "_copia.jpg";
                        break;
                    case 5: //Fotos Informe Técnico
                        informe = Globales.Validarintnonull(request.getParameter("informe"));
                        numero = Globales.Validarintnonull(request.getParameter("numero"));
                        path = new File(ruta + "Adjunto/imagenes/informemantenimineto/" + informe + "/");
                        if (!path.exists()) {
                            path.mkdirs();
                        }
                        ruta += "Adjunto/imagenes/informemantenimineto/" + informe + "/" + numero + ".jpg";
                        ruta2 += "Adjunto/imagenes/informemantenimineto/" + informe + "/" + numero + "_copia.jpg";
                        break;
                    case 6: //Fotos de ajuste
                        ajuste = Globales.Validarintnonull(request.getParameter("ajuste"));
                        numero = Globales.Validarintnonull(request.getParameter("numero"));
                        path = new File(ruta + "Adjunto/imagenes/ajuste/" + ajuste + "/");
                        if (!path.exists()) {
                            path.mkdirs();
                        }
                        ruta += "Adjunto/imagenes/ajuste/" + ajuste + "/" + numero + ".jpg";
                        ruta2 += "Adjunto/imagenes/ajuste/" + ajuste + "/" + numero + "_copia.jpg";
                        break;

                }
                original = new File(Archivo);
                destino = new File(ruta2);

                if (extencion.equals("png")) {
                    Globales.ConvertirJPG(original, destino);
                } else {
                    Files.copy(original.toPath(), destino.toPath(), StandardCopyOption.REPLACE_EXISTING);
                }

                if (Globales.ComprimirIamgen(ruta2, ruta)) {

                    switch (tipo_imagen) {
                        case 2:
                            sql = "update remision_detalle set fotos = " + numero + " where ingreso = " + valor;
                            Globales.DatosAuditoria(sql, "INGRESO", "SUBIR FOTO DE INGRESO NUMERO " + valor + " FOTO = " + numero, idusuario, iplocal, this.getClass() + "-->SubirFoto");
                            break;
                        case 3:
                            sql = "update devolucion set fotos = " + numero + " where devolucion = " + valor;
                            Globales.DatosAuditoria(sql, "INGRESO", "SUBIR FOTO DE DEVOLUCION NUMERO " + valor + " FOTO = " + numero, idusuario, iplocal, this.getClass() + "-->SubirFoto");
                            break;
                        case 5:
                            sql = "update informetecnico_dev set fotoinforme = 1 where informe = " + informe;
                            Globales.DatosAuditoria(sql, "INFORME TECNICO", "SUBIR FOTOS", idusuario, iplocal, this.getClass() + "-->SubirFoto");
                            break;
                        case 6:
                            sql = "update ingreso_ajuste set fotos = 1 where ajuste = " + ajuste;
                            Globales.DatosAuditoria(sql, "AJUSTE", "SUBIR FOTOS", idusuario, iplocal, this.getClass() + "-->SubirFoto");
                            break;
                    }
                    FotoSubidas += Archivo.split("\\/")[Archivo.split("\\/").length - 1] + "<br>";
                } else {
                    FotoError += Archivo.split("\\/")[Archivo.split("\\/").length - 1] + "<br>";
                }
            } catch (IOException ex) {
                return "1|" + ex.getMessage();
            }

        }
        return "0|" + FotoSubidas + "|" + FotoError;
    }

    public String GenerarPDF(HttpServletRequest request) {
        try {
            String documento = request.getParameter("documento");
            String tipo = request.getParameter("tipo");
            String Barra = System.getProperty("file.separator");
            String urlArchivo = request.getRealPath(Barra).replace(Barra + "build", "") + "Reportes" + Barra;
            Map<String, Object> pReporte = new HashMap<String, Object>();
            String nbArchivo = "";
            int GuardarGestion = 1;
            int idpdf = 0;

            String iniciales = "";
            String idversion = "0";
            String creadopor = "0";

            String fechad;
            String fechah;

            String version = "";
            String carpeta = "";
            String numero = "";
            String revision = "";
            String archivo_gestion = "";
            String[] informacion = null;
            String tipofirma;

            String ingreso = "";
            String plantilla = "";

            int idcertificadoprevio = 0;
            int idcertificado = 0;
            String ingresoce = "";
            String numerocer = "";
            String certificado = "";
            String medida = "";
            int impreso = 0;

            String ruta_gestion = "";
            if (!documento.equals("0")) {
                ruta_gestion = Globales.ValidarRutaGestion(tipo, documento, "", 0);
                if (ruta_gestion.equals("")) {
                    return "1||DEBE DE CONFIGURAR LA RUTA DE GESTIÓN PARA EL DOCUMENTO " + tipo + " EN LA BASE DE DATOS";
                }
            }

            switch (tipo) {
                case "Cotizacion":
                case "Cotizacion_Servicio":

                    sql = "select count(idequipo) from cotizacion_detalle cd inner join cotizacion c on c.id = cd.idcotizacion "
                            + "where cotizacion = " + documento + " and idequipo > 0 and idservicio = 0";
                    int equipos = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    sql = "select count(idequipo) from cotizacion_detalle cd inner join cotizacion c on c.id = cd.idcotizacion "
                            + "where cotizacion = " + documento + " and idequipo > 0";
                    int servicios = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    sql = "select c.estado, ini_pais, c.idempresa, version, c.idusuario, v.id as idversion, '" + equipos + "' as equipos, '" + servicios + "' as servicios,  cli.nombrecompleto as cliente "
                            + " from cotizacion c inner join version_documento v on v.id = c.idversion "
                            + "                   inner join clientes cli on cli.id = c.idcliente "
                            + "                   inner join empresa e on e.id = c.idempresa "
                            + " where cotizacion = " + documento;
                    nbArchivo = tipo + ".jasper";

                    datos = Globales.Obtenerdatos(sql, this.getClass() + "->GenerarPDF", 1);
                    datos.next();
                    iniciales = datos.getString("ini_pais");

                    if (datos.getString("estado").equals("Temporal")) {
                        return "1||No se puede imprimir una cotización en estado temporal";
                    } else if (datos.getString("estado").equals("Aprobado")) {
                        nbArchivo = "Cotizacion_Aprobada.jasper";
                    }
                    if (equipos > 0) {
                        return "1||Para imprimir una cotización debe de ingresar todos los servicios a todos los equipos";
                    }
                    if (Globales.Validarintnonull(datos.getString("version")) == 0) {
                        return "1||Debe de configuarar la versión del documento";
                    }
                    if (tipo.equals("Cotizacion_Servicio") && servicios > 0) {
                        return "1||No se puede imprimir esta cotización en servicios porque posee equipos de calibracion";
                    }
                    version = datos.getString("version");
                    archivo_gestion = documento + " - " + datos.getString("cliente") + ".pdf";
                    carpeta = "Cotizacion";
                    idversion = datos.getString("idversion");
                    creadopor = datos.getString("idusuario");

                    break;
                case "Remision":
                    sql = "select r.estado, version, v.id as idversion, r.idusuario, cli.nombrecompleto as cliente "
                            + " from remision r inner join version_documento v on v.id = r.idversion "
                            + "                   inner join clientes cli on cli.id = r.idcliente "
                            + " where remision = " + documento;
                    nbArchivo = tipo + ".jasper";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "->GenerarPDF", 1);
                    datos.next();
                    if (datos.getString("estado").equals("Temporal")) {
                        return "1||No se puede imprimir una remisión en estado temporal";
                    }
                    if (Globales.Validarintnonull(datos.getString("version")) == 0) {
                        return "1||Debe de configuarar la versión del documento";
                    }

                    version = datos.getString("version");
                    archivo_gestion = documento + " - " + datos.getString("cliente") + ".pdf";
                    carpeta = "Remision";
                    idversion = datos.getString("idversion");
                    creadopor = datos.getString("idusuario");
                    break;
                case "Devolucion":
                    sql = "select d.estado, version, impreso, d.idcliente, limitecre, v.id as idversion, cli.nombrecompleto as cliente, d.idusuario "
                            + " from devolucion d inner join version_documento v on v.id = d.idversion "
                            + "                   inner join clientes cli on cli.id = d.idcliente "
                            + " where devolucion = " + documento;
                    nbArchivo = tipo + ".jasper";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "->GenerarPDF", 1);
                    datos.next();
                    double limitecredito = Globales.ValidardoublenoNull(datos.getString("limitecre"));
                    impreso = Globales.Validarintnonull(datos.getString("impreso"));
                    if (impreso == 0) {
                        sql = "select sum(case when extract(days from(now() - vencimiento)) > diasbloqueo then saldo else 0 end) as vencido "
                                + " FROM factura f inner join clientes c on c.id = f.idcliente where f.estado  <> 'Anulado' and saldo <> 0 and f.idcliente = " + datos.getString("idcliente");
                        double saldo = Globales.ValidardoublenoNull(Globales.ObtenerUnValor(sql));

                        if (saldo > limitecredito) {

                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            Calendar calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_YEAR, 3);
                            Date fecha = calendar.getTime();

                            sql = "SELECT s.id  "
                                    + " FROM public.seguimiento_cartera s inner join seguridad.rbac_usuario u on s.idusuario = u.idusu "
                                    + " where devolucion like '%$" + documento + "$%' and to_char(fecha + CAST('3 days' AS INTERVAL),'yyyy-MM-dd') >= '" + dateFormat.format(fecha) + "'";
                            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                            if (encontrado == 0) {
                                return "1||No se puede imprimir la devolución porque el cliente posee saldos pendiente... diríjase a cartera para gestionar su pago";
                            }
                        }
                    }

                    if (datos.getString("estado").equals("Temporal")) {
                        return "1||No se puede imprimir una devolución en estado temporal";
                    }
                    if (Globales.Validarintnonull(datos.getString("version")) == 0) {
                        return "1||Debe de configuarar la versión del documento";
                    }

                    version = datos.getString("version");
                    archivo_gestion = documento + " - " + datos.getString("cliente") + ".pdf";
                    carpeta = "Devolucion";
                    idversion = datos.getString("idversion");
                    creadopor = datos.getString("idusuario");
                    break;

                case "EtiquetaDevolucion":
                    String devolucion = request.getParameter("devolucion");
                    sql = "SELECT COUNT(id) FROM imprimir_etiqueta WHERE devolucion = " + devolucion;
                    int id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    if (id == 0) {
                        sql = "INSERT INTO imprimir_etiqueta (devolucion, tipo) "
                                + " VALUES(" + devolucion + ", 1) ,(" + devolucion + ",2);";
                        Globales.Obtenerdatos(sql, this.getClass() + "-->GenerarPDF", 2);
                    }
                    documento = devolucion;
                    ruta_gestion = Globales.ValidarRutaGestion(tipo, documento, "", 0);
                    if (ruta_gestion.equals("")) {
                        return "1||DEBE DE CONFIGURAR LA RUTA DE GESTIÓN PARA EL DOCUMENTO " + tipo + " EN LA BASE DE DATOS</h3></center>";
                    }

                    nbArchivo = tipo + ".jasper";
                    version = "";
                    archivo_gestion = documento + ".pdf";
                    carpeta = "Etiquetas";
                    break;
                case "Solicitud":
                    sql = "select s.estado || '|' || version || '|' || cli.nombrecompleto "
                            + " from web.solicitud s inner join version_documento v on v.id = s.idversion "
                            + "                   inner join clientes cli on cli.id = s.idcliente "
                            + " where solicitud = " + documento;
                    nbArchivo = tipo + ".jasper";
                    System.out.print(sql);
                    informacion = Globales.ObtenerUnValor(sql).split("\\|");
                    if (informacion[0].equals("Temporal")) {
                        return "1||No se puede imprimir una solicitud en estado temporal";
                    }
                    if (Globales.Validarintnonull(informacion[1]) == 0) {
                        return "1||Debe de configuarar la versión del documento";
                    }

                    version = informacion[1];
                    archivo_gestion = documento + " - " + informacion[2] + ".pdf";
                    carpeta = "Solicitud";
                    break;
                case "Salida":
                    sql = "select s.estado || '|' || version || '|' || cli.nombrecompleto "
                            + " from salida s inner join version_documento v on v.id = s.idversion "
                            + "                   inner join clientes cli on cli.id = s.idcliente "
                            + " where salida = " + documento;
                    nbArchivo = tipo + ".jasper";
                    System.out.print(sql);
                    informacion = Globales.ObtenerUnValor(sql).split("\\|");
                    if (informacion[0].equals("Temporal")) {
                        return "1||No se puede imprimir una solicitud en estado temporal";
                    }
                    if (Globales.Validarintnonull(informacion[1]) == 0) {
                        return "1||Debe de configuarar la versión del documento";
                    }

                    version = informacion[1];
                    archivo_gestion = documento + " - " + informacion[2] + ".pdf";
                    carpeta = "Salida";
                    break;
                case "OrdenCompra":
                    sql = "select o.estado || '|' || version || '|' || cli.nombrecompleto "
                            + " from ordencompra_tercero o inner join version_documento v on v.id = o.idversion "
                            + "                   inner join clientes cli on cli.id = o.idcliente "
                            + " where orden = " + documento;
                    nbArchivo = tipo + ".jasper";
                    System.out.print(sql);
                    informacion = Globales.ObtenerUnValor(sql).split("\\|");
                    if (informacion[0].equals("Temporal")) {
                        return "1||No se puede imprimir una solicitud en estado temporal";
                    }
                    if (Globales.Validarintnonull(informacion[1]) == 0) {
                        return "1||Debe de configuarar la versión del documento";
                    }

                    version = informacion[1];
                    archivo_gestion = documento + " - " + informacion[2] + ".pdf";
                    carpeta = "OrdenCompra";
                    break;
                case "Factura":
                    sql = "select f.estado || '|' || version || '|' || cli.nombrecompleto "
                            + " from factura f inner join version_documento v on v.id = f.idversion "
                            + "                   inner join clientes cli on cli.id = f.idcliente "
                            + " where factura = " + documento;
                    nbArchivo = tipo + ".jasper";
                    System.out.print(sql);
                    informacion = Globales.ObtenerUnValor(sql).split("\\|");
                    if (informacion[0].equals("Temporal")) {
                        return "1||No se puede imprimir una solicitud en estado temporal";
                    }
                    if (Globales.Validarintnonull(informacion[1]) == 0) {
                        return "1||Debe de configuarar la versión del documento";
                    }

                    version = informacion[1];
                    archivo_gestion = documento + " - " + informacion[2] + ".pdf";
                    carpeta = "Factura";
                    break;
                case "InformeTecnico":
                    ingreso = request.getParameter("ingreso");
                    plantilla = request.getParameter("plantilla");
                    sql = "select ip.id, if.id as idinforme, coalesce(version,(select version from version_documento where documento = 'INFORMETECNICO' order by revision desc limit 1)) as version, cli.nombrecompleto as cliente, coalesce(informe,0) as informe "
                            + " from reporte_ingreso ri inner join ingreso_plantilla ip on ip.ingreso = ri.ingreso and ip.idplantilla = ri.plantilla  "
                            + "                         inner join remision_detalle rd on rd.ingreso = ri.ingreso "
                            + "                         inner join clientes cli on cli.id = rd.idcliente "
                            + "                         left join informetecnico_dev if on if.ingreso = ri.ingreso and ri.plantilla = if.idplantilla and if.estado <> 'Anulado' "
                            + "                         left join version_documento v on v.id = if.idversion "
                            + " where ri.ingreso = " + ingreso + " and ri.plantilla = " + plantilla + " order by ri.id desc limit 1";
                    nbArchivo = tipo + ".jasper";
                    System.out.print(sql);
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GenerarPDF", 1);
                    datos.next();
                    documento = datos.getString("id");
                    if (Globales.Validarintnonull(datos.getString("version")) == 0) {
                        return "1||Debe de configuarar la versión del documento";
                    }
                    if (Globales.Validarintnonull(datos.getString("informe")) == 0) {
                        GuardarGestion = 0;
                    } else {
                        ruta_gestion = Globales.ValidarRutaGestion(tipo, datos.getString("idinforme"), "", 0);
                        if (ruta_gestion.equals("")) {
                            return "1||DEBE DE CONFIGURAR LA RUTA DE GESTIÓN PARA EL DOCUMENTO " + tipo + " EN LA BASE DE DATOS";
                        }
                    }
                    version = datos.getString("version");
                    archivo_gestion = documento + " - " + datos.getString("cliente") + ".pdf";
                    carpeta = "InformeTecnico";
                    break;

                case "CertificadoParTorsionalV07":
                    ingreso = request.getParameter("ingreso");
                    numero = request.getParameter("numero");
                    revision = request.getParameter("revision");
                    tipofirma = request.getParameter("tipofirma");
                    int vistaprevia = Globales.Validarintnonull(request.getParameter("vistaprevia"));
                    sql = "SELECT coalesce(c.id,0) as id, coalesce(v.version,'') as version, cp.id as idcertificadoprevio, coalesce(c.idusuario,0) as idusuario, coalesce(to_char(c.fechaexp, 'yyyy-MM-dd'),'') as fecha, coalesce(c.impreso,0) as impreso, to_char(cp.ingreso,'0000000') as ingreso, c.numero  "
                            + " FROM certificados_previos cp inner join version_documento v on v.id = cp.idversioncp "
                            + "                               left join  certificados c on c.ingreso = cp.ingreso and cp.item = c.item and c.revision = cp.revision "
                            + " where cp.ingreso = " + ingreso + " and cp.item = " + numero + " and cp.revision = " + revision;
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "->GenerarPDF", 1);
                    datos.next();
                    int Usuario = Globales.Validarintnonull(datos.getString("idusuario"));
                    impreso = Globales.Validarintnonull(datos.getString("impreso"));

                    idcertificadoprevio = Globales.Validarintnonull(datos.getString("idcertificadoprevio"));
                    idcertificado = Globales.Validarintnonull(datos.getString("id"));
                    ingresoce = datos.getString("ingreso");
                    version = datos.getString("version");
                    numerocer = datos.getString("numero") + " " + datos.getString("ingreso");
                    certificado = datos.getString("numero");
                    archivo_gestion = numerocer + ".pdf";
                    medida = "";

                    if (version.equals("")) {
                        return "1||Debe de configuarar la versión del documento";
                    }

                    if (vistaprevia == 0) {

                        if (idcertificado == 0) {
                            return "1||Debe primero generar el certificado";
                        }

                        ruta_gestion = Globales.ValidarRutaGestion(tipo, String.valueOf(idcertificado).trim(), "", 0);
                        if (ruta_gestion.equals("")) {
                            return "1||DEBE DE CONFIGURAR LA RUTA DE GESTIÓN PARA EL DOCUMENTO " + tipo + " EN LA BASE DE DATOS";
                        }
                    } else {
                        GuardarGestion = 0;
                    }

                    if (vistaprevia == 0) {
                        if (impreso > 0) {
                            if (tipofirma.equals("1")) {
                                return "9||" + numerocer;
                            }
                        } else {
                            if (tipofirma.equals("2")) {
                                return "1||Para imprimir el certificado sin firma debe primero imprimir el certificado con firma digital";
                            }
                        }
                    } else {
                        if (impreso == 1) {
                            return "1||No puede imprimir la vista previa porque ya se generó el PDF del certificado";
                        }
                    }

                    sql = "SELECT valornumerico(despromedio) as despromedio, valornumerico(increlativa) as errores, "
                            + " 0 as ideal, lectura as porlectura, medida "
                            + " FROM certificado_datos_a c inner join remision_detalle r on c.ingreso = r.ingreso "
                            + "                            inner join magnitud_intervalos i on i.id = r.idintervalo "
                            + "                            inner join certificados_previos cp on c.ingreso = cp.ingreso and cp.item = c.numero and c.revision = cp.revision "
                            + "  where cp.id = " + idcertificadoprevio;
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "->GenerarPDF", 1);
                    DefaultStatisticalCategoryDataset dataset = new DefaultStatisticalCategoryDataset();
                    while (datos.next()) {
                        dataset.add(Globales.ValidardoublenoNull(datos.getString("despromedio")), Globales.ValidardoublenoNull(datos.getString("errores")), "Desviacion", datos.getString("porlectura"));
                        dataset.add(0, 0, "Ideal", datos.getString("porlectura"));
                        medida = datos.getString("medida");
                    }

                    JFreeChart chartErrorBars = ChartFactory.createLineChart("Indicación del IBC vs Error de medición", "Par torsional nominal en " + medida, "Error de medición [%]", dataset, PlotOrientation.VERTICAL, true, true, true);
                    StatisticalLineAndShapeRenderer statisticalRenderer = new StatisticalLineAndShapeRenderer(false, true);
                    statisticalRenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator("{4}", NumberFormat.getNumberInstance()));
                    //statisticalRenderer.setBaseShapesVisible(true);
                    statisticalRenderer.setBaseFillPaint(Color.white);
                    statisticalRenderer.setBasePaint(Color.white);
                    //statisticalRenderer.setSeriesPaint(0, Color.blue);
                    statisticalRenderer.setSeriesStroke(0, new BasicStroke(12));
                    statisticalRenderer.setSeriesOutlinePaint(0, Color.blue);

                    //statisticalRenderer.setSeriesPaint(1, Color.red);
                    statisticalRenderer.setSeriesOutlinePaint(1, Color.red);
                    statisticalRenderer.setSeriesStroke(1, new BasicStroke(12));

                    chartErrorBars.getCategoryPlot().setRenderer(statisticalRenderer);
                    chartErrorBars.getCategoryPlot().setBackgroundPaint(Color.white);
                    chartErrorBars.getCategoryPlot().setRangeGridlinesVisible(true);
                    chartErrorBars.getCategoryPlot().setRangeGridlinePaint(Color.gray);
                    chartErrorBars.getCategoryPlot().setDomainGridlinesVisible(true);
                    chartErrorBars.getCategoryPlot().setDomainGridlinePaint(Color.gray);
                    //statisticalRenderer.setBaseLinesVisible(false); 

                    int width = 570;
                    int height = 305;

                    try {
                        FileOutputStream fos;
                        ByteArrayOutputStream baos;
                        /*ChartUtilities.writeChartAsPNG(baos, chart, width, height);
                        baos.writeTo(fos);
                        baos.close();
                        fos.close();*/
                        File archivo = new File(Globales.ruta_archivo + "Adjunto/imagenes/graficas/" + certificado + ".png");
                        if (archivo.exists()) {
                            archivo.delete();
                        }
                        fos = new FileOutputStream(archivo);
                        baos = new ByteArrayOutputStream();
                        ChartUtilities.writeChartAsPNG(baos, chartErrorBars, width, height);
                        baos.writeTo(fos);
                        baos.close();
                        fos.close();

                    } catch (IOException e) {
                        return "1||" + e.getMessage();
                    }

                    documento = String.valueOf(idcertificadoprevio).trim();
                    carpeta = "Laboratorio/Par_Torsional";
                    nbArchivo = "CertificadoPT1.jasper";

                    break;

                case "ResumenPagoEmpleado":
                    String periodo = request.getParameter("periodo");
                    String titulo = Globales.TituloNomina(periodo);
                    pReporte.put("TITULO", new String(titulo));
                    GuardarGestion = 0;
                    carpeta = "Nomina";
                    nbArchivo = tipo + ".jasper";
                    documento = periodo;
                    break;
                case "ResumenDiligencias":
                    int vuelta = 0;
                    String fecha_diligencia = request.getParameter("fecha");
                    sql = "SELECT count(contador) FROM registrovueltas "
                            + " WHERE estado = 'POR APROBAR' and to_char(fechapro, 'yyyy-MM-dd') = '" + fecha_diligencia + "'";
                    vuelta = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    if (vuelta > 0) {
                        return "1||Hay " + vuelta + " vuelta(s) pendiente(s) por autorizar";
                    }

                    sql = "select v.id "
                            + "  from registrovueltas v "
                            + "  WHERE v.estado IN ('AUTORIZADA','NO EJECUTADA','EJECUTADA','ASIGNADA','NO EJECUTADA - RECHAZADA') and to_char(fechapro, 'yyyy-MM-dd') = '" + fecha_diligencia + "' "
                            + "  order by v.id";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "->GenerarPDF", 1);

                    if (!datos.next()) {
                        return "1||No hay nada que reportar";
                    }

                    sql = "select max(contador) "
                            + " from registrovueltas v "
                            + "  WHERE v.estado IN ('AUTORIZADA','NO EJECUTADA','EJECUTADA','ASIGNADA','NO EJECUTADA - RECHAZADA') and to_char(fechapro, 'yyyy-MM-dd') = '" + fecha_diligencia + "'";
                    vuelta = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    if (vuelta == 0) {
                        if (Globales.PermisosSistemas("VUELTAS GENERAR PDF", idusuario) == 0) {
                            return "1||Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                        }

                        sql = "SELECT vueltas from contadores";
                        vuelta = Globales.Validarintnonull(Globales.ObtenerUnValor(sql)) + 1;
                        sql = "select id from version_documento WHERE documento = 'DILIGENCIAS' ORDER BY revision desc limit 1";
                        idversion = Globales.ObtenerUnValor(sql);
                        sql = "UPDATE registrovueltas SET contador = " + vuelta + ", idversion = " + idversion + ", fechapdf = now() "
                                + "WHERE estado IN ('AUTORIZADA','NO EJECUTADA','EJECUTADA','NO EJECUTADA - RECHAZADA', 'ASIGNADA') and to_char(fechapro, 'yyyy-MM-dd') = '" + fecha_diligencia + "';";
                        sql += "UPDATE contadores SET vueltas = " + vuelta;
                        if (!Globales.DatosAuditoria(sql, "VUELTAS", "GENERAR PDF", idusuario, iplocal, this.getClass() + "->GenerarPDF")) {
                            return "1||Error inesperado.... Revise los archivos de log";
                        }
                    }

                    sql = "select v.id, contador, to_char(fechapro, 'yyyy-MM-dd'), version \n"
                            + " from registrovueltas v INNER JOIN version_documento vd on vd.id = v.idversion "
                            + " WHERE (estado IN ('AUTORIZADA','NO EJECUTADA','EJECUTADA','NO EJECUTADA - RECHAZADA','ASIGNADA') and to_char(fechapro, 'yyyy-MM-dd') = '2019-12-10') \n"
                            + " order by fechapro limit 1";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "->GenerarPDF", 1);
                    if (!datos.next()) {
                        return "1||No hay nada que reportar";
                    }
                    version = datos.getString("version");
                    documento = fecha_diligencia;
                    carpeta = "General";
                    nbArchivo = "RegistroVueltas.jasper";
                    ruta_gestion = Globales.ValidarRutaGestion(tipo, datos.getString("id"), "", 0);
                    if (ruta_gestion.equals("")) {
                        return "1||DEBE DE CONFIGURAR LA RUTA DE GESTIÓN PARA EL DOCUMENTO " + tipo + " EN LA BASE DE DATOS";
                    }
                    break;
                case "NotasCreditos":
                    sql = "select n.estado || '|' || version || '|' || cli.nombrecompleto "
                            + " from nota_credito n inner join version_documento v on v.id = n.idversion "
                            + "                   inner join factura f on f.factura = n.factura "
                            + "                   inner join clientes cli on cli.id = f.idcliente "
                            + " where nota = " + documento;
                    nbArchivo = tipo + ".jasper";
                    System.out.print(sql);
                    informacion = Globales.ObtenerUnValor(sql).split("\\|");
                    if (informacion[0].equals("Temporal")) {
                        return "1||No se puede imprimir una solicitud en estado temporal";
                    }
                    if (Globales.Validarintnonull(informacion[1]) == 0) {
                        return "1||Debe de configuarar la versión del documento";
                    }

                    version = informacion[1];
                    archivo_gestion = documento + " - " + informacion[2] + ".pdf";
                    carpeta = "NotasCreditos";
                    break;
                case "actividad":
                    carpeta = "Actividades";
                    version = "01";
                    nbArchivo = "Actividades.jasper";
                    documento = request.getParameter("id");
                    pReporte.put("SUBREPORT_DIR", "");
                    pReporte.put("identificacion", documento);
                    pReporte.put("ruta_logo", Globales.ruta_archivo + "Adjunto/imagenes/logo.png");
                    GuardarGestion = 0;
                    break;
                case "CondicionesAmbientales":
                    carpeta = "Condiciones_Ambientales";
                    sql = "select id, version from version_documento WHERE documento = 'CONDICIONESAMBIENTALES' ORDER BY revision desc limit 1";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "->GenerarPDF", 1);
                    datos.next();
                    version = datos.getString("version");
                    idversion = datos.getString("id");
                    nbArchivo = "Condiciones.jasper";
                    String sensor = request.getParameter("sensor");
                    fechad = request.getParameter("fechad");
                    fechah = request.getParameter("fechah");
                    String valormediot = request.getParameter("valormediot");
                    String valormedioh = request.getParameter("valormedioh");
                    String desviaciont = request.getParameter("desviaciont");
                    String desviacionh = request.getParameter("desviacionh");
                    idpdf = Globales.Validarintnonull(request.getParameter("idpdf"));
                    sql = "select serie from intrumento_condamb_sensores WHERE id = " + sensor;
                    archivo_gestion = Globales.ObtenerUnValor(sql) + " " + fechad + " a " + fechah + ".pdf";
                    pReporte.put("SENSOR", sensor);
                    pReporte.put("FECHA_DESDE", fechad);
                    pReporte.put("FECHA_HASTA", fechah);
                    pReporte.put("TVALOR MEDIO", new Long(valormediot));
                    pReporte.put("TDESVIACION", new Long(desviaciont));
                    pReporte.put("HVALOR MEDIO", new Long(valormedioh));
                    pReporte.put("HDESVIACION", new Long(desviacionh));
                    String nombre = Globales.ObtenerUnValor("SELECT nombrecompleto || ' (' || cargo || ')' FROM seguridad.rbac_usuario where idusu=" + idusuario);
                    pReporte.put("USUARIO", nombre);
                    GuardarGestion = 1;
                    creadopor = idusuario;

                    if (idpdf == 0) {
                        sql = "INSERT INTO public.condiciones_pdf(idsensor, fechadesde, fechahasta, idusuario, valormediot, valormedioh, desviaciont, desviacionh) "
                                + " VALUES (" + sensor + ",'" + fechad + "','" + fechah + "'," + idusuario + "," + valormediot + "," + valormedioh + "," + desviaciont + "," + desviacionh + ")";
                        Globales.DatosAuditoria(sql, "CONDICIONES AMBIENTALES", "GENERAR PDF", idusuario, iplocal, this.getClass() + "->GenerarPDF");
                        idpdf = Globales.Validarintnonull(Globales.ObtenerUnValor("select max(id) from condiciones_pdf"));

                    }

                    ruta_gestion = Globales.ValidarRutaGestion(tipo, String.valueOf(idpdf).trim(), "", 0);
                    if (ruta_gestion.equals("")) {
                        return "1||DEBE DE CONFIGURAR LA RUTA DE GESTIÓN PARA EL DOCUMENTO " + tipo + " EN LA BASE DE DATOS";
                    }

                    break;
                case "CronogramaActividad":
                    carpeta = "Actividades";
                    sql = "select id, version from version_documento WHERE documento = 'CRONOGRAMADEACTIVIDADES' ORDER BY revision desc limit 1";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "->GenerarPDF", 1);
                    datos.next();
                    version = datos.getString("version");
                    idversion = datos.getString("id");
                    nbArchivo = "CronogramaActividades.jasper";
                    fechad = request.getParameter("fechad");
                    fechah = request.getParameter("fechah");
                    pReporte.put("FECHA_DESDE", fechad);
                    pReporte.put("FECHA_HASTA", fechah);
                    GuardarGestion = 1;
                    creadopor = idusuario;
                    archivo_gestion = "DID-03 " + " " + fechad + " a " + fechah + ".pdf";

                    ruta_gestion = Globales.ValidarRutaGestion(tipo, "0", fechad, 0);
                    if (ruta_gestion.equals("")) {
                        return "1||DEBE DE CONFIGURAR LA RUTA DE GESTIÓN PARA EL DOCUMENTO " + tipo + " EN LA BASE DE DATOS";
                    }

                    break;
            }

            urlArchivo += (!carpeta.equals("") ? carpeta + Barra : "") + (!iniciales.equals("") ? iniciales + Barra : "") + (!version.equals("") ? version + Barra : "");
            String nombre_archivo = nbArchivo.split("\\.")[0] + "_" + Globales.FechaActual(3) + ".pdf";
            pReporte.put("DOCUMENTO", new String(documento));

            File origen;
            File destion;

            byte[] bReporte = JasperRunManager.runReportToPdf(urlArchivo + nbArchivo, pReporte, Globales.DBReportes(request));
            FileOutputStream fos = new FileOutputStream(Globales.ruta_archivo + "DocumPDF/" + nombre_archivo);
            fos.write(bReporte);
            if (GuardarGestion == 1) {
                origen = new File(Globales.ruta_archivo + "DocumPDF/" + nombre_archivo);
                destion = new File(ruta_gestion + archivo_gestion);
                Files.copy(origen.toPath(), destion.toPath(), StandardCopyOption.REPLACE_EXISTING);

                sql = "UPDATE sg.propiedades_archivo SET estado = 'ELIMINADO' where nombre = '" + archivo_gestion + "';";
                sql += "INSERT INTO sg.propiedades_archivo(ruta, nombre, tamanio, tipo, carpeta, clasificacion, idusuario, idversion, gestion) "
                        + " VALUES ('" + ruta_gestion + "','" + archivo_gestion + "'," + origen.length() + ",2,'" + ruta_gestion + "','pdf'," + creadopor + "," + idversion + ",'');";
                Globales.DatosAuditoria(sql, "SISTEMA GESTION", "DILIGENCIAR DOCUMENTO", idusuario, iplocal, this.getClass() + "->GenerarPDF");
            }

            switch (tipo) {
                case "Devolucion":
                    sql = "UPDATE devolucion set impreso = 1 WHERE devolucion = " + documento;
                    Globales.DatosAuditoria(sql, "DEVOLUCION", "IMPRIMIR", idusuario, iplocal, this.getClass() + "->GenerarPDF");
                case "CertificadoParTorsionalV07":
                    origen = new File(Globales.ruta_archivo + "DocumPDF/" + nombre_archivo);
                    destion = new File(Globales.ruta_archivo + "Adjunto/Certificados/" + numerocer + ".pdf");
                    Files.copy(origen.toPath(), destion.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    sql = "UPDATE certificados SET impreso = 1, fechaimpreso=now() where id = " + idcertificado + ";";
                    sql += "UPDATE ingreso_plantilla SET certificado = 1 where ingreso =" + ingreso + " and idplantilla=" + numero;
                    Globales.DatosAuditoria(sql, "CERTIFICADO", "IMPRIMIR", idusuario, iplocal, this.getClass() + "->GenerarPDF");
                    break;
                case "CondicionesAmbientales":
                    origen = new File(Globales.ruta_archivo + "DocumPDF/" + nombre_archivo);
                    destion = new File(Globales.ruta_archivo + "Adjunto/CondicionesAmbientales/" + idpdf + ".pdf");
                    Files.copy(origen.toPath(), destion.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    break;
            }

            return "0||" + nombre_archivo;
        } catch (Exception ex) {
            return "1||" + ex.getMessage();
        }

    }

    public String CentralTelefonica() {

        sql = "SELECT numero, oficina, nombrecompleto as usuario"
                + "  FROM central_telefonica ct inner join seguridad.rbac_usuario u on u.idusu = ct.idusuario"
                + "  where ct.estado = 1 order by numero";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->CentralTelefonica");
    }

    public String TablaRespaldo() {

        //Globales.zipDirectory(new File("C:/Users/eurid/Google Drive/paginas/Calibration/Adjunto/"),"C:/Users/eurid/Google Drive/paginas/Calibration/Adjunto.zip");
        sql = "SELECT id, ruta, estado, horario, dias \n"
                + " FROM public.empresa_rutarespaldo order by 1";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaRespaldo");

    }

    public String UnidadesDisco() {

        String sSistemaOperativo = System.getProperty("os.name");
        System.out.print(sSistemaOperativo);
        String Resultado = "<option value=''>--</option>";

        if (sSistemaOperativo.contains("Windows")) {
            Resultado = "<option value=''>--</option>";
            File[] unidades = File.listRoots();
            for (File f : unidades) {
                Resultado += "<option value='" + f + "'>" + FileSystemView.getFileSystemView().getSystemDisplayName(f) + " Capacidad: " + f.getTotalSpace() + ", Disponible: " + f.getFreeSpace() + "</option>";
            }

        } else {
            File ruta = new File(Globales.ruta_media_linux);
            String[] directorio = ruta.list();
            for (int x = 0; x < directorio.length; x++) {
                Resultado += "<option value='" + Globales.ruta_media_linux + directorio[x] + "/'>" + directorio[x] + "</option>";
            }
        }
        return Resultado;
    }

    public String RespaldarArchivos(HttpServletRequest request) {

        String archivo = request.getParameter("archivos");
        String unidad = request.getParameter("unidad");

        String archivos[] = archivo.split(",");

        String ruta;
        String estado;
        String horario;
        String dias;
        String nombre;
        String ruta_respaldo = Globales.ruta_respaldo;

        if (Globales.PermisosSistemas("SEGURIDAD RESPALDAR ARCHIVOS", idusuario) == 0) {
            return "1||Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        if (ruta_respaldo == null) {
            return "1||No se ha configurado la ruta de respaldo en la bade de datos";
        }

        Date fecha = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd HH.mm.ss");
        DateFormat dateFormatAnio = new SimpleDateFormat("yyyy");
        DateFormat dateFormatDia = new SimpleDateFormat("dd");
        DateFormat dateFormatHora = new SimpleDateFormat("HH");

        String Carpeta = dateFormat.format(fecha);

        int Anio = Globales.Validarintnonull(dateFormatAnio.format(fecha));
        int Dia = Globales.Validarintnonull(dateFormatDia.format(fecha));
        int hora = Globales.Validarintnonull(dateFormatHora.format(fecha));

        File origen;
        File destino;

        String dia_respaldo;

        String mensaje = "";

        try {

            for (int x = 0; x < archivos.length; x++) {

                sql = "SELECT id, ruta, estado, horario, dias, nombrerespaldo \n"
                        + " FROM public.empresa_rutarespaldo "
                        + " WHERE id = " + archivos[x] + " order by 1";
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->RespaldarArchivos", 1);
                datos.next();
                ruta = datos.getString("ruta");
                estado = datos.getString("estado");
                horario = datos.getString("horario");
                dias = datos.getString("dias");
                nombre = datos.getString("nombrerespaldo");

                ruta = ruta.replaceAll("\\[ANIO]", String.valueOf(Anio));
                ruta = ruta.replaceAll("\\[ANIO-1]", String.valueOf((Anio - 1)));

                nombre = nombre.replaceAll("\\[ANIO]", String.valueOf(Anio));
                nombre = nombre.replaceAll("\\[ANIO-1]", String.valueOf((Anio - 1)));

                //nombre += ".zip";
                unidad = unidad.replace("\\", "/");

                destino = new File(unidad + "Respaldo_SOM/" + Carpeta);
                if (!destino.exists()) {
                    destino.mkdirs();
                }

                origen = new File(ruta_respaldo + Carpeta);
                if (!origen.exists()) {
                    origen.mkdirs();
                }

                /*if (!estado.equals("Activo")) {
                    Globales.zipDirectory(new File(ruta), ruta_respaldo + Carpeta + "/" + nombre);
                    origen = new File(ruta_respaldo + Carpeta + "/" + nombre);
                    destino = new File(unidad + "Respaldo_SOM/" + Carpeta + "/" + nombre);
                    Files.copy(origen.toPath(), destino.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    mensaje += "Ruta " + ruta + " respaldada con éxito<br>";
                } else {
                    mensaje += "El respaldo de la ruta " + ruta + " está inactivo <br>";
                }*/
                if (!estado.equals("Activo")) {
                    Globales.copiarDirectorio(ruta, Globales.ruta_respaldo + Carpeta + "/" + nombre + "/");
                    Globales.copiarDirectorio(ruta, unidad + "Respaldo_SOM/" + Carpeta + "/" + nombre + "/");
                    mensaje += "Ruta " + ruta + " respaldada con éxito<br>";
                } else {
                    mensaje += "El respaldo de la ruta " + ruta + " está inactivo <br>";
                }

            }

            return "0||<b>Resultado del respaldo</b><br>" + mensaje;

        } catch (Exception ex) {
            return "1||" + ex.getMessage();
        }

    }

    public String Estado_VersionDocumento(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));

        sql = "select id from  version_documento where id = " + id;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->Estado_VersionDocumento");
    }

    public String FechaEjeVuelta() {

        DateFormat formato = new SimpleDateFormat("HH");
        DateFormat formatofecha = new SimpleDateFormat("yyyy-MM-dd");

        Date nfecha = new Date();
        Date fecha2 = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(nfecha);

        if (Globales.DiaSemana(nfecha) == 6) {
            calendar.add(Calendar.DAY_OF_YEAR, (3));
        } else {
            calendar.add(Calendar.DAY_OF_YEAR, (1));
        }

        nfecha = calendar.getTime();

        int hora = Globales.Validarintnonull(formato.format(nfecha));
        if (hora >= 15 && Globales.DiaSemana(fecha2) != 6) {
            calendar.add(Calendar.DAY_OF_YEAR, (1));
        }

        nfecha = calendar.getTime();

        if (Globales.DiaSemana(nfecha) == 7) {
            calendar.add(Calendar.DAY_OF_YEAR, (2));
        }

        nfecha = calendar.getTime();

        int festivo = 1;
        do {
            if (Globales.DiaSemana(nfecha) == 1) {
                calendar.add(Calendar.DAY_OF_YEAR, (1));
                nfecha = calendar.getTime();
            } else {
                sql = "SELECT id from festivos where dia = '" + formatofecha.format(nfecha) + "'";
                festivo = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                if (festivo > 0) {
                    calendar.add(Calendar.DAY_OF_YEAR, (1));
                    nfecha = calendar.getTime();
                }
            }
        } while (festivo > 0);

        return formatofecha.format(nfecha);
    }

    public String TablasVuelta(HttpServletRequest request) {
        String fechaini = request.getParameter("fechaini");
        String fechafin = request.getParameter("fechafin");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String estado = request.getParameter("estado");
        String fechapro = request.getParameter("fechapro");
        String busqueda = "WHERE 1 = 1 ";
        if (usuarios > 0) {
            busqueda += " and idusuario = " + usuarios;
        }
        if (!estado.equals("TODAS")) {
            busqueda += " AND v.estado = '" + estado + "'";
        }
        if (!fechapro.equals("")) {
            busqueda += " AND to_char(fechapro,'yyyy-MM-dd') = '" + fechapro + "'";
        } else {
            busqueda += " and to_char(fecha, 'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fecha,'yyyy-MM-dd') <= '" + fechafin + "'";
        }

        sql = "select v.id, to_char(fecha, 'yyyy/MM/dd') as fecha, actividad, v.estado, u.nombrecompleto as usuario, v.tipo, v.idcliente,"
                + "      case when v.tipo = 'CLIENTE' then (SELECT nombrecompleto FROM clientes c where c.id = v.idcliente) else"
                + "          (SELECT nombrecompleto FROM proveedores p where p.id = v.idcliente) end as cliente, tipovuelta, ingresos,"
                + "      contacto, v.direccion, "
                + "      case when v.fotos > 0 then '<img src=''" + url_archivo + "Adjunto/imagenes/FotoRecogida/' || case when idcliente > 0 then '1' else '2' end  || '/' ||  v.id || '/1.jpg'' onclick=''LlamarFotoDet(' || v.id || ',' || v.fotos || ',' || case when idcliente > 0 then '1' else '2' end || ')'' width=''80px''/><br>' else '' end as imagen,"
                + "      to_char(fechaasi, 'yyyy/MM/dd') || '<br>' || u3.nombrecompleto as asignado,"
                + "          to_char(fechapro, 'yyyy/MM/dd') as fechapro, ejecutado || '<br>' || to_char(fechaeje, 'yyyy/MM/dd') || '<br>' || u2.nombrecompleto as ejecutado,"
                + "          to_char(fechaapro, 'yyyy/MM/dd') || '<br>' || u1.nombrecompleto as aprobado, observacion,"
                + "      tiempo(v.fechapro, case when idusuaeje = 0 then now() else fechaeje end, 4) as tiempo,"
                + "      case when v.estado = 'POR APROBAR' THEN"
                + "          '<button class=''btn btn-glow btn-success'' title=''Aprobar Vuelta'' type=''button'' onclick=' || chr(34) || 'AprobarVuelta(' || v.id || ')' ||chr(34) || '><span data-icon=''&#xe147;''></span></button>' || '<br>' ||"
                + "          '<button class=''btn btn-glow btn-danger'' title=''Rechazar Vuelta'' type=''button'' onclick=' || chr(34) || 'RechazarVuelta(' || v.id || ')' ||chr(34) || '><span data-icon=''&#xe147;''></span></button>' ELSE '' END ||"
                + "      case when v.estado = 'ASIGNADA' OR v.estado = 'NO EJECUTADA' THEN "
                + "      '<button class=''btn btn-glow btn-info imprimir'' title=''Ejecutar Vuelta'' type=''button'' onclick=' || chr(34) || 'EjecutarVuelta(' || v.id || ',''' || case when v.tipo = 'CLIENTE' then (SELECT nombrecompleto FROM clientes c where c.id = v.idcliente) else "
                + "          (SELECT nombrecompleto FROM proveedores p where p.id = v.idcliente) end || ''')' ||chr(34) || '><span data-icon=''&#xe2aa;''></span></button>' ELSE '' END"
                + "         || CASE when v.estado = 'AUTORIZADA' then "
                + "     '<button class=''btn btn-glow btn-primary imprimir'' title=''Asignar Vuelta '' type=''button'' onclick=' || chr(34) || 'AsignarVuelta(' || v.id || ',''' || case when v.tipo = 'CLIENTE' then (SELECT nombrecompleto FROM clientes c where c.id = v.idcliente) else "
                + "         (SELECT nombrecompleto FROM proveedores p where p.id = v.idcliente) end || ''')' ||chr(34) || '><span data-icon=''&#xe147;''></span></button>' ELSE '' END as opcion"
                + "    from registrovueltas v inner join seguridad.rbac_usuario u on v.idusuario = u.idusu"
                + "	                               inner join seguridad.rbac_usuario u1 on v.idusuaprobo = u1.idusu"
                + "                           inner join seguridad.rbac_usuario u3 on v.idusuarioasi = u3.idusu"
                + "                           inner join seguridad.rbac_usuario u2 on v.idusuaeje = u2.idusu " + busqueda + " ORDER BY v.id desc";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablasVuelta");
    }

    public String AutoComplete(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String tipo = request.getParameter("tipo");
        try {
            if (tipo.equals("CLIENTE")) {
                sql = "select nombres || ' (' || coalesce(telefonos,'') || '/' || coalesce(fax,'') || ')' as nombres from clientes_contac where idcliente = " + cliente + " ORDER BY 1";
                String sql2 = "select direccion from clientes_sede where idcliente = " + cliente + " ORDER BY 1";
                String sql3 = "select '<option value=''' || ingreso || '''>' || ingreso || '</option>' "
                        + "  from remision_detalle rd"
                        + "  WHERE salida = 0  and ((select count(ingreso) from ingreso_plantilla ip "
                        + "  where rd.ingreso = ip.ingreso and certificado = 0 and informetecnico = 0 and noautorizado > 0) = 0 or informetercerizado <> '0' or certificados_externos >0) and idcliente = " + cliente + " ORDER BY 1";
                String sql4 = "SELECT nombrecompleto as cliente, emailcartera, plazopago, tp.descripcion as tipo "
                        + "  FROM clientes c inner join tipocliente tp on tp.id = idtipcli WHERE c.id = " + cliente;
                return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->AutoComplete") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->AutoComplete") + "|" + Globales.ObtenerDatosJSon(sql3, this.getClass() + "-->AutoComplete") + "|" + Globales.ObtenerDatosJSon(sql4, this.getClass() + "-->AutoComplete");
            } else {
                sql = "SELECT direccion, contacto FROM proveedores WHERE id = " + cliente;
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AutoComplete", 1);
                String info1 = "", info2 = "";
                if (datos.next()) {
                    info1 = datos.getString(1);
                    info2 = datos.getString(2);
                }
                return info2 + "|" + info1;
            }
        } catch (SQLException ex) {
            return ex.getMessage();
        }

    }

    public String ValidarIngreso(HttpServletRequest request) {
        try {
            String ingreso = request.getParameter("ingreso");

            sql = "SELECT ingreso FROM remision_detalle WHERE recibidoing = 0 and ingreso in (" + ingreso + ")";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->ValidarIngreso", 1);
            ingreso = "";
            while (datos.next()) {
                if (ingreso.equals("")) {
                    ingreso = datos.getString(1);
                } else {
                    ingreso += "," + datos.getString(1);
                }
            }
            if (ingreso.equals("")) {
                ingreso = "XX";
            }
            return ingreso;
        } catch (SQLException ex) {
            Globales.GuardarLogger(ex.getMessage(), this.getClass() + "-->ValidarIngreso");
            return "XX";
        }
    }

    public String OperacionVuelta(HttpServletRequest request) {
        int operacion = Globales.Validarintnonull(request.getParameter("operacion"));
        String fecha = request.getParameter("fecha");
        String actividad = (request.getParameter("actividad"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String ejecutado = (request.getParameter("ejecutado"));
        String observacion = (request.getParameter("observacion"));
        String contacto = (request.getParameter("contacto"));
        String direccion = (request.getParameter("direccion"));
        String tipovuelta = (request.getParameter("tipovuelta"));
        String ingresos = (request.getParameter("ingresos"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String tipo = (request.getParameter("tipo"));
        int asignacion = Globales.Validarintnonull(request.getParameter("asignacion"));

        String resultado = "";

        Date nfecha = Globales.ValidarFecha(fecha);

        try {
            switch (operacion) {
                case 1:
                    int vuelta = 0;
                    if (Globales.PermisosSistemas("VUELTAS SABADOS Y DOMINGOS", idusuario) == 0) {
                        if (Globales.DiaSemana(nfecha) == 1 || Globales.DiaSemana(nfecha) == 7) {
                            return "1|No se puede programar una diligencia para los días domingo o sábado";
                        }
                    }

                    sql = "SELECT max(contador), max(to_char(fechapdf,'yyyy-MM-dd HH24:MI')) FROM registrovueltas "
                            + "  WHERE to_char(fechapro, 'yyyy-MM-dd') = '" + fecha + "'";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->OperacionVuelta", 1);
                    String fechapdf = "";
                    if (datos.next()) {
                        vuelta = Globales.Validarintnonull(datos.getString(1));
                        fechapdf = datos.getString(2);
                    } else {
                        vuelta = 0;
                    }

                    if (fechapdf == null) {
                        fechapdf = "NULL";
                    } else {
                        fechapdf = "'" + fechapdf + "'";
                    }
                    if (vuelta > 0) {
                        if (tipovuelta.equals("ENTREGAR EQUIPO")) {
                            if (Globales.PermisosSistemas("VUELTAS ENTREGAR EQUIPO", idusuario) == 0) {
                                return "1|Ya las diligencias se cerraron para esta fecha";
                            }
                        } else {
                            if (Globales.PermisosSistemas("VUELTAS CERRADAS", idusuario) == 0) {
                                return "1|Ya las diligencias se cerraron para esta fecha";
                            }
                        }
                    }
                    int permisofecha = Globales.PermisosSistemas("VUELTAS FECHA ACTUAL", idusuario);
                    if (permisofecha == 0) {
                        String fechaeje = FechaEjeVuelta();
                        if (fechaeje.compareTo(fecha) > 0) {
                            return "1|La fecha no puede menor a " + fechaeje;
                        }
                    }
                    if (id == 0) {
                        sql = "INSERT INTO registrovueltas(idusuario, idcliente, contacto, direccion, actividad, fechapro, tipo, contador, tipovuelta, fechapdf, ingresos) "
                                + "VALUES (" + idusuario + "," + cliente + ",'" + contacto + "','" + direccion + "','" + actividad + "','" + fecha + "','" + tipo + "'," + vuelta + ",'" + tipovuelta + "'," + fechapdf + ",'" + ingresos + "')";
                        resultado = "0|Vuelta registrada con éxito";
                        if (Globales.PermisosSistemas("VUELTAS GUARDAR", idusuario) == 0) {
                            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                        }
                    } else {
                        if (Globales.PermisosSistemas("VUELTAS EDITAR", idusuario) == 0) {
                            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                        }
                        sql = "SELECT estado from registrovueltas where id = " + id;
                        String estado = Globales.ObtenerUnValor(sql);
                        if (!estado.equals("POR APROBAR")) {
                            return "1|No se puede editar una vuelta con estado " + estado;
                        }
                        sql = "UPDATE  registrovueltas SET idusuario = " + idusuario + ", actividad='" + actividad + "', fechapro='" + fecha + "',ingresos='" + ingresos + "',idcliente = " + cliente + ", contacto='" + contacto + "', direccion='" + direccion + "', tipo='" + tipo + "',tipovuelta='" + tipovuelta + "' WHERE id = " + id;

                    }

                    if (Globales.DatosAuditoria(sql, "VUELTA", "ACTUALIZAR", idusuario, iplocal, this.getClass() + "-->OperacionVuelta")) {
                        resultado = "0|Vuelta actualizada con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }

                    break;
                case 2:
                    if (Globales.PermisosSistemas("VUELTAS APROBAR", idusuario) == 0) {
                        return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    sql = "UPDATE registrovueltas SET estado ='AUTORIZADA', fechaapro = now(), idusuaprobo= " + idusuario + ", observacion='" + observacion + "' WHERE id = " + id;

                    if (Globales.DatosAuditoria(sql, "VUELTA", "APROBAR", idusuario, iplocal, this.getClass() + "-->OperacionVuelta")) {
                        resultado = "0|Vuelta Autorizada";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }

                    break;
                case 3:
                    if (Globales.PermisosSistemas("VUELTAS RECHAZAR", idusuario) == 0) {
                        return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    sql = "UPDATE registrovueltas SET estado ='RECHAZADA', fechaapro = now(), observacion='" + observacion + "', idusuaprobo= " + idusuario + " WHERE id = " + id;

                    if (Globales.DatosAuditoria(sql, "VUELTA", "RECHAZAR", idusuario, iplocal, this.getClass() + "-->OperacionVuelta")) {
                        resultado = "0|Vuelta Rechazada";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }

                    break;
                case 4:
                    sql = "SELECT idusuarioasi FROM registrovueltas where id = " + id;
                    String usuarioasig = Globales.ObtenerUnValor(sql).trim();
                    if (Globales.PermisosSistemas("VUELTAS EJECUTAR OTRO USUARIO", idusuario) == 0) {
                        if (!usuarioasig.equals(idusuario)) {
                            return "1|El usuario que ejecuta las vueltas debe ser el usuario que se le asignó las vueltas";
                        }
                    }
                    if (Globales.PermisosSistemas("VUELTAS EJECUTAR", idusuario) == 0) {
                        return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    if (ejecutado.equals("SI")) {
                        sql = "UPDATE registrovueltas SET estado ='EJECUTADA',  fechaeje = now(), idusuaeje= " + idusuario + ", ejecutado='SI', observacion='" + observacion + "' WHERE id = " + id;
                        resultado = "0|Vuelta Ejecutada";
                    } else {
                        if (ejecutado.equals("RECHAZADA")) {
                            sql = "UPDATE registrovueltas SET estado ='NO EJECUTADA - RECHAZADA', fechaeje = now(), idusuaeje= " + idusuario + ", ejecutado='RECHAZADA', observacion='" + observacion + "' WHERE id = " + id;
                            resultado = "0|Vuelta RECHAZADA";
                        } else {
                            sql = "UPDATE registrovueltas SET estado ='NO EJECUTADA', fechaeje = now(), idusuaeje= " + idusuario + ", ejecutado='NO', observacion='" + observacion + "' WHERE id = " + id;

                        }
                    }

                    if (Globales.DatosAuditoria(sql, "VUELTA", "NO EJECUTAR", idusuario, iplocal, this.getClass() + "-->OperacionVuelta")) {
                        resultado = "0|Vuelta NO Ejecutada";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }

                    break;
                case 5:
                    if (Globales.PermisosSistemas("VUELTAS ASIGNAR", idusuario) == 0) {
                        return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }

                    sql = "UPDATE registrovueltas SET estado ='ASIGNADA', fechaasi = now(), idusuarioasi =" + asignacion + "  WHERE id = " + id;

                    if (Globales.DatosAuditoria(sql, "VUELTA", "ASIGNAR", idusuario, iplocal, this.getClass() + "-->OperacionVuelta")) {
                        resultado = "0|Vuelta asignada con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }

                    break;
            }
            return resultado;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String DescripcionVuelta(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        sql = "select  actividad || '||' || coalesce(observacion,'') from registrovueltas where id = " + id;
        return Globales.ObtenerUnValor(sql);
    }

    public String EnviarClaveContac(HttpServletRequest request) {
        int contacto = Globales.Validarintnonull(request.getParameter("contacto"));
        try {
            if (Globales.PermisosSistemas("CLIENTE ENVIAR CLAVE", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            int error = 0;

            sql = "SELECT clavecontac, nombrecompleto as cliente, cc.email, nombres "
                    + " from clientes c inner join clientes_contac cc on c.id = cc.idcliente "
                    + " where cc.id = " + contacto;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "->EnviarClaveContac", 1);
            datos.next();
            String ClaveAcceso = datos.getString("clavecontac");
            String cliente = datos.getString("cliente");
            String nomcontacto = datos.getString("nombres");
            String correocliente = datos.getString("email");

            String nomusuario = Globales.ObtenerUnValor("select nombrecompleto FROM seguridad.rbac_usuario where idusu=" + idusuario);
            String cargo = Globales.ObtenerUnValor("select cargo FROM seguridad.rbac_usuario where idusu=" + idusuario);

            String e_email = correoenvio;
            String[] email = e_email.split(";");

            String correoemp = correoenvio;

            DateFormat formato = new SimpleDateFormat("HH");
            DateFormat formatomes = new SimpleDateFormat("MM");
            DateFormat formatoanio = new SimpleDateFormat("yyyy");

            int hora = Globales.Validarintnonull(formato.format(new Date()));
            String saludo = "";
            if (hora <= 11) {
                saludo = "Buenos días";
            } else if (hora <= 18) {
                saludo = "Buenas tardes";
            } else {
                saludo = "Buenas noches";
            }

            String nombrearchivo = "";

            if ((correoemp.trim().equals("")) || (clavecorreo.trim().equals(""))) {
                return "1|Debe configurar los parámetros: Correo envio, Clave correo envio";
            }

            String mensaje = saludo + "<br><br>"
                    + "<b>SEÑORES</b><br>" + cliente + "<br>"
                    + "<b>CONTACTO:</b><br> " + nomcontacto + "<br><br>"
                    + "Reciba un cordial saludo por parte de nuestro equipo de trabajo"
                    + (Globales.Validarintnonull(formatomes.format(new Date())) >= 11 ? ", esperamos que las metas planteadas para este año " + formatoanio.format(new Date()) + " se hayan cumplido satisfactoriamente." : ".") + "<br>"
                    + "<br><br>Se envía los datos de acceso de su cuenta de <b>CALIBRATION SERVICE SAS</b>  a través del link <br>"
                    + "<a href='http://sof.calibrationservicesas.com:8090/usuarios/login.php?usuario=" + correocliente + "&clave=" + Base64.getEncoder().withoutPadding().encodeToString(ClaveAcceso.getBytes()) + "'>http://sof.calibrationservicesas.com:8090/usuarios/login.php?usuario=" + correocliente + "&clave=" + Base64.getEncoder().withoutPadding().encodeToString(ClaveAcceso.getBytes()) + "</a><br><br>"
                    + "Sus datos para ingresar son los siguientes: <br><b>usuario</b> " + correocliente
                    + "<br><b>Clave de acceso</b> " + ClaveAcceso + "<br><br>"
                    + (Globales.Validarintnonull(formatomes.format(new Date())) >= 11 ? "Les deseamos unas felices fiestas, una feliz navidad y un próspero año nuevo." : "");

            mensaje += "<br><br>Cordialmente<br><br>";

            mensaje += "<img src='http://www.calibrationservicesas.com/images/logo_320.png' width='30%'><br><b>" + nomusuario + "</b><br>Calibration Service SAS<br>" + cargo + "<br><font color='blue'>+57 (1) 2047699 - 7285146 <br>3138141058</font><br><br>";

            String adjunto = "";

            String correo = Globales.EnviarEmail(correocliente, Globales.Nombre_Empresa, email, "Envío de Control de Acceso CALIBRATION SERVICE SAS ", mensaje, correoemp, adjunto, correoemp, clavecorreo, nombrearchivo);
            if (!correo.trim().equals("")) {
                return "1|" + correo;
            }

            return error + "|Correo enviado a " + correocliente;
        } catch (SQLException ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String TablaSistema(HttpServletRequest request) {
        String tabla = request.getParameter("tabla");
        String Resultado = "";
        int[] tipo = new int[50];
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date fecha;
        int X = 0;
        int pos = 0;
        try {

            String esquema = Globales.ObtenerUnValor("select esquema from tablas where tabla = '" + tabla + "'");

            sql = "SELECT column_name, data_type FROM information_schema.columns "
                    + " WHERE table_schema = '" + esquema + "' AND table_name = '" + tabla + "'";

            datos = Globales.Obtenerdatos(sql, this.getClass() + "->TablaSistema", 1);
            Resultado = "<table class='table table-bordered hover' id='TablaGeneral'><thead><tr>";
            while (datos.next()) {
                if (datos.getString("data_type").equals("date")) {
                    tipo[pos] = 1;
                } else {
                    tipo[pos] = 0;
                }
                Resultado += "<th>" + datos.getString("column_name") + "</th>";
                pos++;
                X++;
            }
            Resultado += "</tr></thead>";
            sql = "SELECT * FROM " + esquema + "." + tabla + " order by 1";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "->TablaSistema", 1);
            Resultado += "<tbody>";
            String nomid = "";
            pos = 0;
            while (datos.next()) {
                Resultado += "<tr>";
                for (int fila = 1; fila <= X; fila++) {
                    nomid = "";
                    if (fila == 0) {
                        nomid = " id='fila-" + pos + "'";
                    }
                    if (tipo[(fila - 1)] == 0) {
                        Resultado += "<td" + nomid + ">" + datos.getString(fila) + "</td>";
                    } else {
                        fecha = Globales.ValidarFecha(datos.getString(fila));
                        Resultado += "<td" + nomid + ">" + dateFormat.format(fecha) + "</td>";
                    }
                }
                Resultado += "</tr>";
            }
            Resultado += "</tbody></table>";
            return "0|" + Resultado;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String ActualizarDatosTabla(HttpServletRequest request) {
        String id = request.getParameter("id").trim();
        String tabla = request.getParameter("tabla");
        String registros = request.getParameter("registros");
        int X = 0;
        int Pos = 0;

        String[] a_registro = registros.split(",");

        try {

            String esquema = Globales.ObtenerUnValor("select esquema from tablas where tabla = '" + tabla + "'");

            sql = "SELECT column_name "
                    + " FROM information_schema.columns "
                    + " WHERE table_schema = '" + esquema + "' "
                    + " AND table_name = '" + tabla + "' and column_name <> 'id'";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "->ActualizarDatosTabla", 1);
            sql = "SELECT count(id) from " + esquema + "." + tabla + " WHERE id = " + id;
            int cantidad = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (cantidad == 0) {
                id = "";
            }
            if (id.equals("") && cantidad == 0) {
                if (Globales.PermisosSistemas("TABLAS DEL SISTEMA AGREGAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO " + esquema + "." + tabla + "(";
                while (datos.next()) {
                    if (Pos != 0) {
                        sql += ",";
                    }
                    sql += datos.getString(1);
                    Pos++;
                    X++;
                }
                sql += ") VALUES (";
                for (int x = 0; x < a_registro.length; x++) {
                    if (x != 0) {
                        sql += ",";
                    }
                    sql += "'" + Globales.neescape(a_registro[x]) + "'";
                }
                sql += ")";
                if (Globales.DatosAuditoria(sql, "TABLAS DEL SISTEMA", "AGREGAR", idusuario, iplocal, this.getClass() + "->ActualizarDatosTabla")) {
                    sql = "select max(id) from " + esquema + "." + tabla;
                    id = Globales.ObtenerUnValor(sql).trim();
                    return "0|Registro agregado con éxito|" + id;
                } else {
                    return "1|Error inesperado.... Revise los archivos de log";
                }
            } else {
                if (Globales.PermisosSistemas("TABLAS DEL SISTEMA EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE " + esquema + "." + tabla + " SET ";
                while (datos.next()) {
                    if (Pos != 0) {
                        sql += ",";
                    }
                    sql += datos.getString(1) + "='" + Globales.neescape(a_registro[Pos]) + "'";
                    Pos++;
                }
                sql += " where id = " + id;
                if (Globales.DatosAuditoria(sql, "TABLAS DEL SISTEMA", "AGREGAR", idusuario, iplocal, this.getClass() + "->ActualizarDatosTabla")) {
                    return "0|Registro actualizado con éxito|" + id;
                } else {
                    return "1|Error inesperado.... Revise los archivos de log";
                }
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarDatosTabla(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String tabla = request.getParameter("tabla");

        if (Globales.PermisosSistemas("TABLAS DEL SISTEMA ELIMINAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        try {
            sql = "DELETE FROM " + tabla + " WHERE id = " + id;
            if (Globales.DatosAuditoria(sql, "TABLAS DEL SISTEMA", "ELIMINAR", idusuario, iplocal, this.getClass() + "->EliminarDatosTabla")) {
                return "0|Registro Eliminado con éxito";
            } else {
                return "1|Error inesperado.... Revise los archivos de log";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public int TablaPrecio_Moneda(HttpServletRequest request) {
        int moneda = Globales.Validarintnonull(request.getParameter("moneda"));
        return Globales.Validarintnonull(Globales.ObtenerUnValor("select tipoprecio from tipo_moneda where id = " + moneda));
    }

    public String Simbolo_Moneda(HttpServletRequest request) {
        int moneda = Globales.Validarintnonull(request.getParameter("moneda"));
        return Globales.ObtenerUnValor("select simbolo from tipo_moneda where id = " + moneda);
    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setContentType("text/html;charset=UTF-8");
            String ruta_origen = request.getHeader("Origin");
            if (ruta_origen == null) {
                ruta_origen = "http://localhost:8090";
            }

            HttpSession misession = request.getSession();
            idusuario = misession.getAttribute("idusuario").toString();
            iplocal = misession.getAttribute("IPLocal").toString();
            usuario = misession.getAttribute("codusu").toString();
            correoenvio = misession.getAttribute("correoenvio").toString();
            clavecorreo = misession.getAttribute("clavecorreo").toString();
            if (misession.getAttribute("ArchivosAdjuntos") != null) {
                archivoadjunto = misession.getAttribute("ArchivosAdjuntos").toString();
            }
            response.setHeader("Access-Control-Allow-Methods", "GET, POST");
            response.setHeader("Access-Control-Allow-Credentials", "true");
            response.setHeader("Access-Control-Allow-Headers", "Origin");
            response.setHeader("Access-Control-Expose-Headers", "Origin");
            response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));

            PrintWriter out = response.getWriter();
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else if (misession.getAttribute("codusu") == null) {
                out.print("cerrada");
            } else if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                out.print("cambio-ip");
            } else {
                String opcion = request.getParameter("opcion");
                switch (opcion) {
                    case "DatosEmpresa":
                        out.print(DatosEmpresa());
                        break;
                    case "EventosCalendarios":
                        out.print(EventosCalendarios(request));
                        break;
                    case "CargarCombos":
                        out.print(CargarCombos(request, null, null, null, null, 0));
                        break;
                    case "CargaComboInicial":
                        out.print(CargaComboInicial(request));
                        break;
                    case "ObtenerChat":
                        out.print(ObtenerChat());
                        break;
                    case "AlertaVueltas":
                        out.print(AlertaVueltas());
                        break;
                    case "TablaConfiguracion":
                        out.print(TablaConfiguracion(request));
                        break;
                    case "GuardarPrecios":
                        out.print(GuardarPrecios(request));
                        break;
                    case "TablaSede":
                        out.print(TablaSede(request));
                        break;
                    case "TablaContacto":
                        out.print(TablaContacto(request));
                        break;
                    case "GuardarCliente":
                        out.print(GuardarCliente(request));
                        break;
                    case "DocumentosCliente":
                        out.print(DocumentosCliente(request));
                        break;
                    case "GuardarDocumentosCliente":
                        out.print(GuardarDocumentosCliente(request));
                        break;
                    case "BuscarSede":
                        out.print(BuscarSede(request));
                        break;
                    case "BuscarContacto":
                        out.print(BuscarContacto(request));
                        break;
                    case "GuardarSede":
                        out.print(GuardarSede(request));
                        break;
                    case "GuardarContacto":
                        out.print(GuardarContacto(request));
                        break;
                    case "BuscarPermisosContac":
                        out.print(BuscarPermisosContac(request));
                        break;
                    case "AsignarPermisos":
                        out.print(AsignarPermisos(request));
                        break;
                    case "EliminarContacto":
                        out.print(EliminarContacto(request));
                        break;
                    case "ConsultarClientes":
                        out.print(ConsultarClientes(request));
                        break;
                    case "BuscarCliente":
                        out.print(BuscarCliente(request));
                        break;
                    case "CambioNit":
                        out.print(CambioNit(request));
                        break;
                    case "BuscarProveedor":
                        out.print(BuscarProveedor(request));
                        break;
                    case "GuardarProveedor":
                        out.print(GuardarProveedor(request));
                        break;
                    case "ConsultarProveedores":
                        out.print(ConsultarProveedores(request));
                        break;
                    case "DescripcionPrecios":
                        out.print(DescripcionPrecios());
                        break;
                    case "TablaPrecios":
                        out.print(TablaPrecios(request));
                        break;
                    case "EliminarFotoPrecio":
                        out.print(EliminarFotoPrecio(request));
                        break;
                    case "EliminarPrecio":
                        out.print(EliminarPrecio(request));
                        break;
                    case "GuardarMagnitudes":
                        out.print(GuardarMagnitudes(request));
                        break;
                    case "MaximoServicio":
                        out.print(MaximoServicio());
                        break;
                    case "GuardarEquipos":
                        out.print(GuardarEquipos(request));
                        break;
                    case "GuardarServicio":
                        out.print(GuardarServicio(request));
                        break;
                    case "EliminarServicio":
                        out.print(EliminarServicio(request));
                        break;
                    case "GuardarOtroServicio":
                        out.print(GuardarOtroServicio(request));
                        break;
                    case "EliminarOtroServicio":
                        out.print(EliminarOtroServicio(request));
                        break;
                    case "GuardarMedidas":
                        out.print(GuardarMedidas(request));
                        break;
                    case "EliminarMedida":
                        out.print(EliminarMedida(request));
                        break;
                    case "GuardarIntervalos":
                        out.print(GuardarIntervalos(request));
                        break;
                    case "EliminarIntervalo":
                        out.print(EliminarIntervalo(request));
                        break;
                    case "GuardarMarcas":
                        out.print(GuardarMarcas(request));
                        break;
                    case "EliminarMarcas":
                        out.print(EliminarMarcas(request));
                        break;
                    case "EliminarModelos":
                        out.print(EliminarModelos(request));
                        break;
                    case "GuardarModelos":
                        out.print(GuardarModelos(request));
                        break;
                    case "GuardarMetodo":
                        out.print(GuardarMetodo(request));
                        break;
                    case "EliminarMetodo":
                        out.print(EliminarMetodo(request));
                        break;
                    case "GuardarProcedimiento":
                        out.print(GuardarProcedimiento(request));
                        break;
                    case "GuardarPlantillas":
                        out.print(GuardarPlantillas(request));
                        break;
                    case "FechaServidor":
                        out.print(FechaServidor(request));
                        break;
                    case "GenerarPDF":
                        out.print(GenerarPDF(request));
                        break;
                    case "SubirFoto":
                        out.print(SubirFoto(request));
                        break;
                    case "SubirFotoMultiple":
                        out.print(SubirFotoMultiple(request));
                        break;
                    case "CentralTelefonica":
                        out.print(CentralTelefonica());
                        break;
                    case "TablaRespaldo":
                        out.print(TablaRespaldo());
                        break;
                    case "UnidadesDisco":
                        out.print(UnidadesDisco());
                        break;
                    case "RespaldarArchivos":
                        out.print(RespaldarArchivos(request));
                        break;
                    case "Estado_VersionDocumento":
                        out.print(Estado_VersionDocumento(request));
                        break;
                    case "FechaEjeVuelta":
                        out.print(FechaEjeVuelta());
                        break;
                    case "TablasVuelta":
                        out.print(TablasVuelta(request));
                        break;
                    case "AutoComplete":
                        out.print(AutoComplete(request));
                        break;
                    case "ValidarIngreso":
                        out.print(ValidarIngreso(request));
                        break;
                    case "OperacionVuelta":
                        out.print(OperacionVuelta(request));
                        break;
                    case "DescripcionVuelta":
                        out.print(DescripcionVuelta(request));
                        break;
                    case "EnviarClaveContac":
                        out.print(EnviarClaveContac(request));
                        break;
                    case "GuardarLeyenda":
                        out.print(GuardarLeyenda(request));
                        break;
                    case "EliminarLeyenda":
                        out.print(EliminarLeyenda(request));
                        break;
                    case "TablaSistema":
                        out.print(TablaSistema(request));
                        break;
                    case "ActualizarDatosTabla":
                        out.print(ActualizarDatosTabla(request));
                        break;
                    case "EliminarDatosTabla":
                        out.print(EliminarDatosTabla(request));
                        break;
                    case "TablaPrecio_Moneda":
                        out.print(TablaPrecio_Moneda(request));
                        break;
                    case "Simbolo_Moneda":
                        out.print(Simbolo_Moneda(request));
                        break;
                    default:
                        throw new AssertionError();
                }
            }
        } catch (Exception ex) {
            try {
                PrintWriter out = response.getWriter();
                out.print(ex.getMessage());

            } catch (IOException exl) {
                Globales.GuardarLogger(exl.getMessage(), Laboratorio.class
                        .getName());
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

}
