﻿var IdArticulo = 0;
var IdSede = 0;
var IdContacto = 0;
var ReferenciaArt = "";
var OpcionEquipo = 0;
var NumFoto = 0;
var TableDependencia = null;
var TablePrecio = null;

//CargarCombo(68, 1,"", idusuariochat)

function ActualizarCombos(){
	ActivarLoad();
    setTimeout(function () {
		$("#Grupo, #CGrupo, #MGrupo, #PGrupo").html(CargarCombo(47, 1));
		$("#Equipo, #CEquipo, #PEquipo").html(CargarCombo(48, 1,"","-1"));
		$("#CModelo, #PModelo").html(CargarCombo(100, 1));
		$("#MUbicacion").html("<option value='0'>Todas</option>");
		$("#Marca, #CMarca, #PMarca").html(CargarCombo(49, 1));
		$("#Proveedor, #CProveedor, #PProveedor").html(CargarCombo(7, 1));
		$("#Iva, #IvaUSD, #PIva").html(CargarCombo(35, 0));
		$("#CuentaContable").html(CargarCombo(83, 1));
		$("#Presentacion, #CPresentacion, #PPresentacion").html(CargarCombo(98, 1));
		$("#Caracteristica, #CCaracteristica, #PCaracteristica").html(CargarCombo(99, 7));
		DesactivarLoad();
	},15);
}

ActualizarCombos();

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Referencia") {
            if ($.trim($(this).val()) != "") {

                BuscarCliente($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function ValidarArticulo(Caja) {
    Referencia = Caja.value;
    if ($.trim(ReferenciaArt) != "") {
        if (ReferenciaArt != Referencia) {
            ReferenciaArt = "";
            LimpiarDatos();
        }
    }
}

function LimpiarTodo() {
    $("#Referencia").val("");
    ReferenciaArt = "";
    LimpiarDatos();
}


function LimpiarDatos() {
    IdArticulo = 0;
    ReferenciaArt = "";
    $("#TipoInventario").val("").trigger("change");
    $("#Grupo").val("").trigger("change");
    $("#Proveedor").val("").trigger("change");
    $("#Presentacion").val("").trigger("change");
    $("#Caracteristica").val("").trigger("change");
    $("#Descripcion").val("");
    $("#LeyendaCotizacion").val("");
    $("#Marca").val("").trigger("change");
    $("#Existencia").val("0");
    $("#IdArticulo").val("0");
    $("#UltimaFactura").val("");
    $("#Fecha").val("");
    $("#CodigoBarras").val("");
    $("#CodigoInterno").val("");
    $("#UltimoCosto").val("0");
    $("#UltimoCostoUSD").val("0");
    $("#Estado").val("1").trigger("change");
    $("#Iva").val("1").trigger("change");
    $("#IvaUSD").val("1").trigger("change");
    $("#Catalogo").val("NO").trigger("change");
    $("#Referencia").val("SI").trigger("change");
    $("#CuentaContable").val("").trigger("change");
    $("#FichaTecnica").val("");
    $("#Manual").val("");
    $("#Garantia").val("0");

    for (var x = 1; x <= 10; x++) {
        $("#Precio" + x).val("0");
        $("#PrecioUSD" + x).val("0");
    }
    
    $("#FotoArticulo").attr("src", url_cliente + "Adjunto/imagenes/logo_articulo.png");
    NumFoto = 0;
    
    LimpiarDependencia();
    $('#TablaDependencia tbody').empty();
    
        
}

function BuscarArticulo(id) {

    LimpiarDatos();
    var parametros = "id=" + id;
    var datos = LlamarAjax("Inventarios","opcion=BuscarArticulos&"+ parametros);
    if (datos != "[]") {
        var data = JSON.parse(datos);

        IdArticulo = data[0].id;
        $("#TipoInventario").val(data[0].tipo).trigger("change");
        $("#Grupo").val(data[0].idgrupo).trigger("change");
        $("#Equipo").val(data[0].idequipo).trigger("change");
        $("#CodigoInterno").val(data[0].codigo);
		$("#Garantia").val(data[0].garantia_mes);
        $("#CodigoBarras").val(data[0].codigobarras);
        $("#Proveedor").val((data[0].proveedor * 1 == 0 ? "" : data[0].proveedor)).trigger("change");
        $("#Presentacion").val(data[0].idpresentacion).trigger("change");
        $("#Caracteristica").val(data[0].idcaracteristica).trigger("change");
        $("#Descripcion").val(data[0].descripcion);
        $("#Marca").val(data[0].idmarca).trigger("change");
        $("#Modelo").val(data[0].idmodelo).trigger("change");
        $("#Existencia").val(data[0].existencia);
        $("#IdArticulo").val(data[0].id);
        $("#UltimaFactura").val(data[0].ultimafactura);
        $("#Fecha").val(data[0].fechafac);
        $("#UltimoCosto").val(formato_numero(data[0].ultimocosto,0,".",",",""));
        $("#UltimoCostoUSD").val(formato_numero(data[0].ultimocostousd,0,".",",",""));
        $("#Estado").val(data[0].estado).trigger("change");
        $("#Iva").val(data[0].idiva).trigger("change");
        $("#IvaUSD").val(data[0].idivausd).trigger("change");
        $("#Catalogo").val(data[0].catalogo).trigger("change");
        $("#Referencia").val(data[0].referencia).trigger("change");
        $("#UsoInterno").val(data[0].uso_interno).trigger("change");
        $("#CuentaContable").val(data[0].idcuecontable > 0 ? data[0].idcuecontable : "").trigger("change");

        $("#Precio1").val(data[0].precio1*1 > 0 ?  formato_numero(data[0].precio1, 2,_CD,_CM,"") : "0");
        $("#Precio2").val(data[0].precio2 * 1 > 0 ? formato_numero(data[0].precio2, 2, _CD, _CM, "") : "0");
        $("#Precio3").val(data[0].precio3 * 1 > 0 ? formato_numero(data[0].precio3, 2, _CD, _CM, "") : "0");
        $("#Precio4").val(data[0].precio4 * 1 > 0 ? formato_numero(data[0].precio4, 2, _CD, _CM, "") : "0");
        $("#Precio5").val(data[0].precio5 * 1 > 0 ? formato_numero(data[0].precio5, 2, _CD, _CM, "") : "0");
        $("#Precio6").val(data[0].precio6 * 1 > 0 ? formato_numero(data[0].precio6, 2, _CD, _CM, "") : "0");
        $("#Precio7").val(data[0].precio7 * 1 > 0 ? formato_numero(data[0].precio7, 2, _CD, _CM, "") : "0");
        $("#Precio8").val(data[0].precio8 * 1 > 0 ? formato_numero(data[0].precio8, 2, _CD, _CM, "") : "0");
        $("#Precio9").val(data[0].precio9 * 1 > 0 ? formato_numero(data[0].precio9, 2, _CD, _CM, "") : "0");
        $("#Precio10").val(data[0].precio10 * 1 > 0 ? formato_numero(data[0].precio10, 2, _CD, _CM, "") : "0");
        
        $("#PrecioUSD1").val(data[0].preciousd1*1 > 0 ?  formato_numero(data[0].preciousd1, 2,_CD,_CM,"") : "0");
        $("#PrecioUSD2").val(data[0].preciousd2 * 1 > 0 ? formato_numero(data[0].preciousd2, 2, _CD, _CM, "") : "0");
        $("#PrecioUSD3").val(data[0].preciousd3 * 1 > 0 ? formato_numero(data[0].preciousd3, 2, _CD, _CM, "") : "0");
        $("#PrecioUSD4").val(data[0].preciousd4 * 1 > 0 ? formato_numero(data[0].preciousd4, 2, _CD, _CM, "") : "0");
        $("#PrecioUSD5").val(data[0].preciousd5 * 1 > 0 ? formato_numero(data[0].preciousd5, 2, _CD, _CM, "") : "0");
        $("#PrecioUSD6").val(data[0].preciousd6 * 1 > 0 ? formato_numero(data[0].preciousd6, 2, _CD, _CM, "") : "0");
        $("#PrecioUSD7").val(data[0].preciousd7 * 1 > 0 ? formato_numero(data[0].preciousd7, 2, _CD, _CM, "") : "0");
        $("#PrecioUSD8").val(data[0].preciousd8 * 1 > 0 ? formato_numero(data[0].preciousd8, 2, _CD, _CM, "") : "0");
        $("#PrecioUSD9").val(data[0].preciousd9 * 1 > 0 ? formato_numero(data[0].preciousd9, 2, _CD, _CM, "") : "0");
        $("#PrecioUSD10").val(data[0].preciousd10 * 1 > 0 ? formato_numero(data[0].preciousd10, 2, _CD, _CM, "") : "0");
        
        $("#FotoArticulo").attr("src", url_cliente + "Adjunto/imagenes/Articulos/" + IdArticulo + "/1.jpg?id=" + NumeroAleatorio());
        NumFoto = 1;
        
        TablaDependencia();
        
    }
}

$("#formArticulos").submit(function (e) {
    e.preventDefault();
    var mensaje = "";
    var ultimocosto = $.trim($("#UltimoCosto").val());
    var ultimocostousd = $.trim($("#UltimoCostoUSD").val());
    
    var iva = $("#Iva").val()*1;
    var ivausd = $("#IvaUSD").val()*1;
    var parametros = $("#formArticulos").serialize().replace(/\%2C/g, "");
    
    if (ultimocosto == "")
        mensaje += "Debe de ingresar el valor del último costo en moneda nacional <br>";
	if (ultimocostousd == "")
        mensaje += "Debe de ingresar el valor del último costo en dólares <br>";
    for (var x = 1; x <= 10; x++) {
        if ($.trim($("#Precio" + x).val()) == "")
            mensaje += "Debe de ingresar el valor del precio número " + x + " <br>";
		if ($.trim($("#PrecioUSD" + x).val()) == "")
            mensaje += "Debe de ingresar el valor del precio en dólares número " + x + " <br>";
        parametros += "&PrecioUSD" + x + "=" + NumeroDecimal($("#PrecioUSD" + x).val());
        parametros += "&Precio" + x + "=" + NumeroDecimal($("#Precio" + x).val());
    }
    parametros += "&UltimoCosto=" + NumeroDecimal(ultimocosto) + "&Iva=" + iva;
    parametros += "&UltimoCostoUSD=" + NumeroDecimal(ultimocostousd) + "&IvaUSD=" + ivausd;
    
    
    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }
    var datos = LlamarAjax("Inventarios","opcion=GuardarArticulos&"+ parametros).split("|");
    if (datos["0"] == "0") {
        IdArticulo = datos[2] * 1;
        $("#IdArticulo").val(IdArticulo);
        $("#CodigoInterno").val(datos[3]);
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

$("#TablaArticulos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var id = row[0].innerText;
    $('#tab-inventario a[href="#tabcrear"]').tab('show');
    BuscarArticulo(id);
});


function ConsultarArticulos() {

    var presentacion =$("#CPresentacion").val()*1;
    var caracteristica =$("#CCaracteristica").val()*1;
    var tipo = $("#CTipo").val();
    var estado = $.trim($("#CEstado").val());
    var uso_interno = $.trim($("#CUsoInterno").val());
    var referencia = $.trim($("#CReferencia").val());
    
    var grupo = $("#CGrupo").val() * 1;
    var equipo = $("#CEquipo").val() * 1;
    var marca = $("#CMarca").val() * 1;
    var modelo = $.trim($("#CModelo").val());
    var proveedor = $("#CProveedor").val() * 1
        
    ActivarLoad();
    setTimeout(function () {
        var parametros = "presentacion=" + presentacion + "&caracteristica=" + caracteristica + "&tipo=" + tipo + "&grupo=" + grupo + "&estado=" + estado + "&marca=" + marca + "&modelo=" + modelo + "&proveedor=" + proveedor + "&equipo=" + equipo + "&uso_interno=" + uso_interno + "&referencia=" + referencia;
        var datos = LlamarAjax("Inventarios","opcion=ConsultarArticulos&"+ parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaArticulos').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "id" },
                { "data": "imagen" },
                { "data": "codigointerno" },
                { "data": "codigobarras" },
                { "data": "tipo" },
                { "data": "grupo" },
                { "data": "presentacion" },
                { "data": "equipo" },
                { "data": "marca" },
                { "data": "modelo" },
                { "data": "caracteristica"},
                { "data": "existencia" },
                { "data": "proveedor" },
                { "data": "ultimocosto", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "ultimafactura" },
                { "data": "iva" },
                { "data": "garantia_mes" },
                { "data": "estado" },
                { "data": "catalogo" },
                { "data": "referencia" },
                { "data": "uso_interno" },
                { "data": "ficha" },
                { "data": "manual" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

function CambioReferencia() {
    if (IdArticulo == 0) {
        swal("Acción Cancelada", "Debe seleccionar el cliente", "warning");
        return false;
    }
    $("#SCambioNit").html(ReferenciaArt);
    $("#modalCambioNitCli").modal("show");

}

function CambiarReferencia() {
    var Referencia = $.trim($("#DReferencia").val());
    if (Referencia == "") {
        $("#DReferencia").focus();
        swal("Acción Cancelada", "Debe de ingresar el nuevo NIT/Cédula", "warning");
        return false;
    }

    var datos = LlamarAjax("Inventarios","opcion=CambioNit&Referencia=" + Referencia + "&id=" + IdArticulo + "&tipo=1").split("|");
    if (datos[0] == "0") {
        $("#modalCambioNitCli").modal("hide");
        $("#Referencia").val(Referencia);
        BuscarCliente(Referencia);
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}


function AgregarOpcion(tipo, mensaje, id) {

    var marca = $("#Marca").val() * 1
    var grupo = $("#Grupo").val() * 1
    var tipoinventario = $("#TipoInventario").val();

    switch (tipo) {

        case 2:
            if (grupo == 0) {
                $("#Grupo").focus();
                swal("Acción Cancelada", "Debe de seleccionar un grupo", "warning");
                return false;
            }
            break;
        case 5:
            if (marca == 0) {
                $("#Marca").focus();
                swal("Acción Cancelada", "Debe de seleccionar una marca", "warning");
                return false;
            }
            break;
    }

    swal({
        title: 'Agregar Opción',
        text: mensaje,
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Agregar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if ($.trim(value)) {
                    var datos = LlamarAjax("Inventarios","opcion=GuardarOpcion&tipo=" + tipo + "&grupo=" + grupo + "&marca=" + marca + "&descripcion=" + _escape(value));
                    datos = datos.split("|");
                    if (datos[0] != "XX") {
                        $("#" + id).html(datos[1]);
                        $("#" + id).val(datos[0]).trigger("change");
                        resolve();
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar la descripción de ' + mensaje));
                }
            })
        }
    }).then(function (result) {
        $("#" + id).focus();
    })
}

function CargarModelos(marca) {
    $("#Modelo").html(CargarCombo(50, 1, "", marca));
}

function DescripcionPrecio() {
    var datos = LlamarAjax("Inventarios","opcion=DescripcionPrecio");
    var data = JSON.parse(datos);
    for (var x = 0; x < data.length; x++) {
        $("#LaPrecio" + (x + 1)).html("<a href=\"javascript:CalcularValor(" + (x + 1) + "," + data[x].porcentaje + ",'')\">" + data[x].precio + "</a>");
        $("#LaPrecioUSD" + (x + 1)).html("<a href=\"javascript:CalcularValor(" + (x + 1) + "," + data[x].porcentaje + ",'USD')\">" + data[x].precio + "</a>");
        $("#Precio" + (x + 1)).prop("disabled", false);
        $("#TitPrecio" + data[x].id).html(data[x].descripcion);
        $("#TitPrecioUSD" + data[x].id).html(data[x].descripcion + "<br>(USD)");
    }
    for (var x = (data.length+1); x <=10; x++) {
        $("#Precio" + x + ", #PrecioUSD" + x).prop("disabled", true);
        $("#Precio" + x + ", #PrecioUSD" + x).val("0");
    }
}

function CalcularValor(pos, porcentaje) {

    if (IdArticulo == 0)
        return false;

    var valoriva = LlamarAjax("Inventarios","opcion=ValorIva&Iva=" + $("#Iva").val());

    $("#Cos").val($("#UltimoCosto").val());
    $("#Por").val(formato_numero(porcentaje, _DE,_CD,_CM,""));
    $("#Pos").val(pos);
    $("#IVA").val(valoriva);
    $("#PreVen").val($("#Precio" + pos).val());
    precio = (NumeroDecimal($("#UltimoCosto").val()) * porcentaje / 100) + NumeroDecimal($("#UltimoCosto").val());
    $("#PreBas").val(formato_numero(precio, 3, ".", ",", ""));
    $("#TitCalPrecio").html($("#Ta_PLaPrecio" + pos).html());
    CalcularResultado("1", 1);
    $("#modalCalPrecio").modal("show");
}

function CalcularResultado(Caja, tipo) {
    if (Caja != "1")
        ValidarTexto(Caja, 1);
    var iva = NumeroDecimal($("#Iva").val());

    var pos = NumeroDecimal($("#Pos").val());
    var costo = NumeroDecimal($("#Cos").val());
    switch (tipo) {
        case 1:
            var valor = NumeroDecimal($("#PreBas").val());
            var porcentaje = (valor * 100 / costo) - 100;
            $("#Por").val(formato_numero(porcentaje, _DE,_CD,_CM,""));
            var precio = valor * (1 + (iva / 100));
            precio = Math.round(precio / 100, 0) * 100;
            $("#PreVen").val(formato_numero(precio, 0,_CD,_CM,""));

            break;
        case 2:
            var valor = NumeroDecimal($("#PreVen").val());
            var precio = valor / (1 + (iva / 100));
            $("#PreBas").val(formato_numero(precio, 3, ".", ",", ""));
            var porcentaje = (precio * 100 / costo) - 100;
            $("#Por").val(formato_numero(porcentaje, _DE,_CD,_CM,""));
            break;
        case 3:
            var porcentaje = NumeroDecimal($("#Por").val());
            var precio = (costo * porcentaje / 100) + costo;
            $("#PreBas").val(formato_numero(precio, 3, ".", ",", ""));
            var precioventa = precio * (1 + (iva / 100));
            precioventa = Math.round(precioventa / 100, 0) * 100;
            $("#PreVen").val(formato_numero(precioventa, 0,_CD,_CM,""));
            break;
    }
}

function AgregarPrecio() {
    var pos = $("#Pos").val();
    var porcentaje = NumeroDecimal($("#Por").val());
    if (porcentaje <= 0) {
        $("#PreVen").focus();
        swal("Acción Cancelada", "El precio base no puede ser menor al costo del producto", "warning");
        return false;
    }
    $("#Precio" + pos).val($("#PreBas").val());
    $("#modalCalPrecio").modal("hide");
}


DescripcionPrecio();

function CargarEquipos(grupo) {
	if (OpcionEquipo == 0) {
		$("#Equipo").html(CargarCombo(48, 1, "", grupo));
	}
    if (grupo * 1 == 0) {
        $("#CodigoInterno").val("");
        return false;
    } 
    else
        $("#CodigoInterno").val(LlamarAjax("Inventarios","opcion=CodigoInterno&Grupo=" + grupo));
    
    if (IdArticulo == 0) {
        var cuenta = $("#CuentaContable").val() * 1;
        if (cuenta == 0) {
            cuenta = LlamarAjax("Inventarios","opcion=CuentaContable_Grupo&grupo=" + grupo) * 1;
            if (cuenta > 0)
                $("#CuentaContable").val(cuenta).trigger("change");
        }
    }

}

function CalcularPrecio() {
    var datos = LlamarAjax("Inventarios","opcion=DescripcionPrecio&");
    var iva = NumeroDecimal($("#Iva").val());
    var costo = NumeroDecimal($("#UltimoCosto").val());
    var data = JSON.parse(datos);
    var resultado = "";
    for (var x = 0; x < data.length; x++) {
        porcentaje = data[x].porcentaje;
        pos = x + 1;
        precio = (costo * porcentaje / 100) + costo;
        precioventa = precio * (1 + (iva / 100));
        precioventa = Math.round(precioventa / 100, 0) * 100;
        $("#Precio" + pos).val(formato_numero(precioventa, 0,_CD,_CM,""));
    }
}

function GuardarDocumento(tipo, id) {
    var documento = document.getElementById(id);
    if ($.trim(documento.value) != "") {
		var archivo = GuardarDocumentoAdjunto(id,4,'', tipo);
		if (archivo == "" || !archivo){
			swal("Acción Cancelada", "Error al adjuntar la imagen", "warning");
			return false;
		}
	}
}

function CambioTipoInventario(tipo){
	$("#Marca").prop("disabled",false);
	$("#Modelo").prop("disabled",false);
	$("#Presentacion").prop("disabled",false);
	$("#Presentacion").html(CargarCombo(98, 1));
	if (tipo == "SERVICIO"){
		$("#Marca").val("").trigger("change");
		$("#Modelo").val("").trigger("change");
		$("#Marca").prop("disabled",true);
		$("#Modelo").prop("disabled",true);
	}
}

function GrupoEquipo(equipo) {
    equipo = equipo * 1;
    var datos = LlamarAjax("Inventarios","opcion=GrupoEquipo&id=" + equipo);
    if (datos != "[]") {
		OpcionEquipo = 1;
        var data = JSON.parse(datos);
        if (data[0].idgrupo * 1 != $("#Grupo").val()*1)
            $("#Grupo").val(data[0].idgrupo).trigger("change");
		OpcionEquipo = 0;
    }
}

function AbrirPDF(tipo) {
	if (IdArticulo == 0)
		return false;
	carpeta = "";
	descripcion = "";
	if (tipo == 1){
		carpeta = "FichaTecnica";
		descripcion = "la <b>ficha técnica</br>";
	}else{
		carpeta = "ManualUsuario";
		descripcion = "el <b>manual de usuario</b>";
	}
    
    var datos = LlamarAjax("Inventarios","opcion=PDFArticulos&articulo=" + IdArticulo + "&tipo=" + tipo)*1;
    if (datos == "0") {
		swal("Acción Cancelada","No se ha cargado ningun pdf para " + descripcion,"warning");
	}else{
		window.open(url_archivo + "Adjunto/" + carpeta + "/"  +  IdArticulo + ".pdf");
    }
}

function SubirFotoArticulo() {
    if (IdArticulo > 0) {
        $("#modalImagenes").modal("show");
        $("#btnguardarfoto").attr("onclick","GuardarFotoArticulo()");
        var opciones = "";
        for (var x=1; x<=5; x++){
			opciones += "<option value='" + x + "'>" + x + "</option>";
		}
		$("#NumeroFoto").html(opciones);
		$("#NumeroFoto").attr("onchange", "CambiarFotoArticulo(this.value)");
		$("#NumeroFoto").trigger("change");
		$("#btneliminarfoto").attr("onclick","EliminarFotoArticulo()");
		$("#EmpleadoFoto").html($("#Grupo option:selected").text() + " - " + $("#TipoInventario option:selected").text() + " - " + $("#Equipo option:selected").text() + " - " + $("#Marca option:selected").text() + " - " + $("#Modelo option:selected").text() + " - " + $("#Caracteristica option:selected").text());
		
    }
}

function CambiarFotoArticulo(numero){
	$("#Imagen_Guardada").attr("src", url_cliente + "Adjunto/imagenes/Articulos/" + IdArticulo + "/" + numero + ".jpg?id=" + NumeroAleatorio());
}

function CatalogoArticulo(){
	if (IdArticulo > 0){
		if (NumFoto == 5){
			NumFoto = 1;
		}else{
			NumFoto += 1;
		}
			
		$("#FotoArticulo").attr("src", url_cliente + "Adjunto/imagenes/Articulos/" + IdArticulo + "/" + NumFoto + ".jpg?id=" + NumeroAleatorio());
	}
		
}

function GuardarFotoArticulo(){
	var cropcanvas = $('#Art_Imagen').cropper('getCroppedCanvas');
    var archivo = cropcanvas.toDataURL("image/jpeg");
	var numero = $("#NumeroFoto").val()*1;
    var ruta = cropcanvas.toDataURL();
        
    ActivarLoad();
    setTimeout(function () {
		var parametros = "opcion=SubirFoto&archivo=" + archivo + "&Articulo=" + IdArticulo + "&numero=" + numero + "&tipo_imagen=6";
		var data = LlamarAjax("Configuracion",parametros).split("|");
		DesactivarLoad();
		if (data[0] == "0") {
			if (numero == 1){
				$("#FotoArticulo").attr("src", url_cliente + "Adjunto/imagenes/Articulos/" + IdArticulo + "/1.jpg?id=" + NumeroAleatorio());
			}
			$("#Imagen_Guardada").attr("src", url_cliente + "Adjunto/imagenes/Articulos/" + IdArticulo + "/" + numero + ".jpg?id=" + NumeroAleatorio());
		} else
			swal("Acción Cancelada", data[1], "warning");
	},15);
}

function EliminarFotoArticulo(){
	if (IdArticulo > 0) {
		var numero = $("#NumeroFoto").val()*1;
		var parametros = "opcion=EliminarFoto&Articulo=" + IdArticulo + "&numero=" + numero;
		var data = LlamarAjax("Inventarios",parametros).split("|");
		if (data[0] == "0") {
			if (numero == 1){
				$("#FotoArticulo").attr("src", url_cliente + "Adjunto/imagenes/logo_articulo.png?id=" + NumeroAleatorio());
			}
			$("#Imagen_Guardada").attr("src", url_cliente + "Adjunto/imagenes/Blanco.png?id=" + NumeroAleatorio());
		} else
			swal("Acción Cancelada", data[1], "warning");
	}
}

$("#FotoArticulo").error(function() {
	$(this).attr("src", url_cliente + "Adjunto/imagenes/logo_articulo.png");
});

function ModalArticulos(){
	if (IdArticulo > 0){
		$("#modalArticulos").modal("show");
	}
}

$("#TablaModalArticulosArt > tbody").on("dblclick", "tr", function (e) {

    var row = $(this).parents("td").context.cells;
    var id = row[1].innerText*1;
    if (id == IdArticulo){
		swal("Acción Cancelada","Artículo en la estructura de costo no debe ser igual al artículo prinicpal","warning");
		return false;
	}
	
	var Encontrado = LlamarAjax("Inventarios","opcion=BuscarArticuloDependencia&articulo=" + IdArticulo + "&dependencia=" + id)*1;
	
	if (Encontrado > 0){
		swal("Acción Cancelada","Este artículo ya se encuentra registrado en la estructura de costo","warning");
		return false;
	}
    var codigo = row[2].innerText;
    var articulo = "TIPO: " + row[5].innerText + ", GRUPO: " + row[4].innerText +
        ", ARTICULO: " + row[6].innerText + ", MARCA: " + row[7].innerText + ", MODELO: " + row[8].innerText + ", PRESENTACION: " + row[9].innerText + 
        ", CARACTERISTICA: " + row[10].innerText;
	var precio = NumeroDecimal(row[13].innerText);
	var preciousd = NumeroDecimal(row[14].innerText);
	
	$("#DepId").val("0");
	$("#DepIdArticulo").val(id);
	$("#DepCodigo").val(codigo);
	$("#DepArticulo").val(articulo);
	$("#DepCantidad").val("1");
	$("#DepPrecioMN").val(formato_numero(precio, _DE, _CD, _CM, ""));
	$("#DepPrecioUSD").val(formato_numero(preciousd, _DE, _CD, _CM, ""));
	
	$("#modalArticulos").modal("hide");
	

});

function LimpiarDependencia(){
	$("#DepId").val("0");
	$("#DepIdArticulo").val("0");
	$("#DepCodigo").val("");
	$("#DepArticulo").val("");
	$("#DepCantidad").val("1");
	$("#DepPrecioMN").val("");
	$("#DepPrecioUSD").val("");
}

function GuardarDependencia(){
	
	var id = $("#DepId").val()*1;
	var articulo = $("#DepIdArticulo").val()*1;
	var cantidad = $("#DepCantidad").val()*1;
	var precio = NumeroDecimal($("#DepPrecioMN").val());
	var preciousd = NumeroDecimal($("#DepPrecioUSD").val());
	
	if (cantidad == 0){
		$("#DepCantidad").focus();
		swal("Acción Cancelada","Debe de ingresar la cantidad","warning");
		return false;
	}
	
	if (precio == 0 && preciousd == 0){
		$("#DepPrecioMN").focus();
		swal("Acción Cancelada","Debe de ingresar por lo mínimo un precio","warning");
		return false;
	}
	
	var parametros = "opcion=GuardarArticuloDependencia&articulo=" + IdArticulo + "&dependencia=" + articulo + "&id=" + id + "&cantidad=" + cantidad + "&precio=" + precio + "&preciousd=" + preciousd;
	var data = LlamarAjax("Inventarios",parametros).split("|");
	if (data[0] == "0") {
		swal("",data[1],"success");
		LimpiarDependencia();
		TablaDependencia();	
	} else{
		swal("Acción Cancelada", data[1], "warning");
	}
	
}

function EliminarDependencia(id){
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar la estructora de costo del id número ' + id + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Inventarios","opcion=EliminarArticuloDependencia&Id=" + id + "&IdReemplazo=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve();
                        else {
                            reject(datos[1]);
                        }

                    })
            })
        }
    }]).then(function (data) {
        LimpiarDependencia();
		TablaDependencia();	
    });
}


function TablaDependencia(){
	ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Inventarios","opcion=ConsultarArticuloDependencia&articulo=" + IdArticulo);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        TableDependencia = $('#TablaDependencia').DataTable({
            data: datajson,
            bProcessing: true,
				bDestroy: true,
            columns: [
                { "data": "imagen" },
                { "data": "id" },
                { "data": "codigointerno" },
                { "data": "articulo" },
                { "data": "cantidad", "className": "text-center text-XX" },
                { "data": "precio", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "preciousd", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "subtotalusd", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "des_estado" },
                { "data": "registro" },
                { "data": "idarticulo" },
                { "data": "eliminar"}
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
        
        var tcantidad = 0;
        var ttotal = 0;
        var ttotalusd = 0;
                
        TableDependencia.rows().data().each(function (datos, index) {
			tcantidad += datos.cantidad * 1;
			ttotal += datos.subtotal * 1;
			ttotalusd += datos.subtotalusd * 1;
		});
		
		$("#TCantidad").val(formato_numero(tcantidad, 0, _CD, _CM, ""));
		$("#TTotal").val(formato_numero(ttotal, _DE, _CD, _CM, ""));
		$("#TTotalUSD").val(formato_numero(ttotalusd, _DE, _CD, _CM, ""));
				
        
    }, 15);
}

$("#TablaDependencia > tbody").on("dblclick", "tr", function (e) {

    var row = $(this).parents("td").context.cells;
    var articulo = $.trim(row[3].innerText).replace(/<b>/g, '').replace(/<\/b>/g, '');
	$("#DepId").val(row[1].innerText);
	$("#DepIdArticulo").val(row[11].innerText);
	$("#DepCodigo").val(row[2].innerText);
	$("#DepArticulo").val(articulo);
	$("#DepCantidad").val(row[4].innerText);
	$("#DepPrecioMN").val(formato_numero(NumeroDecimal(row[5].innerText), _DE, _CD, _CM, ""));
	$("#DepPrecioUSD").val(formato_numero(NumeroDecimal(row[7].innerText), _DE, _CD, _CM, ""));

});

function TablaPrecios() {
	var codigointerno = $.trim($("#PCodigoInterno").val());
	var codigobarra = $.trim($("#PCodigoBarras").val());
	var grupo = $("#PGrupo").val() * 1;
	var tipo = $("#PTipo").val();
	var estado = $.trim($("#PEstado").val());
	var referencia = $.trim($("#PReferencia").val());
	var uso_interno = $.trim($("#PUsoInterno").val());
	var catalogo = $.trim($("#PCatalogo").val());
	var iva = $("#PIva").val() * 1;
	var equipo = $("#PEquipo").val() * 1;
    var marca = $("#PMarca").val() * 1;
    var modelo = $.trim($("#PModelo").val());
    var presentacion = $("#PPresentacion").val() * 1;
    var caracteristica =$("#PCaracteristica").val()*1;
    var proveedor = $("#PProveedor").val() * 1
    

    
        
    ActivarLoad();
    setTimeout(function () {
        var parametros = "codigointerno=" + codigointerno + "&codigobarra=" + codigobarra + "&presentacion=" + presentacion + "&caracteristica=" + caracteristica + 
						  "&tipo=" + tipo + "&grupo=" + grupo + "&estado=" + estado + "&marca=" + marca + "&modelo=" + modelo + "&proveedor=" + proveedor + 
						  "&equipo=" + equipo + "&uso_interno=" + uso_interno + "&referencia=" + referencia + "&iva=" + iva + "&catalogo=" + catalogo;
        var datos = LlamarAjax("Inventarios","opcion=ConsultarTablaPrecio&"+ parametros);
        DesactivarLoad();
        
        if (TablePrecio != null)
            TablePrecio.colReorder.reset();

        var datajson = JSON.parse(datos);
        
        TablePrecio = $('#TablaPrecios').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "id" },
                { "data": "imagen" },
                { "data": "codigointerno" },
                { "data": "codigobarras" },
                { "data": "grupo" },
                { "data": "tipo" },
                { "data": "equipo" },
                { "data": "marca" },
                { "data": "modelo" },
                { "data": "presentacion" },
                { "data": "caracteristica"},
                { "data": "referencia"},
                { "data": "uso_interno"},
                { "data": "catalogo"},
                { "data": "existencia", "className": "text-center, text-XX" },
                { "data": "proveedor" },
                { "data": "garantia_mes", "className": "text-center, text-XX" },
                { "data": "ultimocosto", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "iva" },
                { "data": "precio1", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio2", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio3", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio4", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio5", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio6", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio7", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio8", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio9", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "precio10", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "ultimocostousd", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "ivausd" },
                { "data": "preciousd1", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "preciousd2", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "preciousd3", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "preciousd4", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "preciousd5", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "preciousd6", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "preciousd7", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "preciousd8", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "preciousd9", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                { "data": "preciousd10", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 2, '') },
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ],
            "colReorder": true
        });
    }, 15);
}






$('select').select2();
DesactivarLoad();


