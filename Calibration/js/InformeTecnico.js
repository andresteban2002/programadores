﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaDesde").val(output);
$("#FechaHasta").val(output2);

$("#Magnitud").html(CargarCombo(1, 1));
$("#Equipo").html(CargarCombo(2, 1));
$("#Marca").html(CargarCombo(3, 1));
$("#Cliente").html(CargarCombo(9, 1));
$("#Modelo").html(CargarCombo(5, 1, "", "-1"));
$("#Intervalo").html(CargarCombo(6, 1, "", "-1"));

$("#Usuario").html(CargarCombo(13, 1));

function BuscarInforme() {
    var magnitud = $("#Magnitud").val() * 1;
    var equipo = $("#Equipo").val() * 1;
    var modelo = $("#Modelo").val() * 1;
    var marca = $("#Marca").val() * 1;
    var intervalo = $("#Intervalo").val() * 1;
    var ingreso = $("#Ingreso").val() * 1;
    var serie = $.trim($("#CSerie").val());
    var cliente = $("#Cliente").val() * 1;
    var informe = $("#Informe").val() * 1;
    var usuario = $("#Usuario").val() * 1;
    var tipo = $("#Tipo").val() * 1;

    var estado = $("#Estado").val();
    var fechad = $("#FechaDesde").val();
    var fechah = $("#FechaHasta").val();

    if (fechad == "" && fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar una fecha o números de días"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "informe=" + informe + "&cliente=" + cliente + "&ingreso=" + ingreso + "&estado=" + estado +
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&usuario=" + usuario + "&tipo=" + tipo;
        var datos = LlamarAjax("Laboratorio","opcion=TablaInformeTecnico&" + parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaInforme').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "tipo" },
                { "data": "informe" },
                { "data": "cliente" },
                { "data": "ingreso" },
                { "data": "equipo" },
                { "data": "estado" },
                { "data": "fecha" },
                { "data": "tecnico" },
                { "data": "aprobado" },
                { "data": "anulado" },
                { "data": "opciones" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

$("#TablaInforme > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    $("#NTipo").val(row[0].innerText);
    $("#NInforme").val(row[1].innerText);
    $("#NIngreso").val(row[3].innerText);
    $("#NEquipo").html(row[4].innerText);
    $("#NCliente").val(row[2].innerText);
    $("#NConcepto").val(row[0].innerText == "Devolución" ? 3 : 5);
    var datos = LlamarAjax("Laboratorio","opcion=DatosInformeTecnico&informe=" + row[1].innerText);
    var data = JSON.parse(datos);
    $("#NObservacion").html(data[0].observacion);
    $("#NConclusion").val(data[0].conclusion);

    $("#InformeEquipo").modal("show");
});

function ImprimirInformeTec(ingreso, plantilla) {
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=InformeTecnico&documento=0&ingreso=" + ingreso + "&plantilla=" + plantilla).split("||");
        DesactivarLoad();
        if (datos[0] == "0") {
            window.open(url_archivo + "DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);
} 

function AnularInformeTec(informe, estado, cliente) {

    if (estado == "Anulado") {
        swal("Acción Cancelada", "No se puede anular un informe técnico en estado " + estado, "warning");
        return false;
    }
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Desea Anular el Informe Técnico ') + ' ' + informe + ' ' + ValidarTraduccion('del cliente') + ' ' + cliente,
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Anular'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Laboratorio","opcion=AnularInformeTec&Informe=" + informe + "&Observaciones=" + value)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar una observación'));
                }
            })
        }
    }).then(function (result) {
        BuscarInforme();
        swal({
            type: 'success',
            html: 'Informe Técnico Nro ' + informe + ' anulado con éxito'
        })
    })
}

$("#FomrEdiInforme").submit(function (e) {
    e.preventDefault();
    var parametros = $("#FomrEdiInforme").serialize();
    var datos = LlamarAjax("Laboratorio","opcion=EditarInforme", parametros).split("|");
    if (datos[0] == "0") {
        BuscarInforme();
        $("#InformeEquipo").modal("hide");
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

    return false;
})


function AprobarInformeTec(informe,estado){

    if (estado != "Registrado") {
        swal("Acción Cancelada", "No se puede aporbar un informe técnico con estado " + estado, "warning");
        return false;
    }
        
    var resultado = "";
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Dese') + ' ' + NRecibo + ' ' + ValidarTraduccion('del cliente') + ' ' + NombreCliente,
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Caja/AnularRecibo", "recibo=" + NRecibo + "&observaciones=" + value)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resultado = NRecibo;
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar un observación'));
                }
            })
        }
    }).then(function (result) {
        Limpiar();
        IdCliente = 0;
        $("#Cliente").val("");
        $("#Cliente").focus();
        swal({
            type: 'success',
            html: 'Recibo Nro ' + resultado + ' anulado con éxito'
        })
    })

}

$('select').select2();
DesactivarLoad();
