d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CoFechaDesde").val(output);
$("#CoFechaHasta").val(output2);


DesactivarLoad();
var nbArchivo = "";
TraducirIdioma($("#menu_idioma").val() * 1);
$("#MoTitulo").html(CargarCombo(91, 0, "1", "1")).change();
$("#buscTitulo").html(CargarCombo(91, 0, "1", "1")).change();
$("#idAreasTit").html(CargarCombo(54, 0, "", "1"));
var date = new Date();
$("#MoFechaInicio").attr("min", date.getFullYear() + '-' + (Number(date.getMonth() + 1) < 10 ? '0' + Number(date.getMonth() + 1) : Number(date.getMonth() + 1)) + '-' + (parseInt(date.getDate()) > 9 ? date.getDate() : '0' + date.getDate()));

function fechaIni() {
    var dateE = new Date($("#MoFechaInicio").val());
    $("#MoFechaFinal, #MoFechaRecordatorio").attr("min", $("#MoFechaInicio").val());
}

function fechaInicioII() {
    //TFechaInicio
    $("#TFechaRecordatorio, #TFechaFinal").attr("min", $("#TFechaInicio").val());
}
var cargoSup = LlamarAjax("Actividades", "opcion=esDirector").split("|");
if (cargoSup[0] == 0 && cargoSup[1] == "false")
    $("#linkAdd, #estadoCampo").hide();

async function guardarEvento() {

    var tipo = estado = titulo = descripcion = fechaIni = fechaFin = fechaRec = "";
    tipo = $("#MoTipo").val();
    estado = $("#MoEstado").val();
    titulo = $("#MoTitulo").val();
    descripcion = $("#MoDescripcion").val();
    fechaIni = $("#MoFechaInicio").val();
    fechaFin = $("#MoFechaFinal").val();
    fechaRec = $("#MoFechaRecordatorio").val();
    if (String(tipo) == "" || String(tipo) == "undefined" || String(estado) == "" || String(estado) == "undefined" || String(titulo) == "" || String(titulo) == "undefined" || String(descripcion) == "" || String(descripcion) == "undefined" || String(fechaIni) == "" || String(fechaIni) == "undefined" || String(fechaFin) == "" || String(fechaFin) == "undefined" || String(fechaRec) == "" || String(fechaRec) == "undefined") {
        swal("Acción Cancelada", "Debe completar los campos necesarios para poder agregar el " + $('select[id="Tipo"] option:selected').text() + " al cronograma", "warning");
    } else {
        var parametros = 'id=0&tipo=' + tipo + '&estado=' + estado + '&titulo=' + titulo + '&descripcion=' + descripcion + '&fechaIni=' + fechaIni + '&fechaFin=' + fechaFin + '&fechaRec=' + fechaRec;
        var guardarEvento = LlamarAjax("Actividades", "opcion=AgregarCronograma&" + parametros).split("|");
        if (guardarEvento[0] == 0) {
            nbArchivo = guardarEvento[1];
            await subirArchivoBase64(guardarEvento[1]);
            CargarAgenda(1);
            swal("", "" + $('select[id="Tipo"] option:selected').text() + " ha sido agregada(o) exitosamente al cronograma de actividades", "success");
            $("#ModalEvento").modal("hide");
        } else
            swal("Acción cancelada", "Error: " + guardarEvento[1], "warning");
    }
}

async function ModificarEvento() {
    var tipo = estado = titulo = descripcion = fechaIni = fechaFin = fechaRec = "";
    if (String($("#TID").val()) == "undefined" || String($("#TID").val()) == "") {
        swal("Acción Cancelada", "Para poder modificar la actividad debe ingresar un ID valido", "warning");
    } else {
        tipo = $("#TTipo").val();
        estado = $("#TEstado").val();
        titulo = $("#TTitulo").val();
        descripcion = $("#TDescripcion").val();
        fechaIni = $("#TFechaInicio").val();
        fechaFin = $("#TFechaFinal").val();
        fechaRec = $("#TFechaRecordatorio").val();
        if (String(tipo) == "" || String(tipo) == "undefined" || String(estado) == "" || String(estado) == "undefined" || String(titulo) == "" || String(titulo) == "undefined" || String(descripcion) == "" || String(descripcion) == "undefined" || String(fechaIni) == "") {
            swal("Acción Cancelada", "Debe completar los campos necesarios para poder agregar el " + $('select[id="Tipo"] option:selected').text() + " al cronograma", "warning");
        } else {
            var parametros = 'id=' + (String($("#TID").val()) == "undefined" || String($("#TID").val()) == "" ? 0 : ($("#TID").val())) + '&tipo=' + tipo + '&estado=' + estado + '&titulo=' + titulo + '&descripcion=' + descripcion + '&fechaIni=' + fechaIni + '&fechaFin=' + fechaFin + '&fechaRec=' + fechaRec;
            var guardarEvento = LlamarAjax("Actividades", "opcion=AgregarCronograma&" + parametros).split("|");
            if (guardarEvento[0] == 0) {
                nbArchivo = guardarEvento[1];
                await subirArchivoBase64(guardarEvento[1]);
                CargarAgenda(TipoAgenda);
                swal("", "Modificacion exitosa", "success");
                VerEvento(parseInt($("#TID").val()));
            } else
                swal("Acción cancelada", "Error: " + guardarEvento[1], "warning");
        }
    }

}

function agregarTituloMo() {
    agregarListaTitulos();
    $("#ModalTitulo").modal("show");
}

function eliminarTitulo(id) {
    let titulos = LlamarAjax("Actividades", "opcion=eliminarTitulos&id=" + id).split("|");
    if (parseInt(titulos[0]) == 0)
        swal("", titulos[1], "success");
    agregarListaTitulos()
}

function agregarListaTitulos() {
    let titulos = LlamarAjax("Actividades", "opcion=obtenerTitulos").split("|");
    let body = "";
    if (parseInt(titulos[0]) == 0) {
        let bodyString = JSON.parse(titulos[1]);
        if (bodyString.length == 0)
            body += "<tr><th colspan='3' class='text-center'>Sin actividades</th><tr>";

        for (var i = 0; i < bodyString.length; i++) {
            body += "<tr><th>" + bodyString[i].actividad + "</th><th>";
            if (bodyString[i].areas) {
                for (let j = 0; j < (bodyString[i].areas).length; j++) {
                    body += "<p>" + (bodyString[i].areas)[j].descripcion + "</p>";
                }
            } else if (bodyString[i].areas.includes("|0|")) {
                body += "<p>Todos</p>";
            }

            body += "</th><th><button class='btn btn-glow btn-warning' type='button' onclick='eliminarTitulo(" + bodyString[i].identificacion + ")'>Eliminar</button></th></tr>";
        }
    }
    $("#bodyTitulos").html(body);
}

function guardarTitulo() {
    let parametros = "";
    if (String($("#IdTituloNvo").val()) == "undefined" || String($("#IdTituloNvo").val()) == "")
        swal("Acción Cancelada", "Debe completar los campos necesarios para poder agregar la actividad", "warning");
    else if (String($("#idAreasTit").val()) == "undefined" || String($("#idAreasTit").val()) == "[]")
        swal("Acción Cancelada", "Debe completar los campos necesarios para poder agregar la actividad", "warning");
    else {
        var guardarEvento = LlamarAjax("Actividades", "opcion=AgregarTitulo&titulo=" + ($("#IdTituloNvo").val()) + "&areas=" + String($("#idAreasTit").val())).split("|");
        $("#ModalTitulo").modal("hide");
        $("#MoTitulo").html("").change();
        $("#MoTitulo").html(CargarCombo(91, 0, "1", "1")).change();

    }

}

const archivoToBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});

async function subirArchivoBase64(id) {
    var arrArchivos = document.getElementById("MoArchivo").files.length > 0 ? document.getElementById("MoArchivo") : document.getElementById("TArchivos");
    var datosB64 = new FormData();
    if (arrArchivos && String(arrArchivos) !== "undefined" && typeof arrArchivos === 'object' && arrArchivos.files.length > 0) {
        for (var i = 0; i < arrArchivos.files.length; i++) {
            datosB64.append("datosB64", await archivoToBase64(arrArchivos.files[i]));
            datosB64.append("nbArch", arrArchivos.files[i].name);
        }
        $.ajax({
            url: 'http://localhost:8080/backendSisCalibration/Actividades?opcion=AdjuntarArchivoB64&id=' + id,
            type: 'POST',
            processData: false,
            contentType: false,
            enctype: 'multipart/form-data',
            data: datosB64,
            cache: false,
            async: false,
            beforeSend: function () {

            },
            success: function (response) {

            }
        });
    }
}

var dias = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
var meses = ['--', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

Date.prototype.getWeekNumber = function () {
    var d = new Date(+this);  //Creamos un nuevo Date con la Fecha de "this".
    d.setHours(0, 0, 0, 0);   //Nos aseguramos de limpiar la hora.
    d.setDate(d.getDate() + 4 - (d.getDay() || 7)); // Recorremos los días para asegurarnos de estar "dentro de la semana"
    //Finalmente, calculamos redondeando y ajustando por la naturaleza de los números en JS:
    return Math.ceil((((d - new Date(d.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7);
};

var fecha_h = new Date();
var month_h = fecha_h.getMonth() + 1;
var day_h = fecha_h.getDate();
var Anio_h = fecha_h.getFullYear();
var DatosCalendarios = null;

var FechaActual = Anio_h + '-' +
    (month_h < 10 ? '0' : '') + month_h + '-' +
    (day_h < 10 ? '0' : '') + day_h;


$("#Vistas").html(CargarCombo(13, 4, "", "1"));
$("#Cliente, #ClienteAgenda, #CoCliente").html(CargarCombo(9, 7));

var Fecha = new Date();
Fecha.setHours(0, 0, 0, 0);
var month = Fecha.getMonth() + 1;
var day = Fecha.getDate();
var Anio = Fecha.getFullYear();
var TipoAgenda = 1;
var Semana = Fecha.getWeekNumber();
var SemanaDI = 0;
var SemanaDF = 0;
var FechaCab = null;
var FechaIniSem = null;

var FechaBDesde = null;
var FechaBHasta = null;

var modificar = 0;

function CambiarMes(tipo) {

    switch (TipoAgenda) {
        case 1:
            if (tipo == 1) {
                if (month == 1) {
                    month = 12;
                    Anio = Anio - 1;
                } else
                    month = month - 1;
            } else {
                if (month == 12) {
                    month = 1;
                    Anio = Anio + 1;
                } else
                    month = month + 1;
            }

            if (Anio == Anio_h && month == month_h)
                day = day_h;
            else
                day = 1;

            Fecha = new Date(Anio + "/" + month + "/" + day);
            break;
        case 2:

            if (tipo == 1) {
                if (FechaIniSem != null) {
                    Fecha = new Date(FechaIniSem.getFullYear() + "/" + (FechaIniSem.getMonth() + 1) + "/" + FechaIniSem.getDate());
                    Fecha.setDate(Fecha.getDate() - 1);
                    month = Fecha.getMonth() + 1;
                    day = Fecha.getDate();
                    Anio = Fecha.getFullYear();
                }
            } else {
                if (FechaCab != null) {
                    Fecha = new Date(FechaCab.getFullYear() + "/" + (FechaCab.getMonth() + 1) + "/" + FechaCab.getDate());
                    Fecha.setDate(Fecha.getDate() + 1);
                    month = Fecha.getMonth() + 1;
                    day = Fecha.getDate();
                    Anio = Fecha.getFullYear();
                }
            }
            break;
        case 3:

            if (tipo == 1) {
                Fecha.setDate(Fecha.getDate() - 1);
                month = Fecha.getMonth() + 1;
                day = Fecha.getDate();
                Anio = Fecha.getFullYear();
            } else {
                Fecha.setDate(Fecha.getDate() + 1);
                month = Fecha.getMonth() + 1;
                day = Fecha.getDate();
                Anio = Fecha.getFullYear();
            }
            break;

    }


    CargarAgenda(TipoAgenda);

}

function BuscarEventos() {
    var parametros = "";
    if ($("#buscTitulo").val()) {
        parametros += "titulo=" + $("#buscTitulo").val() + "&";
    }
    DatosCalendarios = LlamarAjax("Actividades", "opcion=BuscarCronograma&" + parametros).split("|");
    CargarAgenda(1);
}

function Obtenerdatos() {
    var parametros = "";
    if ($("#buscTitulo").val())
        DatosCalendarios = LlamarAjax("Actividades", "opcion=BuscarCronograma&" + parametros).split("|");
}

function BuscarDatos(fecha, hora) {
    var resultado = '<div class="fc-event-inner">';
    var span = "";
    var data = null;
    let fechaHoy = Fecha_calendario(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 1);
    let fechaMontar = new Date(fecha);
    data = JSON.parse(DatosCalendarios[1]);
    if (data !== null && data.length > 0) {
        for (var x = 0; x < data.length; x++) {
            if (String(data[x].id) !== "undefined" && data[x].id !== "") {
                var iniFecha = Fecha_calendario(new Date(data[x].fe_inicio).getFullYear(), new Date(data[x].fe_inicio).getMonth() + 1, new Date(data[x].fe_inicio).getDate() + 1);
                var finFecha = Fecha_calendario(new Date(data[x].fe_fin).getFullYear(), new Date(data[x].fe_fin).getMonth() + 1, new Date(data[x].fe_fin).getDate() + 1);
                if (fecha >= iniFecha && fecha <= finFecha) {
                    span += "<span onclick='VerEvento(" + data[x].id + ")' class='label label-block " + (fechaHoy < iniFecha ? 'label-success' : (fechaHoy > finFecha ? 'label-warning' : 'label-info')) + "'>" +
                        "<p><i class='icon-clipboard'>&nbsp;</i>" + data[x].titulo + "</p>" +
                        "<i>" + (String(data[x].descripcion).length > 50 ? String(data[x].descripcion).substring(0, 50) + '...' : String(data[x].descripcion)) + "</i>" +
                        "</span>";
                }
            }
        }
    }
    data = JSON.parse(DatosCalendarios[2]);
    if (data !== null && data.length > 0) {
        if (fechaMontar.getDay() == 5 || fechaMontar.getDay() == 6) {
            return resultado + "</div>";
        } else {
            for (var x = 0; x < data.length; x++) {
                if (String(data[x].id_actividad_info) !== "undefined" && data[x].id_actividad_info !== "") {
                    var proximaRev = new Date(data[x].fe_inicio_eval);
                    let fechaActIni = null;
                    let fechaActfin = null;
                    let diasActividad = 0;
                    for (var y = 0; y < (data[x].actividades).length; y++) {
                        if (parseInt((data[x].actividades)[y].chekeado) == 0) {
                            fechaActIni = new Date(Fecha_calendario(proximaRev.getFullYear(), proximaRev.getMonth() + 1, proximaRev.getDate()));
                            if (y > 0 && parseInt((data[x].actividades)[y - 1].chekeado) == 1) {
                                diasActividad = parseInt((data[x].actividades)[y - 1].dias_eval);
                                let feFinItem = new Date((data[x].actividades)[y - 1].fe_verificacion);
                                let feFinEval = new Date(fechaActIni);
                                diasActividad = moment(Fecha_calendario(feFinItem.getFullYear(), feFinItem.getMonth() + 1, feFinItem.getDate())).diff(moment(Fecha_calendario(feFinEval.getFullYear(), feFinEval.getMonth() + 1, feFinEval.getDate())), 'days');
                                if (diasActividad < 0)
                                    diasActividad = 0;
                                console.log("Diferencias de dias: ", diasActividad);
                            }                            
                            /**For para correr dia a dia y saber si existe algun sabado o domingo en medio */
                            let diasFestivos = 0;
                            for (var j = 0; j < parseInt((data[x].actividades)[y].dias_eval) + diasActividad; j++) {
                                fechaActIni.setDate(fechaActIni.getDate() + 1);
                                while (fechaActIni.getDay() == 6 || fechaActIni.getDay() == 0) {
                                    if (fechaActIni.getDay() == 6 || fechaActIni.getDay() == 0) {
                                        diasFestivos++;
                                    }
                                    fechaActIni.setDate(fechaActIni.getDate() + 1);
                                }
                            }
                            /****************************************************************************** */
                            fechaActIni = new Date(Fecha_calendario(proximaRev.getFullYear(), proximaRev.getMonth() + 1, proximaRev.getDate()));
                            if (diasFestivos > 0 && (fechaActIni.getDay() == 6 || fechaActIni.getDay() == 0))
                                fechaActIni.setDate(fechaActIni.getDate() + diasFestivos);
                            proximaRev.setDate((proximaRev.getDate()) + parseInt((data[x].actividades)[y].dias_eval));
                            fechaActfin = new Date(Fecha_calendario(proximaRev.getFullYear(), proximaRev.getMonth() + 1, proximaRev.getDate()));
                            console.log("Actividad ", (data[x].actividades)[y].identif, fechaActIni, fechaActfin);
                            fechaActfin.setDate(fechaActfin.getDate() + diasActividad);
                            if (diasFestivos > 0)
                                fechaActfin.setDate(fechaActfin.getDate() + diasFestivos);

                            if (fechaMontar >= fechaActIni && fechaMontar < fechaActfin) {
                                span += "<span onclick='VerEvento(" + data[x].id_actividad_info + ", " + (data[x].actividades)[y].identif + ")' class='label label-block label-primary'>" +
                                    "<p><i class='icon-rulers'>&nbsp;</i>" + data[x].titulo + "</p>" +
                                    "<i>" + (String((data[x].actividades)[y].actividad).length > 50 ? String((data[x].actividades)[y].actividad).substring(0, 50) : String((data[x].actividades)[y].actividad)) + "</i>" +
                                    "</span>";
                                resultado += span + '</div>';
                                return resultado;
                            }

                        } else {
                            fechaActIni = Fecha_calendario(proximaRev.getFullYear(), proximaRev.getMonth() + 1, proximaRev.getDate());
                            proximaRev = new Date((data[x].actividades)[y].fe_verificacion);
                            fechaActfin = Fecha_calendario(new Date((data[x].actividades)[y].fe_verificacion).getFullYear(), new Date((data[x].actividades)[y].fe_verificacion).getMonth() + 1, new Date((data[x].actividades)[y].fe_verificacion).getDate());

                            if (fecha >= fechaActIni && fecha <= fechaActfin) {
                                span += "<span onclick='VerEvento(" + data[x].id_actividad_info + ", " + (data[x].actividades)[y].identif + ")' class='label label-block label-primary'>" +
                                    "<p><i class='icon-rulers'>&nbsp;</i>" + data[x].titulo + "</p>" +
                                    "<i>" + (String((data[x].actividades)[y].actividad).length > 50 ? String((data[x].actividades)[y].actividad).substring(0, 50) : String((data[x].actividades)[y].actividad)) + "</i>" +
                                    "</span>";
                            }
                            if (fechaActfin < fechaActIni && fecha >= fechaActfin && fecha <= fechaActIni) {
                                span += "<span onclick='VerEvento(" + data[x].id_actividad_info + ", " + (data[x].actividades)[y].identif + ")' class='label label-block label-primary'>" +
                                    "<p><i class='icon-rulers'>&nbsp;</i>" + data[x].titulo + "</p>" +
                                    "<i>" + (String((data[x].actividades)[y].actividad).length > 50 ? String((data[x].actividades)[y].actividad).substring(0, 50) : String((data[x].actividades)[y].actividad)) + "</i>" +
                                    "</span>";
                            }
                        }
                    }
                }
            }
        }
    }
    resultado += span + '</div>';
    return resultado;
}

function CargarAgenda(tipo) {
    var resultado = "";
    var dia = 0;

    TipoAgenda = tipo;

    for (var x = 1; x <= 3; x++) {
        if (tipo == x)
            $("#OpcionAgenda" + x).addClass("fc-state-active")
        else
            $("#OpcionAgenda" + x).removeClass("fc-state-active")
    }


    switch (tipo) {
        case 1:
            var semana = 0;
            var month_a = month;
            var Anio_a = Anio;
            var day_a = 1;

            if (month_a == 1) {
                month_a = 12;
                Anio_a = Anio_a - 1;
            } else
                month_a = month_a - 1;

            fechaant = new Date(Anio_a + "/" + month_a + "/" + day_a);

            var ultimoDia = new Date(Fecha.getFullYear(), Fecha.getMonth() + 1, 0).getDate();
            var ultimoDiaMA = new Date(fechaant.getFullYear(), fechaant.getMonth() + 1, 0).getDate();

            var fechadia = new Date(Anio + "/" + month + "/01");
            var diames = fechadia.getUTCDay();

            FechaBDesde = Fecha.getFullYear() + "-" + ((Fecha.getMonth() + 1) * 1 < 10 ? "0" : "") + (Fecha.getMonth() + 1) + "-01";
            FechaBHasta = Fecha.getFullYear() + "-" + ((Fecha.getMonth() + 1) * 1 < 10 ? "0" : "") + (Fecha.getMonth() + 1) + "-" + ultimoDia;

            Obtenerdatos();

            $("#TituloCalendario").html(meses[month] + " " + Anio);

            resultado = "<table width='100%' class='table table-bordered'><thead><tr>";
            for (var x = 0; x <= 6; x++) {
                resultado += "<th width='14%'>" + dias[x] + "</th>";
            }

            var fechadia = '';
            var classactual = "";
            for (var x = (1 - diames); x <= ultimoDia; x++) {
                classactual = "";
                if (semana == 0 && dia == 0)
                    resultado += "<tr class='fc-week fc-first'>";
                if (semana == 7) {
                    resultado += "</tr><tr>";
                    semana = 0;
                }
                dia = x;
                if (x < 1) {
                    dia = ultimoDiaMA + x;
                    fechadia = Fecha_calendario(Anio_a, month_a, dia);
                } else
                    fechadia = Fecha_calendario(Anio, month, dia);

                if (fechadia == FechaActual)
                    classactual = "bg-amarilloclaro";

                resultado += "<td ondblclick =\"CrearEvento('" + fechadia + "','')\"" + (x <= 0 ? " style='opacity: 0.3'" : "") + ' class="fc-day fc-widget-content fc-future ' + classactual + (semana == 0 ? " bg-rojoclaro " : "") + '" > <div style="min-height: 112px;"><div class="fc-day-number text-right">' + dia + '</div>' + BuscarDatos(fechadia, "") + '</div></td > ';
                semana++;
            }
            break;
        case 2:

            var diames = Fecha.getUTCDay();
            FechaIniSem = new Date(Fecha.getFullYear() + "/" + (Fecha.getMonth() + 1) + "/" + Fecha.getDate());
            FechaIniSem.setDate(FechaIniSem.getDate() - diames);

            var month_ini = FechaIniSem.getMonth() + 1;
            var day_ini = FechaIniSem.getDate();
            var Anio_ini = FechaIniSem.getFullYear();

            var me = 0;
            var di = 0;
            var an = 0;

            FechaCab = new Date(FechaIniSem.getFullYear() + "/" + (FechaIniSem.getMonth() + 1) + "/" + FechaIniSem.getDate());
            FechaFin = new Date(FechaIniSem.getFullYear() + "/" + (FechaIniSem.getMonth() + 1) + "/" + FechaIniSem.getDate());
            FechaFin.setDate(FechaFin.getDate() + 6);

            FechaBDesde = FechaIniSem.getFullYear() + "-" + (FechaIniSem.getMonth() + 1) + "-" + FechaIniSem.getDate();
            FechaBHasta = FechaFin.getFullYear() + "-" + (FechaFin.getMonth() + 1) + "-" + FechaFin.getDate();

            Obtenerdatos();
            var classactual = "";
            resultado = "<table width='100%' class='table table-bordered'><thead><tr><th>&nbsp;</th>";
            var fechasemana = [];
            for (var x = 0; x <= 6; x++) {
                dm = FechaCab.getUTCDay();
                me = FechaCab.getMonth() + 1;
                di = FechaCab.getDate();
                an = FechaCab.getFullYear();
                fechasemana.push(Fecha_calendario(an, me, di));
                resultado += "<th width='14%'>" + dias[dm] + "<br>" + di + "/" + me + "/" + an + " </th>";
                if (x != 6)
                    FechaCab.setDate(FechaCab.getDate() + 1);
            }

            $("#TituloCalendario").html(meses[month_ini] + " " + day_ini + "/" + Anio_ini + " - " + meses[me] + " " + di + "/" + an);

            var hora2 = "";
            var hora = "";
            for (var x = 0; x <= 23; x++) {
                resultado += "<tr class='fc-week fc-first'>";
                hora = (x < 10 ? "0" : "") + x + ":00";
                hora2 = hora;
                if (x == 0) {
                    hora = "Todo el día";
                    hora2 = "";
                }
                resultado += "<td>" + hora + "</td>";
                for (var y = 0; y <= 6; y++) {
                    classactual = "";
                    fechadia = fechasemana[y];
                    if (fechadia == FechaActual)
                        classactual = "class='bg-amarilloclaro'";
                    resultado += "<td " + classactual + " ondblclick =\"CrearEvento('" + fechadia + "','" + hora2 + "')\">" + BuscarDatos(fechadia, hora2) + "</td>";
                }
                resultado += "</tr>";
            }
            break;
        case 3:
            var classactual = "";
            dm = Fecha.getUTCDay();
            me = Fecha.getMonth() + 1;
            di = Fecha.getDate();
            an = Fecha.getFullYear();
            fechadia = Fecha_calendario(an, me, di);
            if (fechadia == FechaActual)
                classactual = "class='bg-amarilloclaro'";
            resultado = "<table width='100%' class='table table-bordered'><thead><tr>";
            resultado += "<th colspan='2' style='text-align:center'>" + dias[dm] + "<br>" + di + "/" + an + "</th>";

            $("#TituloCalendario").html(dias[dm] + "<br>" + di + " de " + meses[me] + " de " + an);

            var hora = "";
            var hora2 = "";
            for (var x = 0; x <= 23; x++) {
                resultado += "<tr class='fc-week fc-first'>";
                hora = (x < 10 ? "0" : "") + x + ":00";
                hora2 = hora;
                if (x == 0) {
                    hora = "Todo el día";
                    hora2 = "";
                }
                resultado += "<td width='10%'>" + hora + "</td>";
                resultado += "<td " + classactual + " ondblclick =\"CrearEvento('" + fechadia + "','" + hora2 + "')\">" + BuscarDatos(fechadia, hora2) + "</td>";
                resultado += "</tr>";
            }
            break;


    }
    resultado += "</thead></table>";
    $("#Calendario").html(resultado);


}

function VerEvento(id, idAct) {
    $("#TTitulo").html(CargarCombo(91, 0, "1", "1")).change();
    var consArch = JSON.parse(LlamarAjax("Actividades", "opcion=verArchivo&id=" + id));
    let actividadVer = datosMostrar = null;
    let htmlAct = '';
    nbArchivo = id;
    actEditar(0);
    $("#btnactEdit").removeClass("collapse");

    if (datosMostrar == null) {
        let datos = JSON.parse(DatosCalendarios[2]);
        for (var i = 0; i < datos.length; i++) {
            if (parseInt(datos[i].id_actividad_info) == parseInt(id) && datos[i].idtipo_act == 0) {

                $("#DocRigItem").removeClass("collapse");
                $("#DocAfecItem").removeClass("collapse");
                $("#PasosItem").removeClass("collapse");
                $("#actItem").removeClass("collapse");
                $("#ObservItem").removeClass("collapse");

                $("#TID").val(parseInt(id));
                $("#TRegistro").val(datos[i].nombrecompleto);
                $("#TTipo option:contains(" + datos[i].tipo_crono + ")").attr('selected', true).change();
                $("#TEstado option:contains(" + datos[i].estado + ")").attr('selected', true).change();
                $("#TTitulo option:contains(/" + datos[i].titulo + "/)").attr('selected', true).change();
                $("#TDescripcion").val(datos[i].tx_actividad_motivo);
                $("#TDocRigen").val(datos[i].doc_rigen);
                $("#TDocAfecta").val(datos[i].doc_afecta);
                $("#TPasosVerif").val(datos[i].pasos);
                $("#TObservacion").val(datos[i].observacion_eval);
                let feHoy = new Date(datos[i].fe_inicio_eval);
                for (let j = 0; j < (datos[i].actividades).length; j++) {
                    feHoy.setDate(feHoy.getDate() + parseInt(datos[i].actividades[j].dias_eval) + (parseInt(datos[i].actividades[j].horas_eval) / 8));
                    if (parseInt(idAct) == parseInt(datos[i].actividades[j].identif)) {
                        htmlAct += '<p><b>' + datos[i].actividades[j].actividad + '</b></p>';
                        if (parseInt(datos[i].actividades[j].chekeado) == 1)
                            $("#TEstado option:contains('Finalizado')").attr('selected', true).change();
                        else {
                            if (new Date() <= feHoy) {
                                $("#TEstado option:contains('Activo')").attr('selected', true).change();
                            } else {
                                $("#TEstado option:contains('Vencido')").attr('selected', true).change();
                            }
                        }
                    }
                    else {
                        htmlAct += '<p>' + datos[i].actividades[j].actividad + '</p>';
                    }
                }
                if (htmlAct !== '')
                    $("#activDet").html(htmlAct);
                else {
                    $("#activDet").html('<p>Sin actividades</p>');
                }
                $("#TFechaInicio").val(Fecha_calendario(new Date(datos[i].fe_inicio_eval).getFullYear(), new Date(datos[i].fe_inicio_eval).getMonth() + 1, new Date(datos[i].fe_inicio_eval).getDate() + 1));
                $("#TFechaFinal").val(Fecha_calendario(new Date(datos[i].fe_verificacion).getFullYear(), new Date(datos[i].fe_verificacion).getMonth() + 1, new Date(datos[i].fe_verificacion).getDate() + 1));
                $("#TArchivo").html("");
                if (parseInt((consArch[0]).error) == 0)
                    $("#TArchivo").html(((consArch[0]).mensaje).replace(/\|/g, "\""));
                $("#fechaRecord, #btnactEdit").addClass("collapse");
                datosMostrar = 0;
            }
        }
    }
    if (datosMostrar == null) {
        datos = JSON.parse(DatosCalendarios[1]);
        for (var i = 0; i < datos.length; i++) {
            if (parseInt(datos[i].id) == parseInt(id) && datos[i].tipo_act == 1) {
                $("#TID").val(parseInt(id));
                $("#TRegistro").val(datos[i].nombrecompleto);
                $("#TTipo option:contains(" + datos[i].tipo_crono + ")").attr('selected', true).change();
                $("#TEstado option:contains(" + datos[i].estado + ")").attr('selected', true).change();
                $("#TTitulo option:contains(/" + datos[i].titulo + "/)").attr('selected', true).change();
                $("#TDescripcion").val(datos[i].descripcion);

                $("#DocRigItem").addClass("collapse");
                $("#DocAfecItem").addClass("collapse");
                $("#PasosItem").addClass("collapse");
                $("#actItem").addClass("collapse");
                $("#ObservItem").addClass("collapse");

                $("#TFechaInicio").val(Fecha_calendario(new Date(datos[i].fe_inicio).getFullYear(), new Date(datos[i].fe_inicio).getMonth() + 1, new Date(datos[i].fe_inicio).getDate() + 1));
                $("#TFechaFinal").val(Fecha_calendario(new Date(datos[i].fe_fin).getFullYear(), new Date(datos[i].fe_fin).getMonth() + 1, new Date(datos[i].fe_fin).getDate() + 1));
                $("#TFechaRecordatorio").val(Fecha_calendario(new Date(datos[i].fe_recordatorio).getFullYear(), new Date(datos[i].fe_recordatorio).getMonth() + 1, new Date(datos[i].fe_recordatorio).getDate() + 1));
                $("#fechaRecord, #btnactEdit").removeClass("collapse");
                if (parseInt((consArch[0]).error) == 0)
                    $("#TArchivo").html(((consArch[0]).mensaje).replace(/\|/g, "\""));
            }
        }
    }
    if (parseInt(consArch[0].autorizacion) == 0) {
        $("#btnactEdit").addClass("collapse");
    }
    $("#ModalVerEvento").modal("show");
}

function actEditar(indicador) {
    if (indicador == 1) {
        $("#TTipo").removeAttr("disabled");
        $("#TEstado").removeAttr("disabled");
        $("#TTitulo").removeAttr("disabled");
        $("#TDescripcion").removeAttr("readonly");
        $("#TFechaInicio").removeAttr("readonly");
        if ($("#TFechaRecordatorio").val()) {
            $("#TFechaRecordatorio").removeAttr("readonly");
            $("#TFechaRecordatorio").attr("min", $("#TFechaInicio").val());
        }
        if ($("#TFechaFinal").val()) {
            $("#TFechaFinal").removeAttr("readonly");
            $("#TFechaFinal").attr("min", $("#TFechaInicio").val());
        }
        /*$("#TFechaRecordatorio").removeAttr("readonly");
        $("#TFechaFinal").removeAttr("readonly");*/
        $("#TArchivo").html('<input type="file" class="form-control" name="TArchivos" id="TArchivos" onchange="GuardarDocumento(this.id)" multiple/>');
        $("#btnactEdit").addClass("collapse");
        $("#btnmodtarea").removeClass("collapse");
    }
    if (indicador == 0) {
        $("#TTipo").attr("disabled", true);
        $("#TEstado").attr("disabled", true);
        $("#TTitulo").attr("disabled", true);
        $("#TDescripcion").attr("readonly", true);
        $("#TFechaInicio").attr("readonly", true);
        $("#TFechaRecordatorio").attr("readonly", true);
        $("#TFechaFinal").attr("readonly", true);
        $("#TArchivos").removeAttr("disabled");
        $("#btnactEdit").removeClass("collapse");
        $("#btnmodtarea").addClass("collapse");
    }

}

function AgregarTarea() {
    $("#IdTarea").val("0");
    $("#Tarea").val("");
    $("#IdEvenTarea").val($("#TID").html());
    $("#TituloTarea").html($("#TTitulo").html());

    var datos = LlamarAjax("Configuracion/VerTareas", "Idevento=" + $("#IdEvenTarea").val());
    var datajson = JSON.parse(datos);
    $('#TablaEventoTarea').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        searching: false,
        columns: [
            { "data": "id" },
            { "data": "registro" },
            { "data": "tarea" },
            { "data": "eliminar" }
        ],
        "lengthMenu": [[8], [8]],
        "language": {
            "url": LenguajeDataTable
        }
    });

    $("#ModalTarea").modal("show");

}

$("#TablaEventoTarea> tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    $("#IdTarea").val(row[0].innerText);
    $("#Tarea").val(row[2].innerText);
});

function NuevaTarea() {
    $("#IdTarea").val("0");
    $("#Tarea").val("");
}

function EliminarTarea(id) {



    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar esta tarea?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Configuracion/EliminarTarea", "IdTarea=" + id)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            AgregarTarea();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};

function GuardarTarea() {
    var idtarea = $("#IdTarea").val();
    var tarea = $.trim($("#Tarea").val());
    var id = $("#IdEvenTarea").val();

    if (tarea == "") {
        $("#Tarea").focus()
        swal("Acción Cancelada", "Debe de ingresar una tarea", "warning");
        return false;
    }

    var datos = LlamarAjax("Configuracion/GuardarTarea", "IdEvento=" + id + "&IdTarea=" + idtarea + "&Tarea=" + tarea).split("|");
    if (datos[0] == "0") {
        $("#Tarea").focus();
        AgregarTarea();
        swal("", datos[1], "success");
    } else
        swal("Acción Cancelada", datos[1], "warning");
}

function EditarEvento(id) {
    var datos = LlamarAjax("Configuracion/VerEvento", "IdEvento=" + id);
    var data = JSON.parse(datos);

    $("#IdEvento").val(id);
    $("#RegistroEvento").html('<b>Registro: ' + data[0].registro + '</b>');
    $("#Tipo").val(data[0].tipo).trigger("change");
    $("#Estado").val(data[0].estado).trigger("change");;
    $("#Titulo").val(data[0].titulo);
    $("#Cliente").val(data[0].idcliente);
    $("#Descripcion").val(data[0].descripcion);
    $("#HoraInicio").val((data[0].horainicio == "00:00" ? "" : data[0].horainicio));
    $("#FechaInicio").val(data[0].fechainicio);
    $("#HoraFinal").val((data[0].horafinal == "00:00" ? "" : data[0].horafinal));
    $("#FechaFinal").val(data[0].fechafinal);
    $("#FechaRecordatorio").val(data[0].fecharecordatorio);

    var visitas = data[0].visitas.split("/");
    for (var x = 0; x < visitas.length; x++) {
        if ($.trim(visitas[x]) != "")
            $("#Vistas option[value=" + visitas[x] + "]").prop("selected", true);
    }
    $("#Vistas").trigger("change");

    $("#ModalEvento").modal({ backdrop: 'static' }, 'show');
}

function CrearEvento(fecha, hora) {
    $("#IdEvento").val("0");
    $("#Titulo").val("");
    $("#Descripcion").val("");
    $("#Archivo").val("");
    $("#Cliente").val("0").trigger("change");
    $("#Vistas").val("").trigger("change");

    $("#Tipo").val("Evento").trigger("change");
    $("#Estado").val("Activo").trigger("change");

    $("#FechaInicio").val(fecha);
    $("#HoraInicio").val(hora);
    $("#FechaFinal").val(fecha);
    $("#HoraFinal").val(hora);
    $("#FechaRecordatorio").val(fecha);
    $("#RegistroEvento").html("");
    $("#ModalEvento").modal({ backdrop: 'static' }, 'show');
}

function Fecha_calendario(anio, mes, dia) {
    return anio + '-' + (mes < 10 ? '0' : '') + mes + '-' + (dia < 10 ? '0' : '') + dia;
}

function Fecha_cumple(fecha) {
    fecha = fecha.split("-");
    return fecha[1] + '-' + fecha[2];
}

$("#FormEvento").submit(function (e) {
    e.preventDefault();
    var fechainicio = $("#FechaInicio").val();
    var horainicio = $("#HoraInicio").val();
    var fechafinal = $("#FechaFinal").val();
    var horafinal = $("#HoraFinal").val();
    var parametros = $("#FormEvento").serialize();
    var fecharecordatorio = $("#FechaRecordatorio").val();

    if (fechainicio > fechafinal) {
        $("#FechaInicio").focus();
        swal("Acción Cancelada", "La fecha inicial no puede ser mayor a la fecha final", "warning");
        return false;
    }
    if (fechainicio < FechaActual) {
        $("#FechaInicio").focus();
        swal("Acción Cancelada", "La fecha inicial debe ser mayor o igual a " + fechaactual, "warning");
        return false;
    }
    if (horainicio != "" && horafinal == "") {
        $("#HoraFinal").focus();
        swal("Acción Cancelada", "Debe de ingresar la hora final", "warning");
        return false;
    }
    if (horafinal != "" && horainicio == "") {
        $("#HoraInicio").focus();
        swal("Acción Cancelada", "Debe de ingresar la hora inicial", "warning");
        return false;
    }
    if (fecharecordatorio > fechainicio) {
        $("#FechaRecordatorio").focus();
        swal("Acción Cancelada", "La fecha de recordatorio debe ser mayor o igual a la fecha de inicio", "warning");
        return false;
    }

    var datos = LlamarAjax("Configuracion/GuardarEvento", parametros).split("|");
    if (datos[0] == "0") {
        CargarAgenda(TipoAgenda);
        $("#ModalEvento").modal("hide");
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function GuardarDocumento(id) {
    var documento = document.getElementById(id);
    if ($.trim(documento.value) != "") {

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Configuracion/AdjuntarAgenda";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "warning");
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "warning");
                }
            }
        });
    }
}

function verArchivo(id, url) {
    if (url) {
        window.open(url_archivo + "DocumPDF/" + id + "/" + url);
    }
    else
        swal("Acción Cancelada", "Puede que el link del archivo este roto", "warning");
}

function eliminarArchivo(id, url) {
    if (url) {
        var elimnar = JSON.parse(LlamarAjax("Actividades", "opcion=eliminarArchivo&id=" + id + "&file=" + url));
        if (parseInt(elimnar[0].error) == 0) {
            swal("", elimnar[0].mensaje, "success");
            var consArch = JSON.parse(LlamarAjax("Actividades", "opcion=verArchivo&id=" + id));
            if (parseInt((consArch[0]).error) == 0) {
                let archivos = ((consArch[0]).mensaje).replace(/\|/g, "\"");
                $("#TArchivo").html(archivos);
            } else {

            }
        } else {
            swal("Acción Cancelada", elimnar[0].mensaje, "warning");
        }
    }
    else
        swal("Acción Cancelada", "Puede que el link del archivo este roto", "warning");
}

function EliminarArchivo() {

    var id = $("#IdEvento").val() * 1;
    var tipo = $("#Tipo option:selected").text();
    var titulo = $("#Titulo").val();

    if (id == 0)
        return false;


    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar el archivo del(a) ' + tipo + ' ' + titulo + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Configuracion/EliminarArchivoEvento", "IdEvento=" + id)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};

function FiltroAgenda() {
    CargarAgenda(TipoAgenda);
}


function BuscarFecha() {
    swal({
        title: 'Buscador',
        text: 'Ingrese la fecha que desea buscar',
        html: '<input type="date" id="FechaBuscar" >',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Ir...'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                var fecha = $("#FechaBuscar").val();
                if (fecha != "") {
                    fecha = fecha.replace(/-/g, "/");
                    Fecha = new Date(fecha);
                    Fecha.setHours(0, 0, 0, 0);
                    month = Fecha.getMonth() + 1;
                    day = Fecha.getDate();
                    Anio = Fecha.getFullYear();
                    CargarAgenda(TipoAgenda);
                    resolve();
                } else {
                    reject(ValidarTraduccion('Debe de ingresar una fecha'));
                }
            })
        }
    });
}

CargarAgenda(TipoAgenda);

function ConsularCalendario() {
    var control = $("#CoControl").val() * 1;
	var evaluada = $("#CoEvaluada").val();
    var descripcion = $.trim($("#CoTitulo").val());

    var fechad =  $("#CoFechaDesde").val();
    var fechah =  $("#CoFechaHasta").val();
    
    if (fechad == "" && fechah == "") {
        swal("", ValidarTraduccion("debe de ingresar una fecha desde y una fecha hasta"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {

        var parametros = "fechaini=" + fechad + "&fechafin=" + fechah + "&control=" + control + "&evaluada=" + evaluada + "&descripcion=" + descripcion;
        var datos = LlamarAjax("Actividades","opcion=ConsultarActividades&" + parametros);
        var datasedjson = JSON.parse(datos);
        DesactivarLoad();
        var table = $('#TablaCoCalendario').DataTable({
            data: datasedjson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "consecutivo" },
                { "data": "tx_actividad_motivo" },
                { "data": "fecha_inicio" },
                { "data": "fecha_final" },
                { "data": "registro" },
                { "data": "evaluado" }
                
            ],
            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ],
        });
        
	},15);
}

function GeneradPDF(){
	var fechad =  $("#CoFechaDesde").val();
    var fechah =  $("#CoFechaHasta").val();
    
    if (fechad == "" && fechah == "") {
        swal("", ValidarTraduccion("debe de ingresar una fecha desde y una fecha hasta"), "warning");
        return false;
    }
    
    ActivarLoad();
	setTimeout(function () {
		var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=CronogramaActividad&documento=0&fechad=" + fechad + "&fechah=" + fechah).split("||");
		DesactivarLoad();
		if (datos[0] == "0") {
			window.open(url_archivo + "DocumPDF/" + datos[1]);
		} else {
			swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
		}
		
	}, 15);
    
    
}



$('select').select2();
DesactivarLoad();
