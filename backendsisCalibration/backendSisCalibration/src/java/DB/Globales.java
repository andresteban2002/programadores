package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Sistemas
 */
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.Date;
import java.util.Formatter;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;

public class Globales {

    public static String url_db = null;
    public static String usuario_db = null;
    public static String contrasena_db = null;

    public static String url_archivo = null;
    public static String usa_contabilidad = null;
    public static String ruta_archivo = null;
    public static String ruta_gestion = null;
    public static String ruta_respaldo = null;
    public static String ruta_media_linux = null;

    public static String smtp_host = null;
    public static String smtp_port = null;
    public static String Nombre_Empresa = null;

    public static int MAX_WID = 400;
    public static int MAX_HEIGHT = 400;
    public static List<String> filesListInDir = null;
    public static String[] DiasSemana = new String[]{"Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"};

    @SuppressWarnings("UseSpecificCatch")
    public static Connection BaseDatos() {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(url_db, usuario_db, contrasena_db);
        } catch (Exception e) {
            GuardarLogger(e.getMessage(), "CONEXION A BASE DE DATOS");
            return null;
        }
    }

    public static Connection DBReportes(HttpServletRequest request) {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(url_db, usuario_db, contrasena_db);
        } catch (Exception e) {
            GuardarLogger(e.getMessage(), "CONEXION A BASE DE DATOS");
            return null;
        }
    }

    public static void GuardarLogger(String error, String metodo) {
        try {

            Logger logger = Logger.getLogger("LogdeNacho");
            String pathLog = ruta_archivo + "log/MyLog_" + FechaActual(1) + ".log";

            File logDir = new File("./logs/");
            if (!(logDir.exists())) {
                logDir.mkdirs();
            }

            FileHandler fhandler = new FileHandler(pathLog);
            logger.addHandler(fhandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fhandler.setFormatter(formatter);
            logger.info(metodo + "\n" + error + "\n-----------------------------------------------------------------------------------------------------\n");

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ResultSet Obtenerdatos(String sql, String metodo, int opcion) {
        Connection conec = BaseDatos();
        Statement st = null;
        try {

            if (conec == null) {
                return null;
            }
            ResultSet datos = null;

            if (opcion == 1) {
                st = conec.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                datos = st.executeQuery(sql);
            } else {
                st = conec.createStatement();
                st.executeUpdate(sql);
            }
            conec.close();
            return datos;
        } catch (SQLException ex) {
            GuardarLogger(ex.getMessage() + "\n SQL: " + sql, metodo);
            return null;
        }
    }

    private static String TextoAuditoria(String sql) {
        sql = sql.replace("INSERT INTO", "INSERTANDO").replace("UPDATE", "ACTUALIZANDO").replace("DELETE", "ELIMINADO")
                .replace("FROM", "TABLA").replace("SET", "CAMBIAR").replace("WHERE", "DONDE").replace("'", "");
        sql = sql.replace("insert into", "INSERTANDO").replace("update", "ACTUALIZANDO").replace("delete", "ELIMINADO")
                .replace("from", "TABLA").replace("set", "CAMBIAR").replace("where", "DONDE").replace("'", "");
        return sql;
    }

    public static void GuardarAuditoria(String descripcion, String modulo, String accion, String idusuario, String iplocal) {
        String sql = "INSERT INTO seguridad.auditoria(idususario, modulo, accion, consulta, ip) "
                + "VALUES (" + idusuario + ",'" + modulo + "','" + accion + "','" + TextoAuditoria(descripcion) + "','" + iplocal + "')";
        Obtenerdatos(sql, "GuardarAuditoria", 2);

    }

    public static Boolean DatosAuditoria(String sql, String modulo, String accion, String idusuario, String iplocal, String metodo) {
        Connection conec = BaseDatos();
        Statement st = null;
        try {
            if (conec == null) {
                return null;
            }
            st = conec.createStatement();
            st.executeUpdate(sql);

            sql = "INSERT INTO seguridad.auditoria(idususario, modulo, accion, consulta, ip) "
                    + "VALUES (" + idusuario + ",'" + modulo + "','" + accion + "','" + TextoAuditoria(sql) + "','" + iplocal + "')";
            Obtenerdatos(sql, "GUARDAR AUDITORIA", 2);

            return true;

        } catch (SQLException ex) {
            GuardarLogger(ex.getMessage() + "\n SQL: " + sql, metodo);
            return false;
        }
    }

    /*public static String DatosAuditoriaValor(String sql, String modulo, String accion, String idusuario, String iplocal) {
        Connection conec = BaseDatos();
        Statement st = null;
        String valor = "";
        try {

            ResultSet datos = null;
            st = conec.createStatement();

            datos = st.executeQuery(sql);

            sql = "INSERT INTO seguridad.auditoria(idususario, modulo, accion, consulta, ip) "
                    + "  VALUES (" + idusuario + ",'" + modulo + "','" + accion + "','" + TextoAuditoria(sql) + "','" + iplocal + "')";
            Obtenerdatos(sql, "DatosAuditoriaValor", 2);
            if (datos.next()) {
                valor = datos.getString(1).trim();
            }

        } catch (SQLException ex) {
            GuardarLogger(ex.getMessage(), "DatosAuditoriaValor");

        }
        return valor;
    }*/
    public static String AntiJaquer(String cadena) {
        cadena = cadena.replaceAll("DROP ", "");
        cadena = cadena.replaceAll("SELECT ", "");
        cadena = cadena.replaceAll("ALTER ", "");
        cadena = cadena.replaceAll("DELETE ", "");
        cadena = cadena.replaceAll("UPDATE ", "");
        cadena = cadena.replaceAll("OR ", "");
        cadena = cadena.replaceAll("AND ", "");
        cadena = cadena.replaceAll("INSERT ", "");
        cadena = cadena.replaceAll("ORDER BY ", "");
        return cadena;

    }

    public static String ObtenerDatosJSon(String sql, String metodo) {

        try {

            Connection conec = BaseDatos();
            Statement st = null;
            String Resultado = "[]";

            if (conec == null) {
                return null;
            }

            st = conec.createStatement();
            sql = "select json_agg(t.*) as json from (" + sql + ") as t";
            ResultSet datos = st.executeQuery(sql);

            if (datos.next()) {
                if (datos.getString(1) != null) {
                    Resultado = datos.getString(1);
                }
            }
            datos.close();
            st.close();
            conec.close();
            return Resultado;
        } catch (SQLException ex) {
            GuardarLogger(ex.getMessage() + "\n SQL: " + sql, metodo);
            return null;
        }
    }

    public static String ObtenerUnValor(String sql) {

        try {

            Connection conec = BaseDatos();
            Statement st = null;
            String Resultado = "";

            if (conec == null) {
                return null;
            }

            st = conec.createStatement();
            ResultSet datos = st.executeQuery(sql);

            if (datos.next()) {
                if (datos.getString(1) != null) {
                    Resultado = datos.getString(1);
                }
            }
            datos.close();
            st.close();
            conec.close();
            return Resultado;
        } catch (SQLException ex) {
            GuardarLogger(ex.getMessage() + "\n SQL: " + sql, "OBTENER UN VALOR");
            return null;
        }
    }

    public static String IpUsuario(String idusuario) {
        String sql = "SELECT sesion from seguridad.rbac_usuario where idusu = " + idusuario;
        return ObtenerUnValor(sql);
    }

    public static String QuitarCero(String numero) {

        if (numero.trim().equals("") || numero.trim().equals("0")) {
            return "0";
        }

        String resultado = "";
        String caracter = "";
        int i = 0;
        for (i = 0; i < numero.length(); i++) {
            caracter = numero.substring(i, (i + 1));
            if (!caracter.equals("0")) {
                resultado += caracter;
                break;
            }
        }
        int tam = numero.length();
        if ((tam - 1) != i) {
            resultado = resultado + numero.substring(i + 1, tam).trim();
        }
        return resultado;
    }

    public static int Validarintnonull(String numero) {
        if (numero == null) {
            return 0;
        }
        numero = numero.trim();
        int resultado = 0;
        if (isNumeric(numero)) {
            resultado = Integer.parseInt(numero);
        }
        return resultado;
    }

    public static double ValidardoublenoNull(String numero) {
        if (numero == null) {
            return 0;
        }
        double resultado = 0;
        if (isNumeric(numero)) {
            resultado = Double.parseDouble(numero);
        }
        return resultado;
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static Date ValidarFecha(String fecha) {
        Date resultado = null;
        try {
            SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
            formatoFecha.setLenient(false);
            resultado = formatoFecha.parse(fecha);
            return resultado;
        } catch (ParseException ex) {
            GuardarLogger(ex.getMessage(), "Validar-Fecha");
            return null;
        }
    }

    public static String FechaActual(int tipo) {
        Date fecha = new Date();
        DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat dateFormatym = new SimpleDateFormat("yyyy-MM");
        DateFormat dateFormat3 = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat formato = new SimpleDateFormat("EEEE d 'de' MMMM 'de' yyyy", new Locale("es_ES"));
        String resultado = "";
        switch (tipo) {
            case 1:
                resultado = dateFormat.format(fecha);
                break;
            case 2:
                resultado = dateFormat.format(fecha) + " " + hourFormat.format(fecha);
                break;
            case 3:
                resultado = dateFormat3.format(fecha);
                break;
            case 4:
                resultado = formato.format(fecha) + " " + hourFormat.format(fecha);
                break;
            case 5:
                resultado = dateFormatym.format(fecha);
                break;
        }
        return resultado;
    }

    public static String EncryptKey(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static String ObtenerCombo(String sql, int inicial, int titulo, int inactivo, int json) {
        String combo = "";
        if (json == 1) {
            combo = "[";
        }
        if (inicial == 1) {
            if (json == 0) {
                combo = "<option value='' idioma='--Seleccione--'>--Seleccione--</option>";
            }
        }
        if (inicial == 2) {
            if (json == 0) {
                combo = "<option value='' idioma='TODOS'>TODOS</option>";
            }
        }
        if (inicial == 3) {
            if (json == 0) {
                combo = "<option value='0' idioma='VER ESPECIFICACIONES'>VER ESPECIFICACIONES</option>";
            }
        }
        if (inicial == 4) {
            if (json == 0) {
                combo = "<option value='0' idioma='TODOS'>TODOS</option>";
            }
        }
        if (inicial == 5) {
            if (json == 0) {
                combo = "<option value='11' idioma='CALIBRATION SERVICE SAS'>CALIBRATION SERVICE SAS</option>";
            }
        }
        if (inicial == 6) {
            if (json == 0) {
                combo = "<option value='AUTORIZACIÓN'>AUTORIZACIÓN</option>";
            }
        }
        if (inicial == 7) {
            if (json == 0) {
                combo = "<option value='0'>NINGUNO</option>";
            }
        }
        if (inicial == 8) {
            if (json == 0) {
                combo = "<option value='0'>AMBOS SENTIDOS</option>";
            }
        }
        if (inicial == 9) {
            if (json == 0) {
                combo = "<option value='0'>SIN ASIGNAR</option>";
            }
        }

        ResultSet datos = Obtenerdatos(sql, "ObtenerCombo", 1);

        try {
            while (datos.next()) {
                if (json == 0) {
                    combo += "<option  value='" + datos.getString(1) + "' idioma='" + datos.getString(2).trim() + "'" + (titulo != 0 ? " title='" + datos.getString(3) + "'" : "") + (inactivo == 1 ? (datos.getString(4).equals("1") ? " disabled " : "") : "") + ">" + datos.getString(2) + "</option>";
                } else {
                    if (!combo.equals("[")) {
                        combo += ",";
                    }
                    combo += "{\"id\":\"" + datos.getString(1) + "\",\"value\":\"" + datos.getString(2) + "\",\"title\":\"" + (titulo != 0 ? datos.getString(3) : "") + "\",\"inactivo\":\"" + (inactivo == 1 ? datos.getString(4) : "") + "\"}";
                }
            }

            if (json == 1) {
                combo += "]";
            }

        } catch (SQLException ex) {
            GuardarLogger(ex.getMessage(), "ObtenerCombo");
        }
        return combo;
    }

    public static String ObtenerComboDatos(ResultSet datos, int inicial) {
        String combo = "";
        if (inicial == 1) {
            combo = "<option value='' idioma='--Seleccione--'>--Seleccione--</option>";
        }
        if (inicial == 2) {
            combo = "<option value='' idioma='TODOS'>TODOS</option>";
        }
        if (inicial == 3) {
            combo = "<option value='0' idioma='VER ESPECIFICACIONES'>VER ESPECIFICACIONES</option>";
        }
        if (inicial == 4) {
            combo = "<option value='0' idioma='TODOS'>TODOS</option>";
        }
        if (inicial == 5) {
            combo = "<option value='11' idioma='CALIBRATION SERVICE SAS'>CALIBRATION SERVICE SAS</option>";
        }
        if (inicial == 6) {
            combo = "<option value='AUTORIZACIÓN'>AUTORIZACIÓN</option>";
        }
        if (inicial == 7) {
            combo = "<option value='0'>NINGUNO</option>";
        }
        try {
            while (datos.next()) {
                combo += "<option value='" + datos.getString(1) + "' idioma='" + datos.getString(2).trim() + "'>" + datos.getString(2).trim() + "</option>";
            }
        } catch (SQLException ex) {
            GuardarLogger(ex.getMessage(), "ObtenerComboDatos");
        }
        return combo;
    }

    public static int PermisosSistemas(String Accion, String usuario) {
        int permiso = 0;
        String sql = "SELECT max(idgruper) "
                + " FROM seguridad.rbac_grupo_permiso gp INNER JOIN seguridad.rbac_usuario_grupo ug using (idgru) "
                + "		                                         INNER JOIN seguridad.rbac_modulo_accion ma using(idmodacc) "
                + "                                     INNER JOIN seguridad.rbac_grupo g using(idgru) "
                + "                                     INNER JOIN seguridad.rbac_modulo m using(idmod) "
                + "                                     INNER JOIN seguridad.rbac_usuario u using (idusu) "
                + " WHERE estusu = 'ACTIVO' and estgru = 'ACTIVO' AND estmod = 'ACTIVO' and ((desacc = '" + Accion + "') or (supusu =1)) AND idusu = " + usuario;
        permiso = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        return permiso;
    }

    public static String Codigo_Plantilla(String documento) {
        String sql = "SELECT codigo from version_documento where documento = '" + documento + "' ORDER BY revision DESC LIMIT 1";
        return Globales.ObtenerUnValor(sql);
    }

    public static String ValidarRutaGestion(String documento, String numero, String fecha, int plantilla) {
        String ruta = ObtenerUnValor("select ruta from empresa_ruta_gestion where documento = '" + documento + "'");
        ResultSet datos = null;
        String resultado;
        String Anio;
        String Mes;
        String Mes_Corto;
        String Magnitud;
        String NPlantilla;
        String sql;
        if (ruta.equals("")) {
            return "";
        }
        try {
            switch (documento) {
                case "Cotizacion":
                case "Cotizacion_Servicio":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud, '" + Codigo_Plantilla("COTIZACION") + "' as plantilla \n"
                            + "from cotizacion where cotizacion = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "Remision_Cliente":
                case "Remision":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud, '" + Codigo_Plantilla("REMISION") + "' as plantilla \n"
                            + "from remision where remision = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "Devolucion":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud, '" + Codigo_Plantilla("DEVOLUCION") + "' as plantilla \n"
                            + "from devolucion where devolucion = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "EtiquetaDevolucion":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud, '' as plantilla \n"
                            + "from imprimir_etiqueta where devolucion = " + numero + " limit 1";
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "Solicitud":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud, '" + Codigo_Plantilla("SOLICITUD") + "' as plantilla \n"
                            + "from web.solicitud where solicitud = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "Salida":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud, '" + Codigo_Plantilla("SALIDA") + "' as plantilla \n"
                            + "from salida where salida = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "OrdenCompra":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud, '" + Codigo_Plantilla("ORDENCOMPRA") + "' as plantilla \n"
                            + "from ordencompra_tercero where orden = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "Factura":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud, '" + Codigo_Plantilla("FACTURA") + "' as plantilla \n"
                            + "from factura where factura = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "InformeTecnico":
                    sql = "select to_char(if.fecha,'yyyy') as anio, obtener_mes(to_char(if.fecha,'MM')::int) as mes, obtener_mes_corto(to_char(if.fecha,'MM')::int) as mes_corto, m.sistema_gestion as magnitud, '' as plantilla \n"
                            + "from informetecnico_dev if inner join remision_detalle rd on rd.ingreso = if.ingreso "
                            + "                           inner join equipo e on e.id = rd.idequipo "
                            + "                           inner join magnitudes m on m.id = e.idmagnitud "
                            + "where if.id = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "CertificadosEXCEl":
                case "CertificadosPDF":
                case "CertificadosExternos":
                case "InformeTercerizado":
                    sql = "select to_char('" + fecha + "'::date,'yyyy') as anio, obtener_mes(to_char('" + fecha + "'::date,'MM')::int) as mes, obtener_mes_corto(to_char('" + fecha + "'::date,'MM')::int) as mes_corto, m.sistema_gestion as magnitud, p.codigo as plantilla \n"
                            + "from remision_detalle rd inner join equipo e on e.id = rd.idequipo "
                            + "                         inner join magnitudes m on m.id = e.idmagnitud "
                            + "                         inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso and idplantilla = " + plantilla
                            + "                         inner join plantillas p on p.id = ip.idplantilla "
                            + "where rd.ingreso = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "CertificadoParTorsionalV07":
                    sql = "select to_char(c.fecha,'yyyy') as anio, obtener_mes(to_char(c.fecha,'MM')::int) as mes, obtener_mes_corto(to_char(c.fecha,'MM')::int) as mes_corto, m.sistema_gestion as magnitud, p.codigo as plantilla \n"
                            + "from certificados c inner join remision_detalle rd on rd.ingreso = c.ingreso "
                            + "                         inner join equipo e on e.id = rd.idequipo "
                            + "                         inner join magnitudes m on m.id = e.idmagnitud "
                            + "                         inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso and idplantilla = c.item "
                            + "                         inner join plantillas p on p.id = ip.idplantilla "
                            + "where c.id = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "ResumenDiligencias":
                    sql = "select to_char(v.fechapro,'yyyy') as anio, obtener_mes(to_char(v.fechapro,'MM')::int) as mes, obtener_mes_corto(to_char(v.fechapro,'MM')::int) as mes_corto, '' as magnitud, '" + Codigo_Plantilla("DILIGENCIAS") + "' as plantilla\n"
                            + " from registrovueltas v  "
                            + " WHERE id = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "NotasCreditos":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud, '" + Codigo_Plantilla("NOTASCREDITO") + "' as plantilla \n"
                            + "from nota_credito where nota = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "CondicionesAmbientales":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud, '" + Codigo_Plantilla("CONDICIONESAMBIENTALES") + "' as plantilla \n"
                            + "from condiciones_pdf where id = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "CronogramaActividad":
                    sql = "select to_char('" + fecha + "'::date,'yyyy') as anio, obtener_mes(to_char('" + fecha + "'::date,'MM')::int) as mes, obtener_mes_corto(to_char('" + fecha + "'::date,'MM')::int) as mes_corto, '' as magnitud, '" + Codigo_Plantilla("CRONOGRAMADEACTIVIDADES") + "' as plantilla ";
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;

            }
            if (datos != null) {
                datos.next();
                Anio = datos.getString("anio");
                Mes = datos.getString("mes");
                Mes_Corto = datos.getString("mes_corto");
                Magnitud = datos.getString("magnitud");
                NPlantilla = datos.getString("plantilla");

                resultado = ruta_gestion + ruta.replaceAll("\\[ANIO]", Anio);
                resultado = resultado.replaceAll("\\[MES]", Mes);
                resultado = resultado.replaceAll("\\[MES_CORTO]", Mes_Corto);
                resultado = resultado.replaceAll("\\[MAGNITUD]", Magnitud);
                resultado = resultado.replaceAll("\\[PLANTILLA]", NPlantilla);
                File archivo = new File(resultado);
                if (!archivo.exists()) {
                    if (archivo.mkdirs()) {
                        return resultado;
                    } else {
                        return "";
                    }
                } else {
                    return resultado;
                }
            } else {
                return "";
            }
        } catch (Exception e) {
            GuardarLogger(e.getMessage(), "ValidarRutaGestion");
            return "";
        }
    }

    public static boolean ValidarRuta(String ruta) {
        String sql = "select id from empresa_rutaspermitidas where ruta = '" + ruta + "'";
        int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (encontrado > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static String ExtensionArchivo(String imageName) {
        String[] archivo = imageName.split("\\.");
        return archivo[archivo.length - 1];
    }

    //public static void enviarConGMail(String destinatario, String asunto, String cuerpo) {
    public static String EnviarEmail(String Destinatario, String laboratorio, String[] DestinatariosCCO, String Asunto, String Mensaje, String CorreoEmpresa, String Adjunto, String correoempenv, String clavecorreoempenv, String nombrearchivo) {
        // Esto es lo que va delante de @gmail.com en tu cuenta de correo. Es el remitente también.

        Properties props = System.getProperties();
        props.put("mail.smtp.host", Globales.smtp_host);  //El servidor SMTP de Google
        props.put("mail.smtp.user", correoempenv);
        props.put("mail.smtp.clave", clavecorreoempenv);    //La clave de la cuenta
        props.put("mail.smtp.auth", "true");    //Usar autenticación mediante usuario y clave
        props.put("mail.smtp.starttls.enable", "false"); //Para conectar de manera segura al servidor SMTP
        props.put("mail.smtp.port", Globales.smtp_port); //El puerto SMTP seguro de Google

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(correoempenv));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(Destinatario));

            message.addHeader("Content-Type", "text/html; charset=UTF-8");
            for (int x = 0; x < DestinatariosCCO.length; x++) {
                if (!DestinatariosCCO[x].trim().equals("")) {
                    if (!DestinatariosCCO[x].trim().equals(Destinatario.trim()) && !DestinatariosCCO[x].trim().equals(Destinatario)) {
                        message.addRecipients(Message.RecipientType.BCC, new InternetAddress[]{new InternetAddress(DestinatariosCCO[x].trim())});
                    }
                }
            }
            message.setSubject(Asunto);

            BodyPart texto = new MimeBodyPart();
            texto.setContent(Mensaje, "text/html");

            BodyPart adjunto;
            MimeMultipart multiParte = new MimeMultipart();

            if (!Adjunto.equals("")) {

                String[] a_adjunto = Adjunto.split(",");
                String[] a_nombre = nombrearchivo.split(",");
                for (int x = 0; x < a_adjunto.length; x++) {

                    adjunto = new MimeBodyPart();
                    DataSource source = new FileDataSource(a_adjunto[x].trim());
                    adjunto.setDataHandler(new DataHandler(source));
                    adjunto.setFileName(a_nombre[x].trim());
                    multiParte.addBodyPart(adjunto);

                    /*DataSource source = new FileDataSource("/var/www/html/Calibration/DocumPDF/Cotizacion_20200302155333.pdf");
                    adjunto.setDataHandler(new DataHandler(source));
                    adjunto.setFileName("cotizacion_01.pdf");
                    multiParte.addBodyPart(adjunto);*/
                }
            }

            multiParte.addBodyPart(texto);
            message.setContent(multiParte);

            Transport transport = session.getTransport("smtp");
            transport.connect(Globales.smtp_host, correoempenv, clavecorreoempenv);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            return "";
        } catch (MessagingException me) {
            return me.getMessage();   //Si se produce un error
        }
    }

    public static boolean ComprimirIamgen(String Copia, String Original) {
        try {
            File input = new File(Copia);
            BufferedImage image = ImageIO.read(input);
            File compressedImageFile = new File(Original);
            OutputStream os = new FileOutputStream(compressedImageFile);
            Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpeg");
            ImageWriter writer = (ImageWriter) writers.next();
            ImageOutputStream ios = ImageIO.createImageOutputStream(os);
            writer.setOutput(ios);
            ImageWriteParam param = writer.getDefaultWriteParam();
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(0.75f);  // Change the quality value you prefer
            writer.write(null, new IIOImage(image, null, null), param);
            //input.delete();
            os.close();
            ios.close();
            writer.dispose();
            return true;
        } catch (IOException ex) {
            GuardarLogger(ex.getMessage(), "ComprimirIamgen");
            return false;
        }
    }

    public static boolean ConvertirJPG(File input, File output) {

        try {

            BufferedImage image = ImageIO.read(input);
            BufferedImage result = new BufferedImage(
                    image.getWidth(),
                    image.getHeight(),
                    BufferedImage.TYPE_INT_RGB);
            result.createGraphics().drawImage(image, 0, 0, Color.WHITE, null);
            ImageIO.write(result, "jpg", output);

            return true;

        } catch (IOException e) {
            GuardarLogger(e.getMessage(), "ConvertirJPG");
            return false;
        }
    }

    public static boolean CortarImagen(File input, File output, int x1, int y1, int w, int h) {

        try {

            BufferedImage image = ImageIO.read(input);
            BufferedImage result = ((BufferedImage) image).getSubimage(x1, y1, w, h);
            result.createGraphics().drawImage(image, 0, 0, Color.WHITE, null);
            ImageIO.write(result, "jpg", output);

            return true;

        } catch (IOException e) {
            GuardarLogger(e.getMessage(), "CortarImagen");
            return false;
        }
    }

    public static boolean zipDirectory(File dir, String zipDirName) {
        try {
            filesListInDir = new ArrayList<String>();
            populateFilesList(dir);
            //now zip files one by one
            //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipDirName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            for (String filePath : filesListInDir) {
                System.out.print("Zipping " + filePath);
                //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
                ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length() + 1, filePath.length()));
                zos.putNextEntry(ze);
                //read the file and write to ZipOutputStream
                FileInputStream fis = new FileInputStream(filePath);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                zos.closeEntry();
                fis.close();
            }
            zos.close();
            fos.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static void populateFilesList(File dir) throws IOException {
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                filesListInDir.add(file.getAbsolutePath());
            } else {
                populateFilesList(file);
            }
        }
    }

    public static void copiarDirectorio(String origen, String destino) {
        comprobarCrearDirectorio(destino);
        File directorio = new File(origen);
        File f;
        if (directorio.isDirectory()) {
            comprobarCrearDirectorio(destino);
            String[] files = directorio.list();
            if (files.length > 0) {
                for (String archivo : files) {
                    f = new File(origen + File.separator + archivo);
                    if (f.isDirectory()) {
                        comprobarCrearDirectorio(destino + File.separator + archivo + File.separator);
                        copiarDirectorio(origen + File.separator + archivo + File.separator, destino + File.separator + archivo + File.separator);
                    } else { //Es un archivo
                        copiarArchivo(origen + File.separator + archivo, destino + File.separator + archivo);
                    }
                }
            }
        }
    }

    private static void comprobarCrearDirectorio(String ruta) {
        File directorio = new File(ruta);
        if (!directorio.exists()) {
            directorio.mkdirs();
        }
    }

    private static void copiarArchivo(String sOrigen, String sDestino) {
        try {
            if (ValidarExtension(sOrigen)) {
                File origen = new File(sOrigen);
                File destino = new File(sDestino);
                InputStream in = new FileInputStream(origen);
                OutputStream out = new FileOutputStream(destino);

                byte[] buf = new byte[1024];
                int len;

                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }

                in.close();
                out.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean ValidarExtension(String archivo) {
        archivo = archivo.toLowerCase();
        if (archivo.endsWith(".jpg") || archivo.endsWith(".png") || archivo.endsWith(".gif") || archivo.endsWith(".doc")
                || archivo.endsWith(".docx") || archivo.endsWith(".xls") || archivo.endsWith(".xlsx") || archivo.endsWith(".xlsm")
                || archivo.endsWith(".pdf") || archivo.endsWith(".ppt") || archivo.endsWith(".pptx") || archivo.endsWith(".et")
                || archivo.endsWith(".ett") || archivo.endsWith(".dps") || archivo.endsWith(".dpt") || archivo.endsWith(".pps")
                || archivo.endsWith(".pot") || archivo.endsWith(".ppsx") || archivo.endsWith(".txt") || archivo.endsWith(".wpt")
                || archivo.endsWith(".wps") || archivo.endsWith(".wpt") || archivo.endsWith(".jpeg") || archivo.endsWith(".ai")
                || archivo.endsWith(".psd") || archivo.endsWith(".eps") || archivo.endsWith(".svg")) {
            return true;

        } else {
            return false;

        }

    }

    public static int DiaSemana(Date fecha) {
        DateFormat formatodia = new SimpleDateFormat("dd");
        DateFormat formatomes = new SimpleDateFormat("MM");
        DateFormat formatoanio = new SimpleDateFormat("yyyy");
        int dia = Validarintnonull(formatodia.format(fecha));
        int mes = Validarintnonull(formatomes.format(fecha));
        int anio = Validarintnonull(formatoanio.format(fecha));
        TimeZone timezone = TimeZone.getDefault();
        Calendar calendar = new GregorianCalendar(timezone);
        calendar.set(anio, mes - 1, dia);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    public static String TituloNomina(String periodo) {
        String sql = "SELECT codigo || ' ' || titulo || ' (' || n.tipo || ') ' || p.numero || ' - Inicio: ' || to_char(p.fechainicio,'dd/MM/yyyy') || ' Fin: ' || to_char(p.fechafinal,'dd/MM/yyyy') as descripcion "
                + " FROM rrhh.nominas n inner join  rrhh.nominas_periodos p on p.idnomina = n.id "
                + " WHERE p.id = " + periodo;
        return ObtenerUnValor(sql);
    }

    public static String NumeroComa(String numero) {
        return numero.trim().replace(",", ".");
    }

    public static String ConvertirNumero(String numero) {
        String valor = "";
        String caracter;
        numero = numero.replaceAll("\\,", ",");

        for (int i = 0; i < numero.length(); i++) {
            caracter = numero.substring(i, (i + 1));
            if (!caracter.equals(" ") && !caracter.equals("(") && !caracter.equals(")") && !caracter.equals("$")) {
                valor += caracter;
            }
        }

        if (numero.indexOf("(") > -1) {
            valor = "-" + valor;
        }
        return valor;
    }

    public static String ConvertirFecha(String fecha, int tipo) {
        String resultado = "";
        String[] valor;
        int mes = 0;
        int dia = 0;
        Formatter ceros_mes = new Formatter();
        Formatter ceros_dias = new Formatter();
        switch (tipo) {
            case 1:
                String[] meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
                valor = fecha.split(" ");

                for (int x = 0; x < meses.length; x++) {
                    if (valor[0].trim().toUpperCase().equals(meses[x].toUpperCase())) {
                        mes = x + 1;
                        break;
                    }
                }
                dia = Validarintnonull(valor[1]);
                resultado = valor[3] + "-" + ceros_mes.format("%02d", mes) + "-" + ceros_dias.format("%02d", dia);
                break;
            case 2:
                String[] subvalor = fecha.split(" ");
                valor = subvalor[0].split("/");
                dia = Validarintnonull(valor[0]);
                resultado = valor[2] + "-" + ceros_mes.format("%02d", Validarintnonull(valor[1])) + "-" + ceros_dias.format("%02d", dia);
                break;
            case 3:
                valor = fecha.split("-");
                resultado = valor[2] + "-" + valor[1] + "-" + valor[0];
                break;
            case 4:
                String[] meses2 = {"ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"};
                valor = fecha.split("-");
                for (int x = 0; x < meses2.length; x++) {
                    if (valor[0].trim().toUpperCase().equals(meses2[x].toUpperCase())) {
                        mes = x + 1;
                        break;
                    }
                }
                resultado = valor[2] + "-" + ceros_mes.format("%02d", mes) + "-" + valor[1];
                break;
            case 5:
                String[] meses3 = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
                valor = fecha.split(",");
                int posicion = 0;
                if (valor.length == 1) {
                    posicion = -1;
                }
                valor = fecha.split(" ");
                dia = Validarintnonull(valor[1 + posicion]);
                for (int x = 0; x < meses3.length; x++) {
                    if (valor[2 + posicion].trim().toUpperCase() == meses3[x].toUpperCase()) {
                        mes = x + 1;
                        break;
                    }
                }
                resultado = valor[3 + posicion] + "-" + ceros_mes.format("%02d", mes) + "-" + ceros_dias.format("%02d", dia) + " " + valor[4 + posicion];
                break;
            case 6:
                valor = fecha.split("/");
                resultado = valor[2] + "-" + valor[1] + "-" + valor[0];
                break;
        }
        return resultado;
    }

    public static String neescape(String texto) {
        
        if (texto == null)
            return "";

        String[] caracteres = new String[]{"Á", "á", "É", "é", "Í", "í", "Ó", "ó", "Ú", "ú", "Ü", "ü", "Ṅ", "ñ", "&", "<", ">", "“", "‘", "©", "®", "€", "°", "²", "³", "µ", "¼", "½", "¾", "π", "%","´"};
        String[] cambios = new String[]{"u00C1", "u00E1", "u00C9", "u00E9", "u00CD", "u00ED", "u00D3", "u00F3", "u00DA", "u00FA", "u00DC", "u00FC", "u00D1", "u00F1", "u0022", "u003C", "u003E", "u0022", "u0027", "u00A9", "u00AE", "u20AC", "u00B0", "u00B2", "u00B3", "u00B5", "u00BC", "u00BD", "u00BE", "u03C0", "u0025","uff0c"};
        for (int x = 0; x < caracteres.length; x++) {
            texto = texto.replace(cambios[x], caracteres[x]);
        }
        return texto;
    }

}
