d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


var output = d.getFullYear() + '-' +
        (month < 10 ? '0' : '') + month + '-01';

var output2 = d.getFullYear() + '-' +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day;

$(document).on('hidden.bs.modal', function (event) {
    if ($('.modal:visible').length) {
        $('body').addClass('modal-open');
    }
});

function NumCheck(tecla, field, decimal) {
        
    if ((decimal == 0) && (tecla.charCode == 46))
        return false;
    if ((tecla.charCode < 48 || tecla.charCode > 57) && (tecla.charCode != 46) && (tecla.charCode != 8)) {
        return false;
    } else {
        var len = field.value.length;
        var index = field.value.indexOf('.');

        if (index > 0 && tecla.charCode == 46) {
            return false;
        }

        if (index > 0) {
            var CharAfterdot = (len + 1) - index;
            if (CharAfterdot > (decimal + 1)) {
                return false;
            }
        }
    }
}


AppAngular.controller('Principal', function ($scope, $rootScope, $http, General, Socker) {

    $scope.ActivarCargando = function (opcion) {
        $scope.Cargando = opcion;
        alert($scope.Cargando);
    }

    /************************* PRUEBA DE CARGA *****************************************************/
    $scope.scopeHeader = "Este es un elemento del header";
    $scope.global.firstName = "eurides";
    $scope.lastName = "Doe";
    $scope.fullName = function () {
        return $scope.global.firstName + " " + $scope.global.lastName;
    };

    $rootScope.TraducirIdioma = function (Idioma) {
        Idioma = Idioma * 1;
        if (Idioma == 0) {
            Idioma = $rootScope.Idioma.numero;
        }
        if ($rootScope.Idioma.cambiar == 1) {
            sessionStorage.ArchivoIdioma = "";
            $rootScope.Idioma.cambiar == 0;
        }

        sessionStorage.Idioma = Idioma;

        $rootScope.Idioma.numero = Idioma;
        $rootScope.Idioma.clase = [" ", " "];
        $rootScope.Idioma.clase[Idioma - 1] = "active";
        $rootScope.Idioma.LenguajeDataTable = $rootScope.Conexion.url_cliente + $rootScope.Idioma.TipoLenguajeDataTable[Idioma - 1];

        if ((sessionStorage.ArchivoIdioma == null) || (sessionStorage.ArchivoIdioma == "")) {
            General.LlamarAjax("Idioma", "opcion=LlamarArchivoIdioma&Idioma=" + Idioma, false, "", 1).then(function (response) {
                $rootScope.Idioma.ArchivoIdioma = response;
                sessionStorage.ArchivoIdioma = response;
                $rootScope.ModulosTraducir();
            });
        } else {
            $rootScope.Idioma.ArchivoIdioma = localStorage.getItem("ArchivoIdioma");
            $rootScope.ModulosTraducir();
        }

    }

    $rootScope.InicializarFormulario = function (opcion) {
        $rootScope.global.InicializarClass(opcion);
        $rootScope.ValidarSesion();
        $rootScope.global.classbody = "sidebar-narrow";
        $rootScope.Chat.vermensaje = "";
    }

    $rootScope.global.ActivarOpcion = function (opcion) {
        if ($rootScope.global.classbody == "sidebar-narrow") {
            $rootScope.global.display[opcion] = ($rootScope.global.display[opcion] ? false : true);
        }
    }

    $rootScope.ValidarSesion = function () {
        General.LlamarAjax("InicioSesion", "opcion=ValidarSesion", true, "", 1).then(function (response) {
            if (!response[0]) {
                $rootScope.InicioSesion.usuario = "";
                window.location.href = $rootScope.Conexion.url_cliente + "Inicio/#!/ini_entrar";
            }
            if (response[0].estado == "activo") {
                if ($rootScope.InicioSesion.usuario == "") {
                    $rootScope.InicioSesion.estado = response[0].estado;
                    $rootScope.InicioSesion.idioma = response[0].idioma
                    $rootScope.InicioSesion.usuario = response[0].usuario;
                    $rootScope.InicioSesion.empleado = response[0].empleado;
                    $rootScope.InicioSesion.cargo = response[0].cargo;
                    $rootScope.InicioSesion.codusu = response[0].nomusu;
                    $rootScope.InicioSesion.idusuario = response[0].idusuario;
                    $rootScope.InicioSesion.alertas_mensaje = response[0].alertas_mensaje;
                    $rootScope.InicioSesion.foto = response[0].foto;
                    Socker.init_socker();
                    $scope.LlenarChat();
                    General.DatosEmpresa();
                    $rootScope.barraauxiliar = true;
                }
                $rootScope.TraducirIdioma(sessionStorage.Idioma ? sessionStorage.Idioma : $rootScope.InicioSesion.idioma);
            } else {
                $rootScope.InicioSesion.usuario = "";
                window.location.href = $rootScope.Conexion.url_cliente + "Inicio/#!/ini_entrar";
            }
        });
    }

    $rootScope.MenuCerrarSesion = function (redireccionar) {
        General.LlamarAjax("InicioSesion", "opcion=CerrarSession", false, "", 1).then(function (response) {
            $rootScope.InicioSesion.usuario = "";
            sessionStorage.Idioma = null;
            $rootScope._DE = 0;
            $rootScope._CM = "";
            $rootScope._CD = "";
            $rootScope._SM = "";
            $rootScope._TIPOCOTIZACION = 0;
            Socker.clean();
            if (redireccionar == 1) {
                window.location.href = $rootScope.Conexion.url_cliente + "Inicio/#!/ini_entrar";
            }
        });
    }

    $scope.LlenarChat = function () {
        General.LlamarAjax("Configuracion", "opcion=ObtenerChat", false, "", 1).then(function (response) {
            $rootScope.Chat.tabla = response;
            if ($rootScope.Chat.tabla.length > 0)
                $rootScope.Chat.id = $rootScope.Chat.tabla[0].id;
            else
                $rootScope.Chat.id = 0;
        });
    }

    $rootScope.MostrarMensaje = function (msg) {
        console.log(msg);
        $rootScope.Chat.id = $rootScope.Chat.id + 1;
        var nuevomensaje = {
            codusu: msg.username,
            fecha: msg.fecha,
            foto: $rootScope.InicioSesion.foto,
            id: $rootScope.Chat.id,
            idusuario: msg.vista,
            mensaje: msg.message,
            usuario: null
        }

        $rootScope.Chat.tabla.push(nuevomensaje);

        if (msg.username != $rootScope.InicioSesion.codusu && ($.trim(msg.vista) == "0") || ($.trim(msg.vista).indexOf("/" + $rootScope.InicioSesion.idusuario + "/") > -1)) {
            if ($rootScope.Chat.vermensaje == "SI") {
                $.jGrowl("Tienes un mensaje de " + msg.username, {life: 2500, theme: 'growl-success', header: ''});
                //MensajeLeido();
            } else {
                swal.queue([{
                        title: $rootScope.ValidarTraduccion('Tienes un Mensaje!!!'),
                        text: msg.username,
                        type: 'info',
                        showLoaderOnConfirm: true,
                        showCancelButton: true,
                        confirmButtonText: $rootScope.ValidarTraduccion('Ver Mensaje'),
                        cancelButtonText: $rootScope.ValidarTraduccion('Cancelar'),
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        preConfirm: function () {
                            return new Promise(function (resolve, reject) {
                                $rootScope.LlamarChat();
                                resolve();
                            })
                        }
                    }]);
            }
            General.EscucharMensaje("Tienes un mensaje de " + msg.username.toLowerCase());
        }
    }

    $rootScope.LlamarChat = function () {
        window.location.href = $rootScope.Conexion.url_cliente + "Inicio/#!/gen_chat";
    }

});

AppAngular.controller('ControlModal', function ($scope, $rootScope, $http) {
});

AppAngular.config(function ($stateProvider, $locationProvider, $urlRouterProvider, $httpProvider) {
    $urlRouterProvider.otherwise("/");
    $locationProvider.hashPrefix('!');
    // You can also load via resolve
    $stateProvider
            .state('/', {
                url: "/",
                views: {
                    "lazyLoadView": {
                        controller: 'ControlHome', // This view will use AppCtrl loaded below in the resolve
                        templateUrl: "../vistas/Home/Index.html?unico=" + Math.round(Math.random() * 10000),
                    }
                },
                resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load files for an existing module
                            return $ocLazyLoad.load('../Controladores/Controller_Home.js?unico=' + Math.round(Math.random() * 10000));
                        }]
                }
            }).state('/log_registro_entrada', {
				url: "/log_registro_entrada",
				views: {
					"lazyLoadView": {
						controller: 'ControlRegistroEntreda', // This view will use AppCtrl loaded below in the resolve
						templateUrl: "../vistas/Registro/RegistroEntrada.html?unico=" + Math.round(Math.random() * 10000)
					}
				},
				resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
							// you can lazy load files for an existing module
							return $ocLazyLoad.load(['../Controladores/Controller_RegistroEntrada.js?unico=' + Math.round(Math.random() * 10000),
													 '../Scripts/webcam.min.js?unico=' + Math.round(Math.random() * 10000)]);
						}]
				}
			}).state('/Concursantes', {
				url: "/Concursantes",
				views: {
					"lazyLoadView": {
						controller: 'ControlConcursantes', // This view will use AppCtrl loaded below in the resolve
						templateUrl: "../vistas/Registro/Concursantes.html?unico=" + Math.round(Math.random() * 10000)
					}
				},
				resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
							// you can lazy load files for an existing module
							return $ocLazyLoad.load(['../Controladores/Controller_Concursantes.js?unico=' + Math.round(Math.random() * 10000),
													 '../Scripts/webcam.min.js?unico=' + Math.round(Math.random() * 10000)]);
						}]
				}
			}).state('/log_ingreso', {
				url: "/log_ingreso",
				views: {
					"lazyLoadView": {
						controller: 'ControlIngresos', // This view will use AppCtrl loaded below in the resolve
						templateUrl: "../vistas/Registro/Ingreso.html?unico=" + Math.round(Math.random() * 10000)
					}
				},
				resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
							// you can lazy load files for an existing module
							return $ocLazyLoad.load(['../Controladores/Controller_Ingresos.js?unico=' + Math.round(Math.random() * 10000),
													 '../Scripts/webcam.min.js?unico=' + Math.round(Math.random() * 10000)]);
						}]
				}
			}).state('/log_foto_ingreso', {
				url: "/log_foto_ingreso",
				views: {
					"lazyLoadView": {
						controller: 'ControlFotoIngresos', // This view will use AppCtrl loaded below in the resolve
						templateUrl: "../vistas/Registro/FotoIngresos.html?unico=" + Math.round(Math.random() * 10000)
					}
				},
				resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
							// you can lazy load files for an existing module
							return $ocLazyLoad.load(['../Controladores/Controller_FotoIngresos.js?unico=' + Math.round(Math.random() * 10000),
													 '../Scripts/webcam.min.js?unico=' + Math.round(Math.random() * 10000)]);
						}]
				}
			}).state('/log_recibir_ingreso', {
				url: "/log_recibir_ingreso",
				views: {
					"lazyLoadView": {
						controller: 'ControlRecibirIngreso', // This view will use AppCtrl loaded below in the resolve
						templateUrl: "../vistas/Registro/RecibirIngreso.html?unico=" + Math.round(Math.random() * 10000)
					}
				},
				resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
							// you can lazy load files for an existing module
							return $ocLazyLoad.load(['../Controladores/Controller_RecibirIngreso.js?unico=' + Math.round(Math.random() * 10000)]);
						}]
				}
			}).state('/log_recibir_ingreso_no', {
				url: "/log_recibir_ingreso_no",
				views: {
					"lazyLoadView": {
						controller: 'ControlRecibirIngNoCali', // This view will use AppCtrl loaded below in the resolve
						templateUrl: "../vistas/Registro/RecibirIngNoCali.html?unico=" + Math.round(Math.random() * 10000)
					}
				},
				resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
							// you can lazy load files for an existing module
							return $ocLazyLoad.load(['../Controladores/Controller_RecibirIngNoCali.js?unico=' + Math.round(Math.random() * 10000)]);
						}]
				}
			}).state('/log_devolucion', {
				url: "/log_devolucion",
				views: {
					"lazyLoadView": {
						controller: 'ControlDevoluciones', // This view will use AppCtrl loaded below in the resolve
						templateUrl: "../vistas/Registro/Devolucion.html?unico=" + Math.round(Math.random() * 10000)
					}
				},
				resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
							// you can lazy load files for an existing module
							return $ocLazyLoad.load(['../Controladores/Controller_Devoluciones.js?unico=' + Math.round(Math.random() * 10000),
												     '../Scripts/webcam.min.js?unico=' + Math.round(Math.random() * 10000)]);
						}]
				}
			}).state('/log_foto_devolucion', {
				url: "/log_foto_devolucion",
				views: {
					"lazyLoadView": {
						controller: 'ControlFotoDevolucion', // This view will use AppCtrl loaded below in the resolve
						templateUrl: "../vistas/Registro/FotoDevolucion.html?unico=" + Math.round(Math.random() * 10000)
					}
				},
				resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
							// you can lazy load files for an existing module
							return $ocLazyLoad.load(['../Controladores/Controller_FotoDevolucion.js?unico=' + Math.round(Math.random() * 10000),
													 '../Scripts/webcam.min.js?unico=' + Math.round(Math.random() * 10000)]);
						}]
				}
			}).state('/log_ubicacion_ingreso', {
				url: "/log_ubicacion_ingreso",
				views: {
					"lazyLoadView": {
						controller: 'ControlUbicacionIngreso', // This view will use AppCtrl loaded below in the resolve
						templateUrl: "../vistas/Registro/UbicacionIngreso.html?unico=" + Math.round(Math.random() * 10000)
					}
				},
				resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
							// you can lazy load files for an existing module
							return $ocLazyLoad.load(['../Controladores/Controller_UbicacionIngreso.js?unico=' + Math.round(Math.random() * 10000)]);
						}]
				}
			}).state('/com_solicitud', {
				url: "/com_solicitud",
				views: {
					"lazyLoadView": {
						controller: 'ControlSolicitudes', // This view will use AppCtrl loaded below in the resolve
						templateUrl: "../vistas/Registro/Solicitud.html?unico=" + Math.round(Math.random() * 10000),
					}
				},
				resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
							// you can lazy load files for an existing module
							return $ocLazyLoad.load('../Controladores/Controller_Solicitudes.js?unico=' + Math.round(Math.random() * 10000));
						}]
				}
			}).state('/com_cotizacion', {
                url: "/com_cotizacion",
                views: {
                    "lazyLoadView": {
                        controller: 'ControlCotizaciones', // This view will use AppCtrl loaded below in the resolve
                        templateUrl: "../vistas/Registro/Cotizacion.html?unico=" + Math.round(Math.random() * 10000)
                    }
                },
                resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load files for an existing module
                            return $ocLazyLoad.load(['../Controladores/Controller_Cotizaciones.js?unico=' + Math.round(Math.random() * 10000)]);
                        }]
                }
            }).state('/com_orden', {
                url: "/com_orden",
                views: {
                    "lazyLoadView": {
                        controller: 'ControlOrden', // This view will use AppCtrl loaded below in the resolve
                        templateUrl: "../vistas/Registro/Orden.html?unico=" + Math.round(Math.random() * 10000)
                    }
                },
                resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load files for an existing module
                            return $ocLazyLoad.load(['../Controladores/Controller_Orden.js?unico=' + Math.round(Math.random() * 10000)]);
                        }]
                }
            }).state('/com_salida', {
                url: "/com_salida",
                views: {
                    "lazyLoadView": {
                        controller: 'ControlSalida', // This view will use AppCtrl loaded below in the resolve
                        templateUrl: "../vistas/Registro/Salida.html?unico=" + Math.round(Math.random() * 10000)
                    }
                },
                resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load files for an existing module
                            return $ocLazyLoad.load(['../Controladores/Controller_Salida.js?unico=' + Math.round(Math.random() * 10000)]);
                        }]
                }
            }).state('/lab_rreportar_ingreso', {
                url: "/lab_rreportar_ingreso",
                views: {
                    "lazyLoadView": {
                        controller: 'ControlLabReportarIngreso', // This view will use AppCtrl loaded below in the resolve
                        templateUrl: "../vistas/Laboratorio/ReportarIngreso.html?unico=" + Math.round(Math.random() * 10000)
                    }
                },
                resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load files for an existing module
                            return $ocLazyLoad.load(['../Controladores/Controller_LabReportarIngreso.js?unico=' + Math.round(Math.random() * 10000)]);
                        }]
                }
            }).state('/lab_ajuste_ingreso', {
                url: "/lab_ajuste_ingreso",
                views: {
                    "lazyLoadView": {
                        controller: 'ControlLabAjusteIngreso', // This view will use AppCtrl loaded below in the resolve
                        templateUrl: "../vistas/Laboratorio/AjusteIngreso.html?unico=" + Math.round(Math.random() * 10000)
                    }
                },
                resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load files for an existing module
                            return $ocLazyLoad.load(['../Controladores/Controller_LabAjusteIngreso.js?unico=' + Math.round(Math.random() * 10000)]);
                        }]
                }
            }).state('/lab_certificado', {
                url: "/lab_certificado",
                views: {
                    "lazyLoadView": {
                        controller: 'ControlLabCertificado', // This view will use AppCtrl loaded below in the resolve
                        templateUrl: "../vistas/Laboratorio/Certificado.html?unico=" + Math.round(Math.random() * 10000)
                    }
                },
                resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load files for an existing module
                            return $ocLazyLoad.load(['../Controladores/Controller_LabCertificado.js?unico=' + Math.round(Math.random() * 10000)]);
                        }]
                }
            }).state('/lab_consultar_reporte', {
                url: "/lab_consultar_reporte",
                views: {
                    "lazyLoadView": {
                        controller: 'ControlLabConsultarReporte', // This view will use AppCtrl loaded below in the resolve
                        templateUrl: "../vistas/Laboratorio/ConsultarReporte.html?unico=" + Math.round(Math.random() * 10000)
                    }
                },
                resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load files for an existing module
                            return $ocLazyLoad.load(['../Controladores/Controller_LabConsultarReporte.js?unico=' + Math.round(Math.random() * 10000)]);
                        }]
                }
            }).state('/lab_par_operacion_previa_07', {
                url: "/lab_par_operacion_previa_07",
                views: {
                    "lazyLoadView": {
                        controller: 'ControlLabOpePreParTor_07', // This view will use AppCtrl loaded below in the resolve
                        templateUrl: "../vistas/Laboratorio/Par_Torsional/07/OpePreParTor_07.html?unico=" + Math.round(Math.random() * 10000)
                    }
                },
                resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load files for an existing module
                            return $ocLazyLoad.load(['../Controladores/Controller_LabOpePreParTor_07.js?unico=' + Math.round(Math.random() * 10000)]);
                        }]
                }
            }).state('/lab_presion_operacion_previa_06', {
                url: "/lab_presion_operacion_previa_06",
                views: {
                    "lazyLoadView": {
                        controller: 'ControlLabOpePrePresion_06', // This view will use AppCtrl loaded below in the resolve
                        templateUrl: "../vistas/Laboratorio/Presion/06/OpePrePresion_06.html?unico=" + Math.round(Math.random() * 10000)
                    }
                },
                resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load files for an existing module
                            return $ocLazyLoad.load(['../Controladores/Controller_LabOpePrePresion_06.js?unico=' + Math.round(Math.random() * 10000)]);
                        }]
                }
            }).state('/lab_par_certificado_did162_07', {
                url: "/lab_par_certificado_did162_07",
                views: {
                    "lazyLoadView": {
                        controller: 'ControlLabPlaCertidid162_07', // This view will use AppCtrl loaded below in the resolve
                        templateUrl: "../vistas/Laboratorio/Par_Torsional/07/PlaCertiParTor_07?unico=" + Math.round(Math.random() * 10000)
                    }
                },
                resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load files for an existing module
                            return $ocLazyLoad.load(['../Controladores/Controller_LabPlaCertidid162_07.js?unico=' + Math.round(Math.random() * 10000)]);
                        }]
                }
            }).state('/nom_registroempleado', {
                url: "/nom_registroempleado",
                views: {
                    "lazyLoadView": {
                        controller: 'ControlRegistroempleado', // This view will use AppCtrl loaded below in the resolve
                        templateUrl: "../vistas/Nomina/Empleados.html?unico=" + Math.round(Math.random() * 10000)
                    }
                },
                resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load files for an existing module
                            return $ocLazyLoad.load(['../Controladores/Controller_Registroempleado.js?unico=' + Math.round(Math.random() * 10000)]);
                        }]
                }
            }).state('/nom_configuracionnomina', {
                url: "/nom_configuracionnomina",
                views: {
                    "lazyLoadView": {
                        controller: 'ControlConfiguracionnomina', // This view will use AppCtrl loaded below in the resolve
                        templateUrl: "../vistas/Nomina/Configuracion.html?unico=" + Math.round(Math.random() * 10000)
                    }
                },
                resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load files for an existing module
                            return $ocLazyLoad.load(['../Controladores/Controller_Configuracionnomina.js?unico=' + Math.round(Math.random() * 10000)]);
                        }]
                }
            }).state('/nom_prestmonomina', {
                url: "/nom_prestamonomina",
                views: {
                    "lazyLoadView": {
                        controller: 'ControlPrestamonomina', // This view will use AppCtrl loaded below in the resolve
                        templateUrl: "../vistas/Nomina/Prestamo.html?unico=" + Math.round(Math.random() * 10000)
                    }
                },
                resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load files for an existing module
                            return $ocLazyLoad.load(['../Controladores/Controller_Prestamonomina.js?unico=' + Math.round(Math.random() * 10000)]);
                        }]
                }
            }).state('/nom_calculonomina', {
				url: "/nom_calculonomina",
				views: {
					"lazyLoadView": {
						controller: 'ControlCalculonomina', // This view will use AppCtrl loaded below in the resolve
						templateUrl: "../vistas/Nomina/CalculoNomina.html?unico=" + Math.round(Math.random() * 10000)
					}
				},
				resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
							// you can lazy load files for an existing module
							return $ocLazyLoad.load(['../Controladores/Controller_Calculonomina.js?unico=' + Math.round(Math.random() * 10000)]);
						}]
				}
			}).state('/gen_pedido', {
				url: "/gen_pedido",
				views: {
					"lazyLoadView": {
						controller: 'ControlPedido', // This view will use AppCtrl loaded below in the resolve
						templateUrl: "../vistas/General/Pedidos.html?unico=" + Math.round(Math.random() * 10000)
					}
				},
				resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
							// you can lazy load files for an existing module
							return $ocLazyLoad.load(['../Controladores/Controller_Pedidos.js?unico=' + Math.round(Math.random() * 10000)]);
						}]
				}
			}).state('/ini_entrar', {
				url: "/ini_entrar",
				views: {
					"lazyLoadView": {
						controller: 'ControlInicioSesion', // This view will use AppCtrl loaded below in the resolve
						templateUrl: "../vistas/InicioSesion/Entrada.html?unico=" + Math.round(Math.random() * 10000)
					}
				},
				resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
					loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
							// you can lazy load files for an existing module
							return $ocLazyLoad.load(['../Controladores/Controller_InicioSesion.js?unico=' + Math.round(Math.random() * 10000)]);
						}]
				}
			});
            

    $locationProvider.html5Mode(false);
    $httpProvider.defaults.withCredentials = true;

});


