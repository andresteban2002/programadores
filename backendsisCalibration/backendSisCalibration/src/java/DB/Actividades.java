/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author leudi
 */
@WebServlet(name = "Actividades", urlPatterns = {"/Actividades"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB 
        maxFileSize = 1024 * 1024 * 50, // 50 MB
        maxRequestSize = 1024 * 1024 * 100)      // 100 MB
public class Actividades extends HttpServlet {

    String sql = "";
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";

    JsonElement elemJSON = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Content-type", "application/json;charset=utf-8");
        System.out.println("-Accediendo a la clase para la Actividades-");

        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String opcion = request.getParameter("opcion");
            if (opcion == null) {
                opcion = getValorJSON(request, "opcion");
            }
            System.out.println("-Opcion a realizar: " + opcion);
            idusuario = request.getSession().getAttribute("idusuario").toString();
            iplocal = request.getSession().getAttribute("IPLocal").toString();
            usuario = request.getSession().getAttribute("codusu").toString();
            switch (opcion) {
                case "ObtenerTiposActividades":
                    out.print(ObtenerTiposActividades(request));
                    break;
                case "GuardarActividad":
                    out.print(GuardarActividad(request));
                    break;
                case "BuscarActividades":
                    out.print(BuscarActividades(request));
                    break;
                case "GuardarActividadEval":
                    out.print(GuardarActividadEval(request));
                    break;
                case "BuscarActCrono":
                    out.print(BuscarActCrono(request));
                    break;
                case "marcarActividad":
                    out.print(marcarActividad(request));
                    break;
                case "CerrarActividad":
                    out.print(CerrarActividad(request));
                    break;
                case "esDirector":
                    out.print(esDirector(request));
                    break;
                case "esCalidad":
                    out.print(esCalidad(request));
                    break;
                case "AgregarCronograma":
                    out.print(AgregarCronograma(request));
                    break;
                case "BuscarCronograma":
                    out.print(BuscarCronograma(request));
                    break;
                case "AgregarTitulo":
                    out.print(AgregarTitulo(request));
                    break;
                case "AdjuntarArchivoB64":
                    out.print(AdjuntarArchivoB64(request));
                    break;
                case "verArchivo":
                    out.print(verArchivo(request));
                    break;
                case "eliminarArchivo":
                    out.print(eliminarArchivo(request));
                    break;
                case "obtenerTitulos":
                    out.print(obtenerTitulos(request));
                    break;
                case "eliminarTitulos":
                    out.print(eliminarTitulos(request));
                    break;
                case "EliminarActividad":
                    out.print(EliminarActividad(request));
                    break;
                case "ConsultarActividades":
                    out.print(ConsultarActividades(request));
                    break;
                default:
                    throw new AssertionError();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public String ObtenerTiposActividades(HttpServletRequest request) {
        try {
            sql = "SELECT id_actividad_tipos AS identificacion, "
                    + "tx_actividad_tipo AS descripcion FROM public.actividad_tipos WHERE ind_area = 1 and (actividad_tipos.id_cargo LIKE '%|' || (SELECT rrhh.empleados.idarea "
                    + "FROM seguridad.rbac_usuario "
                    + "LEFT JOIN rrhh.empleados ON rrhh.empleados.id = seguridad.rbac_usuario.idempleado "
                    + "WHERE rbac_usuario.idusu = " + idusuario + ") || '|%' OR actividad_tipos.id_cargo LIKE '%|0|%') AND id_status = 0 ORDER BY 2";
            return "0|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ObtenerTiposActividades");
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarActividad(HttpServletRequest request) {
        try {
            String idActividad = request.getParameter("idActividad"),
                    motActividad = request.getParameter("motActividad"),
                    fechaIni = request.getParameter("fechaIni"),
                    dias = request.getParameter("dias"),
                    horas = request.getParameter("horas"),
                    actividades = request.getParameter("actividades");

            sql = "INSERT INTO public.actividad_info(consecutivo, id_actividad_tipo, tx_actividad_motivo, fe_inicio, nu_dias, nu_horas, id_usr_registro, id_cargo_registro) VALUES ((SELECT actividades_crono + 1 as consecutivo FROM public.contadores), '" + idActividad + "', '" + motActividad + "', '" + fechaIni + "', " + dias + ", " + horas + ", " + idusuario + ", (SELECT CASE WHEN seguridad.rbac_usuario.idempleado = 0 THEN 0 ELSE idcargo END FROM rrhh.empleados RIGHT JOIN seguridad.rbac_usuario ON rrhh.empleados.id = seguridad.rbac_usuario.idempleado WHERE idusu = " + idusuario + ")) RETURNING id_actividad_info;";
            int idActiv = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (idActiv > 0) {
                if (actividades.contains(")(")) {
                    actividades = actividades.replaceAll("\\)\\(", "), (");
                }
                sql = "UPDATE public.contadores SET actividades_crono= (actividades_crono+1)";
                Globales.DatosAuditoria(sql, "ACTIVIDADES", "ACTUALIZAR SONSECUTIVO", "0", iplocal, this.getClass() + "-->GuardarActividad");
                actividades = actividades.replaceAll("::", idActiv + "");
                sql = "INSERT INTO public.actividad_item(id_actividad, tx_actividad, nu_dias, nu_horas) VALUES " + actividades;
                Globales.DatosAuditoria(sql, "ACTIVIDADES", "GUARDAR EVALUACION", "0", iplocal, this.getClass() + "-->GuardarActividad");
            } else {
                sql = "DELETE * FROM public.actividad_info WHERE id_actividad_info = " + idActiv;
                Globales.DatosAuditoria(sql, "ACTIVIDADES", "GUARDAR EVALUACION", "0", iplocal, this.getClass() + "-->GuardarActividad");
            }
            return "0|La actividad con ID[" + idActiv + "] se ha registrado correctamente|" + Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT idusu FROM seguridad.rbac_usuario WHERE lower(cargo) like lower('%DIRECTOR DE CALIDAD%')"));
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String BuscarActividades(HttpServletRequest request) {
        try {
            sql = "SELECT \n"
                    + "act_info.id_actividad_info as identificacion, \n"
                    + "act_info.fe_inicio AS fecha_inicio,\n"
                    + " nu_estado_actividad AS estatus,\n"
                    + "(SELECT json_agg(X.*) \n"
                    + "FROM (\n"
                    + "SELECT tx_actividad_tipo as tipo \n"
                    + "FROM public.actividad_tipos AS tpo_act \n"
                    + "WHERE '|' || tpo_act.id_actividad_tipos || '|' like '%'||act_info.id_actividad_tipo||'%') \n"
                    + "AS X) as tipos, \n"
                    + "act_info.tx_actividad_motivo as motivo, \n"
                    + "(SELECT json_agg(X.*) FROM \n"
                    + "(SELECT \n"
                    + "id_actividad_item as identif, \n"
                    + "tx_actividad as actividad, \n"
                    + "nu_dias as dias, \n"
                    + "nu_horas as horas,\n"
                    + "nu_dias_eval as dias_eval,\n"
                    + "nu_horas_eval as horas_eval\n"
                    + "FROM \n"
                    + "public.actividad_item AS actividades \n"
                    + "WHERE actividades.id_actividad = act_info.id_actividad_info) AS X) as actividades,\n"
                    + "tx_pas_verificacion as pasos,\n"
                    + "eval.fe_evaluacion as fecha_evaluacion,\n"
                    + "fe_inicio_eval as fe_ini_eval,\n"
                    + "usr.nombrecompleto as nombre_evaluador,\n"
                    + "usr.cargo as cargo_evaluador,\n"
                    + "usr_creo.nombrecompleto as nombre_creo,\n"
                    + "usr_creo.cargo as cargo_creo,\n"
                    + "tx_observacion as observacion_eval,\n"
                    + "id_prioridad as prioridad,\n"
                    + "CASE WHEN tx_doc_rigen isNULL OR tx_doc_rigen = '' THEN 'N/A' ELSE tx_doc_rigen END as doc_rigen,\n"
                    + "CASE WHEN tx_doc_afecta isNULL OR tx_doc_afecta = '' THEN 'N/A' ELSE tx_doc_afecta END as doc_afecta,\n"
                    + "CASE WHEN eval.fe_verificado isNULL THEN fe_verificacion ELSE eval.fe_verificado  END fe_eval\n"
                    + "FROM public.actividad_info AS act_info \n"
                    + "LEFT JOIN public.actividad_eval as eval ON eval.id_actividad = act_info.id_actividad_info\n"
                    + "LEFT JOIN seguridad.rbac_usuario as usr ON usr.idusu = eval.id_usuario_evaluacion \n"
                    + "RIGHT JOIN seguridad.rbac_usuario as usr_creo ON usr_creo.idusu = act_info.id_usr_registro \n"
                    + "WHERE tipo_actividad = 0 " + (!request.getParameter("estado").isEmpty() ? " AND nu_estado_actividad = " + request.getParameter("estado") : " AND nu_estado_actividad IN (0,1,2)") + " ORDER BY 1";
            System.out.println("QUERY: " + sql);
            return "0|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarActividades");
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String GuardarActividadEval(HttpServletRequest request) {
        try {
            String idActividad = request.getParameter("idActividad"),
                    areas = request.getParameter("areas"),
                    prioridad = request.getParameter("prioridad"),
                    sist_gestion = request.getParameter("sist_gestion"),
                    docafecta = request.getParameter("docafecta"),
                    docrigen = request.getParameter("docrigen"),
                    fechaverif = request.getParameter("fechaverif"),
                    pasverifi = request.getParameter("pasverifi"),
                    actividades = request.getParameter("actividades"),
                    mas_menos = request.getParameter("mas_menos"),
                    fecha_ini_eval = request.getParameter("fecha_ini_eval");

            sql = "UPDATE public.actividad_info SET nu_estado_actividad = 1 WHERE id_actividad_info = " + idActividad + ";INSERT INTO public.actividad_eval(id_actividad, id_areas, id_prioridad, sistema_gestion, tx_doc_afecta, tx_doc_rigen, fe_verificacion, tx_pas_verificacion, fe_inicio_eval, id_usuario_evaluacion)"
                    + "VALUES (" + idActividad + ", '|" + (areas == null ? "-" : areas.replaceAll(",", "|")) + "|', " + prioridad + ", " + sist_gestion + ", '" + docafecta + "', '" + docrigen + "', '" + fechaverif + "', '" + pasverifi + "', '" + fecha_ini_eval + "', " + idusuario + ") RETURNING id_actividad_eval;";
            for (int i = 0; i < actividades.split(";").length; i++) {
                sql += actividades.split(";")[i].replaceAll("UP", "UPDATE public.actividad_item SET ").replaceAll(":", "nu_dias_eval").replaceAll("#", "nu_horas_eval").replaceAll("°", "WHERE id_actividad_item") + ";\n";
            }

            Globales.DatosAuditoria(sql, "ACTIVIDADES", "GUARDAR EVALUACION", "0", iplocal, this.getClass() + "-->GuardarActividadEval");
            sql = "SELECT \n"
                    + "usr.idusu AS identificacion,\n"
                    + "usr.nombrecompleto AS nombre\n"
                    + "\n"
                    + "FROM\n"
                    + "seguridad.rbac_usuario AS usr\n"
                    + "INNER JOIN rrhh.empleados AS empl ON empl.id = usr.idempleado\n"
                    + "INNER JOIN rrhh.area AS area ON area.id = empl.idarea\n"
                    + "\n"
                    + "WHERE\n"
                    + "usr.estusu = 'ACTIVO' \n"
                    + (!("|" + areas.replaceAll(",", "|") + "|").contains("|0|") ? " AND '|" + areas.replaceAll(",", "|") + "|' LIKE '%|' || area.id || '|%'" : "");

            return "0|La actividad ha sido registrada completamente|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->GuardarActividadEval");
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String BuscarActCrono(HttpServletRequest request) {
        try {
            sql = "SELECT \n"
                    + "(SELECT \n"
                    + "	COUNT(*)\n"
                    + "FROM\n"
                    + "	public.actividad_info as inf\n"
                    + "WHERE\n"
                    + "	(inf.id_usr_registro = " + idusuario + " AND inf.id_actividad_info = act_info.id_actividad_info) OR ((SELECT lower(cargo) FROM seguridad.rbac_usuario  WHERE idusu = " + idusuario + ") LIKE '%calidad%')) as usuario_root,"
                    + "                            id_actividad_info,\n"
                    + "                            tx_actividad_motivo,\n"
                    + "                            fe_verificacion,\n"
                    + "                            tx_pas_verificacion as pasos,\n"
                    + "                            (	(SELECT COUNT(*) FROM public.actividad_item AS actividades WHERE actividades.id_actividad = act_info.id_actividad_info AND actividades.nu_verificado = 1)::double precision /\n"
                    + "                            (SELECT COUNT(*) FROM public.actividad_item AS actividades WHERE actividades.id_actividad = act_info.id_actividad_info)::double precision *100) as promedio_activ_rea,\n"
                    + "                            (SELECT json_agg(X.*) FROM \n"
                    + "                            (SELECT id_actividad_item as identif, \n"
                    + "                            tx_actividad as actividad, \n"
                    + "                            CASE WHEN nu_verificado = 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE))) < 0 THEN \n"
                    + "                            100\n"
                    + "                            WHEN nu_verificado = 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE))) >= 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, (SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE))) > 0 THEN\n"
                    + "                            100-(\n"
                    + "                            100*(SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE)))/\n"
                    + "                            (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, (SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE))) \n"
                    + "                            )\n"
                    + "                            WHEN nu_verificado = 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE))) >= 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, (SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE))) <= 0 THEN\n"
                    + "                            100-(\n"
                    + "                            100*(SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE)))/\n"
                    + "                            1\n"
                    + "                            )\n"
                    + "                            WHEN nu_verificado = 1 THEN 100 END::text as por_act,\n"
                    + "\n"
                    + "                            CASE WHEN nu_verificado = 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE))) < 0 THEN '100% Tiempo excedido'\n"
                    + "                            WHEN nu_verificado = 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE))) >= 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, (SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE))) > 0 THEN\n"
                    + "                            100-(\n"
                    + "                            100*(SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE)))/\n"
                    + "                            (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, (SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE))) \n"
                    + "                            )|| '%'\n"
                    + "                            WHEN nu_verificado = 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE))) >= 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, (SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE))) <= 0 THEN\n"
                    + "			                100-(\n"
                    + "                            100*(SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE)))/\n"
                    + "                            1\n"
                    + "                            )|| '%'\n"
                    + "                            WHEN nu_verificado = 1 THEN '100% Actividad finalizada' END as por_act_detalle,\n"
                    + "                            nu_dias as dias,\n"
                    + "                            nu_dias as dias, \n"
                    + "                            nu_horas as horas,\n"
                    + "                            nu_dias_eval as dias_eval,\n"
                    + "                            nu_horas_eval as horas_eval,\n"
                    + "                            nu_verificado as chekeado,\n"
                    + "                            fe_verificado as fe_verificacion,\n"
                    + "                            (act_eval.fe_evaluacion::DATE) AS fecha_revision\n"
                    + "                            FROM public.actividad_item AS actividades \n"
                    + "                            WHERE actividades.id_actividad = act_info.id_actividad_info ORDER BY 1 ASC) AS X) as actividades\n"
                    + "\n"
                    + "\n"
                    + "                        FROM\n"
                    + "                            public.actividad_info as act_info\n"
                    + "                            INNER JOIN public.actividad_eval AS act_eval ON act_eval.id_actividad = act_info.id_actividad_info\n" + (Integer.parseInt(request.getParameter("actividad")) != 1 ? ""
                    : "WHERE \n"
                    + "                            act_info.nu_estado_actividad = 1");
            System.out.println(sql);
            return "0|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarActCrono");
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String marcarActividad(HttpServletRequest request) {
        try {
            String idActividad = request.getParameter("idActividad");
            sql = "UPDATE public.actividad_item SET nu_verificado = 1, fe_verificado=NOW() WHERE id_actividad_item = " + idActividad;
            Globales.DatosAuditoria(sql, "ACTIVIDADES", "MARCAR ACTIVIDAD", "0", iplocal, this.getClass() + "-->marcarActividad");
            return "0|La actividad ha sido cerrada, la siguiente quedara disponible para su realización";
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String CerrarActividad(HttpServletRequest request) {
        try {
            String idActividad = request.getParameter("idActividad"), observacion = request.getParameter("observacion");
            sql = "UPDATE public.actividad_eval SET tx_observacion = '" + observacion + "', fe_verificado=now() WHERE id_actividad = " + idActividad + ";";
            sql += "UPDATE public.actividad_info SET nu_estado_actividad = 2, id_usr_cierra=" + idusuario + " WHERE id_actividad_info = " + idActividad;
            Globales.DatosAuditoria(sql, "ACTIVIDADES", "CERRAR ACTIVIDAD", "0", iplocal, this.getClass() + "-->CerrarActividad");
            return "0|La actividad ha sido cerrada completamente";
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String esDirector(HttpServletRequest request) {
        try {
            sql = "SELECT COUNT(*) FROM seguridad.rbac_usuario WHERE lower(cargo) like lower('%DIRECTOR%') AND idusu=" + idusuario;
            int esDirecctor = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            return "0|" + (esDirecctor > 0 ? "true" : "false");
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String esCalidad(HttpServletRequest request) {
        try {
            sql = "SELECT COUNT(*) FROM seguridad.rbac_usuario WHERE lower(cargo) like lower('%DIRECTOR DE CALIDAD%') AND idusu=" + idusuario;
            int esDirecctor = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            return "0|" + (esDirecctor > 0 ? "true" : "false");
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String AgregarCronograma(HttpServletRequest request) {
        try {
            String tipo = request.getParameter("tipo"),
                    estado = request.getParameter("estado"),
                    titulo = request.getParameter("titulo"),
                    descripcion = request.getParameter("descripcion"),
                    fechaIni = request.getParameter("fechaIni"),
                    fechaFin = request.getParameter("fechaFin"),
                    fechaRec = request.getParameter("fechaRec"),
                    actividad = request.getParameter("id");
            System.out.println("------" + estado);
            if (Integer.parseInt(actividad) < 1) {
                sql = "INSERT INTO public.actividad_info (tipo_actividad, nu_estado_actividad, id_actividad_tipo, tx_actividad_motivo, fe_inicio, fe_fin, fe_evaluacion, id_usr_registro, id_cargo_registro)VALUES (" + (tipo.toLowerCase().equals("actividad") ? 1 : tipo.toLowerCase().equals("evento") ? 2 : 0) + ", " + (estado.toLowerCase().equals("activo") ? 1 : 5) + ", '|" + titulo + "|', '" + descripcion + "', '" + fechaIni + "', '" + fechaFin + "', '" + fechaRec + "', " + idusuario + ", (SELECT CASE WHEN seguridad.rbac_usuario.idempleado = 0 THEN 0 ELSE idcargo END FROM rrhh.empleados RIGHT JOIN seguridad.rbac_usuario ON rrhh.empleados.id = seguridad.rbac_usuario.idempleado WHERE idusu = " + idusuario + ")) RETURNING id_actividad_info;";
            } else {
                sql = "UPDATE public.actividad_info SET tipo_actividad=" + (tipo.toLowerCase().equals("actividad") ? 1 : tipo.toLowerCase().equals("evento") ? 2 : 0) + ", nu_estado_actividad=" + (estado.toLowerCase().equals("activo") ? 1 : 5) + ", id_actividad_tipo='|" + titulo + "|', tx_actividad_motivo='" + descripcion + "', fe_inicio='" + fechaIni + "'" + (!fechaFin.equals("") ? ", fe_fin='" + fechaFin + "'" : "") + (!fechaRec.equals("") ? ", fe_evaluacion='" + fechaRec + "'" : "") + ", id_usr_registro=" + idusuario + ", id_cargo_registro=(SELECT CASE WHEN seguridad.rbac_usuario.idempleado = 0 THEN 0 ELSE idcargo END FROM rrhh.empleados RIGHT JOIN seguridad.rbac_usuario ON rrhh.empleados.id = seguridad.rbac_usuario.idempleado WHERE idusu = " + idusuario + ") WHERE id_actividad_info =" + actividad + " RETURNING id_actividad_info;";
            }

            int actCrono = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            return "0|" + actCrono;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String BuscarCronograma(HttpServletRequest request) {
        try {
            String sql2 = "SELECT \n"
                    + "                                id_actividad_info,\n"
                    + "                                tx_actividad_motivo,\n"
                    + "                                fe_verificacion,\n"
                    + "                                act_eval.fe_inicio_eval,\n"
                    + "                                tx_pas_verificacion as pasos,\n"
                    + "                                tpo_crono.tx_actividad_tipo AS titulo,\n"
                    + "                                act_info.tipo_actividad as idtipo_act,\n"
                    + "                                id_prioridad as prioridad,\n"
                    + "                                CASE WHEN tx_doc_rigen isNULL OR tx_doc_rigen = '' THEN 'N/A' ELSE tx_doc_rigen END as doc_rigen,\n"
                    + "                                CASE WHEN tx_doc_afecta isNULL OR tx_doc_afecta = '' THEN 'N/A' ELSE tx_doc_afecta END as doc_afecta,\n"
                    + "                                tx_pas_verificacion as pasos,\n"
                    + "                                CASE WHEN tx_observacion isNULL THEN 'N/A' ELSE tx_observacion END as observacion_eval,\n"
                    + "                                CASE WHEN act_info.tipo_actividad = 0 THEN 'Actividad'\n"
                    + "                                WHEN act_info.tipo_actividad = 1 THEN 'Actividad'\n"
                    + "                                WHEN act_info.tipo_actividad = 2 THEN 'Evento cronograma' END as tipo_act,\n"
                    + "                                CASE WHEN act_info.nu_estado_actividad = 0 THEN 'Actividad Planteada'\n"
                    + "                                WHEN act_info.nu_estado_actividad = 1 THEN 'Actividad Evaluada'\n"
                    + "                                WHEN act_info.nu_estado_actividad = 2 THEN 'Actividad Finalizada' END as estado,\n"
                    + "                                usr.nombrecompleto,\n"
                    + "                                (	(SELECT COUNT(*) FROM public.actividad_item AS actividades WHERE actividades.id_actividad = act_info.id_actividad_info AND actividades.nu_verificado = 1)::double precision /\n"
                    + "                                (SELECT COUNT(*) FROM public.actividad_item AS actividades WHERE actividades.id_actividad = act_info.id_actividad_info)::double precision *100) as promedio_activ_rea,\n"
                    + "                                (SELECT json_agg(X.*) FROM \n"
                    + "                                (SELECT id_actividad_item as identif, \n"
                    + "                                tx_actividad as actividad, \n"
                    + "                                CASE WHEN nu_verificado = 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE))) < 0 THEN \n"
                    + "                                100\n"
                    + "                                WHEN nu_verificado = 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE))) >= 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, (SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE))) > 0 THEN\n"
                    + "                                100-(\n"
                    + "                                100*(SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE)))/\n"
                    + "                                (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, (SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE))) \n"
                    + "                                )\n"
                    + "                                WHEN nu_verificado = 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE))) >= 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, (SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE))) <= 0 THEN\n"
                    + "                                100-(\n"
                    + "                                100*(SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE)))/\n"
                    + "                                1\n"
                    + "                                )\n"
                    + "                                WHEN nu_verificado = 1 THEN 100 END::text as por_act,\n"
                    + "\n"
                    + "                                CASE WHEN nu_verificado = 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE))) < 0 THEN '100% Tiempo excedido'\n"
                    + "                                WHEN nu_verificado = 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE))) >= 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, (SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE))) > 0 THEN\n"
                    + "                                100-(\n"
                    + "                                100*(SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE)))/\n"
                    + "                                (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, (SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE))) \n"
                    + "                                )|| '%'\n"
                    + "                                WHEN nu_verificado = 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE))) >= 0 AND (SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, (SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE))) <= 0 THEN\n"
                    + "                                100-(\n"
                    + "                                100*(SELECT EXTRACT(days FROM age((SELECT CASE WHEN fe_verificado isNULL THEN act_eval.fe_inicio_eval::DATE ELSE fe_verificado::DATE END FROM public.actividad_item AS int_act_item WHERE int_act_item.id_actividad = act_info.id_actividad_info ORDER BY fe_verificado desc LIMIT 1)::DATE + (nu_dias_eval|| ' days ' || nu_horas_eval || ' Hours')::interval, now()::DATE)))/\n"
                    + "                                1\n"
                    + "                                )|| '%'\n"
                    + "                                WHEN nu_verificado = 1 THEN '100% Actividad finalizada' END as por_act_detalle,\n"
                    + "                                nu_dias as dias,\n"
                    + "                                nu_horas as horas,\n"
                    + "                                nu_dias_eval as dias_eval,\n"
                    + "                                nu_horas_eval as horas_eval,\n"
                    + "                                nu_verificado as chekeado,\n"
                    + "                                fe_verificado as fe_verificacion,\n"
                    + "                                (act_eval.fe_evaluacion::DATE) AS fecha_revision\n"
                    + "                                FROM public.actividad_item AS actividades \n"
                    + "                                WHERE actividades.id_actividad = act_info.id_actividad_info ORDER BY 1 ASC) AS X) as actividades\n"
                    + "\n"
                    + "\n"
                    + "                                FROM\n"
                    + "                                public.actividad_info as act_info\n"
                    + "                                INNER JOIN public.actividad_eval AS act_eval ON act_eval.id_actividad = act_info.id_actividad_info\n"
                    + "                                INNER JOIN public.actividad_tipos AS tpo_crono ON '|' || tpo_crono.id_actividad_tipos || '|' LIKE '%' || act_info.id_actividad_tipo || '%'\n"
                    + "                                INNER JOIN seguridad.rbac_usuario AS usr ON usr.idusu = act_info.id_usr_registro\n"
                    + (request.getParameter("titulo") != null && !request.getParameter("titulo").equals("") ? " AND id_actividad_tipo = '|" + request.getParameter("titulo") + "|'" : "")
                    + "WHERE nu_estado_actividad <> 6";

            sql = "SELECT \n"
                    + "                        crono.id_actividad_info as id,\n"
                    + "                        tpo_crono.tx_actividad_tipo AS titulo,\n"
                    + "                        crono.tx_actividad_motivo AS descripcion,\n"
                    + "                        crono.fe_inicio::date AS fe_inicio,\n"
                    + "                        crono.fe_fin::date AS fe_fin,\n"
                    + "                        crono.fe_evaluacion::date AS fe_recordatorio,\n"
                    + "                        tipo_actividad as tipo_act,\n"
                    + "                         rbac_usuario.nombrecompleto,\n"
                    + "                        nu_estado_actividad as idestado,\n"
                    + "                        CASE WHEN crono.tipo_actividad = 1 OR crono.tipo_actividad = 0 THEN 'Actividad' ELSE 'Evento' END AS tipo_crono,\n"
                    + "                        CASE WHEN crono.nu_estado_actividad = 1 AND now()::date < crono.fe_fin::date THEN 'Activo'\n"
                    + "                        WHEN crono.nu_estado_actividad = 1 AND now()::date > crono.fe_fin::date THEN 'Vencido' \n"
                    + "                        WHEN crono.nu_estado_actividad = 5 THEN 'Bloqueado' END AS estado\n"
                    + "\n"
                    + "                        FROM\n"
                    + "                        public.actividad_info AS crono\n"
                    + "                        INNER JOIN public.actividad_tipos AS tpo_crono ON '|' || tpo_crono.id_actividad_tipos || '|' LIKE '%' || crono.id_actividad_tipo || '%'\n"
                    + "                        INNER JOIN seguridad.rbac_usuario ON crono.id_usr_registro = seguridad.rbac_usuario.idusu \n"
                    + "                        WHERE\n"
                    + "                        tipo_actividad=1 AND crono.nu_estado_actividad <> 6"
                    + (request.getParameter("titulo") != null && !request.getParameter("titulo").equals("") ? " AND id_actividad_tipo = '|" + request.getParameter("titulo") + "|'" : "");
            return "0|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarCronograma") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->BuscarCronograma");
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String AgregarTitulo(HttpServletRequest request) {
        try {
            String titulo = request.getParameter("titulo"), areas = request.getParameter("areas");
            sql = "INSERT INTO public.actividad_tipos(tx_actividad_tipo, id_cargo, ind_area) VALUES('" + titulo + "', '|" + areas.replaceAll(",", "|") + "|', 1) returning id_actividad_tipos; ";
            int actCrono = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

            return "0|" + actCrono;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    private String AdjuntarArchivoB64(HttpServletRequest request) {
        try {
            String[] dataB64 = request.getParameterValues("datosB64");
            String[] dataArch = request.getParameterValues("nbArch");
            String id = request.getParameter("id");
            String rutaArch = Globales.ruta_archivo + "Adjunto/Actividades";
            System.out.println("-Comenzando guardado del archivo-");
            for (int i = 0; i < dataB64.length; i++) {
                String nbArchivo = dataArch[i];
                String baseB64 = "", extB64 = "";
                if (dataB64[i].contains(",")) {
                    baseB64 = dataB64[i].split(",")[1].replaceAll(" ", "+");
                    extB64 = extencionesArc(dataB64[i].split(",")[0]);
                } else {
                    baseB64 = dataB64[i];
                    extB64 = extencionesArc(dataArch[i].split("\\.")[1]);
                }
                if (extB64 == null) {
                    return "[{\"error\":\"1\",\"mensaje\":\"Archivo(s) a subir sin extención reconocible\"}]";
                }
                Path destinFile = Paths.get(rutaArch, id);
                if (new File(destinFile.toString()).exists() == false) {
                    new File(destinFile.toString()).mkdirs();
                }
                destinFile = Paths.get(destinFile.toString(), dataArch[i].split("\\.")[0] + "." + extB64);
                byte[] decodedImg = Base64.getMimeDecoder().decode(baseB64.trim().getBytes(StandardCharsets.UTF_8));
                Files.write(destinFile, decodedImg);
            }
            return "[{\"error\":\"0\",\"mensaje\":\"Archivo(s) subidos correctamente\"}]";
        } catch (Exception ex) {
            return "[{\"error\":\"1\",\"mensaje\":\"" + ex + "\"}]";
        }
    }

    private String extencionesArc(String tipo) {
        switch (tipo) {
            case "data:image/jpeg;base64":
                return "jpg";
            case "data:image/png;base64":
                return "png";
            case "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64":
                return "xlsx";
            case "data:application/msword;base64":
                return "doc";
            case "data:application/pdf;base64":
                return "pdf";
            case "data:application/wps-office.pdf;base64":
                return "pdf";
            default:
                if (tipo.equals("pdf")) {
                    return tipo;
                } else {
                    return null;
                }
        }
    }

    private String verArchivo(HttpServletRequest request) {
        String id = request.getParameter("id");
        String rutaArch = Globales.ruta_archivo + "Adjunto/Actividades";
        Path destinFile = Paths.get(rutaArch, id);
        String autorizacion = Globales.ObtenerUnValor("SELECT \n"
                + "	COUNT(*)\n"
                + "FROM\n"
                + "	public.actividad_info\n"
                + "WHERE\n"
                + "	(id_usr_registro = " + idusuario + " AND id_actividad_info = " + id + ") OR ((SELECT lower(cargo) FROM seguridad.rbac_usuario  WHERE idusu = " + idusuario + ") LIKE '%calidad%')");
        if (new File(destinFile.toString()).exists() == false) {
            return "[{\"error\":\"0\",\"mensaje\":\"Esta actividad no posee archivos\", \"autorizacion\": \"" + autorizacion + "\"}]";
        } else {
            String archivos = "";
            if (new File(destinFile.toString()).list().length > 0) {
                for (int i = 0; i < new File(destinFile.toString()).list().length; i++) {
                    archivos += new File(destinFile.toString()).list()[i] + "&nbsp;<a href='javascript:verArchivo(" + id + ", |" + new File(destinFile.toString()).list()[i] + "|)'><i class='icon-folder3'></i></a>&nbsp;" + (Integer.parseInt(autorizacion) > 0 ? "<a href='javascript:eliminarArchivo(" + id + ", |" + new File(destinFile.toString()).list()[i] + "|)'><i class='icon-remove3'></i></a><br>" : "<br>");
                }
                return "[{\"error\":\"0\",\"mensaje\":\"" + archivos + "\", \"autorizacion\": \"" + autorizacion + "\"}]";
            } else {
                return "[{\"error\":\"0\",\"mensaje\":\"Sin archivos\", \"autorizacion\": \"" + autorizacion + "\"}]";
            }
        }
    }

    private String eliminarArchivo(HttpServletRequest request) {
        String id = request.getParameter("id"), archivo = request.getParameter("file");
        String rutaArch = Globales.ruta_archivo + "Adjunto/Actividades";
        Path destinFile = Paths.get(rutaArch, id + File.separator + archivo);
        if (new File(destinFile.toString()).exists() == false) {
            return "[{\"error\":\"1\",\"mensaje\":\"El archivo que desea eliminart no se encuentra en una ruta accesible\"}]";
        } else {
            boolean elimArc = new File(destinFile.toString()).delete();
            if (elimArc == true) {
                return "[{\"error\":\"0\",\"mensaje\":\"Archivo eliminado\"}]";
            } else {
                return "[{\"error\":\"1\",\"mensaje\":\"Ha ocurrido un error al intentar eliminar el archivo\"}]";
            }
        }
    }

    private String obtenerTitulos(HttpServletRequest request) {
        sql = "SELECT id_actividad_tipos AS identificacion, \n"
                + "tx_actividad_tipo AS actividad,\n"
                + "(SELECT json_agg(t.*) as json from\n"
                + "(SELECT id, descripcion \n"
                + "FROM rrhh.area WHERE estado = 1 AND  actividad_tipos.id_cargo LIKE '%|' || id || '|%'\n"
                + "ORDER BY 2) as t) AS areas\n"
                + "FROM \n"
                + "public.actividad_tipos \n"
                + "WHERE \n"
                + "ind_area = 1 AND id_status = 0 order by 2\n"
                + "\n"
                + "";
        return "0|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->obtenerTitulos");
    }

    private String eliminarTitulos(HttpServletRequest request) {
        sql = "UPDATE public.actividad_tipos SET id_status = 1 WHERE id_actividad_tipos = " + request.getParameter("id");
        Globales.DatosAuditoria(sql, "ACTIVIDADES", "GUARDAR ELIMINAR TITULOS", "0", iplocal, this.getClass() + "-->eliminarTitulos");
        return "0|Eliminado correctamente";
    }

    private String EliminarActividad(HttpServletRequest request) {
        sql = "UPDATE public.actividad_info SET nu_estado_actividad = 6 WHERE id_actividad_info = " + request.getParameter("id");
        Globales.DatosAuditoria(sql, "ACTIVIDADES", "GUARDAR ELIMINAR ACTIVIDAD", "0", iplocal, this.getClass() + "-->EliminarActividad");
        return "0|Eliminado correctamente";
    }

    private String getValorJSON(HttpServletRequest request, String campo) {
        String JSON;
        String valueJSON = "-";
        try {
            if (elemJSON == null) {
                JSON = new BufferedReader(new InputStreamReader(request.getInputStream())).lines().collect(Collectors.joining("\n"));
                JsonParser parseJSON = new JsonParser();
                elemJSON = parseJSON.parse(JSON);
                System.out.println("dataJSON: " + elemJSON);
            }
            JsonArray arrJSON = new JsonArray();
            JsonObject objJSON = new JsonObject();

            if (elemJSON instanceof JsonObject) {
                objJSON = elemJSON.getAsJsonObject();
            } else if (elemJSON instanceof JsonArray) {
                arrJSON = elemJSON.getAsJsonArray();
            }

            if (arrJSON.size() > 0) {
                for (JsonElement obj : arrJSON) {
                    JsonObject gsonObj = obj.getAsJsonObject();
                    valueJSON = gsonObj.get(campo).getAsString();
                    return valueJSON;
                }
            }

            if (objJSON.size() > 0) {
                valueJSON = objJSON.get(campo).getAsString();
                return valueJSON;

            }
        } catch (IOException ex) {
            Logger.getLogger(OpcioneAPP.class
                    .getName()).log(Level.SEVERE, null, ex);
            return valueJSON;
        }
        return valueJSON;
    }

    public String ConsultarActividades(HttpServletRequest request) {
        String fechaini = request.getParameter("fechaini");
        String fechafin = request.getParameter("fechafin");
        int control = Globales.Validarintnonull(request.getParameter("control"));
        String evaluada = request.getParameter("evaluada");
        String descripcion = request.getParameter("descripcion");
        
        String Busqueda = "  WHERE tx_actividad_motivo ilike '%" + descripcion + "%' and to_char(case when e.fe_inicio_eval is null then fe_inicio else e.fe_inicio_eval end,'yyyy-MM-dd') >= '" + fechaini + "' AND to_char(case when e.fe_inicio_eval is null then fe_inicio else e.fe_inicio_eval end,'yyyy-MM-dd') <= '" + fechafin + "' ";
        if (!evaluada.equals("")) {
            if (evaluada.equals("SI")) {
                Busqueda += " and e.fe_inicio_eval is not null";
            }else{
                Busqueda += " and e.fe_inicio_eval is null";
            }
        }
        if (control > 0) {
            Busqueda += " and a.consecutivo = " + control;
        }
        sql = "SELECT row_number() OVER(order by id_actividad_info) as fila,  consecutivo, to_char(case when e.fe_inicio_eval is null then fe_inicio else e.fe_inicio_eval end,'yyyy-MM-dd') as fecha_inicio, \n"
                + "case when e.fe_evaluacion is null then 'NO' else 'SI' END AS evaluado, tx_actividad_motivo,\n"
                + "'<b>' || to_char(fecha_registo,'yyyy/MM/dd HH24:MI') || '</b><br>' || u.nombrecompleto as registro,\n"
                + "'<b>' || to_char(e.fe_evaluacion,'yyyy/MM/dd HH24:MI') || '</b><br>' || ue.nombrecompleto as evaluado,\n"
                + "to_char(case when e.fe_verificacion is null then fe_fin else e.fe_verificacion end,'yyyy-MM-dd') as fecha_final\n"
                + "  FROM public.actividad_info a left join actividad_eval e on id_actividad_eval = id_actividad_info\n"
                + "			       left join seguridad.rbac_usuario u on u.idusu = a.id_usr_registro\n"
                + "			       left join seguridad.rbac_usuario ue on ue.idusu = e.id_usuario_evaluacion " + Busqueda + " order by 1";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaCondiciones");
    }

}
