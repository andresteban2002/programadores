﻿var CodCliente = 0;
var Almacen = localStorage.getItem("almacen");
var Cli_Correo = "";

$('body').removeClass('nav-md');
$('body').addClass('nav-sm');

TraducirIdioma($("#menu_idioma").val() * 1);

function LimpiarTodo() {
    Limpiar();
    $("#Cliente").val("");
    $("#Cliente").focus();

}

function Limpiar() {
    $("#NombreCliente").html("");
    CodCliente = 0;
    
    $("#TToma").val("0");
    $("#TConsig").val("0");
    $("#TFactura").val("0");
    $("#TSaldo").val("0");
    $("#TValor").val("0");

    $("#bodyconsignacion").html("");

}

$("#Cliente").bind('keypress', function (e) {
    if (e.keyCode == 13) {
        $("#Cliente").blur();
    }
});



function BuscarCliente(codigo) {
    if (codigo * 1 == CodCliente) {
        return false;
    }
    Limpiar();
    CodCliente = codigo;
    if (codigo == 0)
        return false;
    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var datos = LlamarAjax(url + "Consignacion/FacturasPendientes", "cliente=" + codigo);
        document.getElementById('preloader').style.display = 'none';
        datos = datos.split("|");
        if (datos[0] == "[]") {
            $("#Cliente").select();
            $("#Cliente").focus();
            swal("Acción Cancelado", "Código " + codigo + " no registrado", "warning");
        } else {
            eval("data=" + datos[0]);
            if (data[0].CliAlmacen != Almacen) {
                document.getElementById('preloader').style.display = 'none';
                $.jGrowl(ValidarTraduccion("Cliente no pertenece a este Almacen " + Almacen), { life: 4000, theme: 'growl-warning', header: '' });
                return false;
            }
            
            if (datos[1] == "[]") {
                $("#Cliente").select();
                $("#Cliente").focus();
                swal("Acción Cancelado", "Código " + codigo + " no posee consignaciones pendientes", "warning");
            }

            Cli_Correo = (data[0].CliMail ? data[0].CliMail : "");
            $("#NombreCliente").html(data[0].NomRazSoc + "<br>" + data[0].CliDesDir + " (" + data[0].NomCiu + ") Tel: " + (data[0].CliDesTel ? data[0].CliDesTel : "") + ", " + (data[0].CliDesFax ? data[0].CliDesFax : "") + ", " + Cli_Correo);

            eval("data=" + datos[1]);
            var resultado = "";
            var Saldo = 0;
            var Valor = 0;
            for (var x = 0; x < data.length; x++) {
                Saldo = (data[x].Cantidad * 1) - (data[x].CantFacturada * 1) - (data[x].TomaFisica * 1);
                Valor = data[x].CantFacturada * data[x].SubTotal;
                resultado += "<tr onclick='SeleccionarFila(" + x + "," + data[x].Cantidad + "," + data[x].SubTotal + ")' id='tr-" + x + "'>" +
                    "<td>" + (x + 1) + "</td>" +
                    "<td>" + data[x].Consignacion + "</td>" +
                    "<td><div id='Cod" + x + "' name='Cod[]'>" + data[x].Codigo + "</div></td>" +
                    "<td><div id='Des" + x + "' name ='Des[]'>" +  data[x].Montura + "</div></td>" +
                    "<td>" + data[x].Fecha + "</td>" +
                    "<td align='center'>" + data[x].DiasDespacho + "</td>" +
                    "<td align='right'>" + formato_numero(data[x].ValorUnitario,"0",".",",","") + "</td>" +
                    "<td align='center'><div style='font-size:18px; font-weight:bold;' id='Exi" + x + "' name='Exi[]'>" + formato_numero(data[x].Cantidad, "0", ".", ",", "") + "</div></td>" +
                    "<td align='center'>" + formato_numero(data[x].IVA, "0", ".", ",", "") + "</td>" +
                    "<td align='right'><div id='Sub" + x + "' name='Sub[]'>" + formato_numero(data[x].SubTotal, "0", ".", ",", "") + "</div></td>" +
                    "<td><input type='text' class='no-borderama' onfocus='focusCaja(this.id)' name='Toma[]' disabled onkeyup='CalcularSaldo(" + x + ",1,this)' id='Toma" + x + "' value='" + formato_numero(data[x].TomaFisica, "0", ".", ",", "") + "'></td>" +
                    "<td><input type='text' class='no-border' onfocus='focusCaja(this.id)' name='Cant[]' id='Cant" + x + "' onkeyup='CalcularSaldo(" + x + ",2,this)' value='" + formato_numero(data[x].CantFacturada, "0", ".", ",", "") + "'></td>" +
                    "<td align='center'><div style='font-size:18px; font-weight:bold;' id='Sal" + x + "' name='Sal[]'>" + formato_numero(Saldo, "0", ".", ",", "") + "</div></td>" +
                    "<td align='right'><div id='Val" + x + "' name='Val[]'>" + formato_numero(Valor, "0", ".", ",", "") + "</div><input type='hidden' name='sel[]' id ='sel" + x + "' value='0'></td></tr>";
                
            }
            $("#bodyconsignacion").html(resultado);
            CalcularTotal();
            if (CodCliente >= 9000)
                $("#btnfacturar").addClass("hidden");
            else
                $("#btnfacturar").removeClass("hidden");
            
        }
    }, 15);
}

function CalcularTotal() {
    var toma = 0;
    var fac = 0;
    var valor = 0;
    var cant = 0;
    var sal = 0;

    var tabla = document.getElementsByName("Cant[]");
    for (x = 0; x < tabla.length; x++) {
        toma += $("#Toma" + x).val() * 1; 
        fac += $("#Cant" + x).val() * 1; 
        cant += $("#Exi" + x).html() * 1; 
        valor += NumeroDecimal($("#Val" + x).html()) * 1; 
        sal += $("#Sal" + x).html() * 1; 
    }

    $("#TToma").val(formato_numero(toma, 0, ".", ",", ""));
    $("#TConsig").val(formato_numero(cant, 0, ".", ",", ""));
    $("#TFactura").val(formato_numero(fac, 0, ".", ",", ""));
    $("#TSaldo").val(formato_numero(sal, 0, ".", ",", ""));
    $("#TValor").val(formato_numero(valor, 0, ".", ",", ""));

}

function CalcularSaldo(x, tipo, caja) {
    ValidarTexto(caja, 1);
    var saldo = 0;
    var subtotal = 0;
    var facturar = 0;
    if (caja.value * 1 > 0) {
        if ((caja.value * 1) > ($("#Exi" + x).html() * 1)) {
            if (tipo == 1)
                $.jGrowl(ValidarTraduccion("La Toma Física no puede ser mayor a la existencia actual"), { life: 4000, theme: 'growl-warning', header: '' });
            else
                $.jGrowl(ValidarTraduccion("La Cantidad no puede ser mayor a la existencia actual"), { life: 4000, theme: 'growl-warning', header: '' });
            caja.value = "";
        }
    }
    
    if (tipo == 2) {
        $("#Toma" + x).val("");
        saldo = ($("#Exi" + x).html() * 1) - ($("#Cant" + x).val() * 1) - ($("#Toma" + x).val() * 1);
        subtotal = NumeroDecimal($("#Sub" + x).html()) / $("#Exi" + x).html() * caja.value;
        $("#Val" + x).html(formato_numero(subtotal, 0, ".", ",", ""));    
    } else {
        $("#Cant" + x).val("");
        saldo = ($("#Exi" + x).html() * 1) - ($("#Cant" + x).val() * 1) - ($("#Toma" + x).val() * 1);
        $("#Cant" + x).val(formato_numero(saldo, 0, ".", ",", ""));    
    }
        
    $("#Sal" + x).html(formato_numero(saldo, 0, ".", ",", ""));    

    CalcularTotal();
        
}

function Guardar() {
    var parametros = Parametros();
    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var datos = LlamarAjax(url + "Consignacion/GuardarPedido", parametros);
        document.getElementById('preloader').style.display = 'none';
        swal(datos);
    }, 15);
    
}

function Parametros() {
    var cant = document.getElementsByName("Cant[]");
    var Toma = document.getElementsByName("Toma[]");
    var Codigo = document.getElementsByName("Cod[]");

    var a_codigo = "";

    for (x = 0; x < cant.length; x++) {
        if (a_codigo == "") {
            a_codigo = Codigo[x].innerHTML+"-" + (cant[x].value*1) + "/" + (Toma[x].value*1);
        } else {
            a_codigo += "," + Codigo[x].innerHTML + "-" + (cant[x].value * 1) + "/" + (Toma[x].value * 1);
        }
    }

    return "cliente=" + CodCliente + "&codigo=" + a_codigo;
}

function Facturar() {
    
    if ($("#TFactura").val() * 1 == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo una montura", "warning")
        return false;
    }

    var parametros = Parametros();

    var resultado = "";
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Seguro que desea facturar estas monturas') + '?' + '    Observación de Factura',
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    parametros += "&observacion=" + value;
                    var datos = LlamarAjax(url + "Consignacion/FacturarPedido", parametros)
                    
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resultado = datos[1];
                        ImprimirFactura(datos[2]);
                        resolve()

                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar un observación'));
                }
            })
        }
    }).then(function (result) {
        Limpiar();
        CodCliente = 0;
        $("#Cliente").val("");
        swal({
            type: 'success',
            html: resultado
        })
    })
}



function Devolver() {

    if ($("#TFactura").val() * 1 == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo una montura", "warning")
        return false;
    }

    var parametros = Parametros();

    var resultado = "";
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Seguro que desea devolver estas monturas') + '?' + '    Observación de Devolución',
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    parametros += "&observacion=" + value;
                    var datos = LlamarAjax(url + "Consignacion/DevolverPedido", parametros)
                    
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resultado = datos[1];
                        ImprimirDevolucion(datos[2]);
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar un observación'));
                }
            })
        }
    }).then(function (result) {
        Limpiar();
        CodCliente = 0;
        $("#Cliente").val("");
        swal({
            type: 'success',
            html: resultado
        })
    })
}
function focusCaja(id) {
    $("#" + id).select();
}

function SeleccionarCliente(cliente) {
    $("#modalClientes").modal("hide");
    CodCliente = 0;
    $("#Cliente").val(cliente);
    $("#Cliente").blur();
}

function BuscarFila() {

    var tabla = document.getElementsByName("Cant[]");
    var cod = document.getElementsByName("Cod[]");
    var des = document.getElementsByName("Des[]");
    var toma = document.getElementsByName("Toma[]");

    if (tabla.length > 0) {
        resultado = "";
        swal({
            title: 'Buscardor',
            text: '¡¡¡' + ValidarTraduccion('Ingrese el Código o Descripción del Artículo') + "!!!",
            input: 'text',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Si'),
            cancelButtonText: ValidarTraduccion('No'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',

            inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                    if (value) {
                        for (var x = 0; x < tabla.length; x++) {

                            if (cod[x].innerHTML == $.trim(value) || des[x].innerHTML.indexOf($.trim(value)) != -1) {
                                resultado = x;
                                resolve();
                            }
                        }

                        resolve();

                    } else {
                        reject(ValidarTraduccion('Debe de ingresar un código o descripción'));
                    }
                })
            }
        }).then(function (result) {
            $("#Toma" + resultado).focus();
            $("#Cant" + resultado).focus();
            if (resultado == "")
                swal({
                    type: 'warning',
                    html: "No se encuentro la siguiente descripción: " + result
                })

        })
    }

}

function SeleccionarFila(x, can, sub) {

}

function ImprimirChequeo(factura) {
    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var parametros = Parametros();
        parametros += "&cliente=" + CodCliente;
        var datos = LlamarAjax(url + "Consignacion/ImprimirCheqeo", parametros);
        document.getElementById('preloader').style.display = 'none';
        datos = datos.split("|");
        if (datos[0] == "0")
            window.open(url + "DocumPDF/" + datos[1]);
        else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
}

function ImprimirFactura(factura) {
    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var datos = LlamarAjax(url + "Consignacion/ImprimirFactura", "factura=" + factura);
        document.getElementById('preloader').style.display = 'none';
        datos = datos.split("|");
        if (datos[0] == "0")
            window.open(url + "DocumPDF/" + datos[1]);
        else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
}

function CargarModalClientes() {

    var Razon = $.trim($("#CRazon").val());
    var Iniciales = $.trim($("#CIniciales").val());
    var Codigo = $("#CCodigo").val() * 1;
    var Cedula = $.trim($("#CCedula").val());

    var todos = 0;

    if ($("#CTodos").prop("checked"))
        todos = 1;

    var resultado = '<table class="table table-bordered" id="tablamodalcliente"><thead><tr><th width="8%"><span idioma="Código">' + ValidarTraduccion('Código') + '</span></th><th width= "20%"><span idioma="Nombre">' + ValidarTraduccion('Nombre') + '</span></th><th><span idioma="Dirección">' + ValidarTraduccion('Dirección') + '</span></th><th><span idioma="Ciudad">' + ValidarTraduccion('Ciudad') + '</span></th><th><span idioma="Tipo Cliente">' + ValidarTraduccion('Tipo Cliente') + '</span></th><th><span idioma="Almacén">' + ValidarTraduccion('Almacén') + '</span></th><th><span idioma="Característica">' + ValidarTraduccion('Característica') + '</span></th><th><span idioma="Cédula/Nit">' + ValidarTraduccion('Cédula/NIT') + '</span></th></tr></thead><tbody>';

    var parametros = "Razon=" + Razon + "&Iniciales=" + Iniciales + "&Codigo=" + Codigo + "&Cedula=" + Cedula + "&Todos=" + todos;
    var datos = LlamarAjax(url + 'Facturacion/ModalClientes', parametros);
    if (datos != "[]") {
        eval("data=" + datos);
        for (var x = 0; x < data.length; x++) {

            resultado += "<tr ondblclick=\"SeleccionarCliente(" + data[x].CodCli + ")\">" +
                "<td>" + data[x].CodCli + "</td>" +
                "<td><a class='btn-default' href='Javascript:SeleccionarCliente(" + data[x].CodCli + ")'>" + data[x].NomRazSoc + "</a></td>" +
                "<td>" + data[x].CliDesDir + "</td>" +
                "<td>" + data[x].NomCiu + "</td>" +
                "<td>" + data[x].Tipos + "</td>" +
                "<td>" + data[x].Almacen + "</td>" +
                "<td>" + data[x].NomCar + "</td>" +
                "<td>" + data[x].CliNit + "</td></tr>";
        }
    }

    resultado += "</tbody></table>"
    $("#divmodalcliente").html(resultado);
    ActivarDataTable("tablamodalcliente", 5);

}

function ImprimirDevolucion(factura) {
    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var datos = LlamarAjax(url + "Consignacion/ImprimirDevolucion", "devolucion=" + factura);
        document.getElementById('preloader').style.display = 'none';
        datos = datos.split("|");
        if (datos[0] == "0")
            window.open(url + "DocumPDF/" + datos[1]);
        else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
}

function ActivarToma() {
    var tabla = document.getElementsByName("Cant[]");
    for (x = 0; x < tabla.length; x++) {

        $("#Cant" + x).val("");

        $("#Cant" + x).prop("disabled", true);
        $("#Toma" + x).prop("disabled", false);

        Saldo = ($("#Exi" + x).html() * 1) - ($("#Toma" + x).val() * 1);
        $("#Cant" + x).val(Saldo);

        subtotal = NumeroDecimal($("#Sub" + x).html()) / $("#Exi" + x).html() * $("#Toma" + x).val();
        $("#Val" + x).val(formato_numero(subtotal, 0, ",", ".", ""));

    }

    CalcularTotal();
}

function Inicializar() {
    var tabla = document.getElementsByName("Cant[]");
    for (x = 0; x < tabla.length; x++) {
        $("#Cant" + x).prop("disabled", false);
        $("#Cant" + x).val("");
        $("#Toma" + x).prop("disabled", true);
        $("#Toma" + x).val("");
    }
}

function ModalClientes() {
    $("#modalClientes").modal("show");
    $("#CValor").focus();
}


function ModalDesbloquearClientes() {
    $("#modalDesbClientes").modal("show");
    $("#DCliente").val("")
}

function DesbloquearClientes(codigo, tipo) {
    if (tipo == 0) {
        codigo = $("#DCliente").val() * 1;
        if (codigo == CodCliente) {
            swal("", ValidarTraduccion("No se puede desbloquear este codigo porque está en uso"), "warning");
        }
    }

    if (codigo * 1 > 0) {
        LlamarAjax(url + "Facturacion/DesbloquearClientes", "codigo=" + codigo + "&tipo=" + tipo)
        $("#modalDesbClientes").modal("hide");
    }
}