angular.module('myApp', ['ui', 'ngSanitize', 'ngTable']).controller('ControlRegistroempleado', function ($scope, $ocLazyLoad, $rootScope, General, NgTableParams) {

    function initTabs() {
        tabClasses = ["", "","",""];
    }

    $scope.getTabClass = function (tabNum) {
        return tabClasses[tabNum];
    };

    $scope.getTabPaneClass = function (tabNum) {
        return "tab-pane " + tabClasses[tabNum];
    }

    $scope.setActiveTab = function (tabNum) {
        initTabs();
        tabClasses[tabNum] = "active";
    };



$scope.IdEmpleado = 0;
$scope.DocumentoEmp = "";




 


$scope.Combos = {
        Pais: "",
        Departamento: "",
        Ciudad: "",
        Cargo:"",
        EPS:"",
        AFP:"",
        Banco:"",
        CESANTIA:"",
        Horario:"",
        Area:"",
        ClaseRiesgo:"",
        CentroCosto:"",
        TipoDocumento:""
        

    }



   
    $scope.CargarCombos = function () {
        $rootScope.global.CargandoPrinicipal = true;
        General.CargarCombo(43, null).then(function (response) {$scope.Combos.EPS = response.data;});   
        General.CargarCombo(44, null).then(function (response) {$scope.Combos.AFP = response.data;});   
        General.CargarCombo(69, null).then(function (response) {$scope.Combos.CESANTIA = response.data;});   
        General.CargarCombo(26, null).then(function (response) {$scope.Combos.Cargo = response.data;}); 
        General.CargarCombo(24, null).then(function (response) {$scope.Combos.Banco = response.data;}); 
        General.CargarCombo(30, null).then(function (response) {$scope.Combos.Horario = response.data;}); 
        General.CargarCombo(18, null).then(function (response) {$scope.Combos.Pais = response.data;});
        General.CargarCombo(54, null).then(function (response) {$scope.Combos.Area = response.data;}); 
        General.CargarCombo(80, null).then(function (response) {$scope.Combos.ClaseRiesgo = response.data;}); 
        General.CargarCombo(61, null).then(function (response) {$scope.Combos.CentroCosto = response.data;}); 
        General.CargarCombo(73, null).then(function (response) {$scope.Combos.TipoDocumento = response.data;}); 
        $rootScope.global.CargandoPrinicipal = false;
        
        
    }
    
    $scope.CargarCombos();



    
    
     $scope.InicializarRegistro = function () {
        $scope.Registro = {
         TipoDocumento:"CC",
         Documento:"",
         Empleado:0,
         Nombres:"",
         Pais:"",
         Departamento:"",
         Ciudad:"",
         EstadoCivil:"",
         Area:"",
         Genero:"",
         Cargo:"",
         Sueldo:"",
         Horario:"",
         Huella:"",
         EPS:"",
         AFP:"",
         CESANTIA:"",
         ClaseRiesgo:"",
         CentroCosto:"",
         PeriodoPago:"QUINCENAL",
         Banco:"",
         TipoCuenta:"",
         Cuenta:"",
         Telefonos:"",
         Celular:"",
         Email:"",
         Direccion:"",
         Barrio:"",
         Pantalon:"",
         Camisa:"",
         TipoSangre:"",
         tabla: {},
         Estado:"ACTIVO",
        FechaNacimiento: {
            value: new Date(output + " 00:00"),
            id: output
        },
        FechaInicio: {
            value: new Date(output2 + " 00:00"),
            id: output2
        },
        FechaFinal: {
            value: new Date(output2 + " 00:00"),
            id: output2
        }
         
        }
   }
    
     $scope.InicializarEmpleado = function () {
        $scope.Empleado = {
         Nombres:"",
         Banco:"",
         TipoCuenta:"",
         Cuenta:"",
         archivo:"",
         tipo:"",
         documento:""
      
        }

    }
    
    
   
    $scope.Consulta = {
        documento: "",
        nombre: "",
        tipodocumento: "T",
        cargo: "",
        estado: "TODOS",
        area: "",
        afp: "",
        eps: "",
        cesantia: "",
    }
   
   
   
   
   
   
      $scope.ConsultarEmpleados = function () {
        
        var parametros = "opcion=ConsultarEmpleados&" + General.ConvertirParametros($scope.Consulta);
        General.LlamarAjax("Recursohumano", parametros, true, "", 1).then(function (response) {
            $rootScope.TablaEmpleados = new NgTableParams({count: 10}, {dataset: response});

        });
    }
  

$scope.AgregarCargo=function() {
    swal({
        title: 'Agregar Opci�n',
        text: 'CARGO',
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Agregar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if ($.trim(value)) {
                    var parametros = "opcion=GuardarOpcion&tipo=9&magnitud=0&marca=0&descripcion=" + value + "&tipoconcepto=0";
                    General.LlamarAjax("Cotizacion",parametros,true,"").then(function (response) {
                    var datos = $.trim(response).split("|");
                
                    if (datos[0] != "XX") {
                        $("#Cargo").html(datos[1]);
                        $("#Cargo").val(datos[0]).trigger("change");
                        
                        resolve();
                    } else {
                        reject(datos[1]);
                    }
                 });

                } else {
                    reject(ValidarTraduccion('Debe de ingresar la descripci�n del CARGO'));
                }
            })
        }
    }).then(function (result) {
        $("#Cargo").focus();
    })
}
 $scope.ObtenerConsulta = function (registro) {
        console.log(registro);
        $scope.LimpiarDatos()
        $scope.Registro.Documento = registro.documento;
        $scope.BuscarEmpleado();
        $scope.setActiveTab(1);

    }




$scope.BuscarEmpleado= function() {
    var documento = $.trim($scope.Registro.Documento);
    if ($.trim(documento) == ""||$scope.DocumentoEmp == documento)
        return false;    

   // $scope.LimpiarDatos();
    $scope.DocumentoEmp = documento;
    var parametros = "opcion=BuscarEmpleados&" + "documento=" + documento;
    General.LlamarAjax("Recursohumano", parametros,false,"",1).then(function (data) {
     
    if (data.length > 0) {
        $scope.IdEmpleado = $.trim(data[0].id);
        $scope.Registro.TipoDocumento=data[0].tipodocumento;
        $scope.Registro.EPS=$.trim(data[0].ideps);
        $scope.Registro.AFP=$.trim(data[0].idafp);
        $scope.Registro.CESANTIA=$.trim(data[0].idcesantia);
        $scope.Registro.Area=$.trim(data[0].idarea);
        $scope.Registro.Cargo=$.trim(data[0].idcargo);
        $scope.Registro.Banco=$.trim(data[0].idbanco);
        $scope.Registro.Estado=data[0].estado;
        $scope.Registro.Sueldo=data[0].sueldo;
        $scope.Registro.ClaseRiesgo=$.trim(data[0].idclaseriesgo);
        $scope.Registro.Cuenta=data[0].cuenta;
        $scope.Registro.TipoCuenta=data[0].tipocuenta;
        $scope.Registro.CentroCosto=$.trim(data[0].idcentrocosto);
        $scope.Registro.PeriodoPago=data[0].periodopago;
        $scope.Registro.Nombres=data[0].nombrecompleto;
        $scope.Empleado.Nombres=data[0].nombrecompleto;
        $scope.Registro.FechaInicio.id = General.Obtener_Fecha($scope.Registro.FechaInicio.value);
        $scope.Registro.FechaFinal.id = General.Obtener_Fecha($scope.Registro.FechaFinal.value);
        $scope.Registro.Telefonos=data[0].telefono;
        $scope.Registro.Celular=data[0].celular;
        $scope.Registro.Email=data[0].email;
        $scope.Registro.Huella=data[0].huella;
        $scope.Registro.Horario=$.trim(data[0].idhorario);
        $scope.Registro.Direccion=data[0].direccion;
        $scope.Registro.Pais=$.trim(data[0].idpais);
        $scope.Registro.Departamento=$.trim(data[0].iddepartamento);
        $scope.Registro.Ciudad=$.trim(data[0].idciudad);
        $scope.Registro.EstadoCivil=data[0].estadocivil;
        $scope.Registro.FechaNacimiento.id = General.Obtener_Fecha($scope.Registro.FechaNacimiento.value);
        $scope.Registro.Genero=data[0].genero;
        $scope.Registro.Barrio=data[0].barrio;
        $scope.Registro.TipoSangre=data[0].tiposangre;
        $scope.Registro.Camisa=data[0].camisa;
        $scope.Registro.Pantalon=data[0].pantalon;
    }
    });
}   

/*$scope.BuscarEmpleado= function(
        ) {

    if ($.trim($scope.Documento) == "")
        return false;

    if ($scope.DocumentoEmp == documento)
        return false;

    $scope.LimpiarDatos();
    $scope.DocumentoEmp = documento;
    var parametros = "opcion=BuscarEmpleados&documento=" + documento;
    General.LlamarAjax("Recursohumano",parametros,true,"",0).then(function (response) {
    if (response.length > 0) {
        var data = response[0];
        c

        var Foto = $.trim(data[0].foto);
        if (Foto != "") {
            $("#Foto_Empleado").attr("src",  "Adjunto/imagenes/FotoEmpleados/" + Foto + "?id=" + NumeroAleatorio());
        } else
            $("#Foto_Empleado").attr("src",  "Adjunto/imagenes/img_avatar.png?id=" + NumeroAleatorio());

        $scope.TabDocumentoEmpleados();

    }
    });
}

*/

$scope.GuardarDocumentoArchivo= function() {
    if ($scope.IdEmpleado > 0) {
        var archivo = $.trim($("#ArchivoEmpleado").val());

        var tipo = $("#TipoDocumentoEmpleado").val() * 1;
        var numero = $("#NroDocumentoEmpleado").val() * 1;

        if ($scope.Empleado.tipo == 0) {
            $("#TipoDocumentoEmpleado").focus();
            swal("Acci�n Cancelada", "debe de seleccionar tipo de documento", "warning");
            return false;
        }
        if ($scope.Empleado.archivo == "") {
            $("#ArchivoEmpleado").focus();
            swal("Acci�n Cancelada", "debe de seleccionar el archivo en pdf del documento", "warning");
            return false;
        }

        var parametros = "opcion=GuardarDocumentosEmpleados&"+"Empleado=" + $scope.IdEmpleado + "&Documento=" + tipo + "&Numero=" + numero;
        General.LlamarAjax("Recursohumano", parametros, true,"").then(function (response) {
        var datos = $.trim(response).split("|");
        if (datos[0] == "0") {
            $scope.CambioDocumento(tipo);
        } else
            swal("Acci�n Cancelada", datos[1], "warning");
    });
    }
}




$scope.CambioDocumentoEmpleado= function(documento) {
    $("#ArchivoEmpleado").val("");

    if ($scope.Empleado.Documento * 1 == 0) {
        $("#DocumentoEmpleado").attr("src", url_cliente + "Adjunto/DocumentosEmpleados/0.pdf?id=" + NumeroAleatorio());
        $("#CreacionDocumento").html("");
        return false;
    }

    if ($scope.IdEmpleado > 0) {

        var parametros = "opcion=DocumentosEmpleado&"+"Empleado=" + $scope.IdEmpleado + "&Documento=" + $scope.Empleado.documento + "&Numero=" + $("#NroDocumentoEmpleado").val();
        General.LlamarAjax("Recursohumano",parametros, true,"",1).then(function (response) {
        var datos = $.trim(response).split("|");
        if (datos == "[]") {
            $("#DocumentoEmpleados").attr("src", url_cliente + "Adjunto/DocumentosEmpleados/0.pdf?id=" + NumeroAleatorio());
            $.jGrowl("Este documento no se le ha cargado al Empleado", { life: 3000, theme: 'growl-warning', header: '' });
        } else {
            var data = JSON.parse(datos);
            $("#DocumentoEmpleados").attr("src", url_cliente + "Adjunto/DocumentosEmpleados/" + IdEmpleado + "/" + documento + "-"+ $("#NroDocumentoEmpleado").val() + ".pdf?id=" + NumeroAleatorio());
            $("#CreacionDocumento").html("Creado por " + data[0].usuario + " el d�a " + data[0].fecha);
        }
    });
    } else {
        $("#TipoDocumentoEmpleado").val("").trigger("change");
        swal("Acci�n Cancelada", "Debe de seleccionar un empleado, o guardar los datos", "warning");
    
    }
}



$scope.TabDocumentoEmpleados= function() {
    
    var parametros = "opcion=TablaDocumentosEmpleado&"+"Empleado=" + $scope.IdEmpleado + "&Documento=0";
    General.LlamarAjax("Recursohumano",parametros,true,"",1).then(function (data) {
     $scope.Registro.tabla = data;
 });
}


$scope.DescargarPDF= function(empleado, id) {
    window.open(url_archivo + "Adjunto/DocumentosEmpleados/" + empleado + "/" + id + ".pdf?id=" + NumeroAleatorio());
}




$scope.LimpiarTodo= function() {
    $scope.Registro.TipoDocumento =="CC";
    $("#Documento").val("");
    $scope.DocumentoEmp = "";
    $scope.LimpiarDatos();
}


 $scope.LimpiarDatos = function () {
        $scope.Empleado = {Documento: ""};
        $scope.InicializarRegistro();
       
    }


$scope.CambioDepartamento= function(pais, combo) {
    $("#" + combo).html(CargarCombo(11, null, "", pais));
}

$scope.CambioCiudad= function(departamento, combo) {
    $("#" + combo).html(CargarCombo(12, null, "", departamento));
}

$scope.ValidarEmpleado= function(event) {
     var documento = $.trim($scope.DocumentoEmp);
    if (documento != "") {
        if ($scope.DocumentoEmp != documento) {
            $scope.DocumentoEmp = "";
            $scope.LimpiarDatos();
        }
    }
}


 $scope.CambioFecha = function (registro, tipo) {
        switch (registro) {
            case 1:
                switch (tipo) {
                    case 1:
                        $scope.Registro.FechaNacimiento.id = General.Obtener_Fecha($scope.Registro.FechaNacimiento.value);
                        break;
                    case 2:
                        $scope.Registro.FechaInicio.id = General.Obtener_Fecha($scope.Registro.FechaInicio.value);
                        break;
                    case 3:
                        $scope.Registro.FechaFinal.id = General.Obtener_Fecha($scope.Registro.FechaFinal.value);
                        break;    
                }
        }
   }






 $scope.GuardarEmpleado = function () {
console.log($scope.IdEmpleado);
    
     if ($scope.Registro.Banco > 0) {
        if ($scope.Registro.Cuenta == "") {
            $("#Cuenta").focus();
            swal("Acci�n Cancelada", "Debe de ingresar el n�mero de cuenta", "warning");
            return false;
        }
        if ($scope.Registro.TipoCuenta == "") {
            $("#TipoCuenta").focus();
            swal("Acci�n Cancelada", "Debe de seleccionar el tipo de cuenta", "warning");
            return false;
        }
    }
    if ($scope.Registro.Cuenta != "") {
        if ($scope.Registro.Banco == 0) {
            $("#Banco").focus();
            swal("Acci�n Cancelada", "Debe de seleccionar el banco ", "warning");
            return false;
        }

        if ($scope.Registro.TipoCuenta == "") {
            $("#TipoCuenta").focus();
            swal("Acci�n Cancelada", "Debe de seleccionar el tipo de cuenta", "warning");
            return false;
        }
    }

    if ($scope.Registro.TipoCuenta != "") {
        if ($scope.Registro.Banco == 0) {
            $("#Banco").focus();
            swal("Acci�n Cancelada", "Debe de seleccionar el banco ", "warning");
            return false;
        }

        if ($scope.Registro.Cuenta == "") {
            $("#Cuenta").focus();
            swal("Acci�n Cancelada", "Debe de ingresar el n�mero de cuenta", "warning");
            return false;
        }
    }

    var parametros = "&IdEmpleado="+$scope.IdEmpleado+"&opcion=GuardarEmpleados&" + $("#formEmpleados").serialize().replace(/\%2C/g, "")+""+General.ConvertirParametros($scope.Registro);
    General.LlamarAjax("Recursohumano", parametros, true,"",1).then(function (response) {
    var datos = $.trim(response).split("|");
        if (datos[0] == "0") {
        if ($scope.IdEmpleado == 0) {
            $scope.IdEmpleado = datos[2] * 1;
            swal("", "Empleado guardado con �xito", "success");
            $scope.LimpiarTodo();
        } else
            swal("", "Empleado actualizado con �xito", "success");
           $scope.LimpiarTodo();
    } else {
        swal("Acci�n Cancelada", datos[1], "warning");
    }

});
 }


$scope.CambioDocumento= function() {
    if ($scope.Empleado == 0) {
        swal("Acci�n Cancelada", "Debe seleccionar el empleado", "warning");
        return false;
    }
    $("#SCambioNit").html(DocumentoCli);
    $("#modalCambioNitCli").modal("show");
    }
      $scope.CambioDepartamento = function (pais) {
        General.CargarCombo(11, pais).then(function (response) {
            $scope.Combos.Departamento = response.data;
        });
    }

    $scope.CambioCiudad = function (departamento) {
        General.CargarCombo(12, departamento).then(function (response) {
            $scope.Combos.Ciudad = response.data;
        });

    }
    
    
    
$scope.MaximizarFoto = function(foto) {
    window.open(url_archivo + "Adjunto/imagenes/FotoEmpleados/" + foto + "?id=" + NumeroAleatorio());
} 


$scope.CambiarDocumento= function() {
    
    if ($scope.Empleado.Documento == "") {
        $("#DDocumento").focus();
        swal("Acci�n Cancelada", "Debe de ingresar el nuevo NIT/C�dula", "warning");
        return false;
    }
    var parametros = "opcion=CambioDocumento&documento=" + $scope.Empleado.Documento + "&id=" + $scope.IdEmpleado + "&tipo=" + $scope.Empleado.tipo;
    General.LlamarAjax("Recursohumano",parametros,"",0).then(function (response) {
    var datos = $.trim(response).split("|");
    if (datos[0] == "0") {
        $("#modalCambioDocumento").modal("hide");
        $("#Documento").val(documento);
        $scope.DocumentoEmp = "";
        $scope.BuscarEmpleado($scope.Empleado.Documento);
        swal("", datos[1], "success");
    } else {
        swal("Acci�n Cancelada", datos[1], "warning");
    }
    });
}

$("img").error(function() {
    $(this).attr("src", "Adjunto/imagenes/img_avatar.png?id=" + NumeroAleatorio());
});


  
 $scope.CargarCombos();  
 $scope.InicializarRegistro(); 
 $scope.InicializarEmpleado();
 $rootScope.InicializarFormulario(16);
 $scope.setActiveTab(1);


});
