var AppAngular = angular.module('myApp', ['ui.router', 'oc.lazyLoad', 'ui.bootstrap', 'ngSanitize', 'ngTable', 'ui']);
var shutter = null;

AppAngular.run(function ($rootScope, $http, $uibModal, $timeout, General, NgTableParams) {


    $rootScope.global = {
        Cargando: false,
        BloquearPantalla: false,
        CargandoPrinicipal: false,
        classmenu: [100],
        display: [100],
        classbody: "",
        movil: 0
    };


    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $rootScope.global.movil = 1;
    }

    $rootScope.Chat = {
        tabla: {},
        mensaje: "",
        usuarioschat: "",
        id: 0,
        vermensaje: ""
    }

    $rootScope._DE = 0;
    $rootScope._CM = "";
    $rootScope._CD = "";
    $rootScope._SM = "";
    $rootScope._TIPOCOTIZACION = 0;
    $rootScope._busquedaingreso = "";

    $rootScope.global.InicializarClass = function (posicion) {
        for (var i = 0; i < 100; i++) {
            if (posicion == i) {
                $rootScope.global.classmenu[i] = "active";
                $rootScope.global.display[i] = false;
            } else {
                $rootScope.global.classmenu[i] = "";
                $rootScope.global.display[i] = true;
            }
        }
    }

    $rootScope.global.InicializarClass(0);


    $rootScope.InicioSesion = {
        idioma: 0,
        estado: "",
        usuario: "",
        empleado: 0,
        cargo: "",
        codusu: "",
        idusuario: 0,
        alertas_mensaje: "",
        foto: "",
        barraauxiliar: false
    };


    $rootScope.Idioma = {
        activo: "Español",
        numero: localStorage.getItem('idioma') ? localStorage.getItem('idioma') : 1,
        icon: ["spanish.png", "english.png"],
        titulo: ["Español", "Inglés"],
        clase: ["active", ""],
        TipoLenguajeDataTable: ["json/Spanish.json", ""],
        ArchivoIdioma: "",
        cambiar: 1,
        LenguajeDataTable: ""
    };

    $rootScope.Modal = {
        Combos: {
            grupo: "",
            ubicacion: "",
            moneda: "",
        },
        titulo: "",
        foto: 0,
        aleatorio: 0,
        numero: 0,
        total: 0,
        grupo: "",
        ubicacion: "",
        moneda: "1",
        codigobarras: "",
        precio: "1",
        descripcion: "1",
        tipo: 0,
        cliente: 0,
        tipo_ingreso: "",
        razonsocial: "",
        cedula: "",
        dis_moneda: true,
        dis_precio: true,
        dis_ubicacion: true,
        table: null,
        ingresoFilter: {
            id: {
                id: "number",
                placeholder: "Filter by Remisión"
            }
        }

    };


    $rootScope.range = function (min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step)
            input.push(i);
        return input;
    };

    $rootScope.CambiarFoto = function (foto, ingreso) {
        $rootScope.Modal.aleatorio = General.NumeroAleatorio();
        $rootScope.Modal.foto = foto;
    }

    $rootScope.Modal.Open = function (modal) {
        $rootScope.Modal.objeto = $uibModal.open({
            templateUrl: $rootScope.Conexion.url_cliente + "Modal/" + modal + ".html?unico=" + Math.round(Math.random() * 10000),
            size: 'lg',
            backdrop: 'static',
            keyboard: false
        });
        $rootScope.TraducirIdioma(0);
        switch ($rootScope.Modal.tipo) {
            case 1:
                General.CargarCombo(47, null).then(function (response) {
                    $rootScope.Modal.Combos.grupo = response.data;
                });
                General.CargarCombo(102, null).then(function (response) {
                    $rootScope.Modal.Combos.moneda = response.data;
                });
                break;

        }
        $timeout(function () {
            $('select').select2();
        }, 500);

    }

    $rootScope.Modal.Close = function () {
        $rootScope.Modal.objeto.dismiss();
    }

    $rootScope.Modal.CargarModal = function () {
        switch ($rootScope.Modal.tipo) {
            case 1:
                var parametros = "opcion=ModalArticulos&codigo=" + $.trim($rootScope.Modal.codigobarras) + "&descripcion=" + $.trim($rootScope.Modal.descripcion) + "&grupo=" + $rootScope.Modal.grupo + "&precio=" + $rootScope.Modal.precio;
                General.LlamarAjax("Inventarios", parametros, false, "", 1).then(function (response) {
                    $rootScope.TablaModalInventarios = new NgTableParams({count: 5, counts: []}, {dataset: response});
                });
                break;
            case 2:
                var parametros = "opcion=ModalClientes&Razon=" + $.trim($rootScope.Modal.razonsocial) + "&Cedula=" + $.trim($rootScope.Modal.cedula);
                General.LlamarAjax("Cotizacion", parametros, false, "", 1).then(function (response) {
                    $rootScope.TablaModalClientes = new NgTableParams({count: 5, counts: []}, {dataset: response});
                });
                break;
            case 3:
                var parametros = "opcion=ModalIngreso&idcliente=" + $rootScope.Modal.cliente + ($rootScope.Modal.tipo_ingreso != "" ? "&tipo=" + $rootScope.Modal.tipo_ingreso : "");
                General.LlamarAjax("Cotizacion", parametros, false, "", 1).then(function (response) {
                    $rootScope.TablaModalIngresos = new NgTableParams({count: 5, counts: []}, {dataset: response});
                });
                break;
        }
    }

    $rootScope.ModulosTraducir = function () {
        console.log($rootScope.Idioma.ArchivoIdioma);
        $("label[idioma]").each(function (index) {
            $(this).attr("title", $(this).attr("idioma"));
            $(this).html($rootScope.ValidarTraduccion($(this).attr("idioma")));
        });

        $("span[idioma]").each(function (index) {
            $(this).attr("title", $(this).attr("idioma"));
            $(this).html($rootScope.ValidarTraduccion($(this).attr("idioma")));
        });

        $("a[idioma]").each(function (index) {
            $(this).attr("title", $(this).attr("idioma"));
            $(this).html($rootScope.ValidarTraduccion($(this).attr("idioma")));
        });

        $("title[idioma]").each(function (index) {
            $(this).html($rootScope.ValidarTraduccion($(this).attr("idioma")));
        });

        $("th[idioma]").each(function (index) {
            $(this).attr("title", $(this).attr("idioma"));
            $(this).html($rootScope.ValidarTraduccion($(this).attr("idioma")));
        });
        $("td[idioma]").each(function (index) {
            $(this).attr("title", $(this).attr("idioma"));
            $(this).html($rootScope.ValidarTraduccion($(this).attr("idioma")));
        });

        $("option[idioma]").each(function (index) {
            $(this).html($rootScope.ValidarTraduccion($(this).attr("idioma")));
        });
    }

    $rootScope.ValidarTraduccion = function (valor) {
        var Archivo = $rootScope.Idioma.ArchivoIdioma;
        var resultado = Archivo[$.trim(valor)];
        if (!(resultado))
            resultado = valor;
        return resultado;
    }

    $rootScope.Conexion = {
        url_servidor: "",
        url_cliente: "",
        url_socker: "",
        url_archivo: "",
    }

    $rootScope.Ruta_Artivo = function () {
        $http({
            method: 'POST',
            url: "../rutas.json",
            dataType: "json"
        }).then(function successCallback(data) {
            console.log(data.data);
            $rootScope.Conexion.url_servidor = data.data[0].url_servidor;
            $rootScope.Conexion.url_cliente = data.data[0].url_cliente;
            $rootScope.Conexion.url_socker = data.data[0].socker;
            $rootScope.Conexion.url_archivo = data.data[0].url_archivo;

            shutter = new Audio();
            shutter.autoplay = false;
            shutter.src = $rootScope.Conexion.url_archivo + "Adjunto/Audio/" + (navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3');
            shutter.play();

            General.LlamarAjax("RutaArchivo", "", false, "", 0).then(function (response) {
                var datos = $.trim(response).split("||");
                if (datos[0] == "0") {
                    $rootScope.IPLocal();
                } else {
                    swal("Acción Cancelada", datos[1], "warning");
                }
            });

        }, function errorCallback(response) {
            swal("Error de Peitición", response, "error");
        });
    }

    $rootScope.Ruta_Artivo();

    $rootScope.IPLocal = function () {
        var myIP = "Bloqueado";
        window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;   //compatibility for firefox and chrome
        var pc = new RTCPeerConnection({iceServers: [{urls: 'stun:stun.l.google.com:19302'}]}), noop = function () { };
        pc.createDataChannel("");    //create a bogus data channel
        pc.createOffer(pc.setLocalDescription.bind(pc), noop);    // create offer and set local description
        pc.onicecandidate = function (ice) {  //listen for candidate events
            if (!ice || !ice.candidate || !ice.candidate.candidate) {
                $.jGrowl("Sin conexión a internet", {life: 3500, theme: 'growl-warning', header: ''});
                myIP = "127.0.0.1";
                var parametros = "opcion=GuardarIP&iplocal=" + myIP;
                General.LlamarAjax("InicioSesion", parametros, true, "", 0).then(function (response) {
                    localStorage.setItem("myIP", myIP);
                    return false;
                });
            }
            var arreglo = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(ice.candidate.candidate);
            if (arreglo) {
                if (arreglo.length > 1) {
                    if (myIP == "Bloqueado") {
                        myIP = arreglo[1];
                        if ($.trim(myIP) != "") {
                            var parametros = "opcion=GuardarIP&iplocal=" + myIP;
                            General.LlamarAjax("InicioSesion", parametros, true, "", 0).then(function (response) {
                                localStorage.setItem("myIP", myIP);
                                return false;
                            });
                        }
                    }
                }
            }
            localStorage.setItem("myIP", myIP);
            if (myIP == "Bloqueado") {
                swal("Acción Cancelada", "Active la IP local del navegador, en el navegador", "warning");
                return false;
            }
            pc.onicecandidate = noop;
        };
    }

    $rootScope.myMoveFunction = function () {

    }

    $rootScope.FormatoEntrada = function (texto, tipo) {
        if (tipo == "1") {
            texto.value = $rootScope.CambiarCaracter(texto.value, ",", "");
        }
        //alert(tipo);
        if (tipo != "3") {
            texto.select();
        }
    }

    $rootScope.CambiarCaracter = function (texto, c, r) {
        var numero = '';
        if ($.trim(texto) != "") {
            for (var i = 0; i < texto.length; i++) {
                if (texto.charAt(i) != c)
                    numero = numero + texto.charAt(i);
                else
                    numero = numero + r;
            }
        }
        return numero;
    }

    $rootScope.ValidarTexto = function (texto, tipo) {
        if (tipo == "1" || tipo == "3") {
            var valorCaja = '';
            var caja = '';
            if (texto.value == "-")
                return false;
            for (var i = 0; i < texto.value.length; i++) {
                if (tipo == 1) {
                    if ((texto.value.charAt(i) != ".") && (texto.value.charAt(i) != ",") && (texto.value.charAt(i) != "-"))
                        valorCaja = valorCaja + texto.value.charAt(i);
                } else {
                    if ((texto.value.charAt(i) != ".") && (texto.value.charAt(i) != "-"))
                        valorCaja = valorCaja + texto.value.charAt(i);
                }
            }

            if (isNaN(valorCaja) && valorCaja != "-")
                texto.value = "";
            else {
                if (tipo == 1) {
                    texto.value = valorCaja.replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                }
            }
        } else {
            valorCaja = ""
            for (var i = 0; i < texto.value.length; i++) {
                if (((texto.value.charAt(i) >= "0") && (texto.value.charAt(i) <= "9"))) {
                    texto.value = valorCaja
                    i = texto.length;
                } else
                    valorCaja = valorCaja + texto.value.charAt(i);
            }
        }
    }

    $rootScope.FormatoSalida = function (texto, decimal, cero) {
        if (decimal == undefined)
            decimal = 0;
        if ($.trim(texto.value) != "") {
            texto.value = $rootScope.NumeroDecimal(texto.value);
        }
        if (cero) {
            if (cero == 2) {
                texto.value = Math.abs(texto.value) * -1;
            }
            texto.value = $rootScope.formato_numero(texto.value, decimal, $rootScope._CD, $rootScope._CM);

        } else {
            texto.value = $.trim(texto.value) == "" ? "0" : $rootScope.formato_numero(texto.value, decimal, $rootScope._CD, $rootScope._CM);
        }
    }

    $rootScope.NumeroDecimal = function (texto) {
        texto = $.trim(texto);
        if (texto != "") {
            texto = $rootScope.CambiarCaracter(texto, ",", "");

            var valorCaja = "";

            for (var i = 0; i < texto.length; i++) {

                if ((texto.charAt(i) == "0") || (texto.charAt(i) == "1") || (texto.charAt(i) == "2") || (texto.charAt(i) == "3") || (texto.charAt(i) == "4") || (texto.charAt(i) == "5") || (texto.charAt(i) == "6") || (texto.charAt(i) == "7") || (texto.charAt(i) == "8") || (texto.charAt(i) == "9") || (texto.charAt(i) == ".") || (texto.charAt(i) == "-") || (texto.charAt(i) == "e"))
                    valorCaja = valorCaja + texto.charAt(i);
            }

            return (valorCaja * 1);
        } else {
            return 0;
        }
    }

    $rootScope.formato_numero = function (numero, decimales, separador_decimal, separador_miles, SiglaMoneda, blanco) { // v2007-08-06

        numero = parseFloat(numero);
        if (!(SiglaMoneda))
            SiglaMoneda = "";

        if (isNaN(numero)) {
            return "";
        }

        if (decimales !== undefined) {
            // Redondeamos
            numero = numero.toFixed(decimales);
        }

        // Convertimos el punto en separador_decimal
        numero = numero.toString().replace(".", separador_decimal !== undefined ? separador_decimal : ",");
        var valores = numero.split(separador_decimal);
        var entera = valores[0];

        if (separador_miles) {
            // Añadimos los separadores de miles
            var miles = new RegExp("(-?[0-9]+)([0-9]{3})");
            while (miles.test(entera)) {
                entera = entera.replace(miles, "$1" + separador_miles + "$2");
            }
        }
        if (SiglaMoneda == "%")
            return $.trim(entera + (valores[1] ? separador_decimal + valores[1] : "") + " " + SiglaMoneda);
        else {
            if (blanco) {
                if (blanco == 3) {
                    if (valores[1] * 1 == 0 && entera * 1 == 0) {
                        if ($.trim(numero) != "")
                            return "0.000"
                    }
                } else {
                    if (blanco == 5) {
                        texto = "";
                        for (var i = valores[1].length; i > 1; i--) {

                            if (valores[1].charAt(i) != "0")
                                texto += valores[1].charAt(i);
                        }
                        return $.trim(SiglaMoneda + " " + entera + separador_decimal + texto);
                    } else {
                        if (valores[1] * 1 == 0 && entera * 1 == 0)
                            return "";
                    }

                }
            }
            return $.trim(SiglaMoneda + " " + entera + (valores[1] ? separador_decimal + valores[1] : ""));
        }
    }


}).service('General', function ($rootScope, $http, $q) {
    this.LlamarAjax = function (controlador, parametros, load, objeto, json) {
        $rootScope.global.Cargando = load;


        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: $rootScope.Conexion.url_servidor + controlador,
            data: parametros,
            headers: {'Content-Type': 'application/x-www-form-urlencoded',
                'Accept-Charset': undefined},
            dataType: (json == 0 ? "html" : "json")
        }).then(function successCallback(data) {
            $rootScope.global.Cargando = false;
            deferred.resolve((json == 0 ? $.trim(data.data) : data.data));
        }, function errorCallback(response) {
            $rootScope.global.Cargando = false;
            $.jGrowl("Sesión cerrada", {life: 1500, theme: 'growl-warning', header: ''});
        });
        //console.log(deferred.promise);
        return deferred.promise;
    }

    this.CargarCombo = function (tipo, opcion) {
        if (!opcion) {
            opcion = 0
        }
        var parametros = "opcion=CargarCombos&tipo=" + tipo + "&inicial=0&opciones=" + opcion + "&vjson=1";
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: $rootScope.Conexion.url_servidor + "Configuracion",
            data: parametros,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            dataType: "json"
        }).then(function successCallback(data) {
            $rootScope.global.Cargando = false;
            deferred.resolve(data);
        }, function errorCallback(response) {
            swal("Error de Peitición", response, "error");
        });
        return deferred.promise;
    }


    this.escape = function (texto) {
        texto = $.trim(texto);

        if (texto == "") {
            return "";
        }
        if (!isNaN(texto)) {
            return texto;
        }

        return encodeURIComponent(texto);

    }

    this.EscucharMensaje = function (mensaje) {
        if ('speechSynthesis' in window) {
            var speech = new SpeechSynthesisUtterance(mensaje);
            window.speechSynthesis.speak(speech);
        }
    }

    this.ConvertirParametros = function (json) {
        var nivel = 0;
        var parametro = "";
        for (var clave in json) {
            nivel = 0;
            parametro += "&" + clave + "=";
            if (json.hasOwnProperty(clave)) {
                for (var j in json[clave]) {
                    if (j == "id") {
                        nivel = 1;
                        parametro += $.trim(this.escape(json[clave][j]));
                        break;
                    }
                }
                if (nivel == 0)
                    parametro += $.trim(this.escape(json[clave]));
            }
        }
        return parametro;
    }

    this.Obtener_Fecha = function (fecha) {
        var month = fecha.getMonth() + 1;
        var day = fecha.getDate();
        var resultado = fecha.getFullYear() + '-' +
                (month < 10 ? '0' : '') + month + '-' +
                (day < 10 ? '0' : '') + day;
        return resultado;
    }

    this.DatosEmpresa = function () {
        this.LlamarAjax("Configuracion", "opcion=DatosEmpresa", false, "", 1).then(function (response) {
            console.log(response);
            if (response.length > 0) {
                $rootScope._CD = response[0].caracter_decimal_moneda;
                $rootScope._CM = response[0].caracter_miles_moneda;
                $rootScope._SM = response[0].simbolo_moneda;
                $rootScope._DE = response[0].decimales * 1;
                $rootScope._TIPOCOTIZACION = response[0].tipocotizacion * 1;
                $rootScope._busquedaingreso = response[0].buscar_ingreso;
            }
        });
    }

    this.NumeroAleatorio = function () {
        var min = 1;
        var max = 1000000;
        return Math.random() * (max - min) + min;
    }


}).service('Socker', function ($rootScope, $timeout, General) {

    var websocket = null;

    this.init_socker = function () {
        if ("WebSocket" in window) {

            websocket = new WebSocket($rootScope.Conexion.url_socker + $rootScope.InicioSesion.codusu);
            websocket.onopen = function (data) {
                //$rootScope.CargarComboChat();
                $.jGrowl("Inicializado sesión en el CHAT interno", {life: 2500, theme: 'growl-success', header: ''});
            };

            websocket.onmessage = function (data) {
                setMessage(JSON.parse(data.data));
            };

            websocket.onerror = function (e) {
                console.log(e);
                $.jGrowl("Ha ocurrido un error, Cierre la aplicación", {life: 2500, theme: 'growl-warning', header: ''});

                this.cleanUp();
            };

            websocket.onclose = function (data) {
                cleanUp();

                var reason = (data.reason && data.reason !== null) ? data.reason : 'Goodbye';
                if (reason == "Goodbye") {
                    $.jGrowl("Sesión Cerrada", {life: 4500, theme: 'growl-warning', header: ''});
                }
            };
        } else {
            $.jGrowl("Chat no soportado", {life: 2500, theme: 'growl-warning', header: ''});
        }
    }

    var cleanUp = function () {
        if (websocket)
            websocket = null;
    }

    this.clean = function () {
        if (websocket)
            websocket = null;
    }

    var setMessage = function (msg) {
        var tipo = msg.tipo * 1;
        switch (tipo) {
            case 1:
                $rootScope.MostrarMensaje(msg);
                if ($rootScope.Chat.vermensaje == "SI") {
                    $timeout(function () {
                        $("#respuestachat").animate({scrollTop: $('#respuestachat')[0].scrollHeight}, 1000);
                    }, 500);
                }
                break;
            case 2:
                TablaTemporalIngreso();
                break;
            case 3:
                $rootScope.MostrarMensaje(msg);
                break;
            case 4:
                NotificacionesReporte(localStorage.getItem("IngresoSocker"), msg.message);
                LlenarChat();
                break;
            case 5:
                $.jGrowl("Actualizando gestón de cartera", {life: 4500, theme: 'growl-success', header: ''});
                TablaGestionCartera();
            case 6:
                $("#FotoAlumno").attr("src", msg.message);
                break;
            case 100:
                $rootScope.CargarComboChat();
                $.jGrowl(msg.message, {life: 2500, theme: 'growl-success', header: ''});
                break;
            case 101:
                $rootScope.CargarComboChat();
                $.jGrowl(msg.message, {life: 2500, theme: 'growl-warning', header: ''});
                break;
        }
    }



    this.sendMessage = function (tipo, mensaje, vista) {

        var messageContent = "";
        var fecha = '';

        if (!vista)
            vista = "0";

        switch (tipo) {
            case 1:
                messageContent = mensaje;
                if (messageContent == "")
                    return false;
                General.LlamarAjax("Configuracion", "opcion=FechaServidor&tipo=4", true, "", 0).then(function (response) {
                    fecha = $.trim(response);
                });
                break;
            case 3:
            case 4:
                General.LlamarAjax("Configuracion", "opcion=FechaServidor&tipo=4", true, "", 0).then(function (response) {
                    fecha = $.trim(response);
                });
                messageContent = mensaje;
                break;
            case 6:
                messageContent = mensaje;
                break;

        }

        $timeout(function () {
            var message = {
                username: $rootScope.InicioSesion.codusu,
                message: messageContent,
                tipo: tipo,
                vista: vista,
                fecha: fecha
            }

            console.log(message);

            websocket.send(JSON.stringify(message));

            if (tipo == 1) {
                $rootScope.Chat.mensaje = "";
                $.jGrowl("Mensaje enviado", {life: 2500, theme: 'growl-success', header: ''});
            }
            setMessage(message);
        }, 200);

    }

}).directive("select2", function ($timeout, $parse) {
    return {
        restrict: 'AC',
        require: 'ngModel',
        link: function (scope, element, attrs) {
            console.log(attrs);
            $timeout(function () {
                element.select2();
                element.select2Initialized = true;
            });

            var refreshSelect = function () {
                if (!element.select2Initialized)
                    return;
                $timeout(function () {
                    element.trigger('change');
                });
            };

            var recreateSelect = function () {
                if (!element.select2Initialized)
                    return;
                $timeout(function () {
                    element.select2('destroy');
                    element.select2();
                });
            };

            scope.$watch(attrs.ngModel, refreshSelect);

            if (attrs.ngOptions) {
                var list = attrs.ngOptions.match(/ in ([^ ]*)/)[1];
                // watch for option list change
                scope.$watch(list, recreateSelect);
            }

            if (attrs.ngDisabled) {
                scope.$watch(attrs.ngDisabled, refreshSelect);
            }
        }
    };

}).directive('autofocus', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function ($scope, $element) {
                $timeout(function () {
                    $element[0].focus();
                });
            }
        }
    }]);



