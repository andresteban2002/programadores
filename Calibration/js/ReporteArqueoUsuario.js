﻿
d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaI").val(output);
$("#FechaF").val(output);

$("#Usuarios").html(CargarCombo(4, 1));
$("#Almacen").html(CargarCombo(1, 1));
$("#Almacen").val($("#menu_idalmacen").val());
$("#Almacen").focus();
var nfilas = 0;
var OpcionReporte = 1;

$("form").submit(function (e) {
    e.preventDefault();
})

function Reporte(opcion) {
    
    var almacen = $("#Almacen").val();
    var usuario = $("#Usuarios").val();
    var fechaf = $("#FechaF").val();
    var fechad = $("#FechaI").val();
    var numero = $("#NroArqueo").val() * 1;

    var ordenado = 0;
    var anulado = 0;
       

    $("#resultadopdf").removeAttr("src");

    ActivarLoad();
    setTimeout(function () {
        var parametros = "fechad=" + fechad + "&fechah=" + fechaf + "&almacen=" + almacen + "&usuario=" + usuario + "&opcion=" + opcion + "&numero=" + numero; 
        var datos = LlamarAjax(url + "Arqueo/ReporteArqueos", parametros);
        datos = datos.split("|");
        if (datos[0] == "0") {
            switch (opcion) {
                case 1:
                    CargarTabla(datos[1]);
                    break;
                case 2:
                    window.open(url + "DocumPDF/" + datos[1]);
                    break;
            }
        } else
            swal("Alerta", datos[1], "warning");
        DesactivarLoad();
    }, 15);
}

function CargarTabla(datos) {
    if (datos != "[]") {
        var datajson = JSON.parse(datos);
        var table = $('#tablaconsulta').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "defaultContent": "<button class='arqueo btn-primary'><i class='glyphicon glyphicon-print' title='Imprimir Arqueo'></button>" },
                { "defaultContent": "<button class='gasto btn-success'><i class='glyphicon glyphicon-print' title='Imprimir Ingresos/Gastos'></button>" },
                { "data": "CV" },
                { "data": "Almacen" },
                { "data": "Usuario" },
                { "data": "NoCierre" },
                { "data": "Fecha" },
                { "data": "Efectivo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "Tarjeta", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "Cheque", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "ChequePosFecha", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "ConsigCliente", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "SaldoAnterior", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "Ingresos", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "OtrosIngresos", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "CajaMenor", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "OtrosEgresos", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "PorConsignar", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "Banco1"},
                { "data": "Planilla1" },
                { "data": "Efectivo1", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "Cheque1", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "Banco2" },
                { "data": "Planilla2" },
                { "data": "Efectivo2", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "Cheque2", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "Banco3"},
                { "data": "Planilla3" },
                { "data": "Efectivo3", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "Cheque3", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "Banco4"},
                { "data": "Planilla4" },
                { "data": "Efectivo4", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "Cheque4", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') }
            ],

            "columnDefs": [
                { "type": "numeric-comma", targets: 3 }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });

        data_arqueo("#tablaconsulta tbody", table);
    }

}

var data_arqueo = function (tbody, table) {

    $(tbody).on("click", "button.arqueo", function () {
        var data = table.row($(this).parents("tr")).data();
        
        var datos = LlamarAjax(url + "Arqueo/ImprimirArqueoUsuario", "numero=" + data.NoCierre + "&almacen=" + data.CV);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open(url + "Documpdf/" + datos[1])
        } else {
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion(datos[1]), "error");
        }
        
        return false;
    });

    $(tbody).on("click", "button.gasto", function () {
        var data = table.row($(this).parents("tr")).data();
        var datos = LlamarAjax(url + "Arqueo/ImprimirCajaMenor", "numero=" + data.NoCierre + "&almacen=" + data.CV + "&tipo=1");
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open(url + "Documpdf/" + datos[1])
        } else {
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion(datos[1]), "error");
        }
        return false;
    });
}




tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    id = $(this).attr("id");
    if ((e.keyCode == 13) && ((id != "Codigo") || (id != "CodigoB"))) {
        cb = parseInt($(this).attr('tabindex'));
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();
            return false;
        }
    }
}

$('select').select2();