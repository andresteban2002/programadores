var resActividades = [];
    var idSelect = null;
    $("#areasMult").html(CargarCombo(54, 1, "", ""));

    function buscarActividad() {
        var bodyHTML = '';
        resActividades = LlamarAjax("Actividades", "opcion=BuscarActividades&estado=" + $("#estadoBusq").val()).split("|");
        if (resActividades[0] == 0) {

            var JSONgeral = JSON.parse(resActividades[1]);
            for (var i = 0; i < JSONgeral.length; i++) {
                if ((JSONgeral[i].actividades) !== null && (JSONgeral[i].actividades) !== "null") {
                    bodyHTML += '<tr>' +
                        '<th onclick="selecActividad(' + i + ')">' + JSONgeral[i].identificacion + '</th><th onclick="selecActividad(' + i + ')">';
                    for (var j = 0; j < (JSONgeral[i].tipos).length; j++) {
                        bodyHTML += '<p>' + (JSONgeral[i].tipos)[j].tipo + '</p>';
                    }
                    bodyHTML += '</th>';
                    bodyHTML += '<th onclick="selecActividad(' + i + ')">' + JSONgeral[i].motivo + '</th><th>';
                    for (var j = 0; j < (JSONgeral[i].actividades).length; j++) {
                        bodyHTML += '<p>' + (JSONgeral[i].actividades)[j].actividad + '.<br/> Tiempo de aplicación: ' + (JSONgeral[i].actividades)[j].dias + ' dias y ' + (JSONgeral[i].actividades)[j].horas + ' horas</p>';
                    }
                    bodyHTML += '</th><th><button class="btn btn-glow btn-success" type="button" onclick="ejectReporte(' + (JSONgeral[i].identificacion) + ')">Reporte</button>&nbsp;'
                        + '<button class="btn btn-glow btn-warning" type="button" onclick="eliminarA(' + (JSONgeral[i].identificacion) + ')">Eliminar</button></th></tr>';
                }
            }
            if (bodyHTML == '')
                bodyHTML += '<tr><th colspan="5" class="text-center">Sin resultados</th></tr>';
        }
        $("#tBodyActidades").html(bodyHTML);
    }

    function selecActividad(id) {
        idSelect = Number(id);
        if (!isNaN(id) && String(id) !== "undefined" && String(resActividades) !== '[]') {
            var JSONgeral = JSON.parse(resActividades[1]);
            var date = new Date(JSONgeral[Number(id)].fecha_inicio);
            var dateLate = new Date(JSONgeral[Number(id)].fecha_inicio);
            var dateHoy = new Date();
            var dateEval = new Date(JSONgeral[Number(id)].fe_eval);
            var motivo = bodyHTML = nu_dias = nu_horas = fe_inicio = fe_min_inicio = fe_verificacion = fe_min_verificacion = fe_fin = prioridad = doc_rigen = doc_afecta = pasos_verif = usr_eval = cargo_eval = usr_creo = cargo_creo = "";

            if (Number(JSONgeral[Number(id)].estatus) == 0) {
                motivo = String(JSONgeral[Number(id)].motivo);
                for (var j = 0; j < (JSONgeral[Number(id)].actividades).length; j++) {
                    /*horas y dias por actividad*/
                    nu_dias = parseInt((JSONgeral[Number(id)].actividades)[j].dias);
                    nu_horas = parseInt((JSONgeral[Number(id)].actividades)[j].horas);
                    /**/
                    bodyHTML += '<tr>' +
                        '<th>' + JSONgeral[Number(id)].identificacion + '</th>';
                    bodyHTML += '<th><p>' + (JSONgeral[Number(id)].actividades)[j].actividad + '</p></th>';
                    bodyHTML += '<th><input min="0" max="100" onchange="cambFecha()" id="diaVer' + j + '" class="form-control input-number" type="number" value="' + nu_dias + '" readonly /></th>';
                    bodyHTML += '<th><input min="0" max="100" onchange="cambFecha()" id="horaVer' + j + '" class="form-control input-number" type="number" value="' + nu_horas + '" readonly /></th></tr>';
                    dateLate.setDate(dateLate.getDate() + nu_dias);
                    dateLate.setHours(dateLate.getHours() + nu_horas);
                }
                fe_inicio = date.getFullYear() + '-' + (Number(date.getMonth() + 1) < 10 ? '0' + Number(date.getMonth() + 1) : Number(date.getMonth() + 1)) + '-' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
                fe_min_inicio = dateHoy.getFullYear() + '-' + (Number(dateHoy.getMonth() + 1) < 10 ? '0' + Number(dateHoy.getMonth() + 1) : Number(dateHoy.getMonth() + 1)) + '-' + (dateHoy.getDate() < 10 ? '0' + dateHoy.getDate() : dateHoy.getDate());
                fe_min_verificacion = dateLate.getFullYear() + '-' + (Number(dateLate.getMonth() + 1) < 10 ? '0' + Number(dateLate.getMonth() + 1) : Number(dateLate.getMonth() + 1)) + '-' + (dateLate.getDate() < 10 ? '0' + dateLate.getDate() : dateLate.getDate());
                usr_creo = (JSONgeral[Number(id)].nombre_creo);
                usr_eval = (JSONgeral[Number(id)].cargo_creo);
                $("#prioridad, #chMayorT, #ch_edi_fe_ini, #afectSG").removeAttr("disabled");
                $("#fecVeri, #docOrigen, #pasosVerif").removeAttr("readonly");
                $("#prioridad").val("0").change();

                $("#motivo").val(motivo);
                $("#tBodyCrearAct").html(bodyHTML);
                $("#fe_lat").val(fe_min_verificacion);
                $("#fecha_inicio").attr("min", fe_min_inicio).val(fe_inicio).change();
                $("#usuarioCreo").val(usr_creo);
                $("#cargoCreo").val(usr_eval);
                $("#docOrigen").val("");
                $("#docAfect").val("");
                $("#pasosVerif").val("");

                $("#observCierre, #datoUsr, #datoCargo").addClass("collapse");
            }
            else {
                motivo = String(JSONgeral[Number(id)].motivo);
                for (var j = 0; j < (JSONgeral[Number(id)].actividades).length; j++) {
                    /*horas y dias por actividad*/
                    nu_dias = parseInt((JSONgeral[Number(id)].actividades)[j].dias_eval);
                    nu_horas = parseInt((JSONgeral[Number(id)].actividades)[j].horas_eval);
                    /**/
                    bodyHTML += '<tr>' +
                        '<th>' + JSONgeral[Number(id)].identificacion + '</th>';
                    bodyHTML += '<th><p>' + (JSONgeral[Number(id)].actividades)[j].actividad + '</p></th>';
                    bodyHTML += '<th><input min="0" max="100" onchange="cambFecha()" id="diaVer' + j + '" class="form-control input-number" type="number" value="' + nu_dias + '" readonly /></th>';
                    bodyHTML += '<th><input min="0" max="100" onchange="cambFecha()" id="horaVer' + j + '" class="form-control input-number" type="number" value="' + nu_horas + '" readonly /></th></tr>';
                    dateLate.setDate(dateLate.getDate() + nu_dias);
                    dateLate.setHours(dateLate.getHours() + nu_horas);
                }
                fe_inicio = date.getFullYear() + '-' + (Number(date.getMonth() + 1) < 10 ? '0' + Number(date.getMonth() + 1) : Number(date.getMonth() + 1)) + '-' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
                fe_min_inicio = (Number(dateHoy.getMonth() + 1) < 10 ? '0' + Number(dateHoy.getMonth() + 1) : Number(dateHoy.getMonth() + 1)) + '-' + (dateHoy.getDate() < 10 ? '0' + dateHoy.getDate() : dateHoy.getDate());
                fe_min_verificacion = dateLate.getFullYear() + '-' + (Number(dateLate.getMonth() + 1) < 10 ? '0' + Number(dateLate.getMonth() + 1) : Number(dateLate.getMonth() + 1)) + '-' + (dateLate.getDate() < 10 ? '0' + dateLate.getDate() : dateLate.getDate());
                fe_verificacion = dateEval.getFullYear() + '-' + (Number(dateEval.getMonth() + 1) < 10 ? '0' + Number(dateEval.getMonth() + 1) : Number(dateEval.getMonth() + 1)) + '-' + (dateEval.getDate() < 10 ? '0' + dateEval.getDate() : dateEval.getDate());
                prioridad = parseInt(JSONgeral[Number(id)].prioridad);
                doc_rigen = String(JSONgeral[Number(id)].doc_rigen);
                doc_afecta = String(JSONgeral[Number(id)].doc_afecta);
                pasos_verif = String(JSONgeral[Number(id)].pasos);
                usr_eval = JSONgeral[Number(id)].nombre_evaluador;
                cargo_eval = JSONgeral[Number(id)].cargo_evaluador;
                $("#motivo").val(motivo);
                $("#tBodyCrearAct").html(bodyHTML);
                $("#fe_lat").val(fe_min_verificacion);
                $("#fecha_inicio").attr("min", fe_min_inicio).val(fe_inicio).change();
                $("#fecVeri").val(fe_verificacion);
                $("#prioridad").val(prioridad).change();
                $("#docOrigen").val("").val(doc_rigen);
                $("#docAfect").val("").val(doc_afecta);
                $("#pasosVerif").val("").val(pasos_verif);
                $("#guardEval").addClass("collapse");
                $("#usuarioEval").val(usr_eval);
                $("#cargoEval").val(cargo_eval);
                $("#observCierre, #datoUsr, #datoCargo").removeClass("collapse");

                $("#prioridad,#chMayorT, #ch_edi_fe_ini, #afectSG").attr("disabled", true);
                $("#fecVeri, #docOrigen, #pasosVerif").attr("readonly", true);
            }

            /********************************************************/
            $('#tabActividades a[href="#tabcrear"]').tab('show');
        }
    }

    function activarCampos(id, estado) {

        if ($("#chMayorT").prop('checked') == true) {
            var JSONgeral = JSON.parse(resActividades[1]);
            var cantActi = (JSONgeral[Number(idSelect)].actividades).length;
            for (var i = 0; i < cantActi; i++) {
                $("#diaVer" + i).removeAttr('readonly');
                $("#horaVer" + i).removeAttr('readonly');
            }
        } else {
            var JSONgeral = JSON.parse(resActividades[1]);
            var cantActi = (JSONgeral[Number(idSelect)].actividades).length;
            for (var i = 0; i < cantActi; i++) {
                $("#diaVer" + i).val((JSONgeral[Number(idSelect)].actividades)[i].dias).attr('readonly', true);
                $("#horaVer" + i).val((JSONgeral[Number(idSelect)].actividades)[i].horas).attr('readonly', true);
            }
        }

        if (String(id).startsWith("chArea")) {
            $("#areasMult").prop("disabled", !estado);
        }
        if (String(id).startsWith("afectSG")) {
            $("#docAfect").prop("disabled", !estado);
            $("#afectSG").val((estado == true ? 1 : 0));
        }
        if (String(id).startsWith("ch_edi_fe_ini")) {
            $("#fecha_inicio").prop("readonly", !estado);
        }
    }

    function guardarEvaluacion() {
        var JSONgeral = JSON.parse(resActividades[1]);
        var idAct = (JSONgeral[Number(idSelect)].identificacion);
        var cantAct = (JSONgeral[Number(idSelect)].actividades).length;
        var actividades = "";

        var areas = $("#areasMult").val();
        var prioridad = ($("#prioridad").val() == null ? 0 : parseInt($("#prioridad").val())),
            sistGestion = $("#afectSG").val(),
            docAfecta = $("#docAfect").val(),
            docRigen = $("#docOrigen").val(),
            fechaVeri = $("#fecVeri").val(),
            pasVerif = $("#pasosVerif").val();

        if (fechaVeri == null || String(fechaVeri) == "undefined")
            swal("Acción Cancelada", "Debes agregar una fecha de revision para poder finalizar", "warning");
        if (pasVerif.trim() == "" || String(pasVerif) == "undefined")
            swal("Acción Cancelada", "Debes ingresar los pasos necesarios para realizar la verificacion de la actividad", "warning");
        if (docRigen.trim() == "" || String(docRigen) == "undefined")
            swal("Acción Cancelada", "Debes describir cuales son los ducumentos por los que se rige este cambio", "warning");


        if (fechaVeri !== null && String(fechaVeri) !== "undefined" && pasVerif.trim() !== "" && String(pasVerif) !== "undefined" && docRigen.trim() !== "" && String(docRigen) !== "undefined") {
            for (var i = 0; i < cantAct; i++) {
                actividades += 'UP :=' + $("#diaVer" + i).val() + ', #=' + $("#horaVer" + i).val() + ' °=' + (JSONgeral[Number(idSelect)].actividades)[i].identif + ';';
            }
            var guardarEval = LlamarAjax("Actividades", "opcion=GuardarActividadEval&idActividad=" + idAct + "&areas=" + areas + "&prioridad=" + prioridad + "&sist_gestion=" + sistGestion + "&docafecta=" + docAfecta + "&docrigen=" + docRigen + "&fechaverif=" + fechaVeri + "&pasverifi=" + pasVerif + "&actividades=" + actividades + "&fecha_ini_eval=" + $("#fecha_inicio").val()).split("|");
            if (Number(guardarEval[0]) == 0) {
                if (guardarEval[2] !== [] && String(guardarEval[2]) !== '[]' && guardarEval[2].length > 0) {
                    let usuarios = JSON.parse(guardarEval[2]);
                    let usuariosEnvio = "/";
                    for (let i = 0; i < usuarios.length; i++) {
                        usuariosEnvio += usuarios[i].identificacion + "/";
                    }
                    sendMessage(3, "Ha sido aprobada una actividad [" + $("#motivo").val() + "] que se espera afecte tu area, por favor revisar en que consiste y tomar las medidas que te sean pertinentes", usuariosEnvio);
                }

                swal("", guardarEval[1], "success");
            } else {
                swal("Acción Cancelada", guardarEval[1], "warning");
            }

        }

    }

    function cambFecha() {
        var JSONgeral = JSON.parse(resActividades[1]);
        var date = new Date(JSONgeral[Number(idSelect)].fecha_inicio);
        var date2 = new Date($("#fecha_inicio").val());
        var horas = 0;
        var sabado = domingo = 0;
        if (date !== date2)
            date = date2;

        date2 = new Date($("#fecha_inicio").val());
        for (var i = 0; i < (JSONgeral[Number(idSelect)].actividades).length; i++) {
            horas += parseInt($("#horaVer" + i).val());

            for (var j = 0; j < parseInt($("#diaVer" + i).val()); j++) {
                date2.setDate(date2.getDate() + 1);
                while (date2.getDay() == 6 || date2.getDay() == 0) {
                    if (date2.getDay() == 6) {
                        sabado++;
                    }
                    if (date2.getDay() == 0) {
                        domingo++;
                    }
                    date2.setDate(date2.getDate() + 1);
                }
            }
        }
        if (domingo > 0 || sabado > 0) {
            swal("Advertencia", "Los rangos de fechas de ejecucion de estas axctividades cuentan con [" + sabado + "] SABADOS y [" + domingo + "] DOMINGOS, ten eso en cuanta al seleccionar la fecha de inicio", "success");
        }
        let fechaCulm = date2.getFullYear() + '-' + (parseInt(date2.getMonth() + 1) < 10 ? '0' + Number(date2.getMonth() + 1) : Number(date2.getMonth() + 1)) + '-' + (parseInt(date2.getDate()) < 10 ? '0' + parseInt(date2.getDate()) : parseInt(date2.getDate()));
        $("#fe_lat").val(fechaCulm);
        $("#fecVeri").attr("min", fechaCulm);
    }

    function ejectReporte(id) {
        var genReporte = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=actividad&documento=0&id=" + id).split("||");
        if (genReporte[0] == 0) {
            window.open(url_cliente + "DocumPDF/" + genReporte[1])
        }
    }

    function eliminarA(id) {
        var genReporte = LlamarAjax("Actividades", "opcion=EliminarActividad&id=" + id).split("|");
        if (genReporte[0] == 0) {
            
        }
    }

    $('select').select2();
    DesactivarLoad();