angular.module('myApp', ['ui', 'ngSanitize', 'ngTable']).controller('ControlIngresos', function ($scope, $ocLazyLoad, $rootScope, General, NgTableParams) {

    function initTabs() {
        tabClasses = ["", "", ""];
    }

    $scope.getTabClass = function (tabNum) {
        return tabClasses[tabNum];
    };

    $scope.getTabPaneClass = function (tabNum) {
        return "tab-pane " + tabClasses[tabNum];
    }

    $scope.setActiveTab = function (tabNum) {
        initTabs();
        tabClasses[tabNum] = "active";
    };

    function initTabsSer() {
        tabClassesser = ["", ""];
    }

    $scope.getTabSerClass = function (tabNum) {
        return tabClassesser[tabNum];
    };

    $scope.getTabPaneSerClass = function (tabNum) {
        return "pane-ser " + tabClassesser[tabNum];
    }

    $scope.setActiveSerTab = function (tabNum) {
        initTabsSer();
        tabClassesser[tabNum] = "active";
        $scope.TipoServicio = tabNum;
    };


    if ($rootScope.global.movil == 0) {
        Webcam.set({
            width: 650,
            height: 510,
            dest_width: 650,
            dest_height: 510,
            image_format: 'jpeg',
            jpeg_quality: 100
        });
    } else {
        Webcam.set({
            width: 400,
            height: 480,
            dest_width: 780,
            dest_height: 960,
            image_format: 'jpeg',
            jpeg_quality: 100
        });
    }



    $scope.Documento = "";
    $scope.OpcionEquipo = 0;
    $scope.OpcionMagnitud = 0;
    $scope.PDFRemision = 0,
            $scope.Remision = "",
            $scope.SerieIngreso = "";
    $scope.Pdf_Remision = "";

    $scope.VisibleServicio = [true, true];
    $scope.TipoServicio = 0;
    $scope.Titulo_Serie = "";
    $scope.Titulo_Identificacion = "";

    

    $rootScope.$watch("_busquedaingreso", function (newValue, oldValue) {


        if (oldValue != newValue) {

            if (newValue == "SERIE") {
                $scope.Titulo_Serie = "Serie";
                $scope.Titulo_Identificacion = "Id Interno";
            } else {
                $scope.Titulo_Serie = "Id Interno";
                $scope.Titulo_Identificacion = "Serie";
            }
            
            alert(newValue);
        }
    });


    $scope.Cliente = {
        id: 0,
        documento: ""
    }

    $scope.Combos = {
        sede: "",
        contacto: "",
        magnitud: "",
        bequipo: "",
        equipo: "",
        equipo_pesa: "",
        marca: "",
        marca_pesa: "",
        medida: "",
        cliente: "",
        modelo: "",
        modelo_pesa: "",
        bmodelo: "",
        bintervalo: "",
        intervalo: "",
        pais: "",
        departamento: "",
        ciudad: "",
        usuario: "",
        accesorio: "",
        accesorio_pesa: "",
        observacion: "",
        observacion_pesa: "",
        grupo: ""

    }

    $rootScope.ObtenerDatatable = function (data) {
        $scope.Documento = $.trim(data.documento);
        $scope.BuscarCliente("", "");
        $rootScope.Modal.Close();
    }


    $scope.InicializarRegistro = function () {
        $scope.Registro = {
            id: 0,
            cliente: 0,
            archivo: "",
            sede: "",
            remision: "",
            cotizacion: "",
            contacto: "",
            estado: "Temporal",
            correoempresa: "",
            observaciones: "",
            recepcion: "",
            usuario: "",
            fechareg: "",
            totales: "",
            usuarioreg: "",
            pdf: 0,
            Pdf_Remision: "",
            btnguardar: true,
            tabla: {},
            tequipo: 0,

        }
    }

    $scope.InicializarIngreso = function () {
        $scope.Ingreso = {
            id: 0,
            magnitud: "",
            equipo: "",
            marca: "",
            modelo: "",
            observacion: "",
            cantidad: "1",
            accesorio: "",
            serie: "",
            ingreso: "",
            intervalo: "0",
            intervalo2: "",
            intervalo3: "",
            sitio: "NO",
            convenio: "0",
            garantia: "NO",
            dis_campos: true,
            dis_serie: false,
            TEquipos: 0,
            comaccesorio: "",
            comobservacion: "",

        }
    }

    $scope.InicializarPesa = function () {
        $scope.Pesa = {
            id: 0,
            magnitud: "",
            equipo: "",
            marca: "",
            modelo: "",
            observacion: "",
            cantidad: "0",
            accesorio: "",
            serie: "",
            ident_equipo: "",
            ingreso: "",
            sitio: "NO",
            convenio: "0",
            garantia: "NO",
            dis_campos: true,
            dis_serie: false,
            TEquipos: 0,
            comaccesorio: "",
            comobservacion: ""
        }
    }


    $scope.CargarCombos = function () {
        $rootScope.global.CargandoPrinicipal = true;
        var parametros = "opcion=CargaComboInicial&tipo=2&json=1";
        General.LlamarAjax("Configuracion", parametros, true, "").then(function (response) {
            var datos = $.trim(response).split("||");
            $scope.Combos.magnitud = JSON.parse(datos[0]);
            $scope.Combos.bequipo = JSON.parse(datos[1]);
            $scope.Combos.equipo = JSON.parse(datos[31]);
            $scope.Combos.equipo_pesa = JSON.parse(datos[31]);
            $scope.Combos.marca = JSON.parse(datos[2]);
            $scope.Combos.marca_pesa = JSON.parse(datos[2]);
            $scope.Combos.medida = JSON.parse(datos[4]);
            $scope.Combos.cliente = JSON.parse(datos[5]);
            $scope.Combos.bmodelo = JSON.parse(datos[6]);
            $scope.Combos.bintervalo = JSON.parse(datos[7]);
            $scope.Combos.pais = JSON.parse(datos[10]);
            $scope.Combos.departamento = JSON.parse(datos[11]);
            $scope.Combos.ciudad = JSON.parse(datos[12]);
            $scope.Combos.usuario = JSON.parse(datos[13]);
            $scope.Combos.accesorio = JSON.parse(datos[14]);
            $scope.Combos.observacion = JSON.parse(datos[15]);
            $rootScope.global.CargandoPrinicipal = false;
        });
    }


    $scope.Consulta = {
        remision: '',
        recepcion: '',
        ingreso: '',
        cliente: '',
        estado: '0',
        magnitud: '',
        equipo: '',
        marca: '',
        modelo: '',
        intervalo: '',
        serie: '',
        pais: '',
        departamento: '',
        ciudad: '',
        usuario: '',
        sitio: '',
        convenio: '',
        garantia: '',
        fechad: {
            value: new Date(output + " 00:00"),
            id: output
        },
        fechah: {
            value: new Date(output2 + " 00:00"),
            id: output2
        }
    }


    $scope.ConsultaIngreso = {
        remision: "",
        recepcion: "",
        ingresos: "",
        cliente: "",
        magnitud: "",
        equipo: "",
        marca: "",
        modelo: "",
        intervalo: "",
        serie: "",
        ciudad: "",
        usuario: "",
        sitio: "",
        convenio: "",
        garantia: "",
        fechad: {
            value: new Date(output + " 00:00"),
            id: output
        },
        fechah: {
            value: new Date(output2 + " 00:00"),
            id: output2
        }
    }


    $scope.BuscarCliente = function (sede, contacto) {
        var documento = $.trim($scope.Documento);
        if (sede == "") {
            if ($.trim(documento) == $scope.Cliente.documento || $.trim(documento) == "") {
                return false;
            }
            $scope.LimpiarTodo();
            $scope.Documento = documento;
        }

        var parametros = "opcion=BuscarCliente&documento=" + documento + "&bloquear=0&json=1";
        General.LlamarAjax("Cotizacion", parametros, true, "", 0).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                var data = JSON.parse(datos[1]);
                $scope.Cliente = data[0];
                $scope.Combos.contacto = JSON.parse(datos[2]);
                $scope.Combos.sede = JSON.parse(datos[3]);
                $scope.Registro.cliente = $scope.Cliente.id;
                if (sede != "") {
                    $scope.Registro.sede = sede;
                    $scope.Registro.contacto = contacto;
                }
                $scope.TablaTemporalIngreso();

            }
        });
    }



    $scope.DatosContacto = function (contacto) {
        $scope.Cliente.email = "";
        $scope.Cliente.telefono = "";
        $scope.Cliente.celular = "";

        General.LlamarAjax("Cotizacion", "opcion=DatosContacto&id=" + contacto, false, "", 1).then(function (response) {
            if (response.length > 0) {
                $scope.Cliente.email = response[0].email;
                $scope.Cliente.telefono = response[0].telefonos;
                $scope.Cliente.celular = response[0].fax;
            }
        });

    }




    $scope.ModalCambioCliente = function () {

        if ($scope.Remision == 0)
            return false;

        $("#AcCliente").html($($scope.Registro.cliente).val() + " (" + $("#Cliente").val() + ")");
        $("#AcDireccion").html($("#Sede option:selected").text());
        $("#AcContacto").html($("#Contacto option:selected").text());

        ($scope.Registro.cliente = "");
        ($scope.Registro.contacto = "");
        ($scope.Registro.sede = "");

        $("#modalCambioEmpresa").modal("show");
    }


    $scope.ModalArticulos = function () {
        if ($scope.Registro.cliente > 0) {
            $scope.LimpiarIngreso();
            $scope.Contadores(2);
            $("#ModalArticulos").modal("show");
        }
    }

    $scope.ModalClientes = function () {
        $rootScope.Modal.cedula = "";
        $rootScope.Modal.razonsocial = "";
        $rootScope.Modal.tipo = 2;
        $rootScope.Modal.Open("Clientes");
    }


    $scope.ModalEquipos = function () {
        if (($scope.Registro.estado == 'Temporal' || $scope.Registro.estado == "Cotizado") && ($scope.Registro.cliente > 0)) {
            if ($scope.Registro.sede == "") {
                $("#Sede").focus();
                swal("Acción Cancelada", "Debe seleccionar la sede del cliente", "warning");
                return false;
            }
            if ($scope.Registro.contacto == "") {
                $("#Contacto").focus();
                swal("Acción Cancelada", "Debe seleccionar el contacto del cliente", "warning");
                return false;
            }
            $("#ModalEquipos").modal({backdrop: 'static'}, 'show');
        }
    };


    $scope.LimpiarTodo = function () {
        $scope.Documento = "";
        $scope.Remision = "";
        $scope.LimpiarDatos();
    }

    $scope.LimpiarDatos = function () {
        $scope.Cliente = {documento: ""};
        $scope.InicializarRegistro();
        $scope.Combos.sede = "";
        $scope.Combos.contacto = "";
    }

    $scope.LimpiarIngresoTodo = function () {
        $scope.LimpiarIngreso();
        $("#Serie").val("");
    }


    $scope.LimpiarIngreso = function () {
        $scope.Ingreso.serie = "";
        $scope.InicializarIngreso();



    }


    $scope.CargarIntervalos = function (magnitud, intervalo, intervalo2, intervalo3) {
        General.CargarCombo(6, magnitud).then(function (response) {
            $scope.Combos.intervalo = response.data;
            if (intervalo != "") {
                $scope.Ingreso.intervalo = intervalo;
            }
            if (intervalo2 != "") {
                $scope.Ingreso.intervalo2 = intervalo2;
            }
            if (intervalo3 != "") {
                $scope.Ingreso.intervalo3 = intervalo3;
            }
        });
        if ($scope.OpcionEquipo == 0) {
            $scope.OpcionMagnitud = 1;
            $scope.Ingreso.equipo = "";
            if (magnitud * 1 > 0) {
                General.CargarCombo(2, magnitud).then(function (response) {
                    $scope.Combos.equipo = response.data;
                    $scope.OpcionMagnitud = 0;
                });
            } else
            {
                General.CargarCombo(58, "").then(function (response) {
                    $scope.Combos.equipo = response.data;
                    $scope.OpcionMagnitud = 0;
                });
                $scope.Combos.intervalo = "";
            }
        }
    }


    $scope.MagnitudEquipo = function (equipo, intervalo, intervalo2, metodo, clase, tipo) {
        if (tipo == 1) {
            if ($scope.OpcionMagnitud > 0)
                return false;
        }
        General.CargarCombo(17, equipo).then(function (response) {
            if (tipo == 1)
                $scope.Combos.metodo = response.data;
            else
                $scope.Combos.metodo_pesa = response.data;

            if (metodo != "") {
                if (tipo == 1)
                    $scope.Servicio.metodo = metodo;
                else
                    $scope.Pesa.metodo = metodo;
            }
        });

        if (tipo == 1) {
            var parametros = "opcion=MagnitudEquipo&id=" + equipo;
            General.LlamarAjax("Cotizacion", parametros, false, "", 1).then(function (response) {
                if (response.length > 0) {
                    $scope.OpcionEquipo = 1;
                    if (response[0].idmagnitud * 1 != $scope.Servicio.magnitud * 1) {
                        $scope.Servicio.magnitud = $.trim(response[0].idmagnitud);
                        $scope.CargarIntervalos($scope.Servicio.magnitud, intervalo, intervalo2);
                        $scope.OpcionEquipo = 0;
                    } else {
                        $scope.OpcionEquipo = 0;
                    }
                }
            });
        } else {
            General.CargarCombo(110, equipo).then(function (response) {
                $scope.Combos.clase = response.data;
                if (clase != "") {
                    $scope.Pesa.clase = clase;
                    $scope.TablaClases();
                }
            });
            var parametros = "opcion=MagnitudEquipo&id=" + equipo;
            General.LlamarAjax("Cotizacion", parametros, false, "", 1).then(function (response) {
                if (response.length > 0) {
                    $scope.Pesa.magnitud = $.trim(response[0].idmagnitud);
                }
            });
        }
    }


    $scope.CargarModelos = function (marca, modelo, tipo) {
        General.CargarCombo(5, marca).then(function (response) {

            if (tipo == 1) {
                $scope.Combos.modelo = response.data;
                if (modelo != "")
                    $scope.Servicio.modelo = modelo;
            } else {
                $scope.Combos.modelo_pesa = response.data;
                if (modelo != "")
                    $scope.Pesa.modelo = modelo;
            }
        });
    }


    $scope.TablaTemporalIngreso = function () {
        var parametros = "opcion=TemporalIngreso&cliente=" + $scope.Registro.cliente + "&remision=" + $scope.Registro.id;
        General.LlamarAjax("Cotizacion", parametros, true, "", 1).then(function (data) {
            $scope.Registro.tabla = data;
        });
    }



    $scope.GuardarIngreso = function () {
        var mensaje = "";

        var remisiones = document.getElementsByName("$scope.Ingreso.remision[]");

        for (var x = 0; x < remisiones.length; x++) {
            if (remisiones[x].value * 1 > 0 && $scope.Ingreso.remision == 0) {
                swal("Acción Cancelada", "No se puede guardar esta remisión porque fue guardada en otra computadora", "warning");
                return false;
            }
        }

        if ($scope.Ingreso.ingreso == 0) {
            mensaje = "<br> Debe de ingresar un número de ingreso";
            $("#Ingreso").focus();
        }

        if ($scope.Ingreso.observacion == "") {
            mensaje = "<br> Debe de ingresar la observación del equipo " + mensaje;
            $("#Observacion").focus();
        }

        if ($.trim($scope.Ingreso.serie) == "") {
            mensaje = "<br> Debe de ingresar la serie del equipo " + mensaje;
            $("#Serie").focus();
        }

        if ($scope.Ingreso.modelo == "") {
            mensaje = "<br> Debe de seleccionar un modelo " + mensaje;
            $("#Modelo").focus();
        }
        if ($scope.Ingreso.marca == "") {
            mensaje = "<br> Debe de seleccionar una marca " + mensaje;
            $("#Marca").focus();
        }
        if ($scope.Ingreso.equipo == "") {
            mensaje = "<br> Debe de seleccionar un equipo " + mensaje;
            $("#Equipo").focus();
        }

        if (mensaje != "") {
            swal("Acción Cancelada", mensaje, "warning");
            return false;
        }

        var parametros = "opcion=GuardarTmpIngreso&cliente=" + $scope.Ingreso.cliente + "&id=" + $scope.Ingreso.id + "&equipo=" + $scope.Ingreso.equipo + "&modelo=" + $scope.Ingreso.modelo + "&convenio=" + $scope.Ingreso.convenio +
                "&observacion=" + $scope.Ingreso.observacion + "&serie=" + $scope.Ingreso.serie + "&ingreso=" + $scope.Ingreso.ingreso + "&cantidad=" + $scope.Ingreso.cantidad + "&intervalo=" + $scope.Ingreso.intervalo + "&intervalo2=" + $scope.Ingreso.intervalo2 + "&intervalo3=" + $scope.Ingreso.intervalo3 +
                "&remision=" + $scope.Ingreso.remision + "&cotizacion=" + $scope.Ingreso.cotizacion + "&sitio=" + $scope.Ingreso.sitio + "&garantia=" + $scope.Ingreso.garantia + "&accesorio=" + $scope.Ingreso.accesorio;

        General.LlamarAjax("Cotizacion", parametros, true, "", 1).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                $("#Serie").focus();
                $scope.Registro.estado = "Temporal";
                swal("", datos[1], "success");
                $scope.Contadores();
                $("#ModalArticulos").modal("hide");
                //CantidadEquipos();
                $scope.Ingreso.id = 0;
                $scope.Ingreso.intervalo = "";
                $scope.Ingreso.observacion = "";
                $scope.Ingreso.accesorio = "";
                $scope.Ingreso.serie = "";
                //sendMessage(2, '');
            } else
                swal("Acción Cancelada", datos[1], "warning");
        });

    }


    $scope.GuardarIntervalo = function () {
        var magnitud = $("#Magnitud").val() * 1;
        var medida = $("#IMedida option:selected").text();
        var desde = $.trim($("#IDesde").val());
        var hasta = $.trim($("#IHasta").val());
        var numero = $("#NumIntervalo").val() * 1;
        if (numero == 1)
            numero = "";

        var mensaje = "";

        if ($.trim($("#IHasta").val()) == "") {
            $("#IHasta").focus();
            mensaje = "<br> *Ingrese el rango hasta" + mensaje;
        }
        if ($.trim($("#IDesde").val()) == "") {
            $("#IDesde").focus();
            mensaje = "<br> *Ingrese el rango desde" + mensaje;
        }

        if (medida == "--Seleccione--") {
            $("#IMedida").focus();
            mensaje = "<br> *Seleccione una unidad de medida" + mensaje;
        }

        if (mensaje != "") {
            swal("Verifique los siguientes campos", mensaje, "warning");
            return false;
        }

        var parametros = "opcion=GuardarIntervalo&magnitud=" + magnitud + "&medida=" + medida + "&desde=" + desde + "&hasta=" + hasta;
        General.LlamarAjax("Cotizacion", parametros, true, "").then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos * 1 > 0) {
                $("#Intervalo" + numero).html(CargarCombo(6, 1, "", magnitud));
                $("#Intervalo" + numero).val(datos).trigger("change");
                $("#modalIntervalo").modal("hide");
            } else {
                swal("Acción Cancelada", "Ya existe un rango de intervalo con estas descripciones", "warning");
            }
        });
    }




    $scope.ValidarCliente = function (event) {
        var documento = $.trim($scope.Documento);
        if (documento == "") {
            return false;
        }
        if (documento != $scope.Cliente.documento) {
            if ($scope.Cliente.documento != "") {
                $scope.LimpiarDatos();
            }
            if (event.keyCode == 13) {
                $("#Sede").focus();
            }
        }
    }


    $scope.ValidarSerie = function (serie) {
        if (serie != $scope.Ingreso.serie) {
            $scope.Ingreso.magnitud = "";
            $scope.Ingreso.equipo = "";
            $scope.Ingreso.modelo = "";
            $scope.Ingreso.marca = "";
            $scope.Ingreso.intervalo = "";
            $scope.Ingreso.intervalo2 = "";
            $scope.Ingreso.intervalo3 = "";
            $scope.Ingreso.selaccesorio = "";
            $scope.Ingreso.selobservacion = "";
            $scope.Ingreso.observacion = "";
            $scope.Ingreso.cantidad = "";
            $scope.Ingreso.accesorio = "";
            $scope.Ingreso.sitio = "";
            $scope.Ingreso.garantia = "";
            $scope.Ingreso.convenio = "";
            if ($($scope.Ingreso.equipo).val() * 1 > 0 || $($scope.Ingreso.marca).val() * 1 > 0 || $($scope.Ingreso.modelo).val() * 1 > 0 || $($scope.Ingreso.intervalo).val() * 1 > 0) {
                $scope.LimpiarIngreso();
                $scope.Ingreso.serie = "";
            }
        }
    }

    $scope.BuscarRemision = function () {
        var numero = $.trim($scope.Remision);
        if ($scope.Registro.remision == numero || numero == "")
            return false;

        if ($scope.Registro.remision != "")
            $scope.InicializarRegistro();

        var parametros = "opcion=BuscarRemision&remision=" + numero;
        General.LlamarAjax("Cotizacion", parametros, true, "", 1).then(function (data) {

            if (data.length > 0) {
                $scope.Registro.id = data[0].id;
                $scope.Registro.estado = data[0].estado;
                $scope.Registro.pdf = data[0].pdf * 1;
                $scope.Registro.cliente = data[0].idcliente * 1;
                $scope.Registro.remision = $.trim(data[0].remision);
                $scope.Documento = data[0].documento;

                $scope.Registro.recepcion = data[0].recepcion;
                $scope.Registro.fechareg = data[0].fecha;
                $scope.Registro.usuarioreg = data[0].usuario;
                $scope.Registro.observaciones = data[0].observacion;

                $scope.BuscarCliente($.trim(data[0].idsede), $.trim(data[0].idcontacto));

            } else {
                $scope.Remision = "";
                $("#Remision").focus();
                swal("Acción Cancelada", "Remisión número " + numero + " no registrada", "warning");
            }
        });
    }

    $scope.BuscarSerie = function () {

        var serie = $.trim($scope.SerieIngreso);
        if (serie == $scope.Ingreso.serie || serie == "")
            return false;

        $scope.InicializarIngreso();

        $scope.Ingreso.serie = serie;

        var parametros = "opcion=BuscarIngreso&ingreso=0&serie=" + serie + "&cliente=" + $scope.Registro.cliente;
        General.LlamarAjax("Cotizacion", parametros, true, "", 1).then(function (data) {
            $scope.Ingreso.dis_serie = true;
            $scope.Ingreso.dis_campos = false;
            if (data.length > 0) {
                $scope.Ingreso.magnitud = $.trim(data[0].idmagnitud);
                $scope.Ingreso.accesorios = data[0].accesorio;
                $scope.Ingreso.ingreso = data[0].ingreso;
                $scope.Ingreso.equipo = $.trim(data[0].idequipo);
                $scope.Ingreso.marca = $.trim(data[0].idmarca);
                $scope.Ingreso.modelo = $.trim(data[0].idmodelo);
                $scope.Ingreso.intervalo = $.trim(data[0].idintervalo);
                $scope.Ingreso.intervalo2 = $.trim(data[0].idintervalo2);
                $scope.Ingreso.intervalo3 = $.trim(data[0].idintervalo3);
            }
        });
    }



    $scope.AgregarIntervalo = function (numero) {

        if ($scope.Ingreso.magnitud == numero)
            return false;

        var magnitud = $scope.Ingreso.magnitud.val() * 1;
        if (magnitud == 0) {
            $("#Magnitud").focus();
            swal("Acción Cancelada", "Debe de seleccionar una magnitud", "warning");
            return false;
        }

        $("#IDesde").val("");
        $("#IHasta").val("");
        $("#IMedida").val("").trigger("change");
        $("#NumIntervalo").val(numero);
        $("#modalIntervalo").modal("show");

    }





    $scope.AgregarOpcion = function (tipo, mensaje, id) {

        var magnitud = $scope.Ingreso.magnitud.val() * 1;
        var marca = $scope.Ingreso.marca.val() * 1

        if ($scope.Ingreso.magnitud.prop("disabled") == true)
            return false;

        switch (tipo) {

            case 1:
                if (magnitud == 0) {
                    $("#Magnitud").focus();
                    swal("Acción Cancelada", "Debe de seleccionar una magnitud", "warning");
                    return false;
                }
                break;
            case 3:
                if (marca == 0) {
                    $("#Marca").focus();
                    swal("Acción Cancelada", "Debe de seleccionar una marca", "warning");
                    return false;
                }
                break;
        }

        swal({
            title: 'Agregar Opción',
            text: mensaje,
            input: 'text',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Agregar'),
            cancelButtonText: ValidarTraduccion('Cancelar'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',

            inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                    if ($.trim(value)) {
                        var parametros = "opcion=GuardarOpcion&tipo=" + tipo + "&magnitud=" + $scope.Ingreso.magnitud + "&marca=" + $scope.Ingreso.marca + "&descripcion=" + $scope.Ingreso.observacion + "&tipoconcepto=0";
                        General.LlamarAjax("Cotizacion", parametros, true, "").then(function (response) {
                            var datos = $.trim(response).split("|");
                        });
                        if (datos[0] != "XX") {
                            $("#" + id).html(datos[1]);
                            $("#" + id).val(datos[0]).trigger("change");
                            resolve();
                        } else {
                            reject(datos[1]);
                        }


                    } else {
                        reject(ValidarTraduccion('Debe de ingresar la descripción de ' + mensaje));
                    }


                }).then(function (result) {
                    $("#" + id).focus();
                })

            }
        })
    }





    $scope.CambiarSerie = function () {

        if ($scope.Ingreso.serie = "")
            return false;

        swal({
            title: 'Cambio de serie',
            input: 'text',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Cambiar'),
            cancelButtonText: ValidarTraduccion('Cancelar'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',

            inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                    if (value) {
                        if ($.trim(value) == "")
                            reject(ValidarTraduccion('Debe de ingresar una serie'));
                        else {

                            var parametros = "opcion=CambioSerie&ingreso=" + $scope.Ingreso.ingreso + "&serie=" + $scope.Ingreso.serie.trim(value) + "&cliente=" + $scope.Registro.cliente;
                            General.LlamarAjax("Cotizacion", parametros, true, "").then(function (response) {
                                var datos = $.trim(response).split("|");
                                if (datos[0] == "0") {
                                    $scope.Ingreso.serie.val($.trim(value));
                                    resolve()
                                } else {
                                    reject("Esta serie se encuentra activa en el laboratorio");
                                }
                            });
                        }
                    } else {
                        reject(ValidarTraduccion('Debe de ingresar una serie'));
                    }


                });
            }
        });

        $(".swal2-input").val(serie);


    }


    $scope.CambioSede = function () {
        if ($scope.Registro.remision != 0) {
            var mensaje = "";
            var sede = $scope.Registro.sede.val() * 1;
            var contacto = $scope.Registro.contacto.val() * 1;

            if (contacto == 0) {
                $("#Contacto").focus();
                mensaje = "<br> Debe de seleccionar un contacto del cliente"
            }
            if (sede == 0) {
                $("#Sede").focus();
                mensaje = "<br> Debe de seleccionar una sede del cliente " + mensaje;
            }

            if (mensaje != "") {
                swal("Acción Cancelada", mensaje, "warning");
                return false;
            }

            var parametros = "opcion=CambioSede&id=" + $scope.Registro.remision + "&sede=" + $scope.Registro.sede + "&contacto=" + $scope.Registro.contacto;
            General.LlamarAjax("Cotizacion", parametros, true, "").then(function (response) {
                var datos = $.trim(response).split("|");

                if (datos[0] == "0") {
                    swal("", "Cambio de sede y contacto actualizado", "success");
                } else {
                    swal("Acción Cancelada", datos[1], "warning");
                }
            });
        }
    }


    $scope.CambiarCliente = function () {
        var mensaje = "";

        if ($scope.Registro.contacto == 0) {
            $("#CaContacto").focus();
            mensaje = "Debe seleccionar el contacto del cliente ";
        }

        if ($scope.Registro.sede == 0) {
            $("#CaDireccion").focus();
            mensaje = "Debe seleccionar la dirección del cliente <br>" + mensaje;
        }

        if ($scope.Registro.cliente == 0) {
            $("#CaCliente").focus();
            mensaje = "Debe seleccionar un cliente <br> " + mensaje;
        }

        if (mensaje != "") {
            swal("Acción Cancelada", mensaje, "warning");
            return false;
        }
        var parametros = "opcion=CambioSede&id=" + $scope.Ingreso.remision + "&sede=" + $scope.Registro.sede + "&contacto=" + $scope.Registro.contacto + "&cliente=" + $scope.Registro.cliente;
        General.LlamarAjax("Cotizacion", parametros, true, "").then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[0] == "0") {
                swal("", datos[1], "success");
                $("#modalCambioEmpresa").modal("hide");
                var numero = $scope.Remision;
                $scope.LimpiarTodo();
                $("#Remision").val(numero);
                $scope.BuscarRemision(numero);
            } else
                swal("Acción Cancelada", datos[1], "warning");
        });
    }


    $scope.LlamarImprimir = function (numero) {
        $scope.Remision = "";
        $scope.ImprimirRemision(numero, Estado, 0);
    }



    $scope.ImprimirRemision = function (remision, estado, descargar) {
        $scope.Registro.Pdf_Remision = "";
        if (estado == "Temporal") {
            swal("Acción Cancelada", "No se puede imprimir una remisión en estado temporal", "warning");
            return false;
        }
        if (remision * 1 != 0) {
            var parametros = "opcion=GenerarPDF&tipo=Remision&documento=" + remision;
            General.LlamarAjax("Configuracion", parametros, true, "", 1).then(function (response) {
                var datos = $.trim(response).split("||");
                if (datos[0] == "0") {
                    if (descargar == 0)
                        window.open($rootScope.Conexion.url_archivo + "DocumPDF/" + datos[1]);
                    else {
                        $scope.Registro.Pdf_Remision = datos[1];
                    }
                } else {
                    swal("Acción Cancelada", datos[1], "warning");
                }
            });
        }

    }



    $scope.CantidadEquipos = function () {

        if ($scope.Ingreso.equipo > 0 && ($scope.Ingreso.modelo > 0 || $scope.Ingreso.intervalo > 0)) {
            var parametros = "opcion=EquiposAgregados&cliente=" + $scope.Ingreso.cliente + "&id=" + $scope.Ingreso.remision + "&equipo=" + $scope.Ingreso.equipo + "&modelo=" + $scope.Ingreso.modelo + "&intervalo=" + $scope.Ingreso.intervalo;
            General.LlamarAjax("Cotizacion", parametros, true, "").then(function (response) {
                var datos = $.trim(response).split("|");
                $scope.Ingreso.TEquipos.val(datos);
            });
        }
    }

    $scope.CambioFecha = function (consulta, tipo) {
        switch (consulta) {
            case 1:
                switch (tipo) {
                    case 1:
                        $scope.Consulta.fechad.id = General.Obtener_Fecha($scope.Consulta.fechad.value);
                        break;
                    case 2:
                        $scope.Consulta.fechah.id = General.Obtener_Fecha($scope.Consulta.fechah.value);
                        break;
                }
                break;
            case 2:
            switch (tipo) {
                case 1:
                    $scope.ConsultaIngreso.fechad.id = General.Obtener_Fecha($scope.ConsultaIngreso.fechad.value);
                    break;
                case 2:
                    $scope.ConsultaIngreso.fechah.id = General.Obtener_Fecha($scope.ConsultaIngreso.fechah.value);
                    break;
            }
        }

    }


    $scope.ConsultarRemisiones = function () {
        if ($scope.Consulta.fechad.id == "" || $scope.Consulta.fechah.id == "") {
            swal("Acción Cancelada", ValidarTraduccion("Debe de ingresar fecha de inicio y fecha final"), "warning");
            return false;
        }
        var parametros = "opcion=ConsultarRemision&" + General.ConvertirParametros($scope.Consulta);
        General.LlamarAjax("Cotizacion", parametros, true, "", 1).then(function (response) {
            $rootScope.TablaRemisiones = new NgTableParams({count: 10}, {dataset: response});

        });
    }



    $scope.ConsultarIngresos = function () {
        if ($scope.Consulta.fechad.id == "" || $scope.Consulta.fechah.id == "") {
            swal("Acción Cancelada", ValidarTraduccion("Debe de ingresar fecha de inicio y fecha final"), "warning");
            return false;
        }
        var parametros = "opcion=ConsultarIngresoDet&" + General.ConvertirParametros($scope.ConsultaIngreso);
        General.LlamarAjax("Cotizacion", parametros, true, "", 1).then(function (response) {
            $rootScope.TablaIngresos = new NgTableParams({count: 10}, {dataset: response});

        });
    }







    $scope.VerRemision = function () {
        if ($scope.PDFRemision == 0)
            return false;
        window.open(url_archivo + "Adjunto/RemisionCliente/" + Remision + ".pdf?id=" + NumeroAleatorio());
    }

    $scope.EnviarIngreso = function () {

        if ($scope.Registro.remision == 0)
            return false;

        if ($scope.Registro.estado == "Temporal") {
            swal("Acción Cancelada", "No se puede enviar una remisión en estado temporal", "warning");
            return false;
        }

        if ($scope.Registro.correoempresa == "") {
            swal("Acción Cancelada", "Cliente no posee ningún correo principal registrado", "warning");
            return false;
        }

        $("#eCliente").val($("#NombreCliente").val());
        $("#eCorreo").val($("#Email").val());

        $scope.ImprimirRemision(Remision, Estado, 1);

        $("#spremision").html(Remision);

        $("#enviar_remision").modal("show");

    }



    $scope.EliminarIngreso = function (registro, index) {
        console.log(index);
        if ($scope.Registro.tabla.length == 1 && $scope.Registro.remision > 0) {
            swal("Acción Cancelada", "No se puede eliminar el único ingreso de la remisión", "warning");
            return false;
        }

        if ($scope.Registro.estado != "Temporal" && $scope.Registro.estado != "Ingresado") {
            swal("Acción Cancelada", "No se puede eliminar un ingreso con estado " + $scope.Registro.estado, "warning");
            return false;
        }

        swal.queue([{
                title: $rootScope.ValidarTraduccion('Advertencia'),
                text: $rootScope.ValidarTraduccion('¿Seguro que desea eliminar el ingreso número ' + registro.ingreso + ' del equipo ' + registro.equipo + '?'),
                type: 'question',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                confirmButtonText: ValidarTraduccion('Eliminar'),
                cancelButtonText: ValidarTraduccion('Cancelar'),
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        General.LlamarAjax("Cotizacion", "opcion=EliminarIngreso&id=" + registro.id + "&ingreso=" + registro.ingreso + "&equipo=" + registro.equipo + "&remision=" + registro.remision, false, "", 1).then(function (response) {
                            var datos = response.split("|");
                            datos = data.split("|");
                            if (datos[0] == "0") {
                                $scope.Registro.estado = "Temporal";
                                $scope.Registro.tabla.splice(index, 1);
                                resolve()
                                $scope.TablaTemporalIngreso();
                            } else {
                                reject(datos[1]);
                            }
                        })
                    })
                }
            }]);
    };



    $scope.AgregarAcceObser = function (tipo) {

        if (tipo == 1) {
            if ($.trim($scope.Ingreso.accesorio) != "")
                $scope.Ingreso.accesorio += "; ";
            $scope.Ingreso.accesorio += $.trim($scope.Ingreso.comaccesorio);
        } else {
            if ($.trim($scope.Ingreso.observacion) != "")
                $scope.Ingreso.observacion += "; ";
            $scope.Ingreso.observacion += $.trim($scope.Ingreso.comobservacion);
        }



    }



    $scope.CambiarFoto2 = function (numero, Ingreso) {
        $("#FotoIngreso2").attr("src", url_cliente + "Adjunto/imagenes/ingresos/" + Ingreso + "/" + numero + ".jpg?id=" + NumeroAleatorio());
    }





    $scope.CambioSedeContacto = function (cliente) {
        if (cliente * 1 == 0)
            return false;
        var parametros = "opcion=BuscarSedeCliente&cliente=" + cliente;
        General.LlamarAjax("Cotizacion", parametros, true, "", 0).then(function (response) {
            var datos = $.trim(response).split("|");
            $scope.Registro.contacto(datos[0]);
            $scope.Registro.sede(datos[1]);
        });
    }




    $scope.Contadores = function (tipo) {
        var parametros = "opcion=Contadores&tipo=2";
        General.LlamarAjax("Cotizacion", parametros, false, "", 0).then(function (response) {
            $scope.Ingreso.ingreso = $.trim(response);
        });
    }



    $scope.EliminarFoto = function () {

        ingreso = $("#FotoIngreso").html() * 1;
        var parametros = "opcion=EliminarFoto&ingreso=" + ingreso;
        General.LlamarAjax("Cotizacion", parametros, true, "", 0).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[1] != "X") {
                $scope.CargarFotos(ingreso, datos[1]);
                $scope.CambiarFoto2(datos[1], ingreso);

            }
        });
    }


    $scope.GuardarFotoIng = function (ingreso, archivo) {

        var parametros = "opcion=SubirFoto&archivo=" + archivo + "&ingreso=" + ingreso + "&tipo_imagen=2";
        General.LlamarAjax("Configuracion", parametros, true, "", 1).then(function (response) {
            var data = $.trim(response).split("|");
            if (data[0] == "0") {
                $rootScope.Modal.total = data[3] * 1;
                $rootScope.Modal.foto = data[3] * 1;

            } else {
                swal("Acción Cancelada", data[1], "success");
            }
        });
    }

    $scope.InicializarCamara = function (registro) {
        $rootScope.Modal.titulo = "Fotos del Ingreso número " + registro.ingreso;
        $rootScope.Modal.numero = registro.ingreso;
        General.LlamarAjax("Cotizacion", "opcion=CantidadFoto&ingreso=" + registro.ingreso, true, "", 0).then(function (response) {
            $rootScope.Modal.total = $.trim(response) * 1;
            $rootScope.Modal.aleatorio = General.NumeroAleatorio();
            $rootScope.Modal.foto = 1;
            $rootScope.Modal.Open("Camaras");
        });
    }



    $rootScope.TomarFoto = function () {
        shutter.play();
        Webcam.snap(function (data_uri) {
            //var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
            $scope.GuardarFotoIng($rootScope.Modal.numero, data_uri);
        });
    }

    $rootScope.EliminarFoto = function () {
        General.LlamarAjax("Cotizacion", "opcion=EliminarFoto&ingreso=" + $rootScope.Modal.numero, true, "", 0).then(function (response) {
            var datos = $.trim(response).split("|");
            if (datos[1] != "X") {
                $rootScope.Modal.total = datos[1] * 1;
                $rootScope.Modal.aleatorio = General.NumeroAleatorio();
                $rootScope.Modal.foto = datos[1] * 1;
            }
        });
    }

    $scope.VerTipoPrecio = function (magnitud) {
        var parametros = "opcion=TipoPrecio&magnitud=" + magnitud + "&servicio=0";
        General.LlamarAjax("Cotizacion", parametros, true, "", 0).then(function (response) {
            var datos = $.trim(response).split("|");
            TipoPrecio = datos * 1;

            switch (TipoPrecio) {
                case 1:
                    $scope.Ingreso.intervalo.attr("disabled", false);
                    break;
                case 2:
                    $scope.Ingreso.intervalo.attr("disabled", true);
                    $scope.Ingreso.intervalo.html("<option value='0'>VER ESPECIFICACIONES</option>");
                    break;
            }
        });
    }









    /*$scope.SelecccionarFila=function(ingreso) {
     
     ActivarLoad();
     setTimeout(function () {
     var parametros = "opcion=BuscarIngreso&ingreso=" + ingreso;
     General.LlamarAjax("Cotizacion",parametros,true,"",0).then(function (response) {
     var datos = $.trim(response).split("|");
     
     
     IdIngreso = data[0].id * 1;
     $("#Equipo").val(data[0].idequipo).trigger("change");
     $("#Marca").val(data[0].idmarca).trigger("change");
     $("#Modelo").val(data[0].idmodelo).trigger("change");
     $("#Intervalo").val(data[0].idintervalo).trigger("change");
     $("#Intervalo2").val(data[0].idintervalo2).trigger("change");
     $("#Intervalo3").val(data[0].idintervalo3).trigger("change");
     $("#Serie").val(data[0].serie);
     $("#Ingreso").val(ingreso);
     $("#Observacion").val(data[0].observacion);
     $("#Serie").focus();
     $("#spanagregar").html("Editar Equipo");
     $("#Accesorio").val(data[0].accesorio);
     $("#Sitio").val(data[0].sitio).trigger("change");
     $("#Garantia").val(data[0].garantia).trigger("change");
     $("#Convenio").val(data[0].convenio).trigger("change");
     
     $("#Serie").prop("disabled", true);
     $("#Magnitud").prop("disabled", false);
     $("#Equipo").prop("disabled", false);
     $("#Modelo").prop("disabled", false);
     $("#Marca").prop("disabled", false);
     $("#Intervalo").prop("disabled", false);
     $("#Intervalo2").prop("disabled", false);
     $("#Intervalo3").prop("disabled", false);
     $("#SelAccesorio").prop("disabled", false);
     $("#SelObservacion").prop("disabled", false);
     $("#Observacion").prop("disabled", false);
     $("#Cantidad").prop("disabled", false);
     $("#Accesorio").prop("disabled", false);
     $("#Sitio").prop("disabled", false);
     $("#Garantia").prop("disabled", false);
     $("#Convenio").prop("disabled", false);
     
     $("#ModalArticulos").modal({ backdrop: 'static', keyboard: false }, "show");
     DesactivarLoad();
     }
     });
     
     
     }
     */


    $scope.CargarCombos();
    $scope.InicializarRegistro();
    $scope.InicializarIngreso();
    $scope.InicializarPesa();
    $rootScope.InicializarFormulario(1);
    $scope.setActiveTab(1);
    $scope.setActiveSerTab(1);


});
