
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Pagina"})
public class Pagina extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String url_archivo = Globales.url_archivo;
    String archivoadjunto = null;
    String origen = "http://190.144.188.100:82/";

    //WILMER
    public String BuscarAcceso(HttpServletRequest request) {
        String correo = request.getParameter("correo");
        String clave = request.getParameter("clave");

        /* 
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Session["cliente"] = "0";
            Inicializar();
         */
        sql = "SELECT id, nombrecompleto, email, documento, clave"
                + "  from clientes "
                + "  where email = '" + correo.trim() + "' and clave = '" + clave.trim() + "'";
        datos = Globales.Obtenerdatos(sql, this.getClass() + "-->BuscarAcceso", 1);
        if (datos.next()) {
            Session("idcliente") = datos.getString("id");
            Session("idcontacto") = "0";
            Session["cliente"] = datos.getString("nombrecompleto");
            Session["documento"] = datos.getString("documento");
            Session["clave"] = datos.getString("clave");
            Session["tiposesion"] = "cliente";
            return "0|" + datos.getString("id") + "|" + datos.getString("nombrecompleto")
                    + "|" + datos.getString("documento") + "|" + datos.getString("clave")
                    + "|" + datos.getString("email") + "|0||1|1|1|1|1|1|1";
        } else {
            sql = "SELECT c.id, cc.id as idcontacto, nombrecompleto, cc.email, c.documento, clavecontac, nombres,"
                    + "  cotizacion, ingreso, estado_cuenta, solicitudes, certificado, inventario, queja"
                    + " from clientes c inner join clientes_contac cc on c.id = cc.idcliente"
                    + "                inner join seguridad.rbac_permiso_pagina p on p.idcontacto = cc.id"
                    + "where cc.email = '" + correo.trim() + "' and clavecontac = '" + clave.trim() + "'";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->BuscarAcceso", 1);
            if (datos.next()) {
                if (datos.Rows[0]["cotizacion"].ToString() == "0" && datos.Rows[0]["ingreso"].ToString() == "0" && datos.Rows[0]["estado_cuenta"].ToString() == "0"
                        && datos.Rows[0]["solicitudes"].ToString() == "0" && datos.Rows[0]["certificado"].ToString() == "0" && datos.Rows[0]["inventario"].ToString() == "0"
                        && datos.Rows[0]["queja"].ToString() == "0") {
                    return "1|Usted no posee permiso..., consulte con su asesor comercial";
                }
                Session["idcontacto"] = datos.Rows[0]["id"].ToString();
                Session["idcliente"] = datos.Rows[0]["idcontacto"].ToString();
                Session["cliente"] = datos.Rows[0]["nombrecompleto"].ToString();
                Session["documento"] = datos.Rows[0]["documento"].ToString();
                Session["clave"] = datos.Rows[0]["clavecontac"].ToString();
                Session["tiposesion"] = "contacto";
                return "0|" + datos.Rows[0]["id"].ToString() + "|" + datos.Rows[0]["nombrecompleto"].ToString()
                        + "|" + datos.Rows[0]["documento"].ToString() + "|" + datos.Rows[0]["clavecontac"].ToString()
                        + "|" + datos.Rows[0]["email"].ToString() + "|" + datos.Rows[0]["idcontacto"].ToString()
                        + "|" + datos.Rows[0]["nombres"].ToString() + "|" + datos.Rows[0]["cotizacion"].ToString() + "|" + datos.Rows[0]["ingreso"].ToString()
                        + "|" + datos.Rows[0]["estado_cuenta"].ToString() + "|" + datos.Rows[0]["solicitudes"].ToString() + "|" + datos.Rows[0]["certificado"].ToString()
                        + "|" + datos.Rows[0]["inventario"].ToString() + "|" + datos.Rows[0]["queja"].ToString();
            } else {
                return "1|Combinación de usuario y clave incorrecta";
            }
        }

    }

    public String CambioClave(HttpServletRequest request) {
        String cclavenueva = request.getParameter("cclavenueva");
        String cclave = request.getParameter("cclave");
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int contacto = Globales.Validarintnonull(request.getParameter("contacto"));
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        if (contacto == null) {
            contacto = 0;
        }
        try {
            if (!cclavenueva.equals(cclave)) {
                return "1|La clave nueva no es igual a la clave a confirmar";
            }

            if (cclave.equals("123456")) {
                return "1|La nueva clave no puede ser 123456";
            }

            if (cclave.trim().length < 6) {
                return "1|La nueva clave debe de tener mínimo 6 caracteres";
            }

            if (contacto == 0) {
                sql = "UPDATE clientes SET clave = '" + cclave + "' where id = " + cliente;
            } else {
                sql = "UPDATE clientes_contac SET clavecontac = '" + cclave + "' where id = " + contacto;
            }

            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->CambioClave", 1);
            return "0|";
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String Magnitudes() {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        sql = "SELECT id, descripcion from magnitudes order by 2";
        return Globales.ObtenerCombo(sql, 0, 0, 0);
    }

    public String Televisor(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int inicio = Globales.Validarintnonull(request.getParameter("inicio"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int recibido = Globales.Validarintnonull(request.getParameter("recibido"));

        sql = "select count(rd.id) "
                + "  from remision_detalle rd inner join equipo e on e.id = rd.idequipo"
                + "                          left join ingreso_plantilla ip on ip.ingreso = rd.ingreso"
                + " where  to_char(fechaing,'yyyy') >= '2019' and salida = 0 and garantia = 'NO' AND idcliente <> 11 " + (magnitud > 0 ? " and e.idmagnitud=" + magnitud : "") + (recibido == 0 ? " and recibidoing=0" : "");
        int cantidad = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (inicio > cantidad) {
            inicio = 0;
        }

        sql = "SELECT rd.ingreso, coalesce(to_char(fechaing,'yyyy/MM/dd HH24:MI'),'') as fecha, m.abreviatura as magnitud, habitual,"
                + "              coalesce(to_char(fechaaprocoti,'yyyy/MM/dd HH24:MI'),'') as fechaaprocoti, coalesce(to_char(fechaaproajus,'yyyy/MM/dd HH24:MI'),'') as fechaaproajus,"
                + "              tiempo_aprobacion(rd.ingreso, 0, ip.fechaaproajus, rd.fechaaprocoti, rd.fechaing, now(),3) as tiempoapro, coalesce(p.codigo,'--') as plantilla,"
                + "              ncotizacion, certificado, informetecnico, reportado, recibidocome, noautorizado, rd.informetercerizado, recibidolab, recibidoing, ncotizacionaprobada, certificados_externos, orden"
                + "  FROM remision_detalle rd inner join equipo e on e.id = rd.idequipo"
                + "                inner join clientes c on c.id = rd.idcliente"
                + "                inner join magnitudes m on m.id = e.idmagnitud"
                + "                left join ingreso_plantilla ip on ip.ingreso = rd.ingreso"
                + "            left join  plantillas p on p.id = ip.idplantilla"
                + "        where to_char(fechaing,'yyyy') >= '2019' and salida = 0 and garantia = 'NO' AND idcliente <> 11 " + (magnitud > 0 ? " and m.id=" + magnitud : "") + (recibido == 0 ? " and recibidoing=0" : "") + ""
                + "order by rd.ingreso"
                + "limit 25 offset " + (inicio) + ";";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->Televisor") + "||" + cantidad + "||" + inicio;

    }

    public String CambioClavePag(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */

        String ClaveC = request.getParameter("ClaveC");
        String ClaveN = request.getParameter("ClaveN");
        String ClaveA = request.getParameter("ClaveA");
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int contacto = Globales.Validarintnonull(request.getParameter("contacto"));
        try {
            String Clave = "";
            if (contacto == 0) {
                sql = "select clave from clientes where id = " + cliente;
            } else {
                sql = "select clavecontac from clientes_contac where id = " + contacto;
            }

            Clave = Globales.ObtenerUnValor(sql);

            if (!Clave.equals(ClaveA)) {
                return "1|La clave nueva no coincide con la clave actual";
            }

            if (!ClaveC.equals(ClaveN)) {
                return "1|La clave nueva no es igual a la clave a confirmar";
            }

            if (ClaveN.equals("123456")) {
                return "1|La nueva clave no puede ser 123456";
            }

            if (ClaveN.trim().length < 6) {
                return "1|La nueva clave debe de tener mínimo 6 caracteres";
            }

            if (contacto == 0) {
                sql = "UPDATE clientes SET clave = '" + ClaveN + "' where id = " + cliente;
            } else {
                sql = "UPDATE clientes_contac SET clavecontac = '" + ClaveN + "' where id = " + contacto;
            }

            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->CambioClavePag", 2);
            return "0|Clave actualizada con éxito";
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String RecordarClave(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        String correo = request.getParameter("correo");
        try {
            String[] copia = new String[1];
            copia[0] = "";
            sql = "SELECT nombrecompleto, clave FROM clientes WHERE email = '" + correo + "'";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->RecordarClave", 1);
            if (!datos.next()) {
                return "1|Este correo electrónico no se encuentra asignado a ningún cliente";
            }

            String clave = datos.getString(2);
            String Nombre = datos.getString(1);

            String correoempenv = "programador@calibrationservicesas.com";
            String clavecorreoempenv = "Calidad2018";

            String mensaje = "<b> Cliente:</b> <br> " + Nombre + "<br><br>   Reciba un cordial Saludo.<br><br>";
            mensaje += "<b>DATOS DE ACCESO</b><br><br> usuario: " + correo + "<br>Clave: " + clave;

            mensaje += "<br><br><br>Desarrollado por: Ing. Eurides Maparí";

            String envcorreo = Globales.EnviarEmail(correo, "CALIBRATION SERVICE SAS", copia, "Envío de Control de Acceso para el sistema de clientes de CALIBRATION SERVICE SAS ", mensaje, correo, "", correoempenv, clavecorreoempenv, "");

            if (!envcorreo.trim().equals("")) {
                return "1|" + envcorreo;
            }

            return "0|Clave enviada al correo electrónico " + correo;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    /*
        public String ValidarUsuarioWeb()
        {
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            if (Session["idcliente"] == null)
            {
                return "cerrada";
            }
            else
            {
                return "abierta";
            }
        }
     */
    public String MagnitudEquipoWeb(HttpServletRequest request) {

        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int id = Globales.Validarintnonull(request.getParameter("id"));

        sql = "SELECT idmagnitud FROM web.equipo WHERE id=" + id;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->MagnitudEquipoWeb");
    }

    public String GuardarOpcionWeb(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        String descripcion = request.getParameter("descripcion");
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));

        int id = 0;
        String resultado = "";
        switch (tipo) {
            case 1:

                descripcion = descripcion.trim().toUpperCase();
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.equipo WHERE descripcion = '" + descripcion + "' and idmagnitud = " + magnitud + " and idcliente=" + cliente));
                if (id > 0) {
                    return "XX|Descripcíon ya existe como equipo";
                }
                sql = "INSERT INTO web.equipo (idcliente, descripcion, idmagnitud) VALUES (" + cliente + ",'" + descripcion + "'," + magnitud + ")";
                if (Globales.DatosAuditoria(sql, "EQUIPO", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarOpcionWeb")) {
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.equipo WHERE descripcion = '" + descripcion + "' and idcliente=" + cliente));

                    resultado = id + "|";

                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

                break;
            case 2:
                descripcion = descripcion.trim().toUpperCase();
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.marca WHERE descripcion = '" + descripcion + "' and idcliente=" + cliente));
                if (id > 0) {
                    return "XX|Descripcíon ya existe como marca";
                }
                sql = "INSERT INTO web.marca (descripcion, idcliente) VALUES ('" + descripcion + "'," + cliente + ")";
                if (Globales.DatosAuditoria(sql, "MARCA", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarOpcionWeb")) {
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.marca WHERE descripcion = '" + descripcion + "'"));
                    resultado = id + "|";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
                break;
            case 3:
                descripcion = descripcion.trim().toUpperCase();
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.modelo WHERE descripcion = '" + descripcion + "' and idmarca = " + marca + " and idcliente=" + cliente));
                if (id > 0) {
                    return "XX|Descripcíon ya existe modelo";
                }
                sql = "INSERT INTO web.modelo (descripcion, idmarca, idcliente) VALUES ('" + descripcion + "'," + marca + "," + cliente + ")";
                if (Globales.DatosAuditoria(sql, "MODELO", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarOpcionWeb")) {
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.modelo WHERE descripcion = '" + descripcion + "' and idmarca = " + marca + " and idcliente=" + cliente));
                    resultado = id + "|";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
                break;
            case 4:
                descripcion = descripcion.trim();
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.ubicacion WHERE descripcion = '" + descripcion + "' and idcliente=" + cliente));
                if (id > 0) {
                    return "XX|Descripcíon ya existe como ubicación";
                }
                sql = "INSERT INTO web.ubicacion (descripcion, idcliente) VALUES ('" + descripcion + "'," + cliente + ")";
                if (Globales.DatosAuditoria(sql, "UBICACION", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarOpcionWeb")) {
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.ubicacion WHERE descripcion = '" + descripcion + "' and idcliente=" + cliente));
                    resultado = id + "|";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
                break;
            case 5:
                descripcion = descripcion.trim().toUpperCase();
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.responsable WHERE descripcion = '" + descripcion + "' and idcliente=" + cliente));
                if (id > 0) {
                    return "XX|Descripcíon ya existe como responsable";
                }
                sql = "INSERT INTO web.responsable (descripcion, idcliente) VALUES ('" + descripcion + "'," + cliente + ")";
                if (Globales.DatosAuditoria(sql, "RESPONSABLE", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarOpcionWeb")) {
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.responsable WHERE descripcion = '" + descripcion + "' and idcliente=" + cliente));
                    resultado = id + "|";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
                break;
        }
        return resultado;
    }

    public String ConsultarCotWeb(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */

        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        switch (tipo) {
            case 1:
                sql = "select row_number() OVER(order by c.id) as fila, c.cotizacion, cs.direccion as sede,  nombres as contacto, "
                        + " observacion, estado, subtotal, descuento, iva, total, to_char(fecha,'dd/MM/yyy HH24:MI') as fecha, estado, nombrecompleto as asesor,"
                        + "(select count(cd.id) FROM cotizacion_detalle cd WHERE cd.idcotizacion = c.id) as cantidad,"
                        + " '<button class=''btn btn-glow btn-success'' title=''Imprimir Cotización'' type=''button'' onclick=' || chr(34) || 'ImprimirCotizacion(' || c.cotizacion || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' || "
                        + " '<button class=''btn btn-glow btn-primary'' title=''Aprobar/Reporbar Cotización'' type=''button'' onclick=' || chr(34) || 'AprobarCotizacion(' || c.cotizacion || ')' ||chr(34) || '><span data-icon=''&#xe12f;''></span></button>' as opcion"
                        + " from cotizacion c INNER JOIN clientes_sede cs on c.idsede = cs.id"
                        + "		                  INNER JOIN clientes_contac cc on c.idcontacto = cc.id"
                        + "		                  INNER JOIN seguridad.rbac_usuario u on idusu = idusuario"
                        + "WHERE web = 1 and c.idcliente = " + cliente + " and c.estado = 'Cerrado'"
                        + "ORDER BY c.id";
                break;
            case 2:
                sql = "select row_number() OVER(order by c.id) as fila, c.cotizacion, cs.direccion as sede,  nombres as contacto, "
                        + "  ca.observacion, subtotal, descuento, iva, total, to_char(fechaaprobacion,'dd/MM/yyy HH24:MI')  as fecha, c.estado, nombrecompleto as asesor,"
                        + "(select count(cd.id) FROM cotizacion_detalle cd WHERE cd.idcotizacion = c.id) as cantidad,"
                        + "'<button class=''btn btn-glow btn-success'' title=''Imprimir Cotización'' type=''button'' onclick=' || chr(34) || 'ImprimirCotizacion(' || c.cotizacion || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as opcion"
                        + "from cotizacion c INNER JOIN clientes_sede cs on c.idsede = cs.id"
                        + "	                  INNER JOIN clientes_contac cc on c.idcontacto = cc.id"
                        + "	                  INNER JOIN seguridad.rbac_usuario u on idusu = idusuario"
                        + "      INNER JOIN cotizacion_aprobacion ca on ca.cotizacion = c.cotizacion"
                        + "WHERE web = 1 and c.idcliente = " + cliente + " and aprobada = 1 "
                        + "ORDER BY c.id";
                break;
            case 3:
                sql = "select row_number() OVER(order by c.id) as fila, c.cotizacion, cs.direccion as sede,  nombres as contacto, "
                        + "  ca.observacion, subtotal, descuento, iva, total, to_char(fecharechazada,'dd/MM/yyy HH24:MI') as fecha, c.estado, nombrecompleto as asesor,"
                        + "(select count(cd.id) FROM cotizacion_detalle cd WHERE cd.idcotizacion = c.id) as cantidad,"
                        + "'<button class=''btn btn-glow btn-success'' title=''Imprimir Cotización'' type=''button'' onclick=' || chr(34) || 'ImprimirCotizacion(' || c.cotizacion || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as opcion"
                        + " from cotizacion c INNER JOIN clientes_sede cs on c.idsede = cs.id"
                        + "		                  INNER JOIN clientes_contac cc on c.idcontacto = cc.id"
                        + "		                  INNER JOIN seguridad.rbac_usuario u on idusu = idusuario"
                        + "              INNER JOIN cotizacion_aprobacion ca on ca.cotizacion = c.cotizacion"
                        + "WHERE web = 1 and c.idcliente = " + cliente + " and c.rechazada = 1 "
                        + "ORDER BY c.id";
                break;
            case 4:
                sql = "select row_number() OVER(order by c.id) as fila, c.cotizacion, cs.direccion as sede,  nombres as contacto, "
                        + "  observacion, estado, subtotal, descuento, iva, total, to_char(fecha,'dd/MM/yyy HH24:MI') as fecha, estado, nombrecompleto as asesor,"
                        + "(select count(cd.id) FROM cotizacion_detalle cd WHERE cd.idcotizacion = c.id) as cantidad,"
                        + "'<button class=''btn btn-glow btn-success'' title=''Imprimir Cotización'' type=''button'' onclick=' || chr(34) || 'ImprimirCotizacion(' || c.cotizacion || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as opcion"
                        + "from cotizacion c INNER JOIN clientes_sede cs on c.idsede = cs.id"
                        + "		                  INNER JOIN clientes_contac cc on c.idcontacto = cc.id"
                        + "		                  INNER JOIN seguridad.rbac_usuario u on idusu = idusuario"
                        + "WHERE web = 1 and c.idcliente = " + cliente + "  "
                        + "ORDER BY c.id";
                break;
        }

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarCotWeb");
    }

    public String ConsultarRepWeb(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        String busqueda = " WHERE r.fotos > 0 and web = 1 and r.idcliente = " + cliente + " AND fechaenvio is not null ";
        switch (tipo) {
            case 1:
                busqueda += " AND entregado = 0 and fecaprobacion is null";
                break;
            case 2:
                busqueda += " AND tipoaprobacion <> 'GENERAR INFORME DE DEVOLUCION'";
                break;
            case 3:
                busqueda += " AND tipoaprobacion = 'GENERAR INFORME DE DEVOLUCION'";
                break;
        }

        sql = "select '<b>' || m.descripcion || '</b><br>' || e.descripcion as equipo, '<b>' || ma.descripcion || '</b><br>' || mo.descripcion  as marca, "
                + "'<b> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '</b><br>' || r.serie as serie, r.ingreso, "
                + "               to_char(ri.fecha, 'yyyy/MM/dd HH24:MI') as fecha, "
                + "               '<button class=''btn btn-glow btn-success'' title=''Aprobar Ajuste y/o Suministro'' type=''button'' onclick=' || chr(34) || 'AprobarReporte(' || r.ingreso || ',' || ri.id || ',1)' ||chr(34) || '><span data-icon=''&#xe2ac;''></span></button><br>' as opciones,"
                + "               case when r.fotos > 0 then '<img src=''" + origen + "imagenes/ingresos/' || r.ingreso || '/1.jpg'' onclick=''LlamarFotoWeb(' || r.ingreso || ',' || r.fotos || ')'' width=''80px''/><br>' else '' end as imagen,"
                + "               tiempo(ri.fecha, now(), 3) as tiempopendiente,"
                + "               tiempo(ri.fecha, ri.fecaprobacion, 3) as tiempoaprobado, observacionapro,"
                + "               case when coalesce(ajuste,'') <> '' then '<b>Ajuste:</b><br>' || ajuste || '<br>' else '' end || "
                + "               case when coalesce(suministro,'') <> '' then '<b>Suministro:</b><br>' || suministro else '' end  as detalle,"
                + "               to_char(fecaprobacion, 'yyyy/MM/dd HH24:MI') || '<br>' || coalesce(usuarioapro,'') || '<br>' || coalesce(cedula,'') || '<br>' || coalesce(cargo,'') as fechaapro, tipoaprobacion,"
                + "               case when fecaprobacion is null THEN 'POR APROBAR' ELSE tipoaprobacion END AS estado"
                + "   from remision_detalle r  inner join equipo e on r.idequipo = e.id"
                + "                        inner join magnitudes m on m.id = e.idmagnitud"
                + "                        inner join modelos mo on mo.id = r.idmodelo  "
                + "                        inner join marcas ma on ma.id = mo.idmarca "
                + "            inner join reporte_ingreso ri on ri.ingreso = r.ingreso"
                + "                    inner join remision re on re.id = r.idremision"
                + "                    inner join magnitud_intervalos i on i.id = r.idintervalo " + busqueda + " ORDER BY r.ingreso";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarRepWeb");
    }

    public String ConsultarIngWeb(HttpServletRequest request) {

        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            string busqueda = "";
         */
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        switch (tipo) {
            case 0:
                busqueda = " AND entregado = 0 ";
                break;
            case 1:
                busqueda = " AND entregado = 1 and recibidocli = 0  ";
                break;
            case 2:
                busqueda = " AND recibidocli = 1 ";
                break;
        }

        sql = "select '<b>' || m.descripcion || '</b><br>' || e.descripcion as equipo, '<b>' || ma.descripcion || '</b><br>' || mo.descripcion  as marca, "
                + "      '<b> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '</b><br>' || r.serie as serie, r.ingreso,  "
                + "                      to_char(re.fecha, 'yyyy/MM/dd HH24:MI') as fecha, (select descripcion from ingreso_estados ie where ie.ingreso = r.ingreso order by ie.id desc limit 1) as estado, "
                + "                      '<button class=''btn btn-glow btn-success'' title=''Detalle del Estado'' type=''button'' onclick=' || chr(34) || 'DetalleIngreso(' || r.ingreso || ',' || r.fotos || ')' ||chr(34) || '><span data-icon=''&#xe003;''></span></button><br>' || "
                + "                          case when r.entregado = 1 then '<button class=''btn btn-glow btn-primary'' title=''Recibir Ingreso'' type=''button'' onclick=' || chr(34) || 'RecibirIngreso(' || r.ingreso || ')' ||chr(34) || '><span data-icon=''&#xe0dd;''></span></button>' else '' end as opciones, "
                + "                      case when r.fotos > 0 then '<img src=''" + origen + "imagenes/ingresos/' || r.ingreso || '/1.jpg'' onclick=''LlamarFotoWeb(' || r.ingreso || ',' || r.fotos || ')'' width=''80px''/><br>' else '' end as imagen, "
                + "                      case when r.certificado = 1 then 'SI' ELSE 'NO' END AS calibrado, to_char(fechareci,'yyyy/MM/dd HH24:MI') as fecharecibido, "
                + "                      to_char((SELECT max(fechaentrega) from devolucion_detalle dd inner join devolucion d on d.id = dd.iddevolucion where dd.ingreso = r.ingreso),'yyyy/MM/dd HH24:MI') as fechaentrega, "
                + "                      tiempo(r.fechaing, (SELECT max(fechaentrega) from devolucion_detalle dd inner join devolucion d on d.id = dd.iddevolucion where dd.ingreso = r.ingreso),1) as tiempoentrega, "
                + "                      tiempo((SELECT max(fechaentrega) from devolucion_detalle dd inner join devolucion d on d.id = dd.iddevolucion where dd.ingreso = r.ingreso),fechareci,1) as tiemporecibi, "
                + "                      tiempo(r.fechaing, case when r.entregado = 0  then now() else (select max(fechaentrega) FROM devolucion_detalle dd inner join devolucion d on d.id = dd.iddevolucion where dd.ingreso = r.ingreso) end, 3) as tiempo "
                + "                      from remision_detalle r  inner join equipo e on r.idequipo = e.id "
                + "                                            inner join magnitudes m on m.id = e.idmagnitud "
                + "                                            inner join modelos mo on mo.id = r.idmodelo   "
                + "                                            inner join marcas ma on ma.id = mo.idmarca  "
                + "                                        inner join remision re on re.id = r.idremision "
                + "                                        inner join magnitud_intervalos i on i.id = r.idintervalo "
                + "               WHERE web = 1 and r.fotos > 0 and r.idcliente = " + cliente + busqueda + " ORDER BY r.ingreso";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarIngWeb");
    }

    public String ConsultarCerWeb(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        sql = "select '<b>' || m.descripcion || '</b><br>' || e.descripcion as equipo, '<b>' || ma.descripcion || '</b><br>' || mo.descripcion  as marca, "
                + "      '<b> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '</b><br>' || r.serie as serie, r.ingreso, "
                + "                     to_char(re.fecha, 'yyyy/MM/dd HH24:MI') as fecha, (select descripcion from ingreso_estados ie where ie.ingreso = r.ingreso order by ie.id desc limit 1) as estado,"
                + "                     '<button class=''btn btn-glow btn-success'' title=''Ver Certificado'' type=''button'' onclick=' || chr(34) || 'ImprimirCertificado(''' || case when c.impreso = 0 then c.item || '-' || c.numero else c.numero || ' ' || to_char(r.ingreso,'0000000') end || ''',' || recibidocli || ',' || r.ingreso || ')' ||chr(34) || '><span data-icon=''&#xe22d;''></span></button><br>' as opcion,"
                + "                     case when r.fotos > 0 then '<img src=''" + origen + "imagenes/ingresos/' || r.ingreso || '/1.jpg'' onclick=''LlamarFotoWeb(' || r.ingreso || ',' || r.fotos || ')'' width=''80px''/><br>' else '' end as imagen,"
                + "                     tiempo(r.fechaing, c.fecha, 3) as tiempo,"
                + "                     to_char(c.fecha, 'yyyy/MM/dd HH24:MI') as fechacer, c.numero"
                + "                     from remision_detalle r  inner join equipo e on r.idequipo = e.id"
                + "                                            inner join magnitudes m on m.id = e.idmagnitud"
                + "                                            inner join modelos mo on mo.id = r.idmodelo "
                + "                                            inner join marcas ma on ma.id = mo.idmarca "
                + "                                            inner join remision re on re.id = r.idremision"
                + "                                            inner join magnitud_intervalos i on i.id = r.idintervalo"
                + "                                inner join certificados c on c.ingreso = r.ingreso"
                + "                WHERE r.idcliente = " + cliente + " AND c.estado <> 'Anulado' and r.salida > 0 ORDER BY r.ingreso DESC";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarCerWeb");
    }

    public String DatosFirma(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        String certificado = request.getParameter("certificado");
        sql = "SELECT certificado, idusuario, to_char(fecha,'yyyy-MM-dd HH24:MI') AS fecha, "
                + "  to_char(vencimiento,'yyyy-MM-dd HH24:MI') AS vencimiento, u.nombrecompleto as usuario, u.documento, u.cargo "
                + "  FROM certificados_firmas f inner join seguridad.rbac_usuario u on f.idusuario = u.idusu "
                + "  where certificado = '" + certificado + "'";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DatosFirma");
    }

    public String BuscarCertificado(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */

        String Certificado = request.getParameter("Certificado");
        String Serial = request.getParameter("Serial");
        String Fecha = request.getParameter("Fecha");
        sql = "SELECT numero, item, impreso, to_char(c.ingreso,'0000000') as ingreso, to_char(fechaexp,'yyyy-MM-dd HH24:MI') AS fecha, idusuarioanula, to_char(fechaanula,'yyyy-MM-dd HH24:MI') AS fechaanula, observacionanula,"
                + "          (select sum(case when extract(days from(now() - vencimiento)) > diasbloqueo then saldo else 0 end)"
                + "         FROM factura f inner join clientes c on c.id = f.idcliente"
                + "         WHERE f.estado  <> 'Anulado' and f.saldo <> 0  and f.idcliente = rd.idcliente) as vencido"
                + " FROM certificados c inner join remision_detalle rd on rd.ingreso = c.ingreso"
                + " WHERE c.numero = '" + Certificado + "' and to_char(fechaexp,'yyyy-MM-dd') = '" + Fecha + "' and rd.serie = '" + Serial + "'";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarCertificado");
    }

    public String ConsultarIngActoWeb(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));

        sql = "select '<b>' || m.descripcion || '</b><br>' || e.descripcion as equipo, '<b>' || ma.descripcion || '</b><br>' || mo.descripcion  as marca, "
                + "          '<b> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '</b><br>' || r.serie as serie, "
                + "          r.ingreso, "
                + "          to_char(r.fechaing,'yyyy/MM/dd HH24:MI') as fechaing, to_char(ri.fecha,'yyyy/MM/dd HH24:MI') as fechainforme, "
                + "          (select descripcion from ingreso_estados ie where ie.ingreso = r.ingreso order by ie.id desc limit 1) as estado,"
                + "          case when r.fotos > 0 then '<img src=''" + origen + "imagenes/ingresos/' || r.ingreso || '/1.jpg'' onclick=''LlamarFotoWeb(' || r.ingreso || ',' || r.fotos || ')'' width=''80px''/><br>' else '' end as imagen,"
                + "          tiempo(r.fechaing, ri.fecha, 3) as tiempo,"
                + "          '<button class=''btn btn-glow btn-success'' title=''Imprimir Informe'' type=''button'' onclick=' || chr(34) || 'ImprimirInforme(' || r.ingreso || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as opcion,"
                + "          ri.conclusion, ri.observacion"
                + "  from remision_detalle r  inner join equipo e on r.idequipo = e.id"
                + "                        inner join magnitudes m on m.id = e.idmagnitud"
                + "                        inner join modelos mo on mo.id = r.idmodelo  "
                + "                        inner join marcas ma on ma.id = mo.idmarca"
                + "            inner join reporte_ingreso ri on ri.ingreso = r.ingreso"
                + "                    inner join remision re on re.id = r.idremision"
                + "                    inner join magnitud_intervalos i on i.id = r.idintervalo"
                + "WHERE r.fotos > 0 and r.idcliente = " + cliente + " and entregado = 0 and  ri.idconcepto = 3 ORDER BY r.ingreso desc";

        String sql2 = "select '<b>' || m.descripcion || '</b><br>' || e.descripcion as equipo, '<b>' || ma.descripcion || '</b><br>' || mo.descripcion  as marca, "
                + "   '<b> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '</b><br>' || r.serie as serie, "
                + "   r.ingreso, "
                + "   to_char(r.fechaing,'yyyy/MM/dd HH24:MI') as fechaing, "
                + "   (select descripcion from ingreso_estados ie where ie.ingreso = r.ingreso order by ie.id desc limit 1) as estado,"
                + "   case when r.fotos > 0 then '<img src=''" + origen + "imagenes/ingresos/' || r.ingreso || '/1.jpg'' onclick=''LlamarFotoWeb(' || r.ingreso || ',' || r.fotos || ')'' width=''80px''/><br>' else '' end as imagen,"
                + "   tiempo(r.fechaing, ri.fecha, 3) as tiempo"
                + "from remision_detalle r  inner join equipo e on r.idequipo = e.id"
                + "                        inner join magnitudes m on m.id = e.idmagnitud"
                + "                        inner join modelos mo on mo.id = r.idmodelo"
                + "                        inner join marcas ma on ma.id = mo.idmarca "
                + "            inner join reporte_ingreso ri on ri.ingreso = r.ingreso"
                + "                   inner join remision re on re.id = r.idremision"
                + "                  inner join magnitud_intervalos i on i.id = r.idintervalo"
                + "  WHERE r.fotos > 0 and r.idcliente = " + cliente + " and entregado = 0 and  ri.idconcepto in (1,2) and informetecnico = 0 ORDER BY r.ingreso desc";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarIngActoWeb") + "||" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->ConsultarIngActoWeb");
    }

    public String SaldoTotal(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        sql = "select sum(saldo) as actual,"
                + "         sum(case when extract(days from(now() - vencimiento)) >= diasbloqueo then saldo else 0 end) as vencido"
                + "  FROM factura f inner join clientes c on c.id = f.idcliente"
                + "  WHERE f.estado  <> 'Anulado' and f.saldo > 2000  and f.idcliente = " + cliente;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->SaldoTotal");
    }

    public String ConsultarFacturaWeb(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));

        switch (tipo) {
            case 1:
                sql = "select '<a href=''javascript:ImprirmirFactura(' || factura || ')'' title=''Ver Factura''>' || factura || '</a>' as factura, to_char(f.fecha,'yyyy/MM/dd') as fecha,"
                        + "      to_char(f.vencimiento,'yyyy/MM/dd') as vencimiento,"
                        + "      case when extract(days from (now()-vencimiento)) <= 0 then saldo else 0 end as ValorActual,"
                        + "      case when extract(days from (now()-vencimiento)) BETWEEN  1 and 30 then saldo else 0 end as Opcion1,'1-30 Días' as DesOpc1,"
                        + "      case when extract(days from (now()-vencimiento)) BETWEEN  31 and 60 then saldo else 0 end as Opcion2,'31-60 Días' as DesOpc2,"
                        + "      case when extract(days from (now()-vencimiento)) BETWEEN  61 and 90 then saldo else 0 end as Opcion3,'61-90 Días' as DesOpc3,"
                        + "      case when extract(days from (now()-vencimiento)) BETWEEN  91 and 120 then saldo else 0 end as Opcion4,'91-120 Días' as DesOpc4,"
                        + "      case when extract(days from (now()-vencimiento)) BETWEEN  121 and 150 then saldo else 0 end as Opcion5,'121-150 Días' as DesOpc5,"
                        + "      case when extract(days from (now()-vencimiento)) >  150 then saldo else 0 end as Opcion6"
                        + "from factura f where idcliente = " + cliente + " order by f.factura";
                break;
            case 2:
                sql = "select factura, to_char(f.fecha,'yyyy/MM/dd') as fecha,"
                        + "          to_char(f.vencimiento,'yyyy/MM/dd') as vencimiento,"
                        + "          case when extract(days from (now()-vencimiento)) < 0 then 0 else extract(days from (now()-vencimiento)) end as dias,"
                        + "          total, saldo, total - saldo as abonado"
                        + "  from factura f  WHERE idcliente = " + cliente + " ORDER BY factura";
                break;
            case 3:
                sql = "SELECT caja, to_char(rc.fecha, 'yyyy-MM-dd HH24:MI') as fecha, numerocontrol, case when tipomov = 'M' THEN 'TRANSFERENCIA' WHEN tipomov = 'E' THEN 'EFECTIVO' ELSE 'CHEQUE' END AS movimiento, recaudado, reteiva, "
                        + "      retecree, reteica, retefuente, rc.estado, efectivo + tarjeta + cheque + consignacion as abonado,"
                        + "      obtener_facturas(caja,1) as facturas, to_char(rp.fecha, 'yyyy-MM-dd') as fechapago,"
                        + "  case when idcuenta > 0 then "
                        + "      (SELECT b.descripcion || ' - ' || c.descripcion || ' ' || substring(c.cuenta,length(cuenta)-4,6) "
                        + "          FROM bancos_cuenta c inner join bancos b on c.idbanco = b.id WHERE c.id = rp.idcuenta) else"
                        + "      (SELECT b.descripcion FROM bancos b WHERE b.id = rp.idbanco) END AS cuenta"
                        + "  FROM recibo_caja rc inner join recibo_caja_pagos rp on rc.id = rp.idcaja"
                        + "  WHERE idcliente = " + cliente + " ORDER BY caja DESC";
                break;
            case 4:
                sql = "SELECT max(rp.id), contador, to_char(fechareg, 'yyyy-MM-dd HH24:MI') as fechareg,  to_char(fechapago, 'yyyy-MM-dd') as fechapago, obtener_facturas(contador,1) as facturas, "
                        + "      sum(monto) as monto, sum(reteiva) as reteiva, sum(reteica) as reteica, sum(retefuente) as retefuente, sum(consignacion) as consignacion, rp.estado,"
                        + "      b.descripcion || ' - ' || c.descripcion || ' ' || substring(c.cuenta,length(cuenta)-4,6) as cuenta, control,"
                        + "      '<button class=''btn btn-glow btn-success'' title=''Ver Pago'' type=''button'' onclick=' || chr(34) || 'VerPago(' || rp.contador || ')' ||chr(34) || '><span data-icon=''&#xe002;''></span></button>&nbsp;' ||"
                        + "      CASE WHEN rp.estado ='Pendiente' THEN '<button class=''btn btn-glow btn-danger'' title=''Anular Pago'' type=''button'' onclick=' || chr(34) || 'AnularPago(' || rp.contador || ')' ||chr(34) || '><span data-icon=''&#xe2a1;''></span></button>' ELSE '' END  as opciones"
                        + "  FROM web.reportarpago rp inner join bancos_cuenta c on c.id = rp.idcuenta"
                        + "                   inner join bancos b on c.idbanco = b.id"
                        + "  WHERE idcliente = " + cliente + " "
                        + "  group by contador, fechareg, fechapago, rp.estado, c.cuenta, b.descripcion, c.descripcion, control"
                        + "  ORDER BY 1";
                break;
        }
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarFacturaWeb");
    }

    public String EstadoIngreso(HttpServletRequest request) {
        /*
            Inicializar();

            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));

        sql = "SELECT id, ingreso, descripcion, to_char(fecha,'dd/MM/yyyy HH24:MI') as fecha, nombrecompleto as usuario, tiempo"
                + "          FROM public.ingreso_estados e inner join seguridad.rbac_usuario u on e.idusuario = u.idusu"
                + "          WHERE ingreso = " + ingreso + " order by id desc";
        String sql2 = "select r.ingreso, '<a title=''Imprimir Remisión'' type=''button'' href=' || chr(34) || 'javascript:ImprimirRemision(' || re.remision || ')' ||chr(34) || '>' || re.remision || '</a><br>' ||"
                + "              u.nombrecompleto || '<br><b>' || to_char(fechaing, 'yyyy/MM/dd HH24:MI') || '</b>' || case when re.pdf > 0 then '<br><a class=''text-success text-XX'' title=''Ver Remisión del Cliente'' href=''javascript:VerRemision(' || re.remision || ')''>REMISION DEL CLIENTE</a>' ELSE '' END as remision,"
                + "              case when re.importado = 1 and ncotizacion = 0 then coalesce((SELECT max(ip.descripcion) FROM importado_cotizacion ip where ip.ingreso = r.ingreso),'NO') else case when r.cotizado = 1 then '<a href=''javascript:ImprimirCotizacion(' || r.ncotizacion || ')'' title=''Ver Cotizaciones''>' || r.ncotizacion || (select '<br>' || co.estado from cotizacion co where cotizacion = r.ncotizacion) || '<br>Ver Detalle</a>' else  tiempo_aprobacion(r.ingreso, 0, r.fechaaproajus, r.fechaaprocoti, r.fechaing, now(),2) END END as cotizado,"
                + "              case when r.facturado> 0 then '<a href=''javascript:ImprirmirFactura(' || r.facturado || ')'' title=''Ver Factura''>' || r.facturado || '</a>' else 'NO' END  as facturado,"
                + "              case when r.entregado = 1 and r.salida = 1 then '<a href=''javascript:ImprimirDevolucion(' || (select max(devolucion) from devolucion d inner join devolucion_detalle dd on dd.iddevolucion = d.id and dd.ingreso = r.ingreso) || ')'' title=''Ver Ingrega''>SI<br>Ver Detalle</a>' else 'NO' END  as entregado,"
                + "              case when r.orden = 1 then '<a href=' || chr(34) || 'javascript:ImprimirOrden(''' || (select orco.numero from orden_compra orco WHERE orco.ingreso = r.ingreso order by orco.id desc limit 1) || ''')' || chr(34) || ' title=''Ver Orden de Compra''>' || (select orco.numero from orden_compra orco WHERE orco.ingreso = r.ingreso order by orco.id desc limit 1) || '</a>' else case when cotizado = 1 then tiempo_aprobacion(r.ingreso, 0, r.fechaaproajus, r.fechaaprocoti,(select MIN(fechareg) from cotizacion_detalle cd where cd.ingreso = r.ingreso), now(),2) ELSE 'NO <br>COTIZADO' END END  as orden"
                + "  from remision_detalle r  inner join equipo e on r.idequipo = e.id"
                + "                                  inner join magnitudes m on m.id = e.idmagnitud"
                + "                                  inner join modelos mo on mo.id = r.idmodelo  "
                + "                                  inner join marcas ma on ma.id = mo.idmarca "
                + "                                  inner join remision re on re.id = r.idremision"
                + "				                     inner join seguridad.rbac_usuario u on u.idusu = r.idusuario"
                + "                                 inner join magnitud_intervalos i on i.id = r.idintervalo"
                + "WHERE r.ingreso = " + ingreso;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->EstadoIngreso") + "||" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->EstadoIngreso");
    }

    public String RecibirIngreso(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String pregunta = request.getParameter("pregunta");
        String respuesta = request.getParameter("respuesta");
        String observacion = request.getParameter("observacion");

        try {
            sql = "SELECT recibidocli FROM remision_detalle WHERE ingreso = " + ingreso;
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (encontrado > 0) {
                return "1|Este ingreso ya fue recibido";
            }

            String[] a_respuesta = respuesta.split("|");
            String[] a_pregunta = pregunta.split("|");

            sql = "INSERT INTO web.encuesta_satisfaccion(ingreso, pregunta, respuesta)"
                    + " VALUES ";

            for (int x = 0; x < a_respuesta.length; x++) {
                if (x > 0) {
                    sql += ",";
                }
                sql += "(" + ingreso + ",'" + a_pregunta[x] + "'," + a_respuesta[x] + ")";
            }

            sql += ";UPDATE remision_detalle SET recibidocli = 1, observacionrecibido = '" + observacion + "', fechareci=now()  WHERE ingreso = " + ingreso + ";";
            sql += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, fecha, tiempo)"
                    + "  VALUES (" + ingreso + ",0,'INGRESO RECIBIDO POR EL CLIENTE',now(),tiempo((SELECT max(fechaentrega) from devolucion_detalle dd inner join devolucion d on d.id = dd.iddevolucion where dd.ingreso = " + ingreso + "),now(),1));";
            if (Globales.DatosAuditoria(sql, "INGRESO", "AGREGAR", idusuario, iplocal, this.getClass() + "-->RegistroRevision")) {
                return "0|";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String TablaRecibirIngreso(HttpServletRequest request) {
        /*
            
            Inicializar();

            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */

        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        sql = "SELECT ingreso, pregunta, to_char(fechaenc,'yyyy-MM-dd HH24:MI') as fecha,"
                + "          case when respuesta = 1 then '1 Muy malo' when respuesta = 2 then '2 Malo' when respuesta = 3 then '3 Regular' when respuesta = 4 then '4 Bueno' when respuesta = 5 then '5 Excelente' end as respuesta"
                + "  FROM web.encuesta_satisfaccion"
                + "  WHERE ingreso = " + ingreso + "  ORDER BY id";
        String sql2 = "SELECT observacionrecibido from remision_detalle where ingreso = " + ingreso;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaRecibirIngreso") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->TablaRecibirIngreso");
    }

    public String BuscarAjusteAprobar(HttpServletRequest request) {

        /*
            Inicializar();

            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int id = Globales.Validarintnonull(request.getParameter("id"));
        sql = "SELECT c.nombrecompleto as cliente, c.id as idcliente,"
                + "                              case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS intervalo, m.descripcion as magnitud,"
                + "                             mo.descripcion as modelo, ma.descripcion as marca, e.descripcion as equipo, rd.serie, ajuste, suministro, ri.usuarioapro, ri.cedula, to_char(ri.fecaprobacion,'yyyy-MM-dd HH24:MI') as fecha,"
                + "                             ri.observacionapro, rd.fotos, aprobado_rep, rd.ingreso, tipo_aprobacion, ri.cargo,"
                + "	                        case when cd.idcontacto is null then cc.nombres else (SELECT cc2.nombres from clientes_contac cc2 where cc2.id = cd.idcontacto) end as contacto"
                + "    FROM remision_detalle rd inner join remision r on r.id = rd.idremision"
                + "	                         inner join clientes_contac cc on cc.id = r.idcontacto"
                + "	                         inner join reporte_ingreso ri on ri.ingreso = rd.ingreso"
                + "	                         inner join clientes c on c.id = r.idcliente"
                + "	                         inner join equipo e on rd.idequipo = e.id"
                + "	                         inner join magnitudes m on m.id = e.idmagnitud"
                + "	                         inner join modelos mo on mo.id = rd.idmodelo"
                + "	                         inner join marcas ma on ma.id = mo.idmarca "
                + "	                         inner join magnitud_intervalos i on i.id = rd.idintervalo"
                + "	                         LEFT JOIN cotizacion_detalle cd on cd.ingreso = rd.ingreso and cd.estado in ('Cerrado','Aprobado','POR REEMPLAZAR')"
                + "where ri.id = " + id + " ORDER BY cd.id desc limit 1";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarAjusteAprobar");
    }

    /*
        public String ImprimirCotizacionWeb(HttpServletRequest request)
        {
            
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            
            int cotizacion= Globales.Validarintnonull(request.getParameter("cotizacion"));
            Session["codusu"] = "11";
            Session["urlprincipal"] = System.Configuration.ConfigurationManager.AppSettings["urlabsolita"];
            string ruta = System.Configuration.ConfigurationManager.AppSettings["urlabsolita"];
            return RpCotizacion(cotizacion, 1);
        }

     */
    public String AprobarCotizacionWeb(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */

        int cotizacion = Globales.Validarintnonull(request.getParameter("id"));
        String usuarios = request.getParameter("usuario");
        String cedula = request.getParameter("cedula");
        String cargo = request.getParameter("cargo");
        String observacion = request.getParameter("observacion");
        String pregunta = request.getParameter("pregunta");
        String respuesta = request.getParameter("respuesta");
        String operacion = request.getParameter("operacion");
        try {
            String mensaje = "";
            sql = "SELECT cotizacion FROM cotizacion WHERE cotizacion = " + cotizacion + " and (aprobada = 1 or rechazada = 1) ";
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (encontrado > 0) {
                return "1|Esta cotización ya fue aprobada/rechazada anteriormente";
            }

            String[] a_pregunta = pregunta.split("|");
            String[] a_respuesta = respuesta.split("|");

            String sql2 = "INSERT INTO public.cotizacion_aprobacion(cotizacion, usuario, cedula, cargo, estado, observacion)"
                    + "VALUES(" + cotizacion + ",'" + usuarios.trim().toUpperCase() + "','" + cedula.trim() + "','" + cargo.trim().toUpperCase() + "','" + operacion + "','" + observacion.trim() + "');";

            if (operacion.equals("Aprobado")) {
                for (int x = 0; x < a_pregunta.length; x++) {
                    sql2 += "INSERT INTO public.cotizacion_autorizacion(cotizacion, pregunta, respuesta)"
                            + "VALUES(" + cotizacion + ",'" + a_pregunta[x] + "','" + a_respuesta[x] + "');";
                }
                sql2 += "UPDATE cotizacion SET aprobada = 1, estado='Aprobado', fechaaprobacion = now()"
                        + " WHERE cotizacion = " + cotizacion + ";";

                sql2 += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, fecha, tiempo)"
                        + "SELECT distinct cd.ingreso, 0,'AUTORIZACIÓN DE AJUSTE',now(),tiempo(ri.fecha,now(),1) "
                        + "FROM reporte_ingreso ri INNER JOIN cotizacion_detalle cd on ri.ingreso = cd.ingreso "
                        + "                       inner join cotizacion co on cd.idcotizacion = co.id"
                        + "WHERE (coalesce(ri.usuarioapro,'') = '' or tipo_aprobacion = 3) and cd.idservicio = 6 and idconcepto = 2 and cd.idserviciodep > 0 and co.cotizacion = " + cotizacion + ";";

                sql2 += "UPDATE remision_detalle set fechaaproajus=now()"
                        + "from cotizacion_detalle cd INNER JOIN cotizacion co on co.id = cd.idcotizacion "
                        + "                           INNER JOIN reporte_ingreso ri on ri.ingreso = cd.ingreso"
                        + "WHERE fechaaproajus is null and  coalesce(ri.usuarioapro,'') = '' and remision_detalle.ingreso = cd.ingreso and cd.idservicio = 6 and idconcepto = 2 and cd.idserviciodep > 0 and co.cotizacion = " + cotizacion + ";";

                sql2 += "UPDATE reporte_ingreso SET archivo_aprueba='3|" + cotizacion + "', observacionapro='Aprobado por cotización web', usuarioapro='" + usuario + "', cedula='" + cedula + "',cargo='" + cargo + "'"
                        + ",fecaprobacion=now(),tipoaprobacion='APROBADO AJUSTE / SUMINISTRO',tipo_aprobacion=1"
                        + "from cotizacion_detalle cd INNER JOIN cotizacion co on co.id = cd.idcotizacion "
                        + "WHERE (coalesce(reporte_ingreso.usuarioapro,'') = '' or tipo_aprobacion = 3) and reporte_ingreso.ingreso = cd.ingreso and cd.idservicio = 6 and idconcepto = 2 and cd.idserviciodep > 0 and co.cotizacion = " + cotizacion + ";";

                sql2 += "UPDATE remision_detalle set fechaaprocoti=now()"
                        + "FROM cotizacion_detalle cd inner join cotizacion c on c.id = cd.idcotizacion "
                        + "WHERE remision_detalle.ingreso = cd.ingreso and fechaaprocoti is null and cotizacion = " + cotizacion;

                mensaje = "0|Cotización aprobada con éxtio";
            } else {
                mensaje = "0|Cotización rechazada con éxtio";
                sql2 += "UPDATE cotizacion SET rechazada = 1, estado='Rechazado', fecharechazada = now() "
                        + " WHERE cotizacion = " + cotizacion + ";";

                sql2 += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, fecha, tiempo)"
                        + "SELECT distinct cd.ingreso, 0,'RECHAZO DE AJUSTE',now(),tiempo(ri.fecha,now(),1) "
                        + "FROM reporte_ingreso ri INNER JOIN cotizacion_detalle cd on ri.ingreso = cd.ingreso "
                        + "                       inner join cotizacion co on cd.idcotizacion = co.id"
                        + "WHERE (coalesce(ri.usuarioapro,'') = '' or tipo_aprobacion = 3) and cd.idservicio = 6 and idconcepto = 2 and cd.idserviciodep > 0 and co.cotizacion = " + cotizacion + ";";

                sql2 += "UPDATE reporte_ingreso SET archivo_aprueba='3|" + cotizacion + "', "
                        + "observacionapro='Rechazo por cotización web', usuarioapro='" + usuario + "', cedula='" + cedula + "',cargo='" + cargo + "'" + ",fecaprobacion=now(),tipoaprobacion='Generar Informe de Devolución',tipo_aprobacion=3"
                        + " from cotizacion_detalle cd INNER JOIN cotizacion co on co.id = cd.idcotizacion "
                        + " WHERE (coalesce(reporte_ingreso.usuarioapro,'') = '' or tipo_aprobacion = 3) and reporte_ingreso.ingreso = cd.ingreso and cd.idservicio = 6 and idconcepto = 2 and cd.idserviciodep > 0 and co.cotizacion = " + cotizacion + ";";
            }

            Globales.Obtenerdatos(sql, this.getClass() + "-->AprobarCotizacionWeb", 2);
            return mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String AprobarReporte(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int opcion = Globales.Validarintnonull(request.getParameter("opcion"));
        String observacion = request.getParameter("observacion");
        String usuarios = request.getParameter("usuario");
        String cargo = request.getParameter("cargo");
        String cedula = request.getParameter("cedula");
        String correo = request.getParameter("correo");
        String claveacceso = request.getParameter("claveacceso");

        try {
            String resultado = "";
            if (String.equals("")
                
                (correo)
            )
                    correo = "";
            if (!correo.equals("")) {
                sql = "SELECT id"
                        + "  from clientes "
                        + " where email = '" + correo.trim() + "' and clave = '" + claveacceso.trim() + "'";
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AprobarReporte", 1);
                if (datos.Rows.Count > 0) {
                    resultado = "0|" + datos.getString("id");
                } else {
                    sql = "SELECT c.id"
                            + "  from clientes c inner join clientes_contac cc on c.id = cc.idcliente"
                            + "  where cc.email = '" + correo.trim() + "' and clavecontac = '" + claveacceso.trim() + "'";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AprobarReporte", 1);
                    if (datos.Rows.Count > 0) {
                        resultado = "0|" + datos.getString("id");
                    } else {
                        resultado = "1|0";
                    }
                }
            }

            String[] a_arreglo = resultado.split("|");

            if (a_arreglo[0].equals("1")) {
                return "1|conbinación de clave o usuario incorrecto";
            }

            int clienteajuste = Globales.Validarintnonull(a_arreglo[1]);

            sql = "SELECT idcliente"
                    + "  FROM remision_detalle rd inner join reporte_ingreso ri on ri.ingreso = rd.ingreso"
                    + "  where ri.id = " + id;
            int cliente = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

            if (cliente != clienteajuste) {
                return "1|este usuario no pertenece a este cliente";
            }

            sql = "SELECT id FROM reporte_ingreso WHERE id = " + id + " and fecaprobacion is not null";
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (encontrado > 0) {
                return "1|Esta ajuste o suministro ya fue aprobado/rechazado anteriormente";
            }

            String descripcion = "";
            switch (opcion) {
                case 1:
                    descripcion = "APROBADO AJUSTE / SUMINISTRO";
                    break;
                case 2:
                    descripcion = "REALIZAR CERTIFICADO ERROR";
                    break;
                case 3:
                    descripcion = "GENERAR INFORME DE DEVOLUCION";
                    break;
            }
            sql = "UPDATE reporte_ingreso SET tipoaprobacion = '" + descripcion + "', fecaprobacion = now(), observacionapro='" + observacion + "'"
                    + ", usuarioapro='" + usuario.trim().toUpperCase() + "', cedula='" + cedula + "', cargo = '" + cargo.trim().toUpperCase() + "', tipo_aprobacion=" + opcion + " WHERE id = " + id + ";";
            sql
                    += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, fecha, tiempo)"
                    + "    select ingreso, 0, "
                    + "    case when coalesce(ajuste,'') <> '' then '<b>Ajuste:</b><br>' || ajuste || '<br>' else '' end ||"
                    + "    case when coalesce(suministro,'') <> '' then '<b>Suministro:</b><br>' || suministro else '' end || ' " + descripcion + "',"
                    + "    now(), tiempo(fecha, now(),1)"
                    + "    from reporte_ingreso where id = " + id;
            if (Globales.DatosAuditoria(sql, "REPORTE", "AGREGAR", idusuario, iplocal, this.getClass() + "-->AprobarReporte")) {

                return "0|" + descripcion;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }


    /*
        public String GuardarFirma(HttpServletRequest request)
        {
            /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
     */
 /*     String firma=request.getParameter("firma");
             int cotizacion= Globales.Validarintnonull(request.getParameter("cotizacion"));
            try
            {
                
                String base64 = firma.Replace("data:image/png;base64,", "").Replace(" ", "+");
                if (base64.length % 4 > 0)
                    base64 = base64.PadRight(base64.length + 4 - base64.length % 4, '=');
                Image foto;
                byte[] imageBytes = Convert.FromBase64String(base64);

                using (var ms = new MemoryStream(imageBytes, 0, imageBytes.length))
                {
                    Image image = Image.FromStream(ms, true);
                    foto = image;
                }

                String path = HostingEnvironment.MapPath("~/imagenes/FirmaCliente");
                if (!Directory.Exists(path))
                {
                    DirectoryInfo di = Directory.CreateDirectory(path);
                }

                String ruta = HostingEnvironment.MapPath("~/imagenes/FirmaCliente/" + cotizacion + ".jpg");
                foto.Save(ruta);

                return "0";

            }
            catch (Exception ex)
            {
                return "1|" + ex.getMessage();
            }
        }
     */
    public String CargarCombosWeb(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */

        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int inicial = Globales.Validarintnonull(request.getParameter("inicial"));
        String opcion = request.getParameter("opcion");
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String combos = "";
        switch (tipo) {
            case 1:
                sql = "SELECT id, descripcion  FROM magnitudes where estado = 1 ORDER BY 2 ";
                break;
            case 2:
                sql = "SELECT e.id, e.descripcion FROM web.equipo e where idcliente=" + cliente + (opcion == "0" ? "" : " AND idmagnitud = " + opcion) + "  ORDER BY 2 ";
                break;
            case 3:
                sql = "SELECT id, descripcion from web.marca where idcliente=" + cliente + " ORDER BY 2";
                break;
            case 5:
                sql = "SELECT id, descripcion from web.modelo where idcliente=" + cliente + (opcion != "-1" ? " and idmarca = " + opcion : "") + " ORDER BY 2";
                break;
            case 6:
                sql = "sELECT id, '(' || desde || ' a ' || hasta || ') ' || medida FROM web.intervalo where idcliente = " + cliente + (opcion != "-1" ? " and idmagnitud = " + opcion : "") + " ORDER BY 2";
                break;
            case 7:
                sql = "SELECT id, descripcion from web.ubicacion where idcliente=" + cliente + " order by 2";
                break;
            case 8:
                sql = "select factura, factura "
                        + "FROM factura where cliente = " + opcion + " and saldo > 0 ORDER BY factura";
                break;
            case 9:
                sql = "SELECT c.id, b.descripcion || ' - ' || c.descripcion || ' ' || substring(c.cuenta,length(cuenta)-4,6)"
                        + "FROM bancos_cuenta c inner join bancos b on c.idbanco = b.id"
                        + "WHERE b.estado = 1 and c.estado = 1 ORDER BY 2";
                break;
            case 10:
                sql = "SELECT id, descripcion || ' (' || cargo || ')' from web.responsable where idcliente=" + cliente + " order by 2";
                break;
        }
        combos += Globales.ObtenerCombo(sql, inicial, 0, 0);
        return combos;
    }

    public String DatosClientesCompletar(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        sql = "select nombres from clientes_contac where idcliente = " + cliente + " ORDER BY 1";
        String sql2 = "select cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||'), ' || pa.descripcion as direccion"
                + "  from clientes_sede cs inner join ciudad ci on ci.id = cs.idciudad"
                + "                               inner join departamento d on d.id = ci.iddepartamento"
                + "                               inner join pais pa on pa.id = d.idpais "
                + " where idcliente = " + cliente + " ORDER BY 1";
        String sql3 = "select email from clientes_contac where idcliente = " + cliente + " ORDER BY 1";
        String sql4 = "select coalesce(telefonos,'')  || '/' || coalesce(fax,'') as telefono from clientes_contac where idcliente = " + cliente + " ORDER BY 1";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DatosClientesCompletar") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->DatosClientesCompletar") + "|"
                + Globales.ObtenerDatosJSon(sql3, this.getClass() + "-->DatosClientesCompletar") + "|" + Globales.ObtenerDatosJSon(sql4, this.getClass() + "-->DatosClientesCompletar");
    }

    public String AgregarItemCotizacion(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */

        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int Magnitud = Globales.Validarintnonull(request.getParameter("Magnitud"));
        int Intervalo = Globales.Validarintnonull(request.getParameter("Intervalo"));
        String Serie = request.getParameter("Serie");
        int Cantidad = Globales.Validarintnonull(request.getParameter("Cantidad"));
        String Acreditacion = request.getParameter("Acreditacion");
        String Sitio = request.getParameter("Sitio");
        String Express = request.getParameter("Express");
        String SubContratado = request.getParameter("SubContratado");
        String Punto = request.getParameter("Punto");
        String Metodo = request.getParameter("Metodo");
        String Nombre = request.getParameter("Nombre");
        String Direccion = request.getParameter("Direccion");
        String Declaracion = request.getParameter("Declaracion");
        String Observacion = request.getParameter("Observacion");
        String Proxima = request.getParameter("Proxima");
        String Certificado = request.getParameter("Certificado");
        String Equipo = request.getParameter("Equipo");
        String Marca = request.getParameter("Marca");
        String Modelo = request.getParameter("Modelo");

        try {

            if (String.equals("")
                
                (Proxima)
            )
                    Proxima = "null";
            else
                    Proxima = "'" + Proxima + "'";

            if (Id == 0) {
                sql = "SELECT id FROM web.solicitud_detalle WHERE solicitud = 0 and idmagnitud = " + Magnitud + " and equipo = '" + Equipo + "' and modelo = '" + Modelo + "' and marca = '" + Marca + "';";
                int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                if (encontrado > 0) {
                    return "1|Ya existe un instrumento agregado con estas especificaciones.<br> Si desea agregar otra cantidad edite el item de la cotización";
                }

                sql = "INSERT INTO web.solicitud_detalle(idcliente, idmagnitud, idintervalo, equipo, marca, modelo, acreditado, "
                        + "      sitio, express, subcontratado, punto, metodo, direccion, declaracion, certificado, proxima, cantidad, serie, "
                        + "      observacion, nombreca)"
                        + "VALUES (" + Cliente + "," + Magnitud + "," + Intervalo + ",'" + Equipo.toUpperCase() + "','" + Marca.toUpperCase() + "','" + Modelo.toUpperCase() + "','" + Acreditacion
                        + "','" + Sitio + "','" + Express + "','" + SubContratado + "','" + Punto + "','" + Metodo + "','" + Direccion + "','" + Declaracion
                        + "','" + Certificado + "'," + Proxima + "," + Cantidad + ",'" + Serie + "','" + Observacion + "','" + Nombre.toUpperCase() + "')";

                if (Globales.DatosAuditoria(sql, "INSTRUMENTO", "AGREGAR", idusuario, iplocal, this.getClass() + "-->AgregarItemCotizacion")) {
                    return "0|Instrumento agregado a la solicitud cotización";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                sql = "SELECT id FROM web.solicitud_detalle WHERE solicitud = 0 and id <> " + Id + " and idmagnitud = " + Magnitud + " and equipo = '" + Equipo + "' and modelo = '" + Modelo + "' and marca = '" + Marca + "';";
                int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                if (encontrado > 0) {
                    return "1|Ya existe un instrumento agregado con estas especificaciones.<br> Si desea agregar otra cantidad edite el item de la cotización";
                }

                sql = "UPDATE web.solicitud_detalle"
                        + "      SET idmagnitud=" + Magnitud + ",equipo='" + Equipo.toUpperCase() + "', marca='" + Marca.toUpperCase() + "',modelo='" + Modelo.toUpperCase() + "',"
                        + "idintervalo=" + Intervalo + ", acreditado='" + Acreditacion + "',sitio='" + Sitio + "',express='" + Express + "',"
                        + "subcontratado='" + SubContratado + "',punto='" + Punto + "',metodo='" + Metodo + "',direccion='" + Direccion + "',"
                        + "declaracion='" + Declaracion + "',certificado='" + Certificado + "',proxima=" + Proxima + ",cantidad=" + Cantidad + ","
                        + "serie='" + Serie + "',observacion='" + Observacion + "',nombreca='" + Nombre.toUpperCase() + "' WHERE id = " + Id;
                if (Globales.DatosAuditoria(sql, "INSTRUMENTO", "ACTUALIZAR", idusuario, iplocal, this.getClass() + "-->AgregarItemCotizacion")) {
                    return "0|Instrumento actualizado a la solicitud cotización";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarItemSolicitud(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int id = Globales.Validarintnonull(request.getParameter("id"));

        try {
            sql = "DELETE FROM web.solicitud_detalle WHERE id = " + id;

            if (Globales.DatosAuditoria(sql, "ITEM", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarItemSolicitud")) {

                return "0|Item eliminado";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    /*
        private static ImageCodecInfo GetEncoderInfoWeb(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        private byte[] StreamFileWeb(String filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

       
        
        
        public String AdjuntarFotoWeb(HttpPostedFileBase documento)
        {
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            String respuesta = "9|Error al Subir el Archivo ";
            if (documento != null)
            {
                String adjunto = "foto" + DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(documento.FileName);
                documento.SaveAs(HostingEnvironment.MapPath("~/uploads/" + adjunto));
                respuesta = "0|" + HostingEnvironment.MapPath("~/uploads/" + adjunto);
            }
            return respuesta;
        }

        private String SubirFotoWeb(HttpServletRequest request)
        {
        int contador= Globales.Validarintnonull(request.getParameter("contador"));
        String foto= request.getParameter("foto");
            /*Inicializar();*/

 /*     Image fotonew;

            int numero = Globales.Validarintnonull(Globales.ObtenerUnValor(sql)) + 1;

            byte[] bytes = StreamFileWeb(foto);

            int width = 680;
            var height = 0; //succeeds at 65499, 65500

            using (var ms = new MemoryStream(bytes, 0, bytes.length))
            {
                Image Imagen = Image.FromStream(ms, true);
                fotonew = Globales.CambiarTamanoImagen(Imagen, width, height);
            }

            if (fotonew == null)
                return "1|Error al guardar la foto";

            ImageCodecInfo myImageCodecInfo;
            Encoder myEncoder;
            EncoderParameter myEncoderParameter;
            EncoderParameters myEncoderParameters;

            // Create a Bitmap object based on a BMP file.

            // Get an ImageCodecInfo object that represents the JPEG codec.
            myImageCodecInfo = GetEncoderInfoWeb("image/jpeg");

            // Create an Encoder object based on the GUID

            // for the Quality parameter category.
            myEncoder = Encoder.Quality;

            //foto = Globales.RecortarImagen(foto, x1, x2, y1, y2, w, h);
            String ruta = HostingEnvironment.MapPath("~/imagenes/pagoclientes/" + contador + ".jpg");

            myEncoderParameters = new EncoderParameters(1);

            // Save the bitmap as a JPEG file with quality level 25.
            myEncoderParameter = new EncoderParameter(myEncoder, 100L);
            myEncoderParameters.Param[0] = myEncoderParameter;
            fotonew.Save(ruta, myImageCodecInfo, myEncoderParameters);

            return "0";


        }
        
     */
    public double MontoFactura(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");*/
        int factura = Globales.Validarintnonull(request.getParameter("factura"));
        sql = "select saldo - coalesce((SELECT monto from web.reportarpago rp where rp.factura = f.factura and rp.estado = 'Pendiente'),0) FROM factura f WHERE factura = " + factura;
        return Globales.ValidardoublenoNull(Globales.ObtenerUnValor(sql));
    }

    public String EliminarSolicitud(HttpServletRequest request) {
        /*Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");*/
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        try {
            sql = "DELETE FROM web.solicitud_detalle WHERE solicitud = 0 and idcliente = " + cliente;

            if (Globales.DatosAuditoria(sql, "ITEM", "ELIMINAR", idusuario, iplocal, this.getClass() + "--> MontoFactura")) {

                return "0|Todos los items de la solicitud de cotización fueron eliminados";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1" + ex.getMessage();
        }
    }

    public String GenerarSolicitud(HttpServletRequest request) {
        /*Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int Cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String MDirecion = (request.getParameter("MDirecion"));
        String MContacto = (request.getParameter("MContacto"));
        String MTelefono = (request.getParameter("MTelefono"));
        String MRDirecion = (request.getParameter("MRDirecion"));
        String MRContacto = (request.getParameter("MRContacto"));
        String MRTelefono = (request.getParameter("MRTelefono"));
        String MRCorreo = (request.getParameter("MRCorreo"));
        String MEDirecion = (request.getParameter("MEDirecion"));
        String MEContacto = (request.getParameter("MEContacto"));
        String METelefono = (request.getParameter("METelefono"));
        String MECorreo = (request.getParameter("MECorreo"));
        String Observacion = (request.getParameter("Observacion"));
        if (String.equals("")
            
            (MRDirecion)
        ) MRDirecion = "";
        if (String.equals("")
            
            (MRContacto)
        ) MRContacto = "";
        if (String.equals("")
            
            (MRTelefono)
        ) MRTelefono = "";
        if (String.equals("")
            
            (MRCorreo)
        ) MRCorreo = "";

        if (String.equals("")
            
            (MEDirecion)
        ) MEDirecion = "";
        if (String.equals("")
            
            (MEContacto)
        ) MEContacto = "";
        if (String.Iequals("")
            
            (METelefono)
        ) METelefono = "";
        if (String.equals("")
            
            (MECorreo)
        ) MECorreo = "";

        try {
            sql = "select solicitud FROM contadores";
            int contador = Globales.Validarintnonull(Globales.ObtenerUnValor(sql)) + 1;
            sql = "INSERT INTO web.solicitud(solicitud, idcliente, direccion, contacto, telefonos, "
                    + "                            rdireccion, rcontacto, rtelefonos, rcorreo, "
                    + "                            edireccion, econtacto, etelefonos, ecorreo, observacion)"
                    + "  VALUES(" + contador + "," + Cliente + ",'" + MDirecion + "','" + MContacto.toUpperCase() + "','" + MTelefono + "'"
                    + ",'" + MRDirecion + "','" + MRContacto.toUpperCase() + "','" + MRTelefono + "','" + MRCorreo.toLowerCase() + "'"
                    + ",'" + MEDirecion + "','" + MEContacto.toUpperCase() + "','" + METelefono + "','" + MECorreo.toLowerCase() + "','" + Observacion + "');";
            sql += "UPDATE web.solicitud_detalle SET solicitud = " + contador + " WHERE solicitud = 0 and idcliente = " + Cliente + ";";
            sql += "UPDATE contadores SET solicitud = " + contador + ";";

            if (Globales.DatosAuditoria(sql, "SOLICITUD", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GenerarSolicitud")) {

                return "0|Solicitud generada con el número |" + contador;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1" + ex.getMessage();
        }
    }

    public String TablaSolicitudDetalle(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int id = Globales.Validarintnonull(request.getParameter("id"));

        if (id == 0) {
            sql = "SELECT cd.id, cd.idmagnitud, cd.idintervalo, "
                    + "              '<b>' || m.descripcion || '</b><br>' || equipo || '<br><b>' || marca || '</b><br>' || modelo || '<br><b>' || case when cd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '</b><br>' || serie as equipo,"
                    + "              '<b>Acreditado:</b> ' || acreditado || '<br><b>Sitio:</b> ' || sitio || '<br><b>Express:</b> ' || express || '<br><b>SubContratado:</b> ' || subcontratado as servicio, "
                    + "              '<b>'||punto || '</b></br>' || metodo as metodo, "
                    + "              '<b>'||nombreca || '</b></br>' || direccion as direccion, "
                    + "              '<b>'||coalesce(proxima,'') || '</b></br>' || certificado as certificado,"
                    + "             declaracion, cantidad, observacion,"
                    + "          '<button class=''btn btn-glow btn-danger'' title=''Eliminar Item'' type=''button'' onclick=' || chr(34) || 'EliminarItem(' || cd.id || ',''' || cd.equipo || ' ' || cd.marca || ' ' || modelo || ''')' ||chr(34) || '><span data-icon=''&#xe0d7;''></span></button>' as opcion"
                    + "  FROM web.solicitud_detalle cd inner join magnitudes m on m.id = cd.idmagnitud"
                    + "                                           inner join magnitud_intervalos i on i.id = cd.idintervalo"
                    + "WHERE idcliente = " + cliente + " and solicitud = 0  ORDER BY cd.id";
        } else {
            sql = "SELECT id, idcliente, solicitud, idmagnitud, equipo, marca, modelo, "
                    + "      idintervalo, acreditado, sitio, express, subcontratado, punto, "
                    + "      metodo, direccion, declaracion, certificado, coalesce(proxima,'') as proxima, cantidad, "
                    + " serie, observacion, nombreca from web.solicitud_detalle WHERE id = " + id;
        }
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaSolicitudDetalle");
    }

    public String AnularSolicitud(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int solicitud = Globales.Validarintnonull(request.getParameter("solicitud"));
        String observacion = (request.getParameter("observacion"));

        sql = "SELECT estado FROM web.solicitud WHERE solicitud = " + solicitud;
        String estado = Globales.ObtenerUnValor(sql);
        if (!estado.equals("Pendiente")) {
            return "1|No se puede anular una solicitud en estado " + estado;
        }

        sql = "UPDATE web.solicitud SET estado='Anulado', fechaanulado = now(), observacionanula = '" + observacion + "' WHERE solicitud = " + solicitud;

        if (Globales.DatosAuditoria(sql, "SOLICITUD", "ANULAR", idusuario, iplocal, this.getClass() + "-->AnularSolicitud")) {

            return "0|Solicitud anulada";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }

    }

    public String TablaSolicitud(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String estado = (request.getParameter("estado"));

        sql = "SELECT solicitud, to_char(fecha,'yyyy/MM/dd HH24:MI') as fecha, s.direccion as direccioncli, contacto, telefonos, c.email,"
                + "     s.observacion, c.nombrecompleto || ' (' || c.documento || ')'  as cliente, sum(sd.cantidad) as cantidad,"
                + "     '<b>' || to_char(fechaanulado,'yyyy/MM/dd HH24:MI')  || '</b><br>' || observacionanula as anulado, '<b>' || cotizacion  || '</b><br>' || to_char(fechacotizado,'yyyy/MM/dd HH24:MI') as cotizado,"
                + "     u.nombrecompleto as asesor, s.estado,"
                + "      '<button class=''btn btn-glow btn-success'' title=''Imprimir Solicitud'' type=''button'' onclick=' || chr(34) || 'ImprimirSolicitud(' || s.solicitud || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' || "
                + "      case when s.estado = 'Pendiente' THEN '&nbsp;<button class=''btn btn-glow btn-danger'' title=''Aprobar/Reporbar Cotización'' type=''button'' onclick=' || chr(34) || 'AnularSolicitud(' || s.solicitud || ')' ||chr(34) || '><span data-icon=''&#xe2a1;''></span></button>' ELSE '' END as opcion"
                + "FROM web.solicitud s INNER JOIN web.solicitud_detalle sd using (solicitud)"
                + "                            INNER JOIN clientes c on c.id = s.idcliente"
                + "                    INNER JOIN seguridad.rbac_usuario u on u.idusu = s.asesor"
                + " WHERE s.idcliente = " + cliente + (estado != "" ? " and s.estado = '" + estado + "'" : "") + "  "
                + " GROUP BY solicitud, fecha, s.direccion, contacto, telefonos, c.email,"
                + "     s.observacion, c.nombrecompleto, c.documento,"
                + "     fechaanulado,observacionanula, s.estado,cotizacion, fechacotizado,u.nombrecompleto"
                + "order by solicitud DESC";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "--> TablaSolicitud");
    }

    public String DetalleRepPago(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */

        int Contador = Globales.Validarintnonull(request.getParameter("Contador"));
        sql = "SELECT contador, to_char(fechareg, 'yyyy-MM-dd HH24:MI') as fechareg,  to_char(fechapago, 'yyyy-MM-dd') as fechapago, factura, "
                + "              monto, reteiva, reteica, retefuente, consignacion"
                + "          FROM web.reportarpago rp inner join bancos_cuenta c on c.id = rp.idcuenta"
                + "                           inner join bancos b on c.idbanco = b.id"
                + "          WHERE contador = " + Contador + " ORDER BY rp.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "--> DetalleRepPago");
    }

    public String VerReportePago(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int Contador = Globales.Validarintnonull(request.getParameter("Contador"));
        sql = "SELECT contador, to_char(fechareg, 'yyyy-MM-dd HH24:MI') as fechareg,  to_char(fechapago, 'yyyy-MM-dd') as fechapago, factura, "
                + "              monto, reteiva, reteica, retefuente, consignacion, rp.estado,"
                + "              b.descripcion || ' - ' || c.descripcion || ' ' || substring(c.cuenta,length(cuenta)-4,6) as cuenta, control,"
                + "              to_char(fechaanulado, 'yyyy-MM-dd HH24:MI') as fechaanulado, observacionanula"
                + "          FROM web.reportarpago rp inner join bancos_cuenta c on c.id = rp.idcuenta"
                + "                           inner join bancos b on c.idbanco = b.id"
                + "          WHERE contador = " + Contador + " "
                + "          ORDER BY rp.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->VerReportePago");
    }

    public String AnularReportePago(HttpServletRequest request) {
        /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */

        int Contador = Globales.Validarintnonull(request.getParameter("Contador"));
        String Observacion = (request.getParameter("Observacion"));
        sql = "SELECT estado FROM web.reportarpago WHERE contador = " + Contador;
        String estado = Globales.ObtenerUnValor(sql);

        if (!estado.equals("Pendiente")) {
            return "1|No se puede anular un pago en estado " + estado;
        }

        sql = "UPDATE web.reportarpago SET fechaanulado = now(), estado='Anulado', observacionanula = '" + Observacion + "'  "
                + "  WHERE contador = " + Contador;
        if (Globales.DatosAuditoria(sql, "REPORTE PAGO", "ANULAR", idusuario, iplocal, this.getClass() + "-->AnularReportePago")) {
            return "0|";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }

    }

    public String ReportarPago(HttpServletRequest request) {
        /*
           Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
         */
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        int NCuenta = Globales.Validarintnonull(request.getParameter("NCuenta"));
        String NFechaPago = (request.getParameter("NFechaPago"));
        String NControl = (request.getParameter("NControl"));
        int[] Factura
        double[] Monto
        double[] Reteiva
        double[] Reteica
        double[] Retefuente
        double[] Consignar
        String ArchivoFoto = (request.getParameter("ArchivoFoto"));

        String textosql = "INSERT INTO web.reportarpago(contador, idcliente, idcuenta, control,  fechapago, factura,"
                + "monto, reteiva, reteica, retefuente, consignacion) VALUES ";
        try {
            sql = "SELECT pagos FROM contadores";
            int contador = Globales.Validarintnonull(Globales.ObtenerUnValor(sql)) + 1;
            for (int x = 0; x < Monto.length; x++) {
                sql = "SELECT saldo FROM factura WHERE saldo <= 0 and factura = " + Factura[x];
                int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                if (encontrado > 0) {
                    return "1|La factura número " + Factura[x] + " ya fue pagada";
                }
                if (x > 0) {
                    textosql += ",";
                }
                textosql += "(" + contador + "," + Cliente + "," + NCuenta + ",'" + NControl + "','" + NFechaPago + "',"
                        + Factura[x] + "," + Monto[x] + "," + Reteiva[x] + "," + Reteica[x] + "," + Retefuente[x] + "," + Consignar[x] + ");";
            }
            textosql += "UPDATE contadores SET pagos = " + contador;

            SubirFotoWeb(contador, ArchivoFoto);

            Globales.Obtenerdatos(textosql);

            System.IO.File.Delete(ArchivoFoto);
            return "0|Reporte de pago guardado con el número " + contador;

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    /*
        public String RpSolicitud(int solicitud)
        {
            /*
            Inicializar();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
     */
 /*      
            String reporte = "ImpSolicitud";

            sql = "SELECT idusuario FROM web.solicitud where solicitud = " + solicitud;
            int usuario = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

            if (usuario > 0)
                sql = "SELECT solicitud, fecha, to_char(fecha,'yyyy-MM-dd') as fechasoli, cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||'), ' || pa.descripcion as direccioncli, cc.nombres as contacto, cc.telefonos, "+
                      "             rdireccion, rcontacto, rtelefonos, rcorreo, edireccion, econtacto, s.observacion as observacionsol,"+
                      "             etelefonos, ecorreo, cotizacion, fechacotizado, "+
                      "             e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, "+
                      "             acreditado, sitio, express, subcontratado, punto, cc.email,"+
                      "             metodo, sd.direccion, declaracion, certificado, proxima, cantidad, "+
                      "             serie, sd.observacion, nombreca, c.nombrecompleto as cliente, c.documento, "+
                      "             case when sd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end as medida"+
                      "        FROM web.solicitud s INNER JOIN web.solicitud_detalle sd using (solicitud)"+
		       "                            INNER JOIN clientes c on c.id = s.idcliente"+
                       "                    INNER JOIN clientes_sede cs on s.idsede = cs.id"+
		       "		                           INNER JOIN clientes_contac cc on s.idcontacto = cc.id"+
                       "                    inner join equipo e on sd.idequipo = e.id"+
			"			                   inner join modelos mo on mo.id = sd.idmodelo"+  
			"			                   inner join marcas ma on ma.id = mo.idmarca"+
			"	                           inner join ciudad ci on ci.id = cs.idciudad"+
			"	                           inner join departamento d on d.id = ci.iddepartamento"+
			"	                           inner join pais pa on pa.id = d.idpais"+
		         "                          inner join magnitudes m on m.id = sd.idmagnitud"+
		        "                           inner join magnitud_intervalos i on i.id = sd.idintervalo"+
                        "        where s.solicitud = " + solicitud + " "+
                         "        ORDER BY sd.id";
            else
                sql = "SELECT solicitud, fecha, to_char(fecha,'yyyy-MM-dd') as fechasoli, s.direccion as direccioncli, contacto, telefonos, "+
                      "         rdireccion, rcontacto, rtelefonos, rcorreo, edireccion, econtacto, s.observacion as observacionsol,"+
                      "         etelefonos, ecorreo, cotizacion, fechacotizado, equipo, marca, modelo,"+ 
                      "         acreditado, sitio, express, subcontratado, punto, c.email,"+
                      "         metodo, sd.direccion, declaracion, certificado, proxima, cantidad, "+
                      "         serie, sd.observacion, nombreca, c.nombrecompleto as cliente, c.documento, "+
                      "         case when sd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end as medida"+
                      "    FROM web.solicitud s INNER JOIN web.solicitud_detalle sd using (solicitud)"+
		       "                        INNER JOIN clientes c on c.id = s.idcliente"+
		       "                        inner join magnitudes m on m.id = sd.idmagnitud"+
		       "                        inner join magnitud_intervalos i on i.id = sd.idintervalo"+
                       "     where s.solicitud = " + solicitud + " "+
                        "    ORDER BY sd.id";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->RpSolicitud", 1);

            if (!datos.next())
                return "1|No hay nada que reportar";

            String DirectorioReportesRelativo = "~/Reportes/";
            String directorio = "~/DocumPDF/";
            String unico = "Solicitud-" + solicitud + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            String urlArchivo = String.Format("{0}.{1}", reporte, "rdlc");

            String urlArchivoGuardar;
            urlArchivoGuardar = String.Format("{0}.{1}", unico, "pdf");

            String FullPathReport = String.Format("{0}{1}",
                                    this.HttpContext.Server.MapPath(DirectorioReportesRelativo),
                                        urlArchivo);
            String FullGuardar = String.Format("{0}{1}",
                                    this.HttpContext.Server.MapPath(directorio),
                                        urlArchivoGuardar);
            Microsoft.Reporting.WebForms.ReportViewer Reporte = new ReportViewer();
            Reporte.Reset();
            Reporte.LocalReport.ReportPath = FullPathReport;
            Reporte.LocalReport.EnableExternalImages = true;

            ReportDataSource DataSource = new ReportDataSource("DataSet1", datos);
            Reporte.LocalReport.DataSources.Add(DataSource);
            Reporte.LocalReport.Refresh();
            byte[] ArchivoPDF = Reporte.LocalReport.Render("PDF");
            FileStream fs = new FileStream(FullGuardar, FileMode.Create);
            fs.Write(ArchivoPDF, 0, ArchivoPDF.Length);
            fs.Close();

            DateTime fechasol = DateTime.Parse(datos.Rows[0]["fechasoli"].ToString());
            string cliente = datos.Rows[0]["cliente"].ToString();

            string rutasolicitud = HostingEnvironment.MapPath("~/DocumPDF/" + urlArchivoGuardar);
            string rutagestion = System.Configuration.ConfigurationManager.AppSettings["sistemagestion"];
            rutagestion += fechasol.Year + "\\6. ÁREA COMERCIAL\\1. VENTAS\\DID-78\\DID-78-" + solicitud + " " + cliente + ".pdf";
            System.IO.File.Copy(rutasolicitud, rutagestion, true);

            return "0|" + urlArchivoGuardar;



        }
     */
    public String TablaConfiguracionWeb(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        switch (tipo) {
            case 1:
                sql = "SELECT id, descripcion, estado,"
                        + "  '<button class=''btn btn-glow btn-danger'' title=''Eliminar Ubicacion'' type=''button'' onclick=' || chr(34) || 'EliminarUbicacion(' || u.id || ',''' || u.descripcion  || ''')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + "  FROM web.ubicacion u "
                        + "  WHERE idcliente = " + cliente + (id > 0 ? " and u.id=" + id : "") + ""
                        + "  ORDER BY 1";
                break;
            case 2:
                sql = "SELECT id, descripcion, cargo, idcliente, estado,"
                        + "  '<button class=''btn btn-glow btn-danger'' title=''Eliminar Responsable'' type=''button'' onclick=' || chr(34) || 'EliminarResponsable(' || r.id || ',''' || r.descripcion  || ''')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + "  FROM web.responsable r"
                        + "  WHERE idcliente = " + cliente + (id > 0 ? " and r.id=" + id : "") + "  "
                        + "  ORDER BY 1";
                break;
            case 3:
                sql = "SELECT e.id, e.descripcion, idmagnitud, e.estado, m.descripcion as magnitud,"
                        + " '<button class=''btn btn-glow btn-danger'' title=''Eliminar Equipo'' type=''button'' onclick=' || chr(34) || 'EliminarEquipo(' || e.id || ',''' || e.descripcion  || ' (' || m.descripcion || ')''' || ')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + " FROM web.equipo e inner join magnitudes m on m.id = e.idmagnitud"
                        + " WHERE idcliente = " + cliente + (id > 0 ? " and e.id=" + id : "") + "  "
                        + " ORDER BY 1";
                break;
            case 4:
                sql = "SELECT id, descripcion, idcliente, estado,"
                        + "  '<button class=''btn btn-glow btn-danger'' title=''Eliminar Marca'' type=''button'' onclick=' || chr(34) || 'EliminarMarca(' || m.id || ',''' || m.descripcion || ''')' || chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + "  FROM web.marca m"
                        + "  WHERE idcliente = " + cliente + (id > 0 ? " and m.id=" + id : "") + "  "
                        + "  ORDER BY 1";
                break;
            case 5:
                sql = "SELECT mo.id, mo.descripcion, idmarca, mo.estado, ma.descripcion as marca,"
                        + "  '<button class=''btn btn-glow btn-danger'' title=''Eliminar Modelo'' type=''button'' onclick=' || chr(34) || 'EliminarModelo(' || mo.id || ',''' || mo.descripcion || '(' || ma.descripcion || ')'',' || mo.idmarca || ')' ||  chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + "  FROM web.modelo mo inner join web.marca ma on ma.id = mo.idmarca"
                        + "  WHERE mo.idcliente = " + cliente + (id > 0 ? " and mo.id=" + id : "") + "  "
                        + "  ORDER BY 1";
                break;
            case 6:
                sql = "SELECT i.id, i.idmagnitud, i.medida, i.idcliente, i.desde, i.hasta, m.descripcion as magnitud,"
                        + "  '<button class=''btn btn-glow btn-danger'' title=''Eliminar Rango'' type=''button'' onclick=' || chr(34) || 'EliminarMedida(' || i.id || ',''' || m.descripcion || ' (' || i.medida || ' ' ||  i.desde || '-' || i.hasta || ')'')' || chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + "  FROM web.intervalo i inner join magnitudes m on m.id = i.idmagnitud"
                        + "  WHERE i.idcliente = " + cliente + (id > 0 ? " and i.id=" + id : "") + "  "
                        + "  ORDER BY 1";
                break;
        }
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaConfiguracionWeb");
    }

    public String GuardarResponsables(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int Res_Id = Globales.Validarintnonull(request.getParameter("Res_Id"));
        String Res_Responsable = (request.getParameter("Res_Responsable"));
        String Res_Cargo = (request.getParameter("Res_Cargo"));
        String Res_Estado = (request.getParameter("Res_Estado"));
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        String Mensaje = "";
        try {
            Res_Responsable = Res_Responsable.trim().toUpperCase();
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.responsable WHERE descripcion = '" + Res_Responsable + "' and idcliente = " + Cliente + " and id <> " + Res_Id));
            if (encontrado > 0) {
                return "1|Esta descripción ya existe como responsable";
            }

            if (Res_Id == 0) {
                sql = "INSERT INTO web.responsable(idcliente, descripcion, cargo, estado)"
                        + "VALUES (" + Cliente + ",'" + Res_Responsable + "','" + Res_Cargo + "','" + Res_Estado + "')";

                if (Globales.DatosAuditoria(sql, "RESPONSABLE", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarResponsables")) {

                    return "0|Responsable agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                sql = "UPDATE web.responsable"
                        + "SET descripcion='" + Res_Responsable + "', estado='" + Res_Estado + "', cargo='" + Res_Cargo + "' "
                        + " WHERE id=" + Res_Id;

                if (Globales.DatosAuditoria(sql, "RESPONSABLE", "AGREGAR", idusuario, iplocal, this.getClass() + "-->RegistroRevision")) {

                    return "0|Responsable actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarResponsables(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        String Mensaje = "";
        try {
            sql = "DELETE FROM web.responsable WHERE id = " + Id;
            Mensaje = "0|Responsable eliminado con éxito";
            if (Globales.DatosAuditoria(sql, "RESPONSABLE", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarResponsables")) {
                return Mensaje;

            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|No se pued eliminar este responsable porque está relacionado a un equipo dentro del inventario";
        }
    }

    public String GuardarUbicacion(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int Ubi_Id = Globales.Validarintnonull(request.getParameter("Ubi_Id"));
        String Ubi_Ubicacion = (request.getParameter("Ubi_Ubicacion"));
        String Ubi_Estado = (request.getParameter("Ubi_Estado"));
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        String Mensaje = "";
        try {
            Ubi_Ubicacion = Ubi_Ubicacion.trim().toUpperCase();
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.ubicacion WHERE descripcion = '" + Ubi_Ubicacion + "' and idcliente = " + Cliente + " and id <> " + Ubi_Id));
            if (encontrado > 0) {
                return "1|Esta descripción ya existe como ubicación";
            }

            if (Ubi_Id == 0) {
                sql = "INSERT INTO web.ubicacion(idcliente, descripcion, estado)"
                        + "VALUES (" + Cliente + ",'" + Ubi_Ubicacion + "','" + Ubi_Estado + "')";
                Mensaje = "0|Ubicación agregada con éxito";
                if (Globales.DatosAuditoria(sql, "UBICACION", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarUbicacion")) {

                    return Mensaje;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                sql = "UPDATE web.ubicacion"
                        + "SET descripcion='" + Ubi_Ubicacion + "', estado='" + Ubi_Estado + "' "
                        + "WHERE id=" + Ubi_Id;
                Mensaje = "0|Ubicación actualizada con éxito";

                if (Globales.DatosAuditoria(sql, "UBICACION", "ACTUALIZAR", idusuario, iplocal, this.getClass() + "-->GuardarUbicacion")) {

                    return Mensaje;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarUbicacion(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        String Mensaje = "";
        try {
            sql = "DELETE FROM web.ubicacion WHERE id = " + Id;
            Mensaje = "0|Ubicación eliminado con éxito";
            if (Globales.DatosAuditoria(sql, "UBICACION", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarUbicacion")) {
                return Mensaje;

            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|No se pued eliminar esta ubicación porque está relacionado a un equipo dentro del inventario";
        }
    }

    public String GuardarEquipos(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int Equ_Magnitud = Globales.Validarintnonull(request.getParameter("Equ_Magnitud"));
        int Equ_Id = Globales.Validarintnonull(request.getParameter("Equ_Id"));
        String Equ_Equipo = (request.getParameter("Equ_Equipo"));
        String Equ_Estado = (request.getParameter("Equ_Estado"));
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        String Mensaje = "";
        try {
            Equ_Equipo = Equ_Equipo.trim().toUpperCase();
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.equipo WHERE descripcion = '" + Equ_Equipo + "' and idcliente = " + Cliente + " and id <> " + Equ_Id));
            if (encontrado > 0) {
                return "1|Esta descripción ya existe como equipo";
            }

            if (Equ_Id == 0) {
                sql = "INSERT INTO web.equipo(idcliente, descripcion, estado, idmagnitud)"
                        + "VALUES (" + Cliente + ",'" + Equ_Equipo + "','" + Equ_Estado + "'," + Equ_Magnitud + ")";
                Mensaje = "0|Equipo agregado con éxito";

                if (Globales.DatosAuditoria(sql, "EQUIPO", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarEquipos")) {
                    return Mensaje;

                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                sql = "UPDATE web.equipo"
                        + "SET descripcion='" + Equ_Equipo + "', estado='" + Equ_Estado + "',idmagnitud=" + Equ_Magnitud
                        + "WHERE id=" + Equ_Id;
                Mensaje = "0|Equipo actualizado con éxito";

                if (Globales.DatosAuditoria(sql, "EQUIPO", "ACTUALIZAR", idusuario, iplocal, this.getClass() + "-->GuardarEquipos")) {
                    return Mensaje;

                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarEquipos(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        String Mensaje = "";
        try {
            sql = "DELETE FROM web.equipo WHERE id = " + Id;
            Mensaje = "0|Equipo eliminado con éxito";

            if (Globales.DatosAuditoria(sql, "EQUIPO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarEquipos")) {

                return Mensaje;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|No se pued eliminar este equipo porque está relacionado a un equipo dentro del inventario";
        }
    }

    public String GuardarMarcas(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int Mar_Id = Globales.Validarintnonull(request.getParameter("Mar_Id"));
        String Mar_Marca = (request.getParameter("Mar_Marca"));
        String Mar_Estado = (request.getParameter("Mar_Estado"));
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        String Mensaje = "";
        try {
            Mar_Marca = Mar_Marca.trim().toUpperCase();
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.marca WHERE descripcion = '" + Mar_Marca + "' and idcliente = " + Cliente + " and id <> " + Mar_Id));
            if (encontrado > 0) {
                return "1|Esta descripción ya existe como marca";
            }
            if (Mar_Id == 0) {
                sql = "INSERT INTO web.marca(idcliente, descripcion, estado)"
                        + "VALUES (" + Cliente + ",'" + Mar_Marca + "','" + Mar_Estado + "')";
                Mensaje = "0|Marca agregada con éxito";

                if (Globales.DatosAuditoria(sql, "MARCAS", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarMarcas")) {

                    return Mensaje;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                sql = "UPDATE web.marca"
                        + "SET descripcion='" + Mar_Marca + "', estado='" + Mar_Estado + "' "
                        + "WHERE id=" + Mar_Id;
                Mensaje = "0|Marca actualizada con éxito";

                if (Globales.DatosAuditoria(sql, "REVISION POR LAS MAGNITUDES", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarMarcas")) {

                    return Mensaje;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarMarcas(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        String Mensaje = "";
        try {
            sql = "DELETE FROM web.marca WHERE id = " + Id;
            Mensaje = "0|Marca eliminada con éxito";

            if (Globales.DatosAuditoria(sql, "MARCAS", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarMarcas")) {

                return Mensaje;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|No se pued eliminar esta marca porque está relacionado a un equipo dentro del inventario";
        }
    }

    public String GuardarModelos(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int Mod_Marca = Globales.Validarintnonull(request.getParameter("Mod_Marca"));
        int Mod_Id = Globales.Validarintnonull(request.getParameter("Mod_Id"));
        String Mod_Modelo = (request.getParameter("Mod_Modelo"));
        String Mod_Estado = (request.getParameter("Mod_Estado"));
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        String Mensaje = "";
        try {
            Mod_Modelo = Mod_Modelo.trim().toUpperCase();
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.modelo WHERE descripcion = '" + Mod_Modelo + "' and idmarca=" + Mod_Marca + " and idcliente=" + Cliente + " and id <> " + Mod_Id));
            if (encontrado > 0) {
                return "1|Esta descripción ya existe como modelo dentro de esta marca";
            }

            if (Mod_Id == 0) {
                sql = "INSERT INTO web.modelo(idcliente, descripcion, estado, idmarca)"
                        + "VALUES (" + Cliente + ",'" + Mod_Modelo + "','" + Mod_Estado + "'," + Mod_Marca + ")";
                Mensaje = "0|Modelo agregado con éxito";
                if (Globales.DatosAuditoria(sql, "MODELOS", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarModelos")) {

                    return Mensaje;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                sql = "UPDATE web.modelo"
                        + "SET descripcion='" + Mod_Modelo + "', estado='" + Mod_Estado + "',idmarca=" + Mod_Marca
                        + "WHERE id=" + Mod_Id;
                Mensaje = "0|Modelo actualizado con éxito";
                if (Globales.DatosAuditoria(sql, "MODELOS", "ACTUALIZAR", idusuario, iplocal, this.getClass() + "-->GuardarModelos")) {

                    return Mensaje;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarModelos(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */

        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        String Mensaje = "";
        try {
            sql = "DELETE FROM web.modelo WHERE id = " + Id;
            Mensaje = "0|Modelo eliminado con éxito";
            if (Globales.DatosAuditoria(sql, "MODELOS", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarModelos")) {
                return Mensaje;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|No se pued eliminar este modelo porque está relacionado a un equipo dentro del inventario";
        }
    }

    public String GuardarRangos(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        String Med_Unidad = (request.getParameter("Med_Unidad"));
        int Med_Id = Globales.Validarintnonull(request.getParameter("Med_Id"));
        String Med_Desde = (request.getParameter("Med_Desde"));
        String Med_Hasta = (request.getParameter("Med_Hasta"));
        int Med_Magnitud = Globales.Validarintnonull(request.getParameter("Med_Magnitud"));
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        String Mensaje = "";
        try {
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.intervalo WHERE medida = '" + Med_Unidad + "' and desde='" + Med_Desde + "' and hasta='" + Med_Hasta + "' and idcliente=" + Cliente + " and id <> " + Med_Id));
            if (encontrado > 0) {
                return "1|Este rango ya se encuentra registrado";
            }

            if (Med_Id == 0) {

                sql = "INSERT INTO web.intervalo(idcliente, idmagnitud, medida, desde, hasta)"
                        + "VALUES (" + Cliente + "," + Med_Magnitud + ",'" + Med_Unidad + "','" + Med_Desde + "','" + Med_Hasta + "')";
                Mensaje = "0|Rango agregado con éxito";

                if (Globales.DatosAuditoria(sql, "RANGOS", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarRangos")) {
                    return Mensaje;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                sql = "UPDATE web.intervalo"
                        + "SET medida='" + Med_Unidad + "', desde='" + Med_Desde + "', hasta='" + Med_Hasta + "', idmagnitud=" + Med_Magnitud
                        + "WHERE id=" + Med_Id;
                Mensaje = "0|Rango actualizado con éxito";
                if (Globales.DatosAuditoria(sql, "RANGOS", "ACTUALIZAR", idusuario, iplocal, this.getClass() + "-->GuardarRangos")) {
                    return Mensaje;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarRangos(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        String Mensaje = "";
        try {
            sql = "DELETE FROM web.intervalo WHERE id = " + Id;
            Mensaje = "0|Rango eliminado con éxito";

            if (Globales.DatosAuditoria(sql, "RANGOS", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->GuardarRangos")) {
                return Mensaje;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|No se puede eliminar este rango porque está relacionado a un equipo dentro del inventario";
        }
    }

    public String GuardarIntervaloWeb(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String medida = (request.getParameter("medida"));
        String desde = (request.getParameter("desde"));
        String hasta = (request.getParameter("hasta"));
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        sql = "SELECT id from web.intervalo "
                + "  where idmagnitud = " + magnitud + " and medida = '" + medida + "' and desde = '" + desde + "' and hasta = '" + hasta + "' and idcliente=" + Cliente;
        int id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

        if (id > 0) {
            return "XX";
        }

        sql = "INSERT INTO web.intervalo(idcliente, idmagnitud, medida, desde, hasta)"
                + "      VALUES (" + Cliente + "," + magnitud + ",'" + medida + "','" + desde + "','" + hasta + "'); "
                + "  SELECT  MAX(id) FROM web.intervalo WHERE idmagnitud = " + magnitud;
        return Globales.ObtenerUnValor(sql);
    }

    public String GuardarNuevoEquipos(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int AId = Globales.Validarintnonull(request.getParameter("AId"));
        int AUbicacion = Globales.Validarintnonull(request.getParameter("AUbicacion"));
        int AResponsable = Globales.Validarintnonull(request.getParameter("AResponsable"));
        int AMagnitud = Globales.Validarintnonull(request.getParameter("AMagnitud"));
        int AEquipo = Globales.Validarintnonull(request.getParameter("AEquipo"));
        int AMarca = Globales.Validarintnonull(request.getParameter("AMarca"));
        int AModelo = Globales.Validarintnonull(request.getParameter("AModelo"));
        int ARango = Globales.Validarintnonull(request.getParameter("ARango"));
        String ASerie = (request.getParameter("ASerie"));
        String AEstado = (request.getParameter("AEstado"));
        String Aproxima = (request.getParameter("Aproxima"));
        String AAccesorio = (request.getParameter("AAccesorio"));
        String AObservacion = (request.getParameter("AObservacion"));
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        String Mensaje = "";
        try {
            ASerie = ASerie.trim();
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM web.inventario WHERE serie = '" + ASerie + "' and idcliente = " + Cliente + " and id <> " + AId));
            if (encontrado > 0) {
                return "1|Esta serie ya se encuentra asignada a un equipo dentro del inventario";
            }
            if (AId == 0) {

                sql = "INSERT INTO web.inventario(idcliente, idmagnitud, idequipo, idmarca, idmodelo, idintervalo, serie, "
                        + "              estado, proxima, fotos, observacion, accesorios, idresponsable,idubicacion)"
                        + "VALUES (" + Cliente + "," + AMagnitud + "," + AEquipo + "," + AMarca + "," + AModelo + "," + ARango + ",'" + ASerie + "','" + AEstado + "','" + Aproxima + "',0,'" + AObservacion + "','" + AAccesorio + "'," + AResponsable + "," + AUbicacion + ")";
                Mensaje = "0|Equipo agregado al inventario con éxito";

                if (Globales.DatosAuditoria(sql, "EQUIPOS", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarNuevoEquipos")) {
                    return Mensaje;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                sql = "UPDATE web.inventario"
                        + "SET idmagnitud=" + AMagnitud + ",idequipo=" + AEquipo + ",idmarca=" + AMarca + ",idmodelo=" + AModelo + ",idintervalo=" + ARango + ",serie='" + ASerie + "',estado='" + AEstado + "',proxima='" + Aproxima
                        + "',observacion='" + AObservacion + "', accesorios='" + AAccesorio + "',idresponsable='" + AResponsable
                        + ",idubicacion=" + AUbicacion
                        + " WHERE id=" + AId;
                Mensaje = "0|Equipo actualizado en el inventario con éxito";
                if (Globales.DatosAuditoria(sql, "EQUIPOS", "ACTUALIZAR", idusuario, iplocal, this.getClass() + "-->GuardarNuevoEquipos")) {
                    return Mensaje;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String ConsultarInventario(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter("responsable"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        String estado = (request.getParameter("estado"));
        String serie = (request.getParameter("serie"));
        String proxima = (request.getParameter("proxima"));
        int rango = Globales.Validarintnonull(request.getParameter("rango"));
        int reportados = Globales.Validarintnonull(request.getParameter("reportados"));
        int vencidos = Globales.Validarintnonull(request.getParameter("vencidos"));
        int certificados = Globales.Validarintnonull(request.getParameter("certificados"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));

        String busqueda = "WHERE inv.idcliente=" + cliente;
        if (ubicacion > 0) {
            busqueda += " AND inv.idubicacion = " + ubicacion;
        }
        if (!estado.equals("Todos")) {
            busqueda += " AND inv.estado = '" + estado + "'";
        }
        if (magnitud > 0) {
            busqueda += " AND inv.idmagnitud = " + magnitud;
        }
        if (marca > 0) {
            busqueda += " AND inv.idmarca = " + marca;
        }
        if (modelo > 0) {
            busqueda += " AND inv.idmodelo = " + modelo;
        }
        if (rango > 0) {
            busqueda += " AND inv.idintervalo = " + rango;
        }
        if (!serie.equals("")) {
            busqueda += " AND inv.serie ilike '%" + serie + "%'";
        }
        if (!String.equals("")
            
            (proxima)
        )
                busqueda += " AND inv.proxima < now()";

        sql = "SELECT inv.id, "
                + "      case when inv.fotos > 0 then '<img src=''FotoEquipo/' || inv.id || '/1.jpg'' onclick=' || chr(34) || 'LlamarFotoDetInv(' || inv.id || ',' || inv.fotos || ',''' ||  m.descripcion || ' ' || e.descripcion || ''')' || chr(34) || ' width=''80px''/><br>' else '' end as foto,"
                + "      '<a href=' || chr(34) || 'javascript:DetalleInv(1,' || inv.id || ',''' || m.descripcion || ' ' || e.descripcion || ''')' || chr(34) || '>Certificados</a><br>"
                + "      <a href=' || chr(34) || 'javascript:DetalleInv(2,' || inv.id || ',''' || m.descripcion || ' ' || e.descripcion || ''')' || chr(34) || '>Reportes</a><br>"
                + "      <a href=' || chr(34) || 'javascript:DetalleInv(3,' || inv.id || ',''' || m.descripcion || ' ' || e.descripcion || ''')' || chr(34) || '>Traslados</a><br> "
                + "      <a href=' || chr(34) || 'javascript:DetalleInv(4,' || inv.id || ',''' || m.descripcion || ' ' || e.descripcion || ''')' || chr(34) || '>Retiro</a>' as opciones,"
                + "      u.descripcion || '<br>' || r.descripcion as responsable,"
                + "          m.descripcion || '<br>' || e.descripcion as equipo,"
                + "          ma.descripcion || '<br>' || mo.descripcion as marca, "
                + "          to_char(inv.fecha,'yyyy/MM/dd HH24:MI') as fecha, to_char(inv.proxima,'yyyy/MM/dd HH24:MI') as proxima, "
                + "         inv.estado, inv.observacion, inv.accesorios, extract(days from (now()-inv.proxima)) || ' días' as tiempo,"
                + "      (select cer.numero from certificados cer inner join remision_detalle rd on rd.ingreso = cer.ingreso where rd.serie = inv.serie order by cer.id desc limit 1) as ultimo,"
                + "      case when inv.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || i.desde || ' a ' || i.hasta || ') ' || i.medida END || '<br>' || inv.serie as serie"
                + "    FROM web.inventario inv inner join web.responsable r on r.id = inv.idresponsable"
                + "                           inner join web.ubicacion u on u.id = inv.idubicacion"
                + "                           inner join magnitudes m on m.id = inv.idmagnitud"
                + "                          inner join web.marca ma on ma.id = inv.idmarca"
                + "                  inner join web.intervalo i on i.id = inv.idintervalo"
                + "                          inner join web.modelo mo on mo.id = inv.idmodelo"
                + "                          inner join web.equipo e on e.id = inv.idequipo " + busqueda + " "
                + "ORDER BY inv.id desc";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarInventario");
    }

    public String DetalleInventario(HttpServletRequest request) {
        /*
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            Inicializar();
         */
        int opcion = Globales.Validarintnonull(request.getParameter("opcion"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String resultado = "";

        switch (opcion) {
            case 1:
                resultado = "<table width='100%' class='table table-bordered hover'>";
                resultado += "<tr>"
                        + "<th>ITEM</th>"
                        + "<th>Nro Certificado</th>"
                        + "<th>Fecha</th>"
                        + "<th>Especialista</th>"
                        + "<th>Observación</th>"
                        + "<th>Tiempo</th>"
                        + "<th>Anulado</th>"
                        + "<td width='5%'>&nbsp;</td></tr>";

                sql = "sELECT c.observacion, to_char(c.fecha, 'dd/MM/yyyy HH24:MI') as fecha, u.nombrecompleto as usuario, c.numero, to_char(c.ingreso,'0000000') as ingreso,"
                        + "      tiempo(min(ri.fecha), c.fecha, 3) as tiempo, c.item,'' as anulado, c.impreso"
                        + "  FROM certificados c inner join seguridad.rbac_usuario u on u.idusu = idusuario"
                        + "                      INNER JOIN remision_detalle rd on rd.ingreso = c.ingreso"
                        + "                      INNER JOIN web.inventario inv on inv.serie = rd.serie "
                        + "                      left join reporte_ingreso ri on ri.ingreso = c.ingreso"
                        + "  WHERE inv.id = " + id + "  "
                        + "  group by c.observacion, c.fecha, u.nombrecompleto, c.numero, c.ingreso, c.item, c.impreso"
                        + "  ORDER BY item";
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleInventario", 1);
                if (datos.Rows.Count > 0) {
                    foreach(DataRow row in datos.Rows
                    
                    
                        )
                        {
                            resultado += "<tr>"
                                + "<td>" + row["item"].ToString() + "</td>"
                                + "<td>" + row["numero"].ToString() + "</td>"
                                + "<td>" + row["fecha"].ToString() + "</td>"
                                + "<td>" + row["usuario"].ToString() + "</td>"
                                + "<td>" + row["observacion"].ToString() + "</td>"
                                + "<td>" + row["tiempo"].ToString() + "</td>"
                                + "<td>" + row["anulado"].ToString() + "</td>"
                                + "<td width='5%'><button class='btn btn-success' title='Imprimir Certificado' type='button' onclick=\"ImprimirCertificadoInv('" + (row["impreso"].ToString() == "1" ? row["numero"].ToString() + " " + row["ingreso"].ToString() : row["item"].ToString() + "-" + row["numero"].ToString()) + "')\"><span data-icon='&#xe0f7;'></span></button></td></tr>";
                    }
                }
                resultado += "</table>";
                break;
            case 2:
                resultado = "<table width='100%' class='table table-bordered hover'>";
                resultado += "<tr>"
                        + "<th>Especialista Entrega</th>"
                        + "<th>Fecha</th>"
                        + "<th>Observación del Asesor</th>"
                        + "<th>Asesor Recibe</th></tr>";

                sql = "SELECT observacion, to_char(fecha, 'dd/MM/yyyy HH24:MI') as fecha, ur.nombrecompleto as usuariorec, ue.nombrecompleto as usuarioent"
                        + "  FROM ingreso_recibidoing ir inner join seguridad.rbac_usuario ue on ue.idusu = idusuarioent"
                        + "                                          inner join seguridad.rbac_usuario ur on ur.idusu = idusuario"
                        + "  WHERE ingreso = " + id;
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleInventario", 1);
                if (datos.Rows.Count > 0) {
                    foreach(DataRow row in datos.Rows
                    
                    
                        )
                        {
                            resultado += "<tr>"
                                + "<td>" + row["usuarioent"].ToString() + "</td>"
                                + "<td>" + row["fecha"].ToString() + "</td>"
                                + "<td>" + row["observacion"].ToString() + "</td>"
                                + "<td>" + row["usuariorec"].ToString() + "</td></tr>";
                    }
                }
                resultado += "</table>";
                break;
            case 3:
                resultado = "<table width='100%' class='table table-bordered hover'>";
                resultado += "<tr>"
                        + "<th>Nro Orden de Compra</th>"
                        + "<th>Tipo</th>"
                        + "<th>Fecha</th>"
                        + "<th>Asesor</th>"
                        + "<td width='5%'>&nbsp;</td></tr>";

                sql = "SELECT numero, to_char(fechaorden, 'dd/MM/yyyy HH24:MI') as fecha, u.nombrecompleto as usuario, tipo"
                        + "  FROM orden_compra oc inner join seguridad.rbac_usuario u on u.idusu = idusuario"
                        + "  WHERE ingreso = " + id;
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleInventario", 1);
                if (datos.Rows.Count > 0) {
                    foreach(DataRow row in datos.Rows
                    
                    
                        )
                        {
                            resultado += "<tr>"
                                + "<td>" + row["numero"].ToString() + "</td>"
                                + "<td>" + row["tipo"].ToString() + "</td>"
                                + "<td>" + row["fecha"].ToString() + "</td>"
                                + "<td>" + row["usuario"].ToString() + "</td>"
                                + "<td width='5%'><button class='btn btn-success' title='Imprimir Órden de Compra' type='button' onclick=\"ImprimirOrden('" + row["numero"].ToString() + "')\"><span data-icon='&#xe0f7;'></span></button></td></tr>";
                    }
                }
                resultado += "</table>";
                break;
            case 4:
                resultado = "<table width='100%' class='table table-bordered hover'>";
                resultado += "<tr>"
                        + "<th width='5%'>&nbsp;</th>"
                        + "<th>Nro Factura</th>"
                        + "<th>Estado</th>"
                        + "<th>Registro</th>"
                        + "<th>Enviada</th>"
                        + "<th>correoenvio</th>"
                        + "<th>Anulada</th></tr>";

                sql = "select DISTINCT f.factura, to_char(f.fecha,'yyyy/MM/dd HH:24') || '<br>' || u.nombrecompleto as registro, f.estado,"
                        + "      to_char(f.fechaenvio,'yyyy/MM/dd HH:24') || '<br>' || ue.nombrecompleto as enviado,"
                        + "      to_char(f.fechaanula,'yyyy/MM/dd HH:24') || '<br>' || ua.nombrecompleto as anulado, f.correoenvia"
                        + "     from factura f inner join factura_datelle fd on f.id = fd.idfactura"
                        + "                    inner join seguridad.rbac_usuario u on u.idusu = f.idusuario"
                        + "                    inner join seguridad.rbac_usuario ua on ua.idusu = f.idusuarioanula"
                        + "                    inner join seguridad.rbac_usuario ue on ue.idusu = f.idusuarioenvia"
                        + "     WHERE ingreso = " + id + " ORDER BY f.factura DESC";
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleInventario", 1);
                if (datos.Rows.Count > 0) {
                    foreach(DataRow row in datos.Rows
                    
                    
                        )
                        {
                            resultado += "<tr>"
                                + "<td width='5%'><button class='btn btn-success' title='Imprimir Factura' type='button' onclick=\"ImprimirFactura('" + row["factura"].ToString() + "')\"><span data-icon='&#xe0f7;'></span></button></td>"
                                + "<td>" + row["factura"].ToString() + "</td>"
                                + "<td>" + row["estado"].ToString() + "</td>"
                                + "<td>" + row["registro"].ToString() + "</td>"
                                + "<td>" + row["enviado"].ToString() + "</td>"
                                + "<td>" + row["anulado"].ToString() + "</td>"
                                + "<td>" + row["correoenvia"].ToString() + "</td></tr>";
                    }
                }
                resultado += "</table>";
                break;
        }

        return resultado;

    }

    //FIN DEL CÓDIGO
    protected void Servidor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String ruta_origen = request.getHeader("Origin");

        HttpSession misession = request.getSession();
        idusuario = misession.getAttribute("idusuario").toString();
        iplocal = misession.getAttribute("IPLocal").toString();
        usuario = misession.getAttribute("codusu").toString();
        if (misession.getAttribute("ArchivosAdjuntos") != null) {
            archivoadjunto = misession.getAttribute("ArchivosAdjuntos").toString();
        }
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");

        try (PrintWriter out = response.getWriter()) {
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else {
                if (misession.getAttribute("codusu") == null) {
                    out.println("cerrada");
                } else {
                    if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                        out.println("cambio-ip");
                    } else {
                        String opcion = request.getParameter("opcion");
                        switch (opcion) {
                            case "BuscarAcceso":
                                out.println(BuscarAcceso(request));
                                break;
                            case "CambioClave":
                                System.out.println(request);
                                out.println(CambioClave(request));
                                break;
                            case "Televisor":
                                System.out.println(request);
                                out.println(Televisor(request));
                                break;
                            case "CambioClavePag":
                                out.println(CambioClavePag(request));
                                break;
                            case "RecordarClave":
                                out.println(RecordarClave(request));
                                break;
                            case "MagnitudEquipoWeb":
                                out.println(MagnitudEquipoWeb(request));
                                break;
                            case "GuardarOpcionWeb":
                                out.println(GuardarOpcionWeb(request));
                                break;
                            case "ConsultarCotWeb":
                                out.println(ConsultarCotWeb(request));
                                break;
                            case "ConsultarRepWeb":
                                out.println(ConsultarRepWeb(request));
                                break;
                            case "ConsultarIngWeb":
                                out.println(ConsultarIngWeb(request));
                                break;
                            case "ConsultarCerWeb":
                                out.println(ConsultarCerWeb(request));
                                break;
                            case "DatosFirma":
                                out.println(DatosFirma(request));
                                break;
                            case "BuscarCertificado":
                                out.println(BuscarCertificado(request));
                                break;
                            case "ConsultarIngActoWeb":
                                out.println(ConsultarIngActoWeb(request));
                                break;
                            case "SaldoTotal":
                                out.println(SaldoTotal(request));
                                break;
                            case "ConsultarFacturaWeb":
                                out.println(ConsultarFacturaWeb(request));
                                break;
                            case "EstadoIngreso":
                                out.println(EstadoIngreso(request));
                                break;
                            case "RecibirIngreso":
                                out.println(RecibirIngreso(request));
                                break;
                            case "TablaRecibirIngreso":
                                out.println(TablaRecibirIngreso(request));
                                break;
                            case "BuscarAjusteAprobar":
                                out.println(BuscarAjusteAprobar(request));
                                break;
                            case "AprobarCotizacionWeb":
                                out.println(AprobarCotizacionWeb(request));
                                break;
                            case "AprobarReporte":
                                out.println(AprobarReporte(request));
                                break;
                            /*    
                            case "GuardarFirma":
                                out.println(GuardarFirma(request));
                                break;
                             */
                            case "CargarCombosWeb":
                                out.println(CargarCombosWeb(request));
                                break;
                            case "DatosClientesCompletar":
                                out.println(DatosClientesCompletar(request));
                                break;
                            case "AgregarItemCotizacion":
                                out.println(AgregarItemCotizacion(request));
                                break;
                            case "EliminarItemSolicitud":
                                out.println(EliminarItemSolicitud(request));
                                break;
                            /*    
                            case "SubirFotoWeb":
                                out.println(SubirFotoWeb(request));
                                break;
                             */
                            case "MontoFactura":
                                out.println(MontoFactura(request));
                                break;
                            case "GenerarSolicitud":
                                out.println(GenerarSolicitud(request));
                                break;
                            case "TablaSolicitudDetalle":
                                out.println(TablaSolicitudDetalle(request));
                                break;
                            case "AnularSolicitud":
                                out.println(AnularSolicitud(request));
                                break;
                            case "TablaSolicitud":
                                out.println(TablaSolicitud(request));
                                break;
                            case "DetalleRepPago":
                                out.println(DetalleRepPago(request));
                                break;
                            case "VerReportePago":
                                out.println(VerReportePago(request));
                                break;
                            case "AnularReportePago":
                                out.println(AnularReportePago(request));
                                break;

                            case "ReportarPago":
                                out.println(ReportarPago(request));
                                break;
                            /*    
                            case "RpSolicitud":
                                out.println(RpSolicitud(request));
                                break;
                             */
                            case " TablaConfiguracionWeb":
                                out.println(TablaConfiguracionWeb(request));
                                break;

                            case "GuardarResponsables":
                                out.println(GuardarResponsables(request));
                                break;
                            case "EliminarResponsables":
                                out.println(EliminarResponsables(request));
                                break;
                            case "GuardarUbicacion":
                                out.println(GuardarUbicacion(request));
                                break;
                            case "EliminarUbicacion":
                                out.println(EliminarUbicacion(request));
                                break;
                            case "GuardarEquipos":
                                out.println(GuardarEquipos(request));
                                break;
                            case "EliminarEquipos":
                                out.println(EliminarEquipos(request));
                                break;
                            case "GuardarMarcas":
                                out.println(GuardarMarcas(request));
                                break;
                            case "EliminarMarcas":
                                out.println(EliminarMarcas(request));
                                break;
                            case "GuardarModelos":
                                out.println(GuardarModelos(request));
                                break;
                            case "EliminarModelos":
                                out.println(EliminarModelos(request));
                                break;
                            case "GuardarRangos":
                                out.println(GuardarRangos(request));
                                break;
                            case "EliminarRangos":
                                out.println(EliminarRangos(request));
                                break;
                            case "GuardarNuevoEquipos":
                                out.println(GuardarNuevoEquipos(request));
                                break;
                            case "ConsultarInventario":
                                out.println(ConsultarInventario(request));
                                break;
                            case "DetalleInventario":
                                out.println(DetalleInventario(request));
                                break;

                            default:
                                throw new AssertionError();
                        }
                    }
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

}
