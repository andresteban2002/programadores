package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.jfree.util.StringUtils;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Correo"})
public class Correo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String archivoadjunto = "";
    String url_archivo = Globales.url_archivo;
    String ruta_archivo = Globales.ruta_archivo;
    String correoenvio;
    String adjuntos = "";
    String Resultado = "";

    public String ObtenerCorreos() {
        Properties prop = new Properties();

        String Asunto = "";
        String Receptor = "";
        String Correo_Receptor = "";
        String Mensaje = "";
        String Tipo = "";
        Date Fecha;
        String IdCorreo = "";
        String[] Encabezado;
        Address[] Formulario;
        int Id = 0;
        DateFormat formatofecha = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        // Deshabilitamos TLS
        prop.setProperty("mail.pop3.starttls.enable", "false");

        // Hay que usar SSL
        prop.setProperty("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        prop.setProperty("mail.pop3.socketFactory.fallback", "false");

        // Puerto 995 para conectarse.
        prop.setProperty("mail.pop3.port", "110");
        prop.setProperty("mail.pop3.socketFactory.port", "110");

        Session sesion = Session.getInstance(prop);
        sesion.setDebug(false);
        int tipo = 0;
        Store store;
        try {
            store = sesion.getStore("pop3");
            store.connect("mail.calibrationservicesas.com", "programador@calibrationservicesas.com", "CS.2020");

            Folder[] folders = store.getDefaultFolder().list("*");
            for (Folder folder : folders) {
                tipo = folder.getType();
                //if (tipo == 3) {

                folder = store.getFolder(folder.getFullName());
                folder.open(Folder.READ_ONLY);

                Message[] mensajes = folder.getMessages();

                for (int i = 0; i < mensajes.length; i++) {
                    adjuntos = "";
                    Resultado = "";
                    Mensaje = "";
                    Encabezado = mensajes[i].getHeader("Message-ID");
                    if (Encabezado == null) {
                        continue;
                    }
                    IdCorreo = Encabezado[0];
                    Id = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from correo.correo_electronico where idnumcorreo = '" + IdCorreo + "'"));
                    if (Id == 0) {
                        Asunto = mensajes[i].getSubject();
                        Encabezado = mensajes[i].getFrom()[0].toString().split("<");
                        Receptor = Encabezado[0];
                        Correo_Receptor = "";
                        if (Encabezado.length > 1) {
                            Correo_Receptor = Encabezado[1].replace(">", "");
                        }
                        Fecha = mensajes[i].getSentDate();
                        sql = "INSERT INTO correo.correo_electronico(idnumcorreo, correo_remitente, idusuario, fecharec, asunto, correo_receptor, "
                                + " nombre_receptor, texto, adjunto) "
                                + " VALUES ('" + IdCorreo + "','" + correoenvio + "','" + idusuario + "'," + (Fecha == null ? "null" : "'" + formatofecha.format(Fecha) + "'") + ",'" 
                                + Asunto + "','" + Correo_Receptor + "','" + Receptor + "','','') ";
                        Globales.DatosAuditoria(sql, "CORREO", "RECIBIR CORREO", idusuario, iplocal, this.getClass() + "->ObtenerCorreos");
                        Id = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from correo.correo_electronico where idnumcorreo = '" + IdCorreo + "'"));

                        if (mensajes[i].isMimeType("text/plain")) {
                            Mensaje = mensajes[i].getContent().toString();
                        } else if (mensajes[i].isMimeType("multipart/*")) {
                            MimeMultipart mimeMultipart = (MimeMultipart) mensajes[i].getContent();
                            Mensaje = getTextFromMimeMultipart(mimeMultipart, Id);

                        }
                        
                        sql = "UPDATE correo.correo_electronico " + 
                                " SET texto='" + Mensaje + "', adjunto='" + adjuntos + "' " + 
                              " WHERE id = " + Id;
                        Globales.Obtenerdatos(sql,this.getClass() + "->ObtenerCorreos",2);
                    }

                    //}
                }
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

        return "1";
    }

    private String getTextFromMimeMultipart(MimeMultipart mimeMultipart, int Id) {
        try {
            String Ruta = "";
            String Tipo = "";
            File path;
            path = new File(ruta_archivo + "Adjunto/AdjuntoCorreo/" + Id);
            if (!path.exists()) {
                path.mkdirs();
            }
            int count = mimeMultipart.getCount();
            for (int i = 0; i < count; i++) {
                BodyPart bodyPart = mimeMultipart.getBodyPart(i);
                Tipo = bodyPart.getContentType();
                if (bodyPart.isMimeType("text/plain")) {
                    Resultado += "<br>" + bodyPart.getContent();
                    break; // without break same text appears twice in my tests
                } else if (bodyPart.isMimeType("text/html")) {
                    String html = (String) bodyPart.getContent();
                    Resultado += "<br>" + html;
                } else if (bodyPart.getContent() instanceof MimeMultipart) {
                    Resultado += getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent(), Id);
                } else {
                    if (bodyPart.getFileName() != null) {
                        InputStream is = bodyPart.getInputStream();

                        Ruta = ruta_archivo + "Adjunto/AdjuntoCorreo/" + Id + "/" + bodyPart.getFileName();
                        File f = new File(Ruta);
                        FileOutputStream fos = new FileOutputStream(f);
                        byte[] buf = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = is.read(buf)) != -1) {
                            fos.write(buf, 0, bytesRead);
                        }
                        fos.close();
                        if (bodyPart.isMimeType("image/*")) {
                            Resultado += "<br><img src=''" + url_archivo + "Adjunto/AdjuntoCorreo/" + Id + "/" + bodyPart.getFileName() + "'' width=''100%'' data-img=''" + bodyPart.getFileName() + "''/>";
                        } else {
                            if (!adjuntos.equals(""))
                                adjuntos+= ";";
                            adjuntos += bodyPart.getFileName();
                        }
                    }
                }
            }
            return Resultado;
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String ruta_origen = request.getHeader("Origin");

        HttpSession misession = request.getSession();
        idusuario = misession.getAttribute("idusuario").toString();
        iplocal = misession.getAttribute("IPLocal").toString();
        usuario = misession.getAttribute("codusu").toString();
        correoenvio = misession.getAttribute("correoenvio").toString();
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");
        if (misession.getAttribute("ArchivosAdjuntos") != null) {
            archivoadjunto = misession.getAttribute("ArchivosAdjuntos").toString();
        }

        try (PrintWriter out = response.getWriter()) {
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else if (misession.getAttribute("codusu") == null) {
                out.println("cerrada");
            } else if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                out.println("cambio-ip");
            } else {
                String opcion = request.getParameter("opcion");
                switch (opcion) {
                    case "ObtenerCorreos":
                        out.println(ObtenerCorreos());
                        break;
                    default:
                        throw new AssertionError();
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

}
