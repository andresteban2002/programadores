﻿
$("#Ley_Servicios").html(CargarCombo(4, 4));
$("#Ley_EquiposCal").html(CargarCombo(58, 4));
$("#Ley_Grupos").html(CargarCombo(47, 4));
$("#Ley_EquiposInv").html(CargarCombo(48, 4,"","-1"));

function TablaLeyenda() {
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=13&id=0');
    var datasedjson = JSON.parse(datos);
    $('#TablaLeyenda').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "leyenda" },
            { "data": "a_servicios" },
            { "data": "a_equipo_cal" },
            { "data": "a_grupos" },
            { "data": "a_equipo_inv" },
            { "data": "desestado" },
            { "data": "fecha" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        },
		dom: 'Bfrtip',
		buttons: [
			'excel', 'csv', 'copy','colvis'
		]
    });
}

function LimpiarLeyenda() {
    $("#Ley_Id").val("0");
    $("#Ley_Servicios").val("").trigger("change");
    $("#Ley_EquiposCal").val("").trigger("change");
    $("#Ley_Grupos").val("").trigger("change");
    $("#Ley_EquiposInv").val("").trigger("change");
    $("#Ley_Descripcion").val("");
    $("#Ley_Estado").val("1").trigger("change");
}

$("#TablaLeyenda > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=13&id=' + numero);
    var data = JSON.parse(datos);
    $("#Ley_Id").val(data[0].id);
    $("#Ley_Descripcion").val(data[0].leyenda);

    var servicios = data[0].servicios.split(",");
    for (var x = 0; x < servicios.length; x++) {
        $("#Ley_Servicios option[value=" + servicios[x] + "]").prop("selected", true);
    }
    $("#Ley_Servicios").trigger("change");
    
    var equipo_cal = data[0].equipo_cal.split(",");
    for (var x = 0; x < equipo_cal.length; x++) {
        $("#Ley_EquiposCal option[value=" + equipo_cal[x] + "]").prop("selected", true);
    }
    $("#Ley_EquiposCal").trigger("change");
    
    var grupos = data[0].grupos.split(",");
    for (var x = 0; x < grupos.length; x++) {
        $("#Ley_Grupos option[value=" + grupos[x] + "]").prop("selected", true);
    }
    $("#Ley_Grupos").trigger("change");
    
    var equipo_inv = data[0].equipo_inv.split(",");
    for (var x = 0; x < equipo_inv.length; x++) {
        $("#Ley_EquiposInv option[value=" + equipo_inv[x] + "]").prop("selected", true);
    }
    $("#Ley_EquiposInv").trigger("change");
    
    $("#Ley_Estado").val(data[0].estado).trigger("change");
    $("#Met_Descripcion").focus();
});


$("#formLeyenda").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Configuracion","opcion=GuardarLeyenda&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaLeyenda();
        LimpiarLeyenda();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function EliminarLeyenda(id) {
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=13&id=' + id);
    var data = JSON.parse(datos);
    var leyenda = data[0].leyenda;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar la leyenda ' + leyenda + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Configuracion","opcion=EliminarLeyenda&Id=" + id + "&IdReemplazo=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve();
                        else {
                            reject(datos[1]);
                        }

                    })
            })
        }
    }]).then(function (data) {
        TablaLeyenda();
        LimpiarLeyenda();
    });
}

TablaLeyenda();

$('select').select2();
DesactivarLoad();
