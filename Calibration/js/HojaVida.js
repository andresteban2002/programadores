﻿var IdEmpleado = localStorage.getItem('EmpleadoSeleccionado') * 1;
var Foto = "";

if (IdEmpleado > 0) {
    $(".eliminahoja").remove();
    BuscarEmpleado(IdEmpleado);
}



$("#tablamodalempleados_hv > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var id = row[8].innerText;
    BuscarEmpleado(id);
    $("#modalEmpleados").modal("hide");
});

function LimpiarTodo() {
    $("#IdEmpleado").val("0");
    $("#Documento").val("");
    $("#Nombre").val("");
    $("#FechaN").val("");
    $("#LugarN").val("");
    $("#EstadoC").val("");
    $("#Direccion").val("");
    $("#Telefono").val("");
    $("#Correo").val("");
    $("#Cargo").val("");
    $("#FotoEmpleado").attr("src", url_cliente + "Adjunto/imagenes/img_avatar.png");
     
    $("#Perfil").val("");
    $("#Estudios").val("");
    $("#Pregrado").val("");
    $("#Postgrado").val("");
    $("#OtrosE").val("");
    $("#UltimoC").val("");
    $("#Logros").val("");
    $("#Jefe").val("");
    $("#TelefonoC").val("");
    $("#TiempoL").val("");

    for (var x = 0; x <= 1; x++) {
        $("#NombreRF" + x).val("");
        $("#TelefonoRF" + x).val("");
        $("#RelacionRF" + x).val("");

        $("#NombreRP" + x).val("");
        $("#TelefonoRP" + x).val("");
        $("#RelacionRP" + x).val("");

        $("#NombreRL" + x).val("");
        $("#TelefonoRL" + x).val("");
        $("#CargoRL" + x).val("");
    }
            
    $("#TablaAnexos").html("");
}


function BuscarEmpleado(empleado) {

    var datos = LlamarAjax("Recursohumano","opcion=BuscarHojaVida&Empleado=" + empleado).split("|");
    IdEmpleado = empleado;
    var data = JSON.parse(datos[0]);
    $("#IdEmpleado").val(empleado);
    $("#Documento").val(data[0].documento);
    $("#Nombre").val(data[0].nombrecompleto);
    $("#FechaN").val(data[0].fechanacimiento);
    $("#LugarN").val(data[0].lugarnacimiento);
    $("#EstadoC").val(data[0].estadocivil);
    $("#Direccion").val(data[0].direccion);
    $("#Telefono").val(data[0].telefono);
    $("#Correo").val(data[0].email);
    $("#Cargo").val(data[0].cargo);
    Foto = $.trim(data[0].foto);
    if (Foto != "") {
        $("#FotoEmpleado").attr("src", url_cliente + "Adjunto/imagenes/FotoEmpleados/" + empleado + ".jpg?id=" + NumeroAleatorio());
    } else
        $("#FotoEmpleado").attr("src", url_cliente + "Adjunto/imagenes/img_avatar.png?id=" + NumeroAleatorio());

    $("#Perfil").val(data[0].perfil_profesional);
    $("#Estudios").val(data[0].estudios_secundarios);
    $("#Pregrado").val(data[0].pregrado);
    $("#Postgrado").val(data[0].postgrado);
    $("#OtrosE").val(data[0].otros_estudios);
    $("#UltimoC").val(data[0].ultimo_cargo);
    $("#Logros").val(data[0].logros);
    $("#Jefe").val(data[0].jefe_inmediato);
    $("#TelefonoC").val(data[0].telefono_contacto);
    $("#TiempoL").val(data[0].tiempo_laborado);

    if (datos[2] != "[]") {
        var data = JSON.parse(datos[2]);
        for (var x = 0; x < data.length; x++) {
            $("#Nombre" + data[x].tipo + data[x].posicion).val(data[x].nombre);
            $("#Telefono" + data[x].tipo + data[x].posicion).val(data[x].telefono);
            if (data[x].tipo == "RL") {
                $("#Cargo" + data[x].tipo + data[x].posicion).val(data[x].cargo);
            } else {
                $("#Relacion" + data[x].tipo + data[x].posicion).val(data[x].relacion);
            }
        }
    }


    var Resultado = "";
    if (datos[1] != "[]") {
        var data = JSON.parse(datos[1]);
        for (var x = 0; x < data.length; x++) {
            Resultado += "<tr id='fila-" + data[x].id + "'>" +
                "<td>" + data[x].foto + "</td>" +
                "<td><a href='javascript:MaximizarAnexo(" + data[x].foto + ")'><img src='" + url_cliente + "Adjunto/imagenes/AnexoHojaVida/" + IdEmpleado + "/" +  data[x].foto + ".jpg?id=" + NumeroAleatorio() + "' width='100%'></a></td>" +
                "<td>" + data[x].archivo + "</td>" +
                "<td><button class='btn btn-danger' title='Eliminar Anexo' type='button' onclick=\"EliminarAnexo(" + data[x].id + ",'" + data[x].archivo + "'," + data[x].foto + ")\"><span data-icon='&#xe0d8;'></span></button></td></tr>";
        }
    }

    $("#TablaAnexos").html(Resultado);
    
}

function MaximizarAnexo(foto) {
    window.open(url_archivo + "Adjunto/imagenes/AnexoHojaVida/" + IdEmpleado + "/" + foto + ".jpg?id=" + NumeroAleatorio())
}

$("#formHojaVida").submit(function (e) {
    e.preventDefault();
    if (IdEmpleado == 0)
        return false;
			
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Recursohumano","opcion=GuardarHojaVida&" + parametros).split("|");
    if (datos[0] == "0")
        swal("", datos[1], "success");
    else
        swal("Acción Cancelada", datos[1], "warning");
});

function AgregarAnexos() {
    if (IdEmpleado > 0) {
        $("#NombreArchivo").val("");
        $("#DocumentoArchivo").val("");
        $("#modalSubirArchivo").modal("show");
    }
}

function GuardarAnexo() {
    var nombre = $.trim($("#NombreArchivo").val());
    var archivo = $.trim($("#DocumentoArchivo").val());

    if (nombre == "") {
        $("#NombreArchivo").focus();
        swal("Acción Cancelada", "Debe de ingresar el nombre del archivo", "warning");
        return false;
    }

    if (archivo == "") {
        $("#DocumentoArchivo").focus();
        swal("Acción Cancelada", "Debe de seleccionar un archivo tipo imagen ", "warning");
        return false;
    }
    var parametros = "Empleado=" + IdEmpleado + "&NombreArchivo=" + nombre;
    var datos = LlamarAjax("Recursohumano","opcion=GuardarAnexoHoja&" + parametros).split("|");
    if (datos[0] == "0") {
        $("#NombreArchivo").val("");
        $("#DocumentoArchivo").val("");
        swal("", datos[1], "success");
        var foto = datos[2];
        var id = datos[3];
        var Resultado = $("#TablaAnexos").html();
        Resultado += "<tr id='fila-" + id + "'>" +
            "<td>" + foto + "</td>" +
            "<td><a href='javascript:MaximizarAnexo(" + foto + ")'><img src='" + url_cliente + "Adjunto/imagenes/AnexoHojaVida/" + IdEmpleado + "/" + foto + ".jpg?id=" + NumeroAleatorio() + "' width='100%'></a></td>" +
            "<td>" + nombre + "</td>" +
            "<td><button class='btn btn-danger' title='Eliminar Anexo' type='button' onclick=\"EliminarAnexo(" + id + ",'" + nombre + "'," + foto + ")\"><span data-icon='&#xe0d8;'></span></button></td></tr>";
        $("#TablaAnexos").html(Resultado);

    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }


}

function EliminarAnexo(id, archivo, foto) {
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar el anexo ' + archivo + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Recursohumano/EliminarAnexoHoja", "FotoId=" + id + "&Empleado=" + IdEmpleado + "&Foto=" + foto)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            $("#fila-" + id).remove();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

function SubirFotoEmpleado() {
    if (IdEmpleado > 0) {
        $("#modalImagenes").modal("show");
    }
}

function EliminarFotoEmpleado() {
    if (IdEmpleado > 0 && Foto != "") {
        var parametros = "Empleado=" + IdEmpleado + "&Foto=" + Foto;
        var datos = LlamarAjax("Recursohumano","opcion=EliminarFotoEmpleado&" + parametros).split("|");
        if (datos[0] == "0") {
            $("#FotoEmpleado").attr("src", url_cliente + "imagenes/img_avatar.png?id=" + NumeroAleatorio());
            $(".fotoperfil").attr("src", url_cliente + "imagenes/300.png?id=" + NumeroAleatorio());
            swal("", datos[1], "success");
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }
}

function GuardarFotoEmpleado() {

    var cropcanvas = $('#Art_Imagen').cropper('getCroppedCanvas');
    var archivo = cropcanvas.toDataURL("image/jpeg");

    var ruta = cropcanvas.toDataURL();
        
    ActivarLoad();
    setTimeout(function () {
		var parametros = "opcion=SubirFoto&archivo=" + archivo + "&IdEmpleado=" + IdEmpleado + "&tipo_imagen=4";
		var data = LlamarAjax("Configuracion",parametros).split("|");
		DesactivarLoad();
		if (data[0] == "0") {
			$("#FotoEmpleado").attr("src", url_cliente + "Adjunto/imagenes/FotoEmpleados/" + IdEmpleado + ".jpg?id=" + NumeroAleatorio());
			if (IdEmpleado == localStorage.getItem('EmpleadoSeleccionado') * 1)
				$(".fotoperfil").attr("src", url_cliente + "Adjunto/imagenes/FotoEmpleados/" + IdEmpleado + ".jpg?id=" + NumeroAleatorio());
			swal("", data[1], "success");
		} else
			swal("Acción Cancelada", data[1], "warning");
	},15);
    
}

function ImprimirHojaVida() {
    if (IdEmpleado == 0) 
        return false;
    ActivarLoad();
    setTimeout(function () {
        var parametros = "Empleado=" + IdEmpleado;
        var datos = LlamarAjax("Recursohumano","opcion=RpHojaVida&" + parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

    

$('select').select2();
DesactivarLoad();
