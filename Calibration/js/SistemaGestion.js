﻿var CarpetaPrincipal = "";
var RutaGestion = LlamarAjax("SistemaGestion","opcion=RutaGestion");

$("#CargosLec, #CargosDil").html(CargarCombo(26, 0));
$("#MagnitudesLec, #MagnitudesDil").html(CargarCombo(1, 0));
$("#UsuariosLec, #UsuariosDil").html(CargarCombo(13, 0));

function CrearCarpeta() {
    if (CarpetaPrincipal != "") {
        swal({
            title: 'Advertencia',
            text: '¿Nombre de la carpeta a crear?',
            input: 'text',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Crear'),
            cancelButtonText: ValidarTraduccion('Cancelar'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',

            inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                    if (value) {
                        var datos = LlamarAjax("SistemaGestion/CrearCarpeta", "directorio=" + CarpetaPrincipal + "&carpeta=" + value)
                        datos = datos.split("|");
                        if (datos[0] == "0") {
                            resultado = datos[1];
                            resolve()
                        } else {
                            reject(datos[1]);
                        }

                    } else {
                        reject(ValidarTraduccion('Debe de ingresar el nombre de la carpeta'));
                    }
                })
            }
        }).then(function (result) {
            TablaArchivos(CarpetaPrincipal);
            swal({
                type: 'success',
                html: 'Carpeta creada con éxito'
            })
        });
    }
}

function EditarCarpeta(carpeta, nombre) {
    var cambio = 0;
    if (CarpetaPrincipal != "") {
        swal({
            title: 'Advertencia',
            text: '¿Ingrese el nuevo nombre de la carpeta \n [' + nombre + ']?',
            input: 'text',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Crear'),
            cancelButtonText: ValidarTraduccion('Cancelar'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',

            inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                    if (value) {
                        if ($.trim(value) != "") {
                            if ($.trim(value) != $.trim(nombre)) {

                                var datos = LlamarAjax("SistemaGestion/EditarCarpeta", "carpeta=" + carpeta + "&nuevonombre=" + $.trim(value) + "&nombre=" + nombre)
                                datos = datos.split("|");
                                if (datos[0] == "0") {
                                    resultado = datos[1];
                                    resolve()
                                } else {
                                    reject(datos[1]);
                                }
                            }
                        } else {
                            reject(ValidarTraduccion('Debe de ingresar el nuevo nombre de la carpeta'));
                        }
                    } else {
                        reject(ValidarTraduccion('Debe de ingresar el nuevo nombre de la carpeta'));
                    }
                })
            }
        }).then(function (result) {
            TablaArchivos(CarpetaPrincipal);
            swal({
                type: 'success',
                html: 'Carpeta editada con éxito'
            })
        });
        $(".swal2-input").val(nombre);
        

    }
}

function EliminarArchivo(id, tipo, ruta, nombre) {
    var mensaje = "";
    var mensaje2 = "";
    if (tipo == 1) {
        mensaje = "Seguro que desea eliminar la carpeta " + nombre + " y todo su contenido";
        mensaje2 = "Carpeta eliminada con éxito";
    } else {
        mensaje = "Seguro que desea eliminar el archivo " + nombre + " y toda su revisión";
        mensaje2 = "Archivo eliminado con éxito";
    }

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿' + mensaje + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("SistemaGestion/EliminarArchivo", "tipo=" + tipo + "&id=" + id + "&directorio=" + ruta + "&archivo=" + nombre)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]).then(function (result) {
        TablaArchivos(CarpetaPrincipal);
        swal({
            type: 'success',
            html: mensaje2
        })
    });
};

function ConfigurarLink(ruta){
	var _ruta = ruta.split(RutaGestion);
	var carpeta = RutaGestion;
	var resultado = "<a href=\"javascript:TablaArchivos('" + RutaGestion + "')\">" + RutaGestion + "</a>";
	if ($.trim(_ruta[1]) == ""){
		return resultado;
	}
	var a_ruta = _ruta[1].split("/");
	for (var x=0; x<a_ruta.length; x++){
		if ($.trim(a_ruta[x]) != ""){
			carpeta += a_ruta[x] + "/";
			resultado += "<a href=\"javascript:TablaArchivos('" + carpeta + "')\">" + a_ruta[x] + "/</a>";
		}
	}
	return resultado;
}

function TablaArchivos(carpeta) {

    var datos = LlamarAjax("SistemaGestion", "opcion=ArchivosSC&carpeta=" + carpeta).split("|-|");
    if (datos[0] == "0") {
        var data = JSON.parse(datos[1]);
        $("#Ruta").html(ConfigurarLink(datos[2]));
        CarpetaPrincipal = datos[2];
        DesactivarLoad();
        var resultado = "";
        var opciones = "";
        for (var x = 0; x < data.length; x++) {
            resultado += "<tr " + (data[x].tipo == 1 ? "class='text-XXX bg-gris' " : (data[x].estado == "OBSOLETO" || data[x].estado == "NO REVISTADO" ? " class='text-danger text-bold'" : "")) + (data[x].tipo == 1 ? " ondblclick=\"AbrirCarpeta('" + data[x].ruta + "')\"" : "ondblclick=\"EditarRevision(" + data[x].id + ",'" + data[x].ruta + "','" + data[x].nombre + "')\"") + ">";

            if (data[x].tipo == 2) {
                resultado += "<td><a title='Descargar Archivo' href=\"javascript:DescargarArchivo(" + data[x].id + ",'" + data[x].ruta + "','" + data[x].nombre + "')\"><img src='" + url_archivo + "Adjunto/imagenes/icon/" + data[x].icono + ".jpg' width='60px'/></a></td>";
            } else {
                resultado += "<td><a title='Abrir Carpeta' href=\"javascript:AbrirCarpeta('" + data[x].ruta + "')\")'><img src='" + url_archivo + "Adjunto/imagenes/icon/" + data[x].icono + ".jpg' width='60px'/></a></td>";
            }
            opciones = "";
            if (data[x].tipo == 1) {
                opciones += "<button class='btn btn-danger' title='Eliminar Carpeta' type='button' onclick=\"EliminarArchivo(0,1,'" + data[x].ruta + "','" + data[x].nombre + "')\"><span data-icon='&#xe0d8;'></span></button><br>"; 
                opciones += "<button class='btn btn-primary' title='Permisos por Usuarios' type='button' onclick=\"PermisosCarpeta('" + data[x].ruta + "')\"><span data-icon='&#xe1a3;'></span></button><br>";
                opciones += "<button class='btn btn-success' title='Editar Nombre' type='button' onclick=\"EditarCarpeta('" + data[x].carpeta + "','" + data[x].nombre + "')\"><span data-icon=' &#xe03c;'></span></button><br>"; 
                opciones += "<button class='btn btn-warning' title='HistorialCambios' type='button' onclick=\"HistorialCambios('" + data[x].ruta + "')\"><span data-icon='&#xe07f;'></span></button><br>"; 
            }else{
				opciones += "<button class='btn btn-danger' title='Eliminar Archivo' type='button' onclick=\"EliminarArchivo(0,1,'" + data[x].ruta + "','" + data[x].nombre + "')\"><span data-icon='&#xe0d8;'></span></button><br>"; 
                opciones += "<button class='btn btn-primary' title='Permisos por Usuarios' type='button' onclick=\"PermisosArchivo('" + data[x].ruta + data[x].nombre + "')\"><span data-icon='&#xe1a3;'></span></button><br>";
                opciones += "<button class='btn btn-success' title='Diligenciar Documento' type='button' onclick=\"DiligenciarDocumento('" + data[x].carpeta.replace + "','" + data[x].nombre + "')\"><span data-icon=' &#xe03c;'></span></button><br>"; 
                opciones += "<button class='btn btn-warning' title='HistorialCambios' type='button' onclick=\"HistorialCambios('" + data[x].ruta + "')\"><span data-icon='&#xe07f;'></span></button><br>"; 
			}

            resultado += "<td>" + data[x].id + "</td>" +
                "<td>" + data[x].nombre + "</td>" +
                "<td>" + data[x].gestion + "</td>" +
                "<td>" + data[x].fecha + "</td>" +
                "<td>" + data[x].creado + "</td>" +
                "<td>" + (data[x].tamanio * 1 > 0 ? data[x].tamanio : "")  + "</td>" +
                "<td>" + data[x].codigo + "</td>" +
                "<td>" + data[x].version + "</td>" +
                "<td>" + data[x].fecharevision + "</td>" +
                "<td>" + data[x].vencimiento + "</td>" +
                "<td>" + data[x].responsable + "</td>" +
                "<td>" + (data[x].tipo == 1 ? "" : data[x].estado) + "</td>" +
                "<td>" + opciones + "</td>" +
                "</tr>";
        }
        $('#TablaArchivos').html(resultado);
    } else {
        ErrorArchivo = 1;
        CarpetaPrincipal = "";
        $('#TablaArchivos').html("");
        swal("Acción Cancelada", datos[1], "warning");
    }
        
}

TablaArchivos("XXX");

function AbrirCarpeta(carpeta) {
    TablaArchivos(carpeta);
}

function EditarRevision(id, ruta) {
    //var datos = LlamarAjax('SistemaGestion/EditarRevision', "id=" + id).split("|");
    $("#modalRevisiones").modal("show");
}

function PermisosCarpeta(ruta){
	var usuariolec = "";
	var usuariodil = "";
	var cargolec = "";
	var cargodil = "";
	var magnitudlec = "";
	var magnituddil = "";
	
	$("#UsuariosLec").val("").trigger("change");
	$("#UsuariosDil").val("").trigger("change");
	$("#CargosLec").val("").trigger("change");
	$("#CargosDil").val("").trigger("change");
	$("#MagnitudesLec").val("").trigger("change");
	$("#MagnitudesDil").val("").trigger("change");
	$("#TodosLec").prop("checked","");
	$("#TodosDil").prop("checked","");
	
	var datos = LlamarAjax("SistemaGestion","opcion=VerPermisosCarpeta&carpeta=" + ruta);
	if (datos != "[]"){
		var data = JSON.parse(datos);
		
		var usuariolec = data[0].permiso_usu_lec.split(",");
        for (var x = 0; x < usuariolec.length; x++) {
            $("#UsuariosLec option[value=" + usuariolec[x] + "]").prop("selected", true);
        }
        $("#UsuariosLec").trigger("change");
        
        var usuariodil = data[0].permiso_usu_dil.split(",");
        for (var x = 0; x < usuariodil.length; x++) {
            $("#UsuariosDil option[value=" + usuariodil[x] + "]").prop("selected", true);
        }
        $("#UsuariosDil").trigger("change");
        
        var cargolec = data[0].permiso_car_lec.split(",");
        for (var x = 0; x < cargolec.length; x++) {
            $("#CargosLec option[value=" + cargolec[x] + "]").prop("selected", true);
        }
        $("#CargosLec").trigger("change");
        
        var cargodil = data[0].permiso_car_dil.split(",");
        for (var x = 0; x < cargodil.length; x++) {
            $("#CargosDil option[value=" + cargodil[x] + "]").prop("selected", true);
        }
        $("#CargosDil").trigger("change");
        
        var magnitudlec = data[0].permiso_mag_lec.split(",");
        for (var x = 0; x < magnitudlec.length; x++) {
            $("#MagnitudesLec option[value=" + magnitudlec[x] + "]").prop("selected", true);
        }
        $("#MagnitudesLec").trigger("change");
        
        var magnituddil = data[0].permiso_mag_dil.split(",");
        for (var x = 0; x < magnituddil.length; x++) {
            $("#MagnitudesDil option[value=" + magnituddil[x] + "]").prop("selected", true);
        }
        $("#MagnitudesDil").trigger("change");
        
        if (data[0].permiso_todo_lec * 1 > 0)
			$("#TodosLec").prop("checked","checked");
			
		if (data[0].permiso_todo_dil *1 > 0)
			$("#TodosDil").prop("checked","checked");
		
	}
	
	$("#CarpetaPermiso").val(ruta);
	$("#modalPermisos").modal("show");
}

$("#FormPermisoCarpeta").submit(function(e){
	e.preventDefault();
	var parametros = $("#FormPermisoCarpeta").serialize();
	var datos = LlamarAjax("SistemaGestion","opcion=PermisosCarpeta&" + parametros).split("|");
	if (datos[0] == "0"){
		swal("",datos[1],"success");
	}else{
		swal("Acción Cancelada",datos[1],"warning");
	}
});

function DescargarArchivo(id, ruta, nombre) {
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("SistemaGestion","opcion=AbrirArchivosSG&d=" + id + "&ruta=" + ruta + "&nombre=" + nombre).split("|");
        DesactivarLoad();
        if (datos[0] == "0")
            window.open(url_archivo + "ArchivoSG/" + datos[1])
        else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
}

$("#TablaScanner > tbody").on("dblclick", "tr", function () {

    ActivarLoad();
    var row = $(this).parents("td").context.cells;
    setTimeout(function () {
        var archivo = row[0].innerText;
        var datos = LlamarAjax('SistemaGestion/AbrirArchivosScanner', "archivo=" + archivo).split("|");
        DesactivarLoad();
        if (datos[0] == "0")
            window.open(url + "ArchivoSG/" + datos[1])
        else
            swal("Acción Cancelada", datos[1], "warning");
        
    }, 15);
});

$('select').select2();
DesactivarLoad();
