package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Idioma"})
public class Idioma extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String url_archivo = Globales.url_archivo;

    public String LlamarArchivoIdioma(HttpServletRequest request) {

        try {
            int Idioma = Globales.Validarintnonull(request.getParameter("Idioma"));

            sql = "SELECT texto, coalesce(espanol,'') as espanol, coalesce(ingles,'') as ingles, coalesce(portugues,'') as portugues FROM traductor";
            ResultSet datos = Globales.Obtenerdatos(sql, this.getClass() + "-->LlamarArchivoIdioma", 1);
            String Archivo = "{";

            while (datos.next()) {
                if (!Archivo.equals("{")) {
                    Archivo += ",";
                }
                Archivo += "\"" + datos.getString("texto") + "\":\"" + datos.getString(Idioma + 1) + "\"";
            }

            Archivo += "}";
            return Archivo;
        } catch (SQLException ex) {
            Globales.GuardarLogger(ex.getMessage(), this.getClass() + "-->LlamarArchivoIdioma");
            return "{}";
        }
    }

    public String CambioIdioma(HttpServletRequest request) {
        HttpSession misession = request.getSession();
        String idioma = request.getParameter("idioma");
        misession.setAttribute("idioma", idioma);
        return idioma;
    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response) {
        try {
             response.setContentType("text/html;charset=UTF-8");
            String ruta_origen = request.getHeader("Origin");
            if (ruta_origen == null) {
                ruta_origen = "http://localhost:8090";
            }
            response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
            response.setHeader("Access-Control-Allow-Credentials", "true");
            PrintWriter out = response.getWriter();
            String opcion = request.getParameter("opcion");
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta... " + ruta_origen + "\"}]");
            } else {
                switch (opcion) {
                    case "LlamarArchivoIdioma":
                        out.print(LlamarArchivoIdioma(request));
                        break;
                    case "CambioIdioma":
                        out.print(CambioIdioma(request));
                        break;
                    default:
                        throw new AssertionError();
                }
            }
        } catch (Exception ex) {
            try {
                PrintWriter out = response.getWriter();
                out.print(ex.getMessage());
            } catch (IOException exl) {
                Globales.GuardarLogger(exl.getMessage(), Laboratorio.class.getName());
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

}
