var AppAngular = angular.module("myApp", ["ngRoute"]);
AppAngular.config(function($routeProvider) {
  $routeProvider
  .when("/", {
	templateUrl : url_archivo + "vistas/Home/Index.html",
	
  })
  .when("/gen_pedido", {
	templateUrl : url_archivo + "vistas/General/Pedidos.html?unico=" + Math.round(Math.random() * 10000),
	controller : "ControlPedido"
  })
  .when("/com_compras", {
	templateUrl : url_archivo + "vistas/Compras/Compra.html?unico=" + Math.round(Math.random() * 10000),
	controller : "ControlCompras"
  })
  .when("/ter_orden_compra", {
	templateUrl : url_archivo + "vistas/Tercerizado/EnvTercerizado.html?unico=" + Math.round(Math.random() * 10000),
	controller : "ControlOrdenCompras"
  })
});


var MenuController = function($scope){
    $scope.scopeHeader = "Este es un elemento del header";
    $scope.firstName = "John";
    $scope.lastName = "Doe";
    $scope.fullName = function() {
        return $scope.firstName + " " + $scope.lastName;
    };

    $scope.edad = function() {
        return $scope.firstName  + " " + 2+5;
    };
};
AppAngular.controller("MenuController", MenuController);

var Contenedor2 = function($scope){
    $scope.edad2 = "hola";
};
AppAngular.controller("Contenedor2", Contenedor2);




