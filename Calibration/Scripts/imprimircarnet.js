var error = "";
var cedula_per = "";
var fototemporal = "";

var altoimage = 0;
var anchoimage = 0;
var valor = 1;
 LlenarTablaPeriodo();
$("#guardarfotocam").on('click', function(){
       // reproducir();
        var data = $("#guardarfotocamara").serialize();
		var url = "../clases/ClassImprimirCarnet.php";
		var parametro = "opcion=8&"+data;
		var datos = LlamarAjax(url,parametro);
		$("#thumbnail").prop("src",datos);
		$("#im_fotoper").prop("src",datos);
		$("#default_modal").modal("hide");
             
});  
 
function reproducir(audio){
         var camara=document.getElementById("audio");	
         camara.play();


}
function ValidarSuma(texto){
	var valor = ""
	for (i = 0; i < texto.length; i++){
		if (texto.charAt(i) == "+")
			valor += "%2B";
		else
			valor += texto.charAt(i);
	}
	return valor;
}

$('form').bind("keypress", function(e) {
  if (e.keyCode == 13) {               
    e.preventDefault();
    return false;
  }
});

LlenarGerencia();

function LlenarGerencia(){
	
	var url = "../clases/ClassImprimirCarnet.php";
	var parametro = "opcion=4";
	var datos = LlamarAjax(url,parametro);
	$("#im_gerencia").html(datos);
	
}

function preview(img, selection) { 
	var scaleX = 200 / selection.width; 
	var scaleY = 200 / selection.height; 
	//reproducir();
	anchoimage = $("#thumbnail").width();
	altoimage = $("#thumbnail").height();
	
	valor = altoimage/anchoimage;
	
	$('#im_fotoper').css({ 
		width: Math.round(scaleX * anchoimage) + 'px', 
		height: Math.round(scaleY * altoimage) + 'px',
		marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px', 
		marginTop: '-' + Math.round(scaleY * selection.y1) + 'px' 
	});
	$('#x1').val(selection.x1);
	$('#y1').val(selection.y1);
	$('#x2').val(selection.x2);
	$('#y2').val(selection.y2);
	$('#w').val(selection.width);
	$('#h').val(selection.height);
}

function ImprimirLateral(){
	var parametro = "opcion=7&id="+$("#im_id").val()+"&sangre="+ValidarSuma($("#im_sangre").val());
	var url = "../clases/ClassImprimirCarnet.php";
	var datos = LlamarAjax(url, parametro);
	datos = datos.split('||');
	if (datos[0] == "0")
		window.open(datos[1]);
	else
		swal("Disculpe...","Error al conectarse con el servidor","warning");
} 

$("#FormCarnet").submit(function(evento){
	var parametro = "opcion=1&"+$("#FormCarnet").serialize()+"&imagen="+$("#thumbnail").prop("src")+"&gerencia="+$("#im_gerencia option:selected").text()+"&imagen2="+$("#im_fotoper").prop("src");
	var url = "../clases/ClassImprimirCarnet.php";
	var datos = LlamarAjax(url, parametro);
	datos = datos.split('||');
	if (datos[0] == "0"){
		window.open(datos[1]);
		return false;
	}
 if (datos[0] == "101"){
		swal("Disculpe...", datos[1],"warning");
		return false;
	}
	 if (datos[0] == "102"){
		swal("Disculpe...", datos[1],"warning");
		return false;
	}
	if (datos[0] == "55"){
		swal("Disculpe...", datos[1],"warning");
		return false;

	}
	if (datos[0] == "99"){
		swal("Disculpe...", datos[1],"warning");
		return false;

	}
	else{
		swal("Disculpe...","Error al conectarse con el servidor","warning");
			return false;

	}
});




$(document).ready(function () { 
	$('#save_thumb').click(function() {
		var x1 = $('#x1').val();
		var y1 = $('#y1').val();
		var x2 = $('#x2').val();
		var y2 = $('#y2').val();
		var w = $('#w').val();
		var h = $('#h').val();
		if(x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h==""){
			alert("Debe de Cortar la Imagen");
			return false;
		}else{
			return true;
		}
	});
}); 

$(window).load(function () { 
	$('#thumbnail').imgAreaSelect({ aspectRatio: '1:'+valor, onSelectChange: preview }); 
});

function RenombrarArchivo(origen, nuevo){
	var url = '../php/RenombrarArchivo.php';
	var data = 'origen='+origen+'&nuevo='+nuevo;
	$.ajax({
		url:url,
		type:'POST',
		data:data,
		success: function(datos){
			resultado = datos;
		}	
	});	
}


 function EditarPeriodo(idperio,fechaperi){

	$("#idperio").val(idperio);
	$("#fechaperi").val(fechaperi);

}


 function Limpiarperiodo(){

	$("#idperio").val("");
	$("#fechaperi").val("");

}



$("#FormPeriodo").submit(function(evento){

	var parametro = "opcion=9&"+$("#FormPeriodo").serialize()
		                       +"&idperio="+$("#idperio").val()
	                           +"&fechaperi="+$("#fechaperi").val()

	var url = '../clases/ClassImprimirCarnet.php';
	var datos = LlamarAjax(url, parametro);



	datos = datos.split("||");


  if (datos[0] == "77"){
		swal("Error...",datos[1],"warning");
  }


if (datos[0] == "80"){
		swal("Error...",datos[1],"warning");
  }


	if (datos[0] == "1"){
		swal("Excelente...",datos[1],"success");
	LlenarTablaPeriodo();
	Limpiarperiodo();
	}else{
		swal("Disculpe...",datos[1],"warning");
	} 
	
	return false;
});	




function CargarFoto(foto){
	var id = $("#im_id").val()*1;
	$("#thumbnail").prop("src","../fotos/rostro.png");
	if (id > 0){ 
		var archivo = LlamarArhivoAjax('im_archivo');
		//alert(archivo);
		archivo =  archivo.split('||');
		if (archivo[0] != "")
			swal('Disculpe...','Error al subir la foto ' + foto,'warning');
		else{
			$("#thumbnail").prop("src","../archivo/"+archivo[1]);
			$("#im_fotoper").prop("src","../archivo/"+archivo[1]);
			
		}	
	}		
}

/*$("#form_foto").submit(function(evento){
	var url = '../php/guardarfotos.php';
	var parametro = $("#form_foto").serialize()+"&lugar="+$("#thumbnail").prop("src");
	var datos = LlamarAjax(url, parametro);
	fototemporal = datos;
	$("#im_fotoper").prop("src",datos);
	return false;
});*/


function LlenarTablaAuditoria(){
	var url = "../clases/ClassImprimirCarnet.php";
	var parametro = "opcion=2&tabla=3";
	var datos = LlamarAjax(url, parametro);
	$("#detalleauditoria").html(datos);
	$("#tablaauditoria").DataTable();
}

function LlenarTablaPeriodo(){
	var url = "../clases/ClassImprimirCarnet.php";
	var parametro = "opcion=2&tabla=4";
	var datos = LlamarAjax(url, parametro);
	$("#detalleperiodo").html(datos);
	$("#tablaperiodo").DataTable();
}


function Limpiar(){
	
	$("#im_id").val("0");
	$("#im_nombres").val("");
	$("#im_apellidos").val("");
	$("#im_sangre").val("");
	$("#im_gerencia").val("");
    $("#id_nom").val("");
    $("#nucleo").val("");
    $("#idperiodoac").val("");
    $("#desperi").val("");
	$("#im_fotoper").prop("src","../fotos/0.jpg");
	$("#thumbnail").prop("src","../fotos/0.jpg");
	
	$('#x1').val("");
	$('#y1').val("");
	$('#x2').val("");
	$('#y2').val("");
	$('#w').val("");
	$('#h').val(""); 
	
}	

function BuscarCedula(codigo, cedula){
	
	if (cedula_per != cedula){		
		if ($("#nombres").val() != "")
			Limpiar();
	}

	if ((codigo.keyCode == 13) && (error == "")){
		var nom = '';
		var ape = '';	
		var url = "../clases/ClassImprimirCarnet.php";
		var parametro = "opcion=5&cedula="+cedula;
		datos = LlamarAjax(url,parametro);

       
		if (datos[0].split("||")){
			data = datos.split("||"); 
			$("#im_id").val(data[4]);
			var nom = $.trim(data[1]).split(" ");

       //alert(nom[2]);



			if (!nom[2]) 
				 nom[2]='';

	

          // alert(nom[2]);
			$("#im_nombres").val(nom[0]+" "+nom[1]+" "+nom[2]);

			//var ape = $.trim(data[2]).split(" ");
			$("#im_apellidos").val(data[2]);

			$("#im_sangre").val(data[3]);
			$("#im_gerencia").val(data[8]);
			$("#nucleo").val(data[9]);
			$("#id_nom").val(data[6]);
			$("#sexo").val(data[5]);
			$("#idperiodoac").val(data[11]);
			$("#desperi").val(data[12]);
			var foto = $.trim(data[4]) + '.jpg';
			$("#im_fotoper").attr("src","../fotos/"+foto);

	   if (data[13]=='200') {
	   	
		 swal("Disculpe...","Este Alumno con la cedula  "+$("#im_cedula").val()+"  YA  RETIRO SU CARNET ESTE AÑO...","warning");
		 return false;

		}

		}

	   if (data[0]=='301') {
	   	
		 swal("Disculpe...","Esta cédula  "+$("#im_cedula").val()+ " no se encuentra asignada a un Estudiante ...","warning");
		 return false;

		}
	if(datos=="300") {
		error = "1";
		 Limpiar();
		 swal("Disculpe...","Esta cédula  "+$("#im_cedula").val()+"  Pertenece a un alumno inscrito en un periodo anterior ...","warning");
		 return false;
		}


	}else{

		error = "";	
	}

	    
    }	
 
