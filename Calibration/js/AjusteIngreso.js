﻿var Ingreso = 0;
var IdReporte = 0;
var ErrorEnter = 0;
var Fotos = 0;
var CorreoEmpresa = "";
var Empresa = "";
var TipoAprobacion = 0;

var Asesor = "";
var Habitual = "NO";
var AprobarCotizacion = "SI";
var Aprobar_Ajuste = "SI";
var Sitio;
var IdReporte = 0;
var FechaReporte = "";

$("#ClienteIngreso").html(CargarCombo(9, 1));

localStorage.setItem("Ingreso", "0");
var PermisioEspecial = PerGeneSistema("PERMISO ADMINISTRADOR LABORATORIO");

function LimpiarTodo() {
    LimpiarDatos();
    $("#IngresoReporte").val("");
}

function LimpiarDatos() {
    Ingreso = 0;
    IdReporte = 0;
    Foto = 0;
    $("#Equipo").val("");
    $("#Marca").val("");
    $("#Modelo").val("");
    $("#Serie").val("");
    $("#Magnitud").val("");
    $("#Observacion").val("");
    $("#Remision").val("");
    $("#Rango").val("");
    localStorage.setItem("Ingreso", "0");

    $("#Usuario").val("");
    $("#FechaIng").val("");
    $("#Tiempo").val("");

    $("#Ajuste").val("");
    $("#Suministro").val("");
    $("#FechaEnvio").val("");
    $("#CorreoEnvio").val("");
    $("#FechaAprobacion").val("");
    $("#EstadoAjuste").val("");
    $("#ObservacionAprobacion").val("");
    $("#NroAjuste").val("");
    $("#FechaAjuste").val("");
    $("#DescripcionAjuste").val("");
    $("#TbSuministros").html("");
    $("#NumFoto").val("");
    $("#Documento").val("");
    $("#FotoMan").val("");
    $("#Concepto").val("").trigger("change");
    $("#Observaciones").val("");
    $("#Conclusion").val("");
    $("#InformeTecnico").val("");
    $("#Plantilla").html("");
    $("#DescripcionAjuste").prop("readonly", true);
    
}

$("#TablaReportar > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#IngresoReporte").val(numero);
    BuscarIngreso(numero,0,"0");
});

function LlamarFotoDet(ingreso, fotos) {
    if (fotos == 0)
        return false
    CargarModalFoto(ingreso, fotos, 1);
}

function ConsularReportesIngresos() {

    var cliente = $("#ClienteIngreso").val() * 1;
    var aprobadas = 0;
    if ($("#Aprobadas").prop("checked"))
        aprobadas = 1
            
    ActivarLoad();
    setTimeout(function () {
        var parametros = "cliente=" + cliente + "&aprobado=" + aprobadas;
        var datos = LlamarAjax("Laboratorio","opcion=TablaReportarAjuste&" + parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaReportar').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "ingreso" },
                { "data": "equipo" },
                { "data": "fecha" },
                { "data": "aprobacion" },
                { "data": "tiempo" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

ConsularReportesIngresos();

$('#IngresoReporte').keyup(function (event) {
	numero = this.value * 1;
	if (numero == -1)
		numero = Ingreso;


	if ((Ingreso != (numero * 1)) && ($("#Equipo").val() != "")) {
		LimpiarDatos();
		Ingreso = 0;
		ErrorEnter = 0;
	}
	if (event.which == 13) {
		event.preventDefault();

		if (Ingreso == numero)
			return false;
		BuscarIngreso(numero);
	}
});


function BuscarIngreso(numero) {
    if (ErrorEnter == 0) {
        if (numero * 1 > 0) {
            var datos = LlamarAjax("Laboratorio","opcion=BuscarIngresoAjuste&ingreso=" + numero).split("|");
            if (datos[0] != "[]") {
                var data = JSON.parse(datos[0]);
                if (data[0].idrecibido * 1 == 0) {
                    $("#IngresoReporte").select();
                    $("#IngresoReporte").focus();
                    ErrorEnter = 1;
                    swal("Acción Cancelada", "El Ingreso número " + numero + " no se ha recibido en el laboratorio", "warning");
                    return false;
                }

                /*IdRemision = data[0].id * 1;
                if (data[0].idmagnitud * 1 == 6) {
                    $("#IngresoReporte").select();
                    $("#IngresoReporte").focus();
                    ErrorEnter = 1;
                    swal("Acción Cancelada", "Debe reportar los ajustes por la plantilla del certificado", "warning");
                    return false;
                }
                /*if (data[0].idmagnitud * 1 == 2) {
                    $("#IngresoReporte").select();
                    $("#IngresoReporte").focus();
                    ErrorEnter = 1;
                    swal("Acción Cancelada", "Debe reportar los ingresos por la opción <br><b>OPERACIONES PREVIAS <br>PRESION</b>", "warning");
                    return false;
                }*/
                Ingreso = data[0].ingreso * 1;
                localStorage.setItem("Ingreso", Ingreso);
                Fotos = data[0].fotos * 1;

                Asesor = "/" + data[0].asesor + "/";
                Sitio = data[0].sitio;
                Habitual = data[0].habitual;
                Aprobar_Ajuste = data[0].aprobar_ajuste;

                $("#Equipo").val(data[0].equipo);
                $("#Marca").val(data[0].marca);
                $("#Modelo").val(data[0].modelo);
                $("#Serie").val(data[0].serie);
                $("#Observacion").val(data[0].observacion);
                $("#Rango").val(data[0].intervalo);
                $("#Remision").val(data[0].remision);
                $("#Magnitud").val(data[0].magnitud);

                $("#ObservRecep").val(data[0].observaesp);

                $("#Sitio").val(data[0].sitio);
                $("#Garantia").val(data[0].garantia);
                $("#Accesorio").val(data[0].accesorio);

                $("#Usuario").val(data[0].usuarioing);
                $("#FechaIng").val(data[0].fechaing);
                $("#Tiempo").val(data[0].tiempo);

                $("#Plantilla").html(datos[1]);
                $("#Plantilla").focus();

            } else {
                $("#IngresoReporte").select();
                $("#IngresoReporte").focus();
                ErrorEnter = 1;
                swal("Acción Cancelada", "El Ingreso número " + numero + " no se encuentra registrado en el laboratorio", "warning");
                return false;
            }
        }
    } else
        ErrorEnter = 0;
}


$("#Plantilla").change(function (event) {
	if (Ingreso == 0)
		return false;
	CambioPlantillaAjus(this.value);
});

function LimpiarCambioPlantilla(){
	$("#Ajuste").val("");
    $("#Suministro").val("");
    $("#FechaEnvio").val("");
    $("#CorreoEnvio").val("");
    $("#FechaAprobacion").val("");
    $("#EstadoAjuste").val("");
    $("#ObservacionAprobacion").val("");
    $("#NroAjuste").val("");
    $("#FechaAjuste").val("");
    $("#UsuarioAjuste").val("");
    $("#DescripcionAjuste").val("");
    $("#TbSuministros").html("");
    $("#NumFoto").val("");
    $("#Documento").val("");
    $("#FotoMan").val("");
    $("#Concepto").val("").trigger("change");
    $("#Observaciones").val("");
    $("#Conclusion").val("");
    $("#InformeTecnico").val("");
    $("#DescripcionAjuste").prop("readonly", true);
    $("#NumFotoAjuste").val("1").trigger("change");
	$("#NumFoto").val("1").trigger("change");
}

function CambioPlantillaAjus(plantilla) {
    
    LimpiarCambioPlantilla();

    if (plantilla * 1 == 0)
        return false;

    var datos = LlamarAjax("Laboratorio","opcion=CambioPlantillaAjuste&ingreso=" + Ingreso + "&plantilla=" + plantilla);
    if (datos == "[]") {
        $("#Plantilla").val("").trigger("change");
        $("#Plantilla").focus();
        swal("Acción Cancelada", "Esta plantilla no posee un reporte que requiera ajuste o suministro", "warning");
        return false;
    }

    var data = JSON.parse(datos);
    	
    var estado = "EN ESPERA DE LA APROBACION DEL CLIENTE";
    TipoAprobacion = data[0].tipo_aprobacion * 1;
    if (data[0].tipoaprobacion != "")
        estado = data[0].tipoaprobacion;
    if (Habitual == "SI" || Aprobar_Ajuste == "NO")
        estado = "NO REQUIERE APROBACION DEL CLIENTE (habitual)";
    if (estado == "EN ESPERA DE LA APROBACION DEL CLIENTE") {
        $("#Plantilla").val("").trigger("change");
        $("#Plantilla").focus();
        swal("Acción Cancelada", "El ajuste o suministro no ha sido autorizado por el cliente", "warning");
        chat.server.send(usuariochat, "Buen días amigo. Se requiere la autorización  del ajuste del ingreso número " + Ingreso + " para proceder a la calibración", '', Asesor);
        return false;
    } else {
        if (TipoAprobacion == 0)
            TipoAprobacion == 1;
    }
       
    $("#Ajuste").val(data[0].ajuste);
    $("#Suministro").val(data[0].suministro);
    $("#FechaEnvio").val(data[0].fechaenvio);
    $("#CorreoEnvio").val(data[0].correoenvio);
    $("#FechaAprobacion").val(data[0].fechaaprobacion);
    NroReporta = data[0].nroreporte;
    IdReporte = data[0].idreporte;
    FechaReporte = data[0].fechareporte;

    $("#EstadoAjuste").val(estado);
    $("#ObservacionAprobacion").val(data[0].observacionapro);
    $("#DescripcionAjuste").prop("readonly", false);
    
    if (data[0].estado == "Anulado"){
		swal("Acción Cancelada","El ajuste número " + data[0].numajuste + " fue anulado","warning");
		return false;
	}
	
	    
    $("#NroAjuste").val(data[0].numajuste);
    $("#FechaAjuste").val(data[0].fechaajus);
    $("#UsuarioAjuste").val(data[0].usuariajuste);
    $("#DescripcionAjuste").val(data[0].descripcion);
    $("#Concepto").val((data[0].idconcepto2 * 1 == 0 ? "" : data[0].idconcepto2)).trigger("change");
    $("#Observaciones").val(data[0].observacion2);
    $("#Conclusion").val(data[0].conclusion2);
    $("#InformeTecnico").val(data[0].informetecnico);

    if ((data[0].fechaajus) == "") {
        if (data[0].ajuste * 1 == 0) {
            if (TipoAprobacion == 2) {
                $("#DescripcionAjuste").val("El cliente aprobó realizar el certificado con el error", "warning");
                $("#DescripcionAjuste").prop("readonly", true);
            }

            $("#Concepto").val("1").trigger("change");

        } else {
            swal("Acción Cancelada", "Este ajuste ya fue aplicado a esta plantilla", "warning");
        }
    }
    
    $("#NumFotoAjuste").val("1").trigger("change");
	$("#NumFoto").val("1").trigger("change");
    
}

function GuardarAjuste() {
    var plantilla = $("#Plantilla").val() * 1;
    if (Ingreso == 0 || plantilla == 0)
        return false;

    var descripcion = $.trim($("#DescripcionAjuste").val());
    var idconcepto = $("#Concepto").val() * 1;
    var concepto = $("#Concepto option:selected").text();
    var observacion = $.trim($("#Observaciones").val());
    var conclusion = $.trim($("#Conclusion").val());

    if (TipoAprobacion == 0 && Habitual == "NO") {
        swal("Acción Cancelada", "El cliente no ha realizando níngun tipo de autorización", "warning");
        return false;
    }

    if (idconcepto == 0) {
        $("#Concepto").focus();
        swal("Acción Cancelada", "Debe de seleccionar un concepto", "warning");
        return false;
    }

    if (TipoAprobacion == 3 && idconcepto != 3) {
        swal("Acción Cancelada", "El cliente aprobó que se le realice el informe técnico para su devolución", "warning");
        return false;
    }

    if (descripcion == "") {
        $("#DescripcionAjuste").focus();
        swal("Acción Cancelada", "Debe de ingresar la descripción del ajuste", "warning");
        return false;
    }

    if (observacion == "") {
        $("#Observaciones").focus();
        swal("Acción Cancelada", "Debe de ingresar una observación", "warning");
        return false;
    }

    if (idconcepto == 3 && conclusion == "") {
        $("#Conclusion").focus();
        swal("Acción Cancelada", "Debe de ingresar una conclusión", "warning");
        return false;
    }

    var parametros = "ingreso=" + Ingreso + "&plantilla=" + plantilla + "&concepto=" + concepto + "&descripcion=" + descripcion + "&observacion=" + observacion +
        "&conclusion=" + conclusion + "&idconcepto=" + idconcepto + "&fecha=" + FechaReporte + "&IdReporte=" + IdReporte;
    var datos = LlamarAjax("Laboratorio","opcion=GuardarAjuste&" + parametros).split("|");
    if (datos[0] == "0") {
        $("#NroAjuste").val(datos[2]);
        $("#FechaAjuste").val(datos[4]);
        $("#UsuarioAjuste").val(datos[3]);
        CambioPlantillaAjus(plantilla);
        swal("", datos[1], "success");
    } else
        swal("Acción Cancelada", datos[1], "warning");
}


function GenerarInforme() {

    var idreporte = $("#IdReporte").val() * 1;
    var plantilla = $("#Plantilla").val() * 1;

    if (Ingreso == 0)
        return false;

    if (idreporte == 0) {
        swal("Acción Cancelada", "Debe primero guardar el ajuste", "warning");
        return false;
    }

    var concepto = $("#Concepto").val() * 1;
    if (concepto != 3) {
        swal("Acción Cancelada", "Solo se podrá generar informes cuando el ingreso no está apto para calibrar ó para mantenimiento y limpieza", "warning");
        return false;
    }

    var mensaje = '¿Seguro que desea generar el informe técnico del ingreso número ' + Ingreso + '?';
       
    
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: mensaje,
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Generar Informe'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Laboratorio","opcion=InformeTecnicoDev&ingreso=" + Ingreso + "&reporte=" + NroReporta + "&plantilla=" + plantilla)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            $("#InformeTecnico").val(datos[2]);
                            ImprimirInforme();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};

function VistaInforme(reporte) {

    if (Ingreso == 0)
        return false;

    var concepto = $("#Concepto").val() * 1;

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&tipo=1&concepto=" + concepto;
        var datos = LlamarAjax("Laboratorio/RpInformeTecnico", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);
}

function ImprimirInforme(reporte) {

    if (Ingreso == 0)
        return false;
        
	var plantilla = $("#Plantilla").val() * 1;
    
    ActivarLoad();
	setTimeout(function () {
		var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=InformeTecnico&documento=0&ingreso=" + Ingreso + "&plantilla=" + plantilla).split("||");
		DesactivarLoad();
		if (datos[0] == "0") {
			window.open(url_archivo + "DocumPDF/" + datos[1]);
		} else {
			swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
		}
		
	}, 15);
}

function CambioConcepto(concepto) {
    $("#Observaciones").prop("readonly", false);
    $("#Conclusion").prop("readonly", true);
    
    $("#Observaciones").addClass("bg-amarillo");
    $("#Conclusion").removeClass("bg-amarillo");

    $("#Documento").removeClass("bg-amarillo");
    $("#NumFoto").removeClass("bg-amarillo");
    $("#Documento").prop("disabled", true);
    $("#NumFoto").prop("disabled", true);

    if (concepto == "2") {
        $("#Observaciones").prop("readonly", true);
        $("#Conclusion").prop("readonly", true);
        $("#Conclusion").removeClass("bg-amarillo");
        $("#Observaciones").removeClass("bg-amarillo");

    } else {
        if (concepto == "3") {
            $("#Observaciones").prop("readonly", false);
            $("#Conclusion").prop("readonly", false);

            $("#Observaciones").addClass("bg-amarillo");
            $("#Conclusion").addClass("bg-amarillo");
            $("#Documento").addClass("bg-amarillo");
            $("#NumFoto").removeClass("bg-amarillo");
            $("#Documento").prop("disabled", false);
            $("#NumFoto").prop("disabled", false);
        }
    }
}
    


function GenerarCertificado() {

}

function AnularAjusteIngreso(){
	var ajuste = $("#NroAjuste").val() * 1;
    var plantilla = $("#Plantilla").val() * 1;
    
    if (ajuste == 0)
        return false;
    

    var mensaje = '¿Seguro que desea anular el ajuste número ' + ajuste + ' del ingreso número ' +  Ingreso + '?';
    swal({
		title: 'Advertencia',
		text: mensaje,
		input: 'text',
		showLoaderOnConfirm: true,
		showCancelButton: true,
		confirmButtonText: ValidarTraduccion('Anular'),
		cancelButtonText: ValidarTraduccion('Cancelar'),
		confirmButtonClass: 'btn btn-success',
		cancelButtonClass: 'btn btn-danger',

		inputValidator: function (value) {
			return new Promise(function (resolve, reject) {
				if (value) {
					var datos = LlamarAjax("Laboratorio","opcion=AnularAjusteIngreso&observaciones=" + value + "&ajuste=" + ajuste)
					datos = datos.split("|");
					if (datos[0] == "0") {
						resultado = datos[1];
						resolve()
					} else {
						reject(datos[1]);
					}

				} else {
					reject(ValidarTraduccion('Debe de ingresar un observación'));
				}
			})
		}
	}).then(function (result) {
					
		swal({
			type: 'success',
			html: 'Ajuste número ' + ajuste + ' anulado con éxito'
		});
		LimpiarCambioPlantilla();
	});
}

function HistorialAjustesAnulados() {
	
	if (Ingreso == 0)
		return false;
    
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Laboratorio","opcion=HistorialAjusteAnulados&ingreso=" + Ingreso);
        DesactivarLoad();
        
        if (datos == "[]"){
			swal("Acción Cancelada","No hay historial de reportes eliminados para este ingreso","warning");
			return false;
		}

        var datajson = JSON.parse(datos);
        $('#TablaAjusteAnulado').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "ajuste", "className": "text-XX" },
                { "data": "plantilla" },
                { "data": "descripcion" },
                { "data": "ajustado"},
                { "data": "anulado"},
                { "data": "observacion_anulacion"}
            ],


            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ],
            "lengthMenu": [[7], [7]],
        });
        $("#rpIngreso").html(Ingreso);
        $("#ModalAjustesAnulados").modal("show");
    }, 15);

}

function GuardarDocumento(tipo, id,idnumero) {
    var documento = document.getElementById(id);
    var ajuste = $("#NroAjuste").val() * 1;
    var numero = $("#" + idnumero).val() * 1;
    var informe = $("#InformeTecnico").val()*1;
    var parametros = "";

    if ($.trim(documento.value) != "" && numero != 0) {

		if (tipo == 1){
			if (ajuste == 0) {
				documento.value = "";
				swal("Acción Cancelada", "Debe guardar primero el ajuste de ingreso", "warning");
				return false;
			}
		}
		
		if (tipo == 2){
			if (informe == 0) {
				documento.value = "";
				swal("Acción Cancelada", "Debe generar primero el informe técnico", "warning");
				return false;
			}
		}
        
        
        var archivo = GuardarDocumentoAdjunto(id,4,'');
        if (archivo == "" || !archivo){
			swal("Acción Cancelada", "Error al adjuntar la imagen", "warning");
			return false;
		}
		
		ActivarLoad();
		setTimeout(function () {
			if (tipo == 1)
				parametros = "opcion=SubirFotoMultiple&ajuste=" + ajuste + "&numero=" + numero + "&tipo_imagen=6";
			else
				parametros = "opcion=SubirFotoMultiple&informe=" + informe + "&numero=" + numero + "&tipo_imagen=5";
			var data = LlamarAjax("Configuracion",parametros).split("|");
			if (data[0] == "0"){
				MostrarFoto(tipo, numero, id);
				DesactivarLoad();
			}else{
				DesactivarLoad();
				swal("Acción Cancelada",data[1],"warning");
			}
		},15);
    }else{
		documento.value = "";
	}
}

function VerFotos() {
    if (Ingreso == 0)
        return false;
    CargarModalFoto(Ingreso, 0,1);
}

function VistaInforme(){
	ImprimirInforme();
}




function MostrarFoto(tipo, numero, id) {
	if (numero*1 == 0)
		return false;
    var carpeta = (tipo == 1 ? "Adjunto/imagenes/ajuste/" + $("#NroAjuste").val() : "Adjunto/imagenes/informemantenimineto/" +  $("#InformeTecnico").val());
    $("#" + (tipo == 1 ? "FotoAjuste" :  "FotoInforme")).attr("src", url_cliente + carpeta + "/" + numero + ".jpg?id=" + NumeroAleatorio());
}

$("img").error(function() {
    $(this).attr("src", url_cliente + "Adjunto/imagenes/default.jpg");
});


$('select').select2();
